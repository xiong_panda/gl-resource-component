package po;

import annotation.FieldOrder;

//核心资源数据 中文期刊
public class WInfoOrgPO {
    @FieldOrder(order = 1)	private String 	F_ID;
    @FieldOrder(order = 1000)	private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)	private String 	FullText;
    @FieldOrder(order = 4)	private String 	NAME;
    @FieldOrder(order = 5)	private String 	NAME_NAME_ANYNAME;
    @FieldOrder(order = 6)	private String 	NAME_ANYNAME;
    @FieldOrder(order = 7)	private String 	ACNAME;
    @FieldOrder(order = 8)	private String 	CNAME;
    @FieldOrder(order = 9)	private String 	SNAME;
    @FieldOrder(order = 10)	private String 	ONAME;
    @FieldOrder(order = 11)	private String 	PERSON;
    @FieldOrder(order = 12)	private String 	PROV;
    @FieldOrder(order = 13)	private String 	CITY;
    @FieldOrder(order = 14)	private String 	COUN;
    @FieldOrder(order = 15)	private String 	REGION;
    @FieldOrder(order = 16)	private String 	ADDR;
    @FieldOrder(order = 17)	private String 	ACODE;
    @FieldOrder(order = 18)	private String 	TEL;
    @FieldOrder(order = 19)	private String 	FAX;
    @FieldOrder(order = 20)	private String 	ZIP;
    @FieldOrder(order = 21)	private String 	EMAIL;
    @FieldOrder(order = 22)	private String 	URL;
    @FieldOrder(order = 23)	private String 	FOUND;
    @FieldOrder(order = 24)	private String 	EMPS;
    @FieldOrder(order = 25)	private String 	ORGTYPE;
    @FieldOrder(order = 26)	private String 	SPACE;
    @FieldOrder(order = 27)	private String 	COLNO;
    @FieldOrder(order = 28)	private String 	INTRO;
    @FieldOrder(order = 29)	private String 	SYS;
    @FieldOrder(order = 30)	private String 	SPECOL;
    @FieldOrder(order = 31)	private String 	SPETYPE;
    @FieldOrder(order = 32)	private String 	DIGRES;
    @FieldOrder(order = 33)	private String 	SELFRES;
    @FieldOrder(order = 34)	private String 	ORGID;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getNAME_NAME_ANYNAME() {
        return NAME_NAME_ANYNAME;
    }

    public void setNAME_NAME_ANYNAME(String NAME_NAME_ANYNAME) {
        this.NAME_NAME_ANYNAME = NAME_NAME_ANYNAME;
    }

    public String getNAME_ANYNAME() {
        return NAME_ANYNAME;
    }

    public void setNAME_ANYNAME(String NAME_ANYNAME) {
        this.NAME_ANYNAME = NAME_ANYNAME;
    }

    public String getACNAME() {
        return ACNAME;
    }

    public void setACNAME(String ACNAME) {
        this.ACNAME = ACNAME;
    }

    public String getCNAME() {
        return CNAME;
    }

    public void setCNAME(String CNAME) {
        this.CNAME = CNAME;
    }

    public String getSNAME() {
        return SNAME;
    }

    public void setSNAME(String SNAME) {
        this.SNAME = SNAME;
    }

    public String getONAME() {
        return ONAME;
    }

    public void setONAME(String ONAME) {
        this.ONAME = ONAME;
    }

    public String getPERSON() {
        return PERSON;
    }

    public void setPERSON(String PERSON) {
        this.PERSON = PERSON;
    }

    public String getPROV() {
        return PROV;
    }

    public void setPROV(String PROV) {
        this.PROV = PROV;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCOUN() {
        return COUN;
    }

    public void setCOUN(String COUN) {
        this.COUN = COUN;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getACODE() {
        return ACODE;
    }

    public void setACODE(String ACODE) {
        this.ACODE = ACODE;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getFOUND() {
        return FOUND;
    }

    public void setFOUND(String FOUND) {
        this.FOUND = FOUND;
    }

    public String getEMPS() {
        return EMPS;
    }

    public void setEMPS(String EMPS) {
        this.EMPS = EMPS;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getSPACE() {
        return SPACE;
    }

    public void setSPACE(String SPACE) {
        this.SPACE = SPACE;
    }

    public String getCOLNO() {
        return COLNO;
    }

    public void setCOLNO(String COLNO) {
        this.COLNO = COLNO;
    }

    public String getINTRO() {
        return INTRO;
    }

    public void setINTRO(String INTRO) {
        this.INTRO = INTRO;
    }

    public String getSYS() {
        return SYS;
    }

    public void setSYS(String SYS) {
        this.SYS = SYS;
    }

    public String getSPECOL() {
        return SPECOL;
    }

    public void setSPECOL(String SPECOL) {
        this.SPECOL = SPECOL;
    }

    public String getSPETYPE() {
        return SPETYPE;
    }

    public void setSPETYPE(String SPETYPE) {
        this.SPETYPE = SPETYPE;
    }

    public String getDIGRES() {
        return DIGRES;
    }

    public void setDIGRES(String DIGRES) {
        this.DIGRES = DIGRES;
    }

    public String getSELFRES() {
        return SELFRES;
    }

    public void setSELFRES(String SELFRES) {
        this.SELFRES = SELFRES;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }


}
