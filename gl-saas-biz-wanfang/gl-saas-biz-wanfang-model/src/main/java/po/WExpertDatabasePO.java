package po;

import annotation.FieldOrder;

//万方原始数据
public class WExpertDatabasePO {
    @FieldOrder(order = 1)
    private String F_ID;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String FullText;
    @FieldOrder(order = 4)
    private String NAME;
    @FieldOrder(order = 5)
    private String GEN;
    @FieldOrder(order = 6)
    private String AGE;
    @FieldOrder(order = 7)
    private String YEAR;
    @FieldOrder(order = 8)
    private String BORN;
    @FieldOrder(order = 9)
    private String NAT;
    @FieldOrder(order = 10)
    private String ORG;
    @FieldOrder(order = 11)
    private String ORG2;
    @FieldOrder(order = 12)
    private String WORK;
    @FieldOrder(order = 13)
    private String WORKO;
    @FieldOrder(order = 14)
    private String EDU;
    @FieldOrder(order = 15)
    private String FLAN;
    @FieldOrder(order = 16)
    private String WORKE;
    @FieldOrder(order = 17)
    private String JOBS;
    @FieldOrder(order = 18)
    private String JOBS2;
    @FieldOrder(order = 19)
    private String HONOR;
    @FieldOrder(order = 20)
    private String TITLE;
    @FieldOrder(order = 21)
    private String ADDR;
    @FieldOrder(order = 22)
    private String ZIP;
    @FieldOrder(order = 23)
    private String LOC;
    @FieldOrder(order = 24)
    private String PROV;
    @FieldOrder(order = 25)
    private String CITY;
    @FieldOrder(order = 26)
    private String COUN;
    @FieldOrder(order = 27)
    private String REGION;
    @FieldOrder(order = 28)
    private String TEL;
    @FieldOrder(order = 29)
    private String FAX;
    @FieldOrder(order = 30)
    private String EMAIL;
    @FieldOrder(order = 31)
    private String PROF;
    @FieldOrder(order = 32)
    private String PROF_ANYNAME;
    @FieldOrder(order = 33)
    private String ACADE;
    @FieldOrder(order = 34)
    private String ACADE_ANYNAME;
    @FieldOrder(order = 35)
    private String AWARDS;
    @FieldOrder(order = 36)
    private String AWARDT;
    @FieldOrder(order = 37)
    private String FOREIGN;
    @FieldOrder(order = 38)
    private String EXPTYPE;
    @FieldOrder(order = 39)
    private String BYIND;
    @FieldOrder(order = 40)
    private String DIS;
    @FieldOrder(order = 41)
    private String DIS_ANYNAME;
    @FieldOrder(order = 42)
    private String DID;
    @FieldOrder(order = 43)
    private String CID;
    @FieldOrder(order = 44)
    private String CLS;


    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getGEN() {
        return GEN;
    }

    public void setGEN(String GEN) {
        this.GEN = GEN;
    }

    public String getAGE() {
        return AGE;
    }

    public void setAGE(String AGE) {
        this.AGE = AGE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getBORN() {
        return BORN;
    }

    public void setBORN(String BORN) {
        this.BORN = BORN;
    }

    public String getNAT() {
        return NAT;
    }

    public void setNAT(String NAT) {
        this.NAT = NAT;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG2() {
        return ORG2;
    }

    public void setORG2(String ORG2) {
        this.ORG2 = ORG2;
    }

    public String getWORK() {
        return WORK;
    }

    public void setWORK(String WORK) {
        this.WORK = WORK;
    }

    public String getWORKO() {
        return WORKO;
    }

    public void setWORKO(String WORKO) {
        this.WORKO = WORKO;
    }

    public String getEDU() {
        return EDU;
    }

    public void setEDU(String EDU) {
        this.EDU = EDU;
    }

    public String getFLAN() {
        return FLAN;
    }

    public void setFLAN(String FLAN) {
        this.FLAN = FLAN;
    }

    public String getWORKE() {
        return WORKE;
    }

    public void setWORKE(String WORKE) {
        this.WORKE = WORKE;
    }

    public String getJOBS() {
        return JOBS;
    }

    public void setJOBS(String JOBS) {
        this.JOBS = JOBS;
    }

    public String getJOBS2() {
        return JOBS2;
    }

    public void setJOBS2(String JOBS2) {
        this.JOBS2 = JOBS2;
    }

    public String getHONOR() {
        return HONOR;
    }

    public void setHONOR(String HONOR) {
        this.HONOR = HONOR;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getLOC() {
        return LOC;
    }

    public void setLOC(String LOC) {
        this.LOC = LOC;
    }

    public String getPROV() {
        return PROV;
    }

    public void setPROV(String PROV) {
        this.PROV = PROV;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCOUN() {
        return COUN;
    }

    public void setCOUN(String COUN) {
        this.COUN = COUN;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getPROF() {
        return PROF;
    }

    public void setPROF(String PROF) {
        this.PROF = PROF;
    }

    public String getPROF_ANYNAME() {
        return PROF_ANYNAME;
    }

    public void setPROF_ANYNAME(String PROF_ANYNAME) {
        this.PROF_ANYNAME = PROF_ANYNAME;
    }

    public String getACADE() {
        return ACADE;
    }

    public void setACADE(String ACADE) {
        this.ACADE = ACADE;
    }

    public String getACADE_ANYNAME() {
        return ACADE_ANYNAME;
    }

    public void setACADE_ANYNAME(String ACADE_ANYNAME) {
        this.ACADE_ANYNAME = ACADE_ANYNAME;
    }

    public String getAWARDS() {
        return AWARDS;
    }

    public void setAWARDS(String AWARDS) {
        this.AWARDS = AWARDS;
    }

    public String getAWARDT() {
        return AWARDT;
    }

    public void setAWARDT(String AWARDT) {
        this.AWARDT = AWARDT;
    }

    public String getFOREIGN() {
        return FOREIGN;
    }

    public void setFOREIGN(String FOREIGN) {
        this.FOREIGN = FOREIGN;
    }

    public String getEXPTYPE() {
        return EXPTYPE;
    }

    public void setEXPTYPE(String EXPTYPE) {
        this.EXPTYPE = EXPTYPE;
    }

    public String getBYIND() {
        return BYIND;
    }

    public void setBYIND(String BYIND) {
        this.BYIND = BYIND;
    }

    public String getDIS() {
        return DIS;
    }

    public void setDIS(String DIS) {
        this.DIS = DIS;
    }

    public String getDIS_ANYNAME() {
        return DIS_ANYNAME;
    }

    public void setDIS_ANYNAME(String DIS_ANYNAME) {
        this.DIS_ANYNAME = DIS_ANYNAME;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getCLS() {
        return CLS;
    }

    public void setCLS(String CLS) {
        this.CLS = CLS;
    }
}
