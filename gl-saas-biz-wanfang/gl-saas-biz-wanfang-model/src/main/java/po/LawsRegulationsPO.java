package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据
public class LawsRegulationsPO {
    @FieldOrder(order = 1)
    private String 	OrderID;
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	Title2;
    @FieldOrder(order = 5)
    private String 	Rules_Text;
    @FieldOrder(order = 6)
    private String 	KeyWord;
    @FieldOrder(order = 7)
    private String 	Proclaim_Department;
    @FieldOrder(order = 8)
    private String 	Proclaim_Department_FullText;
    @FieldOrder(order = 9)
    private String 	Proclaim_Department2;
    @FieldOrder(order = 10)
    private String 	AUTHOR;
    @FieldOrder(order = 11)
    private String 	Cls;
    @FieldOrder(order = 12)
    private String 	Proclaim_Date;
    @FieldOrder(order = 13)
    private String 	Final_Date;
    @FieldOrder(order = 14)
    private String 	RID;
    @FieldOrder(order = 15)
    private String 	Title;
    @FieldOrder(order = 16)
    private String 	English_Title;
    @FieldOrder(order = 17)
    private String 	Post_Document_Code;
    @FieldOrder(order = 18)
    private String 	ORG_ID;
    @FieldOrder(order = 19)
    private String 	Proclaim_Department3;
    @FieldOrder(order = 20)
    private String 	IndustryCode_Department_Code;
    @FieldOrder(order = 21)
    private String 	Value_Level;
    @FieldOrder(order = 22)
    private String 	Value_Code;
    @FieldOrder(order = 23)
    private String 	Timeliness;
    @FieldOrder(order = 24)
    private String 	Approval_Date;
    @FieldOrder(order = 25)
    private String 	Signature_Date;
    @FieldOrder(order = 26)
    private String 	YAPPRDATE;
    @FieldOrder(order = 27)
    private String 	YSIGNDATE;
    @FieldOrder(order = 28)
    private String 	IMPLEMENTATION_Date;
    @FieldOrder(order = 29)
    private String 	Invalid_Date;
    @FieldOrder(order = 30)
    private String 	Final_Court;
    @FieldOrder(order = 31)
    private String 	Final_Date2;
    @FieldOrder(order = 32)
    private String 	mediate_Date;
    @FieldOrder(order = 33)
    private String 	Cintent_Class;
    @FieldOrder(order = 34)
    private String 	Cintent_Class_Code;
    @FieldOrder(order = 35)
    private String 	URL;
    @FieldOrder(order = 36)
    private String 	PDF_FullText;
    @FieldOrder(order = 37)
    private String 	Relevant_link;
    @FieldOrder(order = 38)
    private String 	History_link;
    @FieldOrder(order = 39)
    private String 	Library_Code;
    @FieldOrder(order = 40)
    private String 	Make_Date;
    @FieldOrder(order = 41)
    private String 	Industry_Class;
    @FieldOrder(order = 42)
    private String 	Industry_Class_Code;
    @FieldOrder(order = 43)
    private String 	Discipline_Class;
    @FieldOrder(order = 44)
    private String 	Error_Code;
    @FieldOrder(order = 45)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getTitle2() {
        return Title2;
    }

    public void setTitle2(String title2) {
        Title2 = title2;
    }

    public String getRules_Text() {
        return Rules_Text;
    }

    public void setRules_Text(String rules_Text) {
        Rules_Text = rules_Text;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }

    public String getProclaim_Department() {
        return Proclaim_Department;
    }

    public void setProclaim_Department(String proclaim_Department) {
        Proclaim_Department = proclaim_Department;
    }

    public String getProclaim_Department_FullText() {
        return Proclaim_Department_FullText;
    }

    public void setProclaim_Department_FullText(String proclaim_Department_FullText) {
        Proclaim_Department_FullText = proclaim_Department_FullText;
    }

    public String getProclaim_Department2() {
        return Proclaim_Department2;
    }

    public void setProclaim_Department2(String proclaim_Department2) {
        Proclaim_Department2 = proclaim_Department2;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getCls() {
        return Cls;
    }

    public void setCls(String cls) {
        Cls = cls;
    }

    public String getProclaim_Date() {
        return Proclaim_Date;
    }

    public void setProclaim_Date(String proclaim_Date) {
        Proclaim_Date = proclaim_Date;
    }

    public String getFinal_Date() {
        return Final_Date;
    }

    public void setFinal_Date(String final_Date) {
        Final_Date = final_Date;
    }

    public String getRID() {
        return RID;
    }

    public void setRID(String RID) {
        this.RID = RID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getEnglish_Title() {
        return English_Title;
    }

    public void setEnglish_Title(String english_Title) {
        English_Title = english_Title;
    }

    public String getPost_Document_Code() {
        return Post_Document_Code;
    }

    public void setPost_Document_Code(String post_Document_Code) {
        Post_Document_Code = post_Document_Code;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getProclaim_Department3() {
        return Proclaim_Department3;
    }

    public void setProclaim_Department3(String proclaim_Department3) {
        Proclaim_Department3 = proclaim_Department3;
    }

    public String getIndustryCode_Department_Code() {
        return IndustryCode_Department_Code;
    }

    public void setIndustryCode_Department_Code(String industryCode_Department_Code) {
        IndustryCode_Department_Code = industryCode_Department_Code;
    }

    public String getValue_Level() {
        return Value_Level;
    }

    public void setValue_Level(String value_Level) {
        Value_Level = value_Level;
    }

    public String getValue_Code() {
        return Value_Code;
    }

    public void setValue_Code(String value_Code) {
        Value_Code = value_Code;
    }

    public String getTimeliness() {
        return Timeliness;
    }

    public void setTimeliness(String timeliness) {
        Timeliness = timeliness;
    }

    public String getApproval_Date() {
        return Approval_Date;
    }

    public void setApproval_Date(String approval_Date) {
        Approval_Date = approval_Date;
    }

    public String getSignature_Date() {
        return Signature_Date;
    }

    public void setSignature_Date(String signature_Date) {
        Signature_Date = signature_Date;
    }

    public String getYAPPRDATE() {
        return YAPPRDATE;
    }

    public void setYAPPRDATE(String YAPPRDATE) {
        this.YAPPRDATE = YAPPRDATE;
    }

    public String getYSIGNDATE() {
        return YSIGNDATE;
    }

    public void setYSIGNDATE(String YSIGNDATE) {
        this.YSIGNDATE = YSIGNDATE;
    }

    public String getIMPLEMENTATION_Date() {
        return IMPLEMENTATION_Date;
    }

    public void setIMPLEMENTATION_Date(String IMPLEMENTATION_Date) {
        this.IMPLEMENTATION_Date = IMPLEMENTATION_Date;
    }

    public String getInvalid_Date() {
        return Invalid_Date;
    }

    public void setInvalid_Date(String invalid_Date) {
        Invalid_Date = invalid_Date;
    }

    public String getFinal_Court() {
        return Final_Court;
    }

    public void setFinal_Court(String final_Court) {
        Final_Court = final_Court;
    }

    public String getFinal_Date2() {
        return Final_Date2;
    }

    public void setFinal_Date2(String final_Date2) {
        Final_Date2 = final_Date2;
    }

    public String getMediate_Date() {
        return mediate_Date;
    }

    public void setMediate_Date(String mediate_Date) {
        this.mediate_Date = mediate_Date;
    }

    public String getCintent_Class() {
        return Cintent_Class;
    }

    public void setCintent_Class(String cintent_Class) {
        Cintent_Class = cintent_Class;
    }

    public String getCintent_Class_Code() {
        return Cintent_Class_Code;
    }

    public void setCintent_Class_Code(String cintent_Class_Code) {
        Cintent_Class_Code = cintent_Class_Code;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getPDF_FullText() {
        return PDF_FullText;
    }

    public void setPDF_FullText(String PDF_FullText) {
        this.PDF_FullText = PDF_FullText;
    }

    public String getRelevant_link() {
        return Relevant_link;
    }

    public void setRelevant_link(String relevant_link) {
        Relevant_link = relevant_link;
    }

    public String getHistory_link() {
        return History_link;
    }

    public void setHistory_link(String history_link) {
        History_link = history_link;
    }

    public String getLibrary_Code() {
        return Library_Code;
    }

    public void setLibrary_Code(String library_Code) {
        Library_Code = library_Code;
    }

    public String getMake_Date() {
        return Make_Date;
    }

    public void setMake_Date(String make_Date) {
        Make_Date = make_Date;
    }

    public String getIndustry_Class() {
        return Industry_Class;
    }

    public void setIndustry_Class(String industry_Class) {
        Industry_Class = industry_Class;
    }

    public String getIndustry_Class_Code() {
        return Industry_Class_Code;
    }

    public void setIndustry_Class_Code(String industry_Class_Code) {
        Industry_Class_Code = industry_Class_Code;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getError_Code() {
        return Error_Code;
    }

    public void setError_Code(String error_Code) {
        Error_Code = error_Code;
    }

    @Override
    public String toString() {
        return "LawsRegulationsPO{" +
                "OrderID='" + OrderID + '\'' +
                ", ID='" + ID + '\'' +
                ", FullText='" + FullText + '\'' +
                ", Title2='" + Title2 + '\'' +
                ", Rules_Text='" + Rules_Text + '\'' +
                ", KeyWord='" + KeyWord + '\'' +
                ", Proclaim_Department='" + Proclaim_Department + '\'' +
                ", Proclaim_Department_FullText='" + Proclaim_Department_FullText + '\'' +
                ", Proclaim_Department2='" + Proclaim_Department2 + '\'' +
                ", AUTHOR='" + AUTHOR + '\'' +
                ", Cls='" + Cls + '\'' +
                ", Proclaim_Date='" + Proclaim_Date + '\'' +
                ", Final_Date='" + Final_Date + '\'' +
                ", RID='" + RID + '\'' +
                ", Title='" + Title + '\'' +
                ", English_Title='" + English_Title + '\'' +
                ", Post_Document_Code='" + Post_Document_Code + '\'' +
                ", ORG_ID='" + ORG_ID + '\'' +
                ", Proclaim_Department3='" + Proclaim_Department3 + '\'' +
                ", IndustryCode_Department_Code='" + IndustryCode_Department_Code + '\'' +
                ", Value_Level='" + Value_Level + '\'' +
                ", Value_Code='" + Value_Code + '\'' +
                ", Timeliness='" + Timeliness + '\'' +
                ", Approval_Date='" + Approval_Date + '\'' +
                ", Signature_Date='" + Signature_Date + '\'' +
                ", YAPPRDATE='" + YAPPRDATE + '\'' +
                ", YSIGNDATE='" + YSIGNDATE + '\'' +
                ", IMPLEMENTATION_Date='" + IMPLEMENTATION_Date + '\'' +
                ", Invalid_Date='" + Invalid_Date + '\'' +
                ", Final_Court='" + Final_Court + '\'' +
                ", Final_Date2='" + Final_Date2 + '\'' +
                ", mediate_Date='" + mediate_Date + '\'' +
                ", Cintent_Class='" + Cintent_Class + '\'' +
                ", Cintent_Class_Code='" + Cintent_Class_Code + '\'' +
                ", URL='" + URL + '\'' +
                ", PDF_FullText='" + PDF_FullText + '\'' +
                ", Relevant_link='" + Relevant_link + '\'' +
                ", History_link='" + History_link + '\'' +
                ", Library_Code='" + Library_Code + '\'' +
                ", Make_Date='" + Make_Date + '\'' +
                ", Industry_Class='" + Industry_Class + '\'' +
                ", Industry_Class_Code='" + Industry_Class_Code + '\'' +
                ", Discipline_Class='" + Discipline_Class + '\'' +
                ", Error_Code='" + Error_Code + '\'' +
                '}';
    }
}
