package po;

import annotation.FieldOrder;

//核心资源数据 中文期刊
public class WHigherLearningUniversitiesPO {
    @FieldOrder(order = 1)	private String 	F_ID;
    @FieldOrder(order = 1000)	private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)	private String 	FullText;
    @FieldOrder(order = 4)	private String 	NAME;
    @FieldOrder(order = 5)	private String 	NAME_NAME_ANYNAME;
    @FieldOrder(order = 6)	private String 	NAME_ANYNAME;
    @FieldOrder(order = 7)	private String 	ACNAME;
    @FieldOrder(order = 8)	private String 	OLDNAME;
    @FieldOrder(order = 9)	private String 	COMPUNIT;
    @FieldOrder(order = 10)	private String 	COMPUNIT_IKO;
    @FieldOrder(order = 11)	private String 	IKO;
    @FieldOrder(order = 12)	private String 	PROV;
    @FieldOrder(order = 13)	private String 	CITY;
    @FieldOrder(order = 14)	private String 	COUN;
    @FieldOrder(order = 15)	private String 	ACODE;
    @FieldOrder(order = 16)	private String 	REGION;
    @FieldOrder(order = 17)	private String 	ZIP;
    @FieldOrder(order = 18)	private String 	ADDR;
    @FieldOrder(order = 19)	private String 	TEL;
    @FieldOrder(order = 20)	private String 	FAX;
    @FieldOrder(order = 21)	private String 	URL;
    @FieldOrder(order = 22)	private String 	TECHS;
    @FieldOrder(order = 23)	private String 	EMAIL;
    @FieldOrder(order = 24)	private String 	SPACE;
    @FieldOrder(order = 25)	private String 	LEVELS;
    @FieldOrder(order = 26)	private String 	PERSON;
    @FieldOrder(order = 27)	private String 	STYPE;
    @FieldOrder(order = 28)	private String 	KEYDIS;
    @FieldOrder(order = 29)	private String 	SPESCHL;
    @FieldOrder(order = 30)	private String 	INTRO;
    @FieldOrder(order = 31)	private String 	CAMPUS;
    @FieldOrder(order = 32)	private String 	STR;
    @FieldOrder(order = 33)	private String 	HISTORY;
    @FieldOrder(order = 34)	private String 	SUBSCHL;
    @FieldOrder(order = 35)	private String 	GROSS;
    @FieldOrder(order = 36)	private String 	ORGID;
    @FieldOrder(order = 37)	private String 	FEATURECOLLEGE;
    @FieldOrder(order = 38)	private String 	FEATURECOLLEGE_FEATURECOLLEGE_ANYNAME;
    @FieldOrder(order = 39)	private String 	FEATURECOLLEGE_ANYNAME;
    @FieldOrder(order = 40)	private String 	COLLEGETYPE;
    @FieldOrder(order = 41)	private String 	FPD;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getNAME_NAME_ANYNAME() {
        return NAME_NAME_ANYNAME;
    }

    public void setNAME_NAME_ANYNAME(String NAME_NAME_ANYNAME) {
        this.NAME_NAME_ANYNAME = NAME_NAME_ANYNAME;
    }

    public String getNAME_ANYNAME() {
        return NAME_ANYNAME;
    }

    public void setNAME_ANYNAME(String NAME_ANYNAME) {
        this.NAME_ANYNAME = NAME_ANYNAME;
    }

    public String getACNAME() {
        return ACNAME;
    }

    public void setACNAME(String ACNAME) {
        this.ACNAME = ACNAME;
    }

    public String getOLDNAME() {
        return OLDNAME;
    }

    public void setOLDNAME(String OLDNAME) {
        this.OLDNAME = OLDNAME;
    }

    public String getCOMPUNIT() {
        return COMPUNIT;
    }

    public void setCOMPUNIT(String COMPUNIT) {
        this.COMPUNIT = COMPUNIT;
    }

    public String getCOMPUNIT_IKO() {
        return COMPUNIT_IKO;
    }

    public void setCOMPUNIT_IKO(String COMPUNIT_IKO) {
        this.COMPUNIT_IKO = COMPUNIT_IKO;
    }

    public String getIKO() {
        return IKO;
    }

    public void setIKO(String IKO) {
        this.IKO = IKO;
    }

    public String getPROV() {
        return PROV;
    }

    public void setPROV(String PROV) {
        this.PROV = PROV;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCOUN() {
        return COUN;
    }

    public void setCOUN(String COUN) {
        this.COUN = COUN;
    }

    public String getACODE() {
        return ACODE;
    }

    public void setACODE(String ACODE) {
        this.ACODE = ACODE;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getTECHS() {
        return TECHS;
    }

    public void setTECHS(String TECHS) {
        this.TECHS = TECHS;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getSPACE() {
        return SPACE;
    }

    public void setSPACE(String SPACE) {
        this.SPACE = SPACE;
    }

    public String getLEVELS() {
        return LEVELS;
    }

    public void setLEVELS(String LEVELS) {
        this.LEVELS = LEVELS;
    }

    public String getPERSON() {
        return PERSON;
    }

    public void setPERSON(String PERSON) {
        this.PERSON = PERSON;
    }

    public String getSTYPE() {
        return STYPE;
    }

    public void setSTYPE(String STYPE) {
        this.STYPE = STYPE;
    }

    public String getKEYDIS() {
        return KEYDIS;
    }

    public void setKEYDIS(String KEYDIS) {
        this.KEYDIS = KEYDIS;
    }

    public String getSPESCHL() {
        return SPESCHL;
    }

    public void setSPESCHL(String SPESCHL) {
        this.SPESCHL = SPESCHL;
    }

    public String getINTRO() {
        return INTRO;
    }

    public void setINTRO(String INTRO) {
        this.INTRO = INTRO;
    }

    public String getCAMPUS() {
        return CAMPUS;
    }

    public void setCAMPUS(String CAMPUS) {
        this.CAMPUS = CAMPUS;
    }

    public String getSTR() {
        return STR;
    }

    public void setSTR(String STR) {
        this.STR = STR;
    }

    public String getHISTORY() {
        return HISTORY;
    }

    public void setHISTORY(String HISTORY) {
        this.HISTORY = HISTORY;
    }

    public String getSUBSCHL() {
        return SUBSCHL;
    }

    public void setSUBSCHL(String SUBSCHL) {
        this.SUBSCHL = SUBSCHL;
    }

    public String getGROSS() {
        return GROSS;
    }

    public void setGROSS(String GROSS) {
        this.GROSS = GROSS;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getFEATURECOLLEGE() {
        return FEATURECOLLEGE;
    }

    public void setFEATURECOLLEGE(String FEATURECOLLEGE) {
        this.FEATURECOLLEGE = FEATURECOLLEGE;
    }

    public String getFEATURECOLLEGE_FEATURECOLLEGE_ANYNAME() {
        return FEATURECOLLEGE_FEATURECOLLEGE_ANYNAME;
    }

    public void setFEATURECOLLEGE_FEATURECOLLEGE_ANYNAME(String FEATURECOLLEGE_FEATURECOLLEGE_ANYNAME) {
        this.FEATURECOLLEGE_FEATURECOLLEGE_ANYNAME = FEATURECOLLEGE_FEATURECOLLEGE_ANYNAME;
    }

    public String getFEATURECOLLEGE_ANYNAME() {
        return FEATURECOLLEGE_ANYNAME;
    }

    public void setFEATURECOLLEGE_ANYNAME(String FEATURECOLLEGE_ANYNAME) {
        this.FEATURECOLLEGE_ANYNAME = FEATURECOLLEGE_ANYNAME;
    }

    public String getCOLLEGETYPE() {
        return COLLEGETYPE;
    }

    public void setCOLLEGETYPE(String COLLEGETYPE) {
        this.COLLEGETYPE = COLLEGETYPE;
    }

    public String getFPD() {
        return FPD;
    }

    public void setFPD(String FPD) {
        this.FPD = FPD;
    }
}
