package po;

import annotation.FieldOrder;

//核心资源数据
public class WLawsRegulationsPO {
    @FieldOrder(order = 1)
    private String 	F_ID;
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	TITLE;
    @FieldOrder(order = 5)
    private String 	ABSTRACT;
    @FieldOrder(order = 6)
    private String 	KEYWORD;
    @FieldOrder(order = 7)
    private String 	ORG;
    @FieldOrder(order = 8)
    private String 	ORG_ORG_ANYNAME;
    @FieldOrder(order = 9)
    private String 	ORG_ANYNAME;
    @FieldOrder(order = 10)
    private String 	AUTHOR;
    @FieldOrder(order = 11)
    private String 	CID;
    @FieldOrder(order = 12)
    private String 	DATE;
    @FieldOrder(order = 13)
    private String 	YEAR;
    @FieldOrder(order = 14)
    private String 	RID;
    @FieldOrder(order = 15)
    private String 	TI;
    @FieldOrder(order = 16)
    private String 	TIE;
    @FieldOrder(order = 17)
    private String 	PNO;
    @FieldOrder(order = 18)
    private String 	ORGID;
    @FieldOrder(order = 19)
    private String 	ORGC;
    @FieldOrder(order = 20)
    private String 	CODE;
    @FieldOrder(order = 21)
    private String 	VALLEVL;
    @FieldOrder(order = 22)
    private String 	VALCODE;
    @FieldOrder(order = 23)
    private String 	AGING;
    @FieldOrder(order = 24)
    private String 	APPRDATE;
    @FieldOrder(order = 25)
    private String 	SIGNDATE;
    @FieldOrder(order = 26)
    private String 	YAPPRDATE;
    @FieldOrder(order = 27)
    private String 	YSIGNDATE;
    @FieldOrder(order = 28)
    private String 	IMPDATE;
    @FieldOrder(order = 29)
    private String 	EXPDATE;
    @FieldOrder(order = 30)
    private String 	CFA;
    @FieldOrder(order = 31)
    private String 	APPDATE;
    @FieldOrder(order = 32)
    private String 	MEDDATE;
    @FieldOrder(order = 33)
    private String 	CONCLS;
    @FieldOrder(order = 34)
    private String 	CONCODE;
    @FieldOrder(order = 35)
    private String 	URL;
    @FieldOrder(order = 36)
    private String 	FT;
    @FieldOrder(order = 37)
    private String 	RELINK;
    @FieldOrder(order = 38)
    private String 	HISLINK;
    @FieldOrder(order = 39)
    private String 	DBID;
    @FieldOrder(order = 40)
    private String 	PRODDATE;
    @FieldOrder(order = 41)
    private String 	INDUSTRY;
    @FieldOrder(order = 42)
    private String 	IID;
    @FieldOrder(order = 43)
    private String 	DID;
    @FieldOrder(order = 44)
    private String 	ErrCode;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getRID() {
        return RID;
    }

    public void setRID(String RID) {
        this.RID = RID;
    }

    public String getTI() {
        return TI;
    }

    public void setTI(String TI) {
        this.TI = TI;
    }

    public String getTIE() {
        return TIE;
    }

    public void setTIE(String TIE) {
        this.TIE = TIE;
    }

    public String getPNO() {
        return PNO;
    }

    public void setPNO(String PNO) {
        this.PNO = PNO;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getORGC() {
        return ORGC;
    }

    public void setORGC(String ORGC) {
        this.ORGC = ORGC;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getVALLEVL() {
        return VALLEVL;
    }

    public void setVALLEVL(String VALLEVL) {
        this.VALLEVL = VALLEVL;
    }

    public String getVALCODE() {
        return VALCODE;
    }

    public void setVALCODE(String VALCODE) {
        this.VALCODE = VALCODE;
    }

    public String getAGING() {
        return AGING;
    }

    public void setAGING(String AGING) {
        this.AGING = AGING;
    }

    public String getAPPRDATE() {
        return APPRDATE;
    }

    public void setAPPRDATE(String APPRDATE) {
        this.APPRDATE = APPRDATE;
    }

    public String getSIGNDATE() {
        return SIGNDATE;
    }

    public void setSIGNDATE(String SIGNDATE) {
        this.SIGNDATE = SIGNDATE;
    }

    public String getYAPPRDATE() {
        return YAPPRDATE;
    }

    public void setYAPPRDATE(String YAPPRDATE) {
        this.YAPPRDATE = YAPPRDATE;
    }

    public String getYSIGNDATE() {
        return YSIGNDATE;
    }

    public void setYSIGNDATE(String YSIGNDATE) {
        this.YSIGNDATE = YSIGNDATE;
    }

    public String getIMPDATE() {
        return IMPDATE;
    }

    public void setIMPDATE(String IMPDATE) {
        this.IMPDATE = IMPDATE;
    }

    public String getEXPDATE() {
        return EXPDATE;
    }

    public void setEXPDATE(String EXPDATE) {
        this.EXPDATE = EXPDATE;
    }

    public String getCFA() {
        return CFA;
    }

    public void setCFA(String CFA) {
        this.CFA = CFA;
    }

    public String getAPPDATE() {
        return APPDATE;
    }

    public void setAPPDATE(String APPDATE) {
        this.APPDATE = APPDATE;
    }

    public String getMEDDATE() {
        return MEDDATE;
    }

    public void setMEDDATE(String MEDDATE) {
        this.MEDDATE = MEDDATE;
    }

    public String getCONCLS() {
        return CONCLS;
    }

    public void setCONCLS(String CONCLS) {
        this.CONCLS = CONCLS;
    }

    public String getCONCODE() {
        return CONCODE;
    }

    public void setCONCODE(String CONCODE) {
        this.CONCODE = CONCODE;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getFT() {
        return FT;
    }

    public void setFT(String FT) {
        this.FT = FT;
    }

    public String getRELINK() {
        return RELINK;
    }

    public void setRELINK(String RELINK) {
        this.RELINK = RELINK;
    }

    public String getHISLINK() {
        return HISLINK;
    }

    public void setHISLINK(String HISLINK) {
        this.HISLINK = HISLINK;
    }

    public String getDBID() {
        return DBID;
    }

    public void setDBID(String DBID) {
        this.DBID = DBID;
    }

    public String getPRODDATE() {
        return PRODDATE;
    }

    public void setPRODDATE(String PRODDATE) {
        this.PRODDATE = PRODDATE;
    }

    public String getINDUSTRY() {
        return INDUSTRY;
    }

    public void setINDUSTRY(String INDUSTRY) {
        this.INDUSTRY = INDUSTRY;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }
}
