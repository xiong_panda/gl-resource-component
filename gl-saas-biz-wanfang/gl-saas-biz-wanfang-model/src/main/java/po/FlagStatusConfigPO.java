package po;


import java.util.Date;


public class FlagStatusConfigPO {
    private String id;
    private Integer newFlag;
    private Integer oldFlag;
    private Integer wangfangFlag;
    private Integer dongfangFlag;
    private Integer ningboFlag;
    private Integer guolongFlag;
    private Date createAt;
    private Date updateAt;

    public Integer getWangfangFlag() {
        return wangfangFlag;
    }

    public void setWangfangFlag(Integer wangfangFlag) {
        this.wangfangFlag = wangfangFlag;
    }

    public Integer getDongfangFlag() {
        return dongfangFlag;
    }

    public void setDongfangFlag(Integer dongfangFlag) {
        this.dongfangFlag = dongfangFlag;
    }

    public Integer getNingboFlag() {
        return ningboFlag;
    }

    public void setNingboFlag(Integer ningboFlag) {
        this.ningboFlag = ningboFlag;
    }

    public Integer getGuolongFlag() {
        return guolongFlag;
    }

    public void setGuolongFlag(Integer guolongFlag) {
        this.guolongFlag = guolongFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(Integer newFlag) {
        this.newFlag = newFlag;
    }

    public Integer getOldFlag() {
        return oldFlag;
    }

    public void setOldFlag(Integer oldFlag) {
        this.oldFlag = oldFlag;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
