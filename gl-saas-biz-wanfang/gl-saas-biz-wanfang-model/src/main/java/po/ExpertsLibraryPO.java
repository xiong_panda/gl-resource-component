package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//标记构件PO
public class ExpertsLibraryPO {
    @FieldOrder(order = 1)
    private String 	OrderID;
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	Name;
    @FieldOrder(order = 5)
    private String 	Sex;
    @FieldOrder(order = 6)
    private String 	Age;
    @FieldOrder(order = 7)
    private String 	Birth_Date;
    @FieldOrder(order = 8)
    private String 	Birth_Address;
    @FieldOrder(order = 9)
    private String 	National;
    @FieldOrder(order = 10)
    private String 	Other_Unit;
    @FieldOrder(order = 11)
    private String 	Other_Unit2;
    @FieldOrder(order = 12)
    private String 	Unit;
    @FieldOrder(order = 13)
    private String 	Other_Unit3;
    @FieldOrder(order = 14)
    private String 	Education_Background;
    @FieldOrder(order = 15)
    private String 	Foreign_Languages;
    @FieldOrder(order = 16)
    private String 	Job_Resume;
    @FieldOrder(order = 17)
    private String 	Co_Employment;
    @FieldOrder(order = 18)
    private String 	Job_Position;
    @FieldOrder(order = 19)
    private String 	Experts_Honor;
    @FieldOrder(order = 20)
    private String 	Technical_Titles;
    @FieldOrder(order = 21)
    private String 	Address;
    @FieldOrder(order = 22)
    private String 	Postal_Code;
    @FieldOrder(order = 23)
    private String 	Zone_Bit;
    @FieldOrder(order = 24)
    private String 	Province;
    @FieldOrder(order = 25)
    private String 	City;
    @FieldOrder(order = 26)
    private String 	County;
    @FieldOrder(order = 27)
    private String 	Administrative_Code;
    @FieldOrder(order = 28)
    private String 	Telphone;
    @FieldOrder(order = 29)
    private String 	Fax;
    @FieldOrder(order = 30)
    private String 	Email;
    @FieldOrder(order = 31)
    private String 	Professional_Research;
    @FieldOrder(order = 32)
    private String 	Professional_Research2;
    @FieldOrder(order = 33)
    private String 	Academician;
    @FieldOrder(order = 34)
    private String 	Academician2;
    @FieldOrder(order = 35)
    private String 	Awards;
    @FieldOrder(order = 36)
    private String 	Awardt;
    @FieldOrder(order = 37)
    private String 	Foreign;
    @FieldOrder(order = 38)
    private String 	Exptype;
    @FieldOrder(order = 39)
    private String 	Byind;
    @FieldOrder(order = 40)
    private String 	Discipline_Class;
    @FieldOrder(order = 41)
    private String 	Discipline_Class2;
    @FieldOrder(order = 42)
    private String 	Discipline_Class_Code;
    @FieldOrder(order = 43)
    private String 	Cls;
    @FieldOrder(order = 44)
    private String 	Cls2;
    @FieldOrder(order = 45)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getBirth_Date() {
        return Birth_Date;
    }

    public void setBirth_Date(String birth_Date) {
        Birth_Date = birth_Date;
    }

    public String getBirth_Address() {
        return Birth_Address;
    }

    public void setBirth_Address(String birth_Address) {
        Birth_Address = birth_Address;
    }

    public String getNational() {
        return National;
    }

    public void setNational(String national) {
        National = national;
    }

    public String getOther_Unit() {
        return Other_Unit;
    }

    public void setOther_Unit(String other_Unit) {
        Other_Unit = other_Unit;
    }

    public String getOther_Unit2() {
        return Other_Unit2;
    }

    public void setOther_Unit2(String other_Unit2) {
        Other_Unit2 = other_Unit2;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getOther_Unit3() {
        return Other_Unit3;
    }

    public void setOther_Unit3(String other_Unit3) {
        Other_Unit3 = other_Unit3;
    }

    public String getEducation_Background() {
        return Education_Background;
    }

    public void setEducation_Background(String education_Background) {
        Education_Background = education_Background;
    }

    public String getForeign_Languages() {
        return Foreign_Languages;
    }

    public void setForeign_Languages(String foreign_Languages) {
        Foreign_Languages = foreign_Languages;
    }

    public String getJob_Resume() {
        return Job_Resume;
    }

    public void setJob_Resume(String job_Resume) {
        Job_Resume = job_Resume;
    }

    public String getCo_Employment() {
        return Co_Employment;
    }

    public void setCo_Employment(String co_Employment) {
        Co_Employment = co_Employment;
    }

    public String getJob_Position() {
        return Job_Position;
    }

    public void setJob_Position(String job_Position) {
        Job_Position = job_Position;
    }

    public String getExperts_Honor() {
        return Experts_Honor;
    }

    public void setExperts_Honor(String experts_Honor) {
        Experts_Honor = experts_Honor;
    }

    public String getTechnical_Titles() {
        return Technical_Titles;
    }

    public void setTechnical_Titles(String technical_Titles) {
        Technical_Titles = technical_Titles;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPostal_Code() {
        return Postal_Code;
    }

    public void setPostal_Code(String postal_Code) {
        Postal_Code = postal_Code;
    }

    public String getZone_Bit() {
        return Zone_Bit;
    }

    public void setZone_Bit(String zone_Bit) {
        Zone_Bit = zone_Bit;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getAdministrative_Code() {
        return Administrative_Code;
    }

    public void setAdministrative_Code(String administrative_Code) {
        Administrative_Code = administrative_Code;
    }

    public String getTelphone() {
        return Telphone;
    }

    public void setTelphone(String telphone) {
        Telphone = telphone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getProfessional_Research() {
        return Professional_Research;
    }

    public void setProfessional_Research(String professional_Research) {
        Professional_Research = professional_Research;
    }

    public String getProfessional_Research2() {
        return Professional_Research2;
    }

    public void setProfessional_Research2(String professional_Research2) {
        Professional_Research2 = professional_Research2;
    }

    public String getAcademician() {
        return Academician;
    }

    public void setAcademician(String academician) {
        Academician = academician;
    }

    public String getAcademician2() {
        return Academician2;
    }

    public void setAcademician2(String academician2) {
        Academician2 = academician2;
    }

    public String getAwards() {
        return Awards;
    }

    public void setAwards(String awards) {
        Awards = awards;
    }

    public String getAwardt() {
        return Awardt;
    }

    public void setAwardt(String awardt) {
        Awardt = awardt;
    }

    public String getForeign() {
        return Foreign;
    }

    public void setForeign(String foreign) {
        Foreign = foreign;
    }

    public String getExptype() {
        return Exptype;
    }

    public void setExptype(String exptype) {
        Exptype = exptype;
    }

    public String getByind() {
        return Byind;
    }

    public void setByind(String byind) {
        Byind = byind;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getDiscipline_Class2() {
        return Discipline_Class2;
    }

    public void setDiscipline_Class2(String discipline_Class2) {
        Discipline_Class2 = discipline_Class2;
    }

    public String getDiscipline_Class_Code() {
        return Discipline_Class_Code;
    }

    public void setDiscipline_Class_Code(String discipline_Class_Code) {
        Discipline_Class_Code = discipline_Class_Code;
    }

    public String getCls() {
        return Cls;
    }

    public void setCls(String cls) {
        Cls = cls;
    }

    public String getCls2() {
        return Cls2;
    }

    public void setCls2(String cls2) {
        Cls2 = cls2;
    }
}
