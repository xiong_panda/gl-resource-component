package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据
public class TechnologyAchievementsPO {
    @FieldOrder(order = 1)
    private String 	FullText;
    @FieldOrder(order = 2)
    private String 	Full_Name;
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 3)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 4)
    private String 	OrderID;
    @FieldOrder(order = 5)
    private String 	Abstract;
    @FieldOrder(order = 6)
    private String 	Authorized_Department;
    @FieldOrder(order = 7)
    private String 	PatentApplication_ID;
    @FieldOrder(order = 8)
    private String 	Persons_Involved;
    @FieldOrder(order = 9)
    private String 	Fau;
    @FieldOrder(order = 10)
    private String 	Persons_Involved2;
    @FieldOrder(order = 11)
    private String 	Awards;
    @FieldOrder(order = 12)
    private String 	Awards_Name;
    @FieldOrder(order = 13)
    private String 	Awards_Anyname;
    @FieldOrder(order = 14)
    private String 	Application_Industry_Name;
    @FieldOrder(order = 15)
    private String 	Cls_Code;
    @FieldOrder(order = 16)
    private String 	Compby;
    @FieldOrder(order = 17)
    private String 	CompID;
    @FieldOrder(order = 18)
    private String 	Contact_Address;
    @FieldOrder(order = 19)
    private String 	Contact_Unit_Name;
    @FieldOrder(order = 20)
    private String 	Contact;
    @FieldOrder(order = 21)
    private String 	Contact_Phone_Number;
    @FieldOrder(order = 22)
    private String 	Provinces_City;
    @FieldOrder(order = 23)
    private String 	DataReleased_Date;
    @FieldOrder(order = 24)
    private String 	Work_Start;
    @FieldOrder(order = 25)
    private String 	Work_End;
    @FieldOrder(order = 26)
    private String 	Declare_Date;
    @FieldOrder(order = 27)
    private String 	Discipline_Class;
    @FieldOrder(order = 28)
    private String 	Declare_Unit_Name;
    @FieldOrder(order = 29)
    private String 	ResultsPublished_Date;
    @FieldOrder(order = 30)
    private String 	Dura;
    @FieldOrder(order = 31)
    private String 	Foreign_Exchange_Earning;
    @FieldOrder(order = 32)
    private String 	Mail;
    @FieldOrder(order = 33)
    private String 	Fax;
    @FieldOrder(order = 34)
    private String 	Results_Type;
    @FieldOrder(order = 35)
    private String 	Results_Level;
    @FieldOrder(order = 36)
    private String 	Complete_Unit;
    @FieldOrder(order = 37)
    private String 	Investment_Amount;
    @FieldOrder(order = 38)
    private String 	IDentification_Date;
    @FieldOrder(order = 39)
    private String 	Application_Industry_Code;
    @FieldOrder(order = 40)
    private String 	Investment_Instructions;
    @FieldOrder(order = 41)
    private String 	Investment_Annotation;
    @FieldOrder(order = 42)
    private String 	Results_Summary;
    @FieldOrder(order = 43)
    private String 	Keyword;
    @FieldOrder(order = 44)
    private String 	Note;
    @FieldOrder(order = 45)
    private String 	Included;
    @FieldOrder(order = 46)
    private String 	ORG_Name;
    @FieldOrder(order = 47)
    private String 	ORG_Name_Search;
    @FieldOrder(order = 48)
    private String 	Specification_Unit_Name;
    @FieldOrder(order = 49)
    private String 	CompletionUnit;
    @FieldOrder(order = 50)
    private String 	ORG_Type;
    @FieldOrder(order = 51)
    private String 	Promotion_Info;
    @FieldOrder(order = 52)
    private String 	Promotion_Way;
    @FieldOrder(order = 53)
    private String 	Promotion_Scope;
    @FieldOrder(order = 54)
    private String 	Promotion_Tracking;
    @FieldOrder(order = 55)
    private String 	Output_Value;
    @FieldOrder(order = 56)
    private String 	Promotion_Necessity_Promotion_Forecast;
    @FieldOrder(order = 57)
    private String 	Project_Name;
    @FieldOrder(order = 58)
    private String 	Achievement_ConfIDentiality_Level;
    @FieldOrder(order = 59)
    private String 	Achievement_Type;
    @FieldOrder(order = 60)
    private String 	Patent_Number;
    @FieldOrder(order = 61)
    private String 	Publication_Name_Page_Number;
    @FieldOrder(order = 62)
    private String 	Patent_Authorization_Number;
    @FieldOrder(order = 63)
    private String 	Registration_Date;
    @FieldOrder(order = 64)
    private String 	Record_Date;
    @FieldOrder(order = 65)
    private String 	Recommended_Date;
    @FieldOrder(order = 66)
    private String 	Limit_To_Use;
    @FieldOrder(order = 67)
    private String 	Registration_No;
    @FieldOrder(order = 68)
    private String 	Record_Status;
    @FieldOrder(order = 69)
    private String 	Registration_Department;
    @FieldOrder(order = 70)
    private String 	Recommend_Department;
    @FieldOrder(order = 71)
    private String 	Release_Nnit;
    @FieldOrder(order = 72)
    private String 	Specification_Unit_Name2;
    @FieldOrder(order = 73)
    private String 	Source_Of_Info;
    @FieldOrder(order = 74)
    private String 	Save_Money;
    @FieldOrder(order = 75)
    private String 	Cls_level2;
    @FieldOrder(order = 76)
    private String 	Cls_level3;
    @FieldOrder(order = 77)
    private String 	Transfer_Conditions;
    @FieldOrder(order = 78)
    private String 	Transfer_Fee;
    @FieldOrder(order = 79)
    private String 	Transfer_Content;
    @FieldOrder(order = 80)
    private String 	Chinese_Name;
    @FieldOrder(order = 81)
    private String 	Transfer_Annotation;
    @FieldOrder(order = 82)
    private String 	Ischarge;
    @FieldOrder(order = 83)
    private String 	Registration_Department_Code;
    @FieldOrder(order = 84)
    private String 	Recommended_Department_Code;
    @FieldOrder(order = 85)
    private String 	Transfer_Way;
    @FieldOrder(order = 86)
    private String 	Transfer_Scope;
    @FieldOrder(order = 87)
    private String 	Tax;
    @FieldOrder(order = 88)
    private String 	Cls_Top;
    @FieldOrder(order = 89)
    private String 	Released_Date;
    @FieldOrder(order = 90)
    private String 	Cls_Code2;
    @FieldOrder(order = 91)
    private String 	Post_Code;
    @FieldOrder(order = 92)
    private String 	Other_ID;
    @FieldOrder(order = 93)
    private String 	KeyWord2;
    @FieldOrder(order = 94)
    private String 	ORG_ID;
    @FieldOrder(order = 95)
    private String 	ORG_Hierarchy_ID;
    @FieldOrder(order = 96)
    private String 	ORG_Province;
    @FieldOrder(order = 97)
    private String 	ORG_City;
    @FieldOrder(order = 98)
    private String 	ORG_Final;
    @FieldOrder(order = 99)
    private String 	ORG_Final2;
    @FieldOrder(order = 100)
    private String 	ORG_Frist_Final_ID;
    @FieldOrder(order = 101)
    private String 	ORG_Frist_Hierarchy_ID;
    @FieldOrder(order = 102)
    private String 	ORG_Frist_Province;
    @FieldOrder(order = 103)
    private String 	ORG_Frist_Province2;
    @FieldOrder(order = 104)
    private String 	ORG_Frist_Final_City;
    @FieldOrder(order = 105)
    private String 	ORG_Frist_City;
    @FieldOrder(order = 106)
    private String 	ORG_Frist_Final_Type;
    @FieldOrder(order = 107)
    private String 	ORG_Frist_Type;
    @FieldOrder(order = 108)
    private String 	Aw_Short;
    @FieldOrder(order = 109)
    private String 	Aw_Level;
    @FieldOrder(order = 110)
    private String 	Quarter;
    @FieldOrder(order = 111)
    private String 	Half_Year;
    @FieldOrder(order = 112)
    private String 	Literature_Weight;
    @FieldOrder(order = 113)
    private String 	Error_Code;
    @FieldOrder(order = 114)
    private String 	Author;
    @FieldOrder(order = 115)
    private String 	Author_Info_Name;
    @FieldOrder(order = 116)
    private String 	Author_Info_Order;
    @FieldOrder(order = 117)
    private String 	Author_Info_Unit;
    @FieldOrder(order = 118)
    private String 	Author_Info_Unit_ORG_Level1;
    @FieldOrder(order = 119)
    private String 	Author_Info_Unit_ORG_Level2;
    @FieldOrder(order = 120)
    private String 	Author_Info_Unit_Type;
    @FieldOrder(order = 121)
    private String 	Author_Info_Unit_Province;
    @FieldOrder(order = 122)
    private String 	Author_Info_Unit_City;
    @FieldOrder(order = 123)
    private String 	Author_Info_Unit_County;
    @FieldOrder(order = 124)
    private String 	Author_Info_ID;
    @FieldOrder(order = 125)
    private String 	Author_Info_Unit_ID;
    @FieldOrder(order = 126)
    private String 	ORG_Info;
    @FieldOrder(order = 127)
    private String 	ORG_Info_Name;
    @FieldOrder(order = 128)
    private String 	ORG_Info_Order;
    @FieldOrder(order = 129)
    private String 	ORG_Info_Type;
    @FieldOrder(order = 130)
    private String 	ORG_Info_Province;
    @FieldOrder(order = 131)
    private String 	ORG_Info_City;
    @FieldOrder(order = 132)
    private String 	ORG_Info_County;
    @FieldOrder(order = 133)
    private String 	ORG_Info_Hierarchy;
    @FieldOrder(order = 134)
    private String 	ORG_Info_Level1;
    @FieldOrder(order = 135)
    private String 	ORG_Info_Level2;
    @FieldOrder(order = 136)
    private String 	ORG_Info_Level3;
    @FieldOrder(order = 137)
    private String 	ORG_Info_Level4;
    @FieldOrder(order = 138)
    private String 	ORG_Info_Level5;
    @FieldOrder(order = 139)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getFull_Name() {
        return Full_Name;
    }

    public void setFull_Name(String full_Name) {
        Full_Name = full_Name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getAuthorized_Department() {
        return Authorized_Department;
    }

    public void setAuthorized_Department(String authorized_Department) {
        Authorized_Department = authorized_Department;
    }

    public String getPatentApplication_ID() {
        return PatentApplication_ID;
    }

    public void setPatentApplication_ID(String patentApplication_ID) {
        PatentApplication_ID = patentApplication_ID;
    }

    public String getPersons_Involved() {
        return Persons_Involved;
    }

    public void setPersons_Involved(String persons_Involved) {
        Persons_Involved = persons_Involved;
    }

    public String getFau() {
        return Fau;
    }

    public void setFau(String fau) {
        Fau = fau;
    }

    public String getPersons_Involved2() {
        return Persons_Involved2;
    }

    public void setPersons_Involved2(String persons_Involved2) {
        Persons_Involved2 = persons_Involved2;
    }

    public String getAwards() {
        return Awards;
    }

    public void setAwards(String awards) {
        Awards = awards;
    }

    public String getAwards_Name() {
        return Awards_Name;
    }

    public void setAwards_Name(String awards_Name) {
        Awards_Name = awards_Name;
    }

    public String getAwards_Anyname() {
        return Awards_Anyname;
    }

    public void setAwards_Anyname(String awards_Anyname) {
        Awards_Anyname = awards_Anyname;
    }

    public String getApplication_Industry_Name() {
        return Application_Industry_Name;
    }

    public void setApplication_Industry_Name(String application_Industry_Name) {
        Application_Industry_Name = application_Industry_Name;
    }

    public String getCls_Code() {
        return Cls_Code;
    }

    public void setCls_Code(String cls_Code) {
        Cls_Code = cls_Code;
    }

    public String getCompby() {
        return Compby;
    }

    public void setCompby(String compby) {
        Compby = compby;
    }

    public String getCompID() {
        return CompID;
    }

    public void setCompID(String compID) {
        CompID = compID;
    }

    public String getContact_Address() {
        return Contact_Address;
    }

    public void setContact_Address(String contact_Address) {
        Contact_Address = contact_Address;
    }

    public String getContact_Unit_Name() {
        return Contact_Unit_Name;
    }

    public void setContact_Unit_Name(String contact_Unit_Name) {
        Contact_Unit_Name = contact_Unit_Name;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getContact_Phone_Number() {
        return Contact_Phone_Number;
    }

    public void setContact_Phone_Number(String contact_Phone_Number) {
        Contact_Phone_Number = contact_Phone_Number;
    }

    public String getProvinces_City() {
        return Provinces_City;
    }

    public void setProvinces_City(String provinces_City) {
        Provinces_City = provinces_City;
    }

    public String getDataReleased_Date() {
        return DataReleased_Date;
    }

    public void setDataReleased_Date(String dataReleased_Date) {
        DataReleased_Date = dataReleased_Date;
    }

    public String getWork_Start() {
        return Work_Start;
    }

    public void setWork_Start(String work_Start) {
        Work_Start = work_Start;
    }

    public String getWork_End() {
        return Work_End;
    }

    public void setWork_End(String work_End) {
        Work_End = work_End;
    }

    public String getDeclare_Date() {
        return Declare_Date;
    }

    public void setDeclare_Date(String declare_Date) {
        Declare_Date = declare_Date;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getDeclare_Unit_Name() {
        return Declare_Unit_Name;
    }

    public void setDeclare_Unit_Name(String declare_Unit_Name) {
        Declare_Unit_Name = declare_Unit_Name;
    }

    public String getResultsPublished_Date() {
        return ResultsPublished_Date;
    }

    public void setResultsPublished_Date(String resultsPublished_Date) {
        ResultsPublished_Date = resultsPublished_Date;
    }

    public String getDura() {
        return Dura;
    }

    public void setDura(String dura) {
        Dura = dura;
    }

    public String getForeign_Exchange_Earning() {
        return Foreign_Exchange_Earning;
    }

    public void setForeign_Exchange_Earning(String foreign_Exchange_Earning) {
        Foreign_Exchange_Earning = foreign_Exchange_Earning;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getResults_Type() {
        return Results_Type;
    }

    public void setResults_Type(String results_Type) {
        Results_Type = results_Type;
    }

    public String getResults_Level() {
        return Results_Level;
    }

    public void setResults_Level(String results_Level) {
        Results_Level = results_Level;
    }

    public String getComplete_Unit() {
        return Complete_Unit;
    }

    public void setComplete_Unit(String complete_Unit) {
        Complete_Unit = complete_Unit;
    }

    public String getInvestment_Amount() {
        return Investment_Amount;
    }

    public void setInvestment_Amount(String investment_Amount) {
        Investment_Amount = investment_Amount;
    }

    public String getIDentification_Date() {
        return IDentification_Date;
    }

    public void setIDentification_Date(String IDentification_Date) {
        this.IDentification_Date = IDentification_Date;
    }

    public String getApplication_Industry_Code() {
        return Application_Industry_Code;
    }

    public void setApplication_Industry_Code(String application_Industry_Code) {
        Application_Industry_Code = application_Industry_Code;
    }

    public String getInvestment_Instructions() {
        return Investment_Instructions;
    }

    public void setInvestment_Instructions(String investment_Instructions) {
        Investment_Instructions = investment_Instructions;
    }

    public String getInvestment_Annotation() {
        return Investment_Annotation;
    }

    public void setInvestment_Annotation(String investment_Annotation) {
        Investment_Annotation = investment_Annotation;
    }

    public String getResults_Summary() {
        return Results_Summary;
    }

    public void setResults_Summary(String results_Summary) {
        Results_Summary = results_Summary;
    }

    public String getKeyword() {
        return Keyword;
    }

    public void setKeyword(String keyword) {
        Keyword = keyword;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getIncluded() {
        return Included;
    }

    public void setIncluded(String included) {
        Included = included;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getSpecification_Unit_Name() {
        return Specification_Unit_Name;
    }

    public void setSpecification_Unit_Name(String specification_Unit_Name) {
        Specification_Unit_Name = specification_Unit_Name;
    }

    public String getCompletionUnit() {
        return CompletionUnit;
    }

    public void setCompletionUnit(String completionUnit) {
        CompletionUnit = completionUnit;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getPromotion_Info() {
        return Promotion_Info;
    }

    public void setPromotion_Info(String promotion_Info) {
        Promotion_Info = promotion_Info;
    }

    public String getPromotion_Way() {
        return Promotion_Way;
    }

    public void setPromotion_Way(String promotion_Way) {
        Promotion_Way = promotion_Way;
    }

    public String getPromotion_Scope() {
        return Promotion_Scope;
    }

    public void setPromotion_Scope(String promotion_Scope) {
        Promotion_Scope = promotion_Scope;
    }

    public String getPromotion_Tracking() {
        return Promotion_Tracking;
    }

    public void setPromotion_Tracking(String promotion_Tracking) {
        Promotion_Tracking = promotion_Tracking;
    }

    public String getOutput_Value() {
        return Output_Value;
    }

    public void setOutput_Value(String output_Value) {
        Output_Value = output_Value;
    }

    public String getPromotion_Necessity_Promotion_Forecast() {
        return Promotion_Necessity_Promotion_Forecast;
    }

    public void setPromotion_Necessity_Promotion_Forecast(String promotion_Necessity_Promotion_Forecast) {
        Promotion_Necessity_Promotion_Forecast = promotion_Necessity_Promotion_Forecast;
    }

    public String getProject_Name() {
        return Project_Name;
    }

    public void setProject_Name(String project_Name) {
        Project_Name = project_Name;
    }

    public String getAchievement_ConfIDentiality_Level() {
        return Achievement_ConfIDentiality_Level;
    }

    public void setAchievement_ConfIDentiality_Level(String achievement_ConfIDentiality_Level) {
        Achievement_ConfIDentiality_Level = achievement_ConfIDentiality_Level;
    }

    public String getAchievement_Type() {
        return Achievement_Type;
    }

    public void setAchievement_Type(String achievement_Type) {
        Achievement_Type = achievement_Type;
    }

    public String getPatent_Number() {
        return Patent_Number;
    }

    public void setPatent_Number(String patent_Number) {
        Patent_Number = patent_Number;
    }

    public String getPublication_Name_Page_Number() {
        return Publication_Name_Page_Number;
    }

    public void setPublication_Name_Page_Number(String publication_Name_Page_Number) {
        Publication_Name_Page_Number = publication_Name_Page_Number;
    }

    public String getPatent_Authorization_Number() {
        return Patent_Authorization_Number;
    }

    public void setPatent_Authorization_Number(String patent_Authorization_Number) {
        Patent_Authorization_Number = patent_Authorization_Number;
    }

    public String getRegistration_Date() {
        return Registration_Date;
    }

    public void setRegistration_Date(String registration_Date) {
        Registration_Date = registration_Date;
    }

    public String getRecord_Date() {
        return Record_Date;
    }

    public void setRecord_Date(String record_Date) {
        Record_Date = record_Date;
    }

    public String getRecommended_Date() {
        return Recommended_Date;
    }

    public void setRecommended_Date(String recommended_Date) {
        Recommended_Date = recommended_Date;
    }

    public String getLimit_To_Use() {
        return Limit_To_Use;
    }

    public void setLimit_To_Use(String limit_To_Use) {
        Limit_To_Use = limit_To_Use;
    }

    public String getRegistration_No() {
        return Registration_No;
    }

    public void setRegistration_No(String registration_No) {
        Registration_No = registration_No;
    }

    public String getRecord_Status() {
        return Record_Status;
    }

    public void setRecord_Status(String record_Status) {
        Record_Status = record_Status;
    }

    public String getRegistration_Department() {
        return Registration_Department;
    }

    public void setRegistration_Department(String registration_Department) {
        Registration_Department = registration_Department;
    }

    public String getRecommend_Department() {
        return Recommend_Department;
    }

    public void setRecommend_Department(String recommend_Department) {
        Recommend_Department = recommend_Department;
    }

    public String getRelease_Nnit() {
        return Release_Nnit;
    }

    public void setRelease_Nnit(String release_Nnit) {
        Release_Nnit = release_Nnit;
    }

    public String getSpecification_Unit_Name2() {
        return Specification_Unit_Name2;
    }

    public void setSpecification_Unit_Name2(String specification_Unit_Name2) {
        Specification_Unit_Name2 = specification_Unit_Name2;
    }

    public String getSource_Of_Info() {
        return Source_Of_Info;
    }

    public void setSource_Of_Info(String source_Of_Info) {
        Source_Of_Info = source_Of_Info;
    }

    public String getSave_Money() {
        return Save_Money;
    }

    public void setSave_Money(String save_Money) {
        Save_Money = save_Money;
    }

    public String getCls_level2() {
        return Cls_level2;
    }

    public void setCls_level2(String cls_level2) {
        Cls_level2 = cls_level2;
    }

    public String getCls_level3() {
        return Cls_level3;
    }

    public void setCls_level3(String cls_level3) {
        Cls_level3 = cls_level3;
    }

    public String getTransfer_Conditions() {
        return Transfer_Conditions;
    }

    public void setTransfer_Conditions(String transfer_Conditions) {
        Transfer_Conditions = transfer_Conditions;
    }

    public String getTransfer_Fee() {
        return Transfer_Fee;
    }

    public void setTransfer_Fee(String transfer_Fee) {
        Transfer_Fee = transfer_Fee;
    }

    public String getTransfer_Content() {
        return Transfer_Content;
    }

    public void setTransfer_Content(String transfer_Content) {
        Transfer_Content = transfer_Content;
    }

    public String getChinese_Name() {
        return Chinese_Name;
    }

    public void setChinese_Name(String chinese_Name) {
        Chinese_Name = chinese_Name;
    }

    public String getTransfer_Annotation() {
        return Transfer_Annotation;
    }

    public void setTransfer_Annotation(String transfer_Annotation) {
        Transfer_Annotation = transfer_Annotation;
    }

    public String getIscharge() {
        return Ischarge;
    }

    public void setIscharge(String ischarge) {
        Ischarge = ischarge;
    }

    public String getRegistration_Department_Code() {
        return Registration_Department_Code;
    }

    public void setRegistration_Department_Code(String registration_Department_Code) {
        Registration_Department_Code = registration_Department_Code;
    }

    public String getRecommended_Department_Code() {
        return Recommended_Department_Code;
    }

    public void setRecommended_Department_Code(String recommended_Department_Code) {
        Recommended_Department_Code = recommended_Department_Code;
    }

    public String getTransfer_Way() {
        return Transfer_Way;
    }

    public void setTransfer_Way(String transfer_Way) {
        Transfer_Way = transfer_Way;
    }

    public String getTransfer_Scope() {
        return Transfer_Scope;
    }

    public void setTransfer_Scope(String transfer_Scope) {
        Transfer_Scope = transfer_Scope;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getCls_Top() {
        return Cls_Top;
    }

    public void setCls_Top(String cls_Top) {
        Cls_Top = cls_Top;
    }

    public String getReleased_Date() {
        return Released_Date;
    }

    public void setReleased_Date(String released_Date) {
        Released_Date = released_Date;
    }

    public String getCls_Code2() {
        return Cls_Code2;
    }

    public void setCls_Code2(String cls_Code2) {
        Cls_Code2 = cls_Code2;
    }

    public String getPost_Code() {
        return Post_Code;
    }

    public void setPost_Code(String post_Code) {
        Post_Code = post_Code;
    }

    public String getOther_ID() {
        return Other_ID;
    }

    public void setOther_ID(String other_ID) {
        Other_ID = other_ID;
    }

    public String getKeyWord2() {
        return KeyWord2;
    }

    public void setKeyWord2(String keyWord2) {
        KeyWord2 = keyWord2;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getORG_Hierarchy_ID() {
        return ORG_Hierarchy_ID;
    }

    public void setORG_Hierarchy_ID(String ORG_Hierarchy_ID) {
        this.ORG_Hierarchy_ID = ORG_Hierarchy_ID;
    }

    public String getORG_Province() {
        return ORG_Province;
    }

    public void setORG_Province(String ORG_Province) {
        this.ORG_Province = ORG_Province;
    }

    public String getORG_City() {
        return ORG_City;
    }

    public void setORG_City(String ORG_City) {
        this.ORG_City = ORG_City;
    }

    public String getORG_Final() {
        return ORG_Final;
    }

    public void setORG_Final(String ORG_Final) {
        this.ORG_Final = ORG_Final;
    }

    public String getORG_Final2() {
        return ORG_Final2;
    }

    public void setORG_Final2(String ORG_Final2) {
        this.ORG_Final2 = ORG_Final2;
    }

    public String getORG_Frist_Final_ID() {
        return ORG_Frist_Final_ID;
    }

    public void setORG_Frist_Final_ID(String ORG_Frist_Final_ID) {
        this.ORG_Frist_Final_ID = ORG_Frist_Final_ID;
    }

    public String getORG_Frist_Hierarchy_ID() {
        return ORG_Frist_Hierarchy_ID;
    }

    public void setORG_Frist_Hierarchy_ID(String ORG_Frist_Hierarchy_ID) {
        this.ORG_Frist_Hierarchy_ID = ORG_Frist_Hierarchy_ID;
    }

    public String getORG_Frist_Province() {
        return ORG_Frist_Province;
    }

    public void setORG_Frist_Province(String ORG_Frist_Province) {
        this.ORG_Frist_Province = ORG_Frist_Province;
    }

    public String getORG_Frist_Province2() {
        return ORG_Frist_Province2;
    }

    public void setORG_Frist_Province2(String ORG_Frist_Province2) {
        this.ORG_Frist_Province2 = ORG_Frist_Province2;
    }

    public String getORG_Frist_Final_City() {
        return ORG_Frist_Final_City;
    }

    public void setORG_Frist_Final_City(String ORG_Frist_Final_City) {
        this.ORG_Frist_Final_City = ORG_Frist_Final_City;
    }

    public String getORG_Frist_City() {
        return ORG_Frist_City;
    }

    public void setORG_Frist_City(String ORG_Frist_City) {
        this.ORG_Frist_City = ORG_Frist_City;
    }

    public String getORG_Frist_Final_Type() {
        return ORG_Frist_Final_Type;
    }

    public void setORG_Frist_Final_Type(String ORG_Frist_Final_Type) {
        this.ORG_Frist_Final_Type = ORG_Frist_Final_Type;
    }

    public String getORG_Frist_Type() {
        return ORG_Frist_Type;
    }

    public void setORG_Frist_Type(String ORG_Frist_Type) {
        this.ORG_Frist_Type = ORG_Frist_Type;
    }

    public String getAw_Short() {
        return Aw_Short;
    }

    public void setAw_Short(String aw_Short) {
        Aw_Short = aw_Short;
    }

    public String getAw_Level() {
        return Aw_Level;
    }

    public void setAw_Level(String aw_Level) {
        Aw_Level = aw_Level;
    }

    public String getQuarter() {
        return Quarter;
    }

    public void setQuarter(String quarter) {
        Quarter = quarter;
    }

    public String getHalf_Year() {
        return Half_Year;
    }

    public void setHalf_Year(String half_Year) {
        Half_Year = half_Year;
    }

    public String getLiterature_Weight() {
        return Literature_Weight;
    }

    public void setLiterature_Weight(String literature_Weight) {
        Literature_Weight = literature_Weight;
    }

    public String getError_Code() {
        return Error_Code;
    }

    public void setError_Code(String error_Code) {
        Error_Code = error_Code;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getAuthor_Info_Name() {
        return Author_Info_Name;
    }

    public void setAuthor_Info_Name(String author_Info_Name) {
        Author_Info_Name = author_Info_Name;
    }

    public String getAuthor_Info_Order() {
        return Author_Info_Order;
    }

    public void setAuthor_Info_Order(String author_Info_Order) {
        Author_Info_Order = author_Info_Order;
    }

    public String getAuthor_Info_Unit() {
        return Author_Info_Unit;
    }

    public void setAuthor_Info_Unit(String author_Info_Unit) {
        Author_Info_Unit = author_Info_Unit;
    }

    public String getAuthor_Info_Unit_ORG_Level1() {
        return Author_Info_Unit_ORG_Level1;
    }

    public void setAuthor_Info_Unit_ORG_Level1(String author_Info_Unit_ORG_Level1) {
        Author_Info_Unit_ORG_Level1 = author_Info_Unit_ORG_Level1;
    }

    public String getAuthor_Info_Unit_ORG_Level2() {
        return Author_Info_Unit_ORG_Level2;
    }

    public void setAuthor_Info_Unit_ORG_Level2(String author_Info_Unit_ORG_Level2) {
        Author_Info_Unit_ORG_Level2 = author_Info_Unit_ORG_Level2;
    }

    public String getAuthor_Info_Unit_Type() {
        return Author_Info_Unit_Type;
    }

    public void setAuthor_Info_Unit_Type(String author_Info_Unit_Type) {
        Author_Info_Unit_Type = author_Info_Unit_Type;
    }

    public String getAuthor_Info_Unit_Province() {
        return Author_Info_Unit_Province;
    }

    public void setAuthor_Info_Unit_Province(String author_Info_Unit_Province) {
        Author_Info_Unit_Province = author_Info_Unit_Province;
    }

    public String getAuthor_Info_Unit_City() {
        return Author_Info_Unit_City;
    }

    public void setAuthor_Info_Unit_City(String author_Info_Unit_City) {
        Author_Info_Unit_City = author_Info_Unit_City;
    }

    public String getAuthor_Info_Unit_County() {
        return Author_Info_Unit_County;
    }

    public void setAuthor_Info_Unit_County(String author_Info_Unit_County) {
        Author_Info_Unit_County = author_Info_Unit_County;
    }

    public String getAuthor_Info_ID() {
        return Author_Info_ID;
    }

    public void setAuthor_Info_ID(String author_Info_ID) {
        Author_Info_ID = author_Info_ID;
    }

    public String getAuthor_Info_Unit_ID() {
        return Author_Info_Unit_ID;
    }

    public void setAuthor_Info_Unit_ID(String author_Info_Unit_ID) {
        Author_Info_Unit_ID = author_Info_Unit_ID;
    }

    public String getORG_Info() {
        return ORG_Info;
    }

    public void setORG_Info(String ORG_Info) {
        this.ORG_Info = ORG_Info;
    }

    public String getORG_Info_Name() {
        return ORG_Info_Name;
    }

    public void setORG_Info_Name(String ORG_Info_Name) {
        this.ORG_Info_Name = ORG_Info_Name;
    }

    public String getORG_Info_Order() {
        return ORG_Info_Order;
    }

    public void setORG_Info_Order(String ORG_Info_Order) {
        this.ORG_Info_Order = ORG_Info_Order;
    }

    public String getORG_Info_Type() {
        return ORG_Info_Type;
    }

    public void setORG_Info_Type(String ORG_Info_Type) {
        this.ORG_Info_Type = ORG_Info_Type;
    }

    public String getORG_Info_Province() {
        return ORG_Info_Province;
    }

    public void setORG_Info_Province(String ORG_Info_Province) {
        this.ORG_Info_Province = ORG_Info_Province;
    }

    public String getORG_Info_City() {
        return ORG_Info_City;
    }

    public void setORG_Info_City(String ORG_Info_City) {
        this.ORG_Info_City = ORG_Info_City;
    }

    public String getORG_Info_County() {
        return ORG_Info_County;
    }

    public void setORG_Info_County(String ORG_Info_County) {
        this.ORG_Info_County = ORG_Info_County;
    }

    public String getORG_Info_Hierarchy() {
        return ORG_Info_Hierarchy;
    }

    public void setORG_Info_Hierarchy(String ORG_Info_Hierarchy) {
        this.ORG_Info_Hierarchy = ORG_Info_Hierarchy;
    }

    public String getORG_Info_Level1() {
        return ORG_Info_Level1;
    }

    public void setORG_Info_Level1(String ORG_Info_Level1) {
        this.ORG_Info_Level1 = ORG_Info_Level1;
    }

    public String getORG_Info_Level2() {
        return ORG_Info_Level2;
    }

    public void setORG_Info_Level2(String ORG_Info_Level2) {
        this.ORG_Info_Level2 = ORG_Info_Level2;
    }

    public String getORG_Info_Level3() {
        return ORG_Info_Level3;
    }

    public void setORG_Info_Level3(String ORG_Info_Level3) {
        this.ORG_Info_Level3 = ORG_Info_Level3;
    }

    public String getORG_Info_Level4() {
        return ORG_Info_Level4;
    }

    public void setORG_Info_Level4(String ORG_Info_Level4) {
        this.ORG_Info_Level4 = ORG_Info_Level4;
    }

    public String getORG_Info_Level5() {
        return ORG_Info_Level5;
    }

    public void setORG_Info_Level5(String ORG_Info_Level5) {
        this.ORG_Info_Level5 = ORG_Info_Level5;
    }
}
