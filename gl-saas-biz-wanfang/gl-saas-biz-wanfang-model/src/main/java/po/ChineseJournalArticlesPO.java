package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//万方原始数据
public class ChineseJournalArticlesPO {
    @FieldOrder(order = 1)
    private String FullText;
    @FieldOrder(order = 2)
    private String Topic;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 3)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }

    @FieldOrder(order = 4)
    private String OrderID;
    @FieldOrder(order = 5)
    private String Doi;
    @FieldOrder(order = 6)
    private String W_ID;
    @FieldOrder(order = 7)
    private String V_ID;
    @FieldOrder(order = 8)
    private String C_ID;
    @FieldOrder(order = 9)
    private String L_ID;
    @FieldOrder(order = 10)
    private String Chinese_Title;
    @FieldOrder(order = 11)
    private String English_Title;
    @FieldOrder(order = 12)
    private String C_E_Title;
    @FieldOrder(order = 13)
    private String Author_ID;
    @FieldOrder(order = 14)
    private String Chinese_Author;
    @FieldOrder(order = 15)
    private String English_Author;
    @FieldOrder(order = 16)
    private String Number_Authors;
    @FieldOrder(order = 17)
    private String Author_FID;
    @FieldOrder(order = 18)
    private String Chinese_First_Author;
    @FieldOrder(order = 19)
    private String English_First_Author;
    @FieldOrder(order = 20)
    private String Author_Number;
    @FieldOrder(order = 21)
    private String Chinese_English_Author;
    @FieldOrder(order = 22)
    private String Author_Name;
    @FieldOrder(order = 23)
    private String Author_Name2;
    @FieldOrder(order = 24)
    private String Authority_Unit_Name;
    @FieldOrder(order = 25)
    private String Chinese_Author_Unit;
    @FieldOrder(order = 26)
    private String English_Author_Unit;
    @FieldOrder(order = 27)
    private String Chinese_First_Author_Unit;
    @FieldOrder(order = 28)
    private String Chinese_First_Author_Unit_Name;
    @FieldOrder(order = 29)
    private String First_English_Author;
    @FieldOrder(order = 30)
    private String ORG_Name;
    @FieldOrder(order = 31)
    private String ORG_Name_Search;
    @FieldOrder(order = 32)
    private String C_E_S_Author_Unit;
    @FieldOrder(order = 33)
    private String QkID;
    @FieldOrder(order = 34)
    private String Issn;
    @FieldOrder(order = 35)
    private String Chinese_Journal_Name;
    @FieldOrder(order = 36)
    private String English_Journal_Name;
    @FieldOrder(order = 37)
    private String Journal;
    @FieldOrder(order = 38)
    private String Journal_Anyname;
    @FieldOrder(order = 39)
    private String Journal_Anyname2;
    @FieldOrder(order = 40)
    private String Chinese_Journal_Name2;
    @FieldOrder(order = 41)
    private String Fjoucn_Anyname;
    @FieldOrder(order = 42)
    private String Fjoucn_Anyname2;
    @FieldOrder(order = 43)
    private Date Date;
    @FieldOrder(order = 44)
    private String Year;
    @FieldOrder(order = 45)
    private String Volume;
    @FieldOrder(order = 46)
    private String Period;
    @FieldOrder(order = 47)
    private String Page_Number;
    @FieldOrder(order = 48)
    private String Pages;
    @FieldOrder(order = 49)
    private String Chinese_Column_Name;
    @FieldOrder(order = 50)
    private String English_Column_Name;
    @FieldOrder(order = 51)
    private String Language;
    @FieldOrder(order = 52)
    private String Cls_Machine_Standard_Code;
    @FieldOrder(order = 53)
    private String Discipline_Class;
    @FieldOrder(order = 54)
    private String Literature_IDentification_Code;
    @FieldOrder(order = 55)
    private String Machine_Label_Class_Code;
    @FieldOrder(order = 56)
    private String Cls_Code;
    @FieldOrder(order = 57)
    private String Cls_level2;
    @FieldOrder(order = 58)
    private String Cls_Top;
    @FieldOrder(order = 59)
    private String Cls_level3;
    @FieldOrder(order = 60)
    private String IID;
    @FieldOrder(order = 61)
    private String Chinese_KeyWord;
    @FieldOrder(order = 62)
    private String Ce_KeyWord;
    @FieldOrder(order = 63)
    private String English_KeyWord;
    @FieldOrder(order = 64)
    private String Machine_KeyWord;
    @FieldOrder(order = 65)
    private String Chinese_Abstract;
    @FieldOrder(order = 66)
    private String C_Paragraph;
    @FieldOrder(order = 67)
    private String English_Abstract;
    @FieldOrder(order = 68)
    private String First_Paragraph;
    @FieldOrder(order = 69)
    private String Fund;
    @FieldOrder(order = 70)
    private String Fund_FullText_Search;
    @FieldOrder(order = 71)
    private String Fund_Name;
    @FieldOrder(order = 72)
    private String Fund_Name2;
    @FieldOrder(order = 73)
    private String Fund_Project;
    @FieldOrder(order = 74)
    private String Fund_Name3;
    @FieldOrder(order = 75)
    private String Bycs;
    @FieldOrder(order = 76)
    private String Core;
    @FieldOrder(order = 77)
    private String Score;
    @FieldOrder(order = 78)
    private String Ecore;
    @FieldOrder(order = 79)
    private String Data_Source;
    @FieldOrder(order = 80)
    private String ORG_Hierarchy_ID;
    @FieldOrder(order = 81)
    private String ORG_ID;
    @FieldOrder(order = 82)
    private String ORG_Province;
    @FieldOrder(order = 83)
    private String ORG_City;
    @FieldOrder(order = 84)
    private String ORG_Type;
    @FieldOrder(order = 85)
    private String FORGID;
    @FieldOrder(order = 86)
    private String EndORGtype;
    @FieldOrder(order = 87)
    private String ORG_Frist_Final_ID;
    @FieldOrder(order = 88)
    private String ORG_Frist_Hierarchy_ID;
    @FieldOrder(order = 89)
    private String ORG_Frist_City;
    @FieldOrder(order = 90)
    private String ORG_Frist_Final_City;
    @FieldOrder(order = 91)
    private String ORG_Frist_Province;
    @FieldOrder(order = 92)
    private String ORG_Frist_Province2;
    @FieldOrder(order = 93)
    private String ORG_Frist_Final_Type;
    @FieldOrder(order = 94)
    private String ORG_Frist_Type;
    @FieldOrder(order = 95)
    private String All_Author_ID;
    @FieldOrder(order = 96)
    private String ORG_Number;
    @FieldOrder(order = 97)
    private String Literature_Weight;
    @FieldOrder(order = 98)
    private String Quarter;
    @FieldOrder(order = 99)
    private String Halfyear;
    @FieldOrder(order = 100)
    private String Error_Code;
    @FieldOrder(order = 101)
    private String AuthorInfo;
    @FieldOrder(order = 102)
    private String Author_Info_Name;
    @FieldOrder(order = 103)
    private String Author_Info_Order;
    @FieldOrder(order = 104)
    private String Author_Info_Unit;
    @FieldOrder(order = 105)
    private String Author_Info_Unit_ORG_Level1;
    @FieldOrder(order = 106)
    private String Author_Info_Unit_ORG_Level2;
    @FieldOrder(order = 107)
    private String Author_Info_Unit_Type;
    @FieldOrder(order = 108)
    private String Author_Info_Unit_Province;
    @FieldOrder(order = 109)
    private String Author_Info_Unit_City;
    @FieldOrder(order = 110)
    private String Author_Info_Unit_County;
    @FieldOrder(order = 111)
    private String Author_Info_ID;
    @FieldOrder(order = 112)
    private String Author_Info_Unit_ID;
    @FieldOrder(order = 113)
    private String ORG_Info;
    @FieldOrder(order = 114)
    private String ORG_Info_Name;
    @FieldOrder(order = 115)
    private String ORG_Info_Order;
    @FieldOrder(order = 116)
    private String ORG_Info_Type;
    @FieldOrder(order = 117)
    private String ORG_Info_Province;
    @FieldOrder(order = 118)
    private String ORG_Info_City;
    @FieldOrder(order = 119)
    private String ORG_Info_County;
    @FieldOrder(order = 120)
    private String ORG_Info_Hierarchy;
    @FieldOrder(order = 121)
    private String ORG_Info_Level1;
    @FieldOrder(order = 122)
    private String ORG_Info_Level2;
    @FieldOrder(order = 123)
    private String ORG_Info_Level3;
    @FieldOrder(order = 124)
    private String ORG_Info_Level4;
    @FieldOrder(order = 125)
    private String ORG_Info_Level5;
    @FieldOrder(order = 126)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private java.util.Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String topic) {
        Topic = topic;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getDoi() {
        return Doi;
    }

    public void setDoi(String doi) {
        Doi = doi;
    }

    public String getW_ID() {
        return W_ID;
    }

    public void setW_ID(String w_ID) {
        W_ID = w_ID;
    }

    public String getV_ID() {
        return V_ID;
    }

    public void setV_ID(String v_ID) {
        V_ID = v_ID;
    }

    public String getC_ID() {
        return C_ID;
    }

    public void setC_ID(String c_ID) {
        C_ID = c_ID;
    }

    public String getL_ID() {
        return L_ID;
    }

    public void setL_ID(String l_ID) {
        L_ID = l_ID;
    }

    public String getChinese_Title() {
        return Chinese_Title;
    }

    public void setChinese_Title(String chinese_Title) {
        Chinese_Title = chinese_Title;
    }

    public String getEnglish_Title() {
        return English_Title;
    }

    public void setEnglish_Title(String english_Title) {
        English_Title = english_Title;
    }

    public String getC_E_Title() {
        return C_E_Title;
    }

    public void setC_E_Title(String c_E_Title) {
        C_E_Title = c_E_Title;
    }

    public String getAuthor_ID() {
        return Author_ID;
    }

    public void setAuthor_ID(String author_ID) {
        Author_ID = author_ID;
    }

    public String getChinese_Author() {
        return Chinese_Author;
    }

    public void setChinese_Author(String chinese_Author) {
        Chinese_Author = chinese_Author;
    }

    public String getEnglish_Author() {
        return English_Author;
    }

    public void setEnglish_Author(String english_Author) {
        English_Author = english_Author;
    }

    public String getNumber_Authors() {
        return Number_Authors;
    }

    public void setNumber_Authors(String number_Authors) {
        Number_Authors = number_Authors;
    }

    public String getAuthor_FID() {
        return Author_FID;
    }

    public void setAuthor_FID(String author_FID) {
        Author_FID = author_FID;
    }

    public String getChinese_First_Author() {
        return Chinese_First_Author;
    }

    public void setChinese_First_Author(String chinese_First_Author) {
        Chinese_First_Author = chinese_First_Author;
    }

    public String getEnglish_First_Author() {
        return English_First_Author;
    }

    public void setEnglish_First_Author(String english_First_Author) {
        English_First_Author = english_First_Author;
    }

    public String getAuthor_Number() {
        return Author_Number;
    }

    public void setAuthor_Number(String author_Number) {
        Author_Number = author_Number;
    }

    public String getChinese_English_Author() {
        return Chinese_English_Author;
    }

    public void setChinese_English_Author(String chinese_English_Author) {
        Chinese_English_Author = chinese_English_Author;
    }

    public String getAuthor_Name() {
        return Author_Name;
    }

    public void setAuthor_Name(String author_Name) {
        Author_Name = author_Name;
    }

    public String getAuthor_Name2() {
        return Author_Name2;
    }

    public void setAuthor_Name2(String author_Name2) {
        Author_Name2 = author_Name2;
    }

    public String getAuthority_Unit_Name() {
        return Authority_Unit_Name;
    }

    public void setAuthority_Unit_Name(String authority_Unit_Name) {
        Authority_Unit_Name = authority_Unit_Name;
    }

    public String getChinese_Author_Unit() {
        return Chinese_Author_Unit;
    }

    public void setChinese_Author_Unit(String chinese_Author_Unit) {
        Chinese_Author_Unit = chinese_Author_Unit;
    }

    public String getEnglish_Author_Unit() {
        return English_Author_Unit;
    }

    public void setEnglish_Author_Unit(String english_Author_Unit) {
        English_Author_Unit = english_Author_Unit;
    }

    public String getChinese_First_Author_Unit() {
        return Chinese_First_Author_Unit;
    }

    public void setChinese_First_Author_Unit(String chinese_First_Author_Unit) {
        Chinese_First_Author_Unit = chinese_First_Author_Unit;
    }

    public String getChinese_First_Author_Unit_Name() {
        return Chinese_First_Author_Unit_Name;
    }

    public void setChinese_First_Author_Unit_Name(String chinese_First_Author_Unit_Name) {
        Chinese_First_Author_Unit_Name = chinese_First_Author_Unit_Name;
    }

    public String getFirst_English_Author() {
        return First_English_Author;
    }

    public void setFirst_English_Author(String first_English_Author) {
        First_English_Author = first_English_Author;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getC_E_S_Author_Unit() {
        return C_E_S_Author_Unit;
    }

    public void setC_E_S_Author_Unit(String c_E_S_Author_Unit) {
        C_E_S_Author_Unit = c_E_S_Author_Unit;
    }

    public String getQkID() {
        return QkID;
    }

    public void setQkID(String qkID) {
        QkID = qkID;
    }

    public String getIssn() {
        return Issn;
    }

    public void setIssn(String issn) {
        Issn = issn;
    }

    public String getChinese_Journal_Name() {
        return Chinese_Journal_Name;
    }

    public void setChinese_Journal_Name(String chinese_Journal_Name) {
        Chinese_Journal_Name = chinese_Journal_Name;
    }

    public String getEnglish_Journal_Name() {
        return English_Journal_Name;
    }

    public void setEnglish_Journal_Name(String english_Journal_Name) {
        English_Journal_Name = english_Journal_Name;
    }

    public String getJournal() {
        return Journal;
    }

    public void setJournal(String journal) {
        Journal = journal;
    }

    public String getJournal_Anyname() {
        return Journal_Anyname;
    }

    public void setJournal_Anyname(String journal_Anyname) {
        Journal_Anyname = journal_Anyname;
    }

    public String getJournal_Anyname2() {
        return Journal_Anyname2;
    }

    public void setJournal_Anyname2(String journal_Anyname2) {
        Journal_Anyname2 = journal_Anyname2;
    }

    public String getChinese_Journal_Name2() {
        return Chinese_Journal_Name2;
    }

    public void setChinese_Journal_Name2(String chinese_Journal_Name2) {
        Chinese_Journal_Name2 = chinese_Journal_Name2;
    }

    public String getFjoucn_Anyname() {
        return Fjoucn_Anyname;
    }

    public void setFjoucn_Anyname(String fjoucn_Anyname) {
        Fjoucn_Anyname = fjoucn_Anyname;
    }

    public String getFjoucn_Anyname2() {
        return Fjoucn_Anyname2;
    }

    public void setFjoucn_Anyname2(String fjoucn_Anyname2) {
        Fjoucn_Anyname2 = fjoucn_Anyname2;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date date) {
        Date = date;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String volume) {
        Volume = volume;
    }

    public String getPeriod() {
        return Period;
    }

    public void setPeriod(String period) {
        Period = period;
    }

    public String getPage_Number() {
        return Page_Number;
    }

    public void setPage_Number(String page_Number) {
        Page_Number = page_Number;
    }

    public String getPages() {
        return Pages;
    }

    public void setPages(String pages) {
        Pages = pages;
    }

    public String getChinese_Column_Name() {
        return Chinese_Column_Name;
    }

    public void setChinese_Column_Name(String chinese_Column_Name) {
        Chinese_Column_Name = chinese_Column_Name;
    }

    public String getEnglish_Column_Name() {
        return English_Column_Name;
    }

    public void setEnglish_Column_Name(String english_Column_Name) {
        English_Column_Name = english_Column_Name;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getCls_Machine_Standard_Code() {
        return Cls_Machine_Standard_Code;
    }

    public void setCls_Machine_Standard_Code(String cls_Machine_Standard_Code) {
        Cls_Machine_Standard_Code = cls_Machine_Standard_Code;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getLiterature_IDentification_Code() {
        return Literature_IDentification_Code;
    }

    public void setLiterature_IDentification_Code(String literature_IDentification_Code) {
        Literature_IDentification_Code = literature_IDentification_Code;
    }

    public String getMachine_Label_Class_Code() {
        return Machine_Label_Class_Code;
    }

    public void setMachine_Label_Class_Code(String machine_Label_Class_Code) {
        Machine_Label_Class_Code = machine_Label_Class_Code;
    }

    public String getCls_Code() {
        return Cls_Code;
    }

    public void setCls_Code(String cls_Code) {
        Cls_Code = cls_Code;
    }

    public String getCls_level2() {
        return Cls_level2;
    }

    public void setCls_level2(String cls_level2) {
        Cls_level2 = cls_level2;
    }

    public String getCls_Top() {
        return Cls_Top;
    }

    public void setCls_Top(String cls_Top) {
        Cls_Top = cls_Top;
    }

    public String getCls_level3() {
        return Cls_level3;
    }

    public void setCls_level3(String cls_level3) {
        Cls_level3 = cls_level3;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getChinese_KeyWord() {
        return Chinese_KeyWord;
    }

    public void setChinese_KeyWord(String chinese_KeyWord) {
        Chinese_KeyWord = chinese_KeyWord;
    }

    public String getCe_KeyWord() {
        return Ce_KeyWord;
    }

    public void setCe_KeyWord(String ce_KeyWord) {
        Ce_KeyWord = ce_KeyWord;
    }

    public String getEnglish_KeyWord() {
        return English_KeyWord;
    }

    public void setEnglish_KeyWord(String english_KeyWord) {
        English_KeyWord = english_KeyWord;
    }

    public String getMachine_KeyWord() {
        return Machine_KeyWord;
    }

    public void setMachine_KeyWord(String machine_KeyWord) {
        Machine_KeyWord = machine_KeyWord;
    }

    public String getChinese_Abstract() {
        return Chinese_Abstract;
    }

    public void setChinese_Abstract(String chinese_Abstract) {
        Chinese_Abstract = chinese_Abstract;
    }

    public String getC_Paragraph() {
        return C_Paragraph;
    }

    public void setC_Paragraph(String c_Paragraph) {
        C_Paragraph = c_Paragraph;
    }

    public String getEnglish_Abstract() {
        return English_Abstract;
    }

    public void setEnglish_Abstract(String english_Abstract) {
        English_Abstract = english_Abstract;
    }

    public String getFirst_Paragraph() {
        return First_Paragraph;
    }

    public void setFirst_Paragraph(String first_Paragraph) {
        First_Paragraph = first_Paragraph;
    }

    public String getFund() {
        return Fund;
    }

    public void setFund(String fund) {
        Fund = fund;
    }

    public String getFund_FullText_Search() {
        return Fund_FullText_Search;
    }

    public void setFund_FullText_Search(String fund_FullText_Search) {
        Fund_FullText_Search = fund_FullText_Search;
    }

    public String getFund_Name() {
        return Fund_Name;
    }

    public void setFund_Name(String fund_Name) {
        Fund_Name = fund_Name;
    }

    public String getFund_Name2() {
        return Fund_Name2;
    }

    public void setFund_Name2(String fund_Name2) {
        Fund_Name2 = fund_Name2;
    }

    public String getFund_Project() {
        return Fund_Project;
    }

    public void setFund_Project(String fund_Project) {
        Fund_Project = fund_Project;
    }

    public String getFund_Name3() {
        return Fund_Name3;
    }

    public void setFund_Name3(String fund_Name3) {
        Fund_Name3 = fund_Name3;
    }

    public String getBycs() {
        return Bycs;
    }

    public void setBycs(String bycs) {
        Bycs = bycs;
    }

    public String getCore() {
        return Core;
    }

    public void setCore(String core) {
        Core = core;
    }

    public String getScore() {
        return Score;
    }

    public void setScore(String score) {
        Score = score;
    }

    public String getEcore() {
        return Ecore;
    }

    public void setEcore(String ecore) {
        Ecore = ecore;
    }

    public String getData_Source() {
        return Data_Source;
    }

    public void setData_Source(String data_Source) {
        Data_Source = data_Source;
    }

    public String getORG_Hierarchy_ID() {
        return ORG_Hierarchy_ID;
    }

    public void setORG_Hierarchy_ID(String ORG_Hierarchy_ID) {
        this.ORG_Hierarchy_ID = ORG_Hierarchy_ID;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getORG_Province() {
        return ORG_Province;
    }

    public void setORG_Province(String ORG_Province) {
        this.ORG_Province = ORG_Province;
    }

    public String getORG_City() {
        return ORG_City;
    }

    public void setORG_City(String ORG_City) {
        this.ORG_City = ORG_City;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getEndORGtype() {
        return EndORGtype;
    }

    public void setEndORGtype(String endORGtype) {
        EndORGtype = endORGtype;
    }

    public String getORG_Frist_Final_ID() {
        return ORG_Frist_Final_ID;
    }

    public void setORG_Frist_Final_ID(String ORG_Frist_Final_ID) {
        this.ORG_Frist_Final_ID = ORG_Frist_Final_ID;
    }

    public String getORG_Frist_Hierarchy_ID() {
        return ORG_Frist_Hierarchy_ID;
    }

    public void setORG_Frist_Hierarchy_ID(String ORG_Frist_Hierarchy_ID) {
        this.ORG_Frist_Hierarchy_ID = ORG_Frist_Hierarchy_ID;
    }

    public String getORG_Frist_City() {
        return ORG_Frist_City;
    }

    public void setORG_Frist_City(String ORG_Frist_City) {
        this.ORG_Frist_City = ORG_Frist_City;
    }

    public String getORG_Frist_Final_City() {
        return ORG_Frist_Final_City;
    }

    public void setORG_Frist_Final_City(String ORG_Frist_Final_City) {
        this.ORG_Frist_Final_City = ORG_Frist_Final_City;
    }

    public String getORG_Frist_Province() {
        return ORG_Frist_Province;
    }

    public void setORG_Frist_Province(String ORG_Frist_Province) {
        this.ORG_Frist_Province = ORG_Frist_Province;
    }

    public String getORG_Frist_Province2() {
        return ORG_Frist_Province2;
    }

    public void setORG_Frist_Province2(String ORG_Frist_Province2) {
        this.ORG_Frist_Province2 = ORG_Frist_Province2;
    }

    public String getORG_Frist_Final_Type() {
        return ORG_Frist_Final_Type;
    }

    public void setORG_Frist_Final_Type(String ORG_Frist_Final_Type) {
        this.ORG_Frist_Final_Type = ORG_Frist_Final_Type;
    }

    public String getORG_Frist_Type() {
        return ORG_Frist_Type;
    }

    public void setORG_Frist_Type(String ORG_Frist_Type) {
        this.ORG_Frist_Type = ORG_Frist_Type;
    }

    public String getAll_Author_ID() {
        return All_Author_ID;
    }

    public void setAll_Author_ID(String all_Author_ID) {
        All_Author_ID = all_Author_ID;
    }

    public String getORG_Number() {
        return ORG_Number;
    }

    public void setORG_Number(String ORG_Number) {
        this.ORG_Number = ORG_Number;
    }

    public String getLiterature_Weight() {
        return Literature_Weight;
    }

    public void setLiterature_Weight(String literature_Weight) {
        Literature_Weight = literature_Weight;
    }

    public String getQuarter() {
        return Quarter;
    }

    public void setQuarter(String quarter) {
        Quarter = quarter;
    }

    public String getHalfyear() {
        return Halfyear;
    }

    public void setHalfyear(String halfyear) {
        Halfyear = halfyear;
    }

    public String getError_Code() {
        return Error_Code;
    }

    public void setError_Code(String error_Code) {
        Error_Code = error_Code;
    }

    public String getAuthorInfo() {
        return AuthorInfo;
    }

    public void setAuthorInfo(String authorInfo) {
        AuthorInfo = authorInfo;
    }

    public String getAuthor_Info_Name() {
        return Author_Info_Name;
    }

    public void setAuthor_Info_Name(String author_Info_Name) {
        Author_Info_Name = author_Info_Name;
    }

    public String getAuthor_Info_Order() {
        return Author_Info_Order;
    }

    public void setAuthor_Info_Order(String author_Info_Order) {
        Author_Info_Order = author_Info_Order;
    }

    public String getAuthor_Info_Unit() {
        return Author_Info_Unit;
    }

    public void setAuthor_Info_Unit(String author_Info_Unit) {
        Author_Info_Unit = author_Info_Unit;
    }

    public String getAuthor_Info_Unit_ORG_Level1() {
        return Author_Info_Unit_ORG_Level1;
    }

    public void setAuthor_Info_Unit_ORG_Level1(String author_Info_Unit_ORG_Level1) {
        Author_Info_Unit_ORG_Level1 = author_Info_Unit_ORG_Level1;
    }

    public String getAuthor_Info_Unit_ORG_Level2() {
        return Author_Info_Unit_ORG_Level2;
    }

    public void setAuthor_Info_Unit_ORG_Level2(String author_Info_Unit_ORG_Level2) {
        Author_Info_Unit_ORG_Level2 = author_Info_Unit_ORG_Level2;
    }

    public String getAuthor_Info_Unit_Type() {
        return Author_Info_Unit_Type;
    }

    public void setAuthor_Info_Unit_Type(String author_Info_Unit_Type) {
        Author_Info_Unit_Type = author_Info_Unit_Type;
    }

    public String getAuthor_Info_Unit_Province() {
        return Author_Info_Unit_Province;
    }

    public void setAuthor_Info_Unit_Province(String author_Info_Unit_Province) {
        Author_Info_Unit_Province = author_Info_Unit_Province;
    }

    public String getAuthor_Info_Unit_City() {
        return Author_Info_Unit_City;
    }

    public void setAuthor_Info_Unit_City(String author_Info_Unit_City) {
        Author_Info_Unit_City = author_Info_Unit_City;
    }

    public String getAuthor_Info_Unit_County() {
        return Author_Info_Unit_County;
    }

    public void setAuthor_Info_Unit_County(String author_Info_Unit_County) {
        Author_Info_Unit_County = author_Info_Unit_County;
    }

    public String getAuthor_Info_ID() {
        return Author_Info_ID;
    }

    public void setAuthor_Info_ID(String author_Info_ID) {
        Author_Info_ID = author_Info_ID;
    }

    public String getAuthor_Info_Unit_ID() {
        return Author_Info_Unit_ID;
    }

    public void setAuthor_Info_Unit_ID(String author_Info_Unit_ID) {
        Author_Info_Unit_ID = author_Info_Unit_ID;
    }

    public String getORG_Info() {
        return ORG_Info;
    }

    public void setORG_Info(String ORG_Info) {
        this.ORG_Info = ORG_Info;
    }

    public String getORG_Info_Name() {
        return ORG_Info_Name;
    }

    public void setORG_Info_Name(String ORG_Info_Name) {
        this.ORG_Info_Name = ORG_Info_Name;
    }

    public String getORG_Info_Order() {
        return ORG_Info_Order;
    }

    public void setORG_Info_Order(String ORG_Info_Order) {
        this.ORG_Info_Order = ORG_Info_Order;
    }

    public String getORG_Info_Type() {
        return ORG_Info_Type;
    }

    public void setORG_Info_Type(String ORG_Info_Type) {
        this.ORG_Info_Type = ORG_Info_Type;
    }

    public String getORG_Info_Province() {
        return ORG_Info_Province;
    }

    public void setORG_Info_Province(String ORG_Info_Province) {
        this.ORG_Info_Province = ORG_Info_Province;
    }

    public String getORG_Info_City() {
        return ORG_Info_City;
    }

    public void setORG_Info_City(String ORG_Info_City) {
        this.ORG_Info_City = ORG_Info_City;
    }

    public String getORG_Info_County() {
        return ORG_Info_County;
    }

    public void setORG_Info_County(String ORG_Info_County) {
        this.ORG_Info_County = ORG_Info_County;
    }

    public String getORG_Info_Hierarchy() {
        return ORG_Info_Hierarchy;
    }

    public void setORG_Info_Hierarchy(String ORG_Info_Hierarchy) {
        this.ORG_Info_Hierarchy = ORG_Info_Hierarchy;
    }

    public String getORG_Info_Level1() {
        return ORG_Info_Level1;
    }

    public void setORG_Info_Level1(String ORG_Info_Level1) {
        this.ORG_Info_Level1 = ORG_Info_Level1;
    }

    public String getORG_Info_Level2() {
        return ORG_Info_Level2;
    }

    public void setORG_Info_Level2(String ORG_Info_Level2) {
        this.ORG_Info_Level2 = ORG_Info_Level2;
    }

    public String getORG_Info_Level3() {
        return ORG_Info_Level3;
    }

    public void setORG_Info_Level3(String ORG_Info_Level3) {
        this.ORG_Info_Level3 = ORG_Info_Level3;
    }

    public String getORG_Info_Level4() {
        return ORG_Info_Level4;
    }

    public void setORG_Info_Level4(String ORG_Info_Level4) {
        this.ORG_Info_Level4 = ORG_Info_Level4;
    }

    public String getORG_Info_Level5() {
        return ORG_Info_Level5;
    }

    public void setORG_Info_Level5(String ORG_Info_Level5) {
        this.ORG_Info_Level5 = ORG_Info_Level5;
    }
}
