package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class ChinesePaperOAPO {
    @FieldOrder(order = 1)
    private String FullText;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String OrderID;
    @FieldOrder(order = 4)
    private String Title;
    @FieldOrder(order = 5)
    private String Abstract;
    @FieldOrder(order = 6)
    private String WCHINESE_PAPER_OA_CAB;
    @FieldOrder(order = 7)
    private String Author;
    @FieldOrder(order = 8)
    private String Author_Name;
    @FieldOrder(order = 9)
    private String Author_Name2;
    @FieldOrder(order = 10)
    private String KeyWord;
    @FieldOrder(order = 11)
    private String Cls;
    @FieldOrder(order = 12)
    private String ORG_Name;
    @FieldOrder(order = 13)
    private String ORG_Name_Search;
    @FieldOrder(order = 14)
    private String ORG_Creator;
    @FieldOrder(order = 15)
    private String Issued_Date;
    @FieldOrder(order = 16)
    private String Issued_Year;
    @FieldOrder(order = 17)
    private String IID;
    @FieldOrder(order = 18)
    private String DOI;
    @FieldOrder(order = 19)
    private String WCHINESE_PAPER_OA_ISSN;
    @FieldOrder(order = 20)
    private String WCHINESE_PAPER_OA_EISSN;
    @FieldOrder(order = 21)
    private String Author_Name3;
    @FieldOrder(order = 22)
    private String WCHINESE_PAPER_OA_SOURCE_SOURCE;
    @FieldOrder(order = 23)
    private String WCHINESE_PAPER_OA_SOURCE_SOURCE2;
    @FieldOrder(order = 24)
    private String Source_Vol;
    @FieldOrder(order = 25)
    private String Source_Issue;
    @FieldOrder(order = 26)
    private String Source_Page;
    @FieldOrder(order = 27)
    private String Creator_ORG;
    @FieldOrder(order = 28)
    private String Download_Date;
    @FieldOrder(order = 29)
    private String Publisher_Publisher;
    @FieldOrder(order = 30)
    private String WCHINESE_PAPER_OA_YNFREE;
    @FieldOrder(order = 31)
    private String Data_Link;
    @FieldOrder(order = 32)
    private String WCHINESE_PAPER_OA_Discipline_SELFFL;
    @FieldOrder(order = 33)
    private String WCHINESE_PAPER_OA_IISSNE;
    @FieldOrder(order = 34)
    private String IDentifier_ISSNp;
    @FieldOrder(order = 35)
    private String WCHINESE_PAPER_OA_ZCID;
    @FieldOrder(order = 36)
    private String Discipline_Class;
    @FieldOrder(order = 37)
    private String WCHINESE_PAPER_OA_TZCID;
    @FieldOrder(order = 38)
    private String WCHINESE_PAPER_OA_SZCID;
    @FieldOrder(order = 39)
    private String Cls_level3;
    @FieldOrder(order = 40)
    private String WCHINESE_PAPER_OA_Class;
    @FieldOrder(order = 41)
    private String WCHINESE_PAPER_OA_RN;
    @FieldOrder(order = 42)
    private String WCHINESE_PAPER_OA_BN;
    @FieldOrder(order = 43)
    private String WCHINESE_PAPER_OA_DN;
    @FieldOrder(order = 44)
    private String WCHINESE_PAPER_OA_OTS;
    @FieldOrder(order = 45)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getWCHINESE_PAPER_OA_CAB() {
        return WCHINESE_PAPER_OA_CAB;
    }

    public void setWCHINESE_PAPER_OA_CAB(String WCHINESE_PAPER_OA_CAB) {
        this.WCHINESE_PAPER_OA_CAB = WCHINESE_PAPER_OA_CAB;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getAuthor_Name() {
        return Author_Name;
    }

    public void setAuthor_Name(String author_Name) {
        Author_Name = author_Name;
    }

    public String getAuthor_Name2() {
        return Author_Name2;
    }

    public void setAuthor_Name2(String author_Name2) {
        Author_Name2 = author_Name2;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }

    public String getCls() {
        return Cls;
    }

    public void setCls(String cls) {
        Cls = cls;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getORG_Creator() {
        return ORG_Creator;
    }

    public void setORG_Creator(String ORG_Creator) {
        this.ORG_Creator = ORG_Creator;
    }

    public String getIssued_Date() {
        return Issued_Date;
    }

    public void setIssued_Date(String issued_Date) {
        Issued_Date = issued_Date;
    }

    public String getIssued_Year() {
        return Issued_Year;
    }

    public void setIssued_Year(String issued_Year) {
        Issued_Year = issued_Year;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getWCHINESE_PAPER_OA_ISSN() {
        return WCHINESE_PAPER_OA_ISSN;
    }

    public void setWCHINESE_PAPER_OA_ISSN(String WCHINESE_PAPER_OA_ISSN) {
        this.WCHINESE_PAPER_OA_ISSN = WCHINESE_PAPER_OA_ISSN;
    }

    public String getWCHINESE_PAPER_OA_EISSN() {
        return WCHINESE_PAPER_OA_EISSN;
    }

    public void setWCHINESE_PAPER_OA_EISSN(String WCHINESE_PAPER_OA_EISSN) {
        this.WCHINESE_PAPER_OA_EISSN = WCHINESE_PAPER_OA_EISSN;
    }

    public String getAuthor_Name3() {
        return Author_Name3;
    }

    public void setAuthor_Name3(String author_Name3) {
        Author_Name3 = author_Name3;
    }

    public String getWCHINESE_PAPER_OA_SOURCE_SOURCE() {
        return WCHINESE_PAPER_OA_SOURCE_SOURCE;
    }

    public void setWCHINESE_PAPER_OA_SOURCE_SOURCE(String WCHINESE_PAPER_OA_SOURCE_SOURCE) {
        this.WCHINESE_PAPER_OA_SOURCE_SOURCE = WCHINESE_PAPER_OA_SOURCE_SOURCE;
    }

    public String getWCHINESE_PAPER_OA_SOURCE_SOURCE2() {
        return WCHINESE_PAPER_OA_SOURCE_SOURCE2;
    }

    public void setWCHINESE_PAPER_OA_SOURCE_SOURCE2(String WCHINESE_PAPER_OA_SOURCE_SOURCE2) {
        this.WCHINESE_PAPER_OA_SOURCE_SOURCE2 = WCHINESE_PAPER_OA_SOURCE_SOURCE2;
    }

    public String getSource_Vol() {
        return Source_Vol;
    }

    public void setSource_Vol(String source_Vol) {
        Source_Vol = source_Vol;
    }

    public String getSource_Issue() {
        return Source_Issue;
    }

    public void setSource_Issue(String source_Issue) {
        Source_Issue = source_Issue;
    }

    public String getSource_Page() {
        return Source_Page;
    }

    public void setSource_Page(String source_Page) {
        Source_Page = source_Page;
    }

    public String getCreator_ORG() {
        return Creator_ORG;
    }

    public void setCreator_ORG(String creator_ORG) {
        Creator_ORG = creator_ORG;
    }

    public String getDownload_Date() {
        return Download_Date;
    }

    public void setDownload_Date(String download_Date) {
        Download_Date = download_Date;
    }

    public String getPublisher_Publisher() {
        return Publisher_Publisher;
    }

    public void setPublisher_Publisher(String publisher_Publisher) {
        Publisher_Publisher = publisher_Publisher;
    }

    public String getWCHINESE_PAPER_OA_YNFREE() {
        return WCHINESE_PAPER_OA_YNFREE;
    }

    public void setWCHINESE_PAPER_OA_YNFREE(String WCHINESE_PAPER_OA_YNFREE) {
        this.WCHINESE_PAPER_OA_YNFREE = WCHINESE_PAPER_OA_YNFREE;
    }

    public String getData_Link() {
        return Data_Link;
    }

    public void setData_Link(String data_Link) {
        Data_Link = data_Link;
    }

    public String getWCHINESE_PAPER_OA_Discipline_SELFFL() {
        return WCHINESE_PAPER_OA_Discipline_SELFFL;
    }

    public void setWCHINESE_PAPER_OA_Discipline_SELFFL(String WCHINESE_PAPER_OA_Discipline_SELFFL) {
        this.WCHINESE_PAPER_OA_Discipline_SELFFL = WCHINESE_PAPER_OA_Discipline_SELFFL;
    }

    public String getWCHINESE_PAPER_OA_IISSNE() {
        return WCHINESE_PAPER_OA_IISSNE;
    }

    public void setWCHINESE_PAPER_OA_IISSNE(String WCHINESE_PAPER_OA_IISSNE) {
        this.WCHINESE_PAPER_OA_IISSNE = WCHINESE_PAPER_OA_IISSNE;
    }

    public String getIDentifier_ISSNp() {
        return IDentifier_ISSNp;
    }

    public void setIDentifier_ISSNp(String IDentifier_ISSNp) {
        this.IDentifier_ISSNp = IDentifier_ISSNp;
    }

    public String getWCHINESE_PAPER_OA_ZCID() {
        return WCHINESE_PAPER_OA_ZCID;
    }

    public void setWCHINESE_PAPER_OA_ZCID(String WCHINESE_PAPER_OA_ZCID) {
        this.WCHINESE_PAPER_OA_ZCID = WCHINESE_PAPER_OA_ZCID;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getWCHINESE_PAPER_OA_TZCID() {
        return WCHINESE_PAPER_OA_TZCID;
    }

    public void setWCHINESE_PAPER_OA_TZCID(String WCHINESE_PAPER_OA_TZCID) {
        this.WCHINESE_PAPER_OA_TZCID = WCHINESE_PAPER_OA_TZCID;
    }

    public String getWCHINESE_PAPER_OA_SZCID() {
        return WCHINESE_PAPER_OA_SZCID;
    }

    public void setWCHINESE_PAPER_OA_SZCID(String WCHINESE_PAPER_OA_SZCID) {
        this.WCHINESE_PAPER_OA_SZCID = WCHINESE_PAPER_OA_SZCID;
    }

    public String getCls_level3() {
        return Cls_level3;
    }

    public void setCls_level3(String cls_level3) {
        Cls_level3 = cls_level3;
    }

    public String getWCHINESE_PAPER_OA_Class() {
        return WCHINESE_PAPER_OA_Class;
    }

    public void setWCHINESE_PAPER_OA_Class(String WCHINESE_PAPER_OA_Class) {
        this.WCHINESE_PAPER_OA_Class = WCHINESE_PAPER_OA_Class;
    }

    public String getWCHINESE_PAPER_OA_RN() {
        return WCHINESE_PAPER_OA_RN;
    }

    public void setWCHINESE_PAPER_OA_RN(String WCHINESE_PAPER_OA_RN) {
        this.WCHINESE_PAPER_OA_RN = WCHINESE_PAPER_OA_RN;
    }

    public String getWCHINESE_PAPER_OA_BN() {
        return WCHINESE_PAPER_OA_BN;
    }

    public void setWCHINESE_PAPER_OA_BN(String WCHINESE_PAPER_OA_BN) {
        this.WCHINESE_PAPER_OA_BN = WCHINESE_PAPER_OA_BN;
    }

    public String getWCHINESE_PAPER_OA_DN() {
        return WCHINESE_PAPER_OA_DN;
    }

    public void setWCHINESE_PAPER_OA_DN(String WCHINESE_PAPER_OA_DN) {
        this.WCHINESE_PAPER_OA_DN = WCHINESE_PAPER_OA_DN;
    }

    public String getWCHINESE_PAPER_OA_OTS() {
        return WCHINESE_PAPER_OA_OTS;
    }

    public void setWCHINESE_PAPER_OA_OTS(String WCHINESE_PAPER_OA_OTS) {
        this.WCHINESE_PAPER_OA_OTS = WCHINESE_PAPER_OA_OTS;
    }
}
