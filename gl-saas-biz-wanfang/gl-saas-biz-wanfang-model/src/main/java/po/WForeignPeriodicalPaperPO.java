package po;

import annotation.FieldOrder;

//核心资源数据 中文期刊
public class WForeignPeriodicalPaperPO {
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 1)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 2)
    private String 	F_ID;
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	ZHUTI;
    @FieldOrder(order = 5)
    private String 	TITLE;
    @FieldOrder(order = 6)
    private String 	ABSTRACT;
    @FieldOrder(order = 7)
    private String 	AUTHOR;
    @FieldOrder(order = 8)
    private String 	AUTHOR_AUTHOR_ANYNAME;
    @FieldOrder(order = 9)
    private String 	AUTHOR_ANYNAME;
    @FieldOrder(order = 10)
    private String 	KEYWORD;
    @FieldOrder(order = 11)
    private String 	ORG;
    @FieldOrder(order = 12)
    private String 	ORG_ORG_ANYNAME;
    @FieldOrder(order = 13)
    private String 	ORG_ANYNAME;
    @FieldOrder(order = 14)
    private String 	DATE;
    @FieldOrder(order = 15)
    private String 	YEAR;
    @FieldOrder(order = 16)
    private String 	IID;
    @FieldOrder(order = 17)
    private String 	DOI;
    @FieldOrder(order = 18)
    private String 	ISSN;
    @FieldOrder(order = 19)
    private String 	EISSN;
    @FieldOrder(order = 20)
    private String 	AU;
    @FieldOrder(order = 21)
    private String 	JOUCN;
    @FieldOrder(order = 22)
    private String 	FJOUCN;
    @FieldOrder(order = 23)
    private String 	VOL;
    @FieldOrder(order = 24)
    private String 	PER;
    @FieldOrder(order = 25)
    private String 	PG;
    @FieldOrder(order = 26)
    private String 	FORGC;
    @FieldOrder(order = 27)
    private String 	DOWNLOAD;
    @FieldOrder(order = 28)
    private String 	PUBTYPE;
    @FieldOrder(order = 29)
    private String 	YNFREE;
    @FieldOrder(order = 30)
    private String 	CID;
    @FieldOrder(order = 31)
    private String 	ZCID;
    @FieldOrder(order = 32)
    private String 	TZCID;
    @FieldOrder(order = 33)
    private String 	SZCID;
    @FieldOrder(order = 34)
    private String 	DZCID;
    @FieldOrder(order = 35)
    private String 	DID;
    @FieldOrder(order = 36)
    private String 	SCORE;
    @FieldOrder(order = 37)
    private String 	RN;
    @FieldOrder(order = 38)
    private String 	BN;
    @FieldOrder(order = 39)
    private String 	DN;
    @FieldOrder(order = 40)
    private String 	OTS;
    @FieldOrder(order = 41)
    private String 	SCI;
    @FieldOrder(order = 42)
    private String 	EI;
    @FieldOrder(order = 43)
    private String 	FUND;
    @FieldOrder(order = 44)
    private String 	FUND_FUND_ANYNAME;
    @FieldOrder(order = 45)
    private String 	FUND_ANYNAME;
    @FieldOrder(order = 46)
    private String 	FFUND;
    @FieldOrder(order = 47)
    private String 	CORE;
    @FieldOrder(order = 48)
    private String 	DATALINK;
    @FieldOrder(order = 49)
    private String 	FUNDSUPPORT;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getZHUTI() {
        return ZHUTI;
    }

    public void setZHUTI(String ZHUTI) {
        this.ZHUTI = ZHUTI;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAUTHOR_AUTHOR_ANYNAME() {
        return AUTHOR_AUTHOR_ANYNAME;
    }

    public void setAUTHOR_AUTHOR_ANYNAME(String AUTHOR_AUTHOR_ANYNAME) {
        this.AUTHOR_AUTHOR_ANYNAME = AUTHOR_AUTHOR_ANYNAME;
    }

    public String getAUTHOR_ANYNAME() {
        return AUTHOR_ANYNAME;
    }

    public void setAUTHOR_ANYNAME(String AUTHOR_ANYNAME) {
        this.AUTHOR_ANYNAME = AUTHOR_ANYNAME;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getISSN() {
        return ISSN;
    }

    public void setISSN(String ISSN) {
        this.ISSN = ISSN;
    }

    public String getEISSN() {
        return EISSN;
    }

    public void setEISSN(String EISSN) {
        this.EISSN = EISSN;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getJOUCN() {
        return JOUCN;
    }

    public void setJOUCN(String JOUCN) {
        this.JOUCN = JOUCN;
    }

    public String getFJOUCN() {
        return FJOUCN;
    }

    public void setFJOUCN(String FJOUCN) {
        this.FJOUCN = FJOUCN;
    }

    public String getVOL() {
        return VOL;
    }

    public void setVOL(String VOL) {
        this.VOL = VOL;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getPG() {
        return PG;
    }

    public void setPG(String PG) {
        this.PG = PG;
    }

    public String getFORGC() {
        return FORGC;
    }

    public void setFORGC(String FORGC) {
        this.FORGC = FORGC;
    }

    public String getDOWNLOAD() {
        return DOWNLOAD;
    }

    public void setDOWNLOAD(String DOWNLOAD) {
        this.DOWNLOAD = DOWNLOAD;
    }

    public String getPUBTYPE() {
        return PUBTYPE;
    }

    public void setPUBTYPE(String PUBTYPE) {
        this.PUBTYPE = PUBTYPE;
    }

    public String getYNFREE() {
        return YNFREE;
    }

    public void setYNFREE(String YNFREE) {
        this.YNFREE = YNFREE;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getZCID() {
        return ZCID;
    }

    public void setZCID(String ZCID) {
        this.ZCID = ZCID;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getSZCID() {
        return SZCID;
    }

    public void setSZCID(String SZCID) {
        this.SZCID = SZCID;
    }

    public String getDZCID() {
        return DZCID;
    }

    public void setDZCID(String DZCID) {
        this.DZCID = DZCID;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getSCORE() {
        return SCORE;
    }

    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    public String getRN() {
        return RN;
    }

    public void setRN(String RN) {
        this.RN = RN;
    }

    public String getBN() {
        return BN;
    }

    public void setBN(String BN) {
        this.BN = BN;
    }

    public String getDN() {
        return DN;
    }

    public void setDN(String DN) {
        this.DN = DN;
    }

    public String getOTS() {
        return OTS;
    }

    public void setOTS(String OTS) {
        this.OTS = OTS;
    }

    public String getSCI() {
        return SCI;
    }

    public void setSCI(String SCI) {
        this.SCI = SCI;
    }

    public String getEI() {
        return EI;
    }

    public void setEI(String EI) {
        this.EI = EI;
    }

    public String getFUND() {
        return FUND;
    }

    public void setFUND(String FUND) {
        this.FUND = FUND;
    }

    public String getFUND_FUND_ANYNAME() {
        return FUND_FUND_ANYNAME;
    }

    public void setFUND_FUND_ANYNAME(String FUND_FUND_ANYNAME) {
        this.FUND_FUND_ANYNAME = FUND_FUND_ANYNAME;
    }

    public String getFUND_ANYNAME() {
        return FUND_ANYNAME;
    }

    public void setFUND_ANYNAME(String FUND_ANYNAME) {
        this.FUND_ANYNAME = FUND_ANYNAME;
    }

    public String getFFUND() {
        return FFUND;
    }

    public void setFFUND(String FFUND) {
        this.FFUND = FFUND;
    }

    public String getCORE() {
        return CORE;
    }

    public void setCORE(String CORE) {
        this.CORE = CORE;
    }

    public String getDATALINK() {
        return DATALINK;
    }

    public void setDATALINK(String DATALINK) {
        this.DATALINK = DATALINK;
    }

    public String getFUNDSUPPORT() {
        return FUNDSUPPORT;
    }

    public void setFUNDSUPPORT(String FUNDSUPPORT) {
        this.FUNDSUPPORT = FUNDSUPPORT;
    }
}
