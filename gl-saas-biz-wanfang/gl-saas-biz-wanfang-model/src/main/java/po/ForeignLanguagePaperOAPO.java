package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class ForeignLanguagePaperOAPO {
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 1)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 2)
    private String 	OrderID;
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	Title;
    @FieldOrder(order = 5)
    private String 	Abstract;
    @FieldOrder(order = 6)
    private String 	Author;
    @FieldOrder(order = 7)
    private String 	Author_Name;
    @FieldOrder(order = 8)
    private String 	Author_Name2;
    @FieldOrder(order = 9)
    private String 	KeyWords;
    @FieldOrder(order = 10)
    private String 	Cls;
    @FieldOrder(order = 11)
    private String 	ORG_Name;
    @FieldOrder(order = 12)
    private String 	ORG_Name_Search;
    @FieldOrder(order = 13)
    private String 	ORG_Creator;
    @FieldOrder(order = 14)
    private String 	Issued_Date;
    @FieldOrder(order = 15)
    private String 	Issued_Year;
    @FieldOrder(order = 16)
    private String 	IID;
    @FieldOrder(order = 17)
    private String 	DOI;
    @FieldOrder(order = 18)
    private String 	ISSNP;
    @FieldOrder(order = 19)
    private String 	ISSNE;
    @FieldOrder(order = 20)
    private String 	Author_Name3;
    @FieldOrder(order = 21)
    private String 	JOUCN;
    @FieldOrder(order = 22)
    private String 	FJOUCN;
    @FieldOrder(order = 23)
    private String 	Source_Vol;
    @FieldOrder(order = 24)
    private String 	Source_Issue;
    @FieldOrder(order = 25)
    private String 	Source_Page;
    @FieldOrder(order = 26)
    private String 	Creator_ORG;
    @FieldOrder(order = 27)
    private String 	Download_Date;
    @FieldOrder(order = 28)
    private String 	Publisher_Publisher;
    @FieldOrder(order = 29)
    private String 	WCHINESE_PAPER_OA_YNFREE;
    @FieldOrder(order = 30)
    private String 	Data_Link;
    @FieldOrder(order = 31)
    private String 	SELFFL;
    @FieldOrder(order = 32)
    private String 	ZCID;
    @FieldOrder(order = 33)
    private String 	Discipline_Class;
    @FieldOrder(order = 34)
    private String 	TZCID;
    @FieldOrder(order = 35)
    private String 	SZCID;
    @FieldOrder(order = 36)
    private String 	Cls_level3;
    @FieldOrder(order = 37)
    private String 	SCORE;
    @FieldOrder(order = 38)
    private String 	RN;
    @FieldOrder(order = 39)
    private String 	BN;
    @FieldOrder(order = 40)
    private String 	DN;
    @FieldOrder(order = 41)
    private String 	OTS;
    @FieldOrder(order = 42)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getAuthor_Name() {
        return Author_Name;
    }

    public void setAuthor_Name(String author_Name) {
        Author_Name = author_Name;
    }

    public String getAuthor_Name2() {
        return Author_Name2;
    }

    public void setAuthor_Name2(String author_Name2) {
        Author_Name2 = author_Name2;
    }

    public String getKeyWords() {
        return KeyWords;
    }

    public void setKeyWords(String keyWords) {
        KeyWords = keyWords;
    }

    public String getCls() {
        return Cls;
    }

    public void setCls(String cls) {
        Cls = cls;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getORG_Creator() {
        return ORG_Creator;
    }

    public void setORG_Creator(String ORG_Creator) {
        this.ORG_Creator = ORG_Creator;
    }

    public String getIssued_Date() {
        return Issued_Date;
    }

    public void setIssued_Date(String issued_Date) {
        Issued_Date = issued_Date;
    }

    public String getIssued_Year() {
        return Issued_Year;
    }

    public void setIssued_Year(String issued_Year) {
        Issued_Year = issued_Year;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getISSNP() {
        return ISSNP;
    }

    public void setISSNP(String ISSNP) {
        this.ISSNP = ISSNP;
    }

    public String getISSNE() {
        return ISSNE;
    }

    public void setISSNE(String ISSNE) {
        this.ISSNE = ISSNE;
    }

    public String getAuthor_Name3() {
        return Author_Name3;
    }

    public void setAuthor_Name3(String author_Name3) {
        Author_Name3 = author_Name3;
    }

    public String getJOUCN() {
        return JOUCN;
    }

    public void setJOUCN(String JOUCN) {
        this.JOUCN = JOUCN;
    }

    public String getFJOUCN() {
        return FJOUCN;
    }

    public void setFJOUCN(String FJOUCN) {
        this.FJOUCN = FJOUCN;
    }

    public String getSource_Vol() {
        return Source_Vol;
    }

    public void setSource_Vol(String source_Vol) {
        Source_Vol = source_Vol;
    }

    public String getSource_Issue() {
        return Source_Issue;
    }

    public void setSource_Issue(String source_Issue) {
        Source_Issue = source_Issue;
    }

    public String getSource_Page() {
        return Source_Page;
    }

    public void setSource_Page(String source_Page) {
        Source_Page = source_Page;
    }

    public String getCreator_ORG() {
        return Creator_ORG;
    }

    public void setCreator_ORG(String creator_ORG) {
        Creator_ORG = creator_ORG;
    }

    public String getDownload_Date() {
        return Download_Date;
    }

    public void setDownload_Date(String download_Date) {
        Download_Date = download_Date;
    }

    public String getPublisher_Publisher() {
        return Publisher_Publisher;
    }

    public void setPublisher_Publisher(String publisher_Publisher) {
        Publisher_Publisher = publisher_Publisher;
    }

    public String getWCHINESE_PAPER_OA_YNFREE() {
        return WCHINESE_PAPER_OA_YNFREE;
    }

    public void setWCHINESE_PAPER_OA_YNFREE(String WCHINESE_PAPER_OA_YNFREE) {
        this.WCHINESE_PAPER_OA_YNFREE = WCHINESE_PAPER_OA_YNFREE;
    }

    public String getData_Link() {
        return Data_Link;
    }

    public void setData_Link(String data_Link) {
        Data_Link = data_Link;
    }

    public String getSELFFL() {
        return SELFFL;
    }

    public void setSELFFL(String SELFFL) {
        this.SELFFL = SELFFL;
    }

    public String getZCID() {
        return ZCID;
    }

    public void setZCID(String ZCID) {
        this.ZCID = ZCID;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getSZCID() {
        return SZCID;
    }

    public void setSZCID(String SZCID) {
        this.SZCID = SZCID;
    }

    public String getCls_level3() {
        return Cls_level3;
    }

    public void setCls_level3(String cls_level3) {
        Cls_level3 = cls_level3;
    }

    public String getSCORE() {
        return SCORE;
    }

    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    public String getRN() {
        return RN;
    }

    public void setRN(String RN) {
        this.RN = RN;
    }

    public String getBN() {
        return BN;
    }

    public void setBN(String BN) {
        this.BN = BN;
    }

    public String getDN() {
        return DN;
    }

    public void setDN(String DN) {
        this.DN = DN;
    }

    public String getOTS() {
        return OTS;
    }

    public void setOTS(String OTS) {
        this.OTS = OTS;
    }

}
