package po;

import java.util.Date;
import java.util.List;

//协同设计
public class CollaborativeDesignPO {
    //id
    private String id;
    //作者id
    private String authorId;
    //专利所属者
    private String patentHolder;
    //企业id
    private String companyId;
    //配件名称
    private String itemInfoName;
    //配件规格
    private String specs;
    //汽车品牌
    private String carBrand;
    //车型
    private String motorcycleType;
    //专利所属单位
    private String patentOwnedUnits;
    //专利使用类型(1:转移,2:授权,3:邀请设计)
    private String techniqueType;
    //是否自制(0:自制,1:非自制)
    private String isMark;
    //专利编号
    private String patentNo;
    //供应商
    private String supplier;
    //供应商数量
    private Long supplierNum;
    //专利数
    private Long patentsNum;
    //供应商确认状态(0:确认,1:确认)
    private String supplierStatus;
    //创建人
    private String createBy;
    //创建时间
    private Date createAt;
    //更新人
    private String updateBy;
    //更新时间
    private Date updateAt;
    //删除标记(0:正常,1:删除)
    private String delFlag;
    //是否筛选(0:未筛选,1:已筛选)
    private String supplierFilter;

    //是否筛选(ids)
    private List<String> ids;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public String getSupplierFilter() {
        return supplierFilter;
    }

    public void setSupplierFilter(String supplierFilter) {
        this.supplierFilter = supplierFilter;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getPatentHolder() {
        return patentHolder;
    }

    public void setPatentHolder(String patentHolder) {
        this.patentHolder = patentHolder;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getItemInfoName() {
        return itemInfoName;
    }

    public void setItemInfoName(String itemInfoName) {
        this.itemInfoName = itemInfoName;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getMotorcycleType() {
        return motorcycleType;
    }

    public void setMotorcycleType(String motorcycleType) {
        this.motorcycleType = motorcycleType;
    }

    public String getPatentOwnedUnits() {
        return patentOwnedUnits;
    }

    public void setPatentOwnedUnits(String patentOwnedUnits) {
        this.patentOwnedUnits = patentOwnedUnits;
    }

    public String getTechniqueType() {
        return techniqueType;
    }

    public void setTechniqueType(String techniqueType) {
        this.techniqueType = techniqueType;
    }

    public String getIsMark() {
        return isMark;
    }

    public void setIsMark(String isMark) {
        this.isMark = isMark;
    }

    public String getPatentNo() {
        return patentNo;
    }

    public void setPatentNo(String patentNo) {
        this.patentNo = patentNo;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Long getSupplierNum() {
        return supplierNum;
    }

    public void setSupplierNum(Long supplierNum) {
        this.supplierNum = supplierNum;
    }

    public Long getPatentsNum() {
        return patentsNum;
    }

    public void setPatentsNum(Long patentsNum) {
        this.patentsNum = patentsNum;
    }

    public String getSupplierStatus() {
        return supplierStatus;
    }

    public void setSupplierStatus(String supplierStatus) {
        this.supplierStatus = supplierStatus;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}

