package po;



import java.util.Date;


public class IntegrateConfigPO {
    private String id;
    private String tableName;
    private String deweightField;
    private Integer iterationCondition;
    private Date createAt;
    private Date updateAt;

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDeweightField() {
        return deweightField;
    }

    public void setDeweightField(String deweightField) {
        this.deweightField = deweightField;
    }

    public Integer getIterationCondition() {
        return iterationCondition;
    }

    public void setIterationCondition(Integer iterationCondition) {
        this.iterationCondition = iterationCondition;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
