package po;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class ChineseConferencePaperPO {

    @FieldOrder(order = 1)
    private String 	FullText;
    @FieldOrder(order = 2)
    private String 	Topic;
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 3)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }

    @FieldOrder(order = 4)
    private String 	OrderID;
    @FieldOrder(order = 5)
    private String 	Doi;
    @FieldOrder(order = 6)
    private String 	WID;
    @FieldOrder(order = 7)
    private String 	NID;
    @FieldOrder(order = 8)
    private String 	Chinese_Title;
    @FieldOrder(order = 9)
    private String 	English_Title;
    @FieldOrder(order = 10)
    private String 	C_E_Title;
    @FieldOrder(order = 11)
    private String 	Author_ID;
    @FieldOrder(order = 12)
    private String 	Author;
    @FieldOrder(order = 13)
    private String 	Author_Translatio;
    @FieldOrder(order = 14)
    private String 	First_Author_ID;
    @FieldOrder(order = 15)
    private String 	Fau;
    @FieldOrder(order = 16)
    private String 	Faue;
    @FieldOrder(order = 17)
    private String 	T_N_Author;
    @FieldOrder(order = 18)
    private String 	Author_Name;
    @FieldOrder(order = 19)
    private String 	Author_Name2;
    @FieldOrder(order = 20)
    private String 	Author_Unit_Name;
    @FieldOrder(order = 21)
    private String 	Author_Unit_Specification_Name;
    @FieldOrder(order = 22)
    private String 	Author_Unit_Translation;
    @FieldOrder(order = 23)
    private String 	FORG;
    @FieldOrder(order = 24)
    private String 	FORG_Unit_Name;
    @FieldOrder(order = 25)
    private String 	FORGe;
    @FieldOrder(order = 26)
    private String 	ORG_Name;
    @FieldOrder(order = 27)
    private String 	ORG_Name_Search;
    @FieldOrder(order = 28)
    private String 	U_S_Author_Anyname;
    @FieldOrder(order = 29)
    private String 	CID;
    @FieldOrder(order = 30)
    private String 	Machine_Label_Class_Code;
    @FieldOrder(order = 31)
    private String 	Cls_Code;
    @FieldOrder(order = 32)
    private String 	Cls_level2;
    @FieldOrder(order = 33)
    private String 	Cls_Top;
    @FieldOrder(order = 34)
    private String 	Cls_level3;
    @FieldOrder(order = 35)
    private String 	IID;
    @FieldOrder(order = 36)
    private String 	Discipline_Class_Code;
    @FieldOrder(order = 37)
    private String 	Association_Level;
    @FieldOrder(order = 38)
    private String 	Chinese_KeyWord;
    @FieldOrder(order = 39)
    private String 	English_KeyWord;
    @FieldOrder(order = 40)
    private String 	Machine_KeyWord;
    @FieldOrder(order = 41)
    private String 	C_E_Keyword;
    @FieldOrder(order = 42)
    private String 	Chinese_Abstract;
    @FieldOrder(order = 43)
    private String 	English_Abstract;
    @FieldOrder(order = 44)
    private String 	F_Abstract;
    @FieldOrder(order = 45)
    private String 	C_E_Abstract;
    @FieldOrder(order = 46)
    private String 	Lan;
    @FieldOrder(order = 47)
    private String 	Parent_Literature;
    @FieldOrder(order = 48)
    private String 	Parent_Literature_Name;
    @FieldOrder(order = 49)
    private String 	Parent_Literature_Name2;
    @FieldOrder(order = 50)
    private String 	Institute_Name;
    @FieldOrder(order = 51)
    private String 	Institute_Name_Anyname;
    @FieldOrder(order = 52)
    private String 	Sn_Anyname;
    @FieldOrder(order = 53)
    private String 	Conference_Title;
    @FieldOrder(order = 54)
    private String 	Conference_Title_Anyname;
    @FieldOrder(order = 55)
    private String 	Conference_Sessions;
    @FieldOrder(order = 56)
    private String 	Conference_Meeting_Place;
    @FieldOrder(order = 57)
    private String 	HID;
    @FieldOrder(order = 58)
    private String 	ORGanizer;
    @FieldOrder(order = 59)
    private String 	Fund;
    @FieldOrder(order = 60)
    private String 	Fund_FullText_Search;
    @FieldOrder(order = 61)
    private String 	F_Fund;
    @FieldOrder(order = 62)
    private String 	Fpn;
    @FieldOrder(order = 63)
    private String 	Fundsupport;
    @FieldOrder(order = 64)
    private String 	Start_Meeting_Date;
    @FieldOrder(order = 65)
    private String 	End_Meeting_Date;
    @FieldOrder(order = 66)
    private String 	Start_Published_Date;
    @FieldOrder(order = 67)
    private String 	End_Published_Date;
    @FieldOrder(order = 68)
    private String 	Data_Source;
    @FieldOrder(order = 69)
    private String 	ORGnum;
    @FieldOrder(order = 70)
    private String 	Rn;
    @FieldOrder(order = 71)
    private String 	ORG_Hierarchy_ID;
    @FieldOrder(order = 72)
    private String 	Author_Unit_ID;
    @FieldOrder(order = 73)
    private String 	ORG_Province;
    @FieldOrder(order = 74)
    private String 	ORG_City;
    @FieldOrder(order = 75)
    private String 	ORG_Type;
    @FieldOrder(order = 76)
    private String 	FORGID;
    @FieldOrder(order = 77)
    private String 	EndORGtype;
    @FieldOrder(order = 78)
    private String 	ORG_Frist_Final_ID;
    @FieldOrder(order = 79)
    private String 	ORG_Frist_Hierarchy_ID;
    @FieldOrder(order = 80)
    private String 	ORG_Frist_City;
    @FieldOrder(order = 81)
    private String 	ORG_Frist_Final_City;
    @FieldOrder(order = 82)
    private String 	ORG_Frist_Province;
    @FieldOrder(order = 83)
    private String 	ORG_Frist_Province2;
    @FieldOrder(order = 84)
    private String 	ORG_Frist_Final_Type;
    @FieldOrder(order = 85)
    private String 	ORG_Frist_Type;
    @FieldOrder(order = 86)
    private String 	Quarter;
    @FieldOrder(order = 87)
    private String 	Halfyear;
    @FieldOrder(order = 88)
    private String 	Literature_Weight;
    @FieldOrder(order = 89)
    private String 	Error_Code;
    @FieldOrder(order = 90)
    private String 	Author_Info;
    @FieldOrder(order = 91)
    private String 	Author_Info_Name;
    @FieldOrder(order = 92)
    private String 	Author_Info_Order;
    @FieldOrder(order = 93)
    private String 	Author_Info_Unit;
    @FieldOrder(order = 94)
    private String 	Author_Info_Unit_ORG_Level1;
    @FieldOrder(order = 95)
    private String 	Author_Info_Unit_ORG_Level2;
    @FieldOrder(order = 96)
    private String 	Author_Info_Unit_Type;
    @FieldOrder(order = 97)
    private String 	Author_Info_Unit_Province;
    @FieldOrder(order = 98)
    private String 	Author_Info_Unit_City;
    @FieldOrder(order = 99)
    private String 	Author_Info_Unit_County;
    @FieldOrder(order = 100)
    private String 	Author_Info_ID;
    @FieldOrder(order = 101)
    private String 	Author_Info_Unit_ID;
    @FieldOrder(order = 102)
    private String 	ORG_Info;
    @FieldOrder(order = 103)
    private String 	ORG_Info_Name;
    @FieldOrder(order = 104)
    private String 	ORG_Info_Order;
    @FieldOrder(order = 105)
    private String 	ORG_Info_Type;
    @FieldOrder(order = 106)
    private String 	ORG_Info_Province;
    @FieldOrder(order = 107)
    private String 	ORG_Info_City;
    @FieldOrder(order = 108)
    private String 	ORG_Info_County;
    @FieldOrder(order = 109)
    private String 	ORG_Info_Hierarchy;
    @FieldOrder(order = 110)
    private String 	ORG_Info_Level1;
    @FieldOrder(order = 111)
    private String 	ORG_Info_Level2;
    @FieldOrder(order = 112)
    private String 	ORG_Info_Level3;
    @FieldOrder(order = 113)
    private String 	ORG_Info_Level4;
    @FieldOrder(order = 114)
    private String 	ORG_Info_Level5;
    @FieldOrder(order = 115)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String topic) {
        Topic = topic;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getDoi() {
        return Doi;
    }

    public void setDoi(String doi) {
        Doi = doi;
    }

    public String getWID() {
        return WID;
    }

    public void setWID(String WID) {
        this.WID = WID;
    }

    public String getNID() {
        return NID;
    }

    public void setNID(String NID) {
        this.NID = NID;
    }

    public String getChinese_Title() {
        return Chinese_Title;
    }

    public void setChinese_Title(String chinese_Title) {
        Chinese_Title = chinese_Title;
    }

    public String getEnglish_Title() {
        return English_Title;
    }

    public void setEnglish_Title(String english_Title) {
        English_Title = english_Title;
    }

    public String getC_E_Title() {
        return C_E_Title;
    }

    public void setC_E_Title(String c_E_Title) {
        C_E_Title = c_E_Title;
    }

    public String getAuthor_ID() {
        return Author_ID;
    }

    public void setAuthor_ID(String author_ID) {
        Author_ID = author_ID;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getAuthor_Translatio() {
        return Author_Translatio;
    }

    public void setAuthor_Translatio(String author_Translatio) {
        Author_Translatio = author_Translatio;
    }

    public String getFirst_Author_ID() {
        return First_Author_ID;
    }

    public void setFirst_Author_ID(String first_Author_ID) {
        First_Author_ID = first_Author_ID;
    }

    public String getFau() {
        return Fau;
    }

    public void setFau(String fau) {
        Fau = fau;
    }

    public String getFaue() {
        return Faue;
    }

    public void setFaue(String faue) {
        Faue = faue;
    }

    public String getT_N_Author() {
        return T_N_Author;
    }

    public void setT_N_Author(String t_N_Author) {
        T_N_Author = t_N_Author;
    }

    public String getAuthor_Name() {
        return Author_Name;
    }

    public void setAuthor_Name(String author_Name) {
        Author_Name = author_Name;
    }

    public String getAuthor_Name2() {
        return Author_Name2;
    }

    public void setAuthor_Name2(String author_Name2) {
        Author_Name2 = author_Name2;
    }

    public String getAuthor_Unit_Name() {
        return Author_Unit_Name;
    }

    public void setAuthor_Unit_Name(String author_Unit_Name) {
        Author_Unit_Name = author_Unit_Name;
    }

    public String getAuthor_Unit_Specification_Name() {
        return Author_Unit_Specification_Name;
    }

    public void setAuthor_Unit_Specification_Name(String author_Unit_Specification_Name) {
        Author_Unit_Specification_Name = author_Unit_Specification_Name;
    }

    public String getAuthor_Unit_Translation() {
        return Author_Unit_Translation;
    }

    public void setAuthor_Unit_Translation(String author_Unit_Translation) {
        Author_Unit_Translation = author_Unit_Translation;
    }

    public String getFORG() {
        return FORG;
    }

    public void setFORG(String FORG) {
        this.FORG = FORG;
    }

    public String getFORG_Unit_Name() {
        return FORG_Unit_Name;
    }

    public void setFORG_Unit_Name(String FORG_Unit_Name) {
        this.FORG_Unit_Name = FORG_Unit_Name;
    }

    public String getFORGe() {
        return FORGe;
    }

    public void setFORGe(String FORGe) {
        this.FORGe = FORGe;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getU_S_Author_Anyname() {
        return U_S_Author_Anyname;
    }

    public void setU_S_Author_Anyname(String u_S_Author_Anyname) {
        U_S_Author_Anyname = u_S_Author_Anyname;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getMachine_Label_Class_Code() {
        return Machine_Label_Class_Code;
    }

    public void setMachine_Label_Class_Code(String machine_Label_Class_Code) {
        Machine_Label_Class_Code = machine_Label_Class_Code;
    }

    public String getCls_Code() {
        return Cls_Code;
    }

    public void setCls_Code(String cls_Code) {
        Cls_Code = cls_Code;
    }

    public String getCls_level2() {
        return Cls_level2;
    }

    public void setCls_level2(String cls_level2) {
        Cls_level2 = cls_level2;
    }

    public String getCls_Top() {
        return Cls_Top;
    }

    public void setCls_Top(String cls_Top) {
        Cls_Top = cls_Top;
    }

    public String getCls_level3() {
        return Cls_level3;
    }

    public void setCls_level3(String cls_level3) {
        Cls_level3 = cls_level3;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDiscipline_Class_Code() {
        return Discipline_Class_Code;
    }

    public void setDiscipline_Class_Code(String discipline_Class_Code) {
        Discipline_Class_Code = discipline_Class_Code;
    }

    public String getAssociation_Level() {
        return Association_Level;
    }

    public void setAssociation_Level(String association_Level) {
        Association_Level = association_Level;
    }

    public String getChinese_KeyWord() {
        return Chinese_KeyWord;
    }

    public void setChinese_KeyWord(String chinese_KeyWord) {
        Chinese_KeyWord = chinese_KeyWord;
    }

    public String getEnglish_KeyWord() {
        return English_KeyWord;
    }

    public void setEnglish_KeyWord(String english_KeyWord) {
        English_KeyWord = english_KeyWord;
    }

    public String getMachine_KeyWord() {
        return Machine_KeyWord;
    }

    public void setMachine_KeyWord(String machine_KeyWord) {
        Machine_KeyWord = machine_KeyWord;
    }

    public String getC_E_Keyword() {
        return C_E_Keyword;
    }

    public void setC_E_Keyword(String c_E_Keyword) {
        C_E_Keyword = c_E_Keyword;
    }

    public String getChinese_Abstract() {
        return Chinese_Abstract;
    }

    public void setChinese_Abstract(String chinese_Abstract) {
        Chinese_Abstract = chinese_Abstract;
    }

    public String getEnglish_Abstract() {
        return English_Abstract;
    }

    public void setEnglish_Abstract(String english_Abstract) {
        English_Abstract = english_Abstract;
    }

    public String getF_Abstract() {
        return F_Abstract;
    }

    public void setF_Abstract(String f_Abstract) {
        F_Abstract = f_Abstract;
    }

    public String getC_E_Abstract() {
        return C_E_Abstract;
    }

    public void setC_E_Abstract(String c_E_Abstract) {
        C_E_Abstract = c_E_Abstract;
    }

    public String getLan() {
        return Lan;
    }

    public void setLan(String lan) {
        Lan = lan;
    }

    public String getParent_Literature() {
        return Parent_Literature;
    }

    public void setParent_Literature(String parent_Literature) {
        Parent_Literature = parent_Literature;
    }

    public String getParent_Literature_Name() {
        return Parent_Literature_Name;
    }

    public void setParent_Literature_Name(String parent_Literature_Name) {
        Parent_Literature_Name = parent_Literature_Name;
    }

    public String getParent_Literature_Name2() {
        return Parent_Literature_Name2;
    }

    public void setParent_Literature_Name2(String parent_Literature_Name2) {
        Parent_Literature_Name2 = parent_Literature_Name2;
    }

    public String getInstitute_Name() {
        return Institute_Name;
    }

    public void setInstitute_Name(String institute_Name) {
        Institute_Name = institute_Name;
    }

    public String getInstitute_Name_Anyname() {
        return Institute_Name_Anyname;
    }

    public void setInstitute_Name_Anyname(String institute_Name_Anyname) {
        Institute_Name_Anyname = institute_Name_Anyname;
    }

    public String getSn_Anyname() {
        return Sn_Anyname;
    }

    public void setSn_Anyname(String sn_Anyname) {
        Sn_Anyname = sn_Anyname;
    }

    public String getConference_Title() {
        return Conference_Title;
    }

    public void setConference_Title(String conference_Title) {
        Conference_Title = conference_Title;
    }

    public String getConference_Title_Anyname() {
        return Conference_Title_Anyname;
    }

    public void setConference_Title_Anyname(String conference_Title_Anyname) {
        Conference_Title_Anyname = conference_Title_Anyname;
    }

    public String getConference_Sessions() {
        return Conference_Sessions;
    }

    public void setConference_Sessions(String conference_Sessions) {
        Conference_Sessions = conference_Sessions;
    }

    public String getConference_Meeting_Place() {
        return Conference_Meeting_Place;
    }

    public void setConference_Meeting_Place(String conference_Meeting_Place) {
        Conference_Meeting_Place = conference_Meeting_Place;
    }

    public String getHID() {
        return HID;
    }

    public void setHID(String HID) {
        this.HID = HID;
    }

    public String getORGanizer() {
        return ORGanizer;
    }

    public void setORGanizer(String ORGanizer) {
        this.ORGanizer = ORGanizer;
    }

    public String getFund() {
        return Fund;
    }

    public void setFund(String fund) {
        Fund = fund;
    }

    public String getFund_FullText_Search() {
        return Fund_FullText_Search;
    }

    public void setFund_FullText_Search(String fund_FullText_Search) {
        Fund_FullText_Search = fund_FullText_Search;
    }

    public String getF_Fund() {
        return F_Fund;
    }

    public void setF_Fund(String f_Fund) {
        F_Fund = f_Fund;
    }

    public String getFpn() {
        return Fpn;
    }

    public void setFpn(String fpn) {
        Fpn = fpn;
    }

    public String getFundsupport() {
        return Fundsupport;
    }

    public void setFundsupport(String fundsupport) {
        Fundsupport = fundsupport;
    }

    public String getStart_Meeting_Date() {
        return Start_Meeting_Date;
    }

    public void setStart_Meeting_Date(String start_Meeting_Date) {
        Start_Meeting_Date = start_Meeting_Date;
    }

    public String getEnd_Meeting_Date() {
        return End_Meeting_Date;
    }

    public void setEnd_Meeting_Date(String end_Meeting_Date) {
        End_Meeting_Date = end_Meeting_Date;
    }

    public String getStart_Published_Date() {
        return Start_Published_Date;
    }

    public void setStart_Published_Date(String start_Published_Date) {
        Start_Published_Date = start_Published_Date;
    }

    public String getEnd_Published_Date() {
        return End_Published_Date;
    }

    public void setEnd_Published_Date(String end_Published_Date) {
        End_Published_Date = end_Published_Date;
    }

    public String getData_Source() {
        return Data_Source;
    }

    public void setData_Source(String data_Source) {
        Data_Source = data_Source;
    }

    public String getORGnum() {
        return ORGnum;
    }

    public void setORGnum(String ORGnum) {
        this.ORGnum = ORGnum;
    }

    public String getRn() {
        return Rn;
    }

    public void setRn(String rn) {
        Rn = rn;
    }

    public String getORG_Hierarchy_ID() {
        return ORG_Hierarchy_ID;
    }

    public void setORG_Hierarchy_ID(String ORG_Hierarchy_ID) {
        this.ORG_Hierarchy_ID = ORG_Hierarchy_ID;
    }

    public String getAuthor_Unit_ID() {
        return Author_Unit_ID;
    }

    public void setAuthor_Unit_ID(String author_Unit_ID) {
        Author_Unit_ID = author_Unit_ID;
    }

    public String getORG_Province() {
        return ORG_Province;
    }

    public void setORG_Province(String ORG_Province) {
        this.ORG_Province = ORG_Province;
    }

    public String getORG_City() {
        return ORG_City;
    }

    public void setORG_City(String ORG_City) {
        this.ORG_City = ORG_City;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getEndORGtype() {
        return EndORGtype;
    }

    public void setEndORGtype(String endORGtype) {
        EndORGtype = endORGtype;
    }

    public String getORG_Frist_Final_ID() {
        return ORG_Frist_Final_ID;
    }

    public void setORG_Frist_Final_ID(String ORG_Frist_Final_ID) {
        this.ORG_Frist_Final_ID = ORG_Frist_Final_ID;
    }

    public String getORG_Frist_Hierarchy_ID() {
        return ORG_Frist_Hierarchy_ID;
    }

    public void setORG_Frist_Hierarchy_ID(String ORG_Frist_Hierarchy_ID) {
        this.ORG_Frist_Hierarchy_ID = ORG_Frist_Hierarchy_ID;
    }

    public String getORG_Frist_City() {
        return ORG_Frist_City;
    }

    public void setORG_Frist_City(String ORG_Frist_City) {
        this.ORG_Frist_City = ORG_Frist_City;
    }

    public String getORG_Frist_Final_City() {
        return ORG_Frist_Final_City;
    }

    public void setORG_Frist_Final_City(String ORG_Frist_Final_City) {
        this.ORG_Frist_Final_City = ORG_Frist_Final_City;
    }

    public String getORG_Frist_Province() {
        return ORG_Frist_Province;
    }

    public void setORG_Frist_Province(String ORG_Frist_Province) {
        this.ORG_Frist_Province = ORG_Frist_Province;
    }

    public String getORG_Frist_Province2() {
        return ORG_Frist_Province2;
    }

    public void setORG_Frist_Province2(String ORG_Frist_Province2) {
        this.ORG_Frist_Province2 = ORG_Frist_Province2;
    }

    public String getORG_Frist_Final_Type() {
        return ORG_Frist_Final_Type;
    }

    public void setORG_Frist_Final_Type(String ORG_Frist_Final_Type) {
        this.ORG_Frist_Final_Type = ORG_Frist_Final_Type;
    }

    public String getORG_Frist_Type() {
        return ORG_Frist_Type;
    }

    public void setORG_Frist_Type(String ORG_Frist_Type) {
        this.ORG_Frist_Type = ORG_Frist_Type;
    }

    public String getQuarter() {
        return Quarter;
    }

    public void setQuarter(String quarter) {
        Quarter = quarter;
    }

    public String getHalfyear() {
        return Halfyear;
    }

    public void setHalfyear(String halfyear) {
        Halfyear = halfyear;
    }

    public String getLiterature_Weight() {
        return Literature_Weight;
    }

    public void setLiterature_Weight(String literature_Weight) {
        Literature_Weight = literature_Weight;
    }

    public String getError_Code() {
        return Error_Code;
    }

    public void setError_Code(String error_Code) {
        Error_Code = error_Code;
    }

    public String getAuthor_Info() {
        return Author_Info;
    }

    public void setAuthor_Info(String author_Info) {
        Author_Info = author_Info;
    }

    public String getAuthor_Info_Name() {
        return Author_Info_Name;
    }

    public void setAuthor_Info_Name(String author_Info_Name) {
        Author_Info_Name = author_Info_Name;
    }

    public String getAuthor_Info_Order() {
        return Author_Info_Order;
    }

    public void setAuthor_Info_Order(String author_Info_Order) {
        Author_Info_Order = author_Info_Order;
    }

    public String getAuthor_Info_Unit() {
        return Author_Info_Unit;
    }

    public void setAuthor_Info_Unit(String author_Info_Unit) {
        Author_Info_Unit = author_Info_Unit;
    }

    public String getAuthor_Info_Unit_ORG_Level1() {
        return Author_Info_Unit_ORG_Level1;
    }

    public void setAuthor_Info_Unit_ORG_Level1(String author_Info_Unit_ORG_Level1) {
        Author_Info_Unit_ORG_Level1 = author_Info_Unit_ORG_Level1;
    }

    public String getAuthor_Info_Unit_ORG_Level2() {
        return Author_Info_Unit_ORG_Level2;
    }

    public void setAuthor_Info_Unit_ORG_Level2(String author_Info_Unit_ORG_Level2) {
        Author_Info_Unit_ORG_Level2 = author_Info_Unit_ORG_Level2;
    }

    public String getAuthor_Info_Unit_Type() {
        return Author_Info_Unit_Type;
    }

    public void setAuthor_Info_Unit_Type(String author_Info_Unit_Type) {
        Author_Info_Unit_Type = author_Info_Unit_Type;
    }

    public String getAuthor_Info_Unit_Province() {
        return Author_Info_Unit_Province;
    }

    public void setAuthor_Info_Unit_Province(String author_Info_Unit_Province) {
        Author_Info_Unit_Province = author_Info_Unit_Province;
    }

    public String getAuthor_Info_Unit_City() {
        return Author_Info_Unit_City;
    }

    public void setAuthor_Info_Unit_City(String author_Info_Unit_City) {
        Author_Info_Unit_City = author_Info_Unit_City;
    }

    public String getAuthor_Info_Unit_County() {
        return Author_Info_Unit_County;
    }

    public void setAuthor_Info_Unit_County(String author_Info_Unit_County) {
        Author_Info_Unit_County = author_Info_Unit_County;
    }

    public String getAuthor_Info_ID() {
        return Author_Info_ID;
    }

    public void setAuthor_Info_ID(String author_Info_ID) {
        Author_Info_ID = author_Info_ID;
    }

    public String getAuthor_Info_Unit_ID() {
        return Author_Info_Unit_ID;
    }

    public void setAuthor_Info_Unit_ID(String author_Info_Unit_ID) {
        Author_Info_Unit_ID = author_Info_Unit_ID;
    }

    public String getORG_Info() {
        return ORG_Info;
    }

    public void setORG_Info(String ORG_Info) {
        this.ORG_Info = ORG_Info;
    }

    public String getORG_Info_Name() {
        return ORG_Info_Name;
    }

    public void setORG_Info_Name(String ORG_Info_Name) {
        this.ORG_Info_Name = ORG_Info_Name;
    }

    public String getORG_Info_Order() {
        return ORG_Info_Order;
    }

    public void setORG_Info_Order(String ORG_Info_Order) {
        this.ORG_Info_Order = ORG_Info_Order;
    }

    public String getORG_Info_Type() {
        return ORG_Info_Type;
    }

    public void setORG_Info_Type(String ORG_Info_Type) {
        this.ORG_Info_Type = ORG_Info_Type;
    }

    public String getORG_Info_Province() {
        return ORG_Info_Province;
    }

    public void setORG_Info_Province(String ORG_Info_Province) {
        this.ORG_Info_Province = ORG_Info_Province;
    }

    public String getORG_Info_City() {
        return ORG_Info_City;
    }

    public void setORG_Info_City(String ORG_Info_City) {
        this.ORG_Info_City = ORG_Info_City;
    }

    public String getORG_Info_County() {
        return ORG_Info_County;
    }

    public void setORG_Info_County(String ORG_Info_County) {
        this.ORG_Info_County = ORG_Info_County;
    }

    public String getORG_Info_Hierarchy() {
        return ORG_Info_Hierarchy;
    }

    public void setORG_Info_Hierarchy(String ORG_Info_Hierarchy) {
        this.ORG_Info_Hierarchy = ORG_Info_Hierarchy;
    }

    public String getORG_Info_Level1() {
        return ORG_Info_Level1;
    }

    public void setORG_Info_Level1(String ORG_Info_Level1) {
        this.ORG_Info_Level1 = ORG_Info_Level1;
    }

    public String getORG_Info_Level2() {
        return ORG_Info_Level2;
    }

    public void setORG_Info_Level2(String ORG_Info_Level2) {
        this.ORG_Info_Level2 = ORG_Info_Level2;
    }

    public String getORG_Info_Level3() {
        return ORG_Info_Level3;
    }

    public void setORG_Info_Level3(String ORG_Info_Level3) {
        this.ORG_Info_Level3 = ORG_Info_Level3;
    }

    public String getORG_Info_Level4() {
        return ORG_Info_Level4;
    }

    public void setORG_Info_Level4(String ORG_Info_Level4) {
        this.ORG_Info_Level4 = ORG_Info_Level4;
    }

    public String getORG_Info_Level5() {
        return ORG_Info_Level5;
    }

    public void setORG_Info_Level5(String ORG_Info_Level5) {
        this.ORG_Info_Level5 = ORG_Info_Level5;
    }
}


