package po;

import annotation.FieldOrder;

//核心资源数据
public class WTechnologyAchievementsPO {
    @FieldOrder(order = 1)
    private String FullText;
    @FieldOrder(order = 2)
    private String ZHUTI;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 3)
    private String wfId;
    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }

    @FieldOrder(order = 4)
    private String F_ID;
    @FieldOrder(order = 5)
    private String ABSTRACT;
    @FieldOrder(order = 6)
    private String AD;
    @FieldOrder(order = 7)
    private String AID;
    @FieldOrder(order = 8)
    private String AU;
    @FieldOrder(order = 9)
    private String FAU;
    @FieldOrder(order = 10)
    private String AUTHOR;
    @FieldOrder(order = 11)
    private String AW;
    @FieldOrder(order = 12)
    private String AW_AW_ANYNAME;
    @FieldOrder(order = 13)
    private String AW_ANYNAME;
    @FieldOrder(order = 14)
    private String BYIND;
    @FieldOrder(order = 15)
    private String CID;
    @FieldOrder(order = 16)
    private String COMPBY;
    @FieldOrder(order = 17)
    private String COMPID;
    @FieldOrder(order = 18)
    private String CONADR;
    @FieldOrder(order = 19)
    private String CONNAME;
    @FieldOrder(order = 20)
    private String CONTACT;
    @FieldOrder(order = 21)
    private String CONTEL;
    @FieldOrder(order = 22)
    private String COUNTRY;
    @FieldOrder(order = 23)
    private String DATE;
    @FieldOrder(order = 24)
    private String DATE_BEGIN;
    @FieldOrder(order = 25)
    private String DATE_END;
    @FieldOrder(order = 26)
    private String DE;
    @FieldOrder(order = 27)
    private String DID;
    @FieldOrder(order = 28)
    private String DN;
    @FieldOrder(order = 29)
    private String DOP;
    @FieldOrder(order = 30)
    private String DURA;
    @FieldOrder(order = 31)
    private String EE;
    @FieldOrder(order = 32)
    private String EMAIL;
    @FieldOrder(order = 33)
    private String FAX;
    @FieldOrder(order = 34)
    private String FITCLASS;
    @FieldOrder(order = 35)
    private String FITLEVEL;
    @FieldOrder(order = 36)
    private String FORGC;
    @FieldOrder(order = 37)
    private String IAT;
    @FieldOrder(order = 38)
    private String IDT;
    @FieldOrder(order = 39)
    private String IID;
    @FieldOrder(order = 40)
    private String INDR;
    @FieldOrder(order = 41)
    private String ININFO;
    @FieldOrder(order = 42)
    private String INTRO;
    @FieldOrder(order = 43)
    private String KEYWORD;
    @FieldOrder(order = 44)
    private String MEMO;
    @FieldOrder(order = 45)
    private String ONTIME;
    @FieldOrder(order = 46)
    private String ORG;
    @FieldOrder(order = 47)
    private String ORG_ORG_ANYNAME;
    @FieldOrder(order = 48)
    private String ORG_ANYNAME;
    @FieldOrder(order = 49)
    private String ORGC;
    @FieldOrder(order = 50)
    private String ORGTYPE;
    @FieldOrder(order = 51)
    private String PNIO;
    @FieldOrder(order = 52)
    private String PNMS;
    @FieldOrder(order = 53)
    private String PNRE;
    @FieldOrder(order = 54)
    private String PNTG;
    @FieldOrder(order = 55)
    private String PNVE;
    @FieldOrder(order = 56)
    private String PRND;
    @FieldOrder(order = 57)
    private String PROJTI;
    @FieldOrder(order = 58)
    private String PS;
    @FieldOrder(order = 59)
    private String PTE;
    @FieldOrder(order = 60)
    private String PTS;
    @FieldOrder(order = 61)
    private String PUBINFO;
    @FieldOrder(order = 62)
    private String PW;
    @FieldOrder(order = 63)
    private String RE;
    @FieldOrder(order = 64)
    private String RECTYPE;
    @FieldOrder(order = 65)
    private String REE;
    @FieldOrder(order = 66)
    private String RESTRI;
    @FieldOrder(order = 67)
    private String RR;
    @FieldOrder(order = 68)
    private String RSS;
    @FieldOrder(order = 69)
    private String RT;
    @FieldOrder(order = 70)
    private String RTT;
    @FieldOrder(order = 71)
    private String RUIT;
    @FieldOrder(order = 72)
    private String SCOMP;
    @FieldOrder(order = 73)
    private String SOURCE;
    @FieldOrder(order = 74)
    private String SY;
    @FieldOrder(order = 75)
    private String SZCID;
    @FieldOrder(order = 76)
    private String DZCID;
    @FieldOrder(order = 77)
    private String TC;
    @FieldOrder(order = 78)
    private String TF;
    @FieldOrder(order = 79)
    private String TIFO;
    @FieldOrder(order = 80)
    private String TITLE;
    @FieldOrder(order = 81)
    private String TM;
    @FieldOrder(order = 82)
    private String ISCHARGE;
    @FieldOrder(order = 83)
    private String RD;
    @FieldOrder(order = 84)
    private String RUD;
    @FieldOrder(order = 85)
    private String TME;
    @FieldOrder(order = 86)
    private String TR;
    @FieldOrder(order = 87)
    private String TS;
    @FieldOrder(order = 88)
    private String TZCID;
    @FieldOrder(order = 89)
    private String YEAR;
    @FieldOrder(order = 90)
    private String ZCID;
    @FieldOrder(order = 91)
    private String ZIPCODE;
    @FieldOrder(order = 92)
    private String _ID;
    @FieldOrder(order = 93)
    private String CKEY;
    @FieldOrder(order = 94)
    private String ORGID;
    @FieldOrder(order = 95)
    private String ORGSTRUCID;
    @FieldOrder(order = 96)
    private String ORGPROVINCE;
    @FieldOrder(order = 97)
    private String ORGCITY;
    @FieldOrder(order = 98)
    private String ENDORGTYPE;
    @FieldOrder(order = 99)
    private String FORGID;
    @FieldOrder(order = 100)
    private String FENDORGID;
    @FieldOrder(order = 101)
    private String FORGSTRUCID;
    @FieldOrder(order = 102)
    private String FENDORGPROVINCE;
    @FieldOrder(order = 103)
    private String FORGPROVINCE;
    @FieldOrder(order = 104)
    private String FENDORGCITY;
    @FieldOrder(order = 105)
    private String FORGCITY;
    @FieldOrder(order = 106)
    private String FENDORGTYPE;
    @FieldOrder(order = 107)
    private String FORGTYPE;
    @FieldOrder(order = 108)
    private String AWSHORT;
    @FieldOrder(order = 109)
    private String AWLEVEL;
    @FieldOrder(order = 110)
    private String QUARTER;
    @FieldOrder(order = 111)
    private String HALFYEAR;
    @FieldOrder(order = 112)
    private String WEIGHT;
    @FieldOrder(order = 113)
    private String ErrCode;
    @FieldOrder(order = 114)
    private String AUTHORINFO;
    @FieldOrder(order = 115)
    private String AUTHORINFO_NAME;
    @FieldOrder(order = 116)
    private String AUTHORINFO_CX;
    @FieldOrder(order = 117)
    private String AUTHORINFO_ORG;
    @FieldOrder(order = 118)
    private String AUTHORINFO_FUNIT;
    @FieldOrder(order = 119)
    private String AUTHORINFO_SUNIT;
    @FieldOrder(order = 120)
    private String AUTHORINFO_JGLX;
    @FieldOrder(order = 121)
    private String AUTHORINFO_ORGPROVINCE;
    @FieldOrder(order = 122)
    private String AUTHORINFO_ORGCITY;
    @FieldOrder(order = 123)
    private String AUTHORINFO_ORGCOUNTRY;
    @FieldOrder(order = 124)
    private String AUTHORINFO_AUID;
    @FieldOrder(order = 125)
    private String AUTHORINFO_ORGID;
    @FieldOrder(order = 126)
    private String ORGINFO;
    @FieldOrder(order = 127)
    private String ORGINFO_ORG;
    @FieldOrder(order = 128)
    private String ORGINFO_CX;
    @FieldOrder(order = 129)
    private String ORGINFO_ORGTYPE;
    @FieldOrder(order = 130)
    private String ORGINFO_SHENG;
    @FieldOrder(order = 131)
    private String ORGINFO_SHI;
    @FieldOrder(order = 132)
    private String ORGINFO_XIAN;
    @FieldOrder(order = 133)
    private String ORGINFO_RELATION;
    @FieldOrder(order = 134)
    private String ORGINFO_ORGLEVEL1;
    @FieldOrder(order = 135)
    private String ORGINFO_ORGLEVEL2;
    @FieldOrder(order = 136)
    private String ORGINFO_ORGLEVEL3;
    @FieldOrder(order = 137)
    private String ORGINFO_ORGLEVEL4;
    @FieldOrder(order = 138)
    private String ORGINFO_ORGLEVEL5;

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getZHUTI() {
        return ZHUTI;
    }

    public void setZHUTI(String ZHUTI) {
        this.ZHUTI = ZHUTI;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getAD() {
        return AD;
    }

    public void setAD(String AD) {
        this.AD = AD;
    }

    public String getAID() {
        return AID;
    }

    public void setAID(String AID) {
        this.AID = AID;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getFAU() {
        return FAU;
    }

    public void setFAU(String FAU) {
        this.FAU = FAU;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAW() {
        return AW;
    }

    public void setAW(String AW) {
        this.AW = AW;
    }

    public String getAW_AW_ANYNAME() {
        return AW_AW_ANYNAME;
    }

    public void setAW_AW_ANYNAME(String AW_AW_ANYNAME) {
        this.AW_AW_ANYNAME = AW_AW_ANYNAME;
    }

    public String getAW_ANYNAME() {
        return AW_ANYNAME;
    }

    public void setAW_ANYNAME(String AW_ANYNAME) {
        this.AW_ANYNAME = AW_ANYNAME;
    }

    public String getBYIND() {
        return BYIND;
    }

    public void setBYIND(String BYIND) {
        this.BYIND = BYIND;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getCOMPBY() {
        return COMPBY;
    }

    public void setCOMPBY(String COMPBY) {
        this.COMPBY = COMPBY;
    }

    public String getCOMPID() {
        return COMPID;
    }

    public void setCOMPID(String COMPID) {
        this.COMPID = COMPID;
    }

    public String getCONADR() {
        return CONADR;
    }

    public void setCONADR(String CONADR) {
        this.CONADR = CONADR;
    }

    public String getCONNAME() {
        return CONNAME;
    }

    public void setCONNAME(String CONNAME) {
        this.CONNAME = CONNAME;
    }

    public String getCONTACT() {
        return CONTACT;
    }

    public void setCONTACT(String CONTACT) {
        this.CONTACT = CONTACT;
    }

    public String getCONTEL() {
        return CONTEL;
    }

    public void setCONTEL(String CONTEL) {
        this.CONTEL = CONTEL;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getDATE_BEGIN() {
        return DATE_BEGIN;
    }

    public void setDATE_BEGIN(String DATE_BEGIN) {
        this.DATE_BEGIN = DATE_BEGIN;
    }

    public String getDATE_END() {
        return DATE_END;
    }

    public void setDATE_END(String DATE_END) {
        this.DATE_END = DATE_END;
    }

    public String getDE() {
        return DE;
    }

    public void setDE(String DE) {
        this.DE = DE;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getDN() {
        return DN;
    }

    public void setDN(String DN) {
        this.DN = DN;
    }

    public String getDOP() {
        return DOP;
    }

    public void setDOP(String DOP) {
        this.DOP = DOP;
    }

    public String getDURA() {
        return DURA;
    }

    public void setDURA(String DURA) {
        this.DURA = DURA;
    }

    public String getEE() {
        return EE;
    }

    public void setEE(String EE) {
        this.EE = EE;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getFITCLASS() {
        return FITCLASS;
    }

    public void setFITCLASS(String FITCLASS) {
        this.FITCLASS = FITCLASS;
    }

    public String getFITLEVEL() {
        return FITLEVEL;
    }

    public void setFITLEVEL(String FITLEVEL) {
        this.FITLEVEL = FITLEVEL;
    }

    public String getFORGC() {
        return FORGC;
    }

    public void setFORGC(String FORGC) {
        this.FORGC = FORGC;
    }

    public String getIAT() {
        return IAT;
    }

    public void setIAT(String IAT) {
        this.IAT = IAT;
    }

    public String getIDT() {
        return IDT;
    }

    public void setIDT(String IDT) {
        this.IDT = IDT;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getINDR() {
        return INDR;
    }

    public void setINDR(String INDR) {
        this.INDR = INDR;
    }

    public String getININFO() {
        return ININFO;
    }

    public void setININFO(String ININFO) {
        this.ININFO = ININFO;
    }

    public String getINTRO() {
        return INTRO;
    }

    public void setINTRO(String INTRO) {
        this.INTRO = INTRO;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getMEMO() {
        return MEMO;
    }

    public void setMEMO(String MEMO) {
        this.MEMO = MEMO;
    }

    public String getONTIME() {
        return ONTIME;
    }

    public void setONTIME(String ONTIME) {
        this.ONTIME = ONTIME;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getORGC() {
        return ORGC;
    }

    public void setORGC(String ORGC) {
        this.ORGC = ORGC;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getPNIO() {
        return PNIO;
    }

    public void setPNIO(String PNIO) {
        this.PNIO = PNIO;
    }

    public String getPNMS() {
        return PNMS;
    }

    public void setPNMS(String PNMS) {
        this.PNMS = PNMS;
    }

    public String getPNRE() {
        return PNRE;
    }

    public void setPNRE(String PNRE) {
        this.PNRE = PNRE;
    }

    public String getPNTG() {
        return PNTG;
    }

    public void setPNTG(String PNTG) {
        this.PNTG = PNTG;
    }

    public String getPNVE() {
        return PNVE;
    }

    public void setPNVE(String PNVE) {
        this.PNVE = PNVE;
    }

    public String getPRND() {
        return PRND;
    }

    public void setPRND(String PRND) {
        this.PRND = PRND;
    }

    public String getPROJTI() {
        return PROJTI;
    }

    public void setPROJTI(String PROJTI) {
        this.PROJTI = PROJTI;
    }

    public String getPS() {
        return PS;
    }

    public void setPS(String PS) {
        this.PS = PS;
    }

    public String getPTE() {
        return PTE;
    }

    public void setPTE(String PTE) {
        this.PTE = PTE;
    }

    public String getPTS() {
        return PTS;
    }

    public void setPTS(String PTS) {
        this.PTS = PTS;
    }

    public String getPUBINFO() {
        return PUBINFO;
    }

    public void setPUBINFO(String PUBINFO) {
        this.PUBINFO = PUBINFO;
    }

    public String getPW() {
        return PW;
    }

    public void setPW(String PW) {
        this.PW = PW;
    }

    public String getRE() {
        return RE;
    }

    public void setRE(String RE) {
        this.RE = RE;
    }

    public String getRECTYPE() {
        return RECTYPE;
    }

    public void setRECTYPE(String RECTYPE) {
        this.RECTYPE = RECTYPE;
    }

    public String getREE() {
        return REE;
    }

    public void setREE(String REE) {
        this.REE = REE;
    }

    public String getRESTRI() {
        return RESTRI;
    }

    public void setRESTRI(String RESTRI) {
        this.RESTRI = RESTRI;
    }

    public String getRR() {
        return RR;
    }

    public void setRR(String RR) {
        this.RR = RR;
    }

    public String getRSS() {
        return RSS;
    }

    public void setRSS(String RSS) {
        this.RSS = RSS;
    }

    public String getRT() {
        return RT;
    }

    public void setRT(String RT) {
        this.RT = RT;
    }

    public String getRTT() {
        return RTT;
    }

    public void setRTT(String RTT) {
        this.RTT = RTT;
    }

    public String getRUIT() {
        return RUIT;
    }

    public void setRUIT(String RUIT) {
        this.RUIT = RUIT;
    }

    public String getSCOMP() {
        return SCOMP;
    }

    public void setSCOMP(String SCOMP) {
        this.SCOMP = SCOMP;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    public String getSY() {
        return SY;
    }

    public void setSY(String SY) {
        this.SY = SY;
    }

    public String getSZCID() {
        return SZCID;
    }

    public void setSZCID(String SZCID) {
        this.SZCID = SZCID;
    }

    public String getDZCID() {
        return DZCID;
    }

    public void setDZCID(String DZCID) {
        this.DZCID = DZCID;
    }

    public String getTC() {
        return TC;
    }

    public void setTC(String TC) {
        this.TC = TC;
    }

    public String getTF() {
        return TF;
    }

    public void setTF(String TF) {
        this.TF = TF;
    }

    public String getTIFO() {
        return TIFO;
    }

    public void setTIFO(String TIFO) {
        this.TIFO = TIFO;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getTM() {
        return TM;
    }

    public void setTM(String TM) {
        this.TM = TM;
    }

    public String getISCHARGE() {
        return ISCHARGE;
    }

    public void setISCHARGE(String ISCHARGE) {
        this.ISCHARGE = ISCHARGE;
    }

    public String getRD() {
        return RD;
    }

    public void setRD(String RD) {
        this.RD = RD;
    }

    public String getRUD() {
        return RUD;
    }

    public void setRUD(String RUD) {
        this.RUD = RUD;
    }

    public String getTME() {
        return TME;
    }

    public void setTME(String TME) {
        this.TME = TME;
    }

    public String getTR() {
        return TR;
    }

    public void setTR(String TR) {
        this.TR = TR;
    }

    public String getTS() {
        return TS;
    }

    public void setTS(String TS) {
        this.TS = TS;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getZCID() {
        return ZCID;
    }

    public void setZCID(String ZCID) {
        this.ZCID = ZCID;
    }

    public String getZIPCODE() {
        return ZIPCODE;
    }

    public void setZIPCODE(String ZIPCODE) {
        this.ZIPCODE = ZIPCODE;
    }

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getCKEY() {
        return CKEY;
    }

    public void setCKEY(String CKEY) {
        this.CKEY = CKEY;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getORGSTRUCID() {
        return ORGSTRUCID;
    }

    public void setORGSTRUCID(String ORGSTRUCID) {
        this.ORGSTRUCID = ORGSTRUCID;
    }

    public String getORGPROVINCE() {
        return ORGPROVINCE;
    }

    public void setORGPROVINCE(String ORGPROVINCE) {
        this.ORGPROVINCE = ORGPROVINCE;
    }

    public String getORGCITY() {
        return ORGCITY;
    }

    public void setORGCITY(String ORGCITY) {
        this.ORGCITY = ORGCITY;
    }

    public String getENDORGTYPE() {
        return ENDORGTYPE;
    }

    public void setENDORGTYPE(String ENDORGTYPE) {
        this.ENDORGTYPE = ENDORGTYPE;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getFENDORGID() {
        return FENDORGID;
    }

    public void setFENDORGID(String FENDORGID) {
        this.FENDORGID = FENDORGID;
    }

    public String getFORGSTRUCID() {
        return FORGSTRUCID;
    }

    public void setFORGSTRUCID(String FORGSTRUCID) {
        this.FORGSTRUCID = FORGSTRUCID;
    }

    public String getFENDORGPROVINCE() {
        return FENDORGPROVINCE;
    }

    public void setFENDORGPROVINCE(String FENDORGPROVINCE) {
        this.FENDORGPROVINCE = FENDORGPROVINCE;
    }

    public String getFORGPROVINCE() {
        return FORGPROVINCE;
    }

    public void setFORGPROVINCE(String FORGPROVINCE) {
        this.FORGPROVINCE = FORGPROVINCE;
    }

    public String getFENDORGCITY() {
        return FENDORGCITY;
    }

    public void setFENDORGCITY(String FENDORGCITY) {
        this.FENDORGCITY = FENDORGCITY;
    }

    public String getFORGCITY() {
        return FORGCITY;
    }

    public void setFORGCITY(String FORGCITY) {
        this.FORGCITY = FORGCITY;
    }

    public String getFENDORGTYPE() {
        return FENDORGTYPE;
    }

    public void setFENDORGTYPE(String FENDORGTYPE) {
        this.FENDORGTYPE = FENDORGTYPE;
    }

    public String getFORGTYPE() {
        return FORGTYPE;
    }

    public void setFORGTYPE(String FORGTYPE) {
        this.FORGTYPE = FORGTYPE;
    }

    public String getAWSHORT() {
        return AWSHORT;
    }

    public void setAWSHORT(String AWSHORT) {
        this.AWSHORT = AWSHORT;
    }

    public String getAWLEVEL() {
        return AWLEVEL;
    }

    public void setAWLEVEL(String AWLEVEL) {
        this.AWLEVEL = AWLEVEL;
    }

    public String getQUARTER() {
        return QUARTER;
    }

    public void setQUARTER(String QUARTER) {
        this.QUARTER = QUARTER;
    }

    public String getHALFYEAR() {
        return HALFYEAR;
    }

    public void setHALFYEAR(String HALFYEAR) {
        this.HALFYEAR = HALFYEAR;
    }

    public String getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(String WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getAUTHORINFO() {
        return AUTHORINFO;
    }

    public void setAUTHORINFO(String AUTHORINFO) {
        this.AUTHORINFO = AUTHORINFO;
    }

    public String getAUTHORINFO_NAME() {
        return AUTHORINFO_NAME;
    }

    public void setAUTHORINFO_NAME(String AUTHORINFO_NAME) {
        this.AUTHORINFO_NAME = AUTHORINFO_NAME;
    }

    public String getAUTHORINFO_CX() {
        return AUTHORINFO_CX;
    }

    public void setAUTHORINFO_CX(String AUTHORINFO_CX) {
        this.AUTHORINFO_CX = AUTHORINFO_CX;
    }

    public String getAUTHORINFO_ORG() {
        return AUTHORINFO_ORG;
    }

    public void setAUTHORINFO_ORG(String AUTHORINFO_ORG) {
        this.AUTHORINFO_ORG = AUTHORINFO_ORG;
    }

    public String getAUTHORINFO_FUNIT() {
        return AUTHORINFO_FUNIT;
    }

    public void setAUTHORINFO_FUNIT(String AUTHORINFO_FUNIT) {
        this.AUTHORINFO_FUNIT = AUTHORINFO_FUNIT;
    }

    public String getAUTHORINFO_SUNIT() {
        return AUTHORINFO_SUNIT;
    }

    public void setAUTHORINFO_SUNIT(String AUTHORINFO_SUNIT) {
        this.AUTHORINFO_SUNIT = AUTHORINFO_SUNIT;
    }

    public String getAUTHORINFO_JGLX() {
        return AUTHORINFO_JGLX;
    }

    public void setAUTHORINFO_JGLX(String AUTHORINFO_JGLX) {
        this.AUTHORINFO_JGLX = AUTHORINFO_JGLX;
    }

    public String getAUTHORINFO_ORGPROVINCE() {
        return AUTHORINFO_ORGPROVINCE;
    }

    public void setAUTHORINFO_ORGPROVINCE(String AUTHORINFO_ORGPROVINCE) {
        this.AUTHORINFO_ORGPROVINCE = AUTHORINFO_ORGPROVINCE;
    }

    public String getAUTHORINFO_ORGCITY() {
        return AUTHORINFO_ORGCITY;
    }

    public void setAUTHORINFO_ORGCITY(String AUTHORINFO_ORGCITY) {
        this.AUTHORINFO_ORGCITY = AUTHORINFO_ORGCITY;
    }

    public String getAUTHORINFO_ORGCOUNTRY() {
        return AUTHORINFO_ORGCOUNTRY;
    }

    public void setAUTHORINFO_ORGCOUNTRY(String AUTHORINFO_ORGCOUNTRY) {
        this.AUTHORINFO_ORGCOUNTRY = AUTHORINFO_ORGCOUNTRY;
    }

    public String getAUTHORINFO_AUID() {
        return AUTHORINFO_AUID;
    }

    public void setAUTHORINFO_AUID(String AUTHORINFO_AUID) {
        this.AUTHORINFO_AUID = AUTHORINFO_AUID;
    }

    public String getAUTHORINFO_ORGID() {
        return AUTHORINFO_ORGID;
    }

    public void setAUTHORINFO_ORGID(String AUTHORINFO_ORGID) {
        this.AUTHORINFO_ORGID = AUTHORINFO_ORGID;
    }

    public String getORGINFO() {
        return ORGINFO;
    }

    public void setORGINFO(String ORGINFO) {
        this.ORGINFO = ORGINFO;
    }

    public String getORGINFO_ORG() {
        return ORGINFO_ORG;
    }

    public void setORGINFO_ORG(String ORGINFO_ORG) {
        this.ORGINFO_ORG = ORGINFO_ORG;
    }

    public String getORGINFO_CX() {
        return ORGINFO_CX;
    }

    public void setORGINFO_CX(String ORGINFO_CX) {
        this.ORGINFO_CX = ORGINFO_CX;
    }

    public String getORGINFO_ORGTYPE() {
        return ORGINFO_ORGTYPE;
    }

    public void setORGINFO_ORGTYPE(String ORGINFO_ORGTYPE) {
        this.ORGINFO_ORGTYPE = ORGINFO_ORGTYPE;
    }

    public String getORGINFO_SHENG() {
        return ORGINFO_SHENG;
    }

    public void setORGINFO_SHENG(String ORGINFO_SHENG) {
        this.ORGINFO_SHENG = ORGINFO_SHENG;
    }

    public String getORGINFO_SHI() {
        return ORGINFO_SHI;
    }

    public void setORGINFO_SHI(String ORGINFO_SHI) {
        this.ORGINFO_SHI = ORGINFO_SHI;
    }

    public String getORGINFO_XIAN() {
        return ORGINFO_XIAN;
    }

    public void setORGINFO_XIAN(String ORGINFO_XIAN) {
        this.ORGINFO_XIAN = ORGINFO_XIAN;
    }

    public String getORGINFO_RELATION() {
        return ORGINFO_RELATION;
    }

    public void setORGINFO_RELATION(String ORGINFO_RELATION) {
        this.ORGINFO_RELATION = ORGINFO_RELATION;
    }

    public String getORGINFO_ORGLEVEL1() {
        return ORGINFO_ORGLEVEL1;
    }

    public void setORGINFO_ORGLEVEL1(String ORGINFO_ORGLEVEL1) {
        this.ORGINFO_ORGLEVEL1 = ORGINFO_ORGLEVEL1;
    }

    public String getORGINFO_ORGLEVEL2() {
        return ORGINFO_ORGLEVEL2;
    }

    public void setORGINFO_ORGLEVEL2(String ORGINFO_ORGLEVEL2) {
        this.ORGINFO_ORGLEVEL2 = ORGINFO_ORGLEVEL2;
    }

    public String getORGINFO_ORGLEVEL3() {
        return ORGINFO_ORGLEVEL3;
    }

    public void setORGINFO_ORGLEVEL3(String ORGINFO_ORGLEVEL3) {
        this.ORGINFO_ORGLEVEL3 = ORGINFO_ORGLEVEL3;
    }

    public String getORGINFO_ORGLEVEL4() {
        return ORGINFO_ORGLEVEL4;
    }

    public void setORGINFO_ORGLEVEL4(String ORGINFO_ORGLEVEL4) {
        this.ORGINFO_ORGLEVEL4 = ORGINFO_ORGLEVEL4;
    }

    public String getORGINFO_ORGLEVEL5() {
        return ORGINFO_ORGLEVEL5;
    }

    public void setORGINFO_ORGLEVEL5(String ORGINFO_ORGLEVEL5) {
        this.ORGINFO_ORGLEVEL5 = ORGINFO_ORGLEVEL5;
    }
}
