package en;

import java.util.ArrayList;
import java.util.List;

//专领域分类
public enum EIPCClassify {
    A("A","人类生活必需","a_chinese_patent"),
    B("B","作业；运输","b_chinese_patent"),
    C("C","化学；冶金","c_chinese_patent"),
    D("D","纺织；造纸","d_chinese_patent"),
    E("E","固定建筑物","" + "e_chinese_patent"),
    F("F","机械工程；照明；加热；武器；爆破","f_chinese_patent"),
    G("G","物理","g_chinese_patent"),
    H("H","电学","h_chinese_patent"),
    UNKNOW("unknow", "未知", "u_chinese_patent"),
    ;
    public String classify;
    public String explain;
    public String dataBase;

    EIPCClassify(String classify, String explain, String dataBase) {
        this.classify = classify;
        this.explain = explain;
        this.dataBase=dataBase;
    }

    public static List<String> keyList() {
        List<String> list = new ArrayList<>();
        for (EIPCClassify e : EIPCClassify.values()) {
            list.add(e.classify);
        }
        return list;
    }

    public static String getDataBase(String classify) {
        for (EIPCClassify e : EIPCClassify.values()) {
            if (e.classify.equals(classify)) {
                return e.dataBase;
            }
        }
        return null;
    }
}
