package en;

import com.google.common.collect.Lists;

import java.util.List;

//中图分类
public enum  ECLSClassify {
    A("a", "马克思主义、列宁主义、毛泽东思想、邓小平理论"),
    B("b", "哲学、宗教"),
    C("c", "社会科学总论"),
    D("d", "政治、法律"),
    E("e", "军事"),
    F("f", "经济"),
    G("g", "文化、科学、教育、体育"),
    H("h", "语言、文字"),
    I("i", "文学"),
    J("j", "艺术"),
    K("k", "历史、地理"),
    N("n", "自然科学总论"),
    O("o", "数理科学和化学"),
    P("p", "天文学、地球科学"),
    Q("q", "生物科学"),
    R("r", "医药、卫生"),
    S("s", "农业科学"),
    T("t", "工业技术"),
    U("u", "交通运输"),
    V("v", "航空、航天"),
    X("x", "环境科学、安全科学"),
    Z("z", "综合性图书")
    ;

    public String classify;
    public String explain;

    ECLSClassify(String classify, String explain) {
        this.classify = classify;
        this.explain = explain;
    }

    public static List<String> keyList() {
        List<String> list= Lists.newArrayList();
        for (ECLSClassify e : ECLSClassify.values()) {
            list.add(e.classify);
        }
        return list;
    }
}
