package en;

import java.util.ArrayList;
import java.util.List;

//行业分类
public enum ESICClassify {
    A("a","01", "05", "农、林、牧、渔业"),
    B("b","06", "12", "采矿业"),
    C("c","13", "43", "制造业"),
    D("d","44", "46", "电力、燃气及水的生产和供应业"),
    E("e","47", "50", "建筑业"),
    F("f","51", "52", "批发和零售业"),
    G("g","53", "60", "交通运输、仓储和邮政业"),
    H("h","61", "62", "住宿和餐饮业"),
    I("i","63", "65", "信息传输、软件和信息技术服务业"),
    J("j","66", "69", "金融业"),
    K("k","70", "70", "房地产业 "),
    L("l","71", "72", "租赁和商务服务业"),
    M("m","73", "75", "科学研究和技术服务业"),
    N("n","76", "79", "水利,环境和公共设施管理业"),
    O("o", "80", "82", "居民服务,修理和其他服务业"),
    P("p", "83", "83", "教育"),
    Q("q", "84", "85", "卫生和社会工作"),
    R("r","86","90","文化体育和娱乐业"),
    S("s", "91", "96", "公共管理,社会保障和社会组织"),
    T("t", "97", "97", "国际组织"),
    ;

    public String key;
    public String begin;
    public String end;
    public String explain;

    ESICClassify(String key, String begin, String end, String explain) {
        this.key = key;
        this.begin = begin;
        this.end = end;
        this.explain = explain;
    }

    public static String getKey(String num) {
        for (ESICClassify e : ESICClassify.values()) {
            if (Integer.valueOf(e.begin) <= Integer.valueOf(num)&& Integer.valueOf(num)<= Integer.valueOf(e.end)) {
                return e.key;
            }
        }
        return "u";
    }

    public static Boolean contains(String num) {
        for (ESICClassify e : ESICClassify.values()) {
            if (Integer.valueOf(e.begin) <= Integer.valueOf(num)&& Integer.valueOf(num)<= Integer.valueOf(e.end)) {
                return true;
            }
        }
        return false;
    }

    public static List<String> keyList() {
        List<String> list = new ArrayList<>();
        for (ESICClassify e : ESICClassify.values()) {
            list.add(e.key);
        }
        return list;
    }
}
