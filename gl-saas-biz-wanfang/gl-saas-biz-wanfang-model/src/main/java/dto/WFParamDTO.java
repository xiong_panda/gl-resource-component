package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("WFParamDTO")
public class WFParamDTO {

    @ApiModelProperty("索引库码值")
    private String indexName;
    @ApiModelProperty("索引条件")
    private String filter;
    @ApiModelProperty("excludes")
    private String excludes;
    @ApiModelProperty("起始条数")
    private String from;
    @ApiModelProperty("返回条数限制")
    private String size;
    @ApiModelProperty("不包括")
    private String mustNot;
    @ApiModelProperty("xx字段")
    private String noExistFields;
    @ApiModelProperty("搜索关键字")
    private String range;
    @ApiModelProperty("排序")
    private String scroll;
    @ApiModelProperty("排序码值")
    private String scrollId;
    @ApiModelProperty("existFields")
    private String existFields;
    @ApiModelProperty("must")
    private String must;
    @ApiModelProperty("gn")
    private String gn;
    @ApiModelProperty("should")
    private String should;
    @ApiModelProperty("aggFields")
    private String aggFields;
    @ApiModelProperty("subAggFields")
    private String subAggFields;
    @ApiModelProperty("aggSize")
    private String aggSize;
    @ApiModelProperty("subAggSize")
    private String subAggSize;
    @ApiModelProperty("format")
    private String format;
    @ApiModelProperty("sortField")
    private String sortField;
    @ApiModelProperty("ids")
    private String ids;

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getExcludes() {
        return excludes;
    }

    public void setExcludes(String excludes) {
        this.excludes = excludes;
    }

    public String getFrom() {
        return from==null?"0":from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSize() {
        return size==null?"10":size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMustNot() {
        return mustNot;
    }

    public void setMustNot(String mustNot) {
        this.mustNot = mustNot;
    }

    public String getNoExistFields() {
        return noExistFields;
    }

    public void setNoExistFields(String noExistFields) {
        this.noExistFields = noExistFields;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getScroll() {
        return scroll;
    }

    public void setScroll(String scroll) {
        this.scroll = scroll;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }

    public String getExistFields() {
        return existFields;
    }

    public void setExistFields(String existFields) {
        this.existFields = existFields;
    }

    public String getMust() {
        return must;
    }

    public void setMust(String must) {
        this.must = must;
    }

    public String getGn() {
        return gn;
    }

    public void setGn(String gn) {
        this.gn = gn;
    }

    public String getShould() {
        return should;
    }

    public void setShould(String should) {
        this.should = should;
    }

    public String getAggFields() {
        return aggFields;
    }

    public void setAggFields(String aggFields) {
        this.aggFields = aggFields;
    }

    public String getSubAggFields() {
        return subAggFields;
    }

    public void setSubAggFields(String subAggFields) {
        this.subAggFields = subAggFields;
    }

    public String getAggSize() {
        return aggSize;
    }

    public void setAggSize(String aggSize) {
        this.aggSize = aggSize;
    }

    public String getSubAggSize() {
        return subAggSize;
    }

    public void setSubAggSize(String subAggSize) {
        this.subAggSize = subAggSize;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }
}
