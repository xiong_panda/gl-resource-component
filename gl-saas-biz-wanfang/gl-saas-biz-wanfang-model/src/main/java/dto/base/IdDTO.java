package dto.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("IdDTO")
public class IdDTO<T> {
    @ApiModelProperty("id")
    T id;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
