package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("WashConfigUpdateDTO")
public class WashConfigUpdateDTO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("json集合->字符串String <0关闭,1启用>")
    private Integer jsonCollectionsToString;
    @ApiModelProperty("xml->json <0关闭,1启用>")
    private Integer xmlToJson;
    @ApiModelProperty("验证时间合理性 <0关闭,1启用>")
    private Integer dateReasonable;
    @ApiModelProperty("验证电话合理性 <0关闭,1启用>")
    private Integer phoneReasonable;
    @ApiModelProperty("验证地址合理性 <0关闭,1启用>")
    private Integer addrReasonable;

    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateAt=new Date();

    public Integer getAddrReasonable() {
        return addrReasonable;
    }

    public void setAddrReasonable(Integer addrReasonable) {
        this.addrReasonable = addrReasonable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getJsonCollectionsToString() {
        return jsonCollectionsToString;
    }

    public void setJsonCollectionsToString(Integer jsonCollectionsToString) {
        this.jsonCollectionsToString = jsonCollectionsToString;
    }

    public Integer getXmlToJson() {
        return xmlToJson;
    }

    public void setXmlToJson(Integer xmlToJson) {
        this.xmlToJson = xmlToJson;
    }

    public Integer getDateReasonable() {
        return dateReasonable;
    }

    public void setDateReasonable(Integer dateReasonable) {
        this.dateReasonable = dateReasonable;
    }

    public Integer getPhoneReasonable() {
        return phoneReasonable;
    }

    public void setPhoneReasonable(Integer phoneReasonable) {
        this.phoneReasonable = phoneReasonable;
    }


    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
