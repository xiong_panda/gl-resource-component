package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("DesignInsertDTO")
public class DesignInsertDTO {
    @ApiModelProperty("作者id")
    private String authorId;
    @ApiModelProperty("专利所属者")
    private String patentHolder;
    @ApiModelProperty("企业id")
    private String companyId;
    @ApiModelProperty("配件名称")
    private String itemInfoName;
    @ApiModelProperty("配件规格")
    private String specs;
    @ApiModelProperty("汽车品牌")
    private String carBrand;
    @ApiModelProperty("车型")
    private String motorcycleType;
    @ApiModelProperty("专利所属单位")
    private String patentOwnedUnits;
    @ApiModelProperty("专利使用类型(1:转移,2:授权,3:邀请设计)")
    private String techniqueType;
    @ApiModelProperty("是否自制(0:自制,1:非自制)")
    private String isMark;
    @ApiModelProperty("专利编号")
    private String patentNo;
    @ApiModelProperty("供应商")
    private String supplier;
    @ApiModelProperty("供应商数量")
    private Long supplierNum;
    @ApiModelProperty("专利数")
    private Long patentsNum;
    @ApiModelProperty("供应商确认状态(0:确认,1:确认)")
    private String supplierStatus;

    @ApiModelProperty(value = "创建人",hidden = true)
    private String createBy;
    @ApiModelProperty(value = "创建时间",hidden = true)
    private Date createAt=new Date();
    @ApiModelProperty(value = "删除标记(0:正常,1:删除)",hidden = true)
    private String delFlag="0";

    public String getPatentHolder() {
        return patentHolder;
    }

    public void setPatentHolder(String patentHolder) {
        this.patentHolder = patentHolder;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getItemInfoName() {
        return itemInfoName;
    }

    public void setItemInfoName(String itemInfoName) {
        this.itemInfoName = itemInfoName;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getMotorcycleType() {
        return motorcycleType;
    }

    public void setMotorcycleType(String motorcycleType) {
        this.motorcycleType = motorcycleType;
    }

    public String getPatentOwnedUnits() {
        return patentOwnedUnits;
    }

    public void setPatentOwnedUnits(String patentOwnedUnits) {
        this.patentOwnedUnits = patentOwnedUnits;
    }

    public String getTechniqueType() {
        return techniqueType;
    }

    public void setTechniqueType(String techniqueType) {
        this.techniqueType = techniqueType;
    }

    public String getIsMark() {
        return isMark;
    }

    public void setIsMark(String isMark) {
        this.isMark = isMark;
    }

    public String getPatentNo() {
        return patentNo;
    }

    public void setPatentNo(String patentNo) {
        this.patentNo = patentNo;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Long getSupplierNum() {
        return supplierNum;
    }

    public void setSupplierNum(Long supplierNum) {
        this.supplierNum = supplierNum;
    }

    public Long getPatentsNum() {
        return patentsNum;
    }

    public void setPatentsNum(Long patentsNum) {
        this.patentsNum = patentsNum;
    }

    public String getSupplierStatus() {
        return supplierStatus;
    }

    public void setSupplierStatus(String supplierStatus) {
        this.supplierStatus = supplierStatus;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}
