package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel("TableDetailDTO")
public class TableDetailDTO {
    @ApiModelProperty(value = "tableId",hidden = true)
    private String tableId;
    @ApiModelProperty(value = "数据库")
    private String fromDatabase;
    @ApiModelProperty(value = "表名")
    private String fromTable;
    @ApiModelProperty(value = "数据类型(数字,字符类型需要加长度)")
    private String dataType;
    @ApiModelProperty(value = "是否启用")
    private Integer enabled;
    @ApiModelProperty(value = "原字段名")
    private String originalField;
    @ApiModelProperty(value = "现字段名")
    private String newField;
    @ApiModelProperty(value = "插入条件")
    private String conditions;
    @ApiModelProperty(value = "注释")
    private String fieldComment;
    @ApiModelProperty(value = "关系")
    private String relation;
    @ApiModelProperty(value = "是否属于主表")
    private Integer main;
    @ApiModelProperty(value = "排序字段")
    private Integer fieldOrder;

    public Integer getFieldOrder() {
        return fieldOrder;
    }

    public void setFieldOrder(Integer fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

    public Integer getMain() {
        return main;
    }

    public void setMain(Integer main) {
        this.main = main;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getFieldComment() {
        return fieldComment;
    }

    public void setFieldComment(String fieldComment) {
        this.fieldComment = fieldComment;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getFromDatabase() {
        return fromDatabase;
    }

    public void setFromDatabase(String fromDatabase) {
        this.fromDatabase = fromDatabase;
    }

    public String getFromTable() {
        return fromTable;
    }

    public void setFromTable(String fromTable) {
        this.fromTable = fromTable;
    }



    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getOriginalField() {
        return originalField;
    }

    public void setOriginalField(String originalField) {
        this.originalField = originalField;
    }

    public String getNewField() {
        return newField;
    }

    public void setNewField(String newField) {
        this.newField = newField;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }
}
