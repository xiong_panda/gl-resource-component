package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;


@ApiModel("StandardUpdateDTO")
public class StandardUpdateDTO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("中文注释")
    private String chineseComment;
    @ApiModelProperty("原字段")
    private String originalField;
    @ApiModelProperty("核心库字段")
    private String coreField;
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateAt = new Date();

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getChineseComment() {
        return chineseComment;
    }

    public void setChineseComment(String chineseComment) {
        this.chineseComment = chineseComment;
    }

    public String getOriginalField() {
        return originalField;
    }

    public void setOriginalField(String originalField) {
        this.originalField = originalField;
    }

    public String getCoreField() {
        return coreField;
    }

    public void setCoreField(String coreField) {
        this.coreField = coreField;
    }
}
