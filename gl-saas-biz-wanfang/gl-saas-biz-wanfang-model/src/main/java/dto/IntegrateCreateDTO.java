package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;


@ApiModel("IntegrateCreateDTO")
public class IntegrateCreateDTO {
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("去重标准字段")
    private String deweightField;
    @ApiModelProperty("迭代标准(1,字段完整度,2时间最新)")
    private Integer iterationCondition;
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date createAt = new Date();

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDeweightField() {
        return deweightField;
    }

    public void setDeweightField(String deweightField) {
        this.deweightField = deweightField;
    }

    public Integer getIterationCondition() {
        return iterationCondition;
    }

    public void setIterationCondition(Integer iterationCondition) {
        this.iterationCondition = iterationCondition;
    }
}
