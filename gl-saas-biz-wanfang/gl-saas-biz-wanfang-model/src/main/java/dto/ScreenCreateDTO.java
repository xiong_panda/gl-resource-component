package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;


@ApiModel("ScreenCreateDTO")
public class ScreenCreateDTO {
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("中文注释")
    private String classifyField;
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateAt = new Date();

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getClassifyField() {
        return classifyField;
    }

    public void setClassifyField(String classifyField) {
        this.classifyField = classifyField;
    }
}
