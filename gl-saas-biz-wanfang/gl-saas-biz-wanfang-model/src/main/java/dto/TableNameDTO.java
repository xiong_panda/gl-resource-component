package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("TableNameDTO")
public class TableNameDTO<T> {
    @ApiModelProperty("表名")
    private T tableName;

    public T getTableName() {
        return tableName;
    }

    public void setTableName(T tableName) {
        this.tableName = tableName;
    }
}
