package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("StandardQueryDTO")
public class StandardQueryDTO extends PageDTO {
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("中文注释")
    private String chineseComment;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getChineseComment() {
        return chineseComment;
    }

    public void setChineseComment(String chineseComment) {
        this.chineseComment = chineseComment;
    }
}
