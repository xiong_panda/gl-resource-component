package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;


@ApiModel("StandardCreateDTO")
public class StandardCreateDTO {
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("中文注释")
    private String chineseComment;
    @ApiModelProperty("原字段")
    private String originalField;
    @ApiModelProperty("核心库字段")
    private String coreField;
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date createAt = new Date();

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getChineseComment() {
        return chineseComment;
    }

    public void setChineseComment(String chineseComment) {
        this.chineseComment = chineseComment;
    }

    public String getOriginalField() {
        return originalField;
    }

    public void setOriginalField(String originalField) {
        this.originalField = originalField;
    }

    public String getCoreField() {
        return coreField;
    }

    public void setCoreField(String coreField) {
        this.coreField = coreField;
    }
}
