package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;


@ApiModel("ScreenUpdateDTO")
public class ScreenUpdateDTO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("表名")
    private String tableName;
    @ApiModelProperty("分类字段")
    private String classifyField;
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateAt = new Date();

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getClassifyField() {
        return classifyField;
    }

    public void setClassifyField(String classifyField) {
        this.classifyField = classifyField;
    }
}
