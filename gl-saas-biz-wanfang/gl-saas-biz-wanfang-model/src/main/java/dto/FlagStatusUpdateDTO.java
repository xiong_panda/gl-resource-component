package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;


@ApiModel("FlagStatusUpdateDTO")
public class FlagStatusUpdateDTO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("时态最新标识")
    private Integer newFlag;
    @ApiModelProperty("时态历史标记")
    private Integer oldFlag;
    @ApiModelProperty("万方来源标识")
    private Integer wangfangFlag;
    @ApiModelProperty("东方来源标识")
    private Integer dongfangFlag;
    @ApiModelProperty("宁波来源标识")
    private Integer ningboFlag;
    @ApiModelProperty("国龙来源标识")
    private Integer guolongFlag;

    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateAt = new Date();

    public Integer getWangfangFlag() {
        return wangfangFlag;
    }

    public void setWangfangFlag(Integer wangfangFlag) {
        this.wangfangFlag = wangfangFlag;
    }

    public Integer getDongfangFlag() {
        return dongfangFlag;
    }

    public void setDongfangFlag(Integer dongfangFlag) {
        this.dongfangFlag = dongfangFlag;
    }

    public Integer getNingboFlag() {
        return ningboFlag;
    }

    public void setNingboFlag(Integer ningboFlag) {
        this.ningboFlag = ningboFlag;
    }

    public Integer getGuolongFlag() {
        return guolongFlag;
    }

    public void setGuolongFlag(Integer guolongFlag) {
        this.guolongFlag = guolongFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(Integer newFlag) {
        this.newFlag = newFlag;
    }

    public Integer getOldFlag() {
        return oldFlag;
    }

    public void setOldFlag(Integer oldFlag) {
        this.oldFlag = oldFlag;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
