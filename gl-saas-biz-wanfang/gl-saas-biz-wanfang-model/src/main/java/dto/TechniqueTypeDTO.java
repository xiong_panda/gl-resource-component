package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("TechniqueTypeDTO")
public class TechniqueTypeDTO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("专利使用类型(1:转移,2:授权,3:邀请设计)")
    private String techniqueType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTechniqueType() {
        return techniqueType;
    }

    public void setTechniqueType(String techniqueType) {
        this.techniqueType = techniqueType;
    }
}
