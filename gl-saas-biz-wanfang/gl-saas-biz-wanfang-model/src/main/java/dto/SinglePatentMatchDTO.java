package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("SinglePatentMatchDTO")
public class SinglePatentMatchDTO extends PageDTO {
    @ApiModelProperty("配件名称")
    private String itemInfoName;
    @ApiModelProperty("配件规格")
    private String specs;


    public String getItemInfoName() {
        return itemInfoName;
    }

    public void setItemInfoName(String itemInfoName) {
        this.itemInfoName = itemInfoName;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }
}
