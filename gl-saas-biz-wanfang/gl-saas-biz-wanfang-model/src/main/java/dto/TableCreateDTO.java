package dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("TableCreateDTO")
public class TableCreateDTO {
    @ApiModelProperty(value = "旧表id,(更新必传)")
    private String oldId;
    @ApiModelProperty(hidden = true)
    private String id;
    @ApiModelProperty(value = "表名")
    private String tableName;
    @ApiModelProperty(value = "注释")
    private String tableComment;
    @ApiModelProperty(value = "主表")
    private String mainTable;
    @ApiModelProperty(value = "详情")
    List<TableDetailDTO> detailList;

    public String getOldId() {
        return oldId;
    }

    public void setOldId(String oldId) {
        this.oldId = oldId;
    }

    public String getMainTable() {
        return mainTable;
    }

    public void setMainTable(String mainTable) {
        this.mainTable = mainTable;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<TableDetailDTO> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<TableDetailDTO> detailList) {
        this.detailList = detailList;
    }
}
