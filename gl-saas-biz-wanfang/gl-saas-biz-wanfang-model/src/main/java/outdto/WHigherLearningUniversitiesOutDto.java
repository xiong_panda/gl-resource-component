package outdto;

import annotation.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 高等院校
 *
 * @author code_generator
 */
@Data
public class WHigherLearningUniversitiesOutDto implements java.io.Serializable {

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)//	private Integer id ;
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    /**
     * 全文索引
     **/
    @FieldOrder(order = 3)
    @ApiModelProperty(value = "全文索引")
    private String fulltext;

    /**
     * 学校名称
     **/
    @FieldOrder(order = 4)
    @ApiModelProperty(value = "学校名称")
    private String name;

    /**
     *
     **/
    @FieldOrder(order = 5)
    @ApiModelProperty(value = "")
    private String nameAnyname1;

    /**
     *
     **/
    @FieldOrder(order = 6)
    @ApiModelProperty(value = "")
    private String nameAnyname2;

    /**
     * 学校名称
     **/
    @FieldOrder(order = 7)
    @ApiModelProperty(value = "学校名称")
    private String schoolName;

    /**
     * 曾用名
     **/
    @FieldOrder(order = 8)
    @ApiModelProperty(value = "曾用名")
    private String ohtherName;

    /**
     * 主管单位
     **/
    @FieldOrder(order = 9)
    @ApiModelProperty(value = "主管单位")
    private String compunit;

    /**
     *
     **/
    @FieldOrder(order = 10)
    @ApiModelProperty(value = "")
    private String compunitIko;

    /**
     *
     **/
    @FieldOrder(order = 11)
    @ApiModelProperty(value = "")
    private String iko;

    /**
     * 省名
     **/
    @FieldOrder(order = 12)
    @ApiModelProperty(value = "省名")
    private String province;

    /**
     * 市名
     **/
    @FieldOrder(order = 13)
    @ApiModelProperty(value = "市名")
    private String city;

    /**
     * 县名
     **/
    @FieldOrder(order = 14)
    @ApiModelProperty(value = "县名")
    private String county;

    /**
     * 区号
     **/
    @FieldOrder(order = 15)
    @ApiModelProperty(value = "区号")
    private String acode;

    /**
     * 行政区代码
     **/
    @FieldOrder(order = 16)
    @ApiModelProperty(value = "行政区代码")
    private String region;

    /**
     * 邮码
     **/
    @FieldOrder(order = 17)
    @ApiModelProperty(value = "邮码")
    private String postalCode;

    /**
     * 地址
     **/
    @FieldOrder(order = 18)
    @ApiModelProperty(value = "地址")
    private String address;

    /**
     * 电话
     **/
    @FieldOrder(order = 19)
    @ApiModelProperty(value = "电话")
    private String telphone;

    /**
     * 传真
     **/
    @FieldOrder(order = 20)
    @ApiModelProperty(value = "传真")
    private String fax;

    /**
     * 域名地址
     **/
    @FieldOrder(order = 21)
    @ApiModelProperty(value = "域名地址")
    private String url;

    /**
     * 专职教师数
     **/
    @FieldOrder(order = 22)
    @ApiModelProperty(value = "专职教师数")
    private String techs;

    /**
     * 电子邮件
     **/
    @FieldOrder(order = 23)
    @ApiModelProperty(value = "电子邮件")
    private String email;

    /**
     * 占地面积(平方米)
     **/
    @FieldOrder(order = 24)
    @ApiModelProperty(value = "占地面积(平方米)")
    private String space;

    /**
     * 办学层次
     **/
    @FieldOrder(order = 25)
    @ApiModelProperty(value = "办学层次")
    private String levels;

    /**
     * 负责人
     **/
    @FieldOrder(order = 26)
    @ApiModelProperty(value = "负责人")
    private String person;

    /**
     * 办学类型
     **/
    @FieldOrder(order = 27)
    @ApiModelProperty(value = "办学类型")
    private String schoolType;

    /**
     * 重点学科
     **/
    @FieldOrder(order = 28)
    @ApiModelProperty(value = "重点学科")
    private String keydiscipline;

    /**
     * 特色高校
     **/
    @FieldOrder(order = 29)
    @ApiModelProperty(value = "特色高校")
    private String specialSchool;

    /**
     * 学校简介
     **/
    @FieldOrder(order = 30)
    @ApiModelProperty(value = "学校简介")
    private String intro;

    /**
     * 分校及校区
     **/
    @FieldOrder(order = 31)
    @ApiModelProperty(value = "分校及校区")
    private String campus;

    /**
     * 教学机构设置
     **/
    @FieldOrder(order = 32)
    @ApiModelProperty(value = "教学机构设置")
    private String orgSetting;

    /**
     * 历史沿革
     **/
    @FieldOrder(order = 33)
    @ApiModelProperty(value = "历史沿革")
    private String history;

    /**
     * 并入从属院校
     **/
    @FieldOrder(order = 34)
    @ApiModelProperty(value = "并入从属院校")
    private String subSchool;

    /**
     * 教学科研仪器总值(万元)
     **/
    @FieldOrder(order = 35)
    @ApiModelProperty(value = "教学科研仪器总值(万元)")
    private String gross;

    /**
     * 机构ID
     **/
    @FieldOrder(order = 36)
    @ApiModelProperty(value = "机构ID")
    private String orgId;

    /**
     *
     **/
    @FieldOrder(order = 37)
    @ApiModelProperty(value = "")
    private String featurecollege;

    /**
     *
     **/
    @FieldOrder(order = 38)
    @ApiModelProperty(value = "")
    private String featurecollegeAnyname1;

    /**
     *
     **/
    @FieldOrder(order = 39)
    @ApiModelProperty(value = "")
    private String featurecollegeAnyname2;

    /**
     * 高校类型
     **/
    @FieldOrder(order = 40)
    @ApiModelProperty(value = "高校类型")
    private String collegeType;

    /**
     * 是否成品数据
     **/
    @FieldOrder(order = 41)
    @ApiModelProperty(value = "是否成品数据")
    private String finishedProductData;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;
}
