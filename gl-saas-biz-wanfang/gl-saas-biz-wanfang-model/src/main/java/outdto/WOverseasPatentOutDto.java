package outdto;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 海外专利
 *
 * @author code_generator
 */
@Data
public class WOverseasPatentOutDto implements java.io.Serializable {

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;

    /**
     * 主题全文检索(标题、关键词、摘要)
     **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    @FieldOrder(order = 4)
    private String fullName;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    @FieldOrder(order = 5)
    private String title;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    @FieldOrder(order = 6)
    private String Abstract;

    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    @FieldOrder(order = 7)
    private String keyword;

    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 8)
    private String orgName;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 9)
    private String org2;

    /**
     * 机构名称(全文检索)
     **/
    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 10)
    private String orgNameSearch;

    /**
     * 发明（设计）人
     **/
    @ApiModelProperty(value = "发明（设计）人")
    @FieldOrder(order = 11)
    private String author;

    /**
     * 专利号
     **/
    @ApiModelProperty(value = "专利号")
    @FieldOrder(order = 12)
    private String patentNo;

    /**
     * 申请号
     **/
    @ApiModelProperty(value = "申请号")
    @FieldOrder(order = 13)
    private String requsetNo;

    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    @FieldOrder(order = 14)
    private String date;

    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    @FieldOrder(order = 15)
    private String requestPeople;

    /**
     * 国省代码
     **/
    @ApiModelProperty(value = "国省代码")
    @FieldOrder(order = 16)
    private String countryCode;

    /**
     * 公开（公告）号
     **/
    @ApiModelProperty(value = "公开（公告）号")
    @FieldOrder(order = 17)
    private String publicationNo;

    /**
     * 公开（公告）日
     **/
    @ApiModelProperty(value = "公开（公告）日")
    @FieldOrder(order = 18)
    private String publicationDate1;

    /**
     * 公开（公告）日
     **/
    @ApiModelProperty(value = "公开（公告）日")
    @FieldOrder(order = 19)
    private String publicationDate2;

    /**
     * 颁证日
     **/
    @ApiModelProperty(value = "颁证日")
    @FieldOrder(order = 20)
    private String createDate;

    /**
     * 主分类号
     **/
    @ApiModelProperty(value = "主分类号")
    @FieldOrder(order = 21)
    private String mainClassCode1;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 22)
    private String omaincls;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 23)
    private String ocls;

    /**
     * 主分类号
     **/
    @ApiModelProperty(value = "主分类号")
    @FieldOrder(order = 24)
    private String mainClassCode2;

    /**
     * 分类号
     **/
    @ApiModelProperty(value = "分类号")
    @FieldOrder(order = 25)
    private String classCode;

    /**
     * 外观分类
     **/
    @ApiModelProperty(value = "外观分类")
    @FieldOrder(order = 26)
    private String appearanceCode;

    /**
     * 本国主分类
     **/
    @ApiModelProperty(value = "本国主分类")
    @FieldOrder(order = 27)
    private String countryMainClass;

    /**
     * 本国副分类
     **/
    @ApiModelProperty(value = "本国副分类")
    @FieldOrder(order = 28)
    private String countryNextClass;

    /**
     * 欧洲主分类
     **/
    @ApiModelProperty(value = "欧洲主分类")
    @FieldOrder(order = 29)
    private String europeMainClass;

    /**
     * 附加分类
     **/
    @ApiModelProperty(value = "附加分类")
    @FieldOrder(order = 30)
    private String europeNextClass;

    /**
     * 同族专利项
     **/
    @ApiModelProperty(value = "同族专利项")
    @FieldOrder(order = 31)
    private String congener;

    /**
     * 分案原申请号
     **/
    @ApiModelProperty(value = "分案原申请号")
    @FieldOrder(order = 32)
    private String oreqno;

    /**
     * 中图分类
     **/
    @ApiModelProperty(value = "中图分类")
    @FieldOrder(order = 33)
    private String cls;

    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    @FieldOrder(order = 34)
    private String year;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 35)
    private String reqnov;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 36)
    private String patt;

    /**
     * 国际申请
     **/
    @ApiModelProperty(value = "国际申请")
    @FieldOrder(order = 37)
    private String internationalApplication;

    /**
     * 国际公布
     **/
    @ApiModelProperty(value = "国际公布")
    @FieldOrder(order = 38)
    private String internationalPublish;

    /**
     * WO申请号
     **/
    @ApiModelProperty(value = "WO申请号")
    @FieldOrder(order = 39)
    private String woApplicationNo;

    /**
     * 进入国家日期
     **/
    @ApiModelProperty(value = "进入国家日期")
    @FieldOrder(order = 40)
    private String dec;

    /**
     * 优先权
     **/
    @ApiModelProperty(value = "优先权")
    @FieldOrder(order = 41)
    private String priority;

    /**
     * 优先权日
     **/
    @ApiModelProperty(value = "优先权日")
    @FieldOrder(order = 42)
    private String priorityData;

    /**
     * 再版原专利
     **/
    @ApiModelProperty(value = "再版原专利")
    @FieldOrder(order = 43)
    private String originalPatentNo;

    /**
     * 本国参考文献
     **/
    @ApiModelProperty(value = "本国参考文献")
    @FieldOrder(order = 44)
    private String homeReference;

    /**
     * 国外参考文献
     **/
    @ApiModelProperty(value = "国外参考文献")
    @FieldOrder(order = 45)
    private String foreignReference;

    /**
     * 非专利参考文献
     **/
    @ApiModelProperty(value = "非专利参考文献")
    @FieldOrder(order = 46)
    private String nonref;

    /**
     * 主权项
     **/
    @ApiModelProperty(value = "主权项")
    @FieldOrder(order = 47)
    private String independentClaim;

    /**
     * 机构类型
     **/
    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 48)
    private String orgType;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    @FieldOrder(order = 49)
    private String name;

    /**
     * 发明（设计）人
     **/
    @ApiModelProperty(value = "发明（设计）人")
    @FieldOrder(order = 50)
    private String author2;

    /**
     * 专利代理机构
     **/
    @ApiModelProperty(value = "专利代理机构")
    @FieldOrder(order = 51)
    private String orgAgency;

    /**
     * 代理人
     **/
    @ApiModelProperty(value = "代理人")
    @FieldOrder(order = 52)
    private String agent;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 53)
    private String dbid;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 54)
    private String source;

    /**
     * 学科分类
     **/
    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 55)
    private String disciplineClass;

    /**
     * 失效专利
     **/
    @ApiModelProperty(value = "失效专利")
    @FieldOrder(order = 56)
    private String invalidPatent;

    /**
     * 主分类号显示数
     **/
    @ApiModelProperty(value = "主分类号显示数")
    @FieldOrder(order = 57)
    private String mainclsnum;

    /**
     * 分类号显示数
     **/
    @ApiModelProperty(value = "分类号显示数")
    @FieldOrder(order = 58)
    private String mainCategoryNo;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 59)
    private String requestAddress;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 60)
    private String reference;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 61)
    private String reviewer;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 62)
    private String cdid;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 63)
    private String serAddress;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 64)
    private String publishPath;

    /**
     * 索引管理字段(无用)
     **/
    @ApiModelProperty(value = "索引管理字段(无用)")
    @FieldOrder(order = 65)
    private String errorCode;

    @FieldOrder(order = 66)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private java.util.Date CreateTime;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;


}
