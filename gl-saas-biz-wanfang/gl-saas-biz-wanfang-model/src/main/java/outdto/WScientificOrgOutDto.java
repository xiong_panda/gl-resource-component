package outdto;

import annotation.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 科研机构
 *
 * @author code_generator
 */
@Data
public class WScientificOrgOutDto implements java.io.Serializable {

    /**
     * 记录顺序号
     *
     *

    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/

    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    /**
     * 全文索引
     **/

    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;

    /**
     * 省
     **/

    @ApiModelProperty(value = "省")
    @FieldOrder(order = 4)
    private String province;

    /**
     * 市
     **/

    @ApiModelProperty(value = "市")
    @FieldOrder(order = 5)
    private String city;

    /**
     * 县
     **/

    @ApiModelProperty(value = "县")
    @FieldOrder(order = 6)
    private String county;

    /**
     * 机构名称:规范名称:机构曾用名
     **/

    @ApiModelProperty(value = "机构名称:规范名称:机构曾用名")
    @FieldOrder(order = 7)
    private String orgNameNorm;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 8)
    private String nameAnyname1;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 9)
    private String nameAnyname2;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 10)
    private String acname;

    /**
     * 机构名称
     **/

    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 11)
    private String orgName;

    /**
     * 规范名称
     **/

    @ApiModelProperty(value = "规范名称")
    @FieldOrder(order = 12)
    private String normName;

    /**
     * 机构曾用名
     **/

    @ApiModelProperty(value = "机构曾用名")
    @FieldOrder(order = 13)
    private String orgNameOther;

    /**
     * 依托单位
     **/

    @ApiModelProperty(value = "依托单位")
    @FieldOrder(order = 14)
    private String rely;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 15)
    private String relyAnyname1;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 16)
    private String relyAnyname2;

    /**
     * 上级主管单位
     **/

    @ApiModelProperty(value = "上级主管单位")
    @FieldOrder(order = 17)
    private String compunit;

    /**
     * 内部机构名称
     **/

    @ApiModelProperty(value = "内部机构名称")
    @FieldOrder(order = 18)
    private String orgNameInternal;

    /**
     * 下属机构名称
     **/

    @ApiModelProperty(value = "下属机构名称")
    @FieldOrder(order = 19)
    private String orgNameSub;

    /**
     * 重点实验室名称
     **/

    @ApiModelProperty(value = "重点实验室名称")
    @FieldOrder(order = 20)
    private String keyname;

    /**
     * Email
     **/

    @ApiModelProperty(value = "Email")
    @FieldOrder(order = 21)
    private String email;

    /**
     * Internet
     **/

    @ApiModelProperty(value = "Internet")
    @FieldOrder(order = 22)
    private String url;

    /**
     * 负责人
     **/

    @ApiModelProperty(value = "负责人")
    @FieldOrder(order = 23)
    private String person;

    /**
     * 负责人
     **/

    @ApiModelProperty(value = "负责人")
    @FieldOrder(order = 24)
    private String multiperson;

    /**
     * 通信地址
     **/

    @ApiModelProperty(value = "通信地址")
    @FieldOrder(order = 25)
    private String address;

    /**
     * 电话
     **/

    @ApiModelProperty(value = "电话")
    @FieldOrder(order = 26)
    private String telphone;

    /**
     * 传真
     **/

    @ApiModelProperty(value = "传真")
    @FieldOrder(order = 27)
    private String fax;

    /**
     * 成立年代
     **/

    @ApiModelProperty(value = "成立年代")
    @FieldOrder(order = 28)
    private String year;

    /**
     * 机构简介
     **/

    @ApiModelProperty(value = "机构简介")
    @FieldOrder(order = 29)
    private String orgIntro;

    /**
     * 职工人数
     **/

    @ApiModelProperty(value = "职工人数")
    @FieldOrder(order = 30)
    private String emps;

    /**
     * 科研人数
     **/

    @ApiModelProperty(value = "科研人数")
    @FieldOrder(order = 31)
    private String techs;

    /**
     * 主要研究人员
     **/

    @ApiModelProperty(value = "主要研究人员")
    @FieldOrder(order = 32)
    private String leadresh;

    /**
     * 获奖情况
     **/

    @ApiModelProperty(value = "获奖情况")
    @FieldOrder(order = 33)
    private String awards;

    /**
     * 拥有专利数
     **/

    @ApiModelProperty(value = "拥有专利数")
    @FieldOrder(order = 34)
    private String patents;

    /**
     * 科研成果
     **/

    @ApiModelProperty(value = "科研成果")
    @FieldOrder(order = 35)
    private String patent;

    /**
     * 拥有专利
     **/

    @ApiModelProperty(value = "拥有专利")
    @FieldOrder(order = 36)
    private String profit;

    /**
     * 科研设备
     **/

    @ApiModelProperty(value = "科研设备")
    @FieldOrder(order = 37)
    private String sciasset;

    /**
     * 学科研究范围
     **/

    @ApiModelProperty(value = "学科研究范围")
    @FieldOrder(order = 38)
    private String reshdis;

    /**
     * 进展中课题
     **/

    @ApiModelProperty(value = "进展中课题")
    @FieldOrder(order = 39)
    private String matter;

    /**
     * 推广技术与项目
     **/

    @ApiModelProperty(value = "推广技术与项目")
    @FieldOrder(order = 40)
    private String project;

    /**
     * 产品信息
     **/

    @ApiModelProperty(value = "产品信息")
    @FieldOrder(order = 41)
    private String wScientificOrg;

    /**
     * 出版刊物
     **/

    @ApiModelProperty(value = "出版刊物")
    @FieldOrder(order = 42)
    private String jour;

    /**
     * 机构类别
     **/

    @ApiModelProperty(value = "机构类别")
    @FieldOrder(order = 43)
    private String orgType;

    /**
     * 所在地代码
     **/

    @ApiModelProperty(value = "所在地代码")
    @FieldOrder(order = 44)
    private String placeCode;

    /**
     * 学科分类
     **/

    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 45)
    private String disciplineClass;

    /**
     * 学科代码
     **/

    @ApiModelProperty(value = "学科代码")
    @FieldOrder(order = 46)
    private String disciplineCode;

    /**
     * 学科代码
     **/

    @ApiModelProperty(value = "学科代码")
    @FieldOrder(order = 47)
    private String disciplineCode2;

    /**
     * 关键词
     **/

    @ApiModelProperty(value = "关键词")
    @FieldOrder(order = 48)
    private String keyword;

    /**
     * 机构ID
     **/

    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 49)
    private String orgId;

    /**
     * 科研机构推介信息
     **/

    @ApiModelProperty(value = "科研机构推介信息")
    @FieldOrder(order = 50)
    private String orgRecommendations;

    /**
     * 是否成品数据
     **/

    @ApiModelProperty(value = "是否成品数据")
    @FieldOrder(order = 51)
    private String finishedProduct;
    @FieldOrder(order = 52)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

}
