package outdto;

import annotation.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 法律法规
 */
@Data
public class WLawsRegulationsOutDTO implements java.io.Serializable {

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号 ")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;

    /**
     * 标题:英文标题
     **/
    @ApiModelProperty(value = "标题:英文标题")
    @FieldOrder(order = 4)
    private String title2;

    /**
     * 法规正文
     **/
    @ApiModelProperty(value = "法规正文")
    @FieldOrder(order = 5)
    private String rulesText;

    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    @FieldOrder(order = 6)
    private String keyword;

    /**
     * 颁布部门
     **/
    @ApiModelProperty(value = "颁布部门")
    @FieldOrder(order = 7)
    private String proclaimDepartment;

    /**
     * 颁布部门(全文检索)
     **/
    @ApiModelProperty(value = "颁布部门(全文检索)")
    @FieldOrder(order = 8)
    private String proclaimDepartmentFulltext;

    /**
     * 颁布部门
     **/
    @ApiModelProperty(value = "颁布部门")
    @FieldOrder(order = 9)
    private String proclaimDepartment2;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 10)
    private String author;

    /**
     * 中图分类
     **/
    @ApiModelProperty(value = "中图分类")
    @FieldOrder(order = 11)
    private String cls;

    /**
     * 颁布日期
     **/
    @ApiModelProperty(value = "颁布日期")
    @FieldOrder(order = 12)
    private String proclaimDate;

    /**
     * 终审日期
     **/
    @ApiModelProperty(value = "终审日期")
    @FieldOrder(order = 13)
    private String finalDate;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 14)
    private String rid;

    /**
     * 标题
     **/
    @ApiModelProperty(value = "标题")
    @FieldOrder(order = 15)
    private String title;

    /**
     * 英文标题
     **/
    @ApiModelProperty(value = "英文标题")
    @FieldOrder(order = 16)
    private String englishTitle;

    /**
     * 发文文号
     **/
    @ApiModelProperty(value = "发文文号")
    @FieldOrder(order = 17)
    private String postDocumentCode;

    /**
     * 机构ID
     **/
    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 18)
    private String orgId;

    /**
     * 颁布部门
     **/
    @ApiModelProperty(value = "颁布部门")
    @FieldOrder(order = 19)
    private String proclaimDepartment3;

    /**
     * 行业地区码_部门代码
     **/
    @ApiModelProperty(value = "行业地区码_部门代码")
    @FieldOrder(order = 20)
    private String industrycodeDepartmentCode;

    /**
     * 效力级别
     **/
    @ApiModelProperty(value = "效力级别")
    @FieldOrder(order = 21)
    private String valueLevel;

    /**
     * 效力代码
     **/
    @ApiModelProperty(value = "效力代码")
    @FieldOrder(order = 22)
    private String valueCode;

    /**
     * 时效性
     **/
    @ApiModelProperty(value = "时效性")
    @FieldOrder(order = 23)
    private String timeliness;

    /**
     * 批准日期
     **/
    @ApiModelProperty(value = "批准日期")
    @FieldOrder(order = 24)
    private String approvalDate;

    /**
     * 签字日期
     **/
    @ApiModelProperty(value = "签字日期")
    @FieldOrder(order = 25)
    private String signatureDate;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 26)
    private String yapprdate;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 27)
    private String ysigndate;

    /**
     * 实施日期
     **/
    @ApiModelProperty(value = "实施日期")
    @FieldOrder(order = 28)
    private String implementationDate;

    /**
     * 失效日期
     **/
    @ApiModelProperty(value = "失效日期")
    @FieldOrder(order = 29)
    private String invalidDate;

    /**
     * 终审法院
     **/
    @ApiModelProperty(value = "终审法院")
    @FieldOrder(order = 30)
    private String finalCourt;

    /**
     * 终审日期
     **/
    @ApiModelProperty(value = "终审日期")
    @FieldOrder(order = 31)
    private String finalDate2;

    /**
     * 调解日期
     **/
    @ApiModelProperty(value = "调解日期")
    @FieldOrder(order = 32)
    private String mediateDate;

    /**
     * 内容分类
     **/
    @ApiModelProperty(value = "内容分类")
    @FieldOrder(order = 33)
    private String cintentClass;

    /**
     * 内容分类码
     **/
    @ApiModelProperty(value = "内容分类码")
    @FieldOrder(order = 34)
    private String cintentClassCode;

    /**
     * URL
     **/
    @ApiModelProperty(value = "URL")
    @FieldOrder(order = 35)
    private String url;

    /**
     * PDF全文
     **/
    @ApiModelProperty(value = "PDF全文")
    @FieldOrder(order = 36)
    private String pdfFulltext;

    /**
     * 相关链接
     **/
    @ApiModelProperty(value = "相关链接")
    @FieldOrder(order = 37)
    private String relevantLink;

    /**
     * 历史链接
     **/
    @ApiModelProperty(value = "历史链接")
    @FieldOrder(order = 38)
    private String historyLink;

    /**
     * 库别代码
     **/
    @ApiModelProperty(value = "库别代码")
    @FieldOrder(order = 39)
    private String libraryCode;

    /**
     * 制作日期
     **/
    @ApiModelProperty(value = "制作日期")
    @FieldOrder(order = 40)
    private String makeDate;

    /**
     * 行业分类
     **/
    @ApiModelProperty(value = "行业分类")
    @FieldOrder(order = 41)
    private String industryClass;

    /**
     * 行业分类码
     **/
    @ApiModelProperty(value = "行业分类码")
    @FieldOrder(order = 42)
    private String industryClassCode;

    /**
     * 学科分类
     **/
    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 43)
    private String disciplineClass;

    /**
     * 索引管理字段(无用)
     **/
    @ApiModelProperty(value = "索引管理字段(无用)")
    @FieldOrder(order = 44)
    private String errorCode;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;


}
