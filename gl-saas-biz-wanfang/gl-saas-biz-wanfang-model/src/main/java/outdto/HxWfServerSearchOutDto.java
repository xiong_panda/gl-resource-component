package outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 用户使用统计
 * 接收访问的服务 和 使用的数据
 */
@Data
public class HxWfServerSearchOutDto implements java.io.Serializable{


	/** 主键id **/
	@ApiModelProperty(value = "主键id")
	private String id ;

	/** 用户名称 **/
	@ApiModelProperty(value = "用户名称")
	private String userName ;

	/** 用户领域 **/
	@ApiModelProperty(value = "用户领域")
	private String userField ;

	/** 用户行业 **/
	@ApiModelProperty(value = "用户行业")
	private String userIndustry ;

	/** 用户类型 **/
	@ApiModelProperty(value = "用户类型")
	private String userType ;

	/** 用户地区 **/
	@ApiModelProperty(value = "用户地区")
	private String userArea ;

	/** 平台用户主键标识 **/
	@ApiModelProperty(value = "平台用户主键标识")
	private String userId ;

	/** 搜索关键字 **/
	@ApiModelProperty(value = "搜索关键字")
	private String searchKeyword ;

	/** 服务信息 **/
	@ApiModelProperty(value = "服务信息")
	private String serviceName ;

	/** 资源来源 **/
	@ApiModelProperty(value = "资源来源")
	private String serviceOrigin ;

	/** 资源名称 **/
	@ApiModelProperty(value = "资源名称")
	private String sourcesName ;

	/** 资源来源 **/
	@ApiModelProperty(value = "资源来源")
	private String sourcesOrigin ;

	/** 资源标识 **/
	@ApiModelProperty(value = "资源标识")
	private String sourcesId ;

	/** 来源为服务、资源标识(0服务，1资源) **/
	@ApiModelProperty(value = "来源为服务、资源标识(0服务，1资源)")
	private String flag ;

	/** 使用时间 **/
	@ApiModelProperty(value = "使用时间")
	private Date useTime ;

	/** 采集来源（哈长、万方等城市群） **/
	@ApiModelProperty(value = "采集来源（哈长、万方等城市群）")
	private String collectSource ;

	/** 采集时间 **/
	@ApiModelProperty(value = "采集时间")
	private Date collectTime ;

	/** 采集方式（0：调度平台采集、1：实时接口采集） **/
	@ApiModelProperty(value = "采集方式（0：调度平台采集、1：实时接口采集）")
	private String collectType ;


}
