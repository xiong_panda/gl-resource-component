package outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 整合知识文献 
 * @author code_generator
 */

@Data
public class MixZswxOutDto implements java.io.Serializable{

	/** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 摘要 **/
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 专利代理机构 **/
    @ApiModelProperty(value = "专利代理机构")
	private String orgAgency ; 

	/** 代理人 **/
    @ApiModelProperty(value = "代理人")
	private String agent ; 

	/** 公开（公告）号 **/
    @ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ; 

	/** 公开（公告）日 **/
    @ApiModelProperty(value = "公开（公告）日")
	private String publicationDate ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fAuthor ; 

	/** 发明（设计）人 **/
    @ApiModelProperty(value = "发明（设计）人")
	private String author2 ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** 范畴分类 **/
    @ApiModelProperty(value = "范畴分类")
	private String cate ; 

	/** 光盘号 **/
    @ApiModelProperty(value = "光盘号")
	private String cdId ; 

	/** 颁证日 **/
    @ApiModelProperty(value = "颁证日")
	private Date createDate ; 

	/** 分类号 **/
    @ApiModelProperty(value = "分类号")
	private String classCode ; 

	/** 国省代码 **/
    @ApiModelProperty(value = "国省代码")
	private String countryCode ; 

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private Date date ; 

	/** 数据库标识 **/
    @ApiModelProperty(value = "数据库标识")
	private String databaseIdentity ; 

	/** 进入国家日期 **/
    @ApiModelProperty(value = "进入国家日期")
	private Date dec ; 

	/** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 主权项 **/
    @ApiModelProperty(value = "主权项")
	private String indCla ; 

	/** 法律状态F_LawStatus **/
    @ApiModelProperty(value = "法律状态F_LawStatus")
	private String lawStatus ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ls ; 

	/** 规范单位名称 **/
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName ; 

	/** 国际申请 **/
    @ApiModelProperty(value = "国际申请")
	private String intApp ; 

	/** 国际公布 **/
    @ApiModelProperty(value = "国际公布")
	private String intPush ; 

	/** 失效专利 **/
    @ApiModelProperty(value = "失效专利")
	private String invPat ; 

	/** 关键词 **/
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 主分类号 **/
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode ; 

	/** 中图分类(二级) **/
    @ApiModelProperty(value = "中图分类(二级)")
	private String clsLevel2 ; 

	/** 分案原申请号 **/
    @ApiModelProperty(value = "分案原申请号")
	private String originRequestNumber ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String org ; 

	/** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String orgAnyname2Name ; 

	/** 规范单位名称 **/
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName2 ; 

	/** 机构类型 **/
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 专利号 **/
    @ApiModelProperty(value = "专利号")
	private String patentnumberName ; 

	/** 专利类型 **/
    @ApiModelProperty(value = "专利类型")
	private String patenttypeName ; 

	/** 页数 **/
    @ApiModelProperty(value = "页数")
	private String pages ; 

	/** 优先权 **/
    @ApiModelProperty(value = "优先权")
	private String priority ; 

	/** 发布路径 **/
    @ApiModelProperty(value = "发布路径")
	private String pubPath ; 

	/** 参考文献 **/
    @ApiModelProperty(value = "参考文献")
	private String reference ; 

	/** 地址 **/
    @ApiModelProperty(value = "地址")
	private String requestAddress ; 

	/** 申请号 **/
    @ApiModelProperty(value = "申请号")
	private String requestNumber ; 

	/** 申请号wjy **/
    @ApiModelProperty(value = "申请号wjy")
	private String requestNo ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ; 

	/** 审查员 **/
    @ApiModelProperty(value = "审查员")
	private String reviewer ; 

	/** 服务器地址 **/
    @ApiModelProperty(value = "服务器地址")
	private String serverAddress ; 

	/** 数据来源 **/
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/** 名称 **/
    @ApiModelProperty(value = "名称")
	private String title1 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private Date yannodate ; 

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private String date2 ; 

	/** 主分类号 **/
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String smcls ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String tmcls ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ckey ; 

	/** 机构层级ID **/
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String endOrgType ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fOrgId ; 

	/** 第一机构终级机构ID **/
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String authorId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgNum ; 

	/** 文献权重 **/
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String halfyear ; 

	/** 索引管理字段(无用) **/
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String authorInfo ; 

	/** 作者信息.姓名 **/
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String docid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String mid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String dck ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ga ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String pc ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String pr ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ipc ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ipcr ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String mipc ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String zyft ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String qwft ; 

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号  **/
    @ApiModelProperty(value = "唯一编号 ")
	private String id ; 

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 标题:英文标题 **/
    @ApiModelProperty(value = "标题:英文标题")
	private String title2 ; 

	/** 法规正文 **/
    @ApiModelProperty(value = "法规正文")
	private String rulesText ; 

	/** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment ; 

	/** 颁布部门(全文检索) **/
    @ApiModelProperty(value = "颁布部门(全文检索)")
	private String proclaimDepartmentFulltext ; 

	/** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String author ; 

	/** 中图分类 **/
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 颁布日期 **/
    @ApiModelProperty(value = "颁布日期")
	private String proclaimDate ; 

	/** 终审日期 **/
    @ApiModelProperty(value = "终审日期")
	private String finalDate ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String rid ; 

	/** 标题 **/
    @ApiModelProperty(value = "标题")
	private String title ; 

	/** 英文标题 **/
    @ApiModelProperty(value = "英文标题")
	private String englishTitle ; 

	/** 发文文号 **/
    @ApiModelProperty(value = "发文文号")
	private String postDocumentCode ; 

	/** 机构ID **/
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment3 ; 

	/** 行业地区码_部门代码 **/
    @ApiModelProperty(value = "行业地区码_部门代码")
	private String industrycodeDepartmentCode ; 

	/** 效力级别 **/
    @ApiModelProperty(value = "效力级别")
	private String valueLevel ; 

	/** 效力代码 **/
    @ApiModelProperty(value = "效力代码")
	private String valueCode ; 

	/** 时效性 **/
    @ApiModelProperty(value = "时效性")
	private String timeliness ; 

	/** 批准日期 **/
    @ApiModelProperty(value = "批准日期")
	private String approvalDate ; 

	/** 签字日期 **/
    @ApiModelProperty(value = "签字日期")
	private String signatureDate ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yapprdate ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ysigndate ; 

	/** 实施日期 **/
    @ApiModelProperty(value = "实施日期")
	private String implementationDate ; 

	/** 失效日期 **/
    @ApiModelProperty(value = "失效日期")
	private String invalidDate ; 

	/** 终审法院 **/
    @ApiModelProperty(value = "终审法院")
	private String finalCourt ; 

	/** 终审日期 **/
    @ApiModelProperty(value = "终审日期")
	private String finalDate2 ; 

	/** 调解日期 **/
    @ApiModelProperty(value = "调解日期")
	private String mediateDate ; 

	/** 内容分类 **/
    @ApiModelProperty(value = "内容分类")
	private String cintentClass ; 

	/** 内容分类码 **/
    @ApiModelProperty(value = "内容分类码")
	private String cintentClassCode ; 

	/** URL **/
    @ApiModelProperty(value = "URL")
	private String url ; 

	/** PDF全文 **/
    @ApiModelProperty(value = "PDF全文")
	private String pdfFulltext ; 

	/** 相关链接 **/
    @ApiModelProperty(value = "相关链接")
	private String relevantLink ; 

	/** 历史链接 **/
    @ApiModelProperty(value = "历史链接")
	private String historyLink ; 

	/** 库别代码 **/
    @ApiModelProperty(value = "库别代码")
	private String libraryCode ; 

	/** 制作日期 **/
    @ApiModelProperty(value = "制作日期")
	private String makeDate ; 

	/** 行业分类 **/
    @ApiModelProperty(value = "行业分类")
	private String industryClass ; 

	/** 行业分类码 **/
    @ApiModelProperty(value = "行业分类码")
	private String industryClassCode ; 

	/** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String topic ; 

	/** DOI **/
    @ApiModelProperty(value = "DOI")
	private String doi ; 

	/** w_ID **/
    @ApiModelProperty(value = "w_ID")
	private String wId ; 

	/** V_ID **/
    @ApiModelProperty(value = "V_ID")
	private String vId ; 

	/** C_ID **/
    @ApiModelProperty(value = "C_ID")
	private String cId ; 

	/** L_ID **/
    @ApiModelProperty(value = "L_ID")
	private String lId ; 

	/** 中文标题 **/
    @ApiModelProperty(value = "中文标题")
	private String chineseTitle ; 

	/** 中文标题:英文标题 **/
    @ApiModelProperty(value = "中文标题:英文标题")
	private String cETitle ; 

	/** 中文作者 **/
    @ApiModelProperty(value = "中文作者")
	private String chineseAuthor ; 

	/** 英文作者 **/
    @ApiModelProperty(value = "英文作者")
	private String englishAuthor ; 

	/** 作者个数 **/
    @ApiModelProperty(value = "作者个数")
	private String numberAuthors ; 

	/** 作者FID **/
    @ApiModelProperty(value = "作者FID")
	private String authorFid ; 

	/** 中文第一作者 **/
    @ApiModelProperty(value = "中文第一作者")
	private String chineseFirstAuthor ; 

	/** 英文第一作者 **/
    @ApiModelProperty(value = "英文第一作者")
	private String englishFirstAuthor ; 

	/** 作者个数 **/
    @ApiModelProperty(value = "作者个数")
	private String authorNumber ; 

	/** 中文作者:英文作者 **/
    @ApiModelProperty(value = "中文作者:英文作者")
	private String chineseEnglishAuthor ; 

	/** 规范单位名称 **/
    @ApiModelProperty(value = "规范单位名称")
	private String authorityUnitName ; 

	/** 中文作者单位 **/
    @ApiModelProperty(value = "中文作者单位")
	private String chineseAuthorUnit ; 

	/** 英文作者单位 **/
    @ApiModelProperty(value = "英文作者单位")
	private String englishAuthorUnit ; 

	/** 中文第一作者单位 **/
    @ApiModelProperty(value = "中文第一作者单位")
	private String chineseFirstAuthorUnit ; 

	/** 中文第一作者单位一级名称 **/
    @ApiModelProperty(value = "中文第一作者单位一级名称")
	private String chineseFirstAuthorUnitName ; 

	/** 英文第一作者单位 **/
    @ApiModelProperty(value = "英文第一作者单位")
	private String firstEnglishAuthor ; 

	/** 机构名称 **/
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 中文作者单位:英文作者单位:规范单位名称 **/
    @ApiModelProperty(value = "中文作者单位:英文作者单位:规范单位名称")
	private String cESAuthorUnit ; 

	/** QKID **/
    @ApiModelProperty(value = "QKID")
	private String qkid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String issn ; 

	/** 中文刊名 **/
    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName ; 

	/** 英文刊名 **/
    @ApiModelProperty(value = "英文刊名")
	private String englishJournalName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String journal ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String journalAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String journalAnyname2 ; 

	/** 中文刊名 **/
    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fjoucnAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fjoucnAnyname2 ; 

	/** 年 **/
    @ApiModelProperty(value = "年")
	private String year ; 

	/** 卷 **/
    @ApiModelProperty(value = "卷")
	private String volume ; 

	/** 期 **/
    @ApiModelProperty(value = "期")
	private String period ; 

	/** 页码 **/
    @ApiModelProperty(value = "页码")
	private String pageNumber ; 

	/** 中文栏目名称 **/
    @ApiModelProperty(value = "中文栏目名称")
	private String chineseColumnName ; 

	/** 英文栏目名称 **/
    @ApiModelProperty(value = "英文栏目名称")
	private String englishColumnName ; 

	/** 语种 **/
    @ApiModelProperty(value = "语种")
	private String language ; 

	/** 中图分类号:机标分类号 **/
    @ApiModelProperty(value = "中图分类号:机标分类号")
	private String clsMachineStandardCode ; 

	/** 文献标识码 **/
    @ApiModelProperty(value = "文献标识码")
	private String literatureIdentificationCode ; 

	/** 机标分类号 **/
    @ApiModelProperty(value = "机标分类号")
	private String machineLabelClassCode ; 

	/** 中图分类号 **/
    @ApiModelProperty(value = "中图分类号")
	private String clsCode ; 

	/** 中图分类顶级 **/
    @ApiModelProperty(value = "中图分类顶级")
	private String clsTop ; 

	/** 中图分类(三级) **/
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String iid ; 

	/** 中文关键词 **/
    @ApiModelProperty(value = "中文关键词")
	private String chineseKeyword ; 

	/** 中文关键词:英文关键词:机标关键词 **/
    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String ceKeyword ; 

	/** 英文关键词 **/
    @ApiModelProperty(value = "英文关键词")
	private String englishKeyword ; 

	/** 机标关键词 **/
    @ApiModelProperty(value = "机标关键词")
	private String machineKeyword ; 

	/** 中文摘要 **/
    @ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ; 

	/** 中文摘要:英文摘要:正文首段 **/
    @ApiModelProperty(value = "中文摘要:英文摘要:正文首段")
	private String cParagraph ; 

	/** 英文摘要 **/
    @ApiModelProperty(value = "英文摘要")
	private String englishAbstract ; 

	/** 正文首段 **/
    @ApiModelProperty(value = "正文首段")
	private String firstParagraph ; 

	/** 基金 **/
    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/
    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/** 基金名称 **/
    @ApiModelProperty(value = "基金名称")
	private String fundName ; 

	/** 基金名称 **/
    @ApiModelProperty(value = "基金名称")
	private String fundName2 ; 

	/** 基金项目 **/
    @ApiModelProperty(value = "基金项目")
	private String fundProject ; 

	/** 基金名称 **/
    @ApiModelProperty(value = "基金名称")
	private String fundName3 ; 

	/** BYCS **/
    @ApiModelProperty(value = "BYCS")
	private String bycs ; 

	/** 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊 **/
    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
	private String core ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String forgid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String endorgtype ; 

	/** 全部作者ID **/
    @ApiModelProperty(value = "全部作者ID")
	private String allAuthorId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgNumber ; 

	/** w_ID **/
    @ApiModelProperty(value = "w_ID")
	private String wid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String nid ; 

	/** 作者译名 **/
    @ApiModelProperty(value = "作者译名")
	private String authorTranslatio ; 

	/** 第一作者 **/
    @ApiModelProperty(value = "第一作者")
	private String firstAuthorId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fau ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String faue ; 

	/** 作者姓名:作者译名 **/
    @ApiModelProperty(value = "作者姓名:作者译名")
	private String tNAuthor ; 

	/** 作者单位名称 **/
    @ApiModelProperty(value = "作者单位名称")
	private String authorUnitName ; 

	/** 作者单位规范名称 **/
    @ApiModelProperty(value = "作者单位规范名称")
	private String authorUnitSpecificationName ; 

	/** 作者单位译名 **/
    @ApiModelProperty(value = "作者单位译名")
	private String authorUnitTranslation ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String forg ; 

	/** 作者单位名称 **/
    @ApiModelProperty(value = "作者单位名称")
	private String forgUnitName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String forge ; 

	/** 作者单位名称:作者单位规范名称:作者单位译名 **/
    @ApiModelProperty(value = "作者单位名称:作者单位规范名称:作者单位译名")
	private String uSAuthorAnyname ; 

	/** C_ID **/
    @ApiModelProperty(value = "C_ID")
	private String cid ; 

	/** 学科分类:学科分类机标 **/
    @ApiModelProperty(value = "学科分类:学科分类机标")
	private String disciplineClassCode ; 

	/** 协会级别 **/
    @ApiModelProperty(value = "协会级别")
	private String associationLevel ; 

	/** 中文关键词:英文关键词:机标关键词 **/
    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String cEKeyword ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fAbstract ; 

	/** 中文摘要:英文摘要 **/
    @ApiModelProperty(value = "中文摘要:英文摘要")
	private String cEAbstract ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String lan ; 

	/** 母体文献 **/
    @ApiModelProperty(value = "母体文献")
	private String parentLiterature ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String parentLiteratureName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String parentLiteratureName2 ; 

	/** 学会名称 **/
    @ApiModelProperty(value = "学会名称")
	private String instituteName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String instituteNameAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String snAnyname ; 

	/** 会议名称 **/
    @ApiModelProperty(value = "会议名称")
	private String conferenceTitle ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String conferenceTitleAnyname ; 

	/** 会议届次 **/
    @ApiModelProperty(value = "会议届次")
	private String conferenceSessions ; 

	/** 会议地点 **/
    @ApiModelProperty(value = "会议地点")
	private String conferenceMeetingPlace ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String hid ; 

	/** 主办单位 **/
    @ApiModelProperty(value = "主办单位")
	private String organizer ; 

	/** 基金 **/
    @ApiModelProperty(value = "基金")
	private String fFund ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fpn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fundsupport ; 

	/** 会议时间 **/
    @ApiModelProperty(value = "会议时间")
	private String startMeetingDate ; 

	/** 会议时间 **/
    @ApiModelProperty(value = "会议时间")
	private String endMeetingDate ; 

	/** 出版时间 **/
    @ApiModelProperty(value = "出版时间")
	private String startPublishedDate ; 

	/** 出版时间 **/
    @ApiModelProperty(value = "出版时间")
	private String endPublishedDate ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgnum ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String rn ; 

	/** 作者单位ID **/
    @ApiModelProperty(value = "作者单位ID")
	private String authorUnitId ; 

	/** 数据来源 **/
    @ApiModelProperty(value = "数据来源")
	private String dataFlag ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ecore ; 


}
