package outdto;

import annotation.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文期刊论文
 *
 * @author code_generator
 */

@Data
public class WChineseJournalPaperOutDto implements java.io.Serializable {

    /**
     * 全文索引
     **/

    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 1)
    private String fulltext;

    /**
     * 主题全文检索(标题、关键词、摘要)
     **/

    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    @FieldOrder(order = 2)
    private String topic;

    /**
     * 唯一编号
     **/

    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 3)
    private String wfId;

    /**
     * 记录顺序号
     **/

    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 4)
    private String orderid;

    /**
     * DOI
     **/

    @ApiModelProperty(value = "DOI")
    @FieldOrder(order = 5)
    private String doi;

    /**
     * w_ID
     **/

    @ApiModelProperty(value = "w_ID")
    @FieldOrder(order = 6)
    private String wId;

    /**
     * V_ID
     **/

    @ApiModelProperty(value = "V_ID")
    @FieldOrder(order = 7)
    private String vId;

    /**
     * C_ID
     **/

    @ApiModelProperty(value = "C_ID")
    @FieldOrder(order = 8)
    private String cId;

    /**
     * L_ID
     **/

    @ApiModelProperty(value = "L_ID")
    @FieldOrder(order = 9)
    private String lId;

    /**
     * 中文标题
     **/

    @ApiModelProperty(value = "中文标题")
    @FieldOrder(order = 10)
    private String chineseTitle;

    /**
     * 英文标题
     **/

    @ApiModelProperty(value = "英文标题")
    @FieldOrder(order = 11)
    private String englishTitle;

    /**
     * 中文标题:英文标题
     **/

    @ApiModelProperty(value = "中文标题:英文标题")
    @FieldOrder(order = 12)
    private String cETitle;

    /**
     * 作者ID
     **/

    @ApiModelProperty(value = "作者ID")
    @FieldOrder(order = 13)
    private String authorId;

    /**
     * 中文作者
     **/

    @ApiModelProperty(value = "中文作者")
    @FieldOrder(order = 14)
    private String chineseAuthor;

    /**
     * 英文作者
     **/

    @ApiModelProperty(value = "英文作者")
    @FieldOrder(order = 15)
    private String englishAuthor;

    /**
     * 作者个数
     **/

    @ApiModelProperty(value = "作者个数")
    @FieldOrder(order = 16)
    private String numberAuthors;

    /**
     * 作者FID
     **/

    @ApiModelProperty(value = "作者FID")
    @FieldOrder(order = 17)
    private String authorFid;

    /**
     * 中文第一作者
     **/

    @ApiModelProperty(value = "中文第一作者")
    @FieldOrder(order = 18)
    private String chineseFirstAuthor;

    /**
     * 英文第一作者
     **/

    @ApiModelProperty(value = "英文第一作者")
    @FieldOrder(order = 19)
    private String englishFirstAuthor;

    /**
     * 作者个数
     **/

    @ApiModelProperty(value = "作者个数")
    @FieldOrder(order = 20)
    private String authorNumber;

    /**
     * 中文作者:英文作者
     **/

    @ApiModelProperty(value = "中文作者:英文作者")
    @FieldOrder(order = 21)
    private String chineseEnglishAuthor;

    /**
     * 作者名称(全文检索)
     **/

    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 22)
    private String authorName;

    /**
     * 作者名称(全文检索)
     **/

    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 23)
    private String authorName2;

    /**
     * 规范单位名称
     **/

    @ApiModelProperty(value = "规范单位名称")
    @FieldOrder(order = 24)
    private String authorityUnitName;

    /**
     * 中文作者单位
     **/

    @ApiModelProperty(value = "中文作者单位")
    @FieldOrder(order = 25)
    private String chineseAuthorUnit;

    /**
     * 英文作者单位
     **/

    @ApiModelProperty(value = "英文作者单位")
    @FieldOrder(order = 26)
    private String englishAuthorUnit;

    /**
     * 中文第一作者单位
     **/

    @ApiModelProperty(value = "中文第一作者单位")
    @FieldOrder(order = 27)
    private String chineseFirstAuthorUnit;

    /**
     * 中文第一作者单位一级名称
     **/

    @ApiModelProperty(value = "中文第一作者单位一级名称")
    @FieldOrder(order = 28)
    private String chineseFirstAuthorUnitName;

    /**
     * 英文第一作者单位
     **/

    @ApiModelProperty(value = "英文第一作者单位")
    @FieldOrder(order = 29)
    private String firstEnglishAuthor;

    /**
     * 机构名称
     **/

    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 30)
    private String orgName;

    /**
     * 机构名称(全文检索)
     **/

    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 31)
    private String orgNameSearch;

    /**
     * 中文作者单位:英文作者单位:规范单位名称
     **/

    @ApiModelProperty(value = "中文作者单位:英文作者单位:规范单位名称")
    @FieldOrder(order = 32)
    private String cESAuthorUnit;

    /**
     * QKID
     **/

    @ApiModelProperty(value = "QKID")
    @FieldOrder(order = 33)
    private String qkid;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 34)
    private String issn;

    /**
     * 中文刊名
     **/

    @ApiModelProperty(value = "中文刊名")
    @FieldOrder(order = 35)
    private String chineseJournalName;

    /**
     * 英文刊名
     **/

    @ApiModelProperty(value = "英文刊名")
    @FieldOrder(order = 36)
    private String englishJournalName;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 37)
    private String journal;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 38)
    private String journalAnyname;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 39)
    private String journalAnyname2;

    /**
     * 中文刊名
     **/

    @ApiModelProperty(value = "中文刊名")
    @FieldOrder(order = 40)
    private String chineseJournalName2;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 41)
    private String fjoucnAnyname;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 42)
    private String fjoucnAnyname2;

    /**
     * 日期
     **/

    @ApiModelProperty(value = "日期")
    @FieldOrder(order = 43)
    private String date;

    /**
     * 年
     **/

    @ApiModelProperty(value = "年")
    @FieldOrder(order = 44)
    private String year;

    /**
     * 卷
     **/

    @ApiModelProperty(value = "卷")
    @FieldOrder(order = 45)
    private String volume;

    /**
     * 期
     **/

    @ApiModelProperty(value = "期")
    @FieldOrder(order = 46)
    private String period;

    /**
     * 页码
     **/

    @ApiModelProperty(value = "页码")
    @FieldOrder(order = 47)
    private String pageNumber;

    /**
     * 页数
     **/

    @ApiModelProperty(value = "页数")
    @FieldOrder(order = 48)
    private String pages;

    /**
     * 中文栏目名称
     **/

    @ApiModelProperty(value = "中文栏目名称")
    @FieldOrder(order = 49)
    private String chineseColumnName;

    /**
     * 英文栏目名称
     **/

    @ApiModelProperty(value = "英文栏目名称")
    @FieldOrder(order = 50)
    private String englishColumnName;

    /**
     * 语种
     **/

    @ApiModelProperty(value = "语种")
    @FieldOrder(order = 51)
    private String language;

    /**
     * 中图分类号:机标分类号
     **/

    @ApiModelProperty(value = "中图分类号:机标分类号")
    @FieldOrder(order = 52)
    private String clsMachineStandardCode;

    /**
     * 学科分类
     **/

    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 53)
    private String disciplineClass;

    /**
     * 文献标识码
     **/

    @ApiModelProperty(value = "文献标识码")
    @FieldOrder(order = 54)
    private String literatureIdentificationCode;

    /**
     * 机标分类号
     **/

    @ApiModelProperty(value = "机标分类号")
    @FieldOrder(order = 55)
    private String machineLabelClassCode;

    /**
     * 中图分类号
     **/

    @ApiModelProperty(value = "中图分类号")
    @FieldOrder(order = 56)
    private String clsCode;

    /**
     * 中图分类二级
     **/

    @ApiModelProperty(value = "中图分类二级")
    @FieldOrder(order = 57)
    private String clsLevel2;

    /**
     * 中图分类顶级
     **/

    @ApiModelProperty(value = "中图分类顶级")
    @FieldOrder(order = 58)
    private String clsTop;

    /**
     * 中图分类(三级)
     **/

    @ApiModelProperty(value = "中图分类(三级)")
    @FieldOrder(order = 59)
    private String clsLevel3;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 60)
    private String iid;

    /**
     * 中文关键词
     **/

    @ApiModelProperty(value = "中文关键词")
    @FieldOrder(order = 61)
    private String chineseKeyword;

    /**
     * 中文关键词:英文关键词:机标关键词
     **/

    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
    @FieldOrder(order = 62)
    private String ceKeyword;

    /**
     * 英文关键词
     **/

    @ApiModelProperty(value = "英文关键词")
    @FieldOrder(order = 63)
    private String englishKeyword;

    /**
     * 机标关键词
     **/

    @ApiModelProperty(value = "机标关键词")
    @FieldOrder(order = 64)
    private String machineKeyword;

    /**
     * 中文摘要
     **/

    @ApiModelProperty(value = "中文摘要")
    @FieldOrder(order = 65)
    private String chineseAbstract;

    /**
     * 中文摘要:英文摘要:正文首段
     **/

    @ApiModelProperty(value = "中文摘要:英文摘要:正文首段")
    @FieldOrder(order = 66)
    private String cParagraph;

    /**
     * 英文摘要
     **/

    @ApiModelProperty(value = "英文摘要")
    @FieldOrder(order = 67)
    private String englishAbstract;

    /**
     * 正文首段
     **/

    @ApiModelProperty(value = "正文首段")
    @FieldOrder(order = 68)
    private String firstParagraph;

    /**
     * 基金
     **/

    @ApiModelProperty(value = "基金")
    @FieldOrder(order = 69)
    private String fund;

    /**
     * 基金全文检索
     **/

    @ApiModelProperty(value = "基金全文检索")
    @FieldOrder(order = 70)
    private String fundFulltextSearch;

    /**
     * 基金名称
     **/

    @ApiModelProperty(value = "基金名称")
    @FieldOrder(order = 71)
    private String fundName;

    /**
     * 基金名称
     **/

    @ApiModelProperty(value = "基金名称")
    @FieldOrder(order = 72)
    private String fundName2;

    /**
     * 基金项目
     **/

    @ApiModelProperty(value = "基金项目")
    @FieldOrder(order = 73)
    private String fundProject;

    /**
     * 基金名称
     **/

    @ApiModelProperty(value = "基金名称")
    @FieldOrder(order = 74)
    private String fundName3;

    /**
     * BYCS
     **/

    @ApiModelProperty(value = "BYCS")
    @FieldOrder(order = 75)
    private String bycs;

    /**
     * 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊
     **/

    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
    @FieldOrder(order = 76)
    private String core;

    /**
     * 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊
     **/

    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
    @FieldOrder(order = 77)
    private String score;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 78)
    private String ecore;

    /**
     * 数据来源
     **/

    @ApiModelProperty(value = "数据来源")
    @FieldOrder(order = 79)
    private String dataSource;

    /**
     * 机构层级ID
     **/

    @ApiModelProperty(value = "机构层级ID")
    @FieldOrder(order = 80)
    private String orgHierarchyId;

    /**
     * 机构ID
     **/

    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 81)
    private String orgId;

    /**
     * 机构所在省
     **/

    @ApiModelProperty(value = "机构所在省")
    @FieldOrder(order = 82)
    private String orgProvince;

    /**
     * 机构所在市
     **/

    @ApiModelProperty(value = "机构所在市")
    @FieldOrder(order = 83)
    private String orgCity;

    /**
     * 机构类型
     **/

    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 84)
    private String orgType;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 85)
    private String forgid;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 86)
    private String endorgtype;

    /**
     * 第一机构终级机构ID
     **/

    @ApiModelProperty(value = "第一机构终级机构ID")
    @FieldOrder(order = 87)
    private String orgFristFinalId;

    /**
     * 第一机构层级机构ID
     **/

    @ApiModelProperty(value = "第一机构层级机构ID")
    @FieldOrder(order = 88)
    private String orgFristHierarchyId;

    /**
     * 第一机构所在市
     **/

    @ApiModelProperty(value = "第一机构所在市")
    @FieldOrder(order = 89)
    private String orgFristCity;

    /**
     * 第一机构终级机构所在市
     **/

    @ApiModelProperty(value = "第一机构终级机构所在市")
    @FieldOrder(order = 90)
    private String orgFristFinalCity;

    /**
     * 第一机构所在省
     **/

    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 91)
    private String orgFristProvince;

    /**
     * 第一机构所在省
     **/

    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 92)
    private String orgFristProvince2;

    /**
     * 第一机构终级机构类型
     **/

    @ApiModelProperty(value = "第一机构终级机构类型")
    @FieldOrder(order = 93)
    private String orgFristFinalType;

    /**
     * 第一机构类型
     **/

    @ApiModelProperty(value = "第一机构类型")
    @FieldOrder(order = 94)
    private String orgFristType;

    /**
     * 全部作者ID
     **/

    @ApiModelProperty(value = "全部作者ID")
    @FieldOrder(order = 95)
    private String allAuthorId;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 96)
    private String orgNumber;

    /**
     * 文献权重
     **/

    @ApiModelProperty(value = "文献权重")
    @FieldOrder(order = 97)
    private String literatureWeight;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 98)
    private String quarter;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 99)
    private String halfyear;

    /**
     * 索引管理字段(无用)
     **/

    @ApiModelProperty(value = "索引管理字段(无用)")
    @FieldOrder(order = 100)
    private String errorCode;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 101)
    private String authorInfo;

    /**
     * 作者信息.姓名
     **/

    @ApiModelProperty(value = "作者信息.姓名")
    @FieldOrder(order = 102)
    private String authorInfoName;

    /**
     * 作者信息.作者次序
     **/

    @ApiModelProperty(value = "作者信息.作者次序")
    @FieldOrder(order = 103)
    private String authorInfoOrder;

    /**
     * 作者信息.工作单位
     **/

    @ApiModelProperty(value = "作者信息.工作单位")
    @FieldOrder(order = 104)
    private String authorInfoUnit;

    /**
     * 作者信息.工作单位一级机构
     **/

    @ApiModelProperty(value = "作者信息.工作单位一级机构")
    @FieldOrder(order = 105)
    private String authorInfoUnitOrgLevel1;

    /**
     * 作者信息.工作单位二级机构
     **/

    @ApiModelProperty(value = "作者信息.工作单位二级机构")
    @FieldOrder(order = 106)
    private String authorInfoUnitOrgLevel2;

    /**
     * 作者信息.工作单位类型
     **/

    @ApiModelProperty(value = "作者信息.工作单位类型")
    @FieldOrder(order = 107)
    private String authorInfoUnitType;

    /**
     * 作者信息.工作单位所在省
     **/

    @ApiModelProperty(value = "作者信息.工作单位所在省")
    @FieldOrder(order = 108)
    private String authorInfoUnitProvince;

    /**
     * 作者信息.工作单位所在市
     **/

    @ApiModelProperty(value = "作者信息.工作单位所在市")
    @FieldOrder(order = 109)
    private String authorInfoUnitCity;

    /**
     * 作者信息.工作单位所在县
     **/

    @ApiModelProperty(value = "作者信息.工作单位所在县")
    @FieldOrder(order = 110)
    private String authorInfoUnitCounty;

    /**
     * 作者信息.唯一ID
     **/

    @ApiModelProperty(value = "作者信息.唯一ID")
    @FieldOrder(order = 111)
    private String authorInfoId;

    /**
     * 作者信息.工作单位唯一ID
     **/

    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
    @FieldOrder(order = 112)
    private String authorInfoUnitId;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 113)
    private String orgInfo;

    /**
     * 机构信息.机构名称
     **/

    @ApiModelProperty(value = "机构信息.机构名称")
    @FieldOrder(order = 114)
    private String orgInfoName;

    /**
     * 机构信息.机构次序
     **/

    @ApiModelProperty(value = "机构信息.机构次序")
    @FieldOrder(order = 115)
    private String orgInfoOrder;

    /**
     * 机构信息.机构类型
     **/

    @ApiModelProperty(value = "机构信息.机构类型")
    @FieldOrder(order = 116)
    private String orgInfoType;

    /**
     * 机构信息.省
     **/

    @ApiModelProperty(value = "机构信息.省")
    @FieldOrder(order = 117)
    private String orgInfoProvince;

    /**
     * 机构信息.市
     **/

    @ApiModelProperty(value = "机构信息.市")
    @FieldOrder(order = 118)
    private String orgInfoCity;

    /**
     * 机构信息.县
     **/

    @ApiModelProperty(value = "机构信息.县")
    @FieldOrder(order = 119)
    private String orgInfoCounty;

    /**
     * 机构信息.五级机构层级码
     **/

    @ApiModelProperty(value = "机构信息.五级机构层级码")
    @FieldOrder(order = 120)
    private String orgInfoHierarchy;

    /**
     * 机构信息.1级机构名称
     **/

    @ApiModelProperty(value = "机构信息.1级机构名称")
    @FieldOrder(order = 121)
    private String orgInfoLevel1;

    /**
     * 机构信息.2级机构名称
     **/

    @ApiModelProperty(value = "机构信息.2级机构名称")
    @FieldOrder(order = 122)
    private String orgInfoLevel2;

    /**
     * 机构信息.3级机构名称
     **/

    @ApiModelProperty(value = "机构信息.3级机构名称")
    @FieldOrder(order = 123)
    private String orgInfoLevel3;

    /**
     * 机构信息.4级机构名称
     **/

    @ApiModelProperty(value = "机构信息.4级机构名称")
    @FieldOrder(order = 124)
    private String orgInfoLevel4;

    /**
     * 机构信息.5级机构名称
     **/

    @ApiModelProperty(value = "机构信息.5级机构名称")
    @FieldOrder(order = 125)
    private String orgInfoLevel5;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;
}
