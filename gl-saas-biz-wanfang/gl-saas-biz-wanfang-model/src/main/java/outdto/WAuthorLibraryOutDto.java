package outdto;

import annotation.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 作者库
 *
 * @author code_generator
 */
@Data
public class WAuthorLibraryOutDto implements java.io.Serializable {
    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 3)
    private String wId;

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 4)
    private String fulltext;

    /**
     * 姓名:英文名
     **/
    @ApiModelProperty(value = "姓名:英文名")
    @FieldOrder(order = 5)
    private String aEnglishName;

    /**
     * 工作单位
     **/
    @ApiModelProperty(value = "工作单位")
    @FieldOrder(order = 6)
    private String unit;

    /**
     * 工作单位(全文检索)
     **/
    @ApiModelProperty(value = "工作单位(全文检索)")
    @FieldOrder(order = 7)
    private String unitFulltextSearch;

    /**
     * 工作单位:统计单位名称:英文单位名称
     **/
    @ApiModelProperty(value = "工作单位:统计单位名称:英文单位名称")
    @FieldOrder(order = 8)
    private String sunEnglishUnitName;

    /**
     * 统计单位名称
     **/
    @ApiModelProperty(value = "统计单位名称")
    @FieldOrder(order = 9)
    private String statisticalUnitName;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 10)
    private String orgallAnyname;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 11)
    private String orgallAnyname2;

    /**
     * 一级单位
     **/
    @ApiModelProperty(value = "一级单位")
    @FieldOrder(order = 12)
    private String level1Unit;

    /**
     * 二级单位
     **/
    @ApiModelProperty(value = "二级单位")
    @FieldOrder(order = 13)
    private String level2Unit;

    /**
     * 单位
     **/
    @ApiModelProperty(value = "单位")
    @FieldOrder(order = 14)
    private String wUnit;

    /**
     * 英文单位名称
     **/
    @ApiModelProperty(value = "英文单位名称")
    @FieldOrder(order = 15)
    private String englishUnitName;

    /**
     * 职称
     **/
    @ApiModelProperty(value = "职称")
    @FieldOrder(order = 16)
    private String title;

    /**
     * 简介:备注:工作简历
     **/
    @ApiModelProperty(value = "简介:备注:工作简历")
    @FieldOrder(order = 17)
    private String introductionJobResume;

    /**
     * 简介
     **/
    @ApiModelProperty(value = "简介")
    @FieldOrder(order = 18)
    private String intro;

    /**
     * 姓名
     **/
    @ApiModelProperty(value = "姓名")
    @FieldOrder(order = 19)
    private String name;

    /**
     * 英文名
     **/
    @ApiModelProperty(value = "英文名")
    @FieldOrder(order = 20)
    private String englishName;

    /**
     * 导师
     **/
    @ApiModelProperty(value = "导师")
    @FieldOrder(order = 21)
    private String mentor;

    /**
     * 地区
     **/
    @ApiModelProperty(value = "地区")
    @FieldOrder(order = 22)
    private String region;

    /**
     * 邮编
     **/
    @ApiModelProperty(value = "邮编")
    @FieldOrder(order = 23)
    private String postcode;

    /**
     * 地址
     **/
    @ApiModelProperty(value = "地址")
    @FieldOrder(order = 24)
    private String address;

    /**
     * 邮件地址
     **/
    @ApiModelProperty(value = "邮件地址")
    @FieldOrder(order = 25)
    private String email;

    /**
     * 机构类型
     **/
    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 26)
    private String orgType;

    /**
     * 日期
     **/
    @ApiModelProperty(value = "日期")
    @FieldOrder(order = 27)
    private Date date;

    /**
     * 备注
     **/
    @ApiModelProperty(value = "备注")
    @FieldOrder(order = 28)
    private String note;

    /**
     * 机构ID
     **/
    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 29)
    private String orgId;

    /**
     * 频次
     **/
    @ApiModelProperty(value = "频次")
    @FieldOrder(order = 30)
    private String frequency;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 31)
    private String yfreq;

    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    @FieldOrder(order = 32)
    private String keyword;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 33)
    private String keywordAnyname;

    /**
     * 相关关键词
     **/
    @ApiModelProperty(value = "相关关键词")
    @FieldOrder(order = 34)
    private String relatedKeyword;

    /**
     * 相关期刊
     **/
    @ApiModelProperty(value = "相关期刊")
    @FieldOrder(order = 35)
    private String relatedJournals;

    /**
     * 相关人物
     **/
    @ApiModelProperty(value = "相关人物")
    @FieldOrder(order = 36)
    private String relatedFigure;

    /**
     * 相关学科
     **/
    @ApiModelProperty(value = "相关学科")
    @FieldOrder(order = 37)
    private String relatedDiscipline;

    /**
     * 相关学科
     **/
    @ApiModelProperty(value = "相关学科")
    @FieldOrder(order = 38)
    private String relatedDiscipline2;

    /**
     * 相关基金
     **/
    @ApiModelProperty(value = "相关基金")
    @FieldOrder(order = 39)
    private String relatedFund;

    /**
     * qcode
     **/
    @ApiModelProperty(value = "qcode")
    @FieldOrder(order = 40)
    private String qcode;

    /**
     * 性别
     **/
    @ApiModelProperty(value = "性别")
    @FieldOrder(order = 41)
    private String sex;

    /**
     * 出生日期
     **/
    @ApiModelProperty(value = "出生日期")
    @FieldOrder(order = 42)
    private Date birthDate;

    /**
     * 在职年月
     **/
    @ApiModelProperty(value = "在职年月")
    @FieldOrder(order = 43)
    private Date workingDays;

    /**
     * 出生地点
     **/
    @ApiModelProperty(value = "出生地点")
    @FieldOrder(order = 44)
    private String birthAddress;

    /**
     * 民族
     **/
    @ApiModelProperty(value = "民族")
    @FieldOrder(order = 45)
    private String national;

    /**
     * 工作职务
     **/
    @ApiModelProperty(value = "工作职务")
    @FieldOrder(order = 46)
    private String jobPosition;

    /**
     * 行政码
     **/
    @ApiModelProperty(value = "行政码")
    @FieldOrder(order = 47)
    private String administrativeCode;

    /**
     * 电话
     **/
    @ApiModelProperty(value = "电话")
    @FieldOrder(order = 48)
    private String telphone;

    /**
     * 传真
     **/
    @ApiModelProperty(value = "传真")
    @FieldOrder(order = 49)
    private String fax;

    /**
     * 教育背景
     **/
    @ApiModelProperty(value = "教育背景")
    @FieldOrder(order = 50)
    private String educationBackground;

    /**
     * 最高学位
     **/
    @ApiModelProperty(value = "最高学位")
    @FieldOrder(order = 51)
    private String highestDegree;

    /**
     * 外语语种
     **/
    @ApiModelProperty(value = "外语语种")
    @FieldOrder(order = 52)
    private String foreignLanguages;

    /**
     * 区位
     **/
    @ApiModelProperty(value = "区位")
    @FieldOrder(order = 53)
    private String zoneBit;

    /**
     * 专业领域与研究方向
     **/
    @ApiModelProperty(value = "专业领域与研究方向")
    @FieldOrder(order = 54)
    private String professionalResearch;

    /**
     * 国内外学术或专业团体任职情况
     **/
    @ApiModelProperty(value = "国内外学术或专业团体任职情况")
    @FieldOrder(order = 55)
    private String coEmployment;

    /**
     * 院士
     **/
    @ApiModelProperty(value = "院士")
    @FieldOrder(order = 56)
    private String academician;

    /**
     * 专家荣誉
     **/
    @ApiModelProperty(value = "专家荣誉")
    @FieldOrder(order = 57)
    private String expertsHonor;

    /**
     * 专家荣誉
     **/
    @ApiModelProperty(value = "专家荣誉")
    @FieldOrder(order = 58)
    private String expertsHonor2;

    /**
     * 专家特色
     **/
    @ApiModelProperty(value = "专家特色")
    @FieldOrder(order = 59)
    private String expertsCharacteristics;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 60)
    private String characterAnyname;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 61)
    private String characterAnyname2;

    /**
     * 学科分类名
     **/
    @ApiModelProperty(value = "学科分类名")
    @FieldOrder(order = 62)
    private String disciplineClass;

    /**
     * 学科分类码
     **/
    @ApiModelProperty(value = "学科分类码")
    @FieldOrder(order = 63)
    private String disciplineClassCode;

    /**
     * 学科分类码
     **/
    @ApiModelProperty(value = "学科分类码")
    @FieldOrder(order = 64)
    private String disciplineClassCode2;

    /**
     * 工作简历
     **/
    @ApiModelProperty(value = "工作简历")
    @FieldOrder(order = 65)
    private String jobResume;

    /**
     * 数据来源
     **/
    @ApiModelProperty(value = "数据来源")
    @FieldOrder(order = 66)
    private String dataSource;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 67)
    private String h;

    /**
     * 中文期刊第一作者发文数
     **/
    @ApiModelProperty(value = "中文期刊第一作者发文数")
    @FieldOrder(order = 68)
    private String journalcount;

    /**
     * 总被引次数
     **/
    @ApiModelProperty(value = "总被引次数")
    @FieldOrder(order = 69)
    private String quotecount;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

}
