package outdto;

import annotation.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 外文OA论文
 *
 * @author code_generator
 */
@Data
public class WForeignLanguageOaPaperOutDto implements java.io.Serializable {

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 1)
    private String wfId;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 2)
    private String orderid;

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;

    /**
     * Title_Title
     **/
    @ApiModelProperty(value = "Title_Title")
    @FieldOrder(order = 4)
    private String title;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    @FieldOrder(order = 5)
    private String Abstract;

    /**
     * 作者
     **/
    @ApiModelProperty(value = "作者")
    @FieldOrder(order = 6)
    private String author;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 7)
    private String authorName;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 8)
    private String authorName2;

    /**
     * Discipline_Keywords
     **/
    @ApiModelProperty(value = "Discipline_Keywords")
    @FieldOrder(order = 9)
    private String keywords;

    /**
     * 中图分类
     **/
    @ApiModelProperty(value = "中图分类")
    @FieldOrder(order = 10)
    private String cls;

    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 11)
    private String orgName;

    /**
     * 机构名称(全文检索)
     **/
    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 12)
    private String orgNameSearch;

    /**
     * Creator_ORG
     **/
    @ApiModelProperty(value = "Creator_ORG")
    @FieldOrder(order = 13)
    private String orgCreator;

    /**
     * Date_Issued
     **/
    @ApiModelProperty(value = "Date_Issued")
    @FieldOrder(order = 14)
    private String issuedDate;

    /**
     * Date_Issued
     **/
    @ApiModelProperty(value = "Date_Issued")
    @FieldOrder(order = 15)
    private String issuedYear;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 16)
    private String iid;

    /**
     * IDentifier_DOI
     **/
    @ApiModelProperty(value = "IDentifier_DOI")
    @FieldOrder(order = 17)
    private String doi;

    /**
     * IDentifier_ISSNp
     **/
    @ApiModelProperty(value = "IDentifier_ISSNp")
    @FieldOrder(order = 18)
    private String issnp;

    /**
     * IDentifier_ISSNe
     **/
    @ApiModelProperty(value = "IDentifier_ISSNe")
    @FieldOrder(order = 19)
    private String issne;

    /**
     * Creator_Creator
     **/
    @ApiModelProperty(value = "Creator_Creator")
    @FieldOrder(order = 20)
    private String authorName3;

    /**
     * Source_Source
     **/
    @ApiModelProperty(value = "Source_Source")
    @FieldOrder(order = 21)
    private String joucn;

    /**
     * Source_Source
     **/
    @ApiModelProperty(value = "Source_Source")
    @FieldOrder(order = 22)
    private String fjoucn;

    /**
     * Source_Vol
     **/
    @ApiModelProperty(value = "Source_Vol")
    @FieldOrder(order = 23)
    private String sourceVol;

    /**
     * Source_Issue
     **/
    @ApiModelProperty(value = "Source_Issue")
    @FieldOrder(order = 24)
    private String sourceIssue;

    /**
     * Source_Page
     **/
    @ApiModelProperty(value = "Source_Page")
    @FieldOrder(order = 25)
    private String sourcePage;

    /**
     * Creator_ORG
     **/
    @ApiModelProperty(value = "Creator_ORG")
    @FieldOrder(order = 26)
    private String creatorOrg;

    /**
     * Date_Download
     **/
    @ApiModelProperty(value = "Date_Download")
    @FieldOrder(order = 27)
    private String downloadDate;

    /**
     * Publisher_Publisher
     **/
    @ApiModelProperty(value = "Publisher_Publisher")
    @FieldOrder(order = 28)
    private String publisherPublisher;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 29)
    private String wchinesePaperOaYnfree;

    /**
     * datalink
     **/
    @ApiModelProperty(value = "datalink")
    @FieldOrder(order = 30)
    private String dataLink;

    /**
     * Discipline_SelfFL
     **/
    @ApiModelProperty(value = "Discipline_SelfFL")
    @FieldOrder(order = 31)
    private String selffl;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 32)
    private String zcid;

    /**
     * 学科分类
     **/
    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 33)
    private String disciplineClass;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 34)
    private String tzcid;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 35)
    private String szcid;

    /**
     * 中图分类(三级)
     **/
    @ApiModelProperty(value = "中图分类(三级)")
    @FieldOrder(order = 36)
    private String clsLevel3;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 37)
    private String score;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 38)
    private String rn;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 39)
    private String bn;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 40)
    private String dn;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 41)
    private String ots;

    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;
}
