package com.gl.wanfang.config;

import com.gl.common.MybatisMainConfig;
import com.gl.basis.common.config.CoreMainConfig;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.config.RedisConfig;
import com.gl.basis.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.servlet.MultipartConfigElement;

/**
 * @ClassName: ApplicationConfig
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author
 * @date 2018年12月3日
 * 
 */

@Configuration
@Import(value = { 
		CoreMainConfig.class,
		MybatisMainConfig.class,
		RedisConfig.class
})
public class ApplicationConfig {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

	

	/**
	 * 上传文件限制大小
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// // 设置文件大小限制 ,超了，页面会抛出异常信息，这时候就需要进行异常信息的处理了;
		factory.setMaxFileSize("1000MB"); // KB,MB
		// / 设置总上传数据总大小
		factory.setMaxRequestSize("1000MB");
		return factory.createMultipartConfig();
	}

	@Bean
	public IdWorker idWorkker() {
		return new IdWorker(1, 1);
	}

}
