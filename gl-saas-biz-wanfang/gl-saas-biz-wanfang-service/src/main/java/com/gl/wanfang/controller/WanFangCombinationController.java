package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.ChinesePaperOAService;
import com.gl.wanfang.service.ForeignLanguagePaperOAService;
import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WChinesePaperOaOutDto;
import outdto.WForeignLanguageOaPaperOutDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 万方聚合搜索
 * 聚合搜索专家、专利、企业、机构、知识文献
 * 知识文献：10001,10002,10006,10003,10012
 * 专利：10004,10016
 * 企业：10011
 * 机构：10022
 * 专家：10014
 *
 */
@RestController
@Api(value = "万方",tags = {"聚合搜索专家、专利、企业、机构、知识文献"})
@RequestMapping("/paper")
public class WanFangCombinationController {

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    ForeignLanguagePaperOAService foreignLanguagePaperOAService;

    @Autowired
    ChinesePaperOAService chinesePaperOAService;

//    @ApiOperation(value = "简略信息搜索")
//    @PostMapping("/matchCombinationSimpleInfo")
//    @ResponseBody
//    public String MatchCombinationSimpleInfo(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
//        String indexName = wfParamDTO.getIndexName()==null?"10001,10002,10006,10003,10012,10004,10016,10011,10022,10014":wfParamDTO.getIndexName();
//        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);
//        return data;
//    }

    @ApiOperation(value = "万方外文oA论文检索详细信息")
    @PostMapping("/matchFLPaperOAInfo")
    @ResponseBody
    public  Page<WForeignLanguageOaPaperOutDto>  MatchCombinationDetailedInfo(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        wfParamDTO.setIndexName("10040");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData2(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方外文oA论文简略检索")
    @PostMapping("/matchForeignLanguagePaperOAPOSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WForeignLanguageOaPaperOutDto> matchForeignLanguagePaperOAPOSearch(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request)  {
        String indexName = wfParamDTO.getIndexName()==null?"10040":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        return getData2(wfParamDTO,response, request,indexName,data);

    }

    @ApiOperation(value = "万方外文oA论文批量详情检索")
    @PostMapping("/matchForeignLanguagePaperOAPOInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WForeignLanguageOaPaperOutDto> matchForeignLanguagePaperOAPOInfo(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10040":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData2(wfParamDTO,response, request,indexName,result);

    }

    private Page<WForeignLanguageOaPaperOutDto> getData2(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WForeignLanguageOaPaperOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WForeignLanguageOaPaperOutDto> authorLibraryPOList = new ArrayList<>();
            try {
                authorLibraryPOList = foreignLanguagePaperOAService.flagStatusComponent(result);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

    @ApiOperation(value = "万方中文oA论文简略检索")
    @PostMapping("/matchChinesePaperOASearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WChinesePaperOaOutDto> matchChinesePaperOASearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10037":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        return getData(wfParamDTO,response, request,indexName,data);
    }

    @ApiOperation(value = "万方中文oA论文详细检索")
    @PostMapping("/matchChinesePaperOA")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WChinesePaperOaOutDto> matchChinesePaperOAInfoData(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10037");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方中文oA论文批量详情检索")
    @PostMapping("/matchChinesePaperOAInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WChinesePaperOaOutDto> matchChinesePaperOAInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10037":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData(wfParamDTO,response, request,indexName,result);
    }

    private Page<WChinesePaperOaOutDto> getData(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WChinesePaperOaOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WChinesePaperOaOutDto> wChinesePaperOaOutDtos = new ArrayList<>();
            try {
                wChinesePaperOaOutDtos = chinesePaperOAService.flagStatusComponent(result);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            page.setList(wChinesePaperOaOutDtos);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }
}
