package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.core.*;
import com.gl.wanfang.service.IDataSynchronizationService;
import com.gl.wanfang.util.ConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class DataSynchronizationService implements IDataSynchronizationService {

    @Autowired
    AuthorLibraryMapper authorLibraryMapper;
    @Autowired
    ChineseConferencePaperMapper chineseConferencePaperMapper;
    @Autowired
    ChineseJournalPaperMapper chineseJournalPaperMapper;
    @Autowired
    ChinesePatentMapper chinesePatentMapper;
    @Autowired
    ChinesePaperOAMapper chinesePaperOAMapper;
    @Autowired
    EnterpriseProductDatabaseMapper enterpriseProductDatabaseMapper;
    @Autowired
    ExpertsLibraryMapper expertsLibraryMapper;
    @Autowired
    ForeignLanguagePaperOAMapper foreignLanguagePaperOAMapper;
    @Autowired
    ForeignPeriodicalPaperMapper foreignPeriodicalPaperMapper;
    @Autowired
    HigherLearningUniversitiesMapper higherLearningUniversitiesMapper;
    @Autowired
    InfoOrgMapper infoOrgMapper;
    @Autowired
    LawsRegulationsMapper lawsRegulationsMapper;
    @Autowired
    OverseasPatentMapper overseasPatentMapper;
    @Autowired
    ScientificOrgMapper scientificOrgMapper;
    @Autowired
    TechnologyAchievementsMapper technologyAchievementsMapper;
    
    public List<?> ALDataSyn(String resourceCls){
        //获取前一天的时间
        Map<String, String> map = ConvertUtil.ZeroTime();
        String yesterday = map.get("yesterday");

        if ("10001".equals(resourceCls)){//期刊
            return  chineseJournalPaperMapper.CJPSelectNewData(yesterday);
        }else if("10013".equals(resourceCls)){//高等院校
            return higherLearningUniversitiesMapper.HLUSelectNewData(yesterday);
        }else if("10040".equals(resourceCls)){//万方外文oA论文
            return  foreignLanguagePaperOAMapper.FLPOASelectNewData(yesterday);
        }else if("10037".equals(resourceCls)){//万方中文oA论文
            return  chinesePaperOAMapper.CPOSelectNewData(yesterday);
        }else if("10011".equals(resourceCls)){//万方企业
            return  enterpriseProductDatabaseMapper.EPDSelectNewData(yesterday);
        }else if("10014".equals(resourceCls)){//万方专家信息
            return  expertsLibraryMapper.ELSelectNewData(yesterday);
        }else if("10020".equals(resourceCls)){//万方作者信息
            return authorLibraryMapper.ALSelectNewData(yesterday);
        }else if("10012".equals(resourceCls)){//万方法律法规
            return lawsRegulationsMapper.LRSelectNewData(yesterday);
        }else if("10022".equals(resourceCls)){//万方机构
            return scientificOrgMapper.SOSelectNewData(yesterday);
        }else if("10033".equals(resourceCls)){//万方信息机构信息
            return infoOrgMapper.IOSelectNewData(yesterday);
        }else if("10004".equals(resourceCls)){//万方中文专利信息
            return chinesePatentMapper.CPSelectNewData(yesterday);
        }else if("10016".equals(resourceCls)){//万方海外专利信息
            return overseasPatentMapper.OPSelectNewData(yesterday);
        }else if("10021".equals(resourceCls)){//万方外文期刊论文
            return foreignPeriodicalPaperMapper.FPPSelectNewData(yesterday);
        }else if("10002".equals(resourceCls)){//万方中文会议论文
            return  chineseConferencePaperMapper.CCPSelectNewData(yesterday);
        }else if("10006".equals(resourceCls)){//万方科技成果信息
            return  technologyAchievementsMapper.TASelectNewData(yesterday);
        }
        return  null;

    }

}
