package com.gl.wanfang.controller.component;

import com.github.pagehelper.PageInfo;
import com.gl.wanfang.service.component.StandardConfigService;
import dto.StandardCreateDTO;
import dto.StandardQueryDTO;
import dto.StandardUpdateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import po.StandardConfigPO;

import java.util.Map;

@RestController()
@Api(value = "标准化构件配置")
@RequestMapping("/standard")
public class StandardConfigApi {
    @Autowired
    private StandardConfigService standardConfigService;

    @PostMapping("/update")
    @ApiOperation(value = "更新")
    public String update(@RequestBody StandardUpdateDTO dto) throws Exception {
        return standardConfigService.update(dto);
    }

    @PostMapping("/insert")
    @ApiOperation(value = "新增")
    public String update(@RequestBody StandardCreateDTO dto) throws Exception {
        return standardConfigService.insert(dto);
    }

    @PostMapping("/query")
    @ApiOperation(value = "获取全部数据")
    public PageInfo<StandardConfigPO> get(@RequestBody StandardQueryDTO dto) {
        return standardConfigService.getStanardConfig(dto);
    }
}
