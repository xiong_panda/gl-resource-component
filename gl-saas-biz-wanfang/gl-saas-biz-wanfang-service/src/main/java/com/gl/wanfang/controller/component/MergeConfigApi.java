package com.gl.wanfang.controller.component;

import com.github.pagehelper.PageInfo;
import com.gl.wanfang.service.component.MergeConfigService;
import dto.IdDTO;
import dto.QueryStandardDTO;
import dto.TableCreateDTO;
import dto.TableNameDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@Api(value = "融合构件管理")
@RequestMapping("/merge")
public class MergeConfigApi {
    @Autowired
    private MergeConfigService mergeConfigService;

    @PostMapping("/queryStandardTables")
    @ApiOperation(value = "查询标准数据库中的表")
    public PageInfo<Map<String, Object>> queryStandardTables(@RequestBody QueryStandardDTO dto) {
        return mergeConfigService.queryStandardTables(dto);
    }

    @PostMapping("/queryStandardFields")
    @ApiOperation(value = "查询标准数据表的字段")
    public List<Map<String, Object>> queryStandardTables(@RequestBody IdDTO<String> dto) {
        return mergeConfigService.queryStandardField(dto.getId());
    }

    @PostMapping("/create")
    @ApiOperation(value = "标准库创建")
    public String create(@RequestBody TableCreateDTO dto) throws Exception {
        return mergeConfigService.createTable(dto);
    }

    @PostMapping("/update")
    @ApiOperation(value = "标准库更新")
    public String update(@RequestBody TableCreateDTO dto) throws Exception {
        return mergeConfigService.createTable(dto);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "标准库删除")
    public String delete(@RequestBody TableNameDTO<String> dto) throws Exception {
        return mergeConfigService.deleteTable(dto.getTableName());
    }

    @PostMapping("/querySubjectTables")
    @ApiOperation(value = "查询主题数据库中的表")
    public List<Map<String, Object>> querySubjectTables() {
        return mergeConfigService.querySubjectTables();
    }

    @PostMapping("/queryOriginalField")
    @ApiOperation(value = "查询主题数据表的对应字段信息")
    public List<Map<String, Object>> queryOriginalTables(@RequestBody TableNameDTO<String> dto) {
        return mergeConfigService.querySubjectField(dto.getTableName());
    }

}
