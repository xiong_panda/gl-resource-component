package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.util.StringUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.service.impl.DataSynchronizationService;
import en.EOutApiErrorCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/back")
@Api(value = "数据同步",tags = {"科技资源平台城市群资源回调"})
public class DataSynchronizationContoller  {

    @Autowired
    DataSynchronizationService dataSynchronizationService;


    @ApiOperation(value = "数据同步")
    @PostMapping("/DataCallback")
    @ResponseBody
    public CityOutResult ALDataCallback(@RequestBody CommBean commBean){
        String resourceCls;
        CityOutResult cityOutResult;
        if (commBean != null && !StringUtils.isEmpty(commBean.getBz_data())) {
            cityOutResult = new CityOutResult();
            resourceCls =JSONObject.parseObject(commBean.getBz_data(), Map.class).get("resourceCls").toString();
            if (resourceCls == null || resourceCls=="") {
                throw new BusinessException(resourceCls+ "-" + EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(resourcesCls)", EOutApiErrorCode.PARAM_ERR.code);
            }
        } else {
            throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(json)", EOutApiErrorCode.PARAM_ERR.code);
        }
        List<?> objects = dataSynchronizationService.ALDataSyn(resourceCls);
        cityOutResult.setResult(objects);
        cityOutResult.setTotalSize(objects.size());
        return cityOutResult;
    }

}
