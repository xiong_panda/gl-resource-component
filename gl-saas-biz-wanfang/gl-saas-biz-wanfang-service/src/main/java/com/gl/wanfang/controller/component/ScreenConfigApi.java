package com.gl.wanfang.controller.component;

import com.gl.wanfang.service.component.ScreenConfigService;
import dto.ScreenCreateDTO;
import dto.ScreenUpdateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController()
@Api(value = "筛选构件配置")
@RequestMapping("/screen")
public class ScreenConfigApi {
    @Autowired
    private ScreenConfigService screenConfigService;

    @PostMapping("/update")
    @ApiOperation(value = "更新")
    public String update(@RequestBody ScreenUpdateDTO dto) throws Exception {
        return screenConfigService.update(dto);
    }

    @PostMapping("/insert")
    @ApiOperation(value = "新增")
    public String update(@RequestBody ScreenCreateDTO dto) throws Exception {
        return screenConfigService.insert(dto);
    }

    @PostMapping("/query")
    @ApiOperation(value = "获取全部数据")
    public List<Map<String, Object>> get() {
        return screenConfigService.getScreenConfig();
    }
}
