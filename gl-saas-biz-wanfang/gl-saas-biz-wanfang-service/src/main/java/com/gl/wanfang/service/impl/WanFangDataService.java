package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.util.StringUtils;
import com.gl.basis.util.RedisUtil;
import com.gl.wanfang.feign.GuoLongClient;
import com.gl.wanfang.mapper.core.PageInfoMapper;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.util.ConvertUtil;
import com.gl.wanfang.util.HttpClientService;
import com.gl.wanfang.util.StrUtil;
import com.gl.wanfang.util.downFileUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dto.WFParamDTO;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import po.PageInfoPO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class WanFangDataService implements IWanFangDataService {
    /*
    *索引库对照表
    *10001;wf_mds_chn_qikan;中文期刊论文
    *10002;wf_mds_chn_huiyi;中文会议论文
    *10003;wf_mds_chn_xuewei;中文学位论文
    *10004;wf_mds_chn_zhuanli;中国专利
    *10005;wf_mds_chn_biaozhun;中外标准
    *10006;wf_mds_chn_cstad;科技成果
    *10007;wf_mds_baike;百科词条
    *10008;wf_mds_thesaurus;叙词表
    *10009;wf_mds_ttauthor;智库作者
    *10010;wf_mds_ttnews;智库新闻
    *10011;wf_mds_cecdb;企业产品数据库
    *10012;wf_mds_chn_claw;法律法规
    *10013;wf_mds_daxue;高等院校
    *10014;wf_mds_experts;专家库
    *10015;wf_mds_project;项目库
    *10016;wf_mds_en_zhuanli;海外专利
    *10017;wf_mds_ttorg;智库机构
    *10018;cstpcd_en_lw;CSTPCD 英文论文
    *10019;wf_mds_en_huiyi;外文会议
    *10020;wf_mds_auth_new;作者库
    *10021;wf_mds_en_qikan;外文期刊论文
    *10022;wf_mds_csi;科研机构
    *10023;wf_mds_bid;公告信息
    *10024;cstpdc_en_lw;
    *10025;wf_mds_meeting;会议名录
    *10026;wf_mds_journal;刊名
    *10027;wf_mds_dic_all;英汉词表
    *10028;wf_mds_chn_yinwen;中文会议引文
    */

    @Autowired
    RedisUtil redisUtil ;
    @Autowired
    GuoLongClient guoLongClient ;

    @Autowired
    PageInfoMapper pageInfoMapper ;

    private static final Logger logger = LoggerFactory.getLogger(WanFangDataService.class);



    @Override
    public void ckeyCon(String wfmds, String wid, String docTI, HttpServletResponse response, HttpServletRequest request) {
        try {

            downFileUtil.downFile(wfmds,wid,"1",docTI,request,response);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String creatToken(String appid, HttpServletResponse response, HttpServletRequest request) {
        String data=null;
        try {
            if(null!=redisUtil.get(appid)){
                data = redisUtil.get(appid).toString();
            }else{
                //http://api.kefuju.cn/claimToken?name=chengduguolong&password=chengduguolong&api_id=99003
                data = downFileUtil.gettoken(appid, request, response);
                redisUtil.set(appid,data,180L);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("token:"+data);

        return data;
    }


    @Override
    public String StatisticalRetrieval(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String indexName) {

        String filter =wfParamDTO.getFilter();//
//        String indexName  = wfParamDTO.getIndexName();//索引编码，逗号分隔
        String existFields  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();
        String must  =wfParamDTO.getMust();

        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
        String format  = wfParamDTO.getFormat();// 不存在的字段，逗号分隔
        String range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String should = wfParamDTO.getShould();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//
        String gn   = wfParamDTO.getGn();//



        //1、读取初始URL
        String urlNameString = "CountSearch";
        StringBuffer sb = new StringBuffer();
        sb.append("?indexName=" + indexName).append(existFields==null?"":"&existFields="+existFields).append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(range==null?"":"&range="+range).append(filter==null?"":"&filter="+filter.replace("&","%26")).append(must==null?"":"&must="+must).append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22"))
                .append(should==null?"":"&should="+should).append(format==null?"":"&format="+format);

        if ("true".equals(gn)|| StringUtils.isEmpty(indexName)){
            PageInfoPO pip=new PageInfoPO();
            Integer page = pageInfoMapper.SelectPageNum(indexName);
            page=(page==null?1:page);
            sb= new StringBuffer("");
            sb.append(indexName==null?"":"?indexName="+indexName);
        }
        String data = requestData(response,request,sb.toString(),urlNameString,"99004");
        return data;
    }

    @Override
    public String StatisticalRetrieval(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String indexName  = wfParamDTO.getIndexName();//索引编码，逗号分隔
        return StatisticalRetrieval(wfParamDTO,response, request, indexName);
    }

    @Override
    public String AggregationSearch(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) {

        //1、读取初始URL

        String urlNameString = "AggregationSearch?indexName=" + wfParamDTO.getIndexName();
        StringBuffer sb = new StringBuffer();
        sb.append(wfParamDTO.getExistFields()==null?"":"&existFields="+wfParamDTO.getExistFields())
                .append(wfParamDTO.getNoExistFields()==null?"":"&noExistFields="+wfParamDTO.getNoExistFields())
                .append(wfParamDTO.getRange()==null?"":"&range="+wfParamDTO.getRange())
                .append(wfParamDTO.getFilter()==null?"":"&filter="+wfParamDTO.getFilter().replace("&","%26"))
                .append(wfParamDTO.getAggFields()==null?"":"&aggFields="+wfParamDTO.getAggFields())
                .append(wfParamDTO.getSubAggFields()==null?"":"&subAggFields="+wfParamDTO.getSubAggFields())
                .append(wfParamDTO.getAggSize()==null?"":"&aggSize="+wfParamDTO.getAggSize())
                .append(wfParamDTO.getSubAggSize()==null?"":"&subAggSize="+wfParamDTO.getSubAggSize())
                .append(wfParamDTO.getMust()==null?"":"&must"+wfParamDTO.getMust())
                .append(wfParamDTO.getMustNot()==null?"":"&mustNot="+wfParamDTO.getMustNot().replace("\"","%22"))
                .append(wfParamDTO.getShould()==null?"":"&should="+wfParamDTO.getShould())
                .append(wfParamDTO.getFormat()==null?"":"&format="+wfParamDTO.getFormat());
        String data = requestData(response,request,sb.toString(),urlNameString,"99002");
        return data;
    }




    @Override
    @Transactional
    public String matchSeach(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request,String indexName) {

        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title =wfParamDTO.getFilter();
////        String indexName  = wfParamDTO.getIndexName();//索引编码，逗号分隔
//        String excludes  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
//        String from  =  wfParamDTO.getFrom();;
//        String size  = wfParamDTO.getSize();
//        String mustNot  = wfParamDTO.getMustNot();
//        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
//        String Range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
//        String scroll  = request.getParameter("scroll");//
//        String scrollId   = wfParamDTO.getScrollId();//
//        String gn= wfParamDTO.getGn();//

        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
//        String sortField  = wfParamDTO.getSortField();
//        String sortField  = "DATE.D";//排序
        String data ="";
        if ("true".equals(wfParamDTO.getGn())||indexName==null){
            matchSeachgn(wfParamDTO,response, request, indexName);
            return data;
        }
        param.append(title==null?"":"&filter="+title.replace("&","%26")).
                append(indexName==null?"":"&indexName="+indexName).
                append(wfParamDTO.getExcludes()==null?"":"&excludes="+wfParamDTO.getExcludes()).
                append(wfParamDTO.getFrom()==null?"":"&from="+wfParamDTO.getFrom()).
                append(wfParamDTO.getSize()==null?"":"&size="+wfParamDTO.getSize()).
                append(wfParamDTO.getMustNot()==null?"":"&mustNot="+wfParamDTO.getMustNot().replace("\"","%22")).
                append(wfParamDTO.getNoExistFields()==null?"":"&noExistFields="+wfParamDTO.getNoExistFields())
                .append(wfParamDTO.getRange()==null?"":"&Range="+wfParamDTO.getRange())
                .append(wfParamDTO.getSortField()==null?"":"&sortField="+wfParamDTO.getSortField())
                .append(wfParamDTO.getScroll() ==null?"":"&scroll="+wfParamDTO.getScroll()  )
                .append(wfParamDTO.getScrollId() ==null?"":"&scrollId ="+wfParamDTO.getScrollId() )
                .replace(0,1,"?");


        data = requestData(response,request,param.toString(),url,"99001");

        logger.info("HttpInfoDeatail:"+data);


        return data;
    }

    @Override
    public String matchSeachgn(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request,String indexName) {
        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
//        String indexName  = wfParamDTO.getIndexName();//索引编码，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String size  =wfParamDTO.getSize();

        param.append(indexName==null?"":"&indexName="+indexName).
                append(from==null?"":"&from="+from).
                append(size==null?"":"&size="+size)
                .replace(0,1,"?");
        String data="";
        //是否是调度平台调用接口

        PageInfoPO pip=new PageInfoPO();
        Integer page = pageInfoMapper.SelectPageNum(indexName);
        if(page==null){
            //查询出所有条数
            String resultJson = StatisticalRetrieval( wfParamDTO, response, request,indexName);
            String count="";
            Map parse = (Map) JSON.parse(resultJson);
            if("200".equals(parse.get("code").toString())){
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = data1.get(0).get("value").toString();
            }
            pip.setIndexname(indexName);
            pip.setSize(10);
            pip.setPage(0);
            pip.setCurrentData(0);
            pip.setCount(Integer.parseInt(count==""?"0":count));
            pip.setFrom(1);
            pageInfoMapper.insertData(pip);
        }
        page=(page==null?1:page);
        param=new StringBuilder("");
        param.append(indexName==null?"":"?indexName="+indexName)
                .append("&from="+(page*10))
                .append("&size=10");
        data = requestData(response,request,param.toString(),url,"99001");
        Map dataParse = (Map) JSON.parse(data);
        if("200".equals(dataParse.get("code").toString())){
            pip=new PageInfoPO();
            pip.setIndexname(indexName);
            //更新执行的位置
            pip.setCurrentData((page+1)*10);//todo： 调用统计接口跟新count数据
            pip.setPage(page+1);
            pip.setSize(10);
            pageInfoMapper.batchUpdate(pip);
        }

        return data;

    }


    @Override
    public String matchSeach(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) {
        String indexName  = wfParamDTO.getIndexName();//索引编码，逗号分隔
        return matchSeach( wfParamDTO, response, request,indexName);

    }

    @Override
    public List<Map<String, Object>> menuSearch(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String data=null;
        return null;
    }

    @Override
    public String idsSearch(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {

        String url="idsSearch";

        StringBuilder param=new StringBuilder();
        String indexName = wfParamDTO.getIndexName();// 索引编码，逗号分隔。必填 参数
        String ids = wfParamDTO.getIds();//文档 id
        String format  =wfParamDTO.getFormat();//返回数字格式，目前默认仅 支持 JSON
        param.append(indexName==null?"":"&indexName="+indexName).
                append(ids==null?"":"&ids="+ids).
                append(format==null?"":"&format="+format).
                replace(0,1,"?")
        ;
        String data = requestData(response,request,param.toString(),url,"99003");
        return data;
    }



    @Override
    public String idsSearchs(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request,String ids) {
        String indexName =wfParamDTO.getIndexName();// 索引编码，逗号分隔。必填 参数
        return idsSearchs(wfParamDTO, response,  request, ids,indexName);
    }



    public String idsSearchs(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request,String ids,String indexName) {
        String url="idsSearch";
        StringBuilder param=new StringBuilder();
        String format  =wfParamDTO.getFormat();//返回数字格式，目前默认仅 支持 JSON
//        如果多次ids查询，则循环执行ids接口
        String[] splitIDS = ids.split("/");
        String data ="";
        String dat2="";
        StringBuilder dat3=new StringBuilder();
        for (int i=0;i<splitIDS.length;i++) {
            param=new StringBuilder("");
            param.append(indexName==null?"":"&indexName="+indexName)
                    .append(format==null?"":"&format="+format)
                    .append(splitIDS[i]==null?"":"&ids="+splitIDS[i])
                    .replace(0,1,"?");
            data=requestData(response,request,param.toString(),url,"99003");
            String da2=ConvertUtil.hebing(data);

            if (!StringUtils.isEmpty(da2)) {
                dat3.append(","+da2.substring(1, da2.length() - 1));
            }
        }
        //超过10条信息时数据处理
        Map map= (Map) JSON.parse(data);
        if("200".equals(map.get("code")+"")){
            Map map2 = (Map)map.get("data");
            map2.put("sources", JSON.parse("[" +dat3.toString().substring(1).toString()+"]"));
            map.put("data",map2);
        }
        JSONObject jsonObject = JSONObject.fromObject(map);
        logger.info("HttpDetail:"+jsonObject.toString());
        return jsonObject.toString();
    }





    @Override
    public String knowledgeliterature(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {

        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title = wfParamDTO.getFilter();//
        String excludes  =wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();
//        String indexName  = wfParamDTO.getIndexName();

        String noExistFields  =wfParamDTO.getNoExistFields() ;// 不存在的字段，逗号分隔
        String Range =wfParamDTO.getRange() ;//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//

        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
        String sortField  = wfParamDTO.getSortField();
        //索引库限制在 "10001","10002","10016","10004","10006","10003" 知识文献
//        String[] indexNameArr=indexName.split(",");
//        String[] indexName={"10001","10002","10016","10004","10006","10003","10012"};
          String indexName="10001,10002,10006,10003,10012";
//        indexName = StrUtil.IndexLibrary(indexN, indexNameArr);

        param.append(title==null?"":"&filter="+title.replace("&","%26")).
                append("&indexName="+indexName).
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?");

        String data = requestData(response,request,param.toString(),url,"99001");
        return data;

    }

    @Override
    public String companyProfile(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title = wfParamDTO.getFilter();//
        String excludes  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();

        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
        String Range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//

        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
        String sortField  = wfParamDTO.getSortField();

        param.append(title==null?"":"&filter="+title.replace("&","%26")).
                append("&indexName="+"10011").//索引库限制为10011
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?")
        ;
        String data = requestData(response,request,param.toString(),url,"99001");
        return data;
    }

    @Override
    public String companyIds(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {

        String url="idsSearch";
        //将获取的数据放入stringbuild拼接字符串发送给万方
        StringBuilder param=new StringBuilder();
        String ids =wfParamDTO.getIds();//文档 id
        String format  = wfParamDTO.getFormat();//返回数字格式，目前默认仅 支持 JSON
        param.append("&indexName="+"10011").
                append(ids==null?"":"&ids="+ids).
                append(format==null?"":"&format="+format).
                replace(0,1,"?");

        String data = requestData(response,request,param.toString(),url,"99003");
        return data;
    }

    @Override
    public String companyTechnology(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title = wfParamDTO.getFilter();//
        String excludes  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();
        String indexName  = wfParamDTO.getIndexName();

        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
        String Range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//

        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
        String sortField  = wfParamDTO.getSortField();

        //索引库限制在 "10001","10002","10016","10004","10006","10003" 知识文献
        String[] indexNameArr=indexName.split(",");
        indexName="";
        String[] indexN={"10001","10002","10016","10004","10006","10003"};
        for(int i=0;i<indexN.length;i++){
            for (int j=0;j<indexNameArr.length;j++){
                if(indexN[i].equals(indexNameArr[j])){
                    indexName+=indexNameArr[j]+",";

                }
            }
        }
        indexName=StrUtil.trimBothEndsChars(indexName, ",");

        param.append(title==null?"":"&filter="+title.replace("&","%26")).
                append("&indexName="+indexName).
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?");
        String data = requestData(response,request,param.toString(),url,"99001");
        return data;
    }

    @Override
    public String companyScholar(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title = wfParamDTO.getFilter().replace("&","%26");//
        String excludes  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();

        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
        String Range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//

        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
        String sortField  = wfParamDTO.getSortField();

        param.append(title==null?"":"&filter="+title).
                append("&indexName="+"10014,10020").
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?");

        String data = requestData(response,request,param.toString(),url,"99001");
        return data;

    }

    @Override
    public String companyScholarIds(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {

        String url="idsSearch";
//        将获取的数据放入stringbuild拼接字符串发送给万方
        StringBuilder param=new StringBuilder();
        String ids =wfParamDTO.getIds();//文档 id
        String format  = wfParamDTO.getFormat();//返回数字格式，目前默认仅 支持 JSON
        param.append("&indexName="+"10014,10020").
                append(ids==null?"":"&ids="+ids).
                append(format==null?"":"&format="+format).
                replace(0,1,"?")
        ;
        String data = requestData(response,request,param.toString(),url,"99003");
        return data;
    }

    @Override
    public String scholarTechnologySearch(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title = wfParamDTO.getFilter().replace("&","%26");//
        String excludes  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();

        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
        String Range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//
        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */

        String sortField  = wfParamDTO.getSortField();

        param.append(title==null?"":"&filter="+title).
                append("&indexName="+"10006").
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?");

        String data = requestData(response,request,param.toString(),url,"99001");
        return data;

    }

    @Override
    public String scholarThesisSearch(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String url="PreciseSearch";

        StringBuilder param=new StringBuilder();
        String title = wfParamDTO.getFilter().replace("&","%26");//
        String excludes  = wfParamDTO.getExcludes();// 返回结果排除字段，逗号分隔
        String from  =  wfParamDTO.getFrom();;
        String mustNot  = wfParamDTO.getMustNot();
        String indexName  = wfParamDTO.getIndexName();

        String noExistFields  = wfParamDTO.getNoExistFields();// 不存在的字段，逗号分隔
        String Range = wfParamDTO.getRange();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = wfParamDTO.getScroll();//
        String scrollId   = wfParamDTO.getScrollId();//
        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
        String sortField  = wfParamDTO.getSortField();
        //索引库限制在 "10001","10002","10016","10004","10006","10003" 知识文献
        String[] indexNameArr=indexName.split(",");
        String[] indexN={"10001","10002","10016","10004","10006","10003"};
        indexName = StrUtil.IndexLibrary(indexN, indexNameArr);

        param.append(title==null?"":"&filter="+title).
                append("&indexName="+indexName).
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?");
        String data = requestData(response,request,param.toString(),url,"99001");
        return data;
    }




    //获取数据，发送请求
    public String requestData(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid){

        String data = null;
        if("99001".equals(appid)) {
            if (null != redisUtil.get(param)) {
                data = redisUtil.get(param).toString();
                Long count = String.valueOf(redisUtil.get("count" + param)) == "null" ? 1 : Long.valueOf(String.valueOf(redisUtil.get("count" + param))) + 1;
                redisUtil.set("count" + param, count, 604800L);
            } else {
                try {
                    Gson gson = new Gson();
                    Map map = gson.fromJson(creatToken(appid, response, request), new TypeToken<Map<String, String>>() {
                    }.getType());
                    String token = map.get("token").toString();
                    data = HttpClientService.sendGet(url + param, token);
                    redisUtil.set(param, data, 36000L);
                    Long count = String.valueOf(redisUtil.get("count" + param)) == "null" ? 1 : Long.valueOf(String.valueOf(redisUtil.get("count" + param))) + 1;
                    redisUtil.set("count" + param, count, 604800L);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else {
            Gson gson = new Gson();
            Map map = gson.fromJson(creatToken(appid, response, request), new TypeToken<Map<String, String>>() {
            }.getType());
            String token = map.get("token").toString();
            try {
                data = HttpClientService.sendGet(url + param, token);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        logger.info("HttpInfo:"+data);

        return data;
    }


    /**
     * 处理数据格式，请求详情查询接口
     * xiaweijie
     * @param response
     * @param request
     * @param data
     * @return
     */
    @Override
    public String IdsInfo(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request,String data,String indexName){
        StringBuilder stringBuilder=new StringBuilder();
        Map result = ((Map)JSON.parse(data));
        //{"code":105,"message":"以下索引库无权限：1002"}
        //处理查询数据不成功
        if(!result.get("code").equals(200)){
            return result.toString();
        }
        Object o = result.get("data");//{}
        Map parse = (Map)JSON.parse(o.toString());
        if(parse.size()==0){
            return data;
        }
        List<Map> sources = (List<Map>)parse.get("sources");
        int i=0;
        for (Map maps : sources) {
            Map source =(Map) maps.get("source");
            Object id = source.get("ID");
            i++;
            if(i%10==0){
                stringBuilder.append(id).append("/");
            }else{
                stringBuilder.append(id).append(",");
            }
        }
        String ids = stringBuilder.substring(0, stringBuilder.length() - 1);
        String value = idsSearchs(wfParamDTO,response, request, ids,indexName);

        return ConvertUtil.rmSpecialChar(value);
    }

    @Override
    public String IdsInfo(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String data) {
        String indexName  = wfParamDTO.getIndexName();//索引编码，
        return IdsInfo(wfParamDTO, response,request,data,indexName);
    }

}
