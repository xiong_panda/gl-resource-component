package com.gl.wanfang.service.component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.common.util.RandomUtil;
import com.gl.wanfang.mapper.standard.TableManageMapper;
import com.gl.wanfang.mapper.subject.SubjectTableManageMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import dto.QueryStandardDTO;
import dto.TableCreateDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import dto.TableDetailDTO;
import po.TableDetailPO;
import po.TablePO;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class MergeConfigService {
    @Autowired
    private TableManageMapper tableManageMapper;
    @Autowired
    private SubjectTableManageMapper subjectTableManageMapper;

    private static final Pattern linePattern = Pattern.compile("_(\\w)");


    private static final IdWorker idWorker = new IdWorker();

    @Transactional(value = "transactionManager4", rollbackFor = Exception.class)
    public String createTable(TableCreateDTO dto) throws Exception {
        String returnMsg = "创建成功";
        if (StringUtils.isEmpty(dto.getOldId())) {
            if (tableManageMapper.verify(dto.getTableName()) != 0) {
                throw new Exception("该表已存在");
            }
        } else {
            tableManageMapper.deleteInfo(dto.getOldId());
            tableManageMapper.deleteDetail(dto.getOldId());
            returnMsg = "更新成功";
        }

        dto.setId(String.valueOf(idWorker.nextId()));
        List<TableDetailDTO> detailDTOList = dto.getDetailList();
        List<TableDetailPO> detailPOList = Lists.newArrayList();

        for (TableDetailDTO detailDTO : detailDTOList) {
            TableDetailPO detailPO = new TableDetailPO();
            BeanUtils.copyProperties(detailDTO, detailPO);

            detailPO.setId(String.valueOf(idWorker.nextId()));
            detailPO.setTableId(dto.getId());
            detailPO.setNewField(StringUtils.isEmpty(detailDTO.getNewField()) ? detailDTO.getOriginalField() : detailDTO.getNewField());
            detailPOList.add(detailPO);
        }

        TablePO tablePO = new TablePO();
        BeanUtils.copyProperties(dto, tablePO);
        //插入记录
        if (returnMsg.equals("更新成功")) {
            tableManageMapper.deleteInfo(dto.getOldId());
            tableManageMapper.deleteDetail(dto.getOldId());
        }
        tableManageMapper.insertTableInfo(tablePO);
        tableManageMapper.batchInsertTableDetail(detailPOList);


        //找到启用的字段
        List<TableDetailPO> enabledList = detailPOList.stream().
                filter(e -> e.getEnabled().intValue() == 1).collect(Collectors.toList());

        //去重
        List<TableDetailPO> uniqueList = Lists.newArrayList();
        Set<String> columnNameSet = Sets.newHashSet();

        for (TableDetailPO po : enabledList) {
            String columnName = po.getNewField();

            if (!columnNameSet.contains(columnName)) {
                columnNameSet.add(columnName);
            } else {
                continue;
            }

            uniqueList.add(po);
        }

        //创建表
        tableManageMapper.deleteTable(dto.getTableName());
        tableManageMapper.createTable(uniqueList, dto.getTableName(), dto.getTableComment());


        //排序
//        TableDetailPO dataFlagPO = uniqueList.stream().filter(e -> StringUtils.equals("data_flag", e.getNewField())).findAny().get();
//        List<TableDetailPO> orderList = uniqueList.stream().filter(e-> !StringUtils.equals("data_flag",e.getNewField())).
//                sorted(Comparator.comparing(TableDetailPO::getFieldOrder)).collect(Collectors.toList());
        List<TableDetailPO> orderList = uniqueList.stream()
                .sorted(Comparator.comparing(TableDetailPO::getFieldOrder)).collect(Collectors.toList());

        //新字段
        List<String> newFieldList = orderList.stream()
                .map(e -> "`" + e.getNewField() + "`").collect(Collectors.toList());

        //旧字段
        List<String> originalFieldList = orderList.stream()
                .map(e -> e.getFromTable() + "." + e.getOriginalField()).collect(Collectors.toList());
        //关联表
        List<TableDetailPO> notMainList = orderList.stream().filter(e -> e.getMain().intValue() == 0).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(notMainList) && StringUtils.isNotEmpty(notMainList.get(0).getRelation())) {
            //条件去重
            ArrayList<TableDetailPO> uniqueNotMainList = notMainList.stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(e -> e.getFromTable() + e.getRelation()))), ArrayList::new)
                    );
            //查询数据
            List<Map<String, Object>> dataList = subjectTableManageMapper.selectDataRelation(
                    //选表字段
                    originalFieldList,
                    //选表关联字段
                    uniqueNotMainList,
                    //选表主表名
                    dto.getMainTable()

            );

            if (CollectionUtils.isEmpty(dataList)) {
                return returnMsg;
            }

            List<String> finalList = mixData(orderList, dataList);

            if (!CollectionUtils.isEmpty(finalList)) {
                tableManageMapper.batchInsert(dto.getTableName(), newFieldList, finalList);
            }
        } else {
            Map<String, List<TableDetailPO>> mapByTable = orderList.stream().collect(Collectors.groupingBy(TableDetailPO::getFromTable));
            for (Map.Entry<String, List<TableDetailPO>> entryByTable : mapByTable.entrySet()) {
                List<TableDetailPO> listByTable = entryByTable.getValue().stream()
                        .sorted(Comparator.comparing(TableDetailPO::getFieldOrder)).collect(Collectors.toList());

                List<String> newFieldListByTable = listByTable.stream()
                        .map(e -> "`" + e.getNewField() + "`").collect(Collectors.toList());

                newFieldListByTable.add("`data_flag`");

                List<String> oldFieldListByTable = listByTable.stream()
                        .map(e -> "`" + e.getOriginalField() + "`").collect(Collectors.toList());

                List<Map<String, Object>> dataByTable = subjectTableManageMapper.selectDataNoRelation(oldFieldListByTable, entryByTable.getKey());

                if (org.springframework.util.CollectionUtils.isEmpty(dataByTable)) {
                    continue;
                }

                List<String> finalList = mixData(listByTable, dataByTable);

                List<String> finalList2 = Lists.newArrayList();

                //添加标识,标识为表名
                for (String item : finalList) {
                    finalList2.add(item + "," + "'"+entryByTable.getKey()+"'");
                }


                tableManageMapper.batchInsert(dto.getTableName(), newFieldListByTable, finalList2);

            }
        }

        return returnMsg;
    }

    private List<String> mixData(List<TableDetailPO> orderList, List<Map<String, Object>> dataList) {
        //旧字段
        List<String> databaseFieldList = orderList.stream()
                .map(e -> e.getOriginalField()).collect(Collectors.toList());

        List<String> dataFieldList = Lists.newArrayList();

        //去除下划线改为驼峰
        for (String dataBaseField : databaseFieldList) {
            dataFieldList.add(lineToHump(dataBaseField));
        }

        List<String> finalList = Lists.newArrayList();


        for (Map<String, Object> map : dataList) {

            StringBuilder condition = new StringBuilder();

            for (String field : dataFieldList) {
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    if (entry.getKey().toLowerCase().equals(field.toLowerCase())) {
                        String string = new String();

                        if (entry.getValue() != null) {
                            if (entry.getValue() instanceof Date) {
                                string = "STR_TO_DATE('" + entry.getValue() + "'," + "'%Y-%m-%d %H:%i:%s.%S')";
                            }
                            if (entry.getValue() instanceof String) {
                                string = "'" + entry.getValue() + "'";
                            }
                        } else {
                            string = "NULL";
                        }
                        condition.append(string + ",");
                    }
                }
            }
            int i = condition.toString().lastIndexOf(",");
            finalList.add(condition.toString().substring(0, i));
        }

        return finalList;
    }

    /*
     * 查看主题库表
     * */
    public List<Map<String, Object>> querySubjectTables() {
        return tableManageMapper.queryOriginalTables();
    }

    /*
     * 查看主题库表字段
     * */
    public List<Map<String, Object>> querySubjectField(String tableName) {
        List<Map<String, Object>> list = tableManageMapper.queryOriginalField(tableName);

        if (CollectionUtils.isNotEmpty(list)) {
            List<Map<String, Object>> resultList = Lists.newArrayList();
            for (Map<String, Object> map : list) {
                Map<String, Object> resultMap = Maps.newHashMap(map);
                resultMap.put("enabled", 0);
                resultMap.put("newField", "");
                resultMap.put("relation", "");
                resultMap.put("conditions", "");
                resultMap.put("main", 0);
                resultMap.put("fieldOrder", 0);
                resultMap.put("flag", UUID.randomUUID());
                resultList.add(resultMap);
            }
            return resultList;
        }
        return null;
    }

    /*
     * 查看标准库表
     * */
    public PageInfo<Map<String, Object>> queryStandardTables(QueryStandardDTO dto) {
        Page<Map<String, Object>> page = PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        tableManageMapper.queryStandardTables(dto);
        return new PageInfo<>(page);
    }

    /*
     * 查看标准库表字段
     * */
    public List<Map<String, Object>> queryStandardField(String tableId) {
        return tableManageMapper.queryStandardField(tableId);
    }


    public static String lineToHump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public String deleteTable(String id) {
        Map<String, Object> map = tableManageMapper.getById(id);
        if (map != null) {
            tableManageMapper.deleteInfo(id);
            tableManageMapper.deleteDetail(id);
            tableManageMapper.deleteTable(map.get("tableName").toString());
        }

        return "删除成功";
    }
}
