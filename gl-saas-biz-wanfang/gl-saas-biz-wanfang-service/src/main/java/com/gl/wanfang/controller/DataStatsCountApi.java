package com.gl.wanfang.controller;


import com.github.pagehelper.PageInfo;
import com.gl.wanfang.service.impl.DataLogService;
import dto.DataLogQueryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import po.DataLogPO;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/dataStatsCount")
@Api(value = "数据日志")
public class DataStatsCountApi {
    @Autowired
    DataLogService dataLogService;
    

    @ApiOperation(value = "查询昨日资源池情况")
    @PostMapping("/day")
    @ResponseBody
    public Map<String, Object> queryCountDay(){
        return dataLogService.queryCountDay();
    }

    @ApiOperation(value = "查询上周资源池情况")
    @PostMapping("/week")
    @ResponseBody
    public Map<String, Object> queryCountWeek(){
        return dataLogService.queryCountWeek();
    }

    @ApiOperation(value = "查询上月资源池情况")
    @PostMapping("/month")
    @ResponseBody
    public Map<String, Object> queryCountMonth(){
        return dataLogService.queryCountMonth();
    }

    @ApiOperation(value = "查询上季度资源池情况")
    @PostMapping("/quarter")
    @ResponseBody
    public Map<String, Object> queryCountQuarter(){
        return dataLogService.queryCountQuarter();
    }

    @ApiOperation(value = "查询上半年资源池情况")
    @PostMapping("/halfyear")
    @ResponseBody
    public Map<String, Object> queryCountHalfyear(){
        return dataLogService.queryCountHalfyear();
    }

    @ApiOperation(value = "查询去年资源池情况")
    @PostMapping("/year")
    @ResponseBody
    public Map<String, Object> queryCountYear(){
        return dataLogService.queryCountYear();
    }

    @ApiOperation(value = "资源池情况月份折线图")
    @PostMapping("polyline")
    @ResponseBody
    public Map<String, List<Map<String, Object>>> queryPolyline(){
        return dataLogService.countPolyline();
    }




}
