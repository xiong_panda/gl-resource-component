package com.gl.wanfang;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableSwagger2Doc
@EnableEurekaClient
//@EnableApolloConfig
@EnableFeignClients(basePackages = "com.gl.wanfang.feign")//扫接口的包
@MapperScan(basePackages = "com.gl.wanfang.mapper.*")
public class WanFangApplication {
	public static void main(String[] args) {
		SpringApplication.run(WanFangApplication.class, args);
	}
}
