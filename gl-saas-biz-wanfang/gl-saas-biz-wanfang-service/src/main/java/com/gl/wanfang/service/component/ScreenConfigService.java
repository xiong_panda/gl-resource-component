package com.gl.wanfang.service.component;

import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.mapper.core.ScreenConfigMapper;
import dto.ScreenCreateDTO;
import dto.ScreenUpdateDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.ScreenConfigPO;

import java.util.List;
import java.util.Map;

@Service
public class ScreenConfigService {
    private static final IdWorker idWorker = new IdWorker();

    @Autowired
    private ScreenConfigMapper screenConfigMapper;

    public List<Map<String, Object>> getScreenConfig() {
        return screenConfigMapper.getScreenConfig();

    }

    public String update(ScreenUpdateDTO dto) throws Exception {
        ScreenConfigPO screenConfigPO = new ScreenConfigPO();
        BeanUtils.copyProperties(dto,screenConfigPO);

        if (screenConfigMapper.update(screenConfigPO) != 1) {
            throw new Exception("更新失败");
        }
        return "更新成功";
    }

    public String insert(ScreenCreateDTO dto) throws Exception {
        ScreenConfigPO standardConfigPO = new ScreenConfigPO();
        BeanUtils.copyProperties(dto,standardConfigPO);
        standardConfigPO.setId(String.valueOf(idWorker.nextId()));

        if (screenConfigMapper.insert(standardConfigPO) != 1) {
            throw new Exception("插入失败");
        }

        return "插入成功";

    }
}
