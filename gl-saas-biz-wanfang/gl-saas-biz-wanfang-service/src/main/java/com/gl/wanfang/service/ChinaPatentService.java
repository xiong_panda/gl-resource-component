package com.gl.wanfang.service;

import annotation.FieldOrder;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.mapper.core.ChinesePatentMapper;
import com.gl.wanfang.mapper.core.FlagStatusMapper;
import com.gl.wanfang.mapper.dictionary.MarkDataMapper;
import com.gl.wanfang.mapper.subject.GlChinesePatentMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import dto.SearchDTO;
import en.EDataSource;
import en.EIPCClassify;
import net.sf.json.xml.XMLSerializer;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import outdto.WChinesePatentOUTDTO;
import po.ChinesePatentPO;
import po.MarkDataPO;
import po.WChinesePatentPO;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

//资源构件
@Service
public class ChinaPatentService {
    @Autowired
    private ChinesePatentMapper chinesePatentMapper;
    @Autowired
    private MarkDataMapper markDataMapper;
    @Autowired
    private GlChinesePatentMapper glChinesePatentMapper;
    @Autowired
    private FlagStatusMapper flagStatusMapper;

    private ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

    private static final IdWorker idWorker = new IdWorker();


    public Page<WChinesePatentOUTDTO> query(SearchDTO dto) {
        PageHelper.startPage(1, 20);
        com.github.pagehelper.Page<WChinesePatentOUTDTO> page = (com.github.pagehelper.Page<WChinesePatentOUTDTO>) chinesePatentMapper.query(dto);

        Page<WChinesePatentOUTDTO> result = new Page<>();

        result.setMsg("成功");
        result.setCode("200");
        result.setIsPage(true);
        result.setList(page);
        result.setPageNo(page.getPageNum());
        result.setPageSize(20);
        result.setTotalSize(page.getTotal());
        result.setPages(page.getPages());

        return result;
    }

    //标记构件,也是整个流程的开始
    public List<WChinesePatentOUTDTO> flagStatusComponent(String resultJson) throws IllegalAccessException {
        //数据转换
        Map<String, Object> resultMap = JSONObject.parseObject(resultJson, Map.class);
        String statusCode = resultMap.get("code").toString();
        List<Map> dataList =new ArrayList<>();
        String mainDataJson="";
        Map<String, Object> dataMap;
        if("200".equals(statusCode)){
            dataMap= JSONObject.parseObject(resultMap.get("data").toString(), Map.class);
            if (dataMap.get("sources")!=null){
                mainDataJson=dataMap.get("sources").toString();
                dataList = JSONObject.parseArray(mainDataJson, Map.class);
            }else {
                return Collections.EMPTY_LIST;
            }
        }else {
            return Collections.EMPTY_LIST;
        }

        //进入字典库对照,如果都匹配不上,则不作后续处理,直接返回
        List<String> fieldList = chinesePatentMapper.CPselectField();
        boolean machType=false;//判断进入构件的是详情信息还是简略信息
        if(!CollectionUtils.isEmpty(dataList)&&dataList.get(0).keySet().contains("indexId")){
            machType=true;
            List<Map> arrList = new ArrayList<>();
            for (Map map : dataList) {
                Map map1 = JSONObject.parseObject(map.get("source").toString(), Map.class);
                arrList.add(map1);
            }
            dataList=arrList;
        }
        Set fieldSet;
        if(dataList.size()!=0){
            fieldSet = dataList.get(0).keySet();
            //如果一个字段都对应不上,则作废
            boolean i = true;
            for (Object field : fieldSet) {
                if (fieldList.contains(field)) {
                    i = false;
                    break;
                }
            }
            if (i) {
                return null;
            }
        }else{
            return null;
        }




        //进行标记转换
        List<MarkDataPO> markList = new ArrayList<>();
        List<WChinesePatentPO> wChinesePatentList = new ArrayList<>();

        for (Map data : dataList) {
            WChinesePatentPO wChinesePatentPO = JSONObject.parseObject(
                    JSONObject.toJSONString(data), WChinesePatentPO.class);
            wChinesePatentPO.setWfId(wChinesePatentPO.getID());
            //创建id
            String id = Long.toString(idWorker.nextId());
            wChinesePatentPO.setID(id);

            //创建对应的标记

            Map<String, Object> flagStatusConfigPO = flagStatusMapper.getflagStatusConfig();
            //flagStatusConfigPO.getNewFlag()
            MarkDataPO markDataPO = new MarkDataPO(Long.toString(idWorker.nextId()), id, Integer.valueOf(flagStatusConfigPO.get("newFlag").toString()), EDataSource.WANG_FANG.code, "1");
            markList.add(markDataPO);
            wChinesePatentList.add(wChinesePatentPO);
        }
        //进入标准化构件
        List<WChinesePatentOUTDTO> wChinesePatentOUTDTOS = standardComponent(wChinesePatentList, markList,machType);
        return wChinesePatentOUTDTOS;
    }


    //标准化构件
    @Transactional(rollbackFor = Exception.class)
    public List<WChinesePatentOUTDTO> standardComponent(List<WChinesePatentPO> list, List<MarkDataPO> markList, Boolean machtype) throws IllegalAccessException {
        List<ChinesePatentPO> chinesePatentList = new ArrayList<>();
        for (WChinesePatentPO wChinesePatentPO : list) {

            Field[] wFields = wChinesePatentPO.getClass().getDeclaredFields();

            ChinesePatentPO chinesePatentPO = new ChinesePatentPO();
            Field[] gFields = chinesePatentPO.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(chinesePatentPO, wfield.get(wChinesePatentPO));
                    }
                }
            }
            chinesePatentPO.setCreateTime(new Date());
            chinesePatentPO.setResourceFrom(EDataSource.WANG_FANG.value);
            chinesePatentPO.setResourceLogo(EDataSource.WANG_FANG.logo);
            chinesePatentList.add(chinesePatentPO);
        }
        if(!machtype) {
            singleThreadExecutor.execute(() -> doInsert(chinesePatentList, markList));
        }
        List<WChinesePatentOUTDTO> chinesePatentOUTDTOS = new ArrayList<>();
        for (ChinesePatentPO chinesePatentPO : chinesePatentList) {

            Field[] wFields = chinesePatentPO.getClass().getDeclaredFields();

            WChinesePatentOUTDTO chinesePatentOUTDTO = new WChinesePatentOUTDTO();
            Field[] gFields = chinesePatentOUTDTO.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(chinesePatentOUTDTO, wfield.get(chinesePatentPO));
                    }
                }
            }
            chinesePatentOUTDTOS.add(chinesePatentOUTDTO);
        }
        return chinesePatentOUTDTOS;

        //进入清洗构件
//        washComponent(chinesePatentList);
    }

    private void doInsert(List<ChinesePatentPO> dataList, List<MarkDataPO> markList) {
        List<String> ids = dataList.stream().map(e -> e.getRequestNumber()).collect(Collectors.toList());

        List<String> existIds = chinesePatentMapper.queryIds(ids);
        List<ChinesePatentPO> finalDataList = dataList.stream().filter(e -> !existIds.contains(e.getRequestNumber())).collect(Collectors.toList());

        List<String> localIds = finalDataList.stream().map(e -> e.getId()).collect(Collectors.toList());
        List<MarkDataPO> finalMarkList = markList.stream().filter(e -> localIds.contains(e.getDataId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(finalDataList)) {
            chinesePatentMapper.batchInsertToCore(finalDataList);
            markDataMapper.batchInsertToCore(finalMarkList);
        }
    }

    //清洗构件
    private void washComponent(List<ChinesePatentPO> list) throws IllegalAccessException {
        for (ChinesePatentPO chinesePatentPO : list) {
            //反射字段类型为字符串的数据,进行清洗
            Field[] fields = chinesePatentPO.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(chinesePatentPO);

                if (value != null && value instanceof String) {
                    //判断json是否为集合,如果是,去除json的格式,留下数据,用逗号分隔
                    if (jsonType(value) == 2) {
                        StringBuilder stringBuilder = new StringBuilder();
                        List<String> stringList = JSONObject.parseArray(value.toString(), String.class);

                        for (int i = 0; i < stringList.size(); i++) {
                            if (i == stringList.size() - 1) {
                                stringBuilder.append(stringList.get(i));
                                break;
                            }
                            stringBuilder.append(stringList.get(i) + ",");
                        }

                        field.set(chinesePatentPO, stringBuilder.toString());
                    }

                    //是否为xml数据,将xml数据转换为普通json
                    if (jsonType(value) == 3) {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        String xmlString = value.toString();
                        //int i = xmlString.indexOf(">");
                        //xmlString.substring(i+1)
                        String result = xmlSerializer.read(xmlString).toString();

                        field.set(chinesePatentPO, JSONObject.toJSONString(result));
                    }
                }

                //判断时间是否正常,不正常进行剔除
                if (value != null && value instanceof Date) {
                    Date date = (Date) value;
                    if (date.getTime() > System.currentTimeMillis()) {
                        field.set(chinesePatentPO, null);
                    }
                }
            }
        }

        //进入筛选构件
        screenComponent(list);
    }

    //筛选构件
    private void screenComponent(List<ChinesePatentPO> list) {
        //按mainClassCode2(主分类号)分类,标准以IPC为标准
        Map<String, List<ChinesePatentPO>> mapByMainClass = list.stream().
                collect(Collectors.groupingBy(
                        e -> e.getMainClassCode2() != null ? e.getMainClassCode2().substring(0, 1) : "unknow")
                );
        //将空的分类号标识为未知分类
        if (mapByMainClass.get("unknow") != null) {
            mapByMainClass.get("unknow").stream().forEach(e -> e.setMainClassCode2("unknow"));
        } else {
            mapByMainClass.put("unknow", Lists.newArrayList());
        }


        if (mapByMainClass.size() > 1) {
            Set<String> keySet = Sets.newHashSet(mapByMainClass.keySet());
            keySet.remove("unknow");

            //将不在IPC分类标准中的数据也划分到未知分类中
            for (String key : keySet) {
                if (!EIPCClassify.keyList().contains(key)) {
                    List<ChinesePatentPO> unknowList = mapByMainClass.get(key);

                    unknowList.stream().forEach(e -> e.setMainClassCode2("unknow"));
                    mapByMainClass.get("unknow").addAll(unknowList);

                    mapByMainClass.remove(key);
                }
            }
        }

        //进入集成构件
        integrateComponent(mapByMainClass);
    }

    //集成构件
    //@Transactional(rollbackFor = Exception.class)
    private void integrateComponent(Map<String, List<ChinesePatentPO>> mapByMainClass) {
        List<ChinesePatentPO> mergeList = Lists.newArrayList();
        //去重,专利去重的标准为申请号
        for (Map.Entry<String, List<ChinesePatentPO>> entry : mapByMainClass.entrySet()) {
            List<ChinesePatentPO> list = entry.getValue();
            List<ChinesePatentPO> uniqueList = list.stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(e -> e.getRequestNumber()))), ArrayList::new)
                    );

            if (uniqueList.size() != list.size()) {
                //迭代,留下时间最新,并且字段最多的数据
                list.removeAll(uniqueList);
                ChinesePatentPO chinesePatentPO = list.stream().sorted(
                        Comparator.comparing(ChinesePatentPO::getDate, Comparator.reverseOrder())
                                .thenComparing(
                                        e -> Arrays.stream(e.getClass().getDeclaredFields()).filter(f -> f != null).count())
                ).findFirst().get();

                uniqueList.add(chinesePatentPO);

                entry.setValue(uniqueList);
            }

            //存入主题资源库
            if (entry.getValue().size() > 0) {
                mergeList.addAll(entry.getValue());
                glChinesePatentMapper.batchInsertToCore(uniqueList, EIPCClassify.getDataBase(entry.getKey()));
            }
        }

        //进入融合构件
        mergeComponent(mergeList);
    }

    //融合构件
    private void mergeComponent(List<ChinesePatentPO> list) {

        //todo 存入标准资源库

    }


    //返回顺序字段
    private List<Field> getOrderedField(Field[] fields) {
        List<Field> fieldList = new ArrayList<>();
        for (Field field : fields) {
            if (field.getAnnotation(FieldOrder.class) != null) {
                fieldList.add(field);
            }
        }
        fieldList.sort(Comparator.comparing(
                e -> e.getAnnotation(FieldOrder.class).order()
        ));
        return fieldList;
    }

    public int jsonType(Object object) {
        /*
         * return 0:既不是array也不是object
         * return 1：object
         * return 2 ：Array
         * return 3 ：xml
         */

        try {
            JSONObject.parseObject(object.toString());
            return 1;
        } catch (Exception e) {// 抛错 说明JSON字符不是数组或根本就不是JSON
            try {
                JSONObject.parseArray(object.toString());
                return 2;
            } catch (Exception ee) {// 抛错 说明JSON字符根本就不是JSON
                try {
                    DocumentHelper.parseText(object.toString());
                    return 3;
                } catch (Exception eee) {
                    return 0;
                }

            }
        }
    }
}
