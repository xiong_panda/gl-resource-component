package com.gl.wanfang.controller.design;

import com.gl.basis.auth.controller.BaseController;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.controller.MultitermServiceInvocationController;
import com.gl.wanfang.service.design.CollaborativeDesignService;
import com.google.common.collect.Maps;
import dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import po.CollaborativeDesignPO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/design")
@Api(value = "协同设计")
public class CollaborativeDesignController {
    @Autowired
    private CollaborativeDesignService collaborativeDesignService;
    @Autowired
    private MultitermServiceInvocationController multitermServiceInvocationController;


//    @PostMapping("/insert")
//    @ApiOperation(value = "协同设计插入")
//    @ResponseBody
//    public String insert(@RequestBody DesignInsertDTO dto) throws Exception {
//        return collaborativeDesignService.insertDesign(dto);
//    }



//    @PostMapping("/delete")
//    @ApiOperation(value = "协同设计删除")
//    @ResponseBody
//    public String insert(@RequestBody IdDTO<String> dto) throws Exception {
//        return collaborativeDesignService.deleteDesign(dto.getId());
//    }

    @PostMapping("/query")
    @ApiOperation(value = "协同设计--设计需求清单")
    @ResponseBody
    public Page<CollaborativeDesignPO> query(@RequestBody DesignQueryDTO dto) throws Exception {
        return collaborativeDesignService.queryDesign(dto);
    }

    @PostMapping("/update")
    @ApiOperation(value = "协同设计更新")
    @ResponseBody
    public String insert(@RequestBody DesignUpdateDTO dto) throws Exception {
        return collaborativeDesignService.updateDesign(dto);
    }


    @PostMapping("/patentMatch")
    @ApiOperation(value = "协同设计--自制件需求清单--专利匹配")
    @ResponseBody
    public String patentMatch(@RequestBody List<PatentMatchDTO> list, HttpServletRequest request, HttpServletResponse response) throws Exception {
        collaborativeDesignService.patentMatch(list,request,response);
        return "正在匹配";
    }

    @PostMapping("/patentDetail")
    @ApiOperation(value = "详情(一个配件的所有专利)")
    @ResponseBody
    public Page<?> patentDetail(@RequestBody SinglePatentMatchDTO dto, HttpServletRequest request, HttpServletResponse response) {
        SearchDTO searchDTO = new SearchDTO();
        searchDTO.setField("KeyWord:"+dto.getItemInfoName()+"&"+"Main_Class_Code:"+dto.getSpecs());
        searchDTO.setFrom(String.valueOf((dto.getPageNum()-1)*10));
        return multitermServiceInvocationController.mutiDataSearch(searchDTO,response,request);
    }

    @PostMapping("/techniqueType")
    @ApiOperation(value = "专利使用类型(1:转移,2:授权,3:邀请设计)")
    @ResponseBody
    public String techniqueType(@RequestBody DesignUpdateDTO dto) throws Exception {
        collaborativeDesignService.updateDesign(dto);
        return "操作成功";
    }

    @PostMapping("/supplierFilter")
    @ApiOperation(value = "供应商筛选--协同设计清单--供应商筛选(id字符串用<,>分隔)")
    @ResponseBody
    public String supplierFilter(@RequestBody IdDTO<String> dto) throws Exception {
        collaborativeDesignService.supplierFilter(dto.getId());
        return "筛选成功";
    }

    @PostMapping("/supplierList")
    @ApiOperation(value = "供应商筛选--供应商列表")
    @ResponseBody
    public List<?> supplierList(HttpServletResponse response, HttpServletRequest request) throws Exception {
        return collaborativeDesignService.supplierDetails(response,request);
    }

    @PostMapping("/getPatentHolderInto")
    @ApiOperation(value = "查询专利所属者信息(直接传名字)")
    @ResponseBody
    public Object getPatentHolderInto(@RequestBody IdDTO<String> dto, HttpServletResponse response, HttpServletRequest request) throws Exception {
        Map<String, Object> resultMap = Maps.newHashMap();
        SearchDTO searchDTO = new SearchDTO();

        searchDTO.setPageSize("1");
        if (dto.getId().length() > 1&&dto.getId().length() <= 5) {
            searchDTO.setField("Name:"+dto.getId());
            searchDTO.setResourceCls("10020");
            List<?> list = multitermServiceInvocationController.mutiDataSearch(searchDTO, response, request).getList();

            if (!CollectionUtils.isEmpty(list)) {
                resultMap.put("author", list.get(0));
            } else {
                return "暂无作者信息";
            }


        }

        if (dto.getId().length() > 5) {
            searchDTO.setField("Corp_Name:"+dto.getId());
            searchDTO.setResourceCls("10011");
            List<?> list = multitermServiceInvocationController.mutiDataSearch(searchDTO, response, request).getList();

            if (!CollectionUtils.isEmpty(list)) {
                resultMap.put("company", list.get(0));
            } else {
                return "暂无企业信息";

            }

        }

        return resultMap;
    }
}
