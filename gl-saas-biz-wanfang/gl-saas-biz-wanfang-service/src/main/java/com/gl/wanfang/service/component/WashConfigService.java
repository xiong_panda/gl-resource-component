package com.gl.wanfang.service.component;

import com.gl.wanfang.mapper.core.WashConfigMapper;
import dto.WashConfigUpdateDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.WashConfigPO;

import java.util.Map;


@Service
public class WashConfigService {
    @Autowired
    private WashConfigMapper washConfigMapper;

    public String update(WashConfigUpdateDTO dto) {
        WashConfigPO washConfigPO = new WashConfigPO();
        BeanUtils.copyProperties(dto,washConfigPO);
        washConfigMapper.update(washConfigPO);
        return "更新成功";
    }

    public Map<String, Object> getWashConfig() {
        return washConfigMapper.getWashConfig();
    }
}
