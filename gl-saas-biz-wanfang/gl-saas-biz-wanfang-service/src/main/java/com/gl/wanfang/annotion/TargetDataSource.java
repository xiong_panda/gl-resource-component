//package com.gl.wanfang.annotion;
//
//import com.gl.wanfang.config.EDataSourceKey;
//
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
//@Target(ElementType.METHOD)
//@Retention(RetentionPolicy.RUNTIME)
//public @interface TargetDataSource {
//    EDataSourceKey dataSourceKey() default EDataSourceKey.dataSource;
//}
