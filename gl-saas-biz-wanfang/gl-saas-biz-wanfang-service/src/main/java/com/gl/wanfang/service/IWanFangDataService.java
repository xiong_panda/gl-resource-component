package com.gl.wanfang.service;

import dto.WFParamDTO;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface IWanFangDataService {


    public void ckeyCon(String wfmds, String wid, String docTI, HttpServletResponse response, HttpServletRequest request);


    public String creatToken(String appid, HttpServletResponse response, HttpServletRequest request);

    public String StatisticalRetrieval(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String indexName);

    String StatisticalRetrieval(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String AggregationSearch(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);


    public String matchSeach(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String matchSeachgn(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String indexName);

    public String matchSeach(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String indexName);


    public List<Map<String, Object>> menuSearch(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String idsSearch(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);


    public String idsSearchs(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String ids);

    public String knowledgeliterature(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String companyProfile(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String companyIds(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String companyTechnology(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String companyScholar(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String companyScholarIds(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);



    public String scholarTechnologySearch(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    public String scholarThesisSearch(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request);

    String IdsInfo(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String data);

    String IdsInfo(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String data, String indexName);

}
