package com.gl.wanfang.controller.outController;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.StringUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.feign.GuoLongClient;
import com.gl.wanfang.feign.LingDunClient;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.impl.MultitermServiceInvocationService;
import dto.SearchDTO;
import en.EOutApiErrorCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;


@RestController
@RequestMapping(value = "/outApi/data")
@Api(value = "对外-多服务数据调用", tags = {"对外-万方，宁波，东方灵盾等数据搜索"})
@Slf4j
public class OutMultitermServiceInvocationController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    GuoLongClient guoLongClient;
    @Autowired
    LingDunClient lingDunClient;
    @Autowired
    MultitermServiceInvocationService serviceInvocationService;

    private static final Logger log = LoggerFactory.getLogger(OutMultitermServiceInvocationController.class);
    /**
     *万方数据接口、  创新助手
     * @param  '万方数据'
     * @return
     * 列表
     */
//    @RequiresPermissions(value = "user:select")
//    @ApiOperation(value = "综合服务调用")
//    @GetMapping("/dataCollection")
//    @ResponseBody
//    public Object dataCollection(String serviceID, String resourceCls, HttpServletResponse response, HttpServletRequest request) throws Exception {
//
//        Object objects = null;
//        try {
//            objects =serviceInvocationService.dataCollection(serviceID, resourceCls, response, request);
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        return objects;
//    }

    /**
     * 万方数据接口、  创新助手
     *
     * @param '万方数据'
     * @return 列表
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "综合服务调用-批量详情")
    @PostMapping("/mutiDataSearch")
    @ResponseBody
    public CityOutResult mutiDataSearch(@RequestBody CommBean commBean, HttpServletResponse response, HttpServletRequest request) {
        SearchDTO dto;
        CityOutResult cityOutResult;

        if (commBean != null && !StringUtils.isEmpty(commBean.getBz_data())) {
            log.info("commBean:"+commBean.toString());

            cityOutResult = new CityOutResult();
            dto = JSONObject.parseObject(commBean.getBz_data(), SearchDTO.class);
            if (dto.getField() == null) {
                throw new BusinessException(dto.getResourceCls() + "-" + EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(field)", EOutApiErrorCode.PARAM_ERR.code);
            }
        } else {
            log.error("commBean:"+commBean.toString());
            throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(json)", EOutApiErrorCode.PARAM_ERR.code);
        }

        try {
            Page<?> page = serviceInvocationService.dataCollection(dto, response, request);

            log.info("万方查询数据:"+page.toString());

            cityOutResult.setResult(CollectionUtils.isEmpty(page.getList())? Collections.emptyList():page.getList());
            cityOutResult.setTotalSize(page.getTotalSize()==null?0:page.getTotalSize());
        } catch (DocumentException e) {
            e.printStackTrace();
            throw new BusinessException(dto.getResourceCls() + "-" + EOutApiErrorCode.JSON_PARSE_ERR.msg, EOutApiErrorCode.JSON_PARSE_ERR.code);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (BusinessException e) {
            e.printStackTrace();
            throw new BusinessException(dto.getResourceCls() + "-" + e.getMessage(), e.getCode());
        }
        return cityOutResult;

    }


    /**
     * 万方数据接口、  创新助手
     *
     * @param '万方数据'
     * @return 列表
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "综合服务调用-详情")
    @PostMapping("/mutiDataIdsSearch")
    @ResponseBody
    public CityOutResult mutiDataIdsSearch(@RequestBody CommBean commBean, HttpServletResponse response, HttpServletRequest request) {
        SearchDTO dto;
        CityOutResult cityOutResult;

        if (commBean != null && !StringUtils.isEmpty(commBean.getBz_data())) {
            log.info("commBean:"+commBean.toString());

            cityOutResult = new CityOutResult();
            dto = JSONObject.parseObject(commBean.getBz_data(), SearchDTO.class);
            if (StringUtils.isEmpty(dto.getIds())) {
                throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(ids)", EOutApiErrorCode.PARAM_ERR.code);
            }
        } else {
            log.error("commBean:"+commBean.toString());
            throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(json)", EOutApiErrorCode.PARAM_ERR.code);
        }

        try {
            Page<?> page = serviceInvocationService.dataCollectionIds(dto, response, request);

            log.info("万方查询数据:"+page.toString());

            cityOutResult.setResult(CollectionUtils.isEmpty(page.getList())? Collections.emptyList():page.getList());
            cityOutResult.setTotalSize(page.getTotalSize()==null?0:page.getTotalSize());
        } catch (DocumentException e) {
            e.printStackTrace();
            throw new BusinessException(dto.getResourceCls() + "-" + EOutApiErrorCode.JSON_PARSE_ERR.msg, EOutApiErrorCode.JSON_PARSE_ERR.code);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (BusinessException e) {
            e.printStackTrace();
            throw new BusinessException(dto.getResourceCls() + "-" + e.getMessage(), e.getCode());
        }
        return cityOutResult;
    }

    /**
     * 万方数据接口、  创新助手
     *
     * @param '万方数据'
     * @return 列表
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "综合服务调用-简略")
    @PostMapping("/mutiDataInfoSearch")
    @ResponseBody
    public CityOutResult mutiDataInfoSearch(@RequestBody CommBean commBean, HttpServletResponse response, HttpServletRequest request) {
        SearchDTO dto;
        CityOutResult cityOutResult;

        if (commBean != null && !StringUtils.isEmpty(commBean.getBz_data())) {
            log.info("commBean:"+commBean.toString());

            cityOutResult = new CityOutResult();
            dto = JSONObject.parseObject(commBean.getBz_data(), SearchDTO.class);
            if (dto.getField() == null) {
                throw new BusinessException(dto.getResourceCls() + "-" + EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(field)", EOutApiErrorCode.PARAM_ERR.code);
            }
        } else {
            log.error("commBean:"+commBean.toString());
            throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(json)", EOutApiErrorCode.PARAM_ERR.code);
        }

        try {

            Page<?> page = serviceInvocationService.dataCollectionSearch(dto, response, request);

            log.info("万方查询数据:"+page.toString());

            cityOutResult.setResult(CollectionUtils.isEmpty(page.getList())? Collections.emptyList():page.getList());
            cityOutResult.setTotalSize(page.getTotalSize()==null?0:page.getTotalSize());
        } catch (DocumentException e) {
            e.printStackTrace();
            throw new BusinessException(dto.getResourceCls() + "-" + EOutApiErrorCode.JSON_PARSE_ERR.msg, EOutApiErrorCode.JSON_PARSE_ERR.code);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (BusinessException e) {
            e.printStackTrace();
            throw new BusinessException(dto.getResourceCls() + "-" + e.getMessage(), e.getCode());
        }
        return cityOutResult;
    }

}
