package com.gl.wanfang.service.component;

import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.mapper.core.IntegrateConfigMapper;
import dto.IntegrateCreateDTO;
import dto.IntegrateUpdateDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.IntegrateConfigPO;

import java.util.List;
import java.util.Map;

@Service
public class IntegrateConfigService {
    private static final IdWorker idWorker = new IdWorker();

    @Autowired
    private IntegrateConfigMapper integrateConfigMapper;

    public List<Map<String, Object>> getIntegrateConfig() {
        return integrateConfigMapper.getIntegrateConfig();

    }

    public String update(IntegrateUpdateDTO dto) throws Exception {
        IntegrateConfigPO screenConfigPO = new IntegrateConfigPO();
        BeanUtils.copyProperties(dto,screenConfigPO);

        if (integrateConfigMapper.update(screenConfigPO) != 1) {
            throw new Exception("更新失败");
        }
        return "更新成功";
    }

    public String insert(IntegrateCreateDTO dto) throws Exception {
        IntegrateConfigPO standardConfigPO = new IntegrateConfigPO();
        BeanUtils.copyProperties(dto,standardConfigPO);
        standardConfigPO.setId(String.valueOf(idWorker.nextId()));

        if (integrateConfigMapper.insert(standardConfigPO) != 1) {
            throw new Exception("插入失败");
        }

        return "插入成功";

    }
}
