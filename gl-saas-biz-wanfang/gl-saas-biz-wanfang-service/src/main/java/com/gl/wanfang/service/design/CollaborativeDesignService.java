package com.gl.wanfang.service.design;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.mapper.core.AuthorLibraryMapper;
import com.gl.wanfang.mapper.core.ChinesePatentMapper;
import com.gl.wanfang.mapper.core.CollaborativeDesignMapper;
import com.gl.wanfang.mapper.core.EnterpriseProductDatabaseMapper;
import com.gl.wanfang.service.impl.MultitermServiceInvocationService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import dto.*;
import org.dom4j.DocumentException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import outdto.WChinesePatentOUTDTO;
import po.CollaborativeDesignPO;

import javax.naming.directory.SearchControls;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CollaborativeDesignService {
    @Autowired
    private CollaborativeDesignMapper collaborativeDesignMapper;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private MultitermServiceInvocationService multitermServiceInvocationService;
    @Autowired
    private ChinesePatentMapper chinesePatentMapper;
    @Autowired
    private AuthorLibraryMapper authorLibraryMapper;
    @Autowired
    private EnterpriseProductDatabaseMapper enterpriseProductDatabaseMapper;


    public com.gl.basis.common.pojo.Page<CollaborativeDesignPO> queryDesign(DesignQueryDTO dto) {
        Page<CollaborativeDesignPO> page = PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        collaborativeDesignMapper.queryDesign(dto);

        com.gl.basis.common.pojo.Page result = new com.gl.basis.common.pojo.Page();
        result.setSuccess(true);
        result.setMsg("成功");
        result.setIsPage(true);
        result.setTotalSize(page.getTotal());
        result.setCode("200");
        result.setPageSize(page.getPageSize());
        result.setPageNo(page.getPageNum());
        result.setList(page.getResult());
        result.setPages(page.getPages());
        return result;
    }

    public String insertDesign(DesignInsertDTO dto) throws Exception {
        CollaborativeDesignPO po = new CollaborativeDesignPO();
        BeanUtils.copyProperties(dto, po);
        po.setId(String.valueOf(idWorker.nextId()));

        if (collaborativeDesignMapper.insertDesign(po) != 1) {
            throw new Exception("插入失败");
        }

        return "插入成功";
    }

    public String updateDesign(DesignUpdateDTO dto) throws Exception {
        CollaborativeDesignPO po = new CollaborativeDesignPO();
        BeanUtils.copyProperties(dto, po);
        if (collaborativeDesignMapper.updateDesign(po) != 1) {
            throw new Exception("更新失败");
        }
        return "更新成功";
    }

    public String deleteDesign(String id) throws Exception {
        CollaborativeDesignPO po = new CollaborativeDesignPO();
        po.setDelFlag("1");
        po.setId(id);

        if (collaborativeDesignMapper.updateDesign(po) != 1) {
            throw new Exception("删除失败");
        }
        return "删除失败";
    }

    public void patentMatch(List<PatentMatchDTO> list, HttpServletRequest request, HttpServletResponse response) throws Exception {
        for (PatentMatchDTO dto : list) {
            SearchDTO searchDTO = new SearchDTO();
            searchDTO.setField("KeyWord:"+dto.getItemInfoName()+"&"+"Main_Class_Code:"+dto.getSpecs());
            com.gl.basis.common.pojo.Page<?> page = multitermServiceInvocationService.dataCollection(searchDTO, response, request);

            CollaborativeDesignPO po = new CollaborativeDesignPO();
            BeanUtils.copyProperties(dto,po);
            po.setPatentsNum(page.getTotalSize());

            if (!CollectionUtils.isEmpty(page.getList())) {
                Object patent = page.getList().get(0);
                if (patent instanceof WChinesePatentOUTDTO) {
                    WChinesePatentOUTDTO chinesePatentDTO = (WChinesePatentOUTDTO) patent;
                    po.setPatentHolder(chinesePatentDTO.getOrgNormName2());
                    po.setPatentNo(chinesePatentDTO.getRequestNumber());
                }
            }

            if (collaborativeDesignMapper.updateDesign(po) != 1) {
                throw new Exception("匹配失败");
            }
        }
    }

    public void supplierFilter(String idsString) throws Exception {
        List<String> idList=Lists.newArrayList(idsString.split(","));
        CollaborativeDesignPO po = new CollaborativeDesignPO();
        po.setIds(idList);
        po.setSupplierFilter("1");
        if (collaborativeDesignMapper.updateDesign(po) != idList.size()) {
            throw new Exception("筛选失败");
        }
    }

    public List<?> supplierDetails(HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        DesignQueryDTO dto = new DesignQueryDTO();
        dto.setSupplierFilter("1");
        List<CollaborativeDesignPO> poList = collaborativeDesignMapper.queryDesign(dto);

        List<Object> resultList = Lists.newArrayList();

        for (CollaborativeDesignPO po : poList) {
            SearchDTO searchDTO = new SearchDTO();
            searchDTO.setField("KeyWord:"+po.getItemInfoName()+"&"+"Main_Class_Code:"+po.getSpecs());
            searchDTO.setPageSize("3");

            List<?> list = multitermServiceInvocationService.dataCollection(searchDTO, response, request).getList();
            WChinesePatentOUTDTO patent;

            for (Object object : list) {
                if (object instanceof WChinesePatentOUTDTO) {
                    patent = (WChinesePatentOUTDTO) object;
                    if (!StringUtils.isEmpty(patent.getOrgNormName2())) {
                        if (patent.getOrgNormName2().length() > 1&&patent.getOrgNormName2().length() <= 5) {
                            searchDTO.setField("Name:"+patent.getOrgNormName2());
                            searchDTO.setResourceCls("10020");
                            com.gl.basis.common.pojo.Page<?> page = multitermServiceInvocationService.dataCollection(searchDTO, response, request);

                            if (page != null) {
                                List<?> authorList = page.getList();
                                if (!CollectionUtils.isEmpty(authorList)) {
                                    Map<String,Object> dataMap = Maps.newHashMap(BeanMap.create(authorList.get(0)));
                                    dataMap.put("supplierType", "author");
                                    resultList.add(dataMap);
                                }
                            }
                        }

                        if (patent.getOrgNormName2().length() > 5) {
                            searchDTO.setField("Corp_Name:"+patent.getOrgNormName2());
                            searchDTO.setResourceCls("10011");
                            com.gl.basis.common.pojo.Page<?> page = multitermServiceInvocationService.dataCollection(searchDTO, response, request);
                            if (page != null) {
                                List<?> companyList = page.getList();
                                if (!CollectionUtils.isEmpty(companyList)) {
                                    Map<String,Object> dataMap = Maps.newHashMap(BeanMap.create(companyList.get(0)));
                                    dataMap.put("supplierType", "company");
                                    resultList.add(dataMap);
                                }
                            }
                        }
                    }
                }
            }
        }
        return resultList;
    }
}
