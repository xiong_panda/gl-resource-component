package com.gl.wanfang.service.component;

import com.gl.wanfang.mapper.core.FlagStatusMapper;
import dto.FlagStatusUpdateDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import po.FlagStatusConfigPO;

import java.util.Map;


@Service
public class FlagStatusConfigService {
    @Autowired
    private FlagStatusMapper flagStatusMapper;

    public String update(FlagStatusUpdateDTO dto) {
        FlagStatusConfigPO flagStatusConfigPO = new FlagStatusConfigPO();
        BeanUtils.copyProperties(dto,flagStatusConfigPO);
        flagStatusMapper.update(flagStatusConfigPO);
        return "更新成功";
    }

    public Map<String, Object> getflagStatusConfig() {
        return flagStatusMapper.getflagStatusConfig();
    }
}
