package com.gl.wanfang.service;

import annotation.FieldOrder;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.mapper.core.ScientificOrgMapper;
import com.gl.wanfang.mapper.dictionary.MarkDataMapper;
import com.gl.wanfang.mapper.subject.GlScientificOrgMapper;
import com.google.common.collect.Sets;
import en.EDataSource;
import en.EDataStatus;
import en.EDisciplineClassify;
import net.sf.json.xml.XMLSerializer;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import outdto.WScientificOrgOutDto;
import po.MarkDataPO;
import po.ScientificOrgPO;
import po.WScientificOrgPO;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

//资源构件
@Service
public class ScientificOrgService {
    @Autowired
    private ScientificOrgMapper scientificOrgMapper;
    @Autowired
    private MarkDataMapper markDataMapper;
    @Autowired
    private GlScientificOrgMapper glScientificOrgMapper;

    private ExecutorService singleThreadExecutor= Executors.newSingleThreadExecutor();


    private static final IdWorker idWorker = new IdWorker();

    //标记构件,也是整个流程的开始
    public List<WScientificOrgOutDto> flagStatusComponent(String resultJson) throws IllegalAccessException, DocumentException {
        //数据转换
        Map<String, Object> resultMap = JSONObject.parseObject(resultJson, Map.class);
        String statusCode = resultMap.get("code").toString();
        List<Map> dataList=new ArrayList<>();
        String mainDataJson="";
        Map<String, Object> dataMap;
        if("200".equals(statusCode)){
            dataMap= JSONObject.parseObject(resultMap.get("data").toString(), Map.class);
            if (dataMap.get("sources")!=null){
                mainDataJson=dataMap.get("sources").toString();
                dataList = JSONObject.parseArray(mainDataJson, Map.class);
            }else {
                return Collections.EMPTY_LIST;
            }
        }else {
            return Collections.EMPTY_LIST;
        }
        //进入字典库对照,如果都匹配不上,则不作后续处理,直接返回
        List<String> fieldList=scientificOrgMapper.SOselectField();
        boolean machType=false;//判断进入构件的是详情信息还是简略信息
        if(dataList!=null&&dataList.get(0).keySet().contains("indexId")){
            machType=true;
            List<Map> arrList = new ArrayList<>();
            for (Map map : dataList) {
                Map map1 = JSONObject.parseObject(map.get("source").toString(), Map.class);
                arrList.add(map1);
            }
            dataList=arrList;
        }
        Set fieldSet;
        if(dataList.size()!=0){
            fieldSet = dataList.get(0).keySet();
            //如果一个字段都对应不上,则作废
            boolean i = true;
            for (Object field : fieldSet) {
                if (fieldList.contains(field)) {
                    i = false;
                    break;
                }
            }
            if (i) {
                return null;
            }
        }else{
            return null;
        }

        List<MarkDataPO> markList = new ArrayList<>();
        List<WScientificOrgPO> wScientificOrgPOList = new ArrayList<>();

        for (Map data : dataList) {
            WScientificOrgPO wScientificOrgPO = JSONObject.parseObject(
                    JSONObject.toJSONString(data), WScientificOrgPO.class);
            wScientificOrgPO.setWfId(wScientificOrgPO.getID());
            //创建id
            String id = Long.toString(idWorker.nextId());
            wScientificOrgPO.setID(id);
            //创建对应的标记
            MarkDataPO markDataPO = new MarkDataPO(Long.toString(idWorker.nextId()), id, EDataStatus.NEW.code, EDataSource.WANG_FANG.code, "1");
            markList.add(markDataPO);
            wScientificOrgPOList.add(wScientificOrgPO);
        }

        //进入标准化构件
        List<WScientificOrgOutDto> scientificOrgPOS = standardComponent(wScientificOrgPOList, markList,machType);
        return scientificOrgPOS;
    }


    //标准化构件
    @Transactional(rollbackFor = Exception.class)
    public List<WScientificOrgOutDto> standardComponent(List<WScientificOrgPO> list, List<MarkDataPO> markList, Boolean machtype) throws IllegalAccessException, DocumentException {
        List<ScientificOrgPO> scientificOrgPOList = new ArrayList<>();
        for (WScientificOrgPO wScientificOrgPO : list) {

            Field[] wFields = wScientificOrgPO.getClass().getDeclaredFields();

            ScientificOrgPO scientificOrgPO = new ScientificOrgPO();
            Field[] gFields = scientificOrgPO.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(scientificOrgPO, wfield.get(wScientificOrgPO));
                    }
                }
            }
            scientificOrgPO.setCreateTime(new Date());
            scientificOrgPO.setResourceFrom(EDataSource.WANG_FANG.value);
            scientificOrgPO.setResourceLogo(EDataSource.WANG_FANG.logo);
            scientificOrgPOList.add(scientificOrgPO);
        }
        if(!machtype) {
            singleThreadExecutor.execute(() -> doInsert(scientificOrgPOList, markList));
        }
        List<WScientificOrgOutDto> wScientificOrgOutDtos = new ArrayList<>();
        for (ScientificOrgPO scientificOrgPO : scientificOrgPOList) {

            Field[] wFields = scientificOrgPO.getClass().getDeclaredFields();

            WScientificOrgOutDto wScientificOrgOutDto = new WScientificOrgOutDto();
            Field[] gFields = wScientificOrgOutDto.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(wScientificOrgOutDto, wfield.get(scientificOrgPO));
                    }
                }
            }
            wScientificOrgOutDtos.add(wScientificOrgOutDto);
        }
        return wScientificOrgOutDtos;
        //进入清洗构件
//        washComponent(scientificOrgPOList);
    }

    private void doInsert(List<ScientificOrgPO> dataList, List<MarkDataPO> markList) {
        List<String> ids = dataList.stream().map(e -> e.getORG_Name_Norm()).collect(Collectors.toList());

        List<String> existIds = scientificOrgMapper.queryIds(ids);
        List<ScientificOrgPO> finalDataList = dataList.stream().filter(e -> !existIds.contains(e.getORG_Name_Norm())).collect(Collectors.toList());

        List<String> localIds = finalDataList.stream().map(e -> e.getID()).collect(Collectors.toList());
        List<MarkDataPO> finalMarkList = markList.stream().filter(e -> localIds.contains(e.getDataId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(finalDataList)) {
            scientificOrgMapper.SObatchInsertToCore(finalDataList);
            markDataMapper.batchInsertToCore(finalMarkList);
        }
    }

    //清洗构件
    private void washComponent(List<ScientificOrgPO> list) throws IllegalAccessException, DocumentException {
        for (ScientificOrgPO scientificOrgPO : list) {
            //反射字段类型为字符串的数据,进行清洗
            Field[] fields = scientificOrgPO.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(scientificOrgPO);

                if (value != null && value instanceof String ) {
                    //判断json是否为集合,如果是,去除json的格式,留下数据,用逗号分隔
                    if (jsonType(value) == 2) {
                        StringBuilder stringBuilder = new StringBuilder();
                        List<String> stringList = JSONObject.parseArray(value.toString(), String.class);

                        for (int i=0; i < stringList.size(); i++) {
                            if (i == stringList.size()-1) {
                                stringBuilder.append(stringList.get(i));
                                break;
                            }
                            stringBuilder.append(stringList.get(i)+",");
                        }

                        field.set(scientificOrgPO,stringBuilder.toString());
                    }
                    
                    //是否为xml数据,将xml数据转换为普通json
                    if (jsonType(value) == 3) {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        String xmlString = value.toString();
                        //int i = xmlString.indexOf(">");
                        //xmlString.substring(i+1)
                        String result = xmlSerializer.read(xmlString).toString();

                        field.set(scientificOrgPO,JSONObject.toJSONString(result));
                    }
                }

                //判断时间是否正常,不正常进行剔除
                if (value != null && value instanceof Date) {
                    Date date = (Date) value;
                    if (date.getTime() > System.currentTimeMillis()) {
                        field.set(scientificOrgPO,null);
                    }
                }
            }
        }

        //进入筛选构件
        screenComponent(list);
    }

    //筛选构件
    private void screenComponent(List<ScientificOrgPO> list) {
        //按行业分类
        Map<String, List<ScientificOrgPO>> mapByMainClass = list.stream().
                collect(Collectors.groupingBy(
                        e -> e.getDiscipline_Class() != null ? e.getDiscipline_Code2().substring(0, 2) : "unknow")
                );
        //将空的分类号标识为未知分类
        if (mapByMainClass.get("unknow") != null) {
            mapByMainClass.get("unknow").stream().forEach(e->e.setDiscipline_Code2("unknow"));
        }else {
            mapByMainClass.put("unknow", new ArrayList<>());
        }

        if (mapByMainClass.size() > 1) {
            Set<String> keySet = Sets.newHashSet(mapByMainClass.keySet());
            keySet.remove("unknow");

            //将不在IPC分类标准中的数据也划分到未知分类中
            for (String key : keySet) {
                if (!EDisciplineClassify.keyList().contains(key)) {
                    List<ScientificOrgPO> unknowList=mapByMainClass.get(key);
                    mapByMainClass.get("unknow").addAll(unknowList);
                    mapByMainClass.remove(key);
                }
            }
        }

        //进入集成构件
        integrateComponent(mapByMainClass);
    }

    //集成构件
    private void integrateComponent(Map<String, List<ScientificOrgPO>> mapByMainClass) {
        //去重,专利去重的标准为申请号
        for (Map.Entry<String, List<ScientificOrgPO>> entry : mapByMainClass.entrySet()) {
            List<ScientificOrgPO> list = entry.getValue();
            List<ScientificOrgPO> uniqueList = list.stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(e -> e.getOrderID()))), ArrayList::new)
                    );

            if (uniqueList.size() != list.size()) {
                //迭代,留下时间最新,并且字段最多的数据
                list.removeAll(uniqueList);
                ScientificOrgPO scientificOrgPO = list.stream().sorted(
                        Comparator.comparing(ScientificOrgPO::getORG_Name_Norm, Comparator.reverseOrder())
                                .thenComparing(
                                        e -> Arrays.stream(e.getClass().getDeclaredFields()).filter(f -> f != null).count())
                ).findFirst().get();

                uniqueList.add(scientificOrgPO);

                entry.setValue(uniqueList);

            }
            //存入主题资源库
            if (entry.getValue().size() > 0) {
                glScientificOrgMapper.SObatchInsertToCore(
                        uniqueList,
                        (entry.getKey().equals("unknow")?"u":entry.getKey()+"0")+"_scientific_org" );

            }
        }
        //todo 存入主题资源库
        //进入融合构件
        //mergeComponent(list);
    }

    //融合构件
    private List<?> mergeComponent(List<?> list) {

        //todo 存入标准资源库
        //返回数据
        return list;
    }


    //返回顺序字段
    private List<Field> getOrderedField(Field[] fields) {
        List<Field> fieldList = new ArrayList<>();
        for (Field field : fields) {
            if (field.getAnnotation(FieldOrder.class) != null) {
                fieldList.add(field);
            }
        }
        fieldList.sort(Comparator.comparing(
                e -> e.getAnnotation(FieldOrder.class).order()
        ));
        return fieldList;
    }

    public int jsonType(Object object){
        /*
         * return 0:既不是array也不是object
         * return 1：object
         * return 2 ：Array
         * return 3 ：xml
         */

        try {
            JSONObject.parseObject(object.toString());
            return 1;
        } catch (Exception e) {// 抛错 说明JSON字符不是数组或根本就不是JSON
            try {
                JSONObject.parseArray(object.toString());
                return 2;
            } catch (Exception ee) {// 抛错 说明JSON字符根本就不是JSON
                try {
                    DocumentHelper.parseText(object.toString());
                    return 3;
                }catch (Exception eee) {
                    return 0;
                }

            }
        }

    }
}
