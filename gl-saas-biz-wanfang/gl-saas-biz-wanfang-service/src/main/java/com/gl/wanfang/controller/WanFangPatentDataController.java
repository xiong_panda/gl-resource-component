package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.ChinaPatentService;
import com.gl.wanfang.service.OverseasPatentService;
import com.gl.wanfang.service.impl.WanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WChinesePatentOUTDTO;
import outdto.WOverseasPatentOutDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 万方专利搜索
 */
@RestController
@Api(value = "万方",tags = {"万方专利搜索"})
@RequestMapping("/patent")
public class WanFangPatentDataController {

    @Autowired
    WanFangDataService wanFangDataService;
    @Autowired
    ChinaPatentService chinaPatentService;
     @Autowired
     OverseasPatentService overseasPatentService;

    @ApiOperation(value = "万方中文专利简略检索")
    @PostMapping("/matchChinesePatentSearch")
    @ResponseBody
    public Page<WChinesePatentOUTDTO> matchChinesePatentSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        String indexName = wfParamDTO.getIndexName()==null?"10004":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专利简略信息匹配
        if(null==data || "".equals(data)){
            return null;
        }
        return getData(wfParamDTO,response,request,indexName,data);

    }


    @ApiOperation(value = "万方中文专利详细检索")
    @PostMapping("/matchChinesePatentIdsSearch")
    @ResponseBody
    public Page<WChinesePatentOUTDTO> matchChinesePatentIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        wfParamDTO.setIndexName("10033");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方中文专利批量详情检索")
    @PostMapping("/matchChinesePatentInfo")
    @ResponseBody
    public Page<WChinesePatentOUTDTO> matchPatentInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        String indexName = wfParamDTO.getIndexName()==null?"10004":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专利简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);
        return getData(wfParamDTO,response,request,indexName,result);

    }

    private Page<WChinesePatentOUTDTO> getData( WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WChinesePatentOUTDTO> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WChinesePatentOUTDTO> wChinesePatentOUTDTOS = chinaPatentService.flagStatusComponent(result);
            page.setList(wChinesePatentOUTDTOS);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }
    @ApiOperation(value = "万方海外专利简略检索")
    @PostMapping("/matchOverseasPatentSearch")
    @ResponseBody
    public Page<WOverseasPatentOutDto> matchOverseasPatentSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        String indexName = wfParamDTO.getIndexName()==null?"10016":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专利简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        return getData2(wfParamDTO,response, request,indexName,data);
    }

    @ApiOperation(value = "万方海外专利详细检索")
    @PostMapping("/matchOverseasPatentIdsSearch")
    @ResponseBody
    public Page<WOverseasPatentOutDto> matchOverseasPatentIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        wfParamDTO.setIndexName("10016");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData2(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方海外专利批量详情检索")
    @PostMapping("/matchOverseasPatentInfo")
    @ResponseBody
    public Page<WOverseasPatentOutDto> matchOverseasPatentInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws IllegalAccessException, DocumentException {
        String indexName = wfParamDTO.getIndexName()==null?"10016":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专利简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData2(wfParamDTO,response, request,indexName,result);

    }

    private Page<WOverseasPatentOutDto> getData2( WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WOverseasPatentOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WOverseasPatentOutDto> wOverseasPatentOutDtos = overseasPatentService.flagStatusComponent(result);
            page.setList(wOverseasPatentOutDtos);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

}
