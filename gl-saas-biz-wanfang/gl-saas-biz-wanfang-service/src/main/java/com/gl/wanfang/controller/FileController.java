package com.gl.wanfang.controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@RestController
public class FileController {
    /**
     * @param file
     * @return
     */
    @PostMapping("/upload")
    @ResponseBody
    public String uploadTwo(HttpServletRequest request, @RequestParam("file") MultipartFile file) {

        String flag = "上传失败";
        String fileName = file.getOriginalFilename();
        if (fileName == null) {
            return "文件名为空";
        }
        File targetFile = new File("/home/");
        if (!targetFile.exists()) {
            targetFile.mkdir();
        }
        File file1 = new File(targetFile, fileName);
        try {
            file.transferTo(file1);
            flag = "上传成功";
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 文件上传
        return flag;
    }
    /*@GetMapping("/index")
    public  String index(){
        return  "/static/index";
    }*/
}