package com.gl.wanfang.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@RequestMapping("/gl-api-eq")
@FeignClient(value = "saas-server-guolong-api",fallbackFactory = GuoLongClientCallBackFactory.class)//映射的服务名：在注册中心的服务名,告诉使用哪一个托底类
public interface GuoLongClient {

    @GetMapping("/getFaultInformation")
    @ResponseBody
    public List<Map<String,Object>> getFaultInformation(@RequestParam("factName") String factName);

    @GetMapping("/getCompanyService")
    @ResponseBody
    public List<Map<String,Object>> getCompanyService(@RequestParam("factName") String factName);
}
