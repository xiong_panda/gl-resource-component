package com.gl.wanfang.controller.component;


import com.gl.wanfang.service.component.WashConfigService;
import dto.WashConfigUpdateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController()
@Api(value = "清洗构件配置")
@RequestMapping("/washConfig")
public class WashConfigApi {
    @Autowired
    private WashConfigService washConfigService;

    @PostMapping("/update")
    @ApiOperation(value = "修改清洗构件的属性")
    public String update(@RequestBody WashConfigUpdateDTO dto) {
       return washConfigService.update(dto);
    }

    @PostMapping("/get")
    @ApiOperation(value = "获取清洗构件的配置")
    public Map<String, Object> get() {
        return washConfigService.getWashConfig();
    }
}
