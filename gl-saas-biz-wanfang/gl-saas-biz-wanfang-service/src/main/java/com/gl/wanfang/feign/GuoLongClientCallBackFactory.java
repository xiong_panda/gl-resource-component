package com.gl.wanfang.feign;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class GuoLongClientCallBackFactory implements FallbackFactory<GuoLongClient> {

    @Override
    public GuoLongClient create(Throwable throwable) {
        return new GuoLongClient() {

            @Override
            public List<Map<String, Object>> getFaultInformation(String factName) {
                return null;
            }

            @Override
            public List<Map<String, Object>> getCompanyService(String factName) {
                return null;
            }
        };
    }
}
