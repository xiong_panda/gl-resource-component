package com.gl.wanfang.controller.outController;

import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * xiaweijie
 * 万方知识文献搜索
 */
@RestController
@ApiIgnore
@RequestMapping("/outApi/literature")
@Api(value = "对外-万方",tags = {"对外-万方知识文献搜索"})
public class OutWanFangChliteratureController {

/*    @Autowired
    private JwtUtils jwtUtils;*/

    @Autowired
    IWanFangDataService wanFangDataService;

    @ApiOperation(value = "万方知识文献信息简略搜索")
    @PostMapping("/matchChliteratureSimpleInfo")
    @ResponseBody
    @RequiresPermissions(value = "gluser:select")
    public String MatchChliteratureSimpleInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        String data=wanFangDataService.knowledgeliterature(wfParamDTO,response,request);
        return data;

    }

    @ApiOperation(value = "万方知识文献详细信息搜索")
    @PostMapping("/matchChliteratureDetailedInfo")
    @ResponseBody
    public String MatchChliteratureDetailedInfo(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String data=wanFangDataService.idsSearch(wfParamDTO,response,request);
        return data;
    }

}
