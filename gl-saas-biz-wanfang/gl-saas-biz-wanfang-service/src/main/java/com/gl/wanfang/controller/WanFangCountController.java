package com.gl.wanfang.controller;

import com.gl.basis.common.log.LogUtil;
import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@Api(value = "万方",tags = {"万方统计"})
@RequestMapping("/stat")
public class WanFangCountController {

    @Autowired
    IWanFangDataService wanFangDataService;

    /**
     *统计检索
     * @param  '接口id'
     * @return
     *
     */
    @ApiOperation(value = " 统计检索接口")
    @PostMapping("/countByIndexName")
    @ResponseBody
    public String CountByIndexName(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response , HttpServletRequest request) {
        LogUtil.info("统计检索接口","dfffd");
        return wanFangDataService.StatisticalRetrieval(wfParamDTO,response,request);
    }

}
