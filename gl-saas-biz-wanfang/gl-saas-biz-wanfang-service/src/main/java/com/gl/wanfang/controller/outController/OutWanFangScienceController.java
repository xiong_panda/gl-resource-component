package com.gl.wanfang.controller.outController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.TechnologyAchievementsService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WTechnologyAchievementsOutDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 *
 * 万方科技成果搜索
 * 10006：科技成果
 */
@RestController
@Api(value = "对外-万方",tags = {"对外-万方科技成果搜索"})
@RequestMapping("/outApi/achievements")
public class OutWanFangScienceController {

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    TechnologyAchievementsService technologyAchievementsService;

    @ApiOperation(value = "万方科技成果简略检索")
    @PostMapping("/matchMcholarteThnologySearch")
    @ResponseBody
    public Page<WTechnologyAchievementsOutDto> matchMcholarteThnologySearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10006": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//科技成果简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData(wfParamDTO,response, request,indexName,data);
    }

    @ApiOperation(value = "万方科技成果详细检索")
    @PostMapping("/matchMcholarteThnologySearchDetail")
    @ResponseBody
    public Page<WTechnologyAchievementsOutDto> scholarteThnologySearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10006");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }
    @ApiOperation(value = "万方科技成果信息批量详情检索")
    @PostMapping("/scholarteThnologySearch")
    @ResponseBody
    public Page<WTechnologyAchievementsOutDto> matchMechanismInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10006": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//科技成果简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData(wfParamDTO,response, request,indexName,result);
    }

    private Page<WTechnologyAchievementsOutDto> getData(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WTechnologyAchievementsOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WTechnologyAchievementsOutDto> wTechnologyAchievementsOutDtos = technologyAchievementsService.flagStatusComponent(result);
            page.setList(wTechnologyAchievementsOutDtos);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }
}
