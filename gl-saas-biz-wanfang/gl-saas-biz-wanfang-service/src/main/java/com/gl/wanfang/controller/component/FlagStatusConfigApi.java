package com.gl.wanfang.controller.component;


import com.gl.wanfang.service.component.FlagStatusConfigService;
import dto.FlagStatusUpdateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController()
@Api(value = "标记构件配置")
@RequestMapping("/flagStatus")
public class FlagStatusConfigApi {
    @Autowired
    private FlagStatusConfigService flagStatusConfigService;

    @PostMapping("/update")
    @ApiOperation(value = "修改标记构件的标记属性")
    public String update(@RequestBody FlagStatusUpdateDTO dto) {
       return flagStatusConfigService.update(dto);
    }

    @PostMapping("/get")
    @ApiOperation(value = "获取标记构件的配置")
    public Map<String, Object> get() {
        return flagStatusConfigService.getflagStatusConfig();
    }
}
