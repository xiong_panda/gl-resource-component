package com.gl.wanfang.controller;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.feign.GuoLongClient;
import com.gl.wanfang.feign.LingDunClient;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.impl.MultitermServiceInvocationService;
import dto.SearchDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(value = "/data")
@Api(value = "多服务数据调用",tags = {"万方，宁波，东方灵盾等数据搜索"})
public class MultitermServiceInvocationController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    GuoLongClient guoLongClient;
    @Autowired
    LingDunClient lingDunClient;
    @Autowired
    MultitermServiceInvocationService serviceInvocationService;
    /**
     *万方数据接口、  创新助手
     * @param  '万方数据'
     * @return
     * 列表
     */
//    @RequiresPermissions(value = "user:select")
//    @ApiOperation(value = "综合服务调用")
//    @GetMapping("/dataCollection")
//    @ResponseBody
//    public Object dataCollection(String serviceID, String resourceCls, HttpServletResponse response, HttpServletRequest request) throws Exception {
//
//        Object objects = null;
//        try {
//            objects =serviceInvocationService.dataCollection(serviceID, resourceCls, response, request);
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        return objects;
//    }
    /**
     *万方数据接口、  创新助手
     * @param  '万方数据'
     * @return
     * 列表
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "综合服务调用2")
    @PostMapping ("/mutiDataSearch")
    @ResponseBody
    public Page<?> mutiDataSearch(@RequestBody SearchDTO dto, HttpServletResponse response, HttpServletRequest request) {
        try {
            return serviceInvocationService.dataCollection(dto, response, request);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Page<>();
    }

}
