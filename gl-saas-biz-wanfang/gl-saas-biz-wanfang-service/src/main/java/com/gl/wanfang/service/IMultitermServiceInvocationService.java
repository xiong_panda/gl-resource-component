package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import dto.SearchDTO;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IMultitermServiceInvocationService  {
// 批量详情  数据采集
public Page<?> dataCollection(SearchDTO dto, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException ;
//详情信息
    public Page<?> dataCollectionIds(SearchDTO dto, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException;
 //简略信息
    public Page<?> dataCollectionSearch(SearchDTO dto, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException;

public String fieldSearch(SearchDTO dto);

}
