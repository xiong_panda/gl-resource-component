package com.gl.wanfang.service.component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.mapper.core.StandardConfigMapper;
import dto.StandardCreateDTO;
import dto.StandardQueryDTO;
import dto.StandardUpdateDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;
import po.StandardConfigPO;

import java.util.Map;

@Service
public class StandardConfigService {
    private static final IdWorker idWorker = new IdWorker();

    @Autowired
    private StandardConfigMapper standardConfigMapper;

    public PageInfo<StandardConfigPO> getStanardConfig(StandardQueryDTO dto) {
        Page<StandardConfigPO> page = PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        standardConfigMapper.getStandardConfig(BeanMap.create(dto));
        return new PageInfo<>(page);
    }

    public String update(StandardUpdateDTO dto) throws Exception {
        StandardConfigPO standardConfigPO = new StandardConfigPO();
        BeanUtils.copyProperties(dto,standardConfigPO);

        if (standardConfigMapper.update(standardConfigPO) != 1) {
            throw new Exception("更新失败");
        }
        return "更新成功";
    }

    public String insert(StandardCreateDTO dto) throws Exception {
        StandardConfigPO standardConfigPO = new StandardConfigPO();
        BeanUtils.copyProperties(dto,standardConfigPO);
        standardConfigPO.setId(String.valueOf(idWorker.nextId()));

        if (standardConfigMapper.insert(standardConfigPO) != 1) {
            throw new Exception("插入失败");
        }

        return "插入成功";

    }
}
