package com.gl.wanfang.feign;


//import com.gl.wanfang.config.NativeFeignConf;
//import com.gl.wanfang.config.NativeFeignConf;
import com.gl.basis.common.pojo.Page;
import dto.DFParamDTO;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@FeignClient(value = "saas-platform-dongfang",fallbackFactory = LingDunClientCallBackFactory.class)//映射的服务名：在注册中心的服务名,告诉使用哪一个托底类
public interface LingDunClient {

//    @ApiOperation(value = "用户登录")
    @PostMapping("/userLogin")
    @ResponseBody
    public String UserLogin(@RequestParam("name") String name, @RequestParam("pwd") String pwd);

    //    @ApiOperation(value = "匹配列表检索")
    @PostMapping("/apiListSearch")
    @ResponseBody
    public List<Object> ApiListSearch(@RequestBody DFParamDTO dto) throws DocumentException, IllegalAccessException;

    //    @ApiOperation(value = "匹配列表检索 数据定时执行")
    @PostMapping("/apiListDataSearch")
    @ResponseBody
    public List<Object> ApiListDataSearch(@RequestBody DFParamDTO dto) throws DocumentException, IllegalAccessException;

    //    @ApiOperation(value = "匹配列表检索 数据定时执行")
    @PostMapping("/apiListDataSearchgn")
    @ResponseBody
    public Page<Object> apiListDataSearchgn(@RequestBody DFParamDTO dto) throws DocumentException, IllegalAccessException;

//    @ApiOperation(value = "详情检索")
    @PostMapping("/apiDocinfoSeach")
    @ResponseBody
    public String ApiDocinfoSeach(@RequestBody DFParamDTO dto);
}
