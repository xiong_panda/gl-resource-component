package com.gl.wanfang.controller.outController;

import com.gl.basis.common.log.LogUtil;
import com.gl.wanfang.feign.GuoLongClient;
import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/outApi/wanfang")
@Api(value = "对外-万方",tags = {"对外-万方搜索"})
@SuppressWarnings("all")
public class OutWanFangDataController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    GuoLongClient guoLongClient;
    /**
     *万方数据接口、  创新助手
     * @param  '万方数据'
     * @return
     * 列表
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "技术报告接口")
    @PostMapping("/ckeyCon")
    @ResponseBody
    public void ckeyCon(String wfmds,String wid,String docTI,HttpServletResponse response, HttpServletRequest request) {
        wanFangDataService.ckeyCon(wfmds,wid,docTI,response,request);
    }

    /**
     *万方数据接口、生成token
     * @param  '接口id'
     * @return
     *
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "token生成接口")
   // @PostMapping("/creatToken")
    @RequestMapping(value="/creatToken" ,method = RequestMethod.GET)
    @ResponseBody
    public String creatToken(String appid, HttpServletResponse response, HttpServletRequest request) {
        return  wanFangDataService.creatToken(appid, response, request);

    }


    /**
     *统计检索
     * @param  '接口id'
     * @return
     *
     */
//    @Cacheable("StatisticalRetrieval")
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = " 统计检索接口")
    @PostMapping("/statisticalRetrieval")
    @ResponseBody
    public String StatisticalRetrieval(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response , HttpServletRequest request) {
        LogUtil.info("统计检索接口","dfffd");
        return wanFangDataService.StatisticalRetrieval(wfParamDTO,response,request);
    }


    /**
     *聚合检索
     * @param indexName 索引库
     * @return
     *
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "聚合统计检索接口")
    @PostMapping("/AggregationSearch")
    @ResponseBody
    public String AggregationSearch(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response ,HttpServletRequest request) {
        LogUtil.info("聚合检索接口","dfffd");
        return wanFangDataService.AggregationSearch(wfParamDTO,response,request);
    }

    /*
     * xiaweijie
     * Filter说明：
     * 采用过滤器，多个过滤条件取交集，不会对命中文档打分，性能更快。多个过滤条件可以用 &分隔。
     * 例如： 检索 2019 年关键词为无人机 ： filter=YEAR:2019&KEYWORD:无人机
     * 检索 2019 和 2018 年 ： filter=YEAR:2019,2018
     * 检索无人机或人工智能 ： filter=KEYWORD:无人机,人工智能
     * 检索无人机且人工智能 ： filter=KEYWORD:无人机&KEYWORD:人工智能
     * 检索无人机且不包含人工智能 ： filter=KEYWORD:无人机&{mustNot= KEYWORD:人工智 能}
     * */

    /**
     * 匹配检索
     * xiaweijie
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "匹配检索")
    @PostMapping("/matchSeach")
    @ResponseBody
    public String matchSeach(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        LogUtil.info("匹配检索","dfffd");
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request);
//        Map map = (Map)JSON.parse(data);
       /* String data1=wanFangDataService.StatisticalRetrieval(response,request);
        Map map1 = (Map)JSON.parse(data1);
        Object data2 = map1.get("data");
        Map datamap=(Map)map.get("data");
        if(data2!=null){
            datamap.put("total", data2);
        }*/
        return data;
    }


    /**
     * xiaweijie
     * 匹配检索：检索简略信息时同时检索详情信息
     * @param response
     * @param request
     * @return
     */
    @ApiOperation(value = "输入信息详情检索")
    @PostMapping("/matchSeachAndIdsInfo")
    @ResponseBody
    public String matchSeachAndIdsInfo(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request);//简略信息匹配
        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data);

        return result;

    }

    /**
     * 领域、、、、行业模型
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "领域行业分类模型检索")
    @PostMapping("/menuSearch")
    @ResponseBody
    @ApiIgnore
    public List<Map<String, Object>> menuSearch(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        LogUtil.info("领域行业分类模型检索","dfffd");
        List<Map<String, Object>> maps = wanFangDataService.menuSearch(wfParamDTO,response, request);

        return maps;
    }

    //http://api.kefuju.cn/open/api/idsSearch?indexName=10001&ids=QKC201720170323000660 87 是否需要 token
    /**
     * 详情检索
     * xiaweijie
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "详情检索")
    @PostMapping("/idsSearch")
    @ResponseBody
    public String idsSearch(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        LogUtil.info("详情检索","dfffd");
        return wanFangDataService.idsSearch(wfParamDTO,response,request);
    }

    /**
     * 知识文献 匹配（简介）检索
     * @AU yangpan
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "知识文献 匹配（简介）检索")
    @PostMapping("/knowledgeliterature")
    @ResponseBody
    public String knowledgeliterature(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.knowledgeliterature(wfParamDTO,response,request);
    }

    /**
     * 企业匹配（简介）检索
     * @AU yangpan
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "企业匹配（简介）检索")
    @PostMapping("/companyProfile")
    @ResponseBody
    public String companyProfile(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.companyProfile(wfParamDTO,response,request);
    }
    /**
     * 企业(详情）检索
     * yangpan
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "企业详情检索")
    @PostMapping("/companyIds")
    @ResponseBody
    public String companyIds(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
//        String data=wanFangDataService.companyIds(response,request);
//        Map map = (Map)JSON.parse(data);
//        Map map1 = (Map) map.get("data");
//        JSONArray sources = (JSONArray) map1.get("sources");
//        Map map2 =null;
//        if(sources.size()!=0){
//            map2=(Map)sources.get(0);
//            String corpname = map2.get("CORPNAME") + "";
//            map.put("guolong", guoLongClient.getCompanyService(corpname.substring(0,4)));
//        }
//        return JSON.toJSONString(map);
        return wanFangDataService.companyIds(wfParamDTO,response,request);
    }
    /**
     * 企业技术清单检索
     * yangpan
     * @param response
     * @param request
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "企业技术清单检索")
    @PostMapping("/companyTechnology")
    @ResponseBody
    public String companyTechnology(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.companyTechnology(wfParamDTO,response,request);
    }

    /**
     * 企业专家学者清单检索
     * yangpan
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "企业专家学者清单检索")
    @PostMapping("/companyScholar")
    @ResponseBody
    public String companyScholar(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.companyScholar(wfParamDTO,response,request);
    }
    /**
     * 企业专家学者详情检索
     * yangpan
     * @param response
     * @param request
     * @return
     */
//    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "企业专家学者详情检索")
    @PostMapping("/companyScholarIds")
    @ResponseBody
    public String companyScholarIds(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.companyScholarIds(wfParamDTO,response,request);
    }
    /**
     * 专家发表技术清单（科技成果）
     * yangpan
     * @param response
     * @param request
     * @return
     */
    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "专家发表技术清单（科技成果）")
    @PostMapping("/scholarTechnologySearch")
    @ResponseBody
    public String scholarTechnologySearch(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.scholarTechnologySearch(wfParamDTO,response,request);
    }
    /**
     * 专家发表论文清单（论文）
     * yangpan
     * @param response
     * @param request
     * @return
     */
    @RequiresPermissions(value = "user:select")
    @ApiOperation(value = "专家发表技术清单（科技成果）")
    @PostMapping("/scholarThesisSearch")
    @ResponseBody
    public String scholarThesisSedownFilearch(@RequestBody WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        return wanFangDataService.scholarThesisSearch(wfParamDTO,response,request);
    }
}
