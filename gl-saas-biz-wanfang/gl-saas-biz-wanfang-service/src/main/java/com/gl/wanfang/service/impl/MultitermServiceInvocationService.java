package com.gl.wanfang.service.impl;

import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.StringUtils;
import com.gl.basis.util.RedisUtil;
import com.gl.wanfang.controller.outController.*;
import com.gl.wanfang.feign.GuoLongClient;
import com.gl.wanfang.feign.LingDunClient;
import com.gl.wanfang.mapper.core.GlFieldContrastMapper;
import com.gl.wanfang.mapper.core.PageInfoMapper;
import com.gl.wanfang.service.*;
import dto.DFParamDTO;
import dto.SearchDTO;
import dto.WFParamDTO;
import en.EOutApiErrorCode;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
@SuppressWarnings("all")
public class MultitermServiceInvocationService implements IMultitermServiceInvocationService {
    /*
     *索引库对照表
     *10001;   wf_mds_chn_qikan;中文期刊论文
     *10002;   wf_mds_chn_huiyi;中文会议论文
     *10003;   wf_mds_chn_xuewei;中文学位论文
     *10004;   wf_mds_chn_zhuanli;中国专利
     *10005;   wf_mds_chn_biaozhun;中外标准
     *10006;   wf_mds_chn_cstad;科技成果
     *10007;   wf_mds_baike;百科词条
     *10008;   wf_mds_thesaurus;叙词表
     *10009;   wf_mds_ttauthor;智库作者
     *10010;   wf_mds_ttnews;智库新闻
     *10011;   wf_mds_cecdb;企业产品数据库
     *10012;   wf_mds_chn_claw;法律法规
     *10013;   wf_mds_daxue;高等院校
     *10014;   wf_mds_experts;专家库
     *10015;   wf_mds_project;项目库
     *10016;   wf_mds_en_zhuanli;海外专利
     *10017;   wf_mds_ttorg;智库机构
     *10018;   cstpcd_en_lw;CSTPCD 英文论文
     *10019;   wf_mds_en_huiyi;外文会议
     *10020;   wf_mds_auth_new;作者库
     *10021;   wf_mds_en_qikan;外文期刊论文
     *10022;   wf_mds_csi;科研机构
     *10023;   wf_mds_bid;公告信息
     *10024;   cstpdc_en_lw;
     *10025;   wf_mds_meeting;会议名录
     *10026;   wf_mds_journal;刊名
     *10027;   wf_mds_dic_all;英汉词表
     *10028;   wf_mds_chn_yinwen;中文会议引文
     */

    @Autowired
    RedisUtil redisUtil ;
    @Autowired
    GuoLongClient guoLongClient ;

    @Autowired
    PageInfoMapper pageInfoMapper ;
    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    LingDunClient lingDunClient;
    @Autowired
    MultitermServiceInvocationService serviceInvocationService;

    @Autowired
    OutWanFangCAUAByDBController wanFangCAUAByDBController;
    @Autowired
    OutWanFangChliteratureController wanFangChliteratureController;
    @Autowired
    OutWanFangCombinationController wanFangCombinationController;
    @Autowired
    OutWanFangCompanyDataController wanFangCompanyDataController;
    @Autowired
    OutWanFangCountController wanFangCountController;
    @Autowired
    OutWanFangDataController wanFangDataController;
    @Autowired
    OutWanFangExpertDataController wanFangExpertDataController;
    @Autowired
    OutWanFangLawController wanFangLawController;
    @Autowired
    OutWanFangMechanismController wanFangMechanismController;
    @Autowired
    OutWanFangPatentDataController wanFangPatentDataController;
    @Autowired
    OutWanFangPeriodicalController wanFangPeriodicalController;
    @Autowired
    OutWanFangScienceController wanFangScienceController;

    private static final Logger logger = LoggerFactory.getLogger(MultitermServiceInvocationService.class);



    @Autowired
    private GlFieldContrastMapper glFieldContrastMapper;

    @Override
    public Page<?> dataCollection(SearchDTO dto, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        if (StringUtils.isEmpty(dto.getResourceCls())) {
            dto.setResourceCls("10004");
        }

        dto.setServiceID("1");

        logger.info("searchDTO:"+dto.toString());

        //        dto.setServiceID(String.valueOf((Math.random()*10)%2+1));
        if ("1".equals(dto.getServiceID())&&!StringUtils.isEmpty(dto.getResourceCls())){//万方数据
            //return chinaPatentService.query(dto);
            WFParamDTO wfParamDTO=new WFParamDTO();
            wfParamDTO.setFrom(dto.getFrom());
            if (dto.getPageSize() != null) {
                wfParamDTO.setSize(dto.getPageSize());
            }
            String searchFiled = fieldSearch(dto);
            //wfParamDTO.setFilter(dto.getKeyword()==null?"":"KEYWORD:"+dto.getKeyword());
            wfParamDTO.setFilter(searchFiled);

            logger.info("wfParamDTO:"+wfParamDTO.toString());

            if (dto.getResourceCls().equals("10001")){//期刊
                return wanFangPeriodicalController.matchMechanismInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10013")){//高等院校
                return wanFangCAUAByDBController.matchHLUInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10040")){//万方外文oA论文
                return wanFangCombinationController.matchForeignLanguagePaperOAPOInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10037")){//万方中文oA论文
                return wanFangCombinationController.matchChinesePaperOAInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10011")){//万方企业
                return wanFangCompanyDataController.matchCompanyInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10014")){//万方专家信息
                return wanFangExpertDataController.matchExpertInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10020")){//万方作者信息
                return wanFangExpertDataController.matchAuthorInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10012")){//万方法律法规
                return wanFangLawController.matchMechanismInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10022")){//万方机构
                return wanFangMechanismController.matchMechanismSearch(wfParamDTO,response,request);
                }else if(dto.getResourceCls().equals("10033")){//万方信息机构信息
                return wanFangMechanismController.matchInfoOrg(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10004")){//万方中文专利信息
                return wanFangPatentDataController.matchPatentInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10016")){//万方海外专利信息
                return wanFangPatentDataController.matchOverseasPatentInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10021")){//万方外文期刊论文
                return wanFangPeriodicalController.matchForeignLanguagePaperPOInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10002")){//万方中文会议论文
                return wanFangPeriodicalController.matchChinaConferencePaperPOInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10006")){//万方科技成果信息
                return wanFangScienceController.matchMechanismInfo(wfParamDTO,response,request);
            }
        }else if("2".equals(dto.getServiceID().toString())&& !StringUtils.isEmpty(dto.getResourceCls())){
            //东方灵盾数据
            DFParamDTO dfParamDTO=new DFParamDTO();
            dfParamDTO.setPn(String.valueOf(Integer.valueOf(dto.getFrom())/10)+1);
            //dfParamDTO.setQ(dto.getKeyword()==null?"TI":"TI="+dto.getKeyword());
            if (dto.getResourceCls().equals("10004")){
                dto.setResourceCls("4a2f5e1e");
                dfParamDTO.setQ(fieldSearch(dto));
                Page<Object> page = lingDunClient.apiListDataSearchgn(dfParamDTO);
                return page;
            }

        }else if("3".equals(dto.getServiceID().toString())&& !StringUtils.isEmpty(dto.getResourceCls())){//宁波数据
            // TODO: 2020/6/1  调用宁波信息院的接口
        }else {
            return new Page<>();
        }
        return new Page<>();
    }

    @Override
    public Page<?> dataCollectionIds(SearchDTO dto, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        if (StringUtils.isEmpty(dto.getResourceCls())) {
            dto.setResourceCls("10004");
        }

        dto.setServiceID("1");

        logger.info("searchDTO:"+dto.toString());

        //        dto.setServiceID(String.valueOf((Math.random()*10)%2+1));
        if ("1".equals(dto.getServiceID().toString())&&!StringUtils.isEmpty(dto.getResourceCls())){//万方数据
            //return chinaPatentService.query(dto);
            WFParamDTO wfParamDTO=new WFParamDTO();
            wfParamDTO.setFrom(dto.getFrom());
            wfParamDTO.setIds(dto.getIds());
            if (dto.getPageSize() != null) {
                wfParamDTO.setSize(dto.getPageSize());
            }
            //wfParamDTO.setFilter(dto.getKeyword()==null?"":"KEYWORD:"+dto.getKeyword());
            wfParamDTO.setIds(dto.getIds());

            logger.info("wfParamDTO:"+wfParamDTO.toString());

            if (dto.getResourceCls().equals("10001")){//期刊
                return wanFangPeriodicalController.chineseJournalPapersIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10013")){//高等院校
                return wanFangCAUAByDBController.matchHLUSearchInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10040")){//万方外文oA论文
                return wanFangCombinationController.MatchCombinationDetailedInfo(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10037")){//万方中文oA论文
                return wanFangCombinationController.matchChinesePaperOAInfoData(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10011")){//万方企业
                return wanFangCompanyDataController.matchCropNameSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10014")){//万方专家信息
               return wanFangExpertDataController.matchExpertInfoSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10020")){//万方作者信息
                return wanFangExpertDataController.matchAuthorIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10012")){//万方法律法规
                return wanFangLawController.matchMechanismIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10022")){//万方机构
                return wanFangMechanismController.matchMechanismIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10033")){//万方信息机构信息
                return wanFangMechanismController.matchInfoOrgIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10004")){//万方中文专利信息
                return wanFangPatentDataController.matchChinesePatentIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10016")){//万方海外专利信息
                return wanFangPatentDataController.matchOverseasPatentSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10021")){//万方外文期刊论文
                return wanFangPeriodicalController.matchForeignPeriodicalPaperIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10002")){//万方中文会议论文
                return wanFangPeriodicalController.matchCNConferencePaperIdsSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10006")){//万方科技成果信息
                return wanFangScienceController.scholarteThnologySearch(wfParamDTO,response,request);
            }
        }else if("2".equals(dto.getServiceID().toString())&& !StringUtils.isEmpty(dto.getResourceCls())){
            //东方灵盾数据
            DFParamDTO dfParamDTO=new DFParamDTO();
            dfParamDTO.setPn(String.valueOf(Integer.valueOf(dto.getFrom())/10)+1);
            //dfParamDTO.setQ(dto.getKeyword()==null?"TI":"TI="+dto.getKeyword());
            if (dto.getResourceCls().equals("10004")){
                dto.setResourceCls("4a2f5e1e");
                dfParamDTO.setQ(fieldSearch(dto));
                Page<Object> page = lingDunClient.apiListDataSearchgn(dfParamDTO);
                return page;
            }

        }else if("3".equals(dto.getServiceID().toString())&& !StringUtils.isEmpty(dto.getResourceCls())){//宁波数据
            // TODO: 2020/6/1  调用宁波信息院的接口
        }else {
            return null;
        }
        return null;
    }

    @Override
    public Page<?> dataCollectionSearch(SearchDTO dto, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        if (StringUtils.isEmpty(dto.getResourceCls())) {
            dto.setResourceCls("10004");
        }

        dto.setServiceID("1");

        logger.info("searchDTO:"+dto.toString());

        //        dto.setServiceID(String.valueOf((Math.random()*10)%2+1));
        if ("1".equals(dto.getServiceID().toString())&&!StringUtils.isEmpty(dto.getResourceCls())){//万方数据
            //return chinaPatentService.query(dto);
            WFParamDTO wfParamDTO=new WFParamDTO();
            wfParamDTO.setFrom(dto.getFrom());
            if (dto.getPageSize() != null) {
                wfParamDTO.setSize(dto.getPageSize());
            }
            String searchFiled = fieldSearch(dto);
            //wfParamDTO.setFilter(dto.getKeyword()==null?"":"KEYWORD:"+dto.getKeyword());
            wfParamDTO.setFilter(searchFiled);

            logger.info("wfParamDTO:"+wfParamDTO.toString());

            if (dto.getResourceCls().equals("10001")){//期刊
                return wanFangPeriodicalController.chineseJournalPapersSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10013")){//高等院校
                return wanFangCAUAByDBController.matchHLUSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10040")){//万方外文oA论文
                return wanFangCombinationController.matchForeignLanguagePaperOAPOSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10037")){//万方中文oA论文
                return wanFangCombinationController.matchChinesePaperOASearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10011")){//万方企业
                return wanFangCompanyDataController.matchCompanySearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10014")){//万方专家信息
                return wanFangExpertDataController.matchExpertSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10020")){//万方作者信息
                return wanFangExpertDataController.matchAuthorSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10012")){//万方法律法规
                return wanFangLawController.matchMechanismSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10022")){//万方机构
                return wanFangMechanismController.matchScientificMechanismSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10033")){//万方信息机构信息
                return wanFangMechanismController.matchInfoOrgSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10004")){//万方中文专利信息
                return wanFangPatentDataController.matchChinesePatentSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10016")){//万方海外专利信息
                return wanFangPatentDataController.matchOverseasPatentSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10021")){//万方外文期刊论文
                return wanFangPeriodicalController.chineseJournalPapersSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10002")){//万方中文会议论文
                return wanFangPeriodicalController.matchChinaConferencePaperPOSearch(wfParamDTO,response,request);
            }else if(dto.getResourceCls().equals("10006")){//万方科技成果信息
                return wanFangScienceController.matchMcholarteThnologySearch(wfParamDTO,response,request);
            }
        }else if("2".equals(dto.getServiceID().toString())&& !StringUtils.isEmpty(dto.getResourceCls())){
            //东方灵盾数据
            DFParamDTO dfParamDTO=new DFParamDTO();
            dfParamDTO.setPn(String.valueOf(Integer.valueOf(dto.getFrom())/10)+1);
            //dfParamDTO.setQ(dto.getKeyword()==null?"TI":"TI="+dto.getKeyword());
            if (dto.getResourceCls().equals("10004")){
                dto.setResourceCls("4a2f5e1e");
                dfParamDTO.setQ(fieldSearch(dto));
                Page<Object> page = lingDunClient.apiListDataSearchgn(dfParamDTO);
                return page;
            }

        }else if("3".equals(dto.getServiceID().toString())&& !StringUtils.isEmpty(dto.getResourceCls())){//宁波数据
            // TODO: 2020/6/1  调用宁波信息院的接口
        }else {
            return null;
        }
        return null;
    }

    @Override
    public String fieldSearch(SearchDTO dto){
        try {
            String zf = ":";
            if ("2".equals(dto.getResourceCls().toString())) {
                zf = "=";
            }
            StringBuilder sb = new StringBuilder();
            String[] split = dto.getField().toString().split("&");
            if (split.length > 0) {
                for (String s : split) {
                    String[] split1 = s.toString().split(":");
                    if (split1.length >= 2) {
                        String wfFiled = glFieldContrastMapper.SelectFiledName(split1[0], dto.getResourceCls());
                        if (!StringUtils.isEmpty(wfFiled)) {
                            sb.append(wfFiled + zf + split1[1]);
                        } else {
                            continue;
                        }
                    }
                    sb.append("&");
                }
            }
            return sb.substring(0, sb.toString().length() - 1);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(EOutApiErrorCode.FIELD_ERR.msg, EOutApiErrorCode.FIELD_ERR.code);
        }

    }
}
