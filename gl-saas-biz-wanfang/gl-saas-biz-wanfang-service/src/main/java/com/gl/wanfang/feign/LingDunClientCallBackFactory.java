package com.gl.wanfang.feign;

import com.gl.basis.common.log.LogUtil;
import com.gl.basis.common.pojo.Page;
import dto.DFParamDTO;
import feign.hystrix.FallbackFactory;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Component
public class LingDunClientCallBackFactory implements FallbackFactory<LingDunClient> {

    @Override
    public LingDunClient create(Throwable throwable) {
        return new LingDunClient() {
            @Override
            public String UserLogin(String name, String pwd) {
                return null;
            }

            @Override
            public List<Object> ApiListSearch(DFParamDTO dto) throws DocumentException, IllegalAccessException {
                return null;
            }

            @Override
            public List<Object> ApiListDataSearch(DFParamDTO dto) throws DocumentException, IllegalAccessException {
                return null;
            }

            @Override
            public Page<Object> apiListDataSearchgn(DFParamDTO dto) throws DocumentException, IllegalAccessException {
                return null;
            }

            @Override
            public String ApiDocinfoSeach(DFParamDTO dto) {
                return null;
            }
        };
    }
}
