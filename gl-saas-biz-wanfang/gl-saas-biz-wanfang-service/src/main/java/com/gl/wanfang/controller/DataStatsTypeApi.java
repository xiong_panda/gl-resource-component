package com.gl.wanfang.controller;


import com.gl.wanfang.service.impl.DataLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/dataStatsType")
@Api(value = "数据日志")
public class DataStatsTypeApi {
    @Autowired
    DataLogService dataLogService;
    

    @ApiOperation(value = "查询昨日资源类型情况")
    @PostMapping("/day")
    @ResponseBody
    public List<Map<String, Object>> queryTypeDay(){
        return dataLogService.queryTypeDay();
    }

    @ApiOperation(value = "查询上周资源类型情况")
    @PostMapping("/week")
    @ResponseBody
    public List<Map<String, Object>> queryTypeWeek(){
        return dataLogService.queryTypeWeek();
    }

    @ApiOperation(value = "查询上月资源类型情况")
    @PostMapping("/month")
    @ResponseBody
    public List<Map<String, Object>> queryTypeMonth(){
        return dataLogService.queryTypeMonth();
    }

    @ApiOperation(value = "查询上季度资源类型情况")
    @PostMapping("/quarter")
    @ResponseBody
    public List<Map<String, Object>> queryTypeQuarter(){
        return dataLogService.queryTypeQuarter();
    }

    @ApiOperation(value = "查询上半年资源类型情况")
    @PostMapping("/halfyear")
    @ResponseBody
    public List<Map<String, Object>> queryTypeHalfyear(){
        return dataLogService.queryTypeHalfyear();
    }

    @ApiOperation(value = "查询去年资源类型情况")
    @PostMapping("/year")
    @ResponseBody
    public List<Map<String, Object>> queryTypeYear(){
        return dataLogService.queryTypeYear();
    }

    @ApiOperation(value = "资源池各资源类型月份折线图")
    @PostMapping("/polyline")
    @ResponseBody
    public Map<String, List<Map<String, Object>>> queryTypePolyline(){
        return dataLogService.typePolyline();
    }
    
}
