package com.gl.wanfang.controller.component;

import com.gl.wanfang.service.component.IntegrateConfigService;
import dto.IntegrateCreateDTO;
import dto.IntegrateUpdateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController()
@Api(value = "集成构件配置")
@RequestMapping("/integrate")
public class IntegrateConfigApi {
    @Autowired
    private IntegrateConfigService integrateConfigService;

    @PostMapping("/update")
    @ApiOperation(value = "更新")
    public String update(@RequestBody IntegrateUpdateDTO dto) throws Exception {
        return integrateConfigService.update(dto);
    }

    @PostMapping("/insert")
    @ApiOperation(value = "新增")
    public String update(@RequestBody IntegrateCreateDTO dto) throws Exception {
        return integrateConfigService.insert(dto);
    }

    @PostMapping("/query")
    @ApiOperation(value = "获取全部数据")
    public List<Map<String, Object>> get() {
        return integrateConfigService.getIntegrateConfig();
    }
}
