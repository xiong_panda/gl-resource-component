package com.gl.wanfang.controller.outController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.HigherLearningUniversitiesService;
import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WHigherLearningUniversitiesOutDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 万方知识文献搜索
 */
@RestController
@RequestMapping("/outApi/Universities")
@Api(value = "对外-万方",tags = {"对外-万方高等院校"})
public class OutWanFangCAUAByDBController {

/*    @Autowired
    private JwtUtils jwtUtils;*/

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    HigherLearningUniversitiesService higherLearningUniversitiesService;

    @ApiOperation(value = "万方高等院校简略检索")
    @PostMapping("/matchHLUSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WHigherLearningUniversitiesOutDto> matchHLUSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10013":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配
        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData(wfParamDTO,response,request,indexName,data);
    }


    @ApiOperation(value = "万方高等院校详情检索")
    @PostMapping("/hLUSearchInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WHigherLearningUniversitiesOutDto> matchHLUSearchInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10013");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }


    @ApiOperation(value = "万方高等院校批量详情检索")
    @PostMapping("/matchHLUInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WHigherLearningUniversitiesOutDto> matchHLUInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10013":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);
        return getData(wfParamDTO,response,request,indexName,result);
    }

    private Page<WHigherLearningUniversitiesOutDto> getData(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WHigherLearningUniversitiesOutDto> page = new Page<>();

        String currentPage=wfParamDTO.getFrom();

        if ("200".equals(statusCode)) {
            List<WHigherLearningUniversitiesOutDto> wHigherLearningUniversitiesOutDtos = higherLearningUniversitiesService.flagStatusComponent(result);
            page.setList(wHigherLearningUniversitiesOutDtos);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));
        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

}
