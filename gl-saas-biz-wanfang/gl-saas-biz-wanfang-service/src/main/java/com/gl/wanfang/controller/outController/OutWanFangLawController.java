package com.gl.wanfang.controller.outController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.LawsRegulationsService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WLawsRegulationsOutDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 *
 * 万方法律法规搜索
 * 10012：法律法规
 */
@RestController
@Api(value = "对外-万方",tags = {"对外-万方法律法规搜索"})
@RequestMapping("/outApi/law")
public class OutWanFangLawController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    LawsRegulationsService lawsRegulationsService;

    @ApiOperation(value = "万方法律法规简略检索")
    @PostMapping("/matchLawRegulationsSearch")
    @ResponseBody
    public Page<WLawsRegulationsOutDTO> matchMechanismSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10012": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//法律法规简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData(wfParamDTO,response,request,indexName,data);

    }


    @ApiOperation(value = "万方法律法规详情信息检索")
    @PostMapping("/matchLawRegulationsIdsSearch")
    @ResponseBody
    public Page<WLawsRegulationsOutDTO> matchMechanismIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10012");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);

    }
    @ApiOperation(value = "万方法律法规详情检索")
    @PostMapping("/lawRegulationsSearch")
    @ResponseBody
    public Page<WLawsRegulationsOutDTO> matchMechanismInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10012": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//法律法规简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData(wfParamDTO,response,request,indexName,result);

    }

    private Page<WLawsRegulationsOutDTO> getData( WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WLawsRegulationsOutDTO> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WLawsRegulationsOutDTO> authorLibraryPOList = lawsRegulationsService.flagStatusComponent(result);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

}
