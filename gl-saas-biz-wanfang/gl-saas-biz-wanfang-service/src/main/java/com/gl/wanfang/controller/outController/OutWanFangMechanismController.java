package com.gl.wanfang.controller.outController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.InfoOrgService;
import com.gl.wanfang.service.ScientificOrgService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WScientificOrgOutDto;
import po.InfoOrgPO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 万方机构搜索
 */
@RestController
@Api(value = "对外-万方",tags = {"对外-万方机构搜索"})
@RequestMapping("/outApi/org")
public class OutWanFangMechanismController {

    @Autowired
    ScientificOrgService scientificOrgService;
    @Autowired
    InfoOrgService infoOrgService;
    @Autowired
    IWanFangDataService wanFangDataService;

    @ApiOperation(value = "万方科研机构简略信息检索")
    @PostMapping("/matchScientificMechanismSearch")
    @ResponseBody
    public Page<WScientificOrgOutDto> matchScientificMechanismSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10022": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//机构简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData(wfParamDTO,response, request,indexName,data);

    }
    @ApiOperation(value = "万方科研机构详细信息检索")
    @PostMapping("/matchMechanismIdsSearch")
    @ResponseBody
    public Page<WScientificOrgOutDto> matchMechanismIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10022");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方科研机构批量详情检索")
    @PostMapping("/matchMechanismSearch")
    @ResponseBody
    public Page<WScientificOrgOutDto> matchMechanismSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10022": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//机构简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData(wfParamDTO,response, request,indexName,data);

    }

    private Page<WScientificOrgOutDto> getData(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        //查询出所有条数
        String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO,response, request,indexName);
        String count="";
        Map parse = (Map) JSON.parse(resultJson);
        if("200".equals(parse.get("code").toString())){
            List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
            count = data1.get(0).get("value").toString();
        }
        //封装page
        Page<WScientificOrgOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();;
        if ("200".equals(statusCode)) {
            List<WScientificOrgOutDto> authorLibraryPOList = scientificOrgService.flagStatusComponent(result);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

    @ApiOperation(value = "万方信息机构=简略检索")
    @PostMapping("/matchInfoOrgSearch")
    @ResponseBody
    public Page<InfoOrgPO> matchInfoOrgSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10033": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//机构简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData2(wfParamDTO,response,request,indexName,data);
    }

    @ApiOperation(value = "万方信息机构详细信息检索")
    @PostMapping("/matchInfoOrgIdsSearch")
    @ResponseBody
    public Page<InfoOrgPO> matchInfoOrgIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10033");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData2(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方信息机构批量详情检索")
    @PostMapping("/matchInfoOrg")
    @ResponseBody
    public Page<InfoOrgPO> matchInfoOrg(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10033": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//机构简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =data;
        return getData2(wfParamDTO,response,request,indexName,result);
    }

    private Page<InfoOrgPO> getData2(WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<InfoOrgPO> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<InfoOrgPO> authorLibraryPOList = infoOrgService.flagStatusComponent(result);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

}
