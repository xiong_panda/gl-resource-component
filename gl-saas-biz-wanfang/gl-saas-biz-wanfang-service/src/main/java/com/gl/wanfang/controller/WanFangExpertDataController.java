package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.AuthorLibraryService;
import com.gl.wanfang.service.ExpertsLibraryService;
import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WAuthorLibraryOutDto;
import outdto.WExpertsLibraryOutDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 万方专家搜索
 */
@RestController
@RequestMapping(value = "/person")
@Api(value = "万方",tags = {"万方专家搜索"})
public class WanFangExpertDataController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    ExpertsLibraryService expertsLibraryService;
    @Autowired
    AuthorLibraryService authorLibraryService;
    @ApiOperation(value = "万方专家简略检索")
    @PostMapping("/matchExpertSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WExpertsLibraryOutDto> matchExpertSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10014":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        return getData2(wfParamDTO,response, request,indexName,data);
    }

    @ApiOperation(value = "万方专家详细信息检索")
    @PostMapping("/matchExpertIdsSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WExpertsLibraryOutDto> matchExpertInfoSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10014");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData2(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方专家批量详情信息检索")
    @PostMapping("/matchExpertInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WExpertsLibraryOutDto> matchExpertInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10014":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData2(wfParamDTO,response, request,indexName,result);

    }

    private Page<WExpertsLibraryOutDto> getData2(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WExpertsLibraryOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WExpertsLibraryOutDto> authorLibraryPOList = expertsLibraryService.flagStatusComponent(result);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

    @ApiOperation(value = "万方作者简略检索")
    @PostMapping("/matchAuthorSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WAuthorLibraryOutDto> matchAuthorSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10020":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        return getData(wfParamDTO,response, request, indexName, data);
    }

    @ApiOperation(value = "万方作者详细信息检索")
    @PostMapping("/matchAuthorIdsSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WAuthorLibraryOutDto> matchAuthorIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10020");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方作者信息详情检索")
    @PostMapping("/matchAuthorInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WAuthorLibraryOutDto> matchAuthorInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10020":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return null;
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData(wfParamDTO,response, request, indexName, result);

    }

    private Page<WAuthorLibraryOutDto> getData(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WAuthorLibraryOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WAuthorLibraryOutDto> authorLibraryPOList = authorLibraryService.flagStatusComponent(result);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

}
