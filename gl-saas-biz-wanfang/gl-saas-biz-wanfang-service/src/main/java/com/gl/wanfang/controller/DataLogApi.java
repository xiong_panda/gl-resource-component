package com.gl.wanfang.controller;


import com.github.pagehelper.PageInfo;
import com.gl.wanfang.service.impl.DataLogService;
import dto.DataLogQueryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import po.DataLogPO;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/dataLog")
@Api(value = "数据日志")
public class DataLogApi {
    @Autowired
    DataLogService dataLogService;

    @ApiOperation(value = "查询日志")
    @PostMapping("/query")
    @ResponseBody
    public PageInfo<DataLogPO> query(@RequestBody DataLogQueryDTO dto){
        return dataLogService.query(dto);
    }

}
