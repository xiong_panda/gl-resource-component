package com.gl.wanfang.controller.outController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.service.ChineseConferencePaperService;
import com.gl.wanfang.service.ChineseJournalPaperService;
import com.gl.wanfang.service.ForeignPeriodicalPaperService;
import com.gl.wanfang.service.IWanFangDataService;
import dto.WFParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import outdto.WChineseConferencePaperOutDto;
import outdto.WChineseJournalPaperOutDto;
import outdto.WForeignPeriodicalPaperOutDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 *
 * 万方中文期刊搜索
 * 10001：中文期刊
 */
@RestController
@Api(value = "对外-万方",tags = {"对外-万方论文搜索"})
@RequestMapping("/outApi/journal")
public class OutWanFangPeriodicalController {

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    ForeignPeriodicalPaperService foreignPeriodicalPaperService;
    @Autowired
    ChineseJournalPaperService chineseJournalPaperService;
    @Autowired
    ChineseConferencePaperService chineseConferencePaperService;

    @ApiOperation(value = "万方中文期刊论文简略检索")
    @PostMapping("/chineseJournalPapersSearch")
    @ResponseBody
    public Page<WChineseJournalPaperOutDto> chineseJournalPapersSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10001": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//中文期刊简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData(wfParamDTO,response,request,indexName,data);
    }


    @ApiOperation(value = "万方中文期刊论文详细信息检索")
    @PostMapping("/chineseJournalPapersIdsSearch")
    @ResponseBody
    public Page<WChineseJournalPaperOutDto> chineseJournalPapersIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10001");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);
    }

    @ApiOperation(value = "万方中文期刊论文批量详情检索")
    @PostMapping("/chineseJournalPapers")
    @ResponseBody
    public Page<WChineseJournalPaperOutDto> matchMechanismInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10001": wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//中文期刊简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData(wfParamDTO,response,request,indexName,result);

    }

    private Page<WChineseJournalPaperOutDto> getData( WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WChineseJournalPaperOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();

        if ("200".equals(statusCode)) {
            List<WChineseJournalPaperOutDto> wChineseJournalPaperOutDtos = chineseJournalPaperService.flagStatusComponent(result);
            page.setList(wChineseJournalPaperOutDtos);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

    @ApiOperation(value = "万方外文期刊论文简略检索")
    @PostMapping("/matchForeignPeriodicalPaperSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WForeignPeriodicalPaperOutDto> matchForeignPeriodicalPaperSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10021":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData2(wfParamDTO,response,request,indexName,data);

    }

    @ApiOperation(value = "万方外文期刊论文详细信息检索")
    @PostMapping("/matchForeignPeriodicalPaperIdsSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WForeignPeriodicalPaperOutDto> matchForeignPeriodicalPaperIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10021");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData2(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);

    }
    @ApiOperation(value = "万方外文期刊论文批量详情检索")
    @PostMapping("/matchForeignPeriodicalPaperInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WForeignPeriodicalPaperOutDto> matchForeignLanguagePaperPOInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10021":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配

        if(null==data || "".equals(data)){
            return new Page<>();
        }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.IdsInfo(wfParamDTO,response, request,data,indexName);

        return getData2(wfParamDTO,response,request,indexName,result);

    }

    private Page<WForeignPeriodicalPaperOutDto> getData2( WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();

        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WForeignPeriodicalPaperOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WForeignPeriodicalPaperOutDto> authorLibraryPOList = foreignPeriodicalPaperService.flagStatusComponent(result);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }

    @ApiOperation(value = "万方中文会议论文简略检索")
    @PostMapping("/matchCNConferencePaperSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WChineseConferencePaperOutDto> matchChinaConferencePaperPOSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10002":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配
        if(null==data || "".equals(data)){
            return new Page<>();
        }
        return getData3(wfParamDTO,response,request,indexName,data);

    }


    @ApiOperation(value = "万方中文会议论文详细检索")
    @PostMapping("/matchCNConferencePaperIdsSearch")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WChineseConferencePaperOutDto> matchCNConferencePaperIdsSearch(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        wfParamDTO.setIndexName("10002");
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
        String result =wanFangDataService.idsSearchs(wfParamDTO,response, request,wfParamDTO.getIds());
        return getData3(wfParamDTO,response,request,wfParamDTO.getIndexName(),result);

    }
   @ApiOperation(value = "万方中文会议论文批量详情检索")
    @PostMapping("/matchCNConferencePaperInfo")
    @ResponseBody
//    @RequiresPermissions(value = "wfuser:select")
    public Page<WChineseConferencePaperOutDto> matchChinaConferencePaperPOInfo(@RequestBody WFParamDTO wfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        String indexName = wfParamDTO.getIndexName()==null?"10002":wfParamDTO.getIndexName();
        String data=wanFangDataService.matchSeach(wfParamDTO,response,request,indexName);//专家简略信息匹配
       if(null==data || "".equals(data)){
           return new Page<>();
       }
        /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*/
       return getData3(wfParamDTO,response,request,indexName,data);

    }

    private Page<WChineseConferencePaperOutDto> getData3(WFParamDTO wfParamDTO,HttpServletResponse response, HttpServletRequest request, String indexName, String result) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(result, Map.class);
        String statusCode = resultMap.get("code").toString();


        String count="";
        if(wfParamDTO.getIds()!=null){
            String[] split = wfParamDTO.getIds().split(",");
            count=String.valueOf(split.length);
        }else {
            //查询出所有条数
            String resultJson = wanFangDataService.StatisticalRetrieval(wfParamDTO, response, request, indexName);

            Map parse = (Map) JSON.parse(resultJson);
            if ("200".equals(parse.get("code").toString())) {
                List<Map<String, Object>> data1 = (List<Map<String, Object>>) parse.get("data");
                count = (data1 == null ? "0" : data1.get(0).get("value").toString());
            }
        }
        //封装page
        Page<WChineseConferencePaperOutDto> page = new Page<>();
        String currentPage=wfParamDTO.getFrom();
        if ("200".equals(statusCode)) {
            List<WChineseConferencePaperOutDto> wChineseConferencePaperOutDtos = chineseConferencePaperService.flagStatusComponent(result);
            page.setList(wChineseConferencePaperOutDtos);
            page.setCode("200");
            page.setPageNo((int)Math.ceil(Double.valueOf(currentPage.equals("0")?"10":currentPage)/10));
            page.setTotalSize(Long.valueOf(count));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }

        return page;
    }
}
