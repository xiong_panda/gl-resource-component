package com.gl.wanfang.service;

import annotation.FieldOrder;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.util.RedisUtil;
import com.gl.wanfang.mapper.core.AuthorLibraryMapper;
import com.gl.wanfang.mapper.dictionary.MarkDataMapper;
import com.gl.wanfang.mapper.fifth.FifthAuthorLibraryMapper;
import com.gl.wanfang.mapper.subject.GlAuthorLibraryMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import en.EDataSource;
import en.EDataStatus;
import en.EDisciplineClassify;
import net.sf.json.xml.XMLSerializer;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import outdto.WAuthorLibraryOutDto;
import po.AuthorLibraryPO;
import po.MarkDataPO;
import po.WAuthorLibraryPO;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

//资源构件
@Service
public class AuthorLibraryService {
    @Autowired
    private AuthorLibraryMapper authorLibraryMapper;
    @Autowired
    private MarkDataMapper markDataMapper;
    @Autowired
    private GlAuthorLibraryMapper glAuthorLibraryMapper;
    @Autowired
    private FifthAuthorLibraryMapper fifthAuthorLibraryMapper;
    @Autowired
    RedisUtil redisUtil;

    private static final IdWorker idWorker = new IdWorker();

    private ExecutorService singleThreadExecutor= Executors.newSingleThreadExecutor();

    //标记构件,也是整个流程的开始
    public List<WAuthorLibraryOutDto> flagStatusComponent(String resultJson) throws IllegalAccessException, DocumentException {
        //数据转换
        Map<String, Object> resultMap = JSONObject.parseObject(resultJson, Map.class);
        String statusCode = resultMap.get("code").toString();
        List<Map> dataList;
        String mainDataJson="";
        Map<String, Object> dataMap;
        if("200".equals(statusCode)){
            dataMap= JSONObject.parseObject(resultMap.get("data").toString(), Map.class);
            if (dataMap.get("sources")!=null){
                mainDataJson=dataMap.get("sources").toString();
                dataList = JSONObject.parseArray(mainDataJson, Map.class);
            }else {
                return Collections.EMPTY_LIST;
            }
        }else {
            return Collections.EMPTY_LIST;
        }
        //进入字典库对照,如果都匹配不上,则不作后续处理,直接返回
        List<String> fieldList=authorLibraryMapper.ALselectField();
        boolean machType=false;//判断进入构件的是详情信息还是简略信息
        if(!CollectionUtils.isEmpty(dataList)&&dataList.get(0).keySet().contains("indexId")){
            machType=true;
            List<Map> arrList = new ArrayList<>();
            for (Map map : dataList) {
                Map map1 = JSONObject.parseObject(map.get("source").toString(), Map.class);
                arrList.add(map1);
            }
            dataList=arrList;
        }

        Set fieldSet = dataList.get(0).keySet();

        //如果一个字段都对应不上,则作废
        boolean i=true;

        for (Object field : fieldSet) {
            if(fieldList.contains(field)){
                i=false;
                break;
            }
        }
        if (i) {
            return null;
        }

        List<MarkDataPO> markList = new ArrayList<>();
        List<WAuthorLibraryPO> wAuthorLibraryPOList = new ArrayList<>();

        for (Map data : dataList) {
            WAuthorLibraryPO wAuthorLibraryPO = JSONObject.parseObject(
                    JSONObject.toJSONString(data), WAuthorLibraryPO.class);
            wAuthorLibraryPO.setWfId(data.get("ID").toString());
            //创建id
            String id = Long.toString(idWorker.nextId());
            wAuthorLibraryPO.setID(id);
            //创建对应的标记
            MarkDataPO markDataPO = new MarkDataPO(Long.toString(idWorker.nextId()), id, EDataStatus.NEW.code, EDataSource.WANG_FANG.code, "1");
            markList.add(markDataPO);
            wAuthorLibraryPOList.add(wAuthorLibraryPO);
        }
        //进入标准化构件
        List<WAuthorLibraryOutDto> wAuthorLibraryOutDtos = standardComponent(wAuthorLibraryPOList, markList,machType);
        return wAuthorLibraryOutDtos;
    }


    //标准化构件
    @Transactional(rollbackFor = Exception.class)
    public List<WAuthorLibraryOutDto> standardComponent(List<WAuthorLibraryPO> list, List<MarkDataPO> markList, Boolean machtype) throws IllegalAccessException, DocumentException {
        List<AuthorLibraryPO> authorLibraryPOList = new ArrayList<>();
        for (WAuthorLibraryPO wAuthorLibraryPO : list) {

            Field[] wFields = wAuthorLibraryPO.getClass().getDeclaredFields();

            AuthorLibraryPO authorLibraryPO = new AuthorLibraryPO();
            Field[] gFields = authorLibraryPO.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(authorLibraryPO, wfield.get(wAuthorLibraryPO));
                    }
                }
            }
            authorLibraryPO.setCreateTime(new Date());
            authorLibraryPO.setResourceFrom(EDataSource.WANG_FANG.value);
            authorLibraryPO.setResourceLogo(EDataSource.WANG_FANG.logo);
            authorLibraryPOList.add(authorLibraryPO);
        }

        List<WAuthorLibraryOutDto> authorLibraryOutDtos = new ArrayList<>();
        for (AuthorLibraryPO authorLibraryPO : authorLibraryPOList) {

            Field[] wFields = authorLibraryPO.getClass().getDeclaredFields();

            WAuthorLibraryOutDto authorLibraryOutDto = new WAuthorLibraryOutDto();
            Field[] gFields = authorLibraryOutDto.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(authorLibraryOutDto, wfield.get(authorLibraryPO));
                    }
                }
            }
            authorLibraryOutDtos.add(authorLibraryOutDto);
        }
        if(!machtype) {
            singleThreadExecutor.execute(() -> doInsert(authorLibraryPOList, markList));
        }
        return  authorLibraryOutDtos;
//        fifthAuthorLibraryMapper.ALbatchInsertToCore(authorLibraryPOList);

        //进入清洗构件
//        washComponent(authorLibraryPOList);
    }

    private void doInsert(List<AuthorLibraryPO> dataList, List<MarkDataPO> markList) {
        List<String> ids = dataList.stream().map(e -> e.getW_ID()).collect(Collectors.toList());

        List<String> existIds = authorLibraryMapper.queryIds(ids);
        List<AuthorLibraryPO> finalDataList = dataList.stream().filter(e -> !existIds.contains(e.getW_ID())).collect(Collectors.toList());

        List<String> localIds = finalDataList.stream().map(e -> e.getID()).collect(Collectors.toList());
        List<MarkDataPO> finalMarkList = markList.stream().filter(e -> localIds.contains(e.getDataId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(finalDataList)) {
            authorLibraryMapper.ALbatchInsertToCore(finalDataList);
            markDataMapper.batchInsertToCore(finalMarkList);
        }

    }

    //清洗构件
    private void washComponent(List<AuthorLibraryPO> list) throws IllegalAccessException, DocumentException {
        for (AuthorLibraryPO authorLibraryPO : list) {
            //反射字段类型为字符串的数据,进行清洗
            Field[] fields = authorLibraryPO.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(authorLibraryPO);

                if (value != null && value instanceof String ) {
                    //判断json是否为集合,如果是,去除json的格式,留下数据,用逗号分隔
                    if (jsonType(value) == 2) {
                        StringBuilder stringBuilder = new StringBuilder();
                        List<String> stringList = JSONObject.parseArray(value.toString(), String.class);

                        for (int i=0; i < stringList.size(); i++) {
                            if (i == stringList.size()-1) {
                                stringBuilder.append(stringList.get(i));
                                break;
                            }
                            stringBuilder.append(stringList.get(i)+",");
                        }

                        field.set(authorLibraryPO,stringBuilder.toString());
                    }
                    
                    //是否为xml数据,将xml数据转换为普通json
                    if (jsonType(value) == 3) {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        String xmlString = value.toString();
                        //int i = xmlString.indexOf(">");
                        //xmlString.substring(i+1)
                        String result = xmlSerializer.read(xmlString).toString();

                        field.set(authorLibraryPO,JSONObject.toJSONString(result));
                    }
                }

                //判断时间是否正常,不正常进行剔除
                if (value != null && value instanceof Date) {
                    Date date = (Date) value;
                    if (date.getTime() > System.currentTimeMillis()) {
                        field.set(authorLibraryPO,null);
                    }
                }
            }
        }

        //进入筛选构件
        screenComponent(list);
    }

    //筛选构件
    private void screenComponent(List<AuthorLibraryPO> list) {
        //按科学分类进行分类
        Map<String, List<AuthorLibraryPO>> mapByMainClass = list.stream().
                collect(Collectors.groupingBy(
                        e -> e.getDiscipline_Class() != null ? e.getDiscipline_Class_Code2().substring(0, 2) : "unknow")
                );
        //将空的分类号标识为未知分类
        if (mapByMainClass.get("unknow") != null) {
            mapByMainClass.get("unknow").stream().forEach(e->e.setDiscipline_Class_Code2("unknow"));
        }else {
            mapByMainClass.put("unknow", Lists.newArrayList());
        }


        if (mapByMainClass.size() > 1) {
            Set<String> keySet = Sets.newHashSet(mapByMainClass.keySet());
            keySet.remove("unknow");

            //将不在IPC分类标准中的数据也划分到未知分类中
            for (String key : keySet) {
                if (!EDisciplineClassify.keyList().contains(key)) {
                    List<AuthorLibraryPO> unknowList=mapByMainClass.get(key);
                    unknowList.stream().forEach(e->e.setDiscipline_Class_Code2("unknow"));
                    mapByMainClass.get("unknow").addAll(unknowList);
                    mapByMainClass.remove(key);
                }
            }
        }

        //进入集成构件
        integrateComponent(mapByMainClass);
    }

    //集成构件
    private void integrateComponent(Map<String, List<AuthorLibraryPO>> mapByMainClass) {
        //去重,专利去重的标准为申请号
        for (Map.Entry<String, List<AuthorLibraryPO>> entry : mapByMainClass.entrySet()) {
            List<AuthorLibraryPO> list = entry.getValue();
            List<AuthorLibraryPO> uniqueList = list.stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(
                            () -> new TreeSet<>(Comparator.comparing(e -> e.getName()))), ArrayList::new)
                    );

            if (uniqueList.size() != list.size()) {
                //迭代,留下时间最新,并且字段最多的数据
                list.removeAll(uniqueList);
                AuthorLibraryPO authorLibraryPO = list.stream().sorted(
                        Comparator.comparing(AuthorLibraryPO::getDate, Comparator.reverseOrder())
                                .thenComparing(
                                        e -> Arrays.stream(e.getClass().getDeclaredFields()).filter(f -> f != null).count())
                ).findFirst().get();

                uniqueList.add(authorLibraryPO);

                entry.setValue(uniqueList);

            }
            //存入主题资源库
            if (entry.getValue().size() > 0) {
                glAuthorLibraryMapper.ALbatchInsertToCore(
                        uniqueList,
                        (entry.getKey().equals("unknow")?"u":entry.getKey()+"0")+"_author_library" );

            }
        }
        //进入融合构件
        //mergeComponent(list);
    }

    //融合构件
    private List<?> mergeComponent(List<?> list) {

        //todo 存入标准资源库
        //返回数据
        return list;
    }


    //返回顺序字段
    private List<Field> getOrderedField(Field[] fields) {
        List<Field> fieldList = new ArrayList<>();
        for (Field field : fields) {
            if (field.getAnnotation(FieldOrder.class) != null) {
                fieldList.add(field);
            }
        }
        fieldList.sort(Comparator.comparing(
                e -> e.getAnnotation(FieldOrder.class).order()
        ));
        return fieldList;
    }

    public int jsonType(Object object){
        /*
         * return 0:既不是array也不是object
         * return 1：object
         * return 2 ：Array
         * return 3 ：xml
         */

        try {
            JSONObject.parseObject(object.toString());
            return 1;
        } catch (Exception e) {// 抛错 说明JSON字符不是数组或根本就不是JSON
            try {
                JSONObject.parseArray(object.toString());
                return 2;
            } catch (Exception ee) {// 抛错 说明JSON字符根本就不是JSON
                try {
                    DocumentHelper.parseText(object.toString());
                    return 3;
                }catch (Exception eee) {
                    return 0;
                }

            }
        }

    }
}
