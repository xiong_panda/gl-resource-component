package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChinesePatentPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface GlChinesePatentMapper {
    //核心资源库
    int batchInsertToCore(@Param("list") List<ChinesePatentPO> list,@Param("dataBase") String dataBase);

}
