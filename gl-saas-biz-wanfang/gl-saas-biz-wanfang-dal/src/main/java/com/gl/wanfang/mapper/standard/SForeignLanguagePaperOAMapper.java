package com.gl.wanfang.mapper.standard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ForeignLanguagePaperOAPO;

import java.util.List;

@Repository()
@Mapper
public interface SForeignLanguagePaperOAMapper {
    //核心资源库
    int FLPOAbatchInsertToCore(@Param("list") List<ForeignLanguagePaperOAPO> list);

    List<String> FLPOAselectField();
}
