package com.gl.wanfang.mapper.core;

import dto.DesignQueryDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import po.CollaborativeDesignPO;

import java.util.List;

@Repository
@Mapper
public interface CollaborativeDesignMapper {
    List<CollaborativeDesignPO> queryDesign(DesignQueryDTO dto);

    int updateDesign(CollaborativeDesignPO po);

    int insertDesign(CollaborativeDesignPO po);

}
