package com.gl.wanfang.mapper.standard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChinesePaperOAPO;

import java.util.List;

@Repository
@Mapper
public interface SChinesePaperOAMapper {
    //核心资源库
    int CPOAbatchInsertToCore(@Param("list") List<ChinesePaperOAPO> list);

    List<String> CPOAselectField();
}
