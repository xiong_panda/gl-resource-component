package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.OverseasPatentPO;

import java.util.List;

@Repository
@Mapper
public interface GlOverseasPatentMapper {
    //核心资源库
    int OPbatchInsertToCore(@Param("list") List<OverseasPatentPO> list,@Param("dataBase") String dataBase);

}
