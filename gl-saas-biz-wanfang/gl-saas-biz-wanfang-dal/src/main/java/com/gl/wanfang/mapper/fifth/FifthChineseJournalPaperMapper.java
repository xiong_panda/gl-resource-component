package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseJournalPapersPO;

import java.util.List;

@Repository
@Mapper
public interface FifthChineseJournalPaperMapper {
    //核心资源库
    int CJPbatchInsertToCore(@Param("list") List<ChineseJournalPapersPO> list);

    List<String> CJPselectField();
}
