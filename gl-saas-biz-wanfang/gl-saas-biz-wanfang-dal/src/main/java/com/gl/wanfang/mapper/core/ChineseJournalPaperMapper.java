package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.ChineseJournalPapersPO;

import java.util.List;

@Repository
@Mapper
public interface ChineseJournalPaperMapper {
    //核心资源库
    int CJPbatchInsertToCore(@Param("list") List<ChineseJournalPapersPO> list);

    List<String> CJPselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<ChineseJournalPapersPO> CJPSelectNewData(@Param("CreateTime") String CreateTime);
}
