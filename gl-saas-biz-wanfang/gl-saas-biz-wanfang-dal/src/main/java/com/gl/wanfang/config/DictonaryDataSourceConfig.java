package com.gl.wanfang.config;


import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@MapperScan(
        basePackages = "com.gl.wanfang.mapper.dictionary",
        sqlSessionFactoryRef = "sqlSessionFactory2"
)
@EnableConfigurationProperties(JdbcSecondProperties.class)
@EnableTransactionManagement
public class DictonaryDataSourceConfig {

    private static Logger logger = LoggerFactory.getLogger(DictonaryDataSourceConfig.class);

    @Autowired
    private JdbcSecondProperties jdbcSecondProperties;

    /**
     * 创建主数据源
     *
     * @return
     * @throws SQLException
     */
    @Bean(value="dataSource2",initMethod = "init", destroyMethod = "close")
    public DataSource dataSource2() {
        logger.info("===>>>开始初始化Dictonary数据源:[dataSource]");
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(jdbcSecondProperties.getDriverClassName());
        druidDataSource.setUrl(jdbcSecondProperties.getUrl());
        druidDataSource.setUsername(jdbcSecondProperties.getUsername());
        druidDataSource.setPassword(jdbcSecondProperties.getPassword());
        druidDataSource.setInitialSize(jdbcSecondProperties.getInitialSize());
        druidDataSource.setMinIdle(jdbcSecondProperties.getMinIdle());
        druidDataSource.setMaxActive(jdbcSecondProperties.getMaxActive());
        druidDataSource.setMaxWait(jdbcSecondProperties.getMaxWait());
        druidDataSource.setTimeBetweenEvictionRunsMillis(jdbcSecondProperties.getTimeBetweenEvictionRunsMillis());
        druidDataSource.setMinEvictableIdleTimeMillis(jdbcSecondProperties.getMinEvictableIdleTimeMillis());
        druidDataSource.setValidationQuery(jdbcSecondProperties.getValidationQuery());
        druidDataSource.setTestWhileIdle(jdbcSecondProperties.isTestWhileIdle());
        druidDataSource.setTestOnBorrow(jdbcSecondProperties.isTestOnBorrow());
        druidDataSource.setTestOnReturn(jdbcSecondProperties.isTestOnReturn());
        druidDataSource.setPoolPreparedStatements(jdbcSecondProperties.isPoolPreparedStatements());
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(
                jdbcSecondProperties.getMaxPoolPreparedStatementPerConnectionSize());
        druidDataSource.setRemoveAbandoned(jdbcSecondProperties.isRemoveAbandoned());
        druidDataSource.setRemoveAbandonedTimeout(jdbcSecondProperties.getRemoveAbandonedTimeout());
        druidDataSource.setLogAbandoned(jdbcSecondProperties.isLogAbandoned());

        return druidDataSource;
    }

    /**
     * 事务管理器
     *
     * @return
     */
    @Bean(name = "transactionManager2")
    public PlatformTransactionManager transactionManager2() {
        return new DataSourceTransactionManager(dataSource2());
    }



    /**
     * 配置sqlSessionFactory 在mybatis的配置文件中配置分页拦截器
     *
     * @return
     * @throws Exception
     */
    @Bean(name = "sqlSessionFactory2")
    public SqlSessionFactory sqlSessionFactoryBean2() {
        logger.info("===>>>开始初始化Dictonary数据源mybatis的SqlSessionFactory");

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource2());
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setConfigLocation(resolver.getResource("classpath:config/mybatis/mybatis-config.xml"));
        try {
            sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:**/mapper/dictionary/*.xml"));
            return sqlSessionFactoryBean.getObject();
        } catch (Exception e) {
            logger.error("===>>>初始化Dictonary数据源mybatis的sqlSessionFactory失败:" + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * 配置SqlSessionTemplate
     *
     * @return
     */
    @Bean(name = "sqlSessionTemplate2")
    public SqlSessionTemplate sqlSessionTemplate2() {
        return new SqlSessionTemplate(sqlSessionFactoryBean2());
    }
}
