package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.TechnologyAchievementsPO;

import java.util.List;

@Repository
@Mapper
public interface FifthTechnologyAchievementsMapper {
    //核心资源库
    int TAbatchInsertToCore(@Param("list") List<TechnologyAchievementsPO> list);

    List<String> TAselectField();
}
