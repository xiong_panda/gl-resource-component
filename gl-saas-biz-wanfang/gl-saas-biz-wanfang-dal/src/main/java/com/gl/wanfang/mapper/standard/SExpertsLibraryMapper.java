package com.gl.wanfang.mapper.standard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ExpertsLibraryPO;

import java.util.List;

@Repository
@Mapper
public interface SExpertsLibraryMapper {
    //核心资源库
    int ELPbatchInsertToCore(@Param("list") List<ExpertsLibraryPO> list);

    List<String> ELselectField();
}
