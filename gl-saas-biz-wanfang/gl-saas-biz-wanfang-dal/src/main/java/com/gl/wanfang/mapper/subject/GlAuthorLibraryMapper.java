package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.AuthorLibraryPO;

import java.util.List;

@Repository
@Mapper
public interface GlAuthorLibraryMapper {
    //核心资源库
    int ALbatchInsertToCore(@Param("list") List<AuthorLibraryPO> list,@Param("dataBase")String dataBase);

}
