package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ForeignPeriodicalPaperPO;

import java.util.List;

@Repository
@Mapper
public interface FifthForeignPeriodicalPaperMapper {
    //核心资源库
    int FPPbatchInsertToCore(@Param("list") List<ForeignPeriodicalPaperPO> list);

    List<String> FPPselectField();
}
