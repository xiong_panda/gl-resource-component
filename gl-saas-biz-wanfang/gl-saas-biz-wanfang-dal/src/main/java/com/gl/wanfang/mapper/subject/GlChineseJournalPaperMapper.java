package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseJournalPapersPO;

import java.util.List;

@Repository
@Mapper
public interface GlChineseJournalPaperMapper {
    //核心资源库
    int CJPbatchInsertToCore(@Param("list") List<ChineseJournalPapersPO> list,@Param("dataBase") String dataBase);

}
