package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.ChinesePaperOAPO;

import java.util.List;

@Repository
@Mapper
public interface ChinesePaperOAMapper {
    //核心资源库
    int CPOAbatchInsertToCore(@Param("list") List<ChinesePaperOAPO> list);

    List<String> CPOAselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<ChinesePaperOAPO> CPOSelectNewData(@Param("CreateTime") String CreateTime);
}
