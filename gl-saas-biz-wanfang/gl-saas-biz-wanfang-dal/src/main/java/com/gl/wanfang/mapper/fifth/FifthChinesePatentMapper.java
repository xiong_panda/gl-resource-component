package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChinesePatentPO;

import java.util.List;

@Repository
@Mapper
public interface FifthChinesePatentMapper {
    //核心资源库
    int batchInsertToCore(@Param("list") List<ChinesePatentPO> list);

    List<String> CPselectField();
}
