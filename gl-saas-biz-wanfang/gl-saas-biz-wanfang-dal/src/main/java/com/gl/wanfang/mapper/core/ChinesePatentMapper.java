package com.gl.wanfang.mapper.core;

import dto.SearchDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import outdto.WChinesePatentOUTDTO;
import po.ChineseConferencePaperPO;
import po.ChinesePatentPO;

import java.util.List;

@Repository
@Mapper
public interface ChinesePatentMapper {
    //核心资源库
    int batchInsertToCore(@Param("list") List<ChinesePatentPO> list);

    List<String> CPselectField();

    List<String> queryIds(@Param("ids") List<String> ids);


    List<WChinesePatentOUTDTO> query(SearchDTO dto);

    List<ChinesePatentPO> CPSelectNewData(@Param("CreateTime") String CreateTime);
}
