package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.WashConfigPO;

import java.util.Map;

@Repository
@Mapper
public interface WashConfigMapper {
    Map<String,Object> getWashConfig();

    int update(WashConfigPO po);
}

