package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import po.PageInfoPO;

@Repository
@Mapper
public interface PageInfoMapper {
    //核心资源库
    Integer SelectPageNum(String indexname);

    void batchUpdate(PageInfoPO pageInfoPO);

    void insertData(PageInfoPO pageInfoPO);

}
