package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface GlFieldContrastMapper {
    //核心资源库
    String SelectFiledName(@Param("coreField") String coreField, @Param("indexName") String indexName);

}
