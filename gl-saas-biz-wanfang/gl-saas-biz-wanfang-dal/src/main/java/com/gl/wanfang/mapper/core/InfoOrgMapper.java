package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.InfoOrgPO;

import java.util.List;

@Repository
@Mapper
public
interface InfoOrgMapper {
    //核心资源库
    int IObatchInsertToCore(@Param("list") List<InfoOrgPO> list);

    List<String> IOselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<InfoOrgPO> IOSelectNewData(@Param("CreateTime") String CreateTime);
}
