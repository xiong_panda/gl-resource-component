package com.gl.wanfang.mapper.dictionary;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import po.MarkDataPO;

import java.util.List;

@Repository
@Mapper
public interface MarkDataMapper {
    int batchInsertToCore(List<MarkDataPO> list);

    List<String> selectField();
}
