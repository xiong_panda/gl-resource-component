package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.AuthorLibraryPO;

import java.util.Date;
import java.util.List;

@Repository
@Mapper
public interface AuthorLibraryMapper {
    //核心资源库
    int ALbatchInsertToCore(@Param("list") List<AuthorLibraryPO> list);

    List<String> ALselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<AuthorLibraryPO> ALSelectNewData(@Param("CreateTime") String CreateTime);


}
