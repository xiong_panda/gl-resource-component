package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.TechnologyAchievementsPO;

import java.util.List;

@Repository
@Mapper
public interface TechnologyAchievementsMapper {
    //核心资源库
    int TAbatchInsertToCore(@Param("list") List<TechnologyAchievementsPO> list);

    List<String> TAselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<TechnologyAchievementsPO> TASelectNewData(@Param("CreateTime") String CreateTime);
}
