package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChinesePaperOAPO;

import java.util.List;

@Repository
@Mapper
public interface FifthChinesePaperOAMapper {
    //核心资源库
    int CPOAbatchInsertToCore(@Param("list") List<ChinesePaperOAPO> list);

    List<String> CPOAselectField();
}
