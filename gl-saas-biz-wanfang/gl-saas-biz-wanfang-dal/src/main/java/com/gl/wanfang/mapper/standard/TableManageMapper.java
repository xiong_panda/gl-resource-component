package com.gl.wanfang.mapper.standard;

import dto.QueryStandardDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.TableDetailPO;
import po.TablePO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface TableManageMapper {

    int insertTableInfo(@Param("po") TablePO po);

    int batchInsertTableDetail(@Param("list") List<TableDetailPO> list);

    void createTable(@Param("list") List<TableDetailPO> list,
                     @Param("tableName") String tableName,
                     @Param("tableComment")String tableComment);

    void deleteTable(@Param("tableName") String tableName);

    int batchInsert(@Param("tableName")String tableName,
                    @Param("newFieldList")List<String> newFieldList,
                    @Param(("list"))List<String> list);

    List<Map<String, Object>> queryOriginalTables();

    List<Map<String, Object>> queryOriginalField(@Param("tableName")String tableName);

    List<Map<String, Object>> queryStandardTables(QueryStandardDTO dto);

    List<Map<String, Object>> queryStandardField(@Param("tableId")String tableId);

    int verify(@Param("tableName")String tableName);

    int deleteInfo(@Param("tableId")String tableId);

    int deleteDetail(@Param("tableId")String tableId);

    Map<String,Object> getById(@Param("id")String id);

    Map<String,Object> getDDL(@Param("tableName")String tableName);

    void createTableFromDDL(@Param("ddl") String ddl);

    List<Map<String,Object>> queryOldDataList(@Param("tableName")String tableName);
}
