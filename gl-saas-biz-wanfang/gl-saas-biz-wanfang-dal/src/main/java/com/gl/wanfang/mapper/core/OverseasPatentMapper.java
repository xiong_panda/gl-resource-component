package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.OverseasPatentPO;

import java.util.List;

@Repository
@Mapper
public interface OverseasPatentMapper {
    //核心资源库
    int OPbatchInsertToCore(@Param("list") List<OverseasPatentPO> list);

    List<String> OPselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<OverseasPatentPO> OPSelectNewData(@Param("CreateTime") String CreateTime);
}
