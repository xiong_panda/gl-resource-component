package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Repository;
import po.StandardConfigPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface StandardConfigMapper {
    List<StandardConfigPO> getStandardConfig(Map map);

    int update(StandardConfigPO po);

    int insert(StandardConfigPO po);
}

