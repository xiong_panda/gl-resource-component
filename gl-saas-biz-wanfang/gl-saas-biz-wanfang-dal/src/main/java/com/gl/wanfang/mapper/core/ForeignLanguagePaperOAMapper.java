package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.ForeignLanguagePaperOAPO;

import java.util.List;

@Repository("foreignLanguagePaperOAMapper")
@Mapper
public interface ForeignLanguagePaperOAMapper {
    //核心资源库
    int FLPOAbatchInsertToCore(@Param("list") List<ForeignLanguagePaperOAPO> list);

    List<String> FLPOAselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<ForeignLanguagePaperOAPO> FLPOASelectNewData(@Param("CreateTime") String CreateTime);
}
