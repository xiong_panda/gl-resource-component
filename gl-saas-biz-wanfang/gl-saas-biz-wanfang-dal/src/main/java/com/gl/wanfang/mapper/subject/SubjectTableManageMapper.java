package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.TableDetailPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface SubjectTableManageMapper {
    List<Map<String,Object>> selectDataRelation(
            @Param("originalFieldList")List<String> originalFieldList,
            @Param("notMainlist") List<TableDetailPO> notMainlist,
            @Param("mainTableName")String mainTableName
    );

    List<Map<String,Object>> selectDataNoRelation(
            @Param("originalFieldList")List<String> originalFieldList,
            @Param("mainTableName")String mainTableName
    );



}
