package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.HigherLearningUniversitiesPO;

import java.util.List;

@Repository
@Mapper
public interface HigherLearningUniversitiesMapper {
    //核心资源库
    int HLUbatchInsertToCore(@Param("list") List<HigherLearningUniversitiesPO> list);

    List<String> HLUselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<HigherLearningUniversitiesPO> HLUSelectNewData(@Param("CreateTime") String CreateTime);
}
