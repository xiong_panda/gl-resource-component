package com.gl.wanfang.mapper.standard;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.LawsRegulationsPO;

import java.util.List;

@Repository
@Mapper
public interface SLawsRegulationsMapper {
    //核心资源库
    int LRbatchInsertToCore(@Param("list") List<LawsRegulationsPO> list);

    List<String> LRselectField();
}
