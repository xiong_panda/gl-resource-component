package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ScientificOrgPO;

import java.util.List;

@Repository
@Mapper
public interface FifthScientificOrgMapper {
    //核心资源库
    int SObatchInsertToCore(@Param("list") List<ScientificOrgPO> list);

    List<String> SOselectField();
}
