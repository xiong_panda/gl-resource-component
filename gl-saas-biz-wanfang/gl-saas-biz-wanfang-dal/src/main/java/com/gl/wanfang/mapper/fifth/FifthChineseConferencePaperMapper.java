package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;

import java.util.List;

@Repository
@Mapper
public interface FifthChineseConferencePaperMapper {
    //核心资源库
    int CCPbatchInsertToCore(@Param("list") List<ChineseConferencePaperPO> list);

    List<String> CCPselectField();
}
