package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.EnterpriseProductDatabasePO;

import java.util.List;

@Repository
@Mapper
public interface FifthEnterpriseProductDatabaseMapper {
    //核心资源库
    int EPDbatchInsertToCore(@Param("list") List<EnterpriseProductDatabasePO> list);

    List<String> EPDselectField();
}
