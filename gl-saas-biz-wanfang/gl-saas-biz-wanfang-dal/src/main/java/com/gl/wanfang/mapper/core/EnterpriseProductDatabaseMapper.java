package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.EnterpriseProductDatabasePO;

import java.util.List;

@Repository
@Mapper
public interface EnterpriseProductDatabaseMapper {
    //核心资源库
    int EPDbatchInsertToCore(@Param("list") List<EnterpriseProductDatabasePO> list);

    List<String> EPDselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<EnterpriseProductDatabasePO> EPDSelectNewData(@Param("CreateTime") String CreateTime);
}
