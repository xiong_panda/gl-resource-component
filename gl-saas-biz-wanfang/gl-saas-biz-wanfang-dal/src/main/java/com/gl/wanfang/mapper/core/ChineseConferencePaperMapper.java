package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.AuthorLibraryPO;
import po.ChineseConferencePaperPO;

import java.util.List;

@Repository
@Mapper
public interface ChineseConferencePaperMapper {
    //核心资源库
    int CCPbatchInsertToCore(@Param("list") List<ChineseConferencePaperPO> list);

    List<String> CCPselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<ChineseConferencePaperPO> CCPSelectNewData(@Param("CreateTime") String CreateTime);
}
