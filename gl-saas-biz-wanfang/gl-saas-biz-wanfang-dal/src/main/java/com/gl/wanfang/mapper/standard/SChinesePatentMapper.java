package com.gl.wanfang.mapper.standard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChinesePatentPO;

import java.util.List;

@Repository
@Mapper
public interface SChinesePatentMapper {
    //核心资源库
    int batchInsertToCore(@Param("list") List<ChinesePatentPO> list);

}
