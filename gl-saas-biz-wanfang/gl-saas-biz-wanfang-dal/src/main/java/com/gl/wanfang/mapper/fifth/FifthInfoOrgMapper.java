package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.InfoOrgPO;

import java.util.List;

@Repository
@Mapper
public
interface FifthInfoOrgMapper {
    //核心资源库
    int IObatchInsertToCore(@Param("list") List<InfoOrgPO> list);

    List<String> IOselectField();
}
