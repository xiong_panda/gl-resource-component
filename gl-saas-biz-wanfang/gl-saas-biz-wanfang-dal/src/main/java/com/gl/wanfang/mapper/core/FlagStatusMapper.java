package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.FlagStatusConfigPO;

import java.util.Map;

@Repository
@Mapper
public interface FlagStatusMapper {
    Map<String,Object> getflagStatusConfig();

    int update(FlagStatusConfigPO po);
}

