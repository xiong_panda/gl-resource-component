package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ForeignLanguagePaperOAPO;

import java.util.List;

@Repository
@Mapper
public interface GlForeignLanguagePaperOAMapper {
    //核心资源库
    int FLPOAbatchInsertToCore(@Param("list") List<ForeignLanguagePaperOAPO> list,@Param("dataBase") String dataBase);

}
