package com.gl.wanfang.mapper.standard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.TechnologyAchievementsPO;

import java.util.List;

@Repository
@Mapper
public interface STechnologyAchievementsMapper {
    //核心资源库
    int TAbatchInsertToCore(@Param("list") List<TechnologyAchievementsPO> list);

    List<String> TAselectField();
}
