package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.TechnologyAchievementsPO;

import java.util.List;

@Repository
@Mapper
public interface GlTechnologyAchievementsMapper {
    //核心资源库
    int TAbatchInsertToCore(@Param("list") List<TechnologyAchievementsPO> list,@Param("dataBase") String dataBase);

}
