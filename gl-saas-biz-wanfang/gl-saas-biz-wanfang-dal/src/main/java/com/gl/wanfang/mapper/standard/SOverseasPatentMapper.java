package com.gl.wanfang.mapper.standard;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.OverseasPatentPO;

import java.util.List;

@Repository
@Mapper
public interface SOverseasPatentMapper {
    //核心资源库
    int OPbatchInsertToCore(@Param("list") List<OverseasPatentPO> list);

    List<String> OPselectField();
}
