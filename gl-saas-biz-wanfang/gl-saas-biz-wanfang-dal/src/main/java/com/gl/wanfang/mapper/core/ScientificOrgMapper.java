package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.ScientificOrgPO;

import java.util.List;

@Repository
@Mapper
public interface ScientificOrgMapper {
    //核心资源库
    int SObatchInsertToCore(@Param("list") List<ScientificOrgPO> list);

    List<String> SOselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<ScientificOrgPO> SOSelectNewData(@Param("CreateTime") String CreateTime);
}
