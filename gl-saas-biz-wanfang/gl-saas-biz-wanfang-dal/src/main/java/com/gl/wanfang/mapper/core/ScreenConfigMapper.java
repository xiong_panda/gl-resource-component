package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ScreenConfigPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ScreenConfigMapper {
    List<Map<String,Object>> getScreenConfig();

    int update(@Param("po") ScreenConfigPO po);

    int insert(@Param("po") ScreenConfigPO po);
}

