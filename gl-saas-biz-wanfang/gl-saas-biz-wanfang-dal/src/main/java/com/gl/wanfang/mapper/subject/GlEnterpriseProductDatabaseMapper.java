package com.gl.wanfang.mapper.subject;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.EnterpriseProductDatabasePO;

import java.util.List;

@Repository
@Mapper
public interface GlEnterpriseProductDatabaseMapper {
    //核心资源库
    int EPDbatchInsertToCore(@Param("list") List<EnterpriseProductDatabasePO> list,@Param("dataBase") String dataBase);

}
