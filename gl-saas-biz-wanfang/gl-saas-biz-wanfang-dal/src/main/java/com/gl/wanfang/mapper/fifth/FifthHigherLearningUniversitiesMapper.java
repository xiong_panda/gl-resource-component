package com.gl.wanfang.mapper.fifth;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.HigherLearningUniversitiesPO;

import java.util.List;

@Repository
@Mapper
public interface FifthHigherLearningUniversitiesMapper {
    //核心资源库
    int HLUbatchInsertToCore(@Param("list") List<HigherLearningUniversitiesPO> list);

    List<String> HLUselectField();
}
