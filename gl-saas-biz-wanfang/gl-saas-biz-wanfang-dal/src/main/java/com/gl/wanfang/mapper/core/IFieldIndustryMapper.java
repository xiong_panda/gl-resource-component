package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface IFieldIndustryMapper {

    List<Map<String,Object>> getFieldIndustry();
}
