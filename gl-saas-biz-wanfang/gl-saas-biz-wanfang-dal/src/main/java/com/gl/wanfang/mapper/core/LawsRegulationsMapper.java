package com.gl.wanfang.mapper.core;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.LawsRegulationsPO;

import java.util.List;

@Repository
@Mapper
public interface LawsRegulationsMapper {
    //核心资源库
    int LRbatchInsertToCore(@Param("list") List<LawsRegulationsPO> list);

    List<String> LRselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<LawsRegulationsPO> LRSelectNewData(@Param("CreateTime") String CreateTime);
}
