package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.IntegrateConfigPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface IntegrateConfigMapper {
    List<Map<String,Object>> getIntegrateConfig();

    int update(@Param("po") IntegrateConfigPO po);

    int insert(@Param("po") IntegrateConfigPO po);
}

