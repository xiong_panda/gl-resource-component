package com.gl.wanfang.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import po.ChineseConferencePaperPO;
import po.ForeignPeriodicalPaperPO;

import java.util.List;

@Repository
@Mapper
public interface ForeignPeriodicalPaperMapper {
    //核心资源库
    int FPPbatchInsertToCore(@Param("list") List<ForeignPeriodicalPaperPO> list);

    List<String> FPPselectField();

    List<String> queryIds(@Param("ids") List<String> ids);

    List<ForeignPeriodicalPaperPO> FPPSelectNewData(@Param("CreateTime") String CreateTime);
}
