package com.gl.dongfang.controller;


import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.log.LogUtil;
import com.gl.basis.common.pojo.Page;
import com.gl.dongfang.dto.DFParamDTO;
import com.gl.dongfang.po.DongfangChinesePatentPO;
import com.gl.dongfang.service.DongfangChinesePatentService;
import com.gl.dongfang.service.IDongfangService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "东方",tags = {"东方灵盾专利搜索"})
public class DongfangController {

    @Autowired
    private IDongfangService dongfangService;
    @Autowired
    private DongfangChinesePatentService dongfangChinesePatentService;

    @ApiOperation(value = "用户登录")
    @PostMapping("/userLogin")
    @ResponseBody
    public String UserLogin(HttpServletResponse response, HttpServletRequest request,String name,String pwd) {
        LogUtil.info("用户登录","进入登录流程...");
        String data=dongfangService.userLogin( response, request,name,pwd);
        return data;
    }
    @ApiOperation(value = "匹配列表检索")
    @PostMapping("/apiListSearch")
    @ResponseBody
    public List<DongfangChinesePatentPO>  ApiListSearch(@RequestBody DFParamDTO dfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        dongfangService.userLogin( response,  request,"liaoweizhi","a111111");
        String data=dongfangService.apiListSearch(dfParamDTO,request,response);
        dongfangChinesePatentService.flagStatusComponent(data);
        List<DongfangChinesePatentPO> dongfangChinesePatentPOS = dongfangChinesePatentService.flagStatusComponent(data);
        return dongfangChinesePatentPOS;
    }
    @ApiOperation(value = "匹配列表检索 数据定时执行")
    @PostMapping("/apiListDataSearch")
    @ResponseBody
    public List<DongfangChinesePatentPO> ApiListDataSearch(@RequestBody DFParamDTO dfParamDTO,HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        dongfangService.userLogin( response,  request,"liaoweizhi","a111111");
        String data=dongfangService.apiListDataSearch(dfParamDTO,request,response);
        List<DongfangChinesePatentPO> dongfangChinesePatentPOS = dongfangChinesePatentService.flagStatusComponent(data);
        return dongfangChinesePatentPOS;
    }

    @ApiOperation(value = "匹配列表检索 feign调用")
    @PostMapping("/apiListDataSearchgn")
    @ResponseBody
    public Page<DongfangChinesePatentPO> apiListDataSearchgn(@RequestBody DFParamDTO dfParamDTO, HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
//        request.setAttribute("q",keyword==null?"TI":"TI="+keyword);
        dongfangService.userLogin( response,  request,"liaoweizhi","a111111");
        String data=dongfangService.apiListDataSearch(dfParamDTO,request,response);
        return getData(dfParamDTO,response,request,data);
    }

    private Page<DongfangChinesePatentPO> getData( DFParamDTO dfParamDTO,HttpServletResponse response, HttpServletRequest request, String data) throws IllegalAccessException, DocumentException {
        Map<String, Object> resultMap = JSONObject.parseObject(data, Map.class);
        String statusCode = resultMap.get("status").toString();

        //查询到的数据条数
        if (data.substring(0, 4).equals("null")) {
            data = data.substring(4, data.length());
        }

        Map<String, Object> resultMap2 = JSONObject.parseObject(data, Map.class);
        Map<String, Object> dataMap = JSONObject.parseObject(resultMap2.get("mesg").toString(), Map.class);
        String foundnum = (String) dataMap.get("FOUNDNUM");



        //封装page
        Page<DongfangChinesePatentPO> page = new Page<>();
        String currentPage=dfParamDTO.getPn();
        if ("1".equals(statusCode)) {
            List<DongfangChinesePatentPO> authorLibraryPOList = dongfangChinesePatentService.flagStatusComponent(data);
            page.setList(authorLibraryPOList);
            page.setCode("200");
            page.setPageNo(Integer.valueOf(currentPage));
            page.setTotalSize(Long.valueOf(foundnum));

        } else {
            page.setCode("100");
            page.setMsg("操作失败");
            page.setSuccess(false);
        }
        return page;
    }

    @ApiOperation(value = "详情检索")
    @PostMapping("/apiDocinfoSeach")
    @ResponseBody
    public String ApiDocinfoSeach(@RequestBody DFParamDTO dfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        LogUtil.info("详情检索","开始详情检索---------------------->");
        dongfangService.userLogin( response,request,"liaoweizhi","a111111");
        String data=dongfangService.apiDocinfoSeach(dfParamDTO,request,response);
        return data;
    }

    @ApiOperation(value = "全文下载")
    @PostMapping("/apiGetfileSeach")
    @ResponseBody
    public void ApiGetfileSeach(@RequestBody DFParamDTO dfParamDTO,HttpServletResponse response, HttpServletRequest request) {
        LogUtil.info("全文下载","开始全文下载---------------------->");
        dongfangService.userLogin( response,  request,"liaoweizhi","a111111");
        dongfangService.downFile(dfParamDTO,request,response);
    }
}
