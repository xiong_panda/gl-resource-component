package com.gl.dongfang.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.util.RedisUtil;
import com.gl.dongfang.dto.DFParamDTO;
import com.gl.dongfang.mapper.core.PageInfoMapper;
import com.gl.dongfang.po.PageInfoPO;
import com.gl.dongfang.service.IDongfangService;
import com.gl.dongfang.util.HttpClientService;
import com.gl.dongfang.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 东方灵盾接口访问服务
 */
@Service
public class DongfangServiceimpl  implements IDongfangService {


    @Autowired
    RedisUtil redisUtil;
    @Autowired
    PageInfoMapper pageInfoMapper ;

    private static final IdWorker idWorker = new IdWorker();

    @Override
    public String userLogin(HttpServletResponse response, HttpServletRequest request,String name, String pwd) {

        //1、读取初始URL
        String urlNameString = "userLogin";
        StringBuffer sb = new StringBuffer();
        sb.append(pwd==null?"":"&pwd="+pwd).append(name==null?"":"&name="+name).replace(0,1,"?");

        String data = requestData(response,request,sb.toString(),urlNameString,null);


        return data;
    }

    @Override
    public String apiListSearch(DFParamDTO dto,HttpServletRequest request, HttpServletResponse response) {
        String un = dto.getUn();//
        String dp  = dto.getDp();//索引编码，逗号分隔
        String pn  = dto.getPn();// 返回结果排除字段，逗号分隔
        String fl  = dto.getFl();
        String pkey  =dto.getPkey();
        String sortdown  = dto.getSortdown();
        String normalize  = dto.getNormalize();// 不存在的字段，逗号分隔
        String from  = dto.getFrom();// 不存在的字段，逗号分隔
//        String q = dto.getQ()==null?request.getAttribute("q").toString():dto.getQ();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String q = dto.getQ()==null?request.getAttribute("q").toString():dto.getQ();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        StringBuilder dm=new StringBuilder();
        dm.append(un==null?"liaoweizhi":un)
                .append(fl==null?"TI,PN":fl)
                .append(dp==null?"1":dp)
                .append(pn==null?"10":pn)
                .append(pkey==null?"4a2f5e1e":pkey);//""+un+fl+dp+pn+pkey;
        String secureid  = MD5Util.getMD5(MD5Util.getMD5(dm.toString()).toLowerCase()+MD5Util.getMD5(q==null?"TI":q).toLowerCase()).toLowerCase();
        //1、读取初始URL
        String urlNameString = "ApiSearch";
        StringBuffer sb = new StringBuffer();
        sb.append(un==null?"&un=liaoweizhi":"&un="+un)
                .append(dp==null?"&dp=1":"&dp="+dp)
                .append(pn==null?"&pn=10":"&pn="+pn)
                .append(fl==null?"&fl=TI,PN":"&fl="+fl)
                .append(pkey==null?"&pkey=4a2f5e1e":"&pkey="+pkey)// ""; //
                .append(sortdown==null?"":"&sortdown="+sortdown)
                .append("&secureid="+secureid)
                .append(normalize==null?"":"&normalize="+normalize)
                .append(from==null?"":"&from="+from)
                .append(q==null?"&q=TI":"&q="+q).replace(0,1,"?");
        //是否是调度平台调用接口
        String data= requestData(response,request,sb.toString(),urlNameString,null);
        return data;
    }

    @Override
    public String apiListDataSearch(DFParamDTO dto,HttpServletRequest request, HttpServletResponse response) {
        String un = dto.getUn();
        String dp  = dto.getDp();//索引编码，逗号分隔
        String pn  = dto.getPn();// 返回结果排除字段，逗号分隔
        String fl  = dto.getFl();
        String pkey  =dto.getPkey();
        String sortdown  = dto.getSortdown();
        String normalize  = dto.getNormalize();// 不存在的字段，逗号分隔
        String from  = dto.getFrom();// 不存在的字段，逗号分隔
        String q = dto.getQ()==null?request.getAttribute("q").toString():dto.getQ();//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        StringBuilder dm=new StringBuilder();

        PageInfoPO pip=new PageInfoPO();
        Integer page = pageInfoMapper.SelectPageNum(pkey==null?"4a2f5e1e":pkey);
        if(page==null){
            pip.setIndexname(pkey==null?"4a2f5e1e":pkey);
            pip.setSize(10);
            pip.setPage(1);
            pip.setCurrentData(0);
            pip.setCount(1051892);
            pip.setFrom(2);
            pageInfoMapper.insertData(pip);
        }
        page=(page==null?1:page);
        dm.append(un==null?"liaoweizhi":un)
                .append(fl==null?"DCK,LS,LSNE,TI,AN,AD,PN,PD,PA,GA,AU,ADDR,PC,PR,AGT,AGC,IPC,IPCR,MIPC,AB,ZYFT,QWFT":fl)
                .append(dp==null?page:dp)
                .append(pn==null?"10":pn)
                .append(pkey==null?"4a2f5e1e":pkey);
        //""+un+fl+dp+pn+pkey  加密;
        String secureid  = MD5Util.getMD5(MD5Util.getMD5(dm.toString()).toLowerCase()+MD5Util.getMD5(q==null?"TI":q).toLowerCase()).toLowerCase();
        //1、读取初始URL
        String urlNameString = "ApiSearch";
        StringBuffer sb = new StringBuffer();

        sb.append(un==null?"&un=liaoweizhi":"&un="+un).append(dp==null?"&dp="+page:"&dp="+dp)
                .append(pn==null?"&pn=10":"&pn="+pn)
                .append(fl==null?"&fl=DCK,LS,LSNE,TI,AN,AD,PN,PD,PA,GA,AU,ADDR,PC,PR,AGT,AGC,IPC,IPCR,MIPC,AB,ZYFT,QWFT":"&fl="+fl)
                .append(pkey==null?"&pkey=4a2f5e1e":"&pkey="+pkey)
                .append(sortdown==null?"":"&sortdown="+sortdown)
                .append("&secureid="+secureid)
                .append(normalize==null?"":"&normalize="+normalize)
                .append(from==null?"":"&from="+from)
                .append(q==null?"&q=TI":"&q="+q).replace(0,1,"?");

        //是否是调度平台调用接口
        String data= requestData(response,request,sb.toString(),urlNameString,null);
        //查询到的数据条数
        if (data.substring(0, 4).equals("null")) {
            data = data.substring(4, data.length());
        }

        Map<String, Object> resultMap = JSONObject.parseObject(data, Map.class);
        Map<String, Object> dataMap = JSONObject.parseObject(resultMap.get("mesg").toString(), Map.class);
        String foundnum = (String) dataMap.get("FOUNDNUM");

        pip=new PageInfoPO();
        pip.setIndexname(pkey==null?"4a2f5e1e":pkey);
        //更新执行的位置
        pip.setCurrentData((page)*10);//todo： 调用统计接口跟新count数据
        pip.setPage(++page);
        pip.setSize(10);
        pip.setCount(Integer.parseInt(foundnum));
        pageInfoMapper.batchUpdate(pip);
        return data;
    }

    @Override
    public String apiDocinfoSeach(DFParamDTO dto,HttpServletRequest request, HttpServletResponse response) {
//        un=liaoweizhi&sid=liaoweizhi&fk=PN,TI&pkey=4a2f5e1e&secureid=530184d6e284ae8f18d04313651b5255&dk=[{"DCK":"CN201220703026@CN203117865U@20130807","MID":"3"}]
        String un = dto.getUn();//
        String sid = dto.getSid();//
        String fk = dto.getFk();//
        String pkey  =  dto.getPkey();
        String dk  = dto.getDk();//索引编码，逗号分隔

        String dm=""+un+fk+pkey;
        String secureid  = MD5Util.getMD5(MD5Util.getMD5(dm).toLowerCase()+MD5Util.getMD5(dk).toLowerCase()).toLowerCase();
        //1、读取初始URL
        String urlNameString = "ApiDocinfo";
        StringBuffer sb = new StringBuffer();
        sb.append(un==null?"":"&un="+un)
                .append(sid==null?"":"&sid="+sid)
                .append(fk==null?"":"&fk="+fk)
                .append(pkey==null?"":"&pkey="+pkey)
                .append("&secureid="+secureid)
                .append(dk==null?"":"&dk="+dk)
                .replace(0,1,"?");
        String data = requestData(response,request,sb.toString(),urlNameString,null);
        return data;
    }

    @Override
    public void downFile(DFParamDTO dto,HttpServletRequest request, HttpServletResponse response) {
        String un = dto.getUn();//
        String pkey  =  dto.getPkey();
        String dk  = dto.getDk();//索引编码，逗号分隔
        String t  = dto.getT();//索引编码，逗号分隔
        //加密规则
        String dm=""+un+dk+pkey;
        String secureid  = MD5Util.getMD5(MD5Util.getMD5(dm).toLowerCase()+MD5Util.getMD5(dk).toLowerCase()).toLowerCase();
        //1、读取初始URL
        String urlNameString = "ApiGetfile";
        StringBuffer sb = new StringBuffer();
        sb.append(un==null?"":"&un="+un)
                .append(pkey==null?"":"&pkey="+pkey)
                .append("&secureid="+secureid)
                .append(dk==null?"":"&dk="+dk)
                .replace(0,1,"?");
        requestFile(response,request,sb.toString(),urlNameString,null);
    }

    //获取数据，发送请求  进入构件
    @Override
    public String requestData(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid){

        String data = null;
//        if(null!=redisUtil.get(param)){
//            data=redisUtil.get(param).toString();
//            Long count=String.valueOf(redisUtil.get("count"+param))=="null"?1:Long.valueOf(String.valueOf(redisUtil.get("count"+param)))+1;
//            redisUtil.set("count"+param,count,604800L);
//        }else {
            try {
                data = HttpClientService.sendGet(response,request,url + param, null);
                //进入构件 存取数据
                //将数据存入redis   todo ： 将标准化后的数据存入redis并返回
                redisUtil.set(param, data, 36000L);
                Long count=String.valueOf(redisUtil.get("count"+param))=="null"?1:Long.valueOf(String.valueOf(redisUtil.get("count"+param)))+1;
                redisUtil.set("count"+param,count,604800L);
            } catch (Exception e) {
                e.printStackTrace();
            }
//        }
        return data;
    }

    //获取数据，发送文件下载请求
    @Override
    public void requestFile(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid){
        try {
            HttpClientService.downLoadFromUrl(response,request,url + param, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
