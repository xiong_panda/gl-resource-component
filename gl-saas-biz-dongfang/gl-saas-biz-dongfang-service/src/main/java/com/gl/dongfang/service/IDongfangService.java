package com.gl.dongfang.service;

import com.gl.dongfang.dto.DFParamDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface IDongfangService {
     //用户登录
     String userLogin(HttpServletResponse response, HttpServletRequest request, String name, String pwd);
     //检索列表查询
     String apiListSearch(DFParamDTO dfParamDTO, HttpServletRequest request, HttpServletResponse response);
     //检索列表数据爬取
     String apiListDataSearch( DFParamDTO dfParamDTO,HttpServletRequest request, HttpServletResponse response);
     //检索详情
     String apiDocinfoSeach(DFParamDTO dfParamDTO, HttpServletRequest request, HttpServletResponse response);
     //pdf下载
     void downFile(DFParamDTO dfParamDTO, HttpServletRequest request, HttpServletResponse response);

     String requestData(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid);

     void requestFile(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid);
}
