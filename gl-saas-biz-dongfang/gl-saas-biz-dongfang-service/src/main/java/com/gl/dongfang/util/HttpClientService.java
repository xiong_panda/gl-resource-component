package com.gl.dongfang.util;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.log.LogUtil;
import com.gl.basis.common.util.StringUtils;
import com.gl.basis.util.RedisUtil;
import org.apache.axis.encoding.Base64;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * HttpClient发送GET、POST请求
 * @Author libin
 * @CreateDate 2018.5.28 16:56
 */
public class HttpClientService {

    /**
     * 返回成功状态码
     */
    private static final int SUCCESS_CODE = 200;
    @Autowired
    RedisUtil redisUtil;

    /**
     * 发送GET请求
     * @param url   请求url
     * @return JSON或者字符串
     * @throws Exception
     */
    public static String sendGet(HttpServletResponse response, HttpServletRequest request,String url, String token) throws Exception{
        String urls="http://www.lindenpat.com/search/pub/"+url;

        JSONObject jsonObject = null;
        //当传入的url返回不为空的时候，读取数据
        BufferedReader in = null;
        String data = null;//提高字符数据的生成
        try{
            URL urlInfo = new URL(urls);
            URLConnection connection = urlInfo.openConnection();
            connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
//            connection.addRequestProperty("Content-Type", "application/pdf;charset=UTF-8");
            connection.addRequestProperty("Host",urlInfo.getHost());//设置头
            connection.addRequestProperty("Connection", "keep-alive");
            connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64)");
            connection.addRequestProperty("Accept", "text/html;charset=utf-8");
            connection.addRequestProperty("Cookie", getSessionIncookie(request));
//            "PHPSESSID=naifi8eod712ihoq1tj5eef4s0; kmsign=5e8c29af247be; kongming_name=liaoweizhi; kongming_uid=14224; kongming_sigcode=9da87910c09c1f5aa69f75a11e950495; KMUID=wKgCAl6MKVZ0HRxCBc5WAg=="
            // 获取所有响应头字段
            getCookieToSession(request,connection);
            //获取请求回来的信息
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段--打印信息
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                data += line;
            }
            return data;
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                in.close();
            }catch(Exception e){}
        }
        return null;
    }

    /**
     * 发送POST请求
     * @param url
     * @param nameValuePairList
     * @return JSON或者字符串
     * @throws Exception
     */
    public static Object sendPost(String url, List<NameValuePair> nameValuePairList) throws Exception{
        String urls="http://www.lindenpat.com/search/pub/"+url;

        JSONObject jsonObject = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try{
            /**
             *  创建一个httpclient对象
             */
            client = HttpClients.createDefault();
            /**
             * 创建一个post对象
             */
            HttpPost post = new HttpPost(urls);
            /**
             * 包装成一个Entity对象
             */
            StringEntity entity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
            /**
             * 设置请求的内容
             */
            post.setEntity(entity);
            /**
             * 设置请求的报文头部的编码
             */
            post.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
            /**
             * 设置请求的报文头部的编码
             */
            post.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
            /**
             * 执行post请求
             */
            response = client.execute(post);
            /**
             * 获取响应码
             */
            int statusCode = response.getStatusLine().getStatusCode();
            if (SUCCESS_CODE == statusCode){
                /**
                 * 通过EntityUitls获取返回内容
                 */
                String result = EntityUtils.toString(response.getEntity(),"UTF-8");
                /**
                 * 转换成json,根据合法性返回json或者字符串
                 */
                try{
                    jsonObject = JSONObject.parseObject(result);
                    return jsonObject;
                }catch (Exception e){
                    return result;
                }
            }else{
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            response.close();
            client.close();
        }
        return null;
    }
    /**
     * 发送GET请求获取经纬度
     * @param url   请求url
     * @return JSON或者字符串
     * @throws Exception
     */
    public static String sendbaiduMapGet(HttpServletResponse response, HttpServletRequest request,String url) throws Exception{
        String urls="http://api.map.baidu.com/geocoder"+url;

        JSONObject jsonObject = null;
        //当传入的url返回不为空的时候，读取数据
        BufferedReader in = null;
        String data = null;//提高字符数据的生成
        try{
            URL urlInfo = new URL(urls);
            URLConnection connection = urlInfo.openConnection();
            connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
//            connection.addRequestProperty("Content-Type", "application/pdf;charset=UTF-8");
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段--打印信息
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                data += line;
            }
            return data;
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                in.close();
            }catch(Exception e){}
        }
        return null;
    }

    /**
     * 从Url中下载文件
     *
     * @param request
     * @param response
     * @param url
     * @throws IOException
     */

    public static void downLoadFromUrl(HttpServletResponse response, HttpServletRequest request,String url, String token) throws IOException {
        String urls="http://www.lindenpat.com/search/pub/"+url;

        JSONObject jsonObject = null;
        //当传入的url返回不为空的时候，读取数据
        InputStream input=null;
        byte[] data = null;//提高字符数据的生成
        try{
            URL urlInfo = new URL(urls);
            URLConnection connection = urlInfo.openConnection();
            connection.addRequestProperty("Content-Type", "application/pdf; charset=utf-8");
//            connection.addRequestProperty("Content-Type", "application/pdf;charset=UTF-8");
            connection.addRequestProperty("Host",urlInfo.getHost());//设置头
            connection.addRequestProperty("Connection", "keep-alive");
            connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64)");
            connection.addRequestProperty("Accept", "text/html;charset=utf-8");
            connection.addRequestProperty("Cookie", getSessionIncookie(request));
//            "PHPSESSID=naifi8eod712ihoq1tj5eef4s0; kmsign=5e8c29af247be; kongming_name=liaoweizhi; kongming_uid=14224; kongming_sigcode=9da87910c09c1f5aa69f75a11e950495; KMUID=wKgCAl6MKVZ0HRxCBc5WAg=="
            // 获取所有响应头字段
            getCookieToSession(request,connection);
            //获取请求回来的信息
            //得到输入流
            InputStream inputStream = connection.getInputStream();
            //获取自己数组
            response.setContentType("application/pdf;charset=UTF-8");

            byte[] getData = readInputStream(inputStream);


            BufferedOutputStream Boutput = null;
            BufferedInputStream Binput = null;
            try {
                Boutput = new BufferedOutputStream(response.getOutputStream());
                // 中文文件名必须转码为 ISO8859-1,否则为乱码
                String fileNameDown = new String("fileName.pdf".getBytes(), "ISO8859-1");
                // 作为附件下载
                response.setHeader("Content-Disposition", "attachment;filename="
                        + fileNameDown);
                Boutput.write(getData);
                response.flushBuffer();
            } catch (Exception e) {
                 LogUtil.error("Download log file error",e.getMessage());
            } // 用户可能取消了下载
            finally {
                if (Binput != null)
                    try {
                        Binput.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                if (Boutput != null)
                    try {
                        Boutput.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                input.close();
            }catch(Exception e){}
        }
    }



    /**
     * 从输入流中获取字节数组
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static  byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

    /**
     * 获取在session中保存的cookie
     * @param request
     * @return
     */
    private static String getSessionIncookie(HttpServletRequest request) {
        StringBuilder sb=new StringBuilder();
        HttpSession session = null;
        try {
            session = request.getSession();
            String  KMUID=(String) session.getAttribute("KMUID");
            String  kmsign=(String) session.getAttribute("kmsign");
            String  kongming_name=(String) session.getAttribute("kongming_name");
            String  kongming_uid=(String) session.getAttribute("kongming_uid");
            String  kongming_sigcode=(String) session.getAttribute("kongming_sigcode");
            String  PHPSESSID=(String) session.getAttribute("PHPSESSID");



            if(KMUID!=null){
                sb.append(KMUID);
            }
            if(kongming_sigcode!=null){
                sb.append(kongming_sigcode);
            }

            if(kongming_uid!=null){
                sb.append(kongming_uid);
            }
            if(kongming_name!=null){
                sb.append(kongming_name);
            }

            if(kmsign!=null){
                sb.append(kmsign);
            }
            if(PHPSESSID!=null){
                if(!StringUtils.isEmpty(sb.toString())){
                    sb.append(" ");
                }
                sb.append(PHPSESSID);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;


    }

    /**
     * 将请求的头信息获取到session中
     * @param request
     * @param collection
     */
    private static void getCookieToSession(HttpServletRequest request,
                                           URLConnection  collection) {

        Map<String, List<String>> map = collection .getHeaderFields();
        HttpSession session = request.getSession();

        List<String> cookie=map.get("Set-Cookie");
        if(cookie!=null){

            String  KMUID= getCookieBySet("KMUID",cookie.get(0));
            String  kongming_sigcode =getCookieBySet("kongming_sigcode",cookie.get(1));
            String  kongming_uid =getCookieBySet("kongming_uid",cookie.get(2));
            String  kongming_name =getCookieBySet("kongming_name",cookie.get(3));
            String  kmsign= getCookieBySet("kmsign",cookie.get(4));
            String  PHPSESSID= getCookieBySet("PHPSESSID",cookie.get(5));

            if(KMUID!=null){
                session.setAttribute("KMUID",KMUID);
            }
            if(kmsign!=null){
                session.setAttribute("kongming_sigcode",kmsign);
            }
            if(kongming_name!=null){
                session.setAttribute("kongming_uid",kongming_name);
            }
            if(kongming_uid!=null){
                session.setAttribute("kongming_name",kongming_uid);
            }
            if(kongming_sigcode!=null){
                session.setAttribute("kmsign",kongming_sigcode);
            }
            if(PHPSESSID!=null){
                session.setAttribute("PHPSESSID",PHPSESSID);
            }
        }
    }

    /**
     * 获取verCode=b5pcogZaFGikpAc1mQ+G5wOJGBWtXLsHafpf5wlgF5s=; Path=/SSODAO/; HttpOnly
     * 的cookie信息
     *
     */
    public static String getCookieBySet(String name,String set){

        String regex=name+"=(.*?);";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher =pattern.matcher(set);
        if(matcher.find()){
            return matcher.group();
        }
        return null;

    }

}