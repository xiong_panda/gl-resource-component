package com.gl.dongfang.po;


//核心资源数据 中文期刊
public class TerraceBusinessPO {

    private String  f_pkid;
    private String  f_provided;
    private String  f_under;
    private String  f_name;

    public String getF_pkid() {
        return f_pkid;
    }

    public void setF_pkid(String f_pkid) {
        this.f_pkid = f_pkid;
    }

    public String getF_provided() {
        return f_provided;
    }

    public void setF_provided(String f_provided) {
        this.f_provided = f_provided;
    }

    public String getF_under() {
        return f_under;
    }

    public void setF_under(String f_under) {
        this.f_under = f_under;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }
}
