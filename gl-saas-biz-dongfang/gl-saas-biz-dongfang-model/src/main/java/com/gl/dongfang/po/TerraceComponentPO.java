package com.gl.dongfang.po;


//核心资源数据 中文期刊
public class TerraceComponentPO {

    private String  f_pkid;
    private Integer  f_provided;
    private Integer  f_under;
    private Integer  f_specialization;
    private String  f_name;

    public String getF_pkid() {
        return f_pkid;
    }

    public void setF_pkid(String f_pkid) {
        this.f_pkid = f_pkid;
    }

    public Integer getF_provided() {
        return f_provided;
    }

    public void setF_provided(Integer f_provided) {
        this.f_provided = f_provided;
    }

    public Integer getF_under() {
        return f_under;
    }

    public void setF_under(Integer f_under) {
        this.f_under = f_under;
    }

    public Integer getF_specialization() {
        return f_specialization;
    }

    public void setF_specialization(Integer f_specialization) {
        this.f_specialization = f_specialization;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }
}
