package com.gl.dongfang.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("DFParamDTO")
public class DFParamDTO {

    @ApiModelProperty("用户id")
    private String un;
    @ApiModelProperty("dp")
    private String dp;
    @ApiModelProperty("页码")
    private String pn;
    @ApiModelProperty("fl")
    private String fl;
    @ApiModelProperty("加密-码值（密钥）")
    private String pkey="4a2f5e1e";
    @ApiModelProperty("排序规则")
    private String sortdown;
    @ApiModelProperty("normalize")
    private String normalize;
    @ApiModelProperty("from")
    private String from;
    @ApiModelProperty("检索条件")
    private String q;

    @ApiModelProperty("加密算法")
    private String sid;
    @ApiModelProperty("fk")
    private String fk;
    @ApiModelProperty("dk")
    private String dk;

    @ApiModelProperty("t")
    private String t;
    @ApiModelProperty("gn")
    private String gn;

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String getDp() {
        return dp==null?"10":dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getPn() {
        return pn==null?"1":pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public String getPkey() {
        return pkey;
    }

    public void setPkey(String pkey) {
        this.pkey = pkey;
    }

    public String getSortdown() {
        return sortdown;
    }

    public void setSortdown(String sortdown) {
        this.sortdown = sortdown;
    }

    public String getNormalize() {
        return normalize;
    }

    public void setNormalize(String normalize) {
        this.normalize = normalize;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getFk() {
        return fk;
    }

    public void setFk(String fk) {
        this.fk = fk;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getGn() {
        return gn;
    }

    public void setGn(String gn) {
        this.gn = gn;
    }
}
