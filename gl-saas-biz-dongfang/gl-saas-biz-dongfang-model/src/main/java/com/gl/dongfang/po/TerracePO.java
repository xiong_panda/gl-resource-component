package com.gl.dongfang.po;


import com.gl.dongfang.annotation.FieldOrder;

//核心资源数据 中文期刊
public class TerracePO {

    private String f_pkid;
    private String f_name;
    private Integer f_Number;
    private Integer f_sub_platform;
    private Double f_x;
    private Double f_y;
    private String f_component_id;
    private String f_business_id;
    private String f_patent_id;
    private String f_name_to;
    private String f_pk_id;

    public String getF_pkid() {
        return f_pkid;
    }

    public void setF_pkid(String f_pkid) {
        this.f_pkid = f_pkid;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public Integer getF_Number() {
        return f_Number;
    }

    public void setF_Number(Integer f_Number) {
        this.f_Number = f_Number;
    }

    public Integer getF_sub_platform() {
        return f_sub_platform;
    }

    public void setF_sub_platform(Integer f_sub_platform) {
        this.f_sub_platform = f_sub_platform;
    }

    public Double getF_x() {
        return f_x;
    }

    public void setF_x(Double f_x) {
        this.f_x = f_x;
    }

    public Double getF_y() {
        return f_y;
    }

    public void setF_y(Double f_y) {
        this.f_y = f_y;
    }

    public String getF_component_id() {
        return f_component_id;
    }

    public void setF_component_id(String f_component_id) {
        this.f_component_id = f_component_id;
    }

    public String getF_business_id() {
        return f_business_id;
    }

    public void setF_business_id(String f_business_id) {
        this.f_business_id = f_business_id;
    }

    public String getF_patent_id() {
        return f_patent_id;
    }

    public void setF_patent_id(String f_patent_id) {
        this.f_patent_id = f_patent_id;
    }

    public String getF_name_to() {
        return f_name_to;
    }

    public void setF_name_to(String f_name_to) {
        this.f_name_to = f_name_to;
    }

    public String getF_pk_id() {
        return f_pk_id;
    }

    public void setF_pk_id(String f_pk_id) {
        this.f_pk_id = f_pk_id;
    }
}
