package com.gl.dongfang.po;

import com.gl.dongfang.annotation.FieldOrder;

public class WDongfangChinesePatentPO {

@FieldOrder(order = 1)   private String ID;
@FieldOrder(order = 2)  private String score;
@FieldOrder(order = 3)  private String mid;
@FieldOrder(order = 4)  private String docid;
@FieldOrder(order = 5)  private String DCK;
@FieldOrder(order = 6)  private String LS;
@FieldOrder(order = 7)  private String LSNE;
@FieldOrder(order = 8)  private String TI;
@FieldOrder(order = 9)  private String AN;
@FieldOrder(order = 10)  private String AD;
@FieldOrder(order = 11)   private String PN;
@FieldOrder(order = 12)   private String PD;
@FieldOrder(order = 13)   private String PA;
@FieldOrder(order = 14)   private String GA;
@FieldOrder(order = 15)   private String AU;
@FieldOrder(order = 16)   private String ADDR;
@FieldOrder(order = 17)   private String PC;
@FieldOrder(order = 18)   private String PR;
@FieldOrder(order = 19)   private String AGT;
@FieldOrder(order = 20)   private String AGC;
@FieldOrder(order = 21)   private String IPC;
@FieldOrder(order = 22)   private String IPCR;
@FieldOrder(order = 23)   private String MIPC;
@FieldOrder(order = 24)   private String AB;
@FieldOrder(order = 25)   private String ZYFT;
@FieldOrder(order = 26)   private String QWFT;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getDCK() {
        return DCK;
    }

    public void setDCK(String DCK) {
        this.DCK = DCK;
    }

    public String getLS() {
        return LS;
    }

    public void setLS(String LS) {
        this.LS = LS;
    }

    public String getLSNE() {
        return LSNE;
    }

    public void setLSNE(String LSNE) {
        this.LSNE = LSNE;
    }

    public String getTI() {
        return TI;
    }

    public void setTI(String TI) {
        this.TI = TI;
    }

    public String getAN() {
        return AN;
    }

    public void setAN(String AN) {
        this.AN = AN;
    }

    public String getAD() {
        return AD;
    }

    public void setAD(String AD) {
        this.AD = AD;
    }

    public String getPN() {
        return PN;
    }

    public void setPN(String PN) {
        this.PN = PN;
    }

    public String getPD() {
        return PD;
    }

    public void setPD(String PD) {
        this.PD = PD;
    }

    public String getPA() {
        return PA;
    }

    public void setPA(String PA) {
        this.PA = PA;
    }

    public String getGA() {
        return GA;
    }

    public void setGA(String GA) {
        this.GA = GA;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getPC() {
        return PC;
    }

    public void setPC(String PC) {
        this.PC = PC;
    }

    public String getPR() {
        return PR;
    }

    public void setPR(String PR) {
        this.PR = PR;
    }

    public String getAGT() {
        return AGT;
    }

    public void setAGT(String AGT) {
        this.AGT = AGT;
    }

    public String getAGC() {
        return AGC;
    }

    public void setAGC(String AGC) {
        this.AGC = AGC;
    }

    public String getIPC() {
        return IPC;
    }

    public void setIPC(String IPC) {
        this.IPC = IPC;
    }

    public String getIPCR() {
        return IPCR;
    }

    public void setIPCR(String IPCR) {
        this.IPCR = IPCR;
    }

    public String getMIPC() {
        return MIPC;
    }

    public void setMIPC(String MIPC) {
        this.MIPC = MIPC;
    }

    public String getAB() {
        return AB;
    }

    public void setAB(String AB) {
        this.AB = AB;
    }

    public String getZYFT() {
        return ZYFT;
    }

    public void setZYFT(String ZYFT) {
        this.ZYFT = ZYFT;
    }

    public String getQWFT() {
        return QWFT;
    }

    public void setQWFT(String QWFT) {
        this.QWFT = QWFT;
    }
}
