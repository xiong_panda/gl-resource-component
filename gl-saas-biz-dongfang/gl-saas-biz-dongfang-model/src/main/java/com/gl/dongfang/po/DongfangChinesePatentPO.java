package com.gl.dongfang.po;

import com.alibaba.fastjson.annotation.JSONField;
import com.gl.dongfang.annotation.FieldOrder;

import java.util.Date;

public class DongfangChinesePatentPO {

    @FieldOrder(order = 1) private String ID;
    @FieldOrder(order = 2) private String score ;
    @FieldOrder(order = 3) private String mid ;
    @FieldOrder(order = 4) private String docid ;
    @FieldOrder(order = 5) private String DCK ;
    @FieldOrder(order = 6) private String Ls ;
    @FieldOrder(order = 7) private String Law_Status ;
    @FieldOrder(order = 8) private String Title2 ;
    @FieldOrder(order = 9) private String Country_Code ;
    @FieldOrder(order = 10) private String Date2 ;
    @FieldOrder(order = 11) private String Publication_No ;
    @FieldOrder(order = 12) private String Publication_Date;
    @FieldOrder(order = 13) private String ORG;
    @FieldOrder(order = 14) private String GA;
    @FieldOrder(order = 15) private String Author;
    @FieldOrder(order = 16) private String Request_Address;
    @FieldOrder(order = 17) private String PC;
    @FieldOrder(order = 18) private String PR;
    @FieldOrder(order = 19) private String Agent;
    @FieldOrder(order = 20) private String ORG_Agency;
    @FieldOrder(order = 21) private String IPC;
    @FieldOrder(order = 22) private String IPCR ;
    @FieldOrder(order = 23) private String MIPC ;
    @FieldOrder(order = 24) private String Abstract ;
    @FieldOrder(order = 25) private String ZYFT ;
    @FieldOrder(order = 26) private String QWFT ;
    @FieldOrder(order =27)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }
    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getDCK() {
        return DCK;
    }

    public void setDCK(String DCK) {
        this.DCK = DCK;
    }

    public String getLs() {
        return Ls;
    }

    public void setLs(String ls) {
        Ls = ls;
    }

    public String getLaw_Status() {
        return Law_Status;
    }

    public void setLaw_Status(String law_Status) {
        Law_Status = law_Status;
    }

    public String getTitle2() {
        return Title2;
    }

    public void setTitle2(String title2) {
        Title2 = title2;
    }

    public String getCountry_Code() {
        return Country_Code;
    }

    public void setCountry_Code(String country_Code) {
        Country_Code = country_Code;
    }

    public String getDate() {
        return Date2;
    }

    public void setDate(String date) {
        Date2 = date;
    }

    public String getPublication_No() {
        return Publication_No;
    }

    public void setPublication_No(String publication_No) {
        Publication_No = publication_No;
    }

    public String getPublication_Date() {
        return Publication_Date;
    }

    public void setPublication_Date(String publication_Date) {
        Publication_Date = publication_Date;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getGA() {
        return GA;
    }

    public void setGA(String GA) {
        this.GA = GA;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getRequest_Address() {
        return Request_Address;
    }

    public void setRequest_Address(String request_Address) {
        Request_Address = request_Address;
    }

    public String getPC() {
        return PC;
    }

    public void setPC(String PC) {
        this.PC = PC;
    }

    public String getPR() {
        return PR;
    }

    public void setPR(String PR) {
        this.PR = PR;
    }

    public String getAgent() {
        return Agent;
    }

    public void setAgent(String agent) {
        Agent = agent;
    }

    public String getORG_Agency() {
        return ORG_Agency;
    }

    public void setORG_Agency(String ORG_Agency) {
        this.ORG_Agency = ORG_Agency;
    }

    public String getIPC() {
        return IPC;
    }

    public void setIPC(String IPC) {
        this.IPC = IPC;
    }

    public String getIPCR() {
        return IPCR;
    }

    public void setIPCR(String IPCR) {
        this.IPCR = IPCR;
    }

    public String getMIPC() {
        return MIPC;
    }

    public void setMIPC(String MIPC) {
        this.MIPC = MIPC;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getZYFT() {
        return ZYFT;
    }

    public void setZYFT(String ZYFT) {
        this.ZYFT = ZYFT;
    }

    public String getQWFT() {
        return QWFT;
    }

    public void setQWFT(String QWFT) {
        this.QWFT = QWFT;
    }
}
