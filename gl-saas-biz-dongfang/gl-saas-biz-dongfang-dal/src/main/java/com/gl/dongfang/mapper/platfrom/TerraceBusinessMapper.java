package com.gl.dongfang.mapper.platfrom;


import com.gl.dongfang.po.TerraceBusinessPO;
import com.gl.dongfang.po.TerracePO;
import com.gl.dongfang.po.TerracePatentPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TerraceBusinessMapper {
    //插入统计数据
    int BusinessinsertIntoTerrance(TerraceBusinessPO terraceBusiness);

    int BusinessbatchUpdate(TerraceBusinessPO terraceBusiness);
    String selectData(TerraceBusinessPO terraceBusiness);
}
