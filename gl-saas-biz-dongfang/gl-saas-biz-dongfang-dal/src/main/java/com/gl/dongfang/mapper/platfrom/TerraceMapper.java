package com.gl.dongfang.mapper.platfrom;


import com.gl.dongfang.po.DongfangChinesePatentPO;
import com.gl.dongfang.po.TerracePO;
import com.gl.dongfang.po.TerracePatentPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TerraceMapper {
    //插入统计数据
    int insertIntoTerrance(TerracePO terrace);

    int batchUpdate(TerracePO terrace);

    String selectData(TerracePO terrace);
}
