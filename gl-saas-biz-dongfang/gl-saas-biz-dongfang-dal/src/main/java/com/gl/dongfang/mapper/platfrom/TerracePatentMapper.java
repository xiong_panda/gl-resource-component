package com.gl.dongfang.mapper.platfrom;


import com.gl.dongfang.po.TerraceComponentPO;
import com.gl.dongfang.po.TerracePatentPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TerracePatentMapper {
    //插入统计数据
    int PatentInsertIntoTerrance(TerracePatentPO terracePatent);

    int batchUpdate(TerracePatentPO terracePatent);

    String selectData(TerracePatentPO terracePatent);


}
