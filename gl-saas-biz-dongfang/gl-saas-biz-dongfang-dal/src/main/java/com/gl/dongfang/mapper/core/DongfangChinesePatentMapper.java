package com.gl.dongfang.mapper.core;


import com.gl.dongfang.po.DongfangChinesePatentPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DongfangChinesePatentMapper {
    //核心资源库东方数据
    int DCPbatchInsertToCore(@Param("list") List<DongfangChinesePatentPO> list);
    //核心资源库东方数据
    List<String> DCPselectField();
}
