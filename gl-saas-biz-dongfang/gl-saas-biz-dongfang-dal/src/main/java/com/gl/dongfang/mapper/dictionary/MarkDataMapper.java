package com.gl.dongfang.mapper.dictionary;

import com.gl.dongfang.po.MarkDataPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MarkDataMapper {
    int batchInsertToCore(List<MarkDataPO> list);

    List<String> selectField();

    Integer selectResourceNum();
}
