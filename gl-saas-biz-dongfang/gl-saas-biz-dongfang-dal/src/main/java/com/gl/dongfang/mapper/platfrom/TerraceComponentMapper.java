package com.gl.dongfang.mapper.platfrom;


import com.gl.dongfang.po.TerraceBusinessPO;
import com.gl.dongfang.po.TerraceComponentPO;
import com.gl.dongfang.po.TerracePatentPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TerraceComponentMapper {
    //插入统计数据
    int ComponentInsertIntoTerrance(TerraceComponentPO terraceComponent);

    int ComponentbatchUpdate(TerraceComponentPO terraceComponent);
    String selectData(TerraceComponentPO terraceComponent);
}
