package com.gl.biz.city.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.util.DateUtils;
import com.gl.basis.common.util.StringUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CommBean;
import com.gl.biz.city.anno.FieldOrder;
import com.gl.biz.city.dto.SearchDTO;
import com.gl.biz.city.en.EOutApiErrorCode;
import com.gl.biz.city.mapper.core.*;
import com.gl.biz.city.out.dto.*;
import com.gl.biz.city.po.*;
import com.gl.biz.city.utill.HttpCityUtils;
import com.gl.biz.city.utill.HttpClientService;
import com.gl.biz.city.utill.sign.ConvertCommBeanUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@SuppressWarnings("all")
@ConfigurationProperties(prefix = "remote.saas")
@PropertySource(value = {"classpath:META-INF/app.properties"}, encoding = "UTF-8")
public class MultitermServiceInvocationService   {
    @Autowired
    private RemoteChinesePatentMapper chinesePatentMapper;
    @Autowired
    private RemoteOverseasPatentMapper overseasPatentMapper;
    @Autowired
    private RemoteHigherLearningUniversitiesMapper higherLearningUniversitiesMapper;
    @Autowired
    private RemoteForeignPeriodicalPaperMapper foreignPeriodicalPaperMapper;
    @Autowired
    private RemoteChineseConferencePaperMapper chineseConferencePaperMapper;
    @Autowired
    private RemoteTechnologyAchievementsMapper technologyAchievementsMapper;
    @Autowired
    private RemoteInfoOrgMapper infoOrgMapper;
    @Autowired
    private RemoteLawsRegulationsMapper lawsRegulationsMapper;
    @Autowired
    private RemoteScientificOrgMapper scientificOrgMapper;
    @Autowired
    private RemoteAuthorLibraryMapper authorLibraryMapper;
    @Autowired
    private RemoteEnterpriseProductDatabaseMapper enterpriseProductDatabaseMapper;
    @Autowired
    private RemoteChinesePaperOAMapper chinesePaperOAMapper;
    @Autowired
    private RemoteForeignLanguagePaperOAMapper foreignLanguagePaperOAMapper;
    @Autowired
    private RemoteChineseJournalPaperMapper chineseJournalPaperMapper;
    @Autowired
    private RemoteExpertsLibraryMapper expertsLibraryMapper;
    @Autowired
    private ConvertCommBeanUtil convertCommBeanUtil;


    @Value("${remote.saas.ip}")
    private String ip;
    @Value("${remote.saas.port}")
    private String port;
    //简略信息
    @Value("${remote.saas.mutiDataInfoSearch}")
    private String mutiDataInfoSearch;
    //详情
    @Value("${remote.saas.mutiDataIdsSearch}")
    private String mutiDataIdsSearch;
    //直接批量详情
    @Value("${remote.saas.mutiDataSearch}")
    private String mutiDataSearch;

    @Value("${platform.appkey}")
    private String appkey;
    @Value("${platform.signkey}")
    private String signKey;
    @Value("${platform.request.url}")
    private String url;

    private ExecutorService singleThreadExecutor= Executors.newSingleThreadExecutor();


    private static final Logger logger = LoggerFactory.getLogger(MultitermServiceInvocationService.class);
    //简略
    public CityOutResult mutiDataInfoSearch(SearchDTO dto) throws Exception {
//        if (StringUtils.isEmpty(ip) && StringUtils.isEmpty(port) && StringUtils.isEmpty(mutiDataInfoSearch)) {
//            throw new BusinessException(EOutApiErrorCode.CONIFG_ERR.msg, EOutApiErrorCode.CONIFG_ERR.code);
//        }

        Map<String, String> reqBean = new HashMap<String, String>();
        dto.setFrom(String.valueOf((Integer.valueOf(dto.getFrom())-1)*10));
        reqBean.put("appkey", appkey);
        reqBean.put("method", mutiDataInfoSearch);
        reqBean.put("bz_data", JSONObject.toJSONString(dto));

        CityOutResult<Object> cityOutResult = new CityOutResult<>();
        try {
            cityOutResult = HttpCityUtils.execCityResult(url, reqBean,null, signKey);
            //BeanUtil.transMap2Bean2(exec, cityOutResult);
        } catch (Exception e) {
            throw new BusinessException("远程调用失败:" + e.getMessage());
        }

        return cityOutResult;




    }

    //详情
    public CityOutResult mutiDataIdsSearch(SearchDTO dto) throws Exception {
//        if (StringUtils.isEmpty(ip) && StringUtils.isEmpty(port) && StringUtils.isEmpty(mutiDataIdsSearch)) {
//            throw new BusinessException(EOutApiErrorCode.CONIFG_ERR.msg, EOutApiErrorCode.CONIFG_ERR.code);
//        }
        String idsString = dto.getIds();
        if (StringUtils.isEmpty(idsString)) {
            throw new BusinessException(EOutApiErrorCode.CONIFG_ERR.msg, EOutApiErrorCode.CONIFG_ERR.code);
        }

        PageInfo<?> pageInfo = doQuery(dto,2);

        CityOutResult cityOutResult = new CityOutResult();
        if (!CollectionUtils.isEmpty(pageInfo.getList()) && pageInfo.getList().size() == idsString.split(",").length) {
            cityOutResult.setResult(pageInfo.getList());
            cityOutResult.setTotalSize(pageInfo.getTotal());
            return cityOutResult;
        }

        Map<String, String> reqBean = new HashMap<String, String>();
        dto.setFrom(String.valueOf((Integer.valueOf(dto.getFrom())-1)*10));
        reqBean.put("appkey", appkey);
        reqBean.put("method", mutiDataIdsSearch);
        reqBean.put("bz_data", JSONObject.toJSONString(dto));

        CityOutResult<Object> remoteCityOutResult;
        try {
            remoteCityOutResult = HttpCityUtils.execCityResult(url, reqBean,null, signKey);
        } catch (Exception e) {
            throw new BusinessException("远程调用失败:" + e.getMessage());
        }


        if (0==remoteCityOutResult.getTotalSize() || remoteCityOutResult.gtResult() == null) {
            return cityOutResult;
        } else {
            singleThreadExecutor.execute(()-> {
                try {
                    doInsert(remoteCityOutResult.gtResult(),dto.getResourceCls());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }

        if (remoteCityOutResult.getTotalSize() > cityOutResult.getTotalSize()) {
            return remoteCityOutResult;
        } else {
            return cityOutResult;
        }
    }

    //直接详情
    public CityOutResult mutiDataSearch(SearchDTO dto) throws Exception {
//        if (StringUtils.isEmpty(ip) && StringUtils.isEmpty(port) && StringUtils.isEmpty(mutiDataSearch)) {
//            throw new BusinessException(EOutApiErrorCode.CONIFG_ERR.msg, EOutApiErrorCode.CONIFG_ERR.code);
//        }

        PageInfo<?> pageInfo = doQuery(dto,1);
        CityOutResult cityOutResult = new CityOutResult();
        if (!CollectionUtils.isEmpty(pageInfo.getList()) && pageInfo.getList().size() >= Integer.valueOf(dto.getPageSize()).intValue()) {
            cityOutResult.setResult(pageInfo.getList());
            cityOutResult.setTotalSize(pageInfo.getTotal());
            return cityOutResult;
        }

        Map<String, String> reqBean = new HashMap<String, String>();
        dto.setFrom(String.valueOf((Integer.valueOf(dto.getFrom())-1)*10));
        reqBean.put("appkey", appkey);
        reqBean.put("method", mutiDataInfoSearch);
        reqBean.put("bz_data", JSONObject.toJSONString(dto));

        CityOutResult<Object> remoteCityOutResult;
        try {
            remoteCityOutResult = HttpCityUtils.execCityResult(url, reqBean,null, signKey);
        } catch (Exception e) {
            throw new BusinessException("远程调用失败:" + e.getMessage());
        }

        if ((remoteCityOutResult.gtResult()==null||remoteCityOutResult.getTotalSize() == 0)) {
            return cityOutResult;
        }else {
            singleThreadExecutor.execute(()-> {
                try {
                    doInsert(remoteCityOutResult.gtResult(),dto.getResourceCls());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }

        if (remoteCityOutResult.getTotalSize() > cityOutResult.getTotalSize()) {
            return remoteCityOutResult;
        } else {
            return cityOutResult;
        }
    }

    private PageInfo<?> doQuery(SearchDTO dto, Integer type) throws IllegalAccessException {
        Map<String, Object> conditionMap = Maps.newHashMap();
        String resourceCls = dto.getResourceCls();


        if (type == 1) {
            String conditionString = dto.getField();
            String[] conditionArray = conditionString.split("&");

            for (String condition : conditionArray) {
                String[] item = condition.split(":");
                if (item.length > 2) {
                    throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg, EOutApiErrorCode.PARAM_ERR.code);
                }

                conditionMap.put(item[0], item[1]);
            }
        } else {
            String[] idArray = dto.getIds().split(",");
            if (idArray.length == 0) {
                throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg, EOutApiErrorCode.PARAM_ERR.code);
            }
                List<String> idList = Lists.newArrayList();
            for (String s : idArray) {
                idList.add(s);
            }
            conditionMap.put("ids", idList);
        }


        //万方中文专利信息
        if (StringUtils.isEmpty(resourceCls) || resourceCls.equals("10004")) {
            Page<ChinesePatentPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            chinesePatentMapper.CPselectList(conditionMap);
            PageInfo<ChinesePatentPO> pageInfo = new PageInfo(page);

            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WChinesePatentOUTDTO> list = Lists.newArrayList();
                PageInfo<WChinesePatentOUTDTO> pageInfo2 = new PageInfo(page);

                for (ChinesePatentPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WChinesePatentOUTDTO outdto = new WChinesePatentOUTDTO();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }

        //期刊
        if (resourceCls.equals("10001")) {
            Page<ChineseJournalPapersPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            chineseJournalPaperMapper.CJPselectList(conditionMap);
            PageInfo<ChineseJournalPapersPO> pageInfo = new PageInfo(page);

            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WChineseJournalPaperOutDto> list = Lists.newArrayList();
                PageInfo<WChineseJournalPaperOutDto> pageInfo2 = new PageInfo(page);

                for (ChineseJournalPapersPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WChineseJournalPaperOutDto outdto = new WChineseJournalPaperOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;

        }
        //高等院校
        if (resourceCls.equals("10013")) {
            Page<HigherLearningUniversitiesPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            higherLearningUniversitiesMapper.HLUselectList(conditionMap);
            PageInfo<HigherLearningUniversitiesPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WHigherLearningUniversitiesOutDto> list = Lists.newArrayList();
                PageInfo<WHigherLearningUniversitiesOutDto> pageInfo2 = new PageInfo(page);

                for (HigherLearningUniversitiesPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WHigherLearningUniversitiesOutDto outdto = new WHigherLearningUniversitiesOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方外文oA论文
        if (resourceCls.equals("10040")) {
            Page<ForeignLanguagePaperOAPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            foreignLanguagePaperOAMapper.FLPOAselectList(conditionMap);
            PageInfo<ForeignLanguagePaperOAPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WForeignLanguageOaPaperOutDto> list = Lists.newArrayList();
                PageInfo<WForeignLanguageOaPaperOutDto> pageInfo2 = new PageInfo(page);

                for (ForeignLanguagePaperOAPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WForeignLanguageOaPaperOutDto outdto = new WForeignLanguageOaPaperOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方中文oA论文
        if (resourceCls.equals("10037")) {
            Page<ChinesePaperOAPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            chinesePaperOAMapper.CPOAselectList(conditionMap);
            PageInfo<ChinesePaperOAPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WChinesePaperOaOutDto> list = Lists.newArrayList();
                PageInfo<WChinesePaperOaOutDto> pageInfo2 = new PageInfo(page);

                for (ChinesePaperOAPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WChinesePaperOaOutDto outdto = new WChinesePaperOaOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方企业
        if (resourceCls.equals("10011")) {
            Page<EnterpriseProductDatabasePO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            enterpriseProductDatabaseMapper.EPDselectList(conditionMap);
            PageInfo<EnterpriseProductDatabasePO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WanFangEnterpriseProductDatabaseOutDto> list = Lists.newArrayList();
                PageInfo<WanFangEnterpriseProductDatabaseOutDto> pageInfo2 = new PageInfo(page);

                for (EnterpriseProductDatabasePO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WanFangEnterpriseProductDatabaseOutDto outdto = new WanFangEnterpriseProductDatabaseOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方专家信息
        if (resourceCls.equals("10014")) {
            Page<ExpertsLibraryPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            expertsLibraryMapper.ELselectList(conditionMap);
            PageInfo<ExpertsLibraryPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WExpertsLibraryOutDto> list = Lists.newArrayList();
                PageInfo<WExpertsLibraryOutDto> pageInfo2 = new PageInfo(page);

                for (ExpertsLibraryPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WExpertsLibraryOutDto outdto = new WExpertsLibraryOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }

        //万方作者信息
        if (resourceCls.equals("10020")) {
            Page<AuthorLibraryPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            authorLibraryMapper.ALselectList(conditionMap);
            PageInfo<AuthorLibraryPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WAuthorLibraryOutDto> list = Lists.newArrayList();
                PageInfo<WAuthorLibraryOutDto> pageInfo2 = new PageInfo(page);

                for (AuthorLibraryPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WAuthorLibraryOutDto outdto = new WAuthorLibraryOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方法律法规
        if (resourceCls.equals("10012")) {
            Page<LawsRegulationsPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            lawsRegulationsMapper.LRselectList(conditionMap);
            PageInfo<LawsRegulationsPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WLawsRegulationsOutDTO> list = Lists.newArrayList();
                PageInfo<WLawsRegulationsOutDTO> pageInfo2 = new PageInfo(page);

                for (LawsRegulationsPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WLawsRegulationsOutDTO outdto = new WLawsRegulationsOutDTO();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方机构
        if (resourceCls.equals("10022")) {
            Page<ScientificOrgPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            scientificOrgMapper.SOselectList(conditionMap);
            PageInfo<ScientificOrgPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WScientificOrgOutDto> list = Lists.newArrayList();
                PageInfo<WScientificOrgOutDto> pageInfo2 = new PageInfo(page);

                for (ScientificOrgPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WScientificOrgOutDto outdto = new WScientificOrgOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方信息机构信息
        if (resourceCls.equals("10033")) {
            Page<InfoOrgPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            infoOrgMapper.IOselectList(conditionMap);
            PageInfo<InfoOrgPO> pageInfo = new PageInfo(page);
            return pageInfo;
        }
        //万方海外专利信息
        if (resourceCls.equals("10016")) {
            Page<OverseasPatentPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            overseasPatentMapper.OPselectList(conditionMap);
            PageInfo<OverseasPatentPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WOverseasPatentOutDto> list = Lists.newArrayList();
                PageInfo<WOverseasPatentOutDto> pageInfo2 = new PageInfo(page);

                for (OverseasPatentPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WOverseasPatentOutDto outdto = new WOverseasPatentOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方外文期刊论文
        if (resourceCls.equals("10021")) {
            Page<ForeignPeriodicalPaperPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            foreignPeriodicalPaperMapper.FPPselectList(conditionMap);
            PageInfo<ForeignPeriodicalPaperPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WForeignPeriodicalPaperOutDto> list = Lists.newArrayList();
                PageInfo<WForeignPeriodicalPaperOutDto> pageInfo2 = new PageInfo(page);

                for (ForeignPeriodicalPaperPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WForeignPeriodicalPaperOutDto outdto = new WForeignPeriodicalPaperOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方中文会议论文
        if (resourceCls.equals("10002")) {
            Page<ChineseConferencePaperPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            chineseConferencePaperMapper.CCPselectList(conditionMap);
            PageInfo<ChineseConferencePaperPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WChineseConferenceCitationsOutDto> list = Lists.newArrayList();
                PageInfo<WChineseConferenceCitationsOutDto> pageInfo2 = new PageInfo(page);

                for (ChineseConferencePaperPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WChineseConferenceCitationsOutDto outdto = new WChineseConferenceCitationsOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }
        //万方科技成果信息
        if (resourceCls.equals("10006")) {
            Page<TechnologyAchievementsPO> page = PageHelper.startPage(Integer.valueOf(dto.getFrom()),
                    conditionMap.get("ids")==null?Integer.valueOf(dto.getPageSize()):10000);
            technologyAchievementsMapper.TAselectList(conditionMap);
            PageInfo<TechnologyAchievementsPO> pageInfo = new PageInfo(page);
            if (!CollectionUtils.isEmpty(pageInfo.getList())) {
                List<WTechnologyAchievementsOutDto> list = Lists.newArrayList();
                PageInfo<WTechnologyAchievementsOutDto> pageInfo2 = new PageInfo(page);

                for (TechnologyAchievementsPO po : pageInfo.getList()) {
                    Field[] wFields = po.getClass().getDeclaredFields();
                    WTechnologyAchievementsOutDto outdto = new WTechnologyAchievementsOutDto();
                    Field[] gFields = outdto.getClass().getDeclaredFields();

                    //利用反射填充核心资源库数据
                    for (Field field : gFields) {
                        field.setAccessible(true);
                        for (Field wfield : wFields) {
                            wfield.setAccessible(true);
                            if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                                field.set(outdto, wfield.get(po));
                            }
                        }
                    }
                    list.add(outdto);
                }
                pageInfo2.setList(list);
                pageInfo2.setTotal(pageInfo.getTotal());
                return pageInfo2;
            }
            return pageInfo;
        }

        throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg, EOutApiErrorCode.PARAM_ERR.code);
    }

    private void doInsert(Object data,String resourceCls) throws IllegalAccessException {
        List<Map> dataList = JSONObject.parseArray(data.toString(), Map.class);
        if (CollectionUtils.isEmpty(dataList)) {
            return;
        }

        //万方中文专利信息
        if (StringUtils.isEmpty(resourceCls) || resourceCls.equals("10004")) {
            List<ChinesePatentPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WChinesePatentOUTDTO dto = JSONObject.parseObject(JSONObject.toJSONString(map), WChinesePatentOUTDTO.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ChinesePatentPO chinesePatentPO = new ChinesePatentPO();
                Field[] localFields = chinesePatentPO.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(chinesePatentPO, remoteField.get(dto));
                        }
                    }
                }

                poList.add(chinesePatentPO);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = chinesePatentMapper.queryIds(ids);
            List<ChinesePatentPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                chinesePatentMapper.batchInsertToCore(finalDataList);
            }

            return;
        }

        //万方海外专利信息
        if (resourceCls.equals("10016")) {
            List<OverseasPatentPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WOverseasPatentOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WOverseasPatentOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                OverseasPatentPO po = new OverseasPatentPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }
                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = overseasPatentMapper.queryIds(ids);
            List<OverseasPatentPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                overseasPatentMapper.OPbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方外文期刊论文
        if (resourceCls.equals("10021")) {
            List<ForeignPeriodicalPaperPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WForeignPeriodicalPaperOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WForeignPeriodicalPaperOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ForeignPeriodicalPaperPO po = new ForeignPeriodicalPaperPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }
                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = foreignPeriodicalPaperMapper.queryIds(ids);
            List<ForeignPeriodicalPaperPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                foreignPeriodicalPaperMapper.FPPbatchInsertToCore(finalDataList);
            }

            return;

        }

        //万方中文会议论文
        if (resourceCls.equals("10002")) {
            List<ChineseConferencePaperPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WChineseConferencePaperOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WChineseConferencePaperOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ChineseConferencePaperPO po = new ChineseConferencePaperPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = chineseConferencePaperMapper.queryIds(ids);
            List<ChineseConferencePaperPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                chineseConferencePaperMapper.CCPbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方科技成果信息
        if (resourceCls.equals("10006")) {
            List<TechnologyAchievementsPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WTechnologyAchievementsOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WTechnologyAchievementsOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                TechnologyAchievementsPO po = new TechnologyAchievementsPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = technologyAchievementsMapper.queryIds(ids);
            List<TechnologyAchievementsPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                technologyAchievementsMapper.TAbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方信息机构信息
        if (resourceCls.equals("10033")) {
            List<InfoOrgPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                InfoOrgPO po = JSONObject.parseObject(JSONObject.toJSONString(map), InfoOrgPO.class);
                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = infoOrgMapper.queryIds(ids);
            List<InfoOrgPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                infoOrgMapper.IObatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方法律法规
        if (resourceCls.equals("10012")) {
            List<LawsRegulationsPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WLawsRegulationsOutDTO dto = JSONObject.parseObject(JSONObject.toJSONString(map), WLawsRegulationsOutDTO.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                LawsRegulationsPO po = new LawsRegulationsPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = lawsRegulationsMapper.queryIds(ids);
            List<LawsRegulationsPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                lawsRegulationsMapper.LRbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方机构
        if (resourceCls.equals("10022")) {
            List<ScientificOrgPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WScientificOrgOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WScientificOrgOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ScientificOrgPO po = new ScientificOrgPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = scientificOrgMapper.queryIds(ids);
            List<ScientificOrgPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                scientificOrgMapper.SObatchInsertToCore(finalDataList);
            }

            return;
        }
        //万方作者信息
        if (resourceCls.equals("10020")) {
            List<AuthorLibraryPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WAuthorLibraryOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WAuthorLibraryOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                AuthorLibraryPO po = new AuthorLibraryPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = authorLibraryMapper.queryIds(ids);
            List<AuthorLibraryPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                authorLibraryMapper.ALbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方企业
        if (resourceCls.equals("10011")) {
            List<EnterpriseProductDatabasePO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WanFangEnterpriseProductDatabaseOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WanFangEnterpriseProductDatabaseOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                EnterpriseProductDatabasePO po = new EnterpriseProductDatabasePO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = enterpriseProductDatabaseMapper.queryIds(ids);
            List<EnterpriseProductDatabasePO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                enterpriseProductDatabaseMapper.EPDbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方中文oA论文
        if (resourceCls.equals("10037")) {
            List<ChinesePaperOAPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WChinesePaperOaOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WChinesePaperOaOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ChinesePaperOAPO po = new ChinesePaperOAPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = chinesePaperOAMapper.queryIds(ids);
            List<ChinesePaperOAPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                chinesePaperOAMapper.CPOAbatchInsertToCore(finalDataList);
            }

            return;
        }

        //万方外文oA论文
        if (resourceCls.equals("10040")) {
            List<ForeignLanguagePaperOAPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WForeignLanguageOaPaperOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WForeignLanguageOaPaperOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ForeignLanguagePaperOAPO po = new ForeignLanguagePaperOAPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = foreignLanguagePaperOAMapper.queryIds(ids);
            List<ForeignLanguagePaperOAPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                foreignLanguagePaperOAMapper.FLPOAbatchInsertToCore(finalDataList);
            }

            return;
        }

        //高等院校
        if (resourceCls.equals("10013")) {
            List<HigherLearningUniversitiesPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WHigherLearningUniversitiesOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WHigherLearningUniversitiesOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                HigherLearningUniversitiesPO po = new HigherLearningUniversitiesPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = higherLearningUniversitiesMapper.queryIds(ids);
            List<HigherLearningUniversitiesPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                higherLearningUniversitiesMapper.HLUbatchInsertToCore(finalDataList);
            }

            return;
        }

        //期刊
        if (resourceCls.equals("10001")) {
            List<ChineseJournalPapersPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WChineseJournalPaperOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WChineseJournalPaperOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ChineseJournalPapersPO po = new ChineseJournalPapersPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = chineseJournalPaperMapper.queryIds(ids);
            List<ChineseJournalPapersPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                chineseJournalPaperMapper.CJPbatchInsertToCore(finalDataList);
            }

            return;
        }

        //专家
        if (resourceCls.equals("10014")) {
            List<ExpertsLibraryPO> poList = Lists.newArrayList();

            for (Map map : dataList) {
                WExpertsLibraryOutDto dto = JSONObject.parseObject(JSONObject.toJSONString(map), WExpertsLibraryOutDto.class);
                Field[] remotefields = dto.getClass().getDeclaredFields();

                ExpertsLibraryPO po = new ExpertsLibraryPO();
                Field[] localFields = po.getClass().getDeclaredFields();

                //利用反射填充核心资源库数据
                for (Field localField : localFields) {
                    localField.setAccessible(true);
                    for (Field remoteField : remotefields) {
                        remoteField.setAccessible(true);
                        if (localField.getAnnotation(FieldOrder.class).equals(remoteField.getAnnotation(FieldOrder.class))) {
                            localField.set(po, remoteField.get(dto));
                        }
                    }
                }

                po.setCreateTime(new Date());
                poList.add(po);
            }

            List<String> ids = poList.stream().map(e -> e.getWfId()).collect(Collectors.toList());

            List<String> existIds = expertsLibraryMapper.queryIds(ids);
            List<ExpertsLibraryPO> finalDataList = poList.stream().filter(e -> !existIds.contains(e.getWfId())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(finalDataList)) {
                expertsLibraryMapper.ELPbatchInsertToCore(finalDataList);
            }

            return;
        }

    }
}
