package com.gl.biz.city.utill.sign;

import com.gl.basis.common.util.DateUtils;

public class CityConstant {

    /**（1.万方，2东方（专利））**/
    public static String SERVICEID_WANFANG = "1";
    public static String SERVICEID_DONGFANG = "2";

    /**资源类型码值**/
    public static String CHINESE_PATENT_CODE = "10004";//中文专利

    public static String JOURNAL_CJPAPERS_CODE = "10001";//中文期刊论文
    public static String JOURNAL_CNCPAPERIDS_SEARCH_CODE = "10002";//中文会议论文

    public static String CHINESE_PAPER_OA_CODE = "10037";//中文OA论文
    public static String ENTERPRISE_PRODUCT_DATABASE = "10011";//企业产品数据库
    public static String EXPERTS_LIBRARY = "10014";//专家库
    public static String FOREIGN_LANGUAGE_OA_PAPER = "10040";//外文OA论文
    public static String HIGHER_LEARNING_UNIVERSITIE = "10013";//高等院校
    public static String INFO_ORG = "10022";//科研机构
    public static String LAWS_REGULATIONS = "10012";//法律法规
    public static String OVERSEAS_PATENT = "10016";//海外专利
    public static String TECHNOLOGY_ACHIEVEMENTS= "10006";//科技成果
    public static String AUTHOR_SEARCH_CODE= "10020";//作者

    /**万方接口**/
    public static String WANFANG_JIANLUE = "/outApi/data/mutiDataInfoSearch";
    public static String WANFANG_XAINGQING = "/outApi/data/mutiDataIdsSearch";

    /**万方接口**/
    public static String TIME_NOW =  DateUtils.getCurDate("yyyy-MM-dd HH:mm:ss");
    public static String APP_KEY = "11";
    public static String VERSION = "v0.01";
    public static String SIGN_METHOD = "md5";
    public static String FORMAT = "json";
    public static String KEY = "111111";

    /**本地最小资源数**/
    public static Integer LOCAL_MIN = 200;

}
