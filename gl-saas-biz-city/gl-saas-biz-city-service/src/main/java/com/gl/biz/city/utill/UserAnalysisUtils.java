package com.gl.biz.city.utill;

import com.gl.basis.auth.response.ProfileResult;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;

/**
 * 用户解析
 */
public class UserAnalysisUtils {

    /**
     * 获取用户信息
     * @return
     */
    @ModelAttribute
    public static String getUserInfo(){
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principals = subject.getPrincipals();
        return Optional.ofNullable(principals).map(value->{
            ProfileResult profileResult = (ProfileResult)principals.getPrimaryPrincipal();
            return profileResult.getUserId();
        }).orElse("");
    }
}
