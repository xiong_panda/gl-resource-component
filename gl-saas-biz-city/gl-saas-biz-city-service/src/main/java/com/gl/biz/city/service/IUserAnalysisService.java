package com.gl.biz.city.service;

import com.gl.biz.city.out.dto.IndustryFieldOutDTO;

import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 2020-3-25 18:53
 * 解析跟用户相关的接口
 */
public interface IUserAnalysisService {
    //从登陆信息中获取用户对应的行业领域
    IndustryFieldOutDTO getIndustryFiledByLogin();

    //获取行业领域标签
    List<Map<String, Object>> getIndustryFieldAll();
}
