package com.gl.biz.city.config;

import com.gl.basis.common.context.ContextUtils;
import com.gl.basis.util.RedisUtil;
import com.gl.biz.city.utill.UserAnalysisUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * xiaweijie
 * 记录用户历史记录
 */
@Aspect
@Component
@Slf4j
public class RetrievalHistoryConfig {

    @Resource
    private RedisUtil redisUtil;

    @Resource
    RedisTemplate redisTemplate;

    @Pointcut("@annotation(com.gl.biz.city.config.UserSearchLog)")
    public void pointcut() { }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        String userId= UserAnalysisUtils.getUserInfo();
        if(null==userId || ("").equals(userId)){
            return point.proceed();
        }
        HttpServletRequest request = ContextUtils.getRequest();
        String requestURI = request.getRequestURI();
        Object[] args = point.getArgs();
        Map<String, Object> keyAndValue = getKeyAndValue(args[0]);
        Object filter = null;
        if (null!=keyAndValue.get("field")||!ObjectUtils.isEmpty(keyAndValue.get("field"))) {
            filter = keyAndValue.get("field");
        }
        List<Object> objects = Arrays.asList(filter);

        /*赋值*/
        List<Object> newList=new  ArrayList< Object>(Arrays.asList(new Object[objects.size()]));
        Collections.copy(newList, objects);
        List lists=new ArrayList();
        //多個參數以&分隔開
        String[] split = newList.get(0).toString().split("&");
        for (int i=0;i<split.length;i++){
            String[] sp = split[i].split(":");
            Map map=new HashMap();
            map.put(sp[0],sp[1]);
            lists.add(map);
        }
        if(lists.size()>0){
            setUserSearch(userId+requestURI,lists);
            saveUserSearch(userId,requestURI,lists);
//            heatAnalysis(lists);
        }
        return point.proceed();
    }

    /**
     * 用户搜索缓存记录
     * @param key
     * @param value
     */
    private void setUserSearch(String key,Object value){

        redisTemplate.opsForList().remove(key,-1,value);

        redisUtil.setListLeft(key,value);
        //对搜索不同的条件存储10条就好了
        if(redisTemplate.opsForList().size(key)>10){
            redisTemplate.opsForList().rightPop(key);
        }
    }

    /**
     * 落库数据
     * @param userId
     * @param requestURI
     * @param filter
     * @return
     * @throws Throwable
     */
    private void saveUserSearch(String userId,String requestURI,Object filter){
        //用户：接口：参数：时间
        List<Map<String,Object>> list=new LinkedList();
        Map map=new HashMap();
        map.put("userId",userId);
        map.put("requestURI",requestURI);
        map.put("filter",filter.toString());
        Date date=new Date();
        String dateStr=new SimpleDateFormat("YYYY-MM-dd : HH:mm:ss").format(date);
        map.put("dateStr",dateStr);
        list.add(map);
        //排除重复搜索
        List<Map<String,Object>> data = redisTemplate.opsForList().range("usrSearch",0,-1);
        for (Map maps:data){
            if(maps.containsValue(userId) && maps.containsValue(requestURI) && maps.containsValue(filter.toString())){
                return;
            }
        }

        redisTemplate.opsForList().rightPushAll("usrSearch",list);
    }


    /**
     * 解析切面字段
     * @param obj
     * @return
     */
    private  Map<String, Object> getKeyAndValue(Object obj) {
        Map<String, Object> map = new HashMap<>();
        // 得到类对象
        Class userCla = (Class) obj.getClass();
        /* 得到类中的所有属性集合 */
        Field[] fs = userCla.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            Field f = fs[i];
            f.setAccessible(true); // 设置些属性是可以访问的
            Object val = new Object();
            try {
                val = f.get(obj);
                // 得到此属性的值
                map.put(f.getName(), val);// 设置键值
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return map;
    }


    /**
     * 热度词处理
     * 对搜索热度词值增加
     */
/*
    private void heatAnalysis(Object value){
        if(!value.equals("") && null!=value){

            Long increment = redisTemplate.opsForValue().increment(value.toString(), 1);
            log.info("次数:{}",increment);
        }

    }
*/


}