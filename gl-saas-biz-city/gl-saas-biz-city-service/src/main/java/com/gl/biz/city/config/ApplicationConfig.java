package com.gl.biz.city.config;

import javax.servlet.MultipartConfigElement;

import com.gl.basis.auth.shiro.config.ShiroConfiguration;
import com.gl.common.MybatisMainConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.gl.basis.common.config.CoreMainConfig;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.config.RedisConfig;

/**
 * @ClassName: ApplicationConfig
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author
 * @date 2018年12月3日
 * 
 */

@Configuration
@Import(value = { 
		CoreMainConfig.class,
		MybatisMainConfig.class, ShiroConfiguration.class,RedisConfig.class
		})
public class ApplicationConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);
	
	/** 线程池维护线程的最少数量 *//*
	@Value("${TaskE.corePoolSize}")
	private int corePoolSize;

	*//** 线程池维护线程的最大数量 *//*
	@Value("${TaskE.maxPoolSize}")
	private int maxPoolSize;

	*//** 线程池所使用的缓冲队列 *//*
	@Value("${TaskE.queueCapacity}")
	private int queueCapacity;

	*//** 线程池维护线程所允许的空闲时间 *//*
	@Value("${TaskE.keepAliveSeconds}")
	private int keepAliveSeconds;

	*//**
	 * sendSms线程池
	 *
	 * @param @return 参数 @return TaskExecutor 返回类型 @throws
	 *//*
	@Bean
	TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(corePoolSize);
		executor.setMaxPoolSize(maxPoolSize);
		executor.setQueueCapacity(queueCapacity);
		executor.setKeepAliveSeconds(keepAliveSeconds);
		return executor;
	}



	
*/
	/**
	 * 上传文件限制大小
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// // 设置文件大小限制 ,超了，页面会抛出异常信息，这时候就需要进行异常信息的处理了;
		factory.setMaxFileSize("100MB"); // KB,MB
		// / 设置总上传数据总大小
		return factory.createMultipartConfig();
	}

	@Bean
	public IdWorker idWorkker() {
		return new IdWorker(1, 1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
