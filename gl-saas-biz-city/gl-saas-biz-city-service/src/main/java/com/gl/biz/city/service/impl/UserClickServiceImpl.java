package com.gl.biz.city.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.bo.HxWfServerSearchBO;
import com.gl.biz.city.dto.HxWfServerSearchInDto;
import com.gl.biz.city.entity.HxWfServerSearch;
import com.gl.biz.city.out.dto.HxWfServerSearchOutDto;
import com.gl.biz.city.service.IUserClickDalService;
import com.gl.biz.city.service.IUserClickService;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserClickServiceImpl implements IUserClickService {

    @Autowired
    private IUserClickDalService userClickServiceDal;

    @Override
    public void saveUserClick(List<HxWfServerSearchBO> list) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        List<HxWfServerSearch> hxWfServerSearchList = mapperFactory.getMapperFacade().mapAsList(list, HxWfServerSearch.class);
        userClickServiceDal.saveUserClick(hxWfServerSearchList);
    }

    @Override
    public Page<HxWfServerSearchOutDto> getListByCollectTime(Page<HxWfServerSearchInDto> page) {
        Page<HxWfServerSearchOutDto> newPage=new Page<HxWfServerSearchOutDto>();
        newPage.setIsPage(page.getIsPage());
        newPage.setPageNo(page.getPageNo());
        newPage.setParams(page.getParams());
        newPage.setResult(userClickServiceDal.getListByCollectTime(page.getParams()));
        return newPage;
    }
}
