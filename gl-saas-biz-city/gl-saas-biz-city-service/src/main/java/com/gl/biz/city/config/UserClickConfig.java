package com.gl.biz.city.config;

import com.gl.basis.common.context.ContextUtils;
import com.gl.basis.common.util.BeanUtil;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.common.util.StringUtils;
import com.gl.basis.util.RedisUtil;
import com.gl.biz.city.dto.SysCodeInDto;
import com.gl.biz.city.out.dto.SysCodeOutDto;
import com.gl.biz.city.service.ISysCodeService;
import com.gl.biz.city.service.IUserAnalysisService;
import com.gl.biz.city.utill.UserAnalysisUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author QinYe 记录用户搜索关键字和浏览资源信息
 * @date 2020/6/19 11:18
 * @description
 */
@Aspect
@Component
@Slf4j
public class UserClickConfig {

    @Autowired
    private RedisUtil redisUtil;


    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private IUserAnalysisService userAnalysisService;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private ISysCodeService sysCodeService;

    @Pointcut("@annotation(com.gl.biz.city.config.UserClickLog)")
    public void pointcut() {

    }

    @Around(value="pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //搜索关键字
        String keyWord = "";

        String userId= UserAnalysisUtils.getUserInfo();
        if(null==userId || ("").equals(userId)){
            return point.proceed();
        }
        HttpServletRequest request = ContextUtils.getRequest();
        String requestURI = request.getRequestURI();
        Object[] args = point.getArgs();
        Map<String, Object> keyAndValue = getKeyAndValue(args[0]);
        Object filter = null;
        if (null!=keyAndValue.get("field")||!ObjectUtils.isEmpty(keyAndValue.get("field"))) {
            filter = keyAndValue.get("field");
            List<Object> objects = Arrays.asList(filter);

            /*赋值*/
            List<Object> newList = new ArrayList<Object>(Arrays.asList(new Object[objects.size()]));
            Collections.copy(newList, objects);
            //多個參數以&分隔開
            String[] split = newList.get(0).toString().split("&");

            for (int i = 0; i < split.length; i++) {
                String[] sp = split[i].split(":");
                keyWord += sp[1] + ",";
            }
        }

        //资源id
        String sourcesId=null;
        if (null!=keyAndValue.get("ids")||!ObjectUtils.isEmpty(keyAndValue.get("ids"))) {
            sourcesId = String.valueOf(keyAndValue.get("ids"));
        }

        //只对带参数或获取详情的操作进行记录
        if(null==keyAndValue.get("ids")||ObjectUtils.isEmpty(keyAndValue.get("ids"))) {
            if (null == keyWord || StringUtils.isEmpty(keyWord)) {
                return point.proceed();
            }
        }

        //ID
        String id = idWorker.nextId()+"";

        SysCodeInDto sysCodeInDto = new SysCodeInDto();
        sysCodeInDto.setValCode(String.valueOf(keyAndValue.get("resourceCls")));
        SysCodeOutDto code=sysCodeService.translateCode(BeanUtil.transBean2Map(sysCodeInDto));
        Map<String, Object> codeMap = new HashMap<>();
        if(!ObjectUtils.isEmpty(code)) {
            codeMap = BeanUtil.transBean2Map(code);
        }

        //服务名
        String serviceName = null;
        if(null!=codeMap.get("remark")||!ObjectUtils.isEmpty(codeMap.get("remark"))) {
            serviceName=String.valueOf(codeMap.get("remark"));
            requestURI=String.valueOf(codeMap.get("ValName"));
        }

        //使用时间
        Date useTime =new Date(System.currentTimeMillis());

        //采集方式（0：调度平台采集、1：实时接口采集）
        String collectType = "1";
        //采集时间,为定时任务存取的时间
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY,2);
        Date collectTime = calendar.getTime();

        //解析返回值
        String sourcesName=null;
        String sourcesOrigin =null;
        String flag="1";
        if(StringUtils.isEmpty(keyWord)){
            flag="0";

        }

        Map<String,Object> logMap=new HashMap<>();
        logMap.put("id",id);
        logMap.put("userId",userId);
        logMap.put("searchKeyword",keyWord);
        logMap.put("serviceName",serviceName);
        logMap.put("serviceOrigin",requestURI);
        logMap.put("sourcesName",sourcesName);
        logMap.put("sourcesOrigin",sourcesOrigin);
        logMap.put("sourcesId",sourcesId);
        logMap.put("flag",flag);
        logMap.put("useTime",useTime);
        logMap.put("collectTime",collectTime);
        logMap.put("collectType",collectType);

        setUserSearch(userId+requestURI+keyWord,logMap);
        saveUserSearch(userId,requestURI,keyWord,logMap);

        return point.proceed();
    }

    /**
     * 用户搜索缓存记录
     * @param key
     * @param value
     */
    private void setUserSearch(String key,Object value){

        redisTemplate.opsForList().remove(key,-1,value);
        redisUtil.setListLeft(key,value);
        redisTemplate.opsForList().rightPop(key);
    }

    /**
     * 落库数据
     * @return
     * @throws Throwable
     */
    private void saveUserSearch(String userId,String requestURI,String keyWord,Map<String,Object> map){
        //排除重复搜索
//        redisTemplate.delete("userClick");
        List<Map<String,Object>> resultList=new ArrayList<>();
        List<Map<String,Object>> data = redisTemplate.opsForList().range("userClick",0,-1);
        if (data.size()>0) {
            for (Map maps : data) {
                if (maps.containsValue(userId)&&maps.containsValue(requestURI)&&maps.containsValue(keyWord)) {
                    return;
                }
            }
        }

        resultList.add(map);
        redisTemplate.opsForList().rightPushAll("userClick",resultList);
    }


    /**
     * 解析切面字段
     * @param obj
     * @return
     */
    private  Map<String, Object> getKeyAndValue(Object obj) {
        Map<String, Object> map = new HashMap<>();
        // 得到类对象
        Class userCla = (Class) obj.getClass();
        /* 得到类中的所有属性集合 */
        Field[] fs = userCla.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            Field f = fs[i];
            f.setAccessible(true); // 设置些属性是可以访问的
            Object val = new Object();
            try {
                val = f.get(obj);
                // 得到此属性的值
                map.put(f.getName(), val);// 设置键值
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return map;
    }

}