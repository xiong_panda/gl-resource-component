package com.gl.biz.city.service.impl;

import com.gl.biz.city.service.IUserSearchDalService;
import com.gl.biz.city.service.IUserSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserSearchServiceImpl implements IUserSearchService {

    @Autowired
    IUserSearchDalService userSearchServiceDal;

    /**
     * 用户搜索记录
     * @param list
     */
    @Override
    public void saveUserSearch(List<Map> list) {
        userSearchServiceDal.saveUserSearch(list);
    }
}
