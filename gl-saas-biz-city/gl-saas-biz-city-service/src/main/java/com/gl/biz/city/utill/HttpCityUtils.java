package com.gl.biz.city.utill;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.constant.HttpContants;
import com.gl.biz.city.dto.PlatformResult;
import com.gl.biz.city.utill.sign.Md5Utils;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http请求
 *
 * @author xiehong
 */
@Slf4j
public class HttpCityUtils extends HttpClientUtils {

	private static final Logger log = LoggerFactory.getLogger(HttpClientUtils.class);

	public static Map<String, Object> exec(String url, Map<String, String> reqBean, String token, String prkey) {
		reqBean.put("sign_method", HttpContants.SIGN_METHOD);
		reqBean.put("format", HttpContants.FORMAT);
		reqBean.put("version", HttpContants.VERSION);
		reqBean.put("timestamp", currentTimeMillis());
		//对数据进行签名
		String sign = Md5Utils.signMd5(reqBean, prkey);
		reqBean.put("sign", sign);
		log.info("请求数据：{}", reqBean);
		String json = doJsonPost(url, reqBean, "UTF-8");
		log.info("请求结果：{}", json);
		return JSON.parseObject(json, Map.class);
	}

	public static CityOutResult execCityResult(String url, Map<String, String> reqBean, String token, String prkey) throws Exception {
		reqBean.put("sign_method", HttpContants.SIGN_METHOD);
		reqBean.put("format", HttpContants.FORMAT);
		reqBean.put("version", HttpContants.VERSION);
		reqBean.put("timestamp", currentTimeMillis());
		//对数据进行签名
		String sign = Md5Utils.signMd5(reqBean, prkey);
		reqBean.put("sign", sign);
		log.info("请求数据：{}", reqBean);
		log.info(JSON.toJSONString(reqBean));
		//String json = doJsonPost(url, reqBean, "UTF-8");
		String json = HttpUtils.doPostByJSON(url, reqBean, "UTF-8");
		log.info("远程接口返回结果数据:" + json);
		return JSON.parseObject(json, PlatformResult.class).getData();
	}


	public static String currentTimeMillis() {
		log.info("请求时间：{}", System.currentTimeMillis());
		long timeMillis = System.currentTimeMillis() - 5000;
		log.info("请求时间戳：{}", timeMillis);
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timeMillis));
	}

}
