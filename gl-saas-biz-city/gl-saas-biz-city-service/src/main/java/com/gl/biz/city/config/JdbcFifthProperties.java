package com.gl.biz.city.config;

import com.gl.common.MybatisMainConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


/**
 * 主数据源配置
 */
@Component
@ConfigurationProperties(prefix = "datasource.fifth")
@Import(value = {
        MybatisMainConfig.class
})
@PropertySource(value={"classpath:META-INF/app.properties"},encoding="UTF-8")
public class JdbcFifthProperties {
	
	/** 基本项配置 **/
	private String driverClassName;
	private String url ;
	private String username  ;
	private String password;
	
	/** 数据源配置  **/
	/** 初始化大小 **/
	private int initialSize=2;
	/** 最小 **/
	private int minIdle = 2;
	/** 最大 **/
	private int maxActive =20;
	/** 配置获取连接等待超时的时间 **/
	private long maxWait =60000l;
	/** 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒 **/
	private long timeBetweenEvictionRunsMillis =60000l;
	/** 配置一个连接在池中最小生存的时间，单位是毫秒 **/
	private long minEvictableIdleTimeMillis =300000;
	private String validationQuery ="SELECT 1 FROM DUAL";
	private boolean testWhileIdle =true;
	private boolean testOnBorrow = false;
	private boolean testOnReturn = false;
	/** 超时限制是否回收 **/
	private boolean removeAbandoned = true;
	/** 超时时间；单位为秒。1800秒=30分钟 **/
	private int removeAbandonedTimeout =900;
	/** 打开PSCache，并且指定每个连接上PSCache的大小 **/
	private boolean poolPreparedStatements =false;
	/** 打开PSCache，并且指定每个连接上PSCache的大小 **/
	private int maxPoolPreparedStatementPerConnectionSize =0;
	/** 关闭abanded连接时输出错误日志 **/
	private boolean logAbandoned =false;
	/** 扩展插件 **/
	//private String filters="stat,wall,log4j";
	
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getInitialSize() {
		return initialSize;
	}
	public void setInitialSize(int initialSize) {
		this.initialSize = initialSize;
	}
	public int getMinIdle() {
		return minIdle;
	}
	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}
	public int getMaxActive() {
		return maxActive;
	}
	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}
	public long getMaxWait() {
		return maxWait;
	}
	public void setMaxWait(long maxWait) {
		this.maxWait = maxWait;
	}
	public long getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}
	public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
	}
	public long getMinEvictableIdleTimeMillis() {
		return minEvictableIdleTimeMillis;
	}
	public void setMinEvictableIdleTimeMillis(long minEvictableIdleTimeMillis) {
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
	}
	public String getValidationQuery() {
		return validationQuery;
	}
	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}
	public boolean isTestWhileIdle() {
		return testWhileIdle;
	}
	public void setTestWhileIdle(boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}
	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}
	public void setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}
	public boolean isTestOnReturn() {
		return testOnReturn;
	}
	public void setTestOnReturn(boolean testOnReturn) {
		this.testOnReturn = testOnReturn;
	}
	public boolean isRemoveAbandoned() {
		return removeAbandoned;
	}
	public void setRemoveAbandoned(boolean removeAbandoned) {
		this.removeAbandoned = removeAbandoned;
	}
	public int getRemoveAbandonedTimeout() {
		return removeAbandonedTimeout;
	}
	public void setRemoveAbandonedTimeout(int removeAbandonedTimeout) {
		this.removeAbandonedTimeout = removeAbandonedTimeout;
	}
	public boolean isPoolPreparedStatements() {
		return poolPreparedStatements;
	}
	public void setPoolPreparedStatements(boolean poolPreparedStatements) {
		this.poolPreparedStatements = poolPreparedStatements;
	}
	public int getMaxPoolPreparedStatementPerConnectionSize() {
		return maxPoolPreparedStatementPerConnectionSize;
	}
	public void setMaxPoolPreparedStatementPerConnectionSize(int maxPoolPreparedStatementPerConnectionSize) {
		this.maxPoolPreparedStatementPerConnectionSize = maxPoolPreparedStatementPerConnectionSize;
	}
	public boolean isLogAbandoned() {
		return logAbandoned;
	}
	public void setLogAbandoned(boolean logAbandoned) {
		this.logAbandoned = logAbandoned;
	}
	
	
}
