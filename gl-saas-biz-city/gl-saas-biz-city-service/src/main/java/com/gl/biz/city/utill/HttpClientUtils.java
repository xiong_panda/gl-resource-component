package com.gl.biz.city.utill;

import com.alibaba.fastjson.JSON;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.nio.charset.Charset;
import java.util.*;
import java.util.Map.Entry;


public class HttpClientUtils {
	
	 public static String doPost(String url,Map<String,String> map,String charset){  
	        HttpClient httpClient = null;  
	        HttpPost httpPost = null;  
	        String result = null;  
	        try{  
	            httpClient = new SSLClient();  
	            httpPost = new HttpPost(url);  
	            //设置参数  
	            List<NameValuePair> list = new ArrayList<NameValuePair>();  
	            Iterator iterator = map.entrySet().iterator();  
	            while(iterator.hasNext()){  
	                Entry<String,String> elem = (Entry<String, String>) iterator.next();  
	                list.add(new BasicNameValuePair(elem.getKey(),elem.getValue()));  
	            }  
	            if(list.size() > 0){  
	                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list,charset);  
	                httpPost.setEntity(entity);  
	            }  
	            HttpResponse response = httpClient.execute(httpPost);  
	            if(response != null){  
	                HttpEntity resEntity = response.getEntity();  
	                if(resEntity != null){  
	                    result = EntityUtils.toString(resEntity,charset);  
	                }  
	            }  
	        }catch(Exception ex){  
	            ex.printStackTrace();  
	        }  
	        return result;  
	    }
	 
	 public static String doJsonPost(String url,Map<String,String> map,String charset){  
	        HttpClient httpClient = null;  
	        HttpPost httpPost = null;  
	        String result = null;  
	        try{  
	            httpClient = new SSLClient();  
	            httpPost = new HttpPost(url);
	            System.out.println(JSON.toJSONString(map));
	            //设置参数  
	    		StringEntity reqEntity= new StringEntity(JSON.toJSONString(map), ContentType.APPLICATION_JSON);
	    		httpPost.setEntity(reqEntity);
	    		httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());
	    		//httpPost.addHeader("Authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJBVVRIX0NFTlRFUiIsInN1YiI6IjE4NjE1NzIxNjM0IiwiYXVkIjoiQ0xJRU5UIiwiZXhwIjoxNTI1OTE4OTU2NDg0LCJuYmYiOjE1MjU5MTc3NTY0ODQsImlhdCI6MTUyNTkxNzc1NjQ4NCwianRpIjoiZTE0YjE0YmEzYWY1NGM4ZThjZGVmMTFiYmM4ZDU3ZWFzM3lnIiwiZ3JhIjoxNTI1OTIxMzU2NDg0LCJ0eXAiOiJVU0VSIn0=.34be5030d2a29ecfd048f82b92965c52c43668f9947f4e691ec78d2440bd144f");
	            HttpResponse response = httpClient.execute(httpPost);  
	            if(response != null){  
	                HttpEntity resEntity = response.getEntity();  
	                if(resEntity != null){  
	                	BufferedHttpEntity bufferedEntity = new BufferedHttpEntity(resEntity);
	                	result = EntityUtils.toString(bufferedEntity, Charset.forName(charset));  
	                }
	                Header[] hearders = response.getAllHeaders();
	                if(hearders!=null){
	                	for(Header hear:hearders){
	                		if(hear.getName().equals("authorization")){
	                			System.out.println(hear.getValue());
	                		}
	                	}
	                }
	            }  
	        }catch(Exception ex){  
	            ex.printStackTrace();  
	        }  
	        return result;  
	    }
	 
	 public static void main(String[] args) {
		 Map<String,Object> params = new HashMap<String, Object>();
	    	params.put("method", "/sys/login");
	    	params.put("sign", "3F1D9B38213FD06035BCFF4A8CB057AB");
	    	params.put("format", "json");
	    	params.put("appkey", "00000012");
	    	params.put("bz_data", "{\"password\":\"111111\",\"mobile\":\"15577112526\",\"platformType\":\"00000012\"}");
	    	params.put("version", "v0.01");
	    	params.put("sign_method", "md5");
	    	params.put("timestamp", "2020-07-06 22:04:00");
	    	//{method=/sys/login, sign_method=md5, format=json, sign=4AF9F65625D8EC1013F8DB9749A1682A, appkey=00000012, bz_data={"password":"111111","mobile":"15577112526","platformType":"00000012"}, version=v0.01, timestamp=2020-07-06 18:41:06}
	    //	System.out.println(doJsonPost("http://part.autosaas.cn/open", params, "UTF-8"));
	    	//System.out.println(post("http://part.autosaas.cn/open", params, 2000, 200000));
	}

}
