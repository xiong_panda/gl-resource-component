package com.gl.biz.city.utill.sign;

import com.alibaba.fastjson.JSONObject;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.biz.city.dto.SearchDTO;
import com.gl.biz.city.utill.ApplyNumProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ConvertCommBeanUtil{

    @Autowired
    private ApplyNumProperties applyNumProperties;

    /**
     * @author QinYe
     * @date 2020/6/23 16:14
     * @description 获取签名
     */
    public String getSign(String method, String bzData,String time) {
        Map<String, String> map = new HashMap<>();
        map.put("method", method);
        map.put("sign", null);
        map.put("timestamp", time);
        map.put("appkey", applyNumProperties.getAppkey());
        map.put("sign_method", applyNumProperties.getSignMethod());
        map.put("format", applyNumProperties.getFormat());
        map.put("bz_data", bzData);
        map.put("version", applyNumProperties.getVersion());

        //String sign = Md5Utils.signShaHex(map, applyNumProperties.getSignKey());
        return Md5Utils.signMd5(map, applyNumProperties.getSignKey());
    }

    /**
     * @author QinYe
     * @date 2020/6/23 16:14
     * @description 构造万方所需参数
     */
    public CommBean ConvertCommBean(String method, String bzData,String time) {
        CommBean commBean = new CommBean();
        commBean.setMethod(method);
        commBean.setTimestamp(time);
        commBean.setAppkey(applyNumProperties.getAppkey());
        commBean.setSign_method(applyNumProperties.getSignMethod());
        commBean.setFormat(applyNumProperties.getFormat());
        commBean.setBz_data(bzData);
        commBean.setSign(getSign(method,bzData,time));
        commBean.setVersion(applyNumProperties.getVersion());
        return commBean;
    }

    /**
     * @author QinYe
     * @date 2020/6/23 16:15
     * @description 简略搜索参数
     */
    public CommBean getList(CityPage page,String code) {
        //万方接口参数构造
        SearchDTO dto = new SearchDTO();
        dto.setResourceCls(code);

        Map<String, Object> params = new HashMap<>();
        params = page.getParams();
        params.remove("id");
        params.remove("__pageSize__");
        params.remove("__pageNo__");
        Set<Map.Entry<String, Object>> set = params.entrySet();
        String filed = "";
        if (set.size() > 0) {
            if (set.size() > 1) {
                for (Map.Entry mapKey : set) {
                    if (!"".equals(mapKey.getValue())) {
                        filed += mapKey.getKey() + ":" + mapKey.getValue().toString() + "&";
                    }
                }
            } else {
                for (Map.Entry mapKey : set) {
                    if (!"".equals(mapKey.getValue())) {
                        dto.setKeyword(mapKey.getValue().toString());
                    }
                }
            }
        }
        dto.setField(filed);
        dto.setFrom(page.getPageNo() + "");

        String time=CityConstant.TIME_NOW;

        String sign = this.getSign(CityConstant.WANFANG_JIANLUE, JSONObject.toJSONString(dto),time);

        CommBean commBean = this.ConvertCommBean(CityConstant.WANFANG_JIANLUE, JSONObject.toJSONString(dto),time);

        return commBean;
    }

    /**
     * @author QinYe
     * @date 2020/6/23 16:15
     * @description 详情搜索参数
     */
    public CommBean getOne(CityPage page,String code) {
        //万方接口参数构造
        SearchDTO dto = new SearchDTO();
        dto.setResourceCls(code);
        dto.setField("id:"+page.getParams().get("id"));
        dto.setFrom(page.getPageNo() + "");

        String time=CityConstant.TIME_NOW;

        String sign = this.getSign(CityConstant.WANFANG_XAINGQING, JSONObject.toJSONString(dto),time);

        CommBean commBean = this.ConvertCommBean(CityConstant.WANFANG_XAINGQING, JSONObject.toJSONString(dto),time);
        return commBean;
    }
}
