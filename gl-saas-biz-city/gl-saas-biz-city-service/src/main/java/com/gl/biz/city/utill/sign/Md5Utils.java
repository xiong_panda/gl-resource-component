package com.gl.biz.city.utill.sign;

import java.security.MessageDigest;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import com.gl.basis.common.util.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;

import lombok.extern.slf4j.Slf4j;



/**
 * 加密  解密工具
 *
 */
@Slf4j
public class Md5Utils {
	private static final String hexDigits[] = { "0", "1", "2", "3", "4", "5",
	        "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

	/**
	 * 对STR  MD5加密
	 * @param str 需要加密的str
	 * @return 加密后的数据
	 */
	public static String encode(String str){
		try {
			if(!StringUtils.isEmpty(str)){
				MessageDigest md5 = MessageDigest.getInstance("MD5");
				byte[] encodeBase64 = Base64.encodeBase64(md5.digest(str.getBytes("utf-8")));
				String encodeStr = new String(encodeBase64);
				return encodeStr;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("数据加密失败");
		}
		return null;
	}
	
	/**
	 * 对str 进行16位MD5加密
	 * @param str
	 * @return
	 */
	public static String encode16(String str) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
            System.out.println("MD5(" + str + ",32) = " + result);
            System.out.println("MD5(" + str + ",16) = " + buf.toString().substring(8, 24));
        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }
	
	/**
	 * 对str 进行32位MD5加密
	 * @param str
	 * @return
	 */
	public static String encode32(String str) {
		MessageDigest md5 = null;
		StringBuffer buf = new StringBuffer("");  
		try {
			md5 = MessageDigest.getInstance("MD5");
			md5.update((str).getBytes("UTF-8"));  
			byte b[] = md5.digest();  
			int i;  
			for(int offset=0; offset<b.length; offset++){  
			    i = b[offset];  
			    if(i<0){  
			        i+=256;  
			    }  
			    if(i<16){  
			        buf.append("0");  
			    }  
			    buf.append(Integer.toHexString(i));  
			}  
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buf.toString().toUpperCase();  
	}
	
	private static String byteArrayToHexString(byte b[]) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++)
            resultSb.append(byteToHexString(b[i]));

        return resultSb.toString();
    }

    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0)
            n += 256;
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * md5散列签名
     * @param origin
     * @param charsetname
     * @return
     */
    public static String hash(String origin, String charsetname) {
        String resultString = null;
        try {
            resultString = new String(origin);
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (charsetname == null || "".equals(charsetname))
                resultString = byteArrayToHexString(md.digest(resultString.getBytes()));
            else
                resultString = byteArrayToHexString(md.digest(resultString.getBytes(charsetname)));
        } catch (Exception exception) {
        }
        return resultString;
    }
    
    /**
     * md5签名
     * @param data 参数md5(prkey+key=value&key=value....+prkey)
     * @param prkey 签名key 
     * @return
     */
    public static String signMd5(Map<String, String> data ,String prkey ) {
    	String forSign = buildWaitingForSign(data,prkey);
    	String md5Hex = DigestUtils.md5Hex(forSign);
    	return md5Hex.toUpperCase();
    }
    /**
     * sha1签名
     * @param data 参数md5(prkey+key=value&key=value....+prkey)
     * @param key 签名key
     * @return
     */
    public static String signMd5Hex(Map<String, String> data ,String key) {
    	String forSign = buildWaitingForSign(data,key);
    	String sha1Hex = DigestUtils.md5Hex(forSign);    	
    	return sha1Hex.toUpperCase(); 
    }
    /**
	 * 生成待签字符串
	 *
	 * @param data 原始map数据
	 * @return 待签字符串
	 */
	public static String buildWaitingForSign(Map<String, String> data,String key) {
		if (data == null || data.size() == 0) {
			throw new IllegalArgumentException("请求数据不能为空");
		}
		String waitingForSign = null;
		Map<String, String> sortedMap = new TreeMap<>(data);
		// 如果sign参数存在,去除sign参数,不参与签名
		if (sortedMap.containsKey("sign")) {
			sortedMap.remove("sign");
		}
		StringBuilder stringToSign = new StringBuilder();
		stringToSign.append(key);
		for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
			if (entry.getValue() != null) {
				stringToSign.append(entry.getKey()).append("=").append(JSON.toJSONString(entry.getValue())).append("&");
			}
		}
		stringToSign.deleteCharAt(stringToSign.length() - 1);
		stringToSign.append(key);
		waitingForSign = stringToSign.toString();
		return waitingForSign;
	}
    
	public static void main(String[] args) {
		String h="123456"+"adb";
		System.out.println(encode32(h));
		System.out.println(signMd5Hex(Collections.singletonMap("ss", "as"),"UTF-8"));
	}
}
