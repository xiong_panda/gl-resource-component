package com.gl.biz.city.config;


import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@MapperScan(
        basePackages = "com.gl.biz.city.mapper.standard",
        sqlSessionFactoryRef = "sqlSessionFactory4"
)
@EnableConfigurationProperties(JdbcFourthProperties.class)
@EnableTransactionManagement
public class StandardDataSourceConfig {

    private static Logger logger = LoggerFactory.getLogger(StandardDataSourceConfig.class);

    @Autowired
    private JdbcFourthProperties jdbcFourthProperties;

    /**
     *	 创建主数据源
     *
     * @return
     * @throws SQLException
     */
    @Bean(value="dataSource4",initMethod = "init", destroyMethod = "close")
    public DataSource dataSource4() {
        logger.info("===>>>开始初始化subject数据源:[dataSource]");
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(jdbcFourthProperties.getDriverClassName());
        druidDataSource.setUrl(jdbcFourthProperties.getUrl());
        druidDataSource.setUsername(jdbcFourthProperties.getUsername());
        druidDataSource.setPassword(jdbcFourthProperties.getPassword());
        druidDataSource.setInitialSize(jdbcFourthProperties.getInitialSize());
        druidDataSource.setMinIdle(jdbcFourthProperties.getMinIdle());
        druidDataSource.setMaxActive(jdbcFourthProperties.getMaxActive());
        druidDataSource.setMaxWait(jdbcFourthProperties.getMaxWait());
        druidDataSource.setTimeBetweenEvictionRunsMillis(jdbcFourthProperties.getTimeBetweenEvictionRunsMillis());
        druidDataSource.setMinEvictableIdleTimeMillis(jdbcFourthProperties.getMinEvictableIdleTimeMillis());
        druidDataSource.setValidationQuery(jdbcFourthProperties.getValidationQuery());
        druidDataSource.setTestWhileIdle(jdbcFourthProperties.isTestWhileIdle());
        druidDataSource.setTestOnBorrow(jdbcFourthProperties.isTestOnBorrow());
        druidDataSource.setTestOnReturn(jdbcFourthProperties.isTestOnReturn());
        druidDataSource.setPoolPreparedStatements(jdbcFourthProperties.isPoolPreparedStatements());
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(
                jdbcFourthProperties.getMaxPoolPreparedStatementPerConnectionSize());
        druidDataSource.setRemoveAbandoned(jdbcFourthProperties.isRemoveAbandoned());
        druidDataSource.setRemoveAbandonedTimeout(jdbcFourthProperties.getRemoveAbandonedTimeout());
        druidDataSource.setLogAbandoned(jdbcFourthProperties.isLogAbandoned());

        return druidDataSource;
    }

    /**
     * 事务管理器
     *
     * @return
     */
    @Bean(name = "transactionManager4")
    public PlatformTransactionManager transactionManager4() {
        return new DataSourceTransactionManager(dataSource4());
    }



    /**
     * 配置sqlSessionFactory 在mybatis的配置文件中配置分页拦截器
     *
     * @return
     * @throws Exception
     */
    @Bean(name = "sqlSessionFactory4")
    public SqlSessionFactory sqlSessionFactoryBean4() {
        logger.info("===>>>开始初始化subject数据源mybatis的SqlSessionFactory");

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource4());
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setConfigLocation(resolver.getResource("classpath:config/mybatis/mybatis-config.xml"));
        try {
            sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:**/mapper/standard/*Mapper.xml"));
            return sqlSessionFactoryBean.getObject();
        } catch (Exception e) {
            logger.error("===>>>初始化subject数据源mybatis的sqlSessionFactory失败:" + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * 配置SqlSessionTemplate
     *
     * @return
     */
    @Bean(name = "sqlSessionTemplate4")
    public SqlSessionTemplate sqlSessionTemplate4() {
        return new SqlSessionTemplate(sqlSessionFactoryBean4());
    }
}
