////package com.gl.biz.city.config;//package com.gl.wanfang.config;
//
//
//import com.alibaba.druid.pool.DruidDataSource;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.mybatis.spring.annotation.MapperScan;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//import org.springframework.core.io.support.ResourcePatternResolver;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.sql.DataSource;
//import java.sql.SQLException;
//
//@Configuration
//@MapperScan(
//        basePackages = "com.gl.wanfang.mapper.fifth",
//        sqlSessionFactoryRef = "sqlSessionFactory2"
//)
//@EnableConfigurationProperties(JdbcSecondProperties.class)
//@EnableTransactionManagement
//public class FifthDataSourceConfig {
//
//    private static Logger logger = LoggerFactory.getLogger(FifthDataSourceConfig.class);
//
//    @Autowired
//    private JdbcFifthProperties jdbcFifthProperties;
//
//    /**
//     * 创建主数据源
//     *
//     * @return
//     * @throws SQLException
//     */
//    @Bean(value="dataSource5",initMethod = "init", destroyMethod = "close")
//    public DataSource dataSource5() {
//        logger.info("===>>>开始初始化Dictonary数据源:[dataSource]");
//        DruidDataSource druidDataSource = new DruidDataSource();
//        druidDataSource.setDriverClassName(jdbcFifthProperties.getDriverClassName());
//        druidDataSource.setUrl(jdbcFifthProperties.getUrl());
//        druidDataSource.setUsername(jdbcFifthProperties.getUsername());
//        druidDataSource.setPassword(jdbcFifthProperties.getPassword());
//        druidDataSource.setInitialSize(jdbcFifthProperties.getInitialSize());
//        druidDataSource.setMinIdle(jdbcFifthProperties.getMinIdle());
//        druidDataSource.setMaxActive(jdbcFifthProperties.getMaxActive());
//        druidDataSource.setMaxWait(jdbcFifthProperties.getMaxWait());
//        druidDataSource.setTimeBetweenEvictionRunsMillis(jdbcFifthProperties.getTimeBetweenEvictionRunsMillis());
//        druidDataSource.setMinEvictableIdleTimeMillis(jdbcFifthProperties.getMinEvictableIdleTimeMillis());
//        druidDataSource.setValidationQuery(jdbcFifthProperties.getValidationQuery());
//        druidDataSource.setTestWhileIdle(jdbcFifthProperties.isTestWhileIdle());
//        druidDataSource.setTestOnBorrow(jdbcFifthProperties.isTestOnBorrow());
//        druidDataSource.setTestOnReturn(jdbcFifthProperties.isTestOnReturn());
//        druidDataSource.setPoolPreparedStatements(jdbcFifthProperties.isPoolPreparedStatements());
//        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(
//                jdbcFifthProperties.getMaxPoolPreparedStatementPerConnectionSize());
//        druidDataSource.setRemoveAbandoned(jdbcFifthProperties.isRemoveAbandoned());
//        druidDataSource.setRemoveAbandonedTimeout(jdbcFifthProperties.getRemoveAbandonedTimeout());
//        druidDataSource.setLogAbandoned(jdbcFifthProperties.isLogAbandoned());
//
//        return druidDataSource;
//    }
//
//    /**
//     * 事务管理器
//     *
//     * @return
//     */
//    @Bean(name = "transactionManager5")
//    public PlatformTransactionManager transactionManager5() {
//        return new DataSourceTransactionManager(dataSource5());
//    }
//
//
//
//    /**
//     * 配置sqlSessionFactory 在mybatis的配置文件中配置分页拦截器
//     *
//     * @return
//     * @throws Exception
//     */
//    @Bean(name = "sqlSessionFactory5")
//    public SqlSessionFactory sqlSessionFactoryBean5() {
//        logger.info("===>>>开始初始化Dictonary数据源mybatis的SqlSessionFactory");
//
//        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
//        sqlSessionFactoryBean.setDataSource(dataSource5());
//        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//        sqlSessionFactoryBean.setConfigLocation(resolver.getResource("classpath:config/mybatis/mybatis-config.xml"));
//        try {
//            sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:/mapper/*/*.xml"));
//            return sqlSessionFactoryBean.getObject();
//        } catch (Exception e) {
//            logger.error("===>>>初始化Dictonary数据源mybatis的sqlSessionFactory失败:" + e.getMessage());
//            e.printStackTrace();
//            throw new RuntimeException(e);
//        }
//    }
//
//    /**
//     * 配置SqlSessionTemplate
//     *
//     * @return
//     */
//    @Bean(name = "sqlSessionTemplate5")
//    public SqlSessionTemplate sqlSessionTemplate5() {
//        return new SqlSessionTemplate(sqlSessionFactoryBean5());
//    }
//}
