package com.gl.biz.city.service.impl;

import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.service.ISysCodeDalService;
import com.gl.biz.city.out.dto.SysCodeOutDto;
import com.gl.biz.city.service.ISysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 	 	字典信息获取
 * @author Administrator
 *
 */

/**
 * @author admin
 *
 */
@Service
public class SysCodeServiceImpl implements ISysCodeService {

	@Autowired
	private ISysCodeDalService sysCodeDalService;

	@Override
	public SysCodeOutDto translateCode(Map<String, Object> params) {
		return ConverBeanUtils.doToDto(sysCodeDalService.translateCode(params),SysCodeOutDto.class);
	}

}
