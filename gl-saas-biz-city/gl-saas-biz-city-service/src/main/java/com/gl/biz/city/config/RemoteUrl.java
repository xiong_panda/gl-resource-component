package com.gl.biz.city.config;

import com.gl.basis.common.util.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "datasync.remote")
public class RemoteUrl {
    private String ip;
    private String port;

    public String getUrl() throws Exception {
        if (StringUtils.isNotEmpty(ip) && StringUtils.isNotEmpty(port)) {
            return "http://"+ip+":"+port+"/dataSync" + "/dataPush";
        }
        throw new Exception("远程url为空");
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
