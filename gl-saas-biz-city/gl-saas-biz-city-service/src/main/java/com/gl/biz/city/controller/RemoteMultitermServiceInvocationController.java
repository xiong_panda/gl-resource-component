package com.gl.biz.city.controller;

import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.config.UserClickLog;
import com.gl.biz.city.config.UserSearchLog;
import com.gl.biz.city.dto.SearchDTO;
import com.gl.biz.city.service.MultitermServiceInvocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "remote/data")
@Api(value = "远程调用平台综合搜索接口", tags = {"远程调用平台综合搜索接口"})
@Slf4j
public class RemoteMultitermServiceInvocationController {
    private static final Logger log = LoggerFactory.getLogger(MultitermServiceInvocationService.class);


    @Autowired
    MultitermServiceInvocationService serviceInvocationService;


    @ApiOperation(value = "综合服务调用-批量详情")
    @PostMapping("/mutiDataSearch")
    @ResponseBody
    @UserClickLog
    @UserSearchLog
    public CityOutResult mutiDataSearch(@RequestBody SearchDTO dto) throws Exception {
        return serviceInvocationService.mutiDataSearch(dto);
    }


    /**
     * 万方数据接口、  创新助手
     *
     * @param '万方数据'
     * @return 列表
     */
    @ApiOperation(value = "综合服务调用-详情")
    @PostMapping("/mutiDataIdsSearch")
    @ResponseBody
//    @UserClickLog
//    @UserSearchLog
    public CityOutResult mutiDataIdsSearch(@RequestBody SearchDTO dto) throws Exception {

        return serviceInvocationService.mutiDataIdsSearch(dto);
    }

    /**
     * 万方数据接口、  创新助手
     *
     * @param '万方数据'
     * @return 列表
     */
    @ApiOperation(value = "综合服务调用-简略")
    @PostMapping("/mutiDataInfoSearch")
    @ResponseBody
    @UserClickLog
    @UserSearchLog
    public CityOutResult mutiDataInfoSearch(@RequestBody SearchDTO dto) throws Exception {
        return serviceInvocationService.mutiDataInfoSearch(dto);
    }
}
