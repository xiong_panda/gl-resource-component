package com.gl.biz.city.service;

import java.util.List;
import java.util.Map;

public interface IUserSearchService {

    /**
     * 用户搜索记录
     * @param list
     */
    public void saveUserSearch(List<Map> list);

}
