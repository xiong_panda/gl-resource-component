package com.gl.biz.city.controller;

import com.gl.biz.city.service.DataSyncService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(value = "运维数据同步")
@RequestMapping("/dataSync")
public class DataSyncApi {
    @Autowired
    private DataSyncService dataSyncService;

    @PostMapping("/dataPull")
    @ApiOperation(value = "远程数据拉取")
    public String dataPull() throws Exception {
        return dataSyncService.dataPull();
    }

//    @PostMapping("/dataInsert")
//    @ApiOperation(value = "远程数据拉取")
//    public String dataPull() throws Exception {
//        return dataSyncService.dataPull();
//    }

    @GetMapping("/dataPush")
    @ApiOperation(value = "本地数据提取")
    @ResponseBody
    public Map<String, Object> dataPush() {
        return dataSyncService.dataPush();
    }
}
