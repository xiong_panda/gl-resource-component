package com.gl.biz.city.config;

import com.gl.biz.city.bo.HxWfServerSearchBO;
import com.gl.biz.city.service.IUserClickService;
import com.gl.biz.city.service.IUserSearchService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 定时执行任务
 * 用户搜索落库
 */
@Component
@Slf4j
public class UserPersistenceQuartZConfig extends QuartzJobBean {


    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IUserSearchService userSearchService;

    @Autowired
    private IUserClickService userClickService;


    @Override
    @Transactional
    protected void executeInternal(JobExecutionContext jobExecutionContext) {

        try{
            List<Map> usrSearch = redisTemplate.opsForList().range("usrSearch", 0, -1);
            List<HxWfServerSearchBO> userClickvBO = redisTemplate.opsForList().range("userClick", 0, -1);
            if(usrSearch.size()>0) {
                Set set = new HashSet(usrSearch);
                usrSearch.clear();
                usrSearch.addAll(set);
                userSearchService.saveUserSearch(usrSearch);
                redisTemplate.delete("usrSearch");
            }
                if(userClickvBO.size()>0){
                Set clickSet=new HashSet(userClickvBO);
                userClickvBO.clear();
                userClickvBO.addAll(clickSet);
                userClickService.saveUserClick(userClickvBO);
                redisTemplate.delete("userClick");
            }
        }catch (Exception e){
            throw new RuntimeException("数据落库异常:"+e);
        }
    }
}