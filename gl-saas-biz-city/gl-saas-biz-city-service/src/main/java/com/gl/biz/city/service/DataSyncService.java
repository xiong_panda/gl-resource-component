package com.gl.biz.city.service;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.util.IdWorker;
import com.gl.biz.city.config.RemoteUrl;
import com.gl.biz.city.mapper.core.RemoteDataSyncMapper;
import com.gl.biz.city.po.HxwfServerSearchPO;
import com.gl.biz.city.utill.HttpClientService;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;



@Service
public class DataSyncService {
    @Autowired
    private RemoteDataSyncMapper dataSyncMapper;
    @Autowired
    private RemoteUrl remoteUrl;

    private static final IdWorker idWorker = new IdWorker();

    private static final Logger log = LoggerFactory.getLogger(DataSyncService.class);

    public String dataPull() throws Exception {
        String resultJson = HttpClientService.sendGet(remoteUrl.getUrl() , null);

        Map<String, Object> resultMap = JSONObject.parseObject(resultJson, Map.class);

        if (!StringUtils.equals("200", resultMap.get("code").toString())) {
            if(StringUtils.equals("000", resultMap.get("code").toString())){
            log.error("远程调用失败");
            throw new Exception("远程调用失败");
            }

            if(StringUtils.equals("100", resultMap.get("code").toString())){
                log.info("无数据");
                return "拉取成功";
            }
        }

        List<HxwfServerSearchPO> dataList = JSONObject.parseArray(resultMap.get("data").toString(), HxwfServerSearchPO.class);

        for (HxwfServerSearchPO po : dataList) {
            po.setId(String.valueOf(idWorker.nextId()));
            po.setRemoteFlag("1");
        }

        dataSyncMapper.batchInsert(dataList);

        return "拉取成功";
    }

    public Map<String, Object> dataPush() {
        List<HxwfServerSearchPO> dataList;
        Map<String, Object> resultMap = Maps.newHashMap();
        String code = "200";

        try {
            dataList = dataSyncMapper.queryYesterday();
            if (CollectionUtils.isEmpty(dataList)) {
                resultMap.put("data", Collections.EMPTY_LIST);
                code = "100";
            } else {
                resultMap.put("data", dataList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put("data", Collections.EMPTY_LIST);
            code = "000";
            log.error("数据读取失败");
        }finally {
            resultMap.put("code", code);
        }
        return resultMap;
    }
}
