package com.gl.biz.city.service.impl;


import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.entity.CarPatentBom;
import com.gl.biz.city.out.dto.CarPatentBomOutDto;
import com.gl.biz.city.service.ICarPatentBomDalService;
import com.gl.biz.city.service.ICarPatentBomService;


/**
 * 汽车bom清单
 * @author admin
 *
 */
@Service
public class CarPatentBomServiceImpl implements ICarPatentBomService {

	@Autowired
	private ICarPatentBomDalService carPatentBomDalService;

	@Override
	public Page<CarPatentBomOutDto> findCarPatenBom() {
		//查詢所有bom數據
		List<CarPatentBom> list = carPatentBomDalService.getListByPage(Collections.singletonMap("", ""));
		//匹配搜索专利
		Page<CarPatentBomOutDto> page = new Page<CarPatentBomOutDto>();
		page.setResult(list);
		return page;
	}
	
	
	
}
