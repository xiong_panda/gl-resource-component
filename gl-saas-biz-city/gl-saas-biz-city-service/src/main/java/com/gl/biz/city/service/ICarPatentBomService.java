package com.gl.biz.city.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.out.dto.CarPatentBomOutDto;

public interface ICarPatentBomService {

	/**
	 * 	查询bom清单
	 * @return
	 */
	Page<CarPatentBomOutDto> findCarPatenBom();

}
