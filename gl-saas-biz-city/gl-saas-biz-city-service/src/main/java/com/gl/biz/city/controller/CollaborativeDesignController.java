package com.gl.biz.city.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.out.dto.CarPatentBomOutDto;
import com.gl.biz.city.service.ICarPatentBomService;


/**
 * 协同设计
 * @author admin
 *
 */
@RequestMapping("/collaborative/design")
@CrossOrigin
@RestController
public class CollaborativeDesignController {
	
	@Autowired
	private ICarPatentBomService carPatentBomService;
	
	//@RequiresPermissions("collaborativeDesign:findCarPatenBom")
    @ResponseBody
    @RequestMapping(value = "/findCarPatenBom", method = RequestMethod.POST)
	public Page<CarPatentBomOutDto> findCarPatenBom(){
		
    	Page<CarPatentBomOutDto> page = carPatentBomService.findCarPatenBom();
		return page;
	}

}
