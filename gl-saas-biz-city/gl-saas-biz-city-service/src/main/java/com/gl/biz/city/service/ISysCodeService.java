package com.gl.biz.city.service;


import com.gl.biz.city.out.dto.SysCodeOutDto;

import java.util.Map;

public interface ISysCodeService {

	/**
	 * @auther Qinye
	 * @Description 分页查询（用户数据采集使用）
	 * @date 2020/6/15 15:18
	 */
	SysCodeOutDto translateCode(Map<String, Object> params);
}
