package com.gl.biz.city;


import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//scanBasePackages = {"com.gl.biz.city.service", "com.gl.biz.city.controller",
//        "mapper.core"}
@EnableSwagger2Doc
@MapperScan(basePackages = "com.gl.biz.city.mapper.*")
//@EnableApolloConfig
public class CityApplication {

    public static void main(String[] args) {
        SpringApplication.run(CityApplication.class);
        System.out.println("-----------程序启动成功--------------");
    }
}
