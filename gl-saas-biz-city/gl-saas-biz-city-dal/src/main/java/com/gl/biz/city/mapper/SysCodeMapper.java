package com.gl.biz.city.mapper;

import com.gl.biz.city.entity.SysCode;
import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface SysCodeMapper extends MyMapper<SysCode> {

	List<SysCode> getList(Map<String, Object> params);

	/**
	 *	 根据类型获取码值
	 * @param singletonMap
	 * @return
	 */
	List<Map<String, Object>> findCooperationTypeCode(Map<String, String> singletonMap);

	SysCode getOneById(String id);

	void update(SysCode sysCode);

	SysCode validteCode(SysCode sysCode);

	SysCode validteName(SysCode sysCode);

	List<SysCode> selectByType(Map<String, Object> params);

	/**
	 * 根据码值名获取码值信息
	 * @param name
	 * @return
	 */
	SysCode getCodeByName(@Param("name") String name);
	/**
	 * 企业端获取平台标志
	 * @param serviceType
	 * @return
	 */
	List<Map<String, Object>> findPlatformType(@Param("serviceType") String serviceType);

	String findPlatformTypeCode(@Param("platformType") String platformType);
}
