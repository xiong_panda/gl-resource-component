package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ScientificOrgPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteScientificOrgMapper {
    //核心资源库
    int SObatchInsertToCore(@Param("list") List<ScientificOrgPO> list);

    List<String> SOselectField();

    List<ScientificOrgPO> SOselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);
}
