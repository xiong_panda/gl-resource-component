package com.gl.biz.city.service;

import com.gl.biz.city.dto.IndustryFieldDTO;

import java.util.List;
import java.util.Map;

/**
 * xiaweijie
 * 2020-3-25 18:53
 * 解析跟用户相关的接口
 */
public interface IUserAnalysisDalService {
    /**
     * 从登陆信息中获取用户对应的行业领域
     */
    public IndustryFieldDTO getIndustryFiledByLogin(String userId);

    /**
     * 所有的领域行业树数据
     * @return
     */
    List<Map<String, Object>> getIndustryFieldAll();
}
