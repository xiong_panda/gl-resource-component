package com.gl.biz.city.service;

import java.util.List;
import java.util.Map;

public interface IUserSearchDalService {

    /**
     * 保存用户搜索记录
     * @param list
     */
    public void saveUserSearch(List<Map> list);
}
