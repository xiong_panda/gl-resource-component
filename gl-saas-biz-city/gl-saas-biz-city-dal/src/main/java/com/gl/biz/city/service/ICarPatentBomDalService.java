package com.gl.biz.city.service;

import java.util.List;
import java.util.Map;

import com.gl.biz.city.entity.CarPatentBom;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ICarPatentBomDalService  {

List<CarPatentBom> getListByPage(Map<String,Object> params);

}
