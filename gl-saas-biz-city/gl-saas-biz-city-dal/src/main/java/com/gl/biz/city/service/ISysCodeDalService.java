package com.gl.biz.city.service;


import com.gl.biz.city.entity.SysCode;

import java.util.Map;

public interface ISysCodeDalService {
	SysCode translateCode(Map<String, Object> params);
}
