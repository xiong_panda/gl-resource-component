package com.gl.biz.city.service.impl;

import com.gl.biz.city.entity.SysCode;
import com.gl.biz.city.mapper.SysCodeMapper;
import com.gl.biz.city.service.ISysCodeDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional(readOnly = true)
@Service
public class SysCodeDalServiceImpl implements ISysCodeDalService {

	@Autowired
	private SysCodeMapper sysCodeMapper;
	
	@Override
	public SysCode translateCode(Map<String, Object> params) {
		return sysCodeMapper.getOne(params);
	}


}
