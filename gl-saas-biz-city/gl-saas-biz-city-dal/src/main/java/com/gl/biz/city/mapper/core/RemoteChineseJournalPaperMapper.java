package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ChineseJournalPapersPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteChineseJournalPaperMapper {
    //核心资源库
    int CJPbatchInsertToCore(@Param("list") List<ChineseJournalPapersPO> list);

    List<String> CJPselectField();

    List<ChineseJournalPapersPO> CJPselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);


}
