package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ExpertsLibraryPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteExpertsLibraryMapper {
    //核心资源库
    int ELPbatchInsertToCore(@Param("list") List<ExpertsLibraryPO> list);

    List<String> ELselectField();

    List<ExpertsLibraryPO> ELselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);

}
