package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ChinesePaperOAPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteChinesePaperOAMapper {
    //核心资源库
    int CPOAbatchInsertToCore(@Param("list") List<ChinesePaperOAPO> list);

    List<String> CPOAselectField();

    List<ChinesePaperOAPO> CPOAselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);

}
