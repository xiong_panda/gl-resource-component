package com.gl.biz.city.mapper.core;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.LawsRegulationsPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteLawsRegulationsMapper {
    //核心资源库
    int LRbatchInsertToCore(@Param("list") List<LawsRegulationsPO> list);

    List<String> LRselectField();
    List<LawsRegulationsPO> LRselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);
}
