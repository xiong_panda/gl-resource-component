package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.EnterpriseProductDatabasePO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteEnterpriseProductDatabaseMapper {
    //核心资源库
    int EPDbatchInsertToCore(@Param("list") List<EnterpriseProductDatabasePO> list);

    List<String> EPDselectField();

    List<EnterpriseProductDatabasePO> EPDselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);
}
