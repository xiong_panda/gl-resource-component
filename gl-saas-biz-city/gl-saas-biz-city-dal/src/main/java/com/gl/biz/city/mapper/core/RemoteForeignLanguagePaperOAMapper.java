package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ForeignLanguagePaperOAPO;

import java.util.List;
import java.util.Map;

@Repository("foreignLanguagePaperOAMapper")
@Mapper
public interface RemoteForeignLanguagePaperOAMapper {
    //核心资源库
    int FLPOAbatchInsertToCore(@Param("list") List<ForeignLanguagePaperOAPO> list);

    List<String> FLPOAselectField();

    List<ForeignLanguagePaperOAPO> FLPOAselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);


}
