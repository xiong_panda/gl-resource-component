package com.gl.biz.city.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

/**
 * 注意:注解只能加在属性字段上才会生效! 汽车专利清单
 * 
 * @author code_generator
 */
@Table(name = "CAR_PATENT_BOM")
public class CarPatentBom implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@GeneratedValue(generator = "JDBC")
	@ApiModelProperty(value = "主键")
	private String fPkid;

	/** 配件名称 **/
	@Column(name = "ItemInfo_ItemName")
	@ApiModelProperty(value = "配件名称")
	private String iteminfoItemname;

	/** 配件规格 **/
	@Column(name = "specs")
	@ApiModelProperty(value = "配件规格")
	private String specs;

	/** 车品牌 **/
	@Column(name = "car_brand")
	@ApiModelProperty(value = "车品牌")
	private String carBrand;

	/** 车型 **/
	@Column(name = "motorcycle_type")
	@ApiModelProperty(value = "车型")
	private String motorcycleType;

	/** 专利所属单位 **/
	@Column(name = "patent_owned_units")
	@ApiModelProperty(value = "专利所属单位")
	private String patentOwnedUnits;
	
	@Column(name="is_make")
    @ApiModelProperty(value = "是否自制")
	private String isMake ; 
	
	/** 供应商 **/
	@Column(name = "supplier")
	@ApiModelProperty(value = "供应商")
	private String supplier;

	/** 专利数 **/
	@Column(name = "patents_num")
	@ApiModelProperty(value = "专利数")
	private Integer patentsNum;

	/** 启用状态：1：禁用，0：启用 **/
	@Column(name = "f_enable")
	@ApiModelProperty(value = "启用状态：1：禁用，0：启用")
	private String fEnable;

	/** 删除标记（1：已删除，0：正常） **/
	@Column(name = "f_isdelete")
	@ApiModelProperty(value = "删除标记（1：已删除，0：正常）")
	private String fIsdelete;

	/** 乐观锁 **/
	@Column(name = "revision")
	@ApiModelProperty(value = "乐观锁")
	private String revision;

	/** 版本号 **/
	@Column(name = "version")
	@ApiModelProperty(value = "版本号")
	private String version;

	/** 创建人姓名 **/
	@Column(name = "f_inputname")
	@ApiModelProperty(value = "创建人姓名")
	private String fInputname;

	/** 创建人 **/
	@Column(name = "f_inputid")
	@ApiModelProperty(value = "创建人")
	private String fInputid;

	/** 创建时间 **/
	@Column(name = "f_inputtime")
	@ApiModelProperty(value = "创建时间")
	private Date fInputtime;

	/** 更新人姓名 **/
	@Column(name = "f_endname")
	@ApiModelProperty(value = "更新人姓名")
	private String fEndname;

	/** 更新人 **/
	@Column(name = "f_endid")
	@ApiModelProperty(value = "更新人")
	private String fEndid;

	/** 更新时间 **/
	@Column(name = "f_endtime")
	@ApiModelProperty(value = "更新时间")
	private Date fEndtime;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getIteminfoItemname() {
		return iteminfoItemname;
	}

	public void setIteminfoItemname(String iteminfoItemname) {
		this.iteminfoItemname = iteminfoItemname;
	}

	public String getSpecs() {
		return specs;
	}

	public void setSpecs(String specs) {
		this.specs = specs;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getMotorcycleType() {
		return motorcycleType;
	}

	public void setMotorcycleType(String motorcycleType) {
		this.motorcycleType = motorcycleType;
	}

	public String getPatentOwnedUnits() {
		return patentOwnedUnits;
	}

	public void setPatentOwnedUnits(String patentOwnedUnits) {
		this.patentOwnedUnits = patentOwnedUnits;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Integer getPatentsNum() {
		return patentsNum;
	}

	public void setPatentsNum(Integer patentsNum) {
		this.patentsNum = patentsNum;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getfInputname() {
		return fInputname;
	}

	public void setfInputname(String fInputname) {
		this.fInputname = fInputname;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndname() {
		return fEndname;
	}

	public void setfEndname(String fEndname) {
		this.fEndname = fEndname;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	}

}
