package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.InfoOrgPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public
interface RemoteInfoOrgMapper {
    //核心资源库
    int IObatchInsertToCore(@Param("list") List<InfoOrgPO> list);

    List<String> IOselectField();

    List<InfoOrgPO> IOselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);
}
