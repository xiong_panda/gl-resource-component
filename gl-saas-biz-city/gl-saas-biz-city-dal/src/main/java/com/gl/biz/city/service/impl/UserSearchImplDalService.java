package com.gl.biz.city.service.impl;

import com.gl.biz.city.mapper.IUserSearchMapper;
import com.gl.biz.city.service.IUserSearchDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserSearchImplDalService implements IUserSearchDalService {

    @Autowired
    IUserSearchMapper userSearchMapper;

    /**
     * 保存用户搜索记录
     * @param list
     */
    public void saveUserSearch(List<Map> list){
        userSearchMapper.saveUserSearch(list);
    }
}
