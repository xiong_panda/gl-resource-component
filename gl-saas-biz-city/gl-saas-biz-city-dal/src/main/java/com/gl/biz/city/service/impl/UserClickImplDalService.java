package com.gl.biz.city.service.impl;

import com.gl.biz.city.entity.HxWfServerSearch;
import com.gl.biz.city.mapper.HxWfServerSearchMapper;
import com.gl.biz.city.service.IUserClickDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserClickImplDalService implements IUserClickDalService {

    @Autowired
    private HxWfServerSearchMapper hxWfServerSearchMapper;

    @Override
    public void saveUserClick(List<HxWfServerSearch> list) {
        hxWfServerSearchMapper.saveUserWFUserRecord(list);
    }

    @Override
    public List<HxWfServerSearch> getListByCollectTime(Map<String, Object> params) {
        return hxWfServerSearchMapper.getListByCollectTime(params);
    }
}
