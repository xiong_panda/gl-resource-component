package com.gl.biz.city.mapper;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

import com.gl.biz.city.entity.CarPatentBom;
import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface CarPatentBomMapper extends MyMapper<CarPatentBom>{

	List<CarPatentBom> getList(Map<String,Object> params);

}
