package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.AuthorLibraryPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteAuthorLibraryMapper {
    //核心资源库
    int ALbatchInsertToCore(@Param("list") List<AuthorLibraryPO> list);

    List<String> ALselectField();

    List<AuthorLibraryPO> ALselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);


}
