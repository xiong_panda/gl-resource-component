package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ForeignPeriodicalPaperPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteForeignPeriodicalPaperMapper {
    //核心资源库
    int FPPbatchInsertToCore(@Param("list") List<ForeignPeriodicalPaperPO> list);

    List<String> FPPselectField();

    List<ForeignPeriodicalPaperPO> FPPselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);


}
