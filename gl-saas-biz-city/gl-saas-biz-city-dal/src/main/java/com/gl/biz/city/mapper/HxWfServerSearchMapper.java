package com.gl.biz.city.mapper;

import com.gl.biz.city.entity.HxWfServerSearch;
import com.gl.biz.city.entity.HxWfServerSearchOriginal;
import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface HxWfServerSearchMapper extends MyMapper<HxWfServerSearch> {

	List<HxWfServerSearch> getList(Map<String, Object> params);

	List<HxWfServerSearchOriginal> getOriginalListByParame(Map<String, Object> params);

	void saveUserWFUseRecord(Map<String, Object> params);

	void saveUserWFUserRecord(@Param("list") List<HxWfServerSearch> list);

	List<HxWfServerSearch> getListByCollectTime(Map<String, Object> params);
}
