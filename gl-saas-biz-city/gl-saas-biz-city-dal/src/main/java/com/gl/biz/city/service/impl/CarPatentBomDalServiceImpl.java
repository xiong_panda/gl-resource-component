package com.gl.biz.city.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.biz.city.entity.CarPatentBom;
import com.gl.biz.city.mapper.CarPatentBomMapper;
import com.gl.biz.city.service.ICarPatentBomDalService;
/**
 * 
 * @author admin
 *
 */
@Service
public class CarPatentBomDalServiceImpl implements ICarPatentBomDalService{

	@Autowired
	private CarPatentBomMapper carPatentBomMapper;
	
	@Override
	public List<CarPatentBom> getListByPage(Map<String, Object> params) {
		return carPatentBomMapper.getList(params);
	}

}
