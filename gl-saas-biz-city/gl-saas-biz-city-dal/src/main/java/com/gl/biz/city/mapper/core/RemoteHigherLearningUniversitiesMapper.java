package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.HigherLearningUniversitiesPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteHigherLearningUniversitiesMapper {
    //核心资源库
    int HLUbatchInsertToCore(@Param("list") List<HigherLearningUniversitiesPO> list);

    List<String> HLUselectField();
    List<HigherLearningUniversitiesPO> HLUselectList(Map<String, Object> map);


    List<String> queryIds(@Param("ids") List<String> ids);
}
