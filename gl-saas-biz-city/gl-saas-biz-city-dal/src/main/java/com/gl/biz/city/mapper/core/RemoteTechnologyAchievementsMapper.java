package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.TechnologyAchievementsPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteTechnologyAchievementsMapper {
    //核心资源库
    int TAbatchInsertToCore(@Param("list") List<TechnologyAchievementsPO> list);

    List<String> TAselectField();

    List<TechnologyAchievementsPO> TAselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);
}
