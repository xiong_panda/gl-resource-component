package com.gl.biz.city.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 
 * @author code_generator
 */
@Data
public class HxWfServerSearchOriginal implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键id **/
	@ApiModelProperty(value = "主键id")
	private String id ;


	/** 用户名称 **/
    @ApiModelProperty(value = "用户名称")
	private String user_name ;

	/** 用户领域 **/
    @ApiModelProperty(value = "用户领域")
	private String user_field ;

	/** 用户行业 **/
    @ApiModelProperty(value = "用户行业")
	private String user_industry ;

	/** 用户类型 **/
    @ApiModelProperty(value = "用户类型")
	private String user_type ;

	/** 用户地区 **/
    @ApiModelProperty(value = "用户地区")
	private String user_area ;

	/** 平台用户主键标识 **/
    @ApiModelProperty(value = "平台用户主键标识")
	private String user_id ;

	/** 搜索关键字 **/
    @ApiModelProperty(value = "搜索关键字")
	private String search_keyword ;

	/** 服务信息 **/
    @ApiModelProperty(value = "服务信息")
	private String service_name ;

	/** 服务来源 **/
    @ApiModelProperty(value = "服务来源")
	private String service_origin ;

	/** 资源名称 **/
    @ApiModelProperty(value = "资源名称")
	private String sources_name ;

	/** 资源来源 **/
    @ApiModelProperty(value = "资源来源")
	private String sources_origin ;

	/** 资源标识 **/
    @ApiModelProperty(value = "资源标识")
	private String sources_id ;

	/** 来源为服务、资源标识(0服务，1资源) **/
    @ApiModelProperty(value = "来源为服务、资源标识(0服务，1资源)")
	private String flag ; 

	/** 使用时间 **/
    @ApiModelProperty(value = "使用时间")
	private String use_time ;

	/** 采集来源（哈长、万方等城市群） **/
    @ApiModelProperty(value = "采集来源（哈长、万方等城市群）")
	private String collect_source ;

	/** 采集时间 **/
    @ApiModelProperty(value = "采集时间")
	private String collect_time ;

	/** 采集方式（0：调度平台采集、1：实时接口采集） **/
    @ApiModelProperty(value = "采集方式（0：调度平台采集、1：实时接口采集）")
	private String collect_type ;

	/**
	 * 用户行业id
	 */
	private String user_industry_id;

	/**
	 * 用户领域id
	 */
	private String user_field_id;

	/** 非数据库字段表名 **/
	@ApiModelProperty(value = "表名")
	private String tableName;

}
