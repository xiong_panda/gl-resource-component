package com.gl.biz.city.mapper;

import com.gl.biz.city.dto.IndustryFieldDTO;
import com.gl.common.mybatis.annotation.MapperPrimary;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@MapperPrimary
@Component
public interface IFieldIndustryMapper {

//    List<Map<String,Object>> getFieldIndustry();

    /**
     * 根据用户id获取领域行业信息
     * @param userId
     * @return
     */
    IndustryFieldDTO getIndustryFiledByUserId(@Param("userId") String userId);

    /**
     * 获取所有的领域行业信息
     * @return
     */
    List<Map<String, Object>> getIndustryFieldAll();
}
