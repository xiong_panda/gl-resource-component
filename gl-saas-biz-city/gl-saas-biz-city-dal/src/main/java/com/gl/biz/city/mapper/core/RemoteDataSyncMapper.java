package com.gl.biz.city.mapper.core;

import com.gl.biz.city.po.HxwfServerSearchPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface RemoteDataSyncMapper {
    List<HxwfServerSearchPO> queryYesterday();

    int batchInsert(@Param("list") List<HxwfServerSearchPO> list);
}
