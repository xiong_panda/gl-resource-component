package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.OverseasPatentPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteOverseasPatentMapper {
    //核心资源库
    int OPbatchInsertToCore(@Param("list") List<OverseasPatentPO> list);

    List<String> OPselectField();

    List<OverseasPatentPO> OPselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);
}
