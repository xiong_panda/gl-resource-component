package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ChinesePatentPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteChinesePatentMapper {
    //核心资源库
    int batchInsertToCore(@Param("list") List<ChinesePatentPO> list);

    List<String> CPselectField();


    List<ChinesePatentPO> CPselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);


}
