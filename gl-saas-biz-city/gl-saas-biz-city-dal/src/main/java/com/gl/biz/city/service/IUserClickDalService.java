package com.gl.biz.city.service;

import com.gl.biz.city.entity.HxWfServerSearch;

import java.util.List;
import java.util.Map;

public interface IUserClickDalService {

    /**
     * @auther Qinye
     * @Description 用户浏览记录
     * @date 2020/6/11 19:06
     */
    void saveUserClick(List<HxWfServerSearch> list);

    /**
     * @auther Qinye
     * @Description 用户访问数据采集提供
     * @date 2020/6/15 14:59
     */
    List<HxWfServerSearch> getListByCollectTime(Map<String, Object> params);
}
