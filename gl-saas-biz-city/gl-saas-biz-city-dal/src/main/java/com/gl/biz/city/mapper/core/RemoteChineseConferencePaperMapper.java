package com.gl.biz.city.mapper.core;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.gl.biz.city.po.ChineseConferencePaperPO;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface RemoteChineseConferencePaperMapper {
    //核心资源库
    int CCPbatchInsertToCore(@Param("list") List<ChineseConferencePaperPO> list);

    List<String> CCPselectField();

    List<ChineseConferencePaperPO> CCPselectList(Map<String, Object> map);

    List<String> queryIds(@Param("ids") List<String> ids);

}
