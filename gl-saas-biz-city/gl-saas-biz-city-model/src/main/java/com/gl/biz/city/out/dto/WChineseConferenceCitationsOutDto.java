package com.gl.biz.city.out.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文会议引文 
 * @author code_generator
 */
@Data
public class WChineseConferenceCitationsOutDto implements java.io.Serializable{

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private Integer id ; 

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String type ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String lwid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String doi ; 

	/** Lw_title **/
    @ApiModelProperty(value = "Lw_title")
	private String lwTitle ; 

	/** 作者 **/
    @ApiModelProperty(value = "作者")
	private String author ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String authorid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String tea ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String teaTeaAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String teaAnyname ; 

	/** Lw_QKID **/
    @ApiModelProperty(value = "Lw_QKID")
	private String lwQkid ; 

	/** Lw_Journal **/
    @ApiModelProperty(value = "Lw_Journal")
	private String lwJournal ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String lwJournalJoucnAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String joucnAnyname ; 

	/** Lw_Year **/
    @ApiModelProperty(value = "Lw_Year")
	private String lwYear ; 

	/** Lw_VOL **/
    @ApiModelProperty(value = "Lw_VOL")
	private String lwVol ; 

	/** Lw_Issue **/
    @ApiModelProperty(value = "Lw_Issue")
	private String lwIssue ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String degree ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String rn ; 

	/** 机构名称 **/
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 机构全文检索 **/
    @ApiModelProperty(value = "机构全文检索")
	private String orgFulltextSearch ; 

	/** 机构ID **/
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构类型 **/
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 机构层级ID **/
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/** 第一机构终级机构ID **/
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String zcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String tzcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String szcid ; 

	/** 中图分类(三级) **/
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ckey ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ekey ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String mn ; 

	/** 基金 **/
    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/
    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fundAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String status ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String patt ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String core ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywtype ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywdoi ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywtitle ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywauthor ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywauthorAnynameFulltext ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywauthorAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywauthorid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywtea ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywteaAnynameFulltext ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywteaAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywjouid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywjoucn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywjoucnAnynameFulltext ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywjoucnAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywyear ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywvol ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywper ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywdegree ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywrn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworg ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgAnynameFulltext ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgtype ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywzcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywtzcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywszcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywdzcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywckey ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywekey ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywmn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfund ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfundYwfundAnynameFulltext ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfundYwfundAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywpatt ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywstatus ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywcore ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywscore ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgstrucid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgprovince ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yworgcity ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfendorgid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywforgstrucid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfendorgprovince ;

	/**  **/
    @ApiModelProperty(value = "")
	private String ywforgprovince ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfendorgcity ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywforgcity ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywfendorgtype ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ywforgtype ; 

	/** 索引管理字段(无用) **/
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 


}
