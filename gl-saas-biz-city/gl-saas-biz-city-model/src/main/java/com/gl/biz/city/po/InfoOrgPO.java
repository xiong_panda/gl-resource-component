package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class InfoOrgPO {
    @FieldOrder(order = 1)
    private String OrderID;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String FullText;
    @FieldOrder(order = 4)
    private String ORG_Other;
    @FieldOrder(order = 5)
    private String Name_Anyname1;
    @FieldOrder(order = 6)
    private String Name_Anyname2;
    @FieldOrder(order = 7)
    private String Acname;
    @FieldOrder(order = 8)
    private String ORG_Name;
    @FieldOrder(order = 9)
    private String Simple_Name;
    @FieldOrder(order = 10)
    private String Other_Name;
    @FieldOrder(order = 11)
    private String Person;
    @FieldOrder(order = 12)
    private String Province;
    @FieldOrder(order = 13)
    private String City;
    @FieldOrder(order = 14)
    private String County;
    @FieldOrder(order = 15)
    private String Region;
    @FieldOrder(order = 16)
    private String Address;
    @FieldOrder(order = 17)
    private String Zone_Bit;
    @FieldOrder(order = 18)
    private String Telphone;
    @FieldOrder(order = 19)
    private String Fax;
    @FieldOrder(order = 20)
    private String Postal_Code;
    @FieldOrder(order = 21)
    private String Email;
    @FieldOrder(order = 22)
    private String Url;
    @FieldOrder(order = 23)
    private String Found;
    @FieldOrder(order = 24)
    private String Emps;
    @FieldOrder(order = 25)
    private String ORG_Type;
    @FieldOrder(order = 26)
    private String ORG_area;
    @FieldOrder(order = 27)
    private String Colno;
    @FieldOrder(order = 28)
    private String ORG;
    @FieldOrder(order = 29)
    private String Sys;
    @FieldOrder(order = 30)
    private String Specol_Collections;
    @FieldOrder(order = 31)
    private String Specol_Type;
    @FieldOrder(order = 32)
    private String Digres;
    @FieldOrder(order = 33)
    private String Self_Resource;
    @FieldOrder(order = 34)
    private String ORG_ID;
    @FieldOrder(order = 35)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getORG_Other() {
        return ORG_Other;
    }

    public void setORG_Other(String ORG_Other) {
        this.ORG_Other = ORG_Other;
    }

    public String getName_Anyname1() {
        return Name_Anyname1;
    }

    public void setName_Anyname1(String name_Anyname1) {
        Name_Anyname1 = name_Anyname1;
    }

    public String getName_Anyname2() {
        return Name_Anyname2;
    }

    public void setName_Anyname2(String name_Anyname2) {
        Name_Anyname2 = name_Anyname2;
    }

    public String getAcname() {
        return Acname;
    }

    public void setAcname(String acname) {
        Acname = acname;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getSimple_Name() {
        return Simple_Name;
    }

    public void setSimple_Name(String simple_Name) {
        Simple_Name = simple_Name;
    }

    public String getOther_Name() {
        return Other_Name;
    }

    public void setOther_Name(String other_Name) {
        Other_Name = other_Name;
    }

    public String getPerson() {
        return Person;
    }

    public void setPerson(String person) {
        Person = person;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZone_Bit() {
        return Zone_Bit;
    }

    public void setZone_Bit(String zone_Bit) {
        Zone_Bit = zone_Bit;
    }

    public String getTelphone() {
        return Telphone;
    }

    public void setTelphone(String telphone) {
        Telphone = telphone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getPostal_Code() {
        return Postal_Code;
    }

    public void setPostal_Code(String postal_Code) {
        Postal_Code = postal_Code;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getFound() {
        return Found;
    }

    public void setFound(String found) {
        Found = found;
    }

    public String getEmps() {
        return Emps;
    }

    public void setEmps(String emps) {
        Emps = emps;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getORG_area() {
        return ORG_area;
    }

    public void setORG_area(String ORG_area) {
        this.ORG_area = ORG_area;
    }

    public String getColno() {
        return Colno;
    }

    public void setColno(String colno) {
        Colno = colno;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getSys() {
        return Sys;
    }

    public void setSys(String sys) {
        Sys = sys;
    }

    public String getSpecol_Collections() {
        return Specol_Collections;
    }

    public void setSpecol_Collections(String specol_Collections) {
        Specol_Collections = specol_Collections;
    }

    public String getSpecol_Type() {
        return Specol_Type;
    }

    public void setSpecol_Type(String specol_Type) {
        Specol_Type = specol_Type;
    }

    public String getDigres() {
        return Digres;
    }

    public void setDigres(String digres) {
        Digres = digres;
    }

    public String getSelf_Resource() {
        return Self_Resource;
    }

    public void setSelf_Resource(String self_Resource) {
        Self_Resource = self_Resource;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }
}
