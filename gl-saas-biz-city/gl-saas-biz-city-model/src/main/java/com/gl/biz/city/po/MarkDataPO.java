package com.gl.biz.city.po;

//标记构件PO
public class MarkDataPO {
    //id
    private String id;
    //数据id
    private String dataId;
    //时态
    private Integer status;
    //来源
    private Integer from;
    //调用方
    private String userId;

    public MarkDataPO(String id,String dataId,Integer status, Integer from,String userId) {
        this.id = id;
        this.status = status;
        this.from = from;
        this.userId=userId;
        this.dataId=dataId;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }
}
