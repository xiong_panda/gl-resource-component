package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


//核心资源数据 中文期刊
public class WOverseasPatentPO {
    @FieldOrder(order = 1)	private String 	F_ID;
    @FieldOrder(order = 2)	private String 	ID;
    @FieldOrder(order = 3)	private String 	FullText;
    @FieldOrder(order = 4)	private String 	ZHUTI;
    @FieldOrder(order = 5)	private String 	TITLE;
    @FieldOrder(order = 6)	private String 	ABSTRACT;
    @FieldOrder(order = 7)	private String 	KEYWORD;
    @FieldOrder(order = 8)	private String 	ORG;
    @FieldOrder(order = 9)	private String 	ORG_ORG;
    @FieldOrder(order = 10)	private String 	ORG_ORG_ANYNAME;
    @FieldOrder(order = 11)	private String 	AUTHOR;
    @FieldOrder(order = 12)	private String 	PATNO;
    @FieldOrder(order = 13)	private String 	REQNO;
    @FieldOrder(order = 14)	private String 	DATE;
    @FieldOrder(order = 15)	private String 	REQPEP;
    @FieldOrder(order = 16)	private String 	COUNTRY;
    @FieldOrder(order = 17)	private String 	ANNO;
    @FieldOrder(order = 18)	private String 	ANNODATE;
    @FieldOrder(order = 19)	private String 	YANNODATE;
    @FieldOrder(order = 20)	private String 	CERTDATE;
    @FieldOrder(order = 21)	private String 	ZMCLS;
    @FieldOrder(order = 22)	private String 	OMAINCLS;
    @FieldOrder(order = 23)	private String 	OCLS;
    @FieldOrder(order = 24)	private String 	MAINCLS;
    @FieldOrder(order = 25)	private String 	CLS;
    @FieldOrder(order = 26)	private String 	CATE;
    @FieldOrder(order = 27)	private String 	CCLS;
    @FieldOrder(order = 28)	private String 	CCLSS;
    @FieldOrder(order = 29)	private String 	ECLS;
    @FieldOrder(order = 30)	private String 	ACLS;
    @FieldOrder(order = 31)	private String 	CONGENER;
    @FieldOrder(order = 32)	private String 	OREQNO;
    @FieldOrder(order = 33)	private String 	CID;
    @FieldOrder(order = 34)	private String 	YEAR;
    @FieldOrder(order = 35)	private String 	REQNOV;
    @FieldOrder(order = 36)	private String 	PATT;
    @FieldOrder(order = 37)	private String 	INTAPP;
    @FieldOrder(order = 38)	private String 	INTPUB;
    @FieldOrder(order = 39)	private String 	CREQNO;
    @FieldOrder(order = 40)	private String 	DEC;
    @FieldOrder(order = 41)	private String 	PRIORITY;
    @FieldOrder(order = 42)	private String 	PRIDATE;
    @FieldOrder(order = 43)	private String 	OPATNO;
    @FieldOrder(order = 44)	private String 	CREF;
    @FieldOrder(order = 45)	private String 	FREF;
    @FieldOrder(order = 46)	private String 	NONREF;
    @FieldOrder(order = 47)	private String 	DOM;
    @FieldOrder(order = 48)	private String 	ORGTYPE;
    @FieldOrder(order = 49)	private String 	TI;
    @FieldOrder(order = 50)	private String 	AU;
    @FieldOrder(order = 51)	private String 	AGENCY;
    @FieldOrder(order = 52)	private String 	AGENT;
    @FieldOrder(order = 53)	private String 	DBID;
    @FieldOrder(order = 54)	private String 	SOURCE;
    @FieldOrder(order = 55)	private String 	DID;
    @FieldOrder(order = 56)	private String 	INVPAT;
    @FieldOrder(order = 57)	private String 	MAINCLSNUM;
    @FieldOrder(order = 58)	private String 	CLSNUM;
    @FieldOrder(order = 59)	private String 	REQADDR;
    @FieldOrder(order = 60)	private String 	REF;
    @FieldOrder(order = 61)	private String 	REVIEWER;
    @FieldOrder(order = 62)	private String 	CDID;
    @FieldOrder(order = 63)	private String 	SERADDR;
    @FieldOrder(order = 64)	private String 	PUBPATH;
    @FieldOrder(order = 65)	private String 	ErrCode;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getZHUTI() {
        return ZHUTI;
    }

    public void setZHUTI(String ZHUTI) {
        this.ZHUTI = ZHUTI;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG() {
        return ORG_ORG;
    }

    public void setORG_ORG(String ORG_ORG) {
        this.ORG_ORG = ORG_ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getPATNO() {
        return PATNO;
    }

    public void setPATNO(String PATNO) {
        this.PATNO = PATNO;
    }

    public String getREQNO() {
        return REQNO;
    }

    public void setREQNO(String REQNO) {
        this.REQNO = REQNO;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getREQPEP() {
        return REQPEP;
    }

    public void setREQPEP(String REQPEP) {
        this.REQPEP = REQPEP;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getANNO() {
        return ANNO;
    }

    public void setANNO(String ANNO) {
        this.ANNO = ANNO;
    }

    public String getANNODATE() {
        return ANNODATE;
    }

    public void setANNODATE(String ANNODATE) {
        this.ANNODATE = ANNODATE;
    }

    public String getYANNODATE() {
        return YANNODATE;
    }

    public void setYANNODATE(String YANNODATE) {
        this.YANNODATE = YANNODATE;
    }

    public String getCERTDATE() {
        return CERTDATE;
    }

    public void setCERTDATE(String CERTDATE) {
        this.CERTDATE = CERTDATE;
    }

    public String getZMCLS() {
        return ZMCLS;
    }

    public void setZMCLS(String ZMCLS) {
        this.ZMCLS = ZMCLS;
    }

    public String getOMAINCLS() {
        return OMAINCLS;
    }

    public void setOMAINCLS(String OMAINCLS) {
        this.OMAINCLS = OMAINCLS;
    }

    public String getOCLS() {
        return OCLS;
    }

    public void setOCLS(String OCLS) {
        this.OCLS = OCLS;
    }

    public String getMAINCLS() {
        return MAINCLS;
    }

    public void setMAINCLS(String MAINCLS) {
        this.MAINCLS = MAINCLS;
    }

    public String getCLS() {
        return CLS;
    }

    public void setCLS(String CLS) {
        this.CLS = CLS;
    }

    public String getCATE() {
        return CATE;
    }

    public void setCATE(String CATE) {
        this.CATE = CATE;
    }

    public String getCCLS() {
        return CCLS;
    }

    public void setCCLS(String CCLS) {
        this.CCLS = CCLS;
    }

    public String getCCLSS() {
        return CCLSS;
    }

    public void setCCLSS(String CCLSS) {
        this.CCLSS = CCLSS;
    }

    public String getECLS() {
        return ECLS;
    }

    public void setECLS(String ECLS) {
        this.ECLS = ECLS;
    }

    public String getACLS() {
        return ACLS;
    }

    public void setACLS(String ACLS) {
        this.ACLS = ACLS;
    }

    public String getCONGENER() {
        return CONGENER;
    }

    public void setCONGENER(String CONGENER) {
        this.CONGENER = CONGENER;
    }

    public String getOREQNO() {
        return OREQNO;
    }

    public void setOREQNO(String OREQNO) {
        this.OREQNO = OREQNO;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getREQNOV() {
        return REQNOV;
    }

    public void setREQNOV(String REQNOV) {
        this.REQNOV = REQNOV;
    }

    public String getPATT() {
        return PATT;
    }

    public void setPATT(String PATT) {
        this.PATT = PATT;
    }

    public String getINTAPP() {
        return INTAPP;
    }

    public void setINTAPP(String INTAPP) {
        this.INTAPP = INTAPP;
    }

    public String getINTPUB() {
        return INTPUB;
    }

    public void setINTPUB(String INTPUB) {
        this.INTPUB = INTPUB;
    }

    public String getCREQNO() {
        return CREQNO;
    }

    public void setCREQNO(String CREQNO) {
        this.CREQNO = CREQNO;
    }

    public String getDEC() {
        return DEC;
    }

    public void setDEC(String DEC) {
        this.DEC = DEC;
    }

    public String getPRIORITY() {
        return PRIORITY;
    }

    public void setPRIORITY(String PRIORITY) {
        this.PRIORITY = PRIORITY;
    }

    public String getPRIDATE() {
        return PRIDATE;
    }

    public void setPRIDATE(String PRIDATE) {
        this.PRIDATE = PRIDATE;
    }

    public String getOPATNO() {
        return OPATNO;
    }

    public void setOPATNO(String OPATNO) {
        this.OPATNO = OPATNO;
    }

    public String getCREF() {
        return CREF;
    }

    public void setCREF(String CREF) {
        this.CREF = CREF;
    }

    public String getFREF() {
        return FREF;
    }

    public void setFREF(String FREF) {
        this.FREF = FREF;
    }

    public String getNONREF() {
        return NONREF;
    }

    public void setNONREF(String NONREF) {
        this.NONREF = NONREF;
    }

    public String getDOM() {
        return DOM;
    }

    public void setDOM(String DOM) {
        this.DOM = DOM;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getTI() {
        return TI;
    }

    public void setTI(String TI) {
        this.TI = TI;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getAGENCY() {
        return AGENCY;
    }

    public void setAGENCY(String AGENCY) {
        this.AGENCY = AGENCY;
    }

    public String getAGENT() {
        return AGENT;
    }

    public void setAGENT(String AGENT) {
        this.AGENT = AGENT;
    }

    public String getDBID() {
        return DBID;
    }

    public void setDBID(String DBID) {
        this.DBID = DBID;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getINVPAT() {
        return INVPAT;
    }

    public void setINVPAT(String INVPAT) {
        this.INVPAT = INVPAT;
    }

    public String getMAINCLSNUM() {
        return MAINCLSNUM;
    }

    public void setMAINCLSNUM(String MAINCLSNUM) {
        this.MAINCLSNUM = MAINCLSNUM;
    }

    public String getCLSNUM() {
        return CLSNUM;
    }

    public void setCLSNUM(String CLSNUM) {
        this.CLSNUM = CLSNUM;
    }

    public String getREQADDR() {
        return REQADDR;
    }

    public void setREQADDR(String REQADDR) {
        this.REQADDR = REQADDR;
    }

    public String getREF() {
        return REF;
    }

    public void setREF(String REF) {
        this.REF = REF;
    }

    public String getREVIEWER() {
        return REVIEWER;
    }

    public void setREVIEWER(String REVIEWER) {
        this.REVIEWER = REVIEWER;
    }

    public String getCDID() {
        return CDID;
    }

    public void setCDID(String CDID) {
        this.CDID = CDID;
    }

    public String getSERADDR() {
        return SERADDR;
    }

    public void setSERADDR(String SERADDR) {
        this.SERADDR = SERADDR;
    }

    public String getPUBPATH() {
        return PUBPATH;
    }

    public void setPUBPATH(String PUBPATH) {
        this.PUBPATH = PUBPATH;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }
}
