package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


//核心资源数据 中文期刊
public class WChineseJournalPapersPO {
    @FieldOrder(order = 1)
    private String 	FullText;
    @FieldOrder(order = 2)
    private String 	ZHUTI;
    @FieldOrder(order = 3)
    private String 	ID;
    @FieldOrder(order = 4)
    private String 	F_ID;
    @FieldOrder(order = 5)
    private String 	DOI;
    @FieldOrder(order = 6)
    private String 	WID;
    @FieldOrder(order = 7)
    private String 	VID;
    @FieldOrder(order = 8)
    private String 	NID;
    @FieldOrder(order = 9)
    private String 	LID;
    @FieldOrder(order = 10)
    private String 	TI;
    @FieldOrder(order = 11)
    private String 	TIE;
    @FieldOrder(order = 12)
    private String 	TITLE;
    @FieldOrder(order = 13)
    private String 	AUID;
    @FieldOrder(order = 14)
    private String 	AU;
    @FieldOrder(order = 15)
    private String 	AUE;
    @FieldOrder(order = 16)
    private String 	FN;
    @FieldOrder(order = 17)
    private String 	FAUID;
    @FieldOrder(order = 18)
    private String 	FAU;
    @FieldOrder(order = 19)
    private String 	FAUE;
    @FieldOrder(order = 20)
    private String 	AUNUM;
    @FieldOrder(order = 21)
    private String 	AUTHOR;
    @FieldOrder(order = 22)
    private String 	AUTHOR_AUTHOR_ANYNAME;
    @FieldOrder(order = 23)
    private String 	AUTHOR_ANYNAME;
    @FieldOrder(order = 24)
    private String 	ORGS;
    @FieldOrder(order = 25)
    private String 	ORGC;
    @FieldOrder(order = 26)
    private String 	ORGE;
    @FieldOrder(order = 27)
    private String 	FORG;
    @FieldOrder(order = 28)
    private String 	FORGC;
    @FieldOrder(order = 29)
    private String 	FORGE;
    @FieldOrder(order = 30)
    private String 	ORG;
    @FieldOrder(order = 31)
    private String 	ORG_ORG_ANYNAME;
    @FieldOrder(order = 32)
    private String 	ORG_ANYNAME;
    @FieldOrder(order = 33)
    private String 	JOUID;
    @FieldOrder(order = 34)
    private String 	ISSN;
    @FieldOrder(order = 35)
    private String 	JOUCN;
    @FieldOrder(order = 36)
    private String 	JOUEN;
    @FieldOrder(order = 37)
    private String 	JOURNAL;
    @FieldOrder(order = 38)
    private String 	JOURNAL_JOURNAL_ANYNAME;
    @FieldOrder(order = 39)
    private String 	JOURNAL_ANYNAME;
    @FieldOrder(order = 40)
    private String 	FJOUCN;
    @FieldOrder(order = 41)
    private String 	FJOUCN_FJOUCN_ANYNAME;
    @FieldOrder(order = 42)
    private String 	FJOUCN_ANYNAME;
    @FieldOrder(order = 43)
    private String 	DATE;
    @FieldOrder(order = 44)
    private String 	YEAR;
    @FieldOrder(order = 45)
    private String 	VOL;
    @FieldOrder(order = 46)
    private String 	PER;
    @FieldOrder(order = 47)
    private String 	PG;
    @FieldOrder(order = 48)
    private String 	PN;
    @FieldOrder(order = 49)
    private String 	COLCN;
    @FieldOrder(order = 50)
    private String 	COLEN;
    @FieldOrder(order = 51)
    private String 	LAN;
    @FieldOrder(order = 52)
    private String 	CID;
    @FieldOrder(order = 53)
    private String 	DID;
    @FieldOrder(order = 54)
    private String 	DOCID;
    @FieldOrder(order = 55)
    private String 	MCID;
    @FieldOrder(order = 56)
    private String 	ZCID;
    @FieldOrder(order = 57)
    private String 	SZCID;
    @FieldOrder(order = 58)
    private String 	TZCID;
    @FieldOrder(order = 59)
    private String 	DZCID;
    @FieldOrder(order = 60)
    private String 	IID;
    @FieldOrder(order = 61)
    private String 	CKEY;
    @FieldOrder(order = 62)
    private String 	KEYWORD;
    @FieldOrder(order = 63)
    private String 	EKEY;
    @FieldOrder(order = 64)
    private String 	MKEY;
    @FieldOrder(order = 65)
    private String 	CAB;
    @FieldOrder(order = 66)
    private String 	ABSTRACT;
    @FieldOrder(order = 67)
    private String 	EAB;
    @FieldOrder(order = 68)
    private String 	FAB;
    @FieldOrder(order = 69)
    private String 	FUND;
    @FieldOrder(order = 70)
    private String 	FUND_FUND_ANYNAME;
    @FieldOrder(order = 71)
    private String 	FUND_ANYNAME;
    @FieldOrder(order = 72)
    private String 	FFUND;
    @FieldOrder(order = 73)
    private String 	FPN;
    @FieldOrder(order = 74)
    private String 	FUNDSUPPORT;
    @FieldOrder(order = 75)
    private String 	RN;
    @FieldOrder(order = 76)
    private String 	CORE;
    @FieldOrder(order = 77)
    private String 	SCORE;
    @FieldOrder(order = 78)
    private String 	ECORE;
    @FieldOrder(order = 79)
    private String 	SOURCE;
    @FieldOrder(order = 80)
    private String 	ORGSTRUCID;
    @FieldOrder(order = 81)
    private String 	ORGID;
    @FieldOrder(order = 82)
    private String 	ORGPROVINCE;
    @FieldOrder(order = 83)
    private String 	ORGCITY;
    @FieldOrder(order = 84)
    private String 	ORGTYPE;
    @FieldOrder(order = 85)
    private String 	FORGID;
    @FieldOrder(order = 86)
    private String 	ENDORGTYPE;
    @FieldOrder(order = 87)
    private String 	FENDORGID;
    @FieldOrder(order = 88)
    private String 	FORGSTRUCID;
    @FieldOrder(order = 89)
    private String 	FORGCITY;
    @FieldOrder(order = 90)
    private String 	FENDORGCITY;
    @FieldOrder(order = 91)
    private String 	FORGPROVINCE;
    @FieldOrder(order = 92)
    private String 	FENDORGPROVINCE;
    @FieldOrder(order = 93)
    private String 	FENDORGTYPE;
    @FieldOrder(order = 94)
    private String 	FORGTYPE;
    @FieldOrder(order = 95)
    private String 	AUIDS;
    @FieldOrder(order = 96)
    private String 	ORGNUM;
    @FieldOrder(order = 97)
    private String 	WEIGHT;
    @FieldOrder(order = 98)
    private String 	QUARTER;
    @FieldOrder(order = 99)
    private String 	HALFYEAR;
    @FieldOrder(order = 100)
    private String 	ErrCode;
    @FieldOrder(order = 101)
    private String 	AUTHORINFO;
    @FieldOrder(order = 102)
    private String 	AUTHORINFO_NAME;
    @FieldOrder(order = 103)
    private String 	AUTHORINFO_CX;
    @FieldOrder(order = 104)
    private String 	AUTHORINFO_ORG;
    @FieldOrder(order = 105)
    private String 	AUTHORINFO_FUNIT;
    @FieldOrder(order = 106)
    private String 	AUTHORINFO_SUNIT;
    @FieldOrder(order = 107)
    private String 	AUTHORINFO_JGLX;
    @FieldOrder(order = 108)
    private String 	AUTHORINFO_ORGPROVINCE;
    @FieldOrder(order = 109)
    private String 	AUTHORINFO_ORGCITY;
    @FieldOrder(order = 110)
    private String 	AUTHORINFO_ORGCOUNTRY;
    @FieldOrder(order = 111)
    private String 	AUTHORINFO_AUID;
    @FieldOrder(order = 112)
    private String 	AUTHORINFO_ORGID;
    @FieldOrder(order = 113)
    private String 	ORGINFO;
    @FieldOrder(order = 114)
    private String 	ORGINFO_ORG;
    @FieldOrder(order = 115)
    private String 	ORGINFO_CX;
    @FieldOrder(order = 116)
    private String 	ORGINFO_ORGTYPE;
    @FieldOrder(order = 117)
    private String 	ORGINFO_SHENG;
    @FieldOrder(order = 118)
    private String 	ORGINFO_SHI;
    @FieldOrder(order = 119)
    private String 	ORGINFO_XIAN;
    @FieldOrder(order = 120)
    private String 	ORGINFO_RELATION;
    @FieldOrder(order = 121)
    private String 	ORGINFO_ORGLEVEL1;
    @FieldOrder(order = 122)
    private String 	ORGINFO_ORGLEVEL2;
    @FieldOrder(order = 123)
    private String 	ORGINFO_ORGLEVEL3;
    @FieldOrder(order = 124)
    private String 	ORGINFO_ORGLEVEL4;
    @FieldOrder(order = 125)
    private String 	ORGINFO_ORGLEVEL5;

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getZHUTI() {
        return ZHUTI;
    }

    public void setZHUTI(String ZHUTI) {
        this.ZHUTI = ZHUTI;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getWID() {
        return WID;
    }

    public void setWID(String WID) {
        this.WID = WID;
    }

    public String getVID() {
        return VID;
    }

    public void setVID(String VID) {
        this.VID = VID;
    }

    public String getNID() {
        return NID;
    }

    public void setNID(String NID) {
        this.NID = NID;
    }

    public String getLID() {
        return LID;
    }

    public void setLID(String LID) {
        this.LID = LID;
    }

    public String getTI() {
        return TI;
    }

    public void setTI(String TI) {
        this.TI = TI;
    }

    public String getTIE() {
        return TIE;
    }

    public void setTIE(String TIE) {
        this.TIE = TIE;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getAUID() {
        return AUID;
    }

    public void setAUID(String AUID) {
        this.AUID = AUID;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getAUE() {
        return AUE;
    }

    public void setAUE(String AUE) {
        this.AUE = AUE;
    }

    public String getFN() {
        return FN;
    }

    public void setFN(String FN) {
        this.FN = FN;
    }

    public String getFAUID() {
        return FAUID;
    }

    public void setFAUID(String FAUID) {
        this.FAUID = FAUID;
    }

    public String getFAU() {
        return FAU;
    }

    public void setFAU(String FAU) {
        this.FAU = FAU;
    }

    public String getFAUE() {
        return FAUE;
    }

    public void setFAUE(String FAUE) {
        this.FAUE = FAUE;
    }

    public String getAUNUM() {
        return AUNUM;
    }

    public void setAUNUM(String AUNUM) {
        this.AUNUM = AUNUM;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAUTHOR_AUTHOR_ANYNAME() {
        return AUTHOR_AUTHOR_ANYNAME;
    }

    public void setAUTHOR_AUTHOR_ANYNAME(String AUTHOR_AUTHOR_ANYNAME) {
        this.AUTHOR_AUTHOR_ANYNAME = AUTHOR_AUTHOR_ANYNAME;
    }

    public String getAUTHOR_ANYNAME() {
        return AUTHOR_ANYNAME;
    }

    public void setAUTHOR_ANYNAME(String AUTHOR_ANYNAME) {
        this.AUTHOR_ANYNAME = AUTHOR_ANYNAME;
    }

    public String getORGS() {
        return ORGS;
    }

    public void setORGS(String ORGS) {
        this.ORGS = ORGS;
    }

    public String getORGC() {
        return ORGC;
    }

    public void setORGC(String ORGC) {
        this.ORGC = ORGC;
    }

    public String getORGE() {
        return ORGE;
    }

    public void setORGE(String ORGE) {
        this.ORGE = ORGE;
    }

    public String getFORG() {
        return FORG;
    }

    public void setFORG(String FORG) {
        this.FORG = FORG;
    }

    public String getFORGC() {
        return FORGC;
    }

    public void setFORGC(String FORGC) {
        this.FORGC = FORGC;
    }

    public String getFORGE() {
        return FORGE;
    }

    public void setFORGE(String FORGE) {
        this.FORGE = FORGE;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getJOUID() {
        return JOUID;
    }

    public void setJOUID(String JOUID) {
        this.JOUID = JOUID;
    }

    public String getISSN() {
        return ISSN;
    }

    public void setISSN(String ISSN) {
        this.ISSN = ISSN;
    }

    public String getJOUCN() {
        return JOUCN;
    }

    public void setJOUCN(String JOUCN) {
        this.JOUCN = JOUCN;
    }

    public String getJOUEN() {
        return JOUEN;
    }

    public void setJOUEN(String JOUEN) {
        this.JOUEN = JOUEN;
    }

    public String getJOURNAL() {
        return JOURNAL;
    }

    public void setJOURNAL(String JOURNAL) {
        this.JOURNAL = JOURNAL;
    }

    public String getJOURNAL_JOURNAL_ANYNAME() {
        return JOURNAL_JOURNAL_ANYNAME;
    }

    public void setJOURNAL_JOURNAL_ANYNAME(String JOURNAL_JOURNAL_ANYNAME) {
        this.JOURNAL_JOURNAL_ANYNAME = JOURNAL_JOURNAL_ANYNAME;
    }

    public String getJOURNAL_ANYNAME() {
        return JOURNAL_ANYNAME;
    }

    public void setJOURNAL_ANYNAME(String JOURNAL_ANYNAME) {
        this.JOURNAL_ANYNAME = JOURNAL_ANYNAME;
    }

    public String getFJOUCN() {
        return FJOUCN;
    }

    public void setFJOUCN(String FJOUCN) {
        this.FJOUCN = FJOUCN;
    }

    public String getFJOUCN_FJOUCN_ANYNAME() {
        return FJOUCN_FJOUCN_ANYNAME;
    }

    public void setFJOUCN_FJOUCN_ANYNAME(String FJOUCN_FJOUCN_ANYNAME) {
        this.FJOUCN_FJOUCN_ANYNAME = FJOUCN_FJOUCN_ANYNAME;
    }

    public String getFJOUCN_ANYNAME() {
        return FJOUCN_ANYNAME;
    }

    public void setFJOUCN_ANYNAME(String FJOUCN_ANYNAME) {
        this.FJOUCN_ANYNAME = FJOUCN_ANYNAME;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getVOL() {
        return VOL;
    }

    public void setVOL(String VOL) {
        this.VOL = VOL;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getPG() {
        return PG;
    }

    public void setPG(String PG) {
        this.PG = PG;
    }

    public String getPN() {
        return PN;
    }

    public void setPN(String PN) {
        this.PN = PN;
    }

    public String getCOLCN() {
        return COLCN;
    }

    public void setCOLCN(String COLCN) {
        this.COLCN = COLCN;
    }

    public String getCOLEN() {
        return COLEN;
    }

    public void setCOLEN(String COLEN) {
        this.COLEN = COLEN;
    }

    public String getLAN() {
        return LAN;
    }

    public void setLAN(String LAN) {
        this.LAN = LAN;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getDOCID() {
        return DOCID;
    }

    public void setDOCID(String DOCID) {
        this.DOCID = DOCID;
    }

    public String getMCID() {
        return MCID;
    }

    public void setMCID(String MCID) {
        this.MCID = MCID;
    }

    public String getZCID() {
        return ZCID;
    }

    public void setZCID(String ZCID) {
        this.ZCID = ZCID;
    }

    public String getSZCID() {
        return SZCID;
    }

    public void setSZCID(String SZCID) {
        this.SZCID = SZCID;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getDZCID() {
        return DZCID;
    }

    public void setDZCID(String DZCID) {
        this.DZCID = DZCID;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getCKEY() {
        return CKEY;
    }

    public void setCKEY(String CKEY) {
        this.CKEY = CKEY;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getEKEY() {
        return EKEY;
    }

    public void setEKEY(String EKEY) {
        this.EKEY = EKEY;
    }

    public String getMKEY() {
        return MKEY;
    }

    public void setMKEY(String MKEY) {
        this.MKEY = MKEY;
    }

    public String getCAB() {
        return CAB;
    }

    public void setCAB(String CAB) {
        this.CAB = CAB;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getEAB() {
        return EAB;
    }

    public void setEAB(String EAB) {
        this.EAB = EAB;
    }

    public String getFAB() {
        return FAB;
    }

    public void setFAB(String FAB) {
        this.FAB = FAB;
    }

    public String getFUND() {
        return FUND;
    }

    public void setFUND(String FUND) {
        this.FUND = FUND;
    }

    public String getFUND_FUND_ANYNAME() {
        return FUND_FUND_ANYNAME;
    }

    public void setFUND_FUND_ANYNAME(String FUND_FUND_ANYNAME) {
        this.FUND_FUND_ANYNAME = FUND_FUND_ANYNAME;
    }

    public String getFUND_ANYNAME() {
        return FUND_ANYNAME;
    }

    public void setFUND_ANYNAME(String FUND_ANYNAME) {
        this.FUND_ANYNAME = FUND_ANYNAME;
    }

    public String getFFUND() {
        return FFUND;
    }

    public void setFFUND(String FFUND) {
        this.FFUND = FFUND;
    }

    public String getFPN() {
        return FPN;
    }

    public void setFPN(String FPN) {
        this.FPN = FPN;
    }

    public String getFUNDSUPPORT() {
        return FUNDSUPPORT;
    }

    public void setFUNDSUPPORT(String FUNDSUPPORT) {
        this.FUNDSUPPORT = FUNDSUPPORT;
    }

    public String getRN() {
        return RN;
    }

    public void setRN(String RN) {
        this.RN = RN;
    }

    public String getCORE() {
        return CORE;
    }

    public void setCORE(String CORE) {
        this.CORE = CORE;
    }

    public String getSCORE() {
        return SCORE;
    }

    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    public String getECORE() {
        return ECORE;
    }

    public void setECORE(String ECORE) {
        this.ECORE = ECORE;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    public String getORGSTRUCID() {
        return ORGSTRUCID;
    }

    public void setORGSTRUCID(String ORGSTRUCID) {
        this.ORGSTRUCID = ORGSTRUCID;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getORGPROVINCE() {
        return ORGPROVINCE;
    }

    public void setORGPROVINCE(String ORGPROVINCE) {
        this.ORGPROVINCE = ORGPROVINCE;
    }

    public String getORGCITY() {
        return ORGCITY;
    }

    public void setORGCITY(String ORGCITY) {
        this.ORGCITY = ORGCITY;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getENDORGTYPE() {
        return ENDORGTYPE;
    }

    public void setENDORGTYPE(String ENDORGTYPE) {
        this.ENDORGTYPE = ENDORGTYPE;
    }

    public String getFENDORGID() {
        return FENDORGID;
    }

    public void setFENDORGID(String FENDORGID) {
        this.FENDORGID = FENDORGID;
    }

    public String getFORGSTRUCID() {
        return FORGSTRUCID;
    }

    public void setFORGSTRUCID(String FORGSTRUCID) {
        this.FORGSTRUCID = FORGSTRUCID;
    }

    public String getFORGCITY() {
        return FORGCITY;
    }

    public void setFORGCITY(String FORGCITY) {
        this.FORGCITY = FORGCITY;
    }

    public String getFENDORGCITY() {
        return FENDORGCITY;
    }

    public void setFENDORGCITY(String FENDORGCITY) {
        this.FENDORGCITY = FENDORGCITY;
    }

    public String getFORGPROVINCE() {
        return FORGPROVINCE;
    }

    public void setFORGPROVINCE(String FORGPROVINCE) {
        this.FORGPROVINCE = FORGPROVINCE;
    }

    public String getFENDORGPROVINCE() {
        return FENDORGPROVINCE;
    }

    public void setFENDORGPROVINCE(String FENDORGPROVINCE) {
        this.FENDORGPROVINCE = FENDORGPROVINCE;
    }

    public String getFENDORGTYPE() {
        return FENDORGTYPE;
    }

    public void setFENDORGTYPE(String FENDORGTYPE) {
        this.FENDORGTYPE = FENDORGTYPE;
    }

    public String getFORGTYPE() {
        return FORGTYPE;
    }

    public void setFORGTYPE(String FORGTYPE) {
        this.FORGTYPE = FORGTYPE;
    }

    public String getAUIDS() {
        return AUIDS;
    }

    public void setAUIDS(String AUIDS) {
        this.AUIDS = AUIDS;
    }

    public String getORGNUM() {
        return ORGNUM;
    }

    public void setORGNUM(String ORGNUM) {
        this.ORGNUM = ORGNUM;
    }

    public String getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(String WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public String getQUARTER() {
        return QUARTER;
    }

    public void setQUARTER(String QUARTER) {
        this.QUARTER = QUARTER;
    }

    public String getHALFYEAR() {
        return HALFYEAR;
    }

    public void setHALFYEAR(String HALFYEAR) {
        this.HALFYEAR = HALFYEAR;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getAUTHORINFO() {
        return AUTHORINFO;
    }

    public void setAUTHORINFO(String AUTHORINFO) {
        this.AUTHORINFO = AUTHORINFO;
    }

    public String getAUTHORINFO_NAME() {
        return AUTHORINFO_NAME;
    }

    public void setAUTHORINFO_NAME(String AUTHORINFO_NAME) {
        this.AUTHORINFO_NAME = AUTHORINFO_NAME;
    }

    public String getAUTHORINFO_CX() {
        return AUTHORINFO_CX;
    }

    public void setAUTHORINFO_CX(String AUTHORINFO_CX) {
        this.AUTHORINFO_CX = AUTHORINFO_CX;
    }

    public String getAUTHORINFO_ORG() {
        return AUTHORINFO_ORG;
    }

    public void setAUTHORINFO_ORG(String AUTHORINFO_ORG) {
        this.AUTHORINFO_ORG = AUTHORINFO_ORG;
    }

    public String getAUTHORINFO_FUNIT() {
        return AUTHORINFO_FUNIT;
    }

    public void setAUTHORINFO_FUNIT(String AUTHORINFO_FUNIT) {
        this.AUTHORINFO_FUNIT = AUTHORINFO_FUNIT;
    }

    public String getAUTHORINFO_SUNIT() {
        return AUTHORINFO_SUNIT;
    }

    public void setAUTHORINFO_SUNIT(String AUTHORINFO_SUNIT) {
        this.AUTHORINFO_SUNIT = AUTHORINFO_SUNIT;
    }

    public String getAUTHORINFO_JGLX() {
        return AUTHORINFO_JGLX;
    }

    public void setAUTHORINFO_JGLX(String AUTHORINFO_JGLX) {
        this.AUTHORINFO_JGLX = AUTHORINFO_JGLX;
    }

    public String getAUTHORINFO_ORGPROVINCE() {
        return AUTHORINFO_ORGPROVINCE;
    }

    public void setAUTHORINFO_ORGPROVINCE(String AUTHORINFO_ORGPROVINCE) {
        this.AUTHORINFO_ORGPROVINCE = AUTHORINFO_ORGPROVINCE;
    }

    public String getAUTHORINFO_ORGCITY() {
        return AUTHORINFO_ORGCITY;
    }

    public void setAUTHORINFO_ORGCITY(String AUTHORINFO_ORGCITY) {
        this.AUTHORINFO_ORGCITY = AUTHORINFO_ORGCITY;
    }

    public String getAUTHORINFO_ORGCOUNTRY() {
        return AUTHORINFO_ORGCOUNTRY;
    }

    public void setAUTHORINFO_ORGCOUNTRY(String AUTHORINFO_ORGCOUNTRY) {
        this.AUTHORINFO_ORGCOUNTRY = AUTHORINFO_ORGCOUNTRY;
    }

    public String getAUTHORINFO_AUID() {
        return AUTHORINFO_AUID;
    }

    public void setAUTHORINFO_AUID(String AUTHORINFO_AUID) {
        this.AUTHORINFO_AUID = AUTHORINFO_AUID;
    }

    public String getAUTHORINFO_ORGID() {
        return AUTHORINFO_ORGID;
    }

    public void setAUTHORINFO_ORGID(String AUTHORINFO_ORGID) {
        this.AUTHORINFO_ORGID = AUTHORINFO_ORGID;
    }

    public String getORGINFO() {
        return ORGINFO;
    }

    public void setORGINFO(String ORGINFO) {
        this.ORGINFO = ORGINFO;
    }

    public String getORGINFO_ORG() {
        return ORGINFO_ORG;
    }

    public void setORGINFO_ORG(String ORGINFO_ORG) {
        this.ORGINFO_ORG = ORGINFO_ORG;
    }

    public String getORGINFO_CX() {
        return ORGINFO_CX;
    }

    public void setORGINFO_CX(String ORGINFO_CX) {
        this.ORGINFO_CX = ORGINFO_CX;
    }

    public String getORGINFO_ORGTYPE() {
        return ORGINFO_ORGTYPE;
    }

    public void setORGINFO_ORGTYPE(String ORGINFO_ORGTYPE) {
        this.ORGINFO_ORGTYPE = ORGINFO_ORGTYPE;
    }

    public String getORGINFO_SHENG() {
        return ORGINFO_SHENG;
    }

    public void setORGINFO_SHENG(String ORGINFO_SHENG) {
        this.ORGINFO_SHENG = ORGINFO_SHENG;
    }

    public String getORGINFO_SHI() {
        return ORGINFO_SHI;
    }

    public void setORGINFO_SHI(String ORGINFO_SHI) {
        this.ORGINFO_SHI = ORGINFO_SHI;
    }

    public String getORGINFO_XIAN() {
        return ORGINFO_XIAN;
    }

    public void setORGINFO_XIAN(String ORGINFO_XIAN) {
        this.ORGINFO_XIAN = ORGINFO_XIAN;
    }

    public String getORGINFO_RELATION() {
        return ORGINFO_RELATION;
    }

    public void setORGINFO_RELATION(String ORGINFO_RELATION) {
        this.ORGINFO_RELATION = ORGINFO_RELATION;
    }

    public String getORGINFO_ORGLEVEL1() {
        return ORGINFO_ORGLEVEL1;
    }

    public void setORGINFO_ORGLEVEL1(String ORGINFO_ORGLEVEL1) {
        this.ORGINFO_ORGLEVEL1 = ORGINFO_ORGLEVEL1;
    }

    public String getORGINFO_ORGLEVEL2() {
        return ORGINFO_ORGLEVEL2;
    }

    public void setORGINFO_ORGLEVEL2(String ORGINFO_ORGLEVEL2) {
        this.ORGINFO_ORGLEVEL2 = ORGINFO_ORGLEVEL2;
    }

    public String getORGINFO_ORGLEVEL3() {
        return ORGINFO_ORGLEVEL3;
    }

    public void setORGINFO_ORGLEVEL3(String ORGINFO_ORGLEVEL3) {
        this.ORGINFO_ORGLEVEL3 = ORGINFO_ORGLEVEL3;
    }

    public String getORGINFO_ORGLEVEL4() {
        return ORGINFO_ORGLEVEL4;
    }

    public void setORGINFO_ORGLEVEL4(String ORGINFO_ORGLEVEL4) {
        this.ORGINFO_ORGLEVEL4 = ORGINFO_ORGLEVEL4;
    }

    public String getORGINFO_ORGLEVEL5() {
        return ORGINFO_ORGLEVEL5;
    }

    public void setORGINFO_ORGLEVEL5(String ORGINFO_ORGLEVEL5) {
        this.ORGINFO_ORGLEVEL5 = ORGINFO_ORGLEVEL5;
    }
}
