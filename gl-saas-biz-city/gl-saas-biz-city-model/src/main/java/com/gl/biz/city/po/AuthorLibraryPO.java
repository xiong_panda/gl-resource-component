package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class AuthorLibraryPO {
    @FieldOrder(order = 1)
    private String OrderID;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }

    @FieldOrder(order = 3)
    private String w_ID;
    @FieldOrder(order = 4)
    private String FullText;
    @FieldOrder(order = 5)
    private String A_English_Name;
    @FieldOrder(order = 6)
    private String Unit;
    @FieldOrder(order = 7)
    private String Unit_FullText_Search;
    @FieldOrder(order = 8)
    private String Sun_English_Unit_Name;
    @FieldOrder(order = 9)
    private String Statistical_Unit_Name;
    @FieldOrder(order = 10)
    private String ORGall_Anyname;
    @FieldOrder(order = 11)
    private String ORGall_Anyname2;
    @FieldOrder(order = 12)
    private String level1_Unit;
    @FieldOrder(order = 13)
    private String level2_Unit;
    @FieldOrder(order = 14)
    private String w_Unit;
    @FieldOrder(order = 15)
    private String English_Unit_Name;
    @FieldOrder(order = 16)
    private String Title;
    @FieldOrder(order = 17)
    private String Introduction_Job_Resume;
    @FieldOrder(order = 18)
    private String Intro;
    @FieldOrder(order = 19)
    private String Name;
    @FieldOrder(order = 20)
    private String English_Name;
    @FieldOrder(order = 21)
    private String Mentor;
    @FieldOrder(order = 22)
    private String Region;
    @FieldOrder(order = 23)
    private String Postcode;
    @FieldOrder(order = 24)
    private String Address;
    @FieldOrder(order = 25)
    private String Email;
    @FieldOrder(order = 26)
    private String ORG_Type;
    @FieldOrder(order = 27)
    private String Date;
    @FieldOrder(order = 28)
    private String Note;
    @FieldOrder(order = 29)
    private String ORG_ID;
    @FieldOrder(order = 30)
    private String Frequency;
    @FieldOrder(order = 31)
    private String Yfreq;
    @FieldOrder(order = 32)
    private String KeyWord;
    @FieldOrder(order = 33)
    private String Keyword_Anyname;
    @FieldOrder(order = 34)
    private String Related_Keyword;
    @FieldOrder(order = 35)
    private String Related_Journals;
    @FieldOrder(order = 36)
    private String Related_Figure;
    @FieldOrder(order = 37)
    private String Related_Discipline;
    @FieldOrder(order = 38)
    private String Related_Discipline2;
    @FieldOrder(order = 39)
    private String Related_Fund;
    @FieldOrder(order = 40)
    private String Qcode;
    @FieldOrder(order = 41)
    private String Sex;
    @FieldOrder(order = 42)
    private String Birth_Date;
    @FieldOrder(order = 43)
    private String Working_Days;
    @FieldOrder(order = 44)
    private String Birth_Address;
    @FieldOrder(order = 45)
    private String National;
    @FieldOrder(order = 46)
    private String Job_Position;
    @FieldOrder(order = 47)
    private String Administrative_Code;
    @FieldOrder(order = 48)
    private String Telphone;
    @FieldOrder(order = 49)
    private String Fax;
    @FieldOrder(order = 50)
    private String Education_Background;
    @FieldOrder(order = 51)
    private String Highest_Degree;
    @FieldOrder(order = 52)
    private String Foreign_Languages;
    @FieldOrder(order = 53)
    private String Zone_Bit;
    @FieldOrder(order = 54)
    private String Professional_Research;
    @FieldOrder(order = 55)
    private String Co_Employment;
    @FieldOrder(order = 56)
    private String Academician;
    @FieldOrder(order = 57)
    private String Experts_Honor;
    @FieldOrder(order = 58)
    private String Experts_Honor2;
    @FieldOrder(order = 59)
    private String Experts_Characteristics;
    @FieldOrder(order = 60)
    private String Character_Anyname;
    @FieldOrder(order = 61)
    private String Character_Anyname2;
    @FieldOrder(order = 62)
    private String Discipline_Class;
    @FieldOrder(order = 63)
    private String Discipline_Class_Code;
    @FieldOrder(order = 64)
    private String Discipline_Class_Code2;
    @FieldOrder(order = 65)
    private String Job_Resume;
    @FieldOrder(order = 66)
    private String Data_Source;
    @FieldOrder(order = 67)
    private String H;
    @FieldOrder(order = 68)
    private String Journalcount;
    @FieldOrder(order = 69)
    private String Quotecount;
    @FieldOrder(order = 70)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private java.util.Date CreateTime;

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getW_ID() {
        return w_ID;
    }

    public void setW_ID(String w_ID) {
        this.w_ID = w_ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getA_English_Name() {
        return A_English_Name;
    }

    public void setA_English_Name(String a_English_Name) {
        A_English_Name = a_English_Name;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getUnit_FullText_Search() {
        return Unit_FullText_Search;
    }

    public void setUnit_FullText_Search(String unit_FullText_Search) {
        Unit_FullText_Search = unit_FullText_Search;
    }

    public String getSun_English_Unit_Name() {
        return Sun_English_Unit_Name;
    }

    public void setSun_English_Unit_Name(String sun_English_Unit_Name) {
        Sun_English_Unit_Name = sun_English_Unit_Name;
    }

    public String getStatistical_Unit_Name() {
        return Statistical_Unit_Name;
    }

    public void setStatistical_Unit_Name(String statistical_Unit_Name) {
        Statistical_Unit_Name = statistical_Unit_Name;
    }

    public String getORGall_Anyname() {
        return ORGall_Anyname;
    }

    public void setORGall_Anyname(String ORGall_Anyname) {
        this.ORGall_Anyname = ORGall_Anyname;
    }

    public String getORGall_Anyname2() {
        return ORGall_Anyname2;
    }

    public void setORGall_Anyname2(String ORGall_Anyname2) {
        this.ORGall_Anyname2 = ORGall_Anyname2;
    }

    public String getLevel1_Unit() {
        return level1_Unit;
    }

    public void setLevel1_Unit(String level1_Unit) {
        this.level1_Unit = level1_Unit;
    }

    public String getLevel2_Unit() {
        return level2_Unit;
    }

    public void setLevel2_Unit(String level2_Unit) {
        this.level2_Unit = level2_Unit;
    }

    public String getW_Unit() {
        return w_Unit;
    }

    public void setW_Unit(String w_Unit) {
        this.w_Unit = w_Unit;
    }

    public String getEnglish_Unit_Name() {
        return English_Unit_Name;
    }

    public void setEnglish_Unit_Name(String english_Unit_Name) {
        English_Unit_Name = english_Unit_Name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getIntroduction_Job_Resume() {
        return Introduction_Job_Resume;
    }

    public void setIntroduction_Job_Resume(String introduction_Job_Resume) {
        Introduction_Job_Resume = introduction_Job_Resume;
    }

    public String getIntro() {
        return Intro;
    }

    public void setIntro(String intro) {
        Intro = intro;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEnglish_Name() {
        return English_Name;
    }

    public void setEnglish_Name(String english_Name) {
        English_Name = english_Name;
    }

    public String getMentor() {
        return Mentor;
    }

    public void setMentor(String mentor) {
        Mentor = mentor;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getPostcode() {
        return Postcode;
    }

    public void setPostcode(String postcode) {
        Postcode = postcode;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getYfreq() {
        return Yfreq;
    }

    public void setYfreq(String yfreq) {
        Yfreq = yfreq;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }

    public String getKeyword_Anyname() {
        return Keyword_Anyname;
    }

    public void setKeyword_Anyname(String keyword_Anyname) {
        Keyword_Anyname = keyword_Anyname;
    }

    public String getRelated_Keyword() {
        return Related_Keyword;
    }

    public void setRelated_Keyword(String related_Keyword) {
        Related_Keyword = related_Keyword;
    }

    public String getRelated_Journals() {
        return Related_Journals;
    }

    public void setRelated_Journals(String related_Journals) {
        Related_Journals = related_Journals;
    }

    public String getRelated_Figure() {
        return Related_Figure;
    }

    public void setRelated_Figure(String related_Figure) {
        Related_Figure = related_Figure;
    }

    public String getRelated_Discipline() {
        return Related_Discipline;
    }

    public void setRelated_Discipline(String related_Discipline) {
        Related_Discipline = related_Discipline;
    }

    public String getRelated_Discipline2() {
        return Related_Discipline2;
    }

    public void setRelated_Discipline2(String related_Discipline2) {
        Related_Discipline2 = related_Discipline2;
    }

    public String getRelated_Fund() {
        return Related_Fund;
    }

    public void setRelated_Fund(String related_Fund) {
        Related_Fund = related_Fund;
    }

    public String getQcode() {
        return Qcode;
    }

    public void setQcode(String qcode) {
        Qcode = qcode;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirth_Date() {
        return Birth_Date;
    }

    public void setBirth_Date(String birth_Date) {
        Birth_Date = birth_Date;
    }

    public String getWorking_Days() {
        return Working_Days;
    }

    public void setWorking_Days(String working_Days) {
        Working_Days = working_Days;
    }

    public String getBirth_Address() {
        return Birth_Address;
    }

    public void setBirth_Address(String birth_Address) {
        Birth_Address = birth_Address;
    }

    public String getNational() {
        return National;
    }

    public void setNational(String national) {
        National = national;
    }

    public String getJob_Position() {
        return Job_Position;
    }

    public void setJob_Position(String job_Position) {
        Job_Position = job_Position;
    }

    public String getAdministrative_Code() {
        return Administrative_Code;
    }

    public void setAdministrative_Code(String administrative_Code) {
        Administrative_Code = administrative_Code;
    }

    public String getTelphone() {
        return Telphone;
    }

    public void setTelphone(String telphone) {
        Telphone = telphone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEducation_Background() {
        return Education_Background;
    }

    public void setEducation_Background(String education_Background) {
        Education_Background = education_Background;
    }

    public String getHighest_Degree() {
        return Highest_Degree;
    }

    public void setHighest_Degree(String highest_Degree) {
        Highest_Degree = highest_Degree;
    }

    public String getForeign_Languages() {
        return Foreign_Languages;
    }

    public void setForeign_Languages(String foreign_Languages) {
        Foreign_Languages = foreign_Languages;
    }

    public String getZone_Bit() {
        return Zone_Bit;
    }

    public void setZone_Bit(String zone_Bit) {
        Zone_Bit = zone_Bit;
    }

    public String getProfessional_Research() {
        return Professional_Research;
    }

    public void setProfessional_Research(String professional_Research) {
        Professional_Research = professional_Research;
    }

    public String getCo_Employment() {
        return Co_Employment;
    }

    public void setCo_Employment(String co_Employment) {
        Co_Employment = co_Employment;
    }

    public String getAcademician() {
        return Academician;
    }

    public void setAcademician(String academician) {
        Academician = academician;
    }

    public String getExperts_Honor() {
        return Experts_Honor;
    }

    public void setExperts_Honor(String experts_Honor) {
        Experts_Honor = experts_Honor;
    }

    public String getExperts_Honor2() {
        return Experts_Honor2;
    }

    public void setExperts_Honor2(String experts_Honor2) {
        Experts_Honor2 = experts_Honor2;
    }

    public String getExperts_Characteristics() {
        return Experts_Characteristics;
    }

    public void setExperts_Characteristics(String experts_Characteristics) {
        Experts_Characteristics = experts_Characteristics;
    }

    public String getCharacter_Anyname() {
        return Character_Anyname;
    }

    public void setCharacter_Anyname(String character_Anyname) {
        Character_Anyname = character_Anyname;
    }

    public String getCharacter_Anyname2() {
        return Character_Anyname2;
    }

    public void setCharacter_Anyname2(String character_Anyname2) {
        Character_Anyname2 = character_Anyname2;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getDiscipline_Class_Code() {
        return Discipline_Class_Code;
    }

    public void setDiscipline_Class_Code(String discipline_Class_Code) {
        Discipline_Class_Code = discipline_Class_Code;
    }

    public String getDiscipline_Class_Code2() {
        return Discipline_Class_Code2;
    }

    public void setDiscipline_Class_Code2(String discipline_Class_Code2) {
        Discipline_Class_Code2 = discipline_Class_Code2;
    }

    public String getJob_Resume() {
        return Job_Resume;
    }

    public void setJob_Resume(String job_Resume) {
        Job_Resume = job_Resume;
    }

    public String getData_Source() {
        return Data_Source;
    }

    public void setData_Source(String data_Source) {
        Data_Source = data_Source;
    }

    public String getH() {
        return H;
    }

    public void setH(String h) {
        H = h;
    }

    public String getJournalcount() {
        return Journalcount;
    }

    public void setJournalcount(String journalcount) {
        Journalcount = journalcount;
    }

    public String getQuotecount() {
        return Quotecount;
    }

    public void setQuotecount(String quotecount) {
        Quotecount = quotecount;
    }
}
