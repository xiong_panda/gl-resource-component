package com.gl.biz.city.po;

import java.util.Date;

public class PageInfoPO {

    private Integer id;
    private Integer page;
    private Integer count;
    private String indexname;
    private Integer size;
    private Integer CurrentData;
    private Integer from;
    private Date CreateTime;

    public PageInfoPO() {
    }

    public PageInfoPO(Integer id, Integer page, Integer count, String indexname, Integer size, Integer currentData, Integer from, Date createTime) {
        this.id = id;
        this.page = page;
        this.count = count;
        this.indexname = indexname;
        this.size = size;
        CurrentData = currentData;
        this.from = from;
        CreateTime = createTime;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getCurrentData() {
        return CurrentData;
    }

    public void setCurrentData(Integer currentData) {
        CurrentData = currentData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getIndexname() {
        return indexname;
    }

    public void setIndexname(String indexname) {
        this.indexname = indexname;
    }
}
