package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class ForeignPeriodicalPaperPO {
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 1)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 2)
    private String 	OrderID;
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	Full_Name;
    @FieldOrder(order = 5)
    private String 	Title;
    @FieldOrder(order = 6)
    private String 	Abstract;
    @FieldOrder(order = 7)
    private String 	Author;
    @FieldOrder(order = 8)
    private String 	Author_Name;
    @FieldOrder(order = 9)
    private String 	Author_Name2;
    @FieldOrder(order = 10)
    private String 	Keyword;
    @FieldOrder(order = 11)
    private String 	ORG_Name;
    @FieldOrder(order = 12)
    private String 	ORG_Name_Search;
    @FieldOrder(order = 13)
    private String 	Creator_ORG;
    @FieldOrder(order = 14)
    private String 	Date;
    @FieldOrder(order = 15)
    private String 	Year;
    @FieldOrder(order = 16)
    private String 	IID;
    @FieldOrder(order = 17)
    private String 	Doi;
    @FieldOrder(order = 18)
    private String 	Issn;
    @FieldOrder(order = 19)
    private String 	Eissn;
    @FieldOrder(order = 20)
    private String 	Author2;
    @FieldOrder(order = 21)
    private String 	Joucn;
    @FieldOrder(order = 22)
    private String 	Fjoucn;
    @FieldOrder(order = 23)
    private String 	Vol;
    @FieldOrder(order = 24)
    private String 	Per;
    @FieldOrder(order = 25)
    private String 	Pg;
    @FieldOrder(order = 26)
    private String 	Creator_ORG2;
    @FieldOrder(order = 27)
    private String 	Download_Date;
    @FieldOrder(order = 28)
    private String 	Pubtype;
    @FieldOrder(order = 29)
    private String 	Ynfree;
    @FieldOrder(order = 30)
    private String 	CID;
    @FieldOrder(order = 31)
    private String 	ZcID;
    @FieldOrder(order = 32)
    private String 	TzcID;
    @FieldOrder(order = 33)
    private String 	SzcID;
    @FieldOrder(order = 34)
    private String 	Cls_level3;
    @FieldOrder(order = 35)
    private String 	Discipline_Class;
    @FieldOrder(order = 36)
    private String 	Score;
    @FieldOrder(order = 37)
    private String 	Rn;
    @FieldOrder(order = 38)
    private String 	Bn;
    @FieldOrder(order = 39)
    private String 	Dn;
    @FieldOrder(order = 40)
    private String 	Ots;
    @FieldOrder(order = 41)
    private String 	Sci;
    @FieldOrder(order = 42)
    private String 	Ei;
    @FieldOrder(order = 43)
    private String 	Fund;
    @FieldOrder(order = 44)
    private String 	Fund_FullText_Search;
    @FieldOrder(order = 45)
    private String 	Fund_Anyname2;
    @FieldOrder(order = 46)
    private String 	F_Fund;
    @FieldOrder(order = 47)
    private String 	Core;
    @FieldOrder(order = 48)
    private String 	Data_Link;
    @FieldOrder(order = 49)
    private String 	Fund_Support;
    @FieldOrder(order = 50)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private java.util.Date CreateTime;

    public java.util.Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getFull_Name() {
        return Full_Name;
    }

    public void setFull_Name(String full_Name) {
        Full_Name = full_Name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getAuthor_Name() {
        return Author_Name;
    }

    public void setAuthor_Name(String author_Name) {
        Author_Name = author_Name;
    }

    public String getAuthor_Name2() {
        return Author_Name2;
    }

    public void setAuthor_Name2(String author_Name2) {
        Author_Name2 = author_Name2;
    }

    public String getKeyword() {
        return Keyword;
    }

    public void setKeyword(String keyword) {
        Keyword = keyword;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getCreator_ORG() {
        return Creator_ORG;
    }

    public void setCreator_ORG(String creator_ORG) {
        Creator_ORG = creator_ORG;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDoi() {
        return Doi;
    }

    public void setDoi(String doi) {
        Doi = doi;
    }

    public String getIssn() {
        return Issn;
    }

    public void setIssn(String issn) {
        Issn = issn;
    }

    public String getEissn() {
        return Eissn;
    }

    public void setEissn(String eissn) {
        Eissn = eissn;
    }

    public String getAuthor2() {
        return Author2;
    }

    public void setAuthor2(String author2) {
        Author2 = author2;
    }

    public String getJoucn() {
        return Joucn;
    }

    public void setJoucn(String joucn) {
        Joucn = joucn;
    }

    public String getFjoucn() {
        return Fjoucn;
    }

    public void setFjoucn(String fjoucn) {
        Fjoucn = fjoucn;
    }

    public String getVol() {
        return Vol;
    }

    public void setVol(String vol) {
        Vol = vol;
    }

    public String getPer() {
        return Per;
    }

    public void setPer(String per) {
        Per = per;
    }

    public String getPg() {
        return Pg;
    }

    public void setPg(String pg) {
        Pg = pg;
    }

    public String getCreator_ORG2() {
        return Creator_ORG2;
    }

    public void setCreator_ORG2(String creator_ORG2) {
        Creator_ORG2 = creator_ORG2;
    }

    public String getDownload_Date() {
        return Download_Date;
    }

    public void setDownload_Date(String download_Date) {
        Download_Date = download_Date;
    }

    public String getPubtype() {
        return Pubtype;
    }

    public void setPubtype(String pubtype) {
        Pubtype = pubtype;
    }

    public String getYnfree() {
        return Ynfree;
    }

    public void setYnfree(String ynfree) {
        Ynfree = ynfree;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getZcID() {
        return ZcID;
    }

    public void setZcID(String zcID) {
        ZcID = zcID;
    }

    public String getTzcID() {
        return TzcID;
    }

    public void setTzcID(String tzcID) {
        TzcID = tzcID;
    }

    public String getSzcID() {
        return SzcID;
    }

    public void setSzcID(String szcID) {
        SzcID = szcID;
    }

    public String getCls_level3() {
        return Cls_level3;
    }

    public void setCls_level3(String cls_level3) {
        Cls_level3 = cls_level3;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getScore() {
        return Score;
    }

    public void setScore(String score) {
        Score = score;
    }

    public String getRn() {
        return Rn;
    }

    public void setRn(String rn) {
        Rn = rn;
    }

    public String getBn() {
        return Bn;
    }

    public void setBn(String bn) {
        Bn = bn;
    }

    public String getDn() {
        return Dn;
    }

    public void setDn(String dn) {
        Dn = dn;
    }

    public String getOts() {
        return Ots;
    }

    public void setOts(String ots) {
        Ots = ots;
    }

    public String getSci() {
        return Sci;
    }

    public void setSci(String sci) {
        Sci = sci;
    }

    public String getEi() {
        return Ei;
    }

    public void setEi(String ei) {
        Ei = ei;
    }

    public String getFund() {
        return Fund;
    }

    public void setFund(String fund) {
        Fund = fund;
    }

    public String getFund_FullText_Search() {
        return Fund_FullText_Search;
    }

    public void setFund_FullText_Search(String fund_FullText_Search) {
        Fund_FullText_Search = fund_FullText_Search;
    }

    public String getFund_Anyname2() {
        return Fund_Anyname2;
    }

    public void setFund_Anyname2(String fund_Anyname2) {
        Fund_Anyname2 = fund_Anyname2;
    }

    public String getF_Fund() {
        return F_Fund;
    }

    public void setF_Fund(String f_Fund) {
        F_Fund = f_Fund;
    }

    public String getCore() {
        return Core;
    }

    public void setCore(String core) {
        Core = core;
    }

    public String getData_Link() {
        return Data_Link;
    }

    public void setData_Link(String data_Link) {
        Data_Link = data_Link;
    }

    public String getFund_Support() {
        return Fund_Support;
    }

    public void setFund_Support(String fund_Support) {
        Fund_Support = fund_Support;
    }
}
