package com.gl.biz.city.out.dto;

import com.gl.biz.city.anno.FieldOrder;
import io.swagger.annotations.ApiModelProperty;

/**
 * 企业库
 */
public class WanFangEnterpriseProductDatabaseOutDto implements java.io.Serializable {


    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;


    /**
     * 唯一编号
     **/

    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;


    /**
     * 全文索引
     **/

    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;


    /**
     * 企业名称:铭牌:简称:曾用名
     **/

    @ApiModelProperty(value = "企业名称:铭牌:简称:曾用名")
    @FieldOrder(order = 4)
    private String corpFullname;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 5)
    private String titleAnyname;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 6)
    private String anyname;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 7)
    private String acname;


    /**
     * 企业名称
     **/

    @ApiModelProperty(value = "企业名称")
    @FieldOrder(order = 8)
    private String corpName;


    /**
     * 铭牌
     **/

    @ApiModelProperty(value = "铭牌")
    @FieldOrder(order = 9)
    private String corpNameplate;


    /**
     * 简称
     **/

    @ApiModelProperty(value = "简称")
    @FieldOrder(order = 10)
    private String corpShortname;


    /**
     * 曾用名
     **/

    @ApiModelProperty(value = "曾用名")
    @FieldOrder(order = 11)
    private String corpOldname;


    /**
     * 负责人姓名
     **/

    @ApiModelProperty(value = "负责人姓名")
    @FieldOrder(order = 12)
    private String corpPerson;


    /**
     * 负责人
     **/

    @ApiModelProperty(value = "负责人")
    @FieldOrder(order = 13)
    private String corpMultiperson;


    /**
     * 省名
     **/

    @ApiModelProperty(value = "省名")
    @FieldOrder(order = 14)
    private String corpProv;


    /**
     * 市名
     **/

    @ApiModelProperty(value = "市名")
    @FieldOrder(order = 15)
    private String corpCity;


    /**
     * 县名
     **/

    @ApiModelProperty(value = "县名")
    @FieldOrder(order = 16)
    private String corpCoun;


    /**
     * 行政区代码
     **/

    @ApiModelProperty(value = "行政区代码")
    @FieldOrder(order = 17)
    private String corpRegion;


    /**
     * 地址
     **/

    @ApiModelProperty(value = "地址")
    @FieldOrder(order = 18)
    private String corpAddr;


    /**
     * 电话
     **/

    @ApiModelProperty(value = "电话")
    @FieldOrder(order = 19)
    private String telphone;


    /**
     * 区位号
     **/

    @ApiModelProperty(value = "区位号")
    @FieldOrder(order = 20)
    private String corpRecode;


    /**
     * 传真
     **/

    @ApiModelProperty(value = "传真")
    @FieldOrder(order = 21)
    private String corpFax;


    /**
     * 邮码
     **/

    @ApiModelProperty(value = "邮码")
    @FieldOrder(order = 22)
    private String postalCode;


    /**
     * 电子邮件
     **/

    @ApiModelProperty(value = "电子邮件")
    @FieldOrder(order = 23)
    private String corpEmail;


    /**
     * 网址
     **/

    @ApiModelProperty(value = "网址")
    @FieldOrder(order = 24)
    private String corpUrl;


    /**
     * 成立年代
     **/

    @ApiModelProperty(value = "成立年代")
    @FieldOrder(order = 25)
    private String corpFound;


    /**
     * 注册资金
     **/

    @ApiModelProperty(value = "注册资金")
    @FieldOrder(order = 26)
    private String corpMoney;


    /**
     * 固定资产
     **/

    @ApiModelProperty(value = "固定资产")
    @FieldOrder(order = 27)
    private String corpAsset;


    /**
     * 职工人数
     **/

    @ApiModelProperty(value = "职工人数")
    @FieldOrder(order = 28)
    private String corpEmps;


    /**
     * 技术人员数
     **/

    @ApiModelProperty(value = "技术人员数")
    @FieldOrder(order = 29)
    private String corpTechs;


    /**
     * 营业额
     **/

    @ApiModelProperty(value = "营业额")
    @FieldOrder(order = 30)
    private String corpTurnover;


    /**
     * 利税
     **/

    @ApiModelProperty(value = "利税")
    @FieldOrder(order = 31)
    private String corpTax;


    /**
     * 创汇额
     **/

    @ApiModelProperty(value = "创汇额")
    @FieldOrder(order = 32)
    private String corpExchange;


    /**
     * 进出口权
     **/

    @ApiModelProperty(value = "进出口权")
    @FieldOrder(order = 33)
    private String corpImpexp;


    /**
     * 性质与级别
     **/

    @ApiModelProperty(value = "性质与级别")
    @FieldOrder(order = 34)
    private String corpLevel;


    /**
     * 股票代码
     **/

    @ApiModelProperty(value = "股票代码")
    @FieldOrder(order = 35)
    private String corpStock;


    /**
     * 机构类型
     **/

    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 36)
    private String orgType;


    /**
     * 企业简介
     **/

    @ApiModelProperty(value = "企业简介")
    @FieldOrder(order = 37)
    private String corpIntro;


    /**
     * 企业占地面积
     **/

    @ApiModelProperty(value = "企业占地面积")
    @FieldOrder(order = 38)
    private String corpSpace;


    /**
     * 厂房办公面积
     **/

    @ApiModelProperty(value = "厂房办公面积")
    @FieldOrder(order = 39)
    private String corpArea;


    /**
     * 主管单位
     **/

    @ApiModelProperty(value = "主管单位")
    @FieldOrder(order = 40)
    private String corpCompunit;


    /**
     * 派出机构
     **/

    @ApiModelProperty(value = "派出机构")
    @FieldOrder(order = 41)
    private String corp;


    /**
     * 行业GBM
     **/

    @ApiModelProperty(value = "行业GBM")
    @FieldOrder(order = 42)
    private String igbm;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 43)
    private String igbmAnyname;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 44)
    private String igbmAnyname2;


    /**
     * 行业GBM
     **/

    @ApiModelProperty(value = "行业GBM")
    @FieldOrder(order = 45)
    private String figbm;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 46)
    private String figbmAnyname;


    /**
     * 机构名称
     **/

    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 47)
    private String orgName;


    /**
     * 机构名称(全文检索)
     **/

    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 48)
    private String orgNameSearch;


    /**
     * 机构全文检索
     **/

    @ApiModelProperty(value = "机构全文检索")
    @FieldOrder(order = 49)
    private String orgFulltextSearch;


    /**
     * 行业SIC
     **/

    @ApiModelProperty(value = "行业SIC")
    @FieldOrder(order = 50)
    private String isic;


    /**
     * 商标
     **/

    @ApiModelProperty(value = "商标")
    @FieldOrder(order = 51)
    private String brand;


    /**
     * 产品信息
     **/

    @ApiModelProperty(value = "产品信息")
    @FieldOrder(order = 52)
    private String productInfo;


    /**
     * 经营项目
     **/

    @ApiModelProperty(value = "经营项目")
    @FieldOrder(order = 53)
    private String businessProject;


    /**
     * 经营项目英
     **/

    @ApiModelProperty(value = "经营项目英")
    @FieldOrder(order = 54)
    private String businessProjectEn;


    /**
     * 产品关键词:英文产品关键词
     **/

    @ApiModelProperty(value = "产品关键词:英文产品关键词")
    @FieldOrder(order = 55)
    private String fullkeyword;


    /**
     * 产品关键词
     **/

    @ApiModelProperty(value = "产品关键词")
    @FieldOrder(order = 56)
    private String productKeyword;


    /**
     * 英文产品关键词
     **/

    @ApiModelProperty(value = "英文产品关键词")
    @FieldOrder(order = 57)
    private String englishProductKeyword;


    /**
     * 产品SIC
     **/

    @ApiModelProperty(value = "产品SIC")
    @FieldOrder(order = 58)
    private String psic;


    /**
     * 产品GBM
     **/

    @ApiModelProperty(value = "产品GBM")
    @FieldOrder(order = 59)
    private String pgbm;


    /**
     * 企业排名
     **/

    @ApiModelProperty(value = "企业排名")
    @FieldOrder(order = 60)
    private String corpRanking;


    /**
     * 重点行业
     **/

    @ApiModelProperty(value = "重点行业")
    @FieldOrder(order = 61)
    private String corpKeyent;


    /**
     * 成品数据
     **/

    @ApiModelProperty(value = "成品数据")
    @FieldOrder(order = 62)
    private String corpCpdata;


    /**
     * 电力数据
     **/

    @ApiModelProperty(value = "电力数据")
    @FieldOrder(order = 63)
    private String corpDldata;


    /**
     * 冶金数据
     **/

    @ApiModelProperty(value = "冶金数据")
    @FieldOrder(order = 64)
    private String corpYjdata;


    /**
     * 机构ID
     **/

    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 65)
    private String orgId;


    /**
     * --企业排名
     **/

    @ApiModelProperty(value = "--企业排名")
    @FieldOrder(order = 66)
    private String crankname;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 67)
    private String crankname1;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 68)
    private String crankname2;


    /**
     * --企业排名
     **/

    @ApiModelProperty(value = "--企业排名")
    @FieldOrder(order = 69)
    private String cranksource;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 70)
    private String cranksource1;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 71)
    private String cranksource2;


    /**
     * --企业排名
     **/

    @ApiModelProperty(value = "--企业排名")
    @FieldOrder(order = 72)
    private String cranktype;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 73)
    private String cranktype1;


    /**
     *
     **/

    @ApiModelProperty(value = "")
    @FieldOrder(order = 74)
    private String cranktype2;


    /**
     * 是否成品数据
     **/

    @ApiModelProperty(value = "是否成品数据")
    @FieldOrder(order = 75)
    private String isFinishedProductData;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFulltext() {
        return fulltext;
    }

    public void setFulltext(String fulltext) {
        this.fulltext = fulltext;
    }

    public String getCorpFullname() {
        return corpFullname;
    }

    public void setCorpFullname(String corpFullname) {
        this.corpFullname = corpFullname;
    }

    public String getTitleAnyname() {
        return titleAnyname;
    }

    public void setTitleAnyname(String titleAnyname) {
        this.titleAnyname = titleAnyname;
    }

    public String getAnyname() {
        return anyname;
    }

    public void setAnyname(String anyname) {
        this.anyname = anyname;
    }

    public String getAcname() {
        return acname;
    }

    public void setAcname(String acname) {
        this.acname = acname;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getCorpNameplate() {
        return corpNameplate;
    }

    public void setCorpNameplate(String corpNameplate) {
        this.corpNameplate = corpNameplate;
    }

    public String getCorpShortname() {
        return corpShortname;
    }

    public void setCorpShortname(String corpShortname) {
        this.corpShortname = corpShortname;
    }

    public String getCorpOldname() {
        return corpOldname;
    }

    public void setCorpOldname(String corpOldname) {
        this.corpOldname = corpOldname;
    }

    public String getCorpPerson() {
        return corpPerson;
    }

    public void setCorpPerson(String corpPerson) {
        this.corpPerson = corpPerson;
    }

    public String getCorpMultiperson() {
        return corpMultiperson;
    }

    public void setCorpMultiperson(String corpMultiperson) {
        this.corpMultiperson = corpMultiperson;
    }

    public String getCorpProv() {
        return corpProv;
    }

    public void setCorpProv(String corpProv) {
        this.corpProv = corpProv;
    }

    public String getCorpCity() {
        return corpCity;
    }

    public void setCorpCity(String corpCity) {
        this.corpCity = corpCity;
    }

    public String getCorpCoun() {
        return corpCoun;
    }

    public void setCorpCoun(String corpCoun) {
        this.corpCoun = corpCoun;
    }

    public String getCorpRegion() {
        return corpRegion;
    }

    public void setCorpRegion(String corpRegion) {
        this.corpRegion = corpRegion;
    }

    public String getCorpAddr() {
        return corpAddr;
    }

    public void setCorpAddr(String corpAddr) {
        this.corpAddr = corpAddr;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getCorpRecode() {
        return corpRecode;
    }

    public void setCorpRecode(String corpRecode) {
        this.corpRecode = corpRecode;
    }

    public String getCorpFax() {
        return corpFax;
    }

    public void setCorpFax(String corpFax) {
        this.corpFax = corpFax;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCorpEmail() {
        return corpEmail;
    }

    public void setCorpEmail(String corpEmail) {
        this.corpEmail = corpEmail;
    }

    public String getCorpUrl() {
        return corpUrl;
    }

    public void setCorpUrl(String corpUrl) {
        this.corpUrl = corpUrl;
    }

    public String getCorpFound() {
        return corpFound;
    }

    public void setCorpFound(String corpFound) {
        this.corpFound = corpFound;
    }

    public String getCorpMoney() {
        return corpMoney;
    }

    public void setCorpMoney(String corpMoney) {
        this.corpMoney = corpMoney;
    }

    public String getCorpAsset() {
        return corpAsset;
    }

    public void setCorpAsset(String corpAsset) {
        this.corpAsset = corpAsset;
    }

    public String getCorpEmps() {
        return corpEmps;
    }

    public void setCorpEmps(String corpEmps) {
        this.corpEmps = corpEmps;
    }

    public String getCorpTechs() {
        return corpTechs;
    }

    public void setCorpTechs(String corpTechs) {
        this.corpTechs = corpTechs;
    }

    public String getCorpTurnover() {
        return corpTurnover;
    }

    public void setCorpTurnover(String corpTurnover) {
        this.corpTurnover = corpTurnover;
    }

    public String getCorpTax() {
        return corpTax;
    }

    public void setCorpTax(String corpTax) {
        this.corpTax = corpTax;
    }

    public String getCorpExchange() {
        return corpExchange;
    }

    public void setCorpExchange(String corpExchange) {
        this.corpExchange = corpExchange;
    }

    public String getCorpImpexp() {
        return corpImpexp;
    }

    public void setCorpImpexp(String corpImpexp) {
        this.corpImpexp = corpImpexp;
    }

    public String getCorpLevel() {
        return corpLevel;
    }

    public void setCorpLevel(String corpLevel) {
        this.corpLevel = corpLevel;
    }

    public String getCorpStock() {
        return corpStock;
    }

    public void setCorpStock(String corpStock) {
        this.corpStock = corpStock;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getCorpIntro() {
        return corpIntro;
    }

    public void setCorpIntro(String corpIntro) {
        this.corpIntro = corpIntro;
    }

    public String getCorpSpace() {
        return corpSpace;
    }

    public void setCorpSpace(String corpSpace) {
        this.corpSpace = corpSpace;
    }

    public String getCorpArea() {
        return corpArea;
    }

    public void setCorpArea(String corpArea) {
        this.corpArea = corpArea;
    }

    public String getCorpCompunit() {
        return corpCompunit;
    }

    public void setCorpCompunit(String corpCompunit) {
        this.corpCompunit = corpCompunit;
    }

    public String getCorp() {
        return corp;
    }

    public void setCorp(String corp) {
        this.corp = corp;
    }

    public String getIgbm() {
        return igbm;
    }

    public void setIgbm(String igbm) {
        this.igbm = igbm;
    }

    public String getIgbmAnyname() {
        return igbmAnyname;
    }

    public void setIgbmAnyname(String igbmAnyname) {
        this.igbmAnyname = igbmAnyname;
    }

    public String getIgbmAnyname2() {
        return igbmAnyname2;
    }

    public void setIgbmAnyname2(String igbmAnyname2) {
        this.igbmAnyname2 = igbmAnyname2;
    }

    public String getFigbm() {
        return figbm;
    }

    public void setFigbm(String figbm) {
        this.figbm = figbm;
    }

    public String getFigbmAnyname() {
        return figbmAnyname;
    }

    public void setFigbmAnyname(String figbmAnyname) {
        this.figbmAnyname = figbmAnyname;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgNameSearch() {
        return orgNameSearch;
    }

    public void setOrgNameSearch(String orgNameSearch) {
        this.orgNameSearch = orgNameSearch;
    }

    public String getOrgFulltextSearch() {
        return orgFulltextSearch;
    }

    public void setOrgFulltextSearch(String orgFulltextSearch) {
        this.orgFulltextSearch = orgFulltextSearch;
    }

    public String getIsic() {
        return isic;
    }

    public void setIsic(String isic) {
        this.isic = isic;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getBusinessProject() {
        return businessProject;
    }

    public void setBusinessProject(String businessProject) {
        this.businessProject = businessProject;
    }

    public String getBusinessProjectEn() {
        return businessProjectEn;
    }

    public void setBusinessProjectEn(String businessProjectEn) {
        this.businessProjectEn = businessProjectEn;
    }

    public String getFullkeyword() {
        return fullkeyword;
    }

    public void setFullkeyword(String fullkeyword) {
        this.fullkeyword = fullkeyword;
    }

    public String getProductKeyword() {
        return productKeyword;
    }

    public void setProductKeyword(String productKeyword) {
        this.productKeyword = productKeyword;
    }

    public String getEnglishProductKeyword() {
        return englishProductKeyword;
    }

    public void setEnglishProductKeyword(String englishProductKeyword) {
        this.englishProductKeyword = englishProductKeyword;
    }

    public String getPsic() {
        return psic;
    }

    public void setPsic(String psic) {
        this.psic = psic;
    }

    public String getPgbm() {
        return pgbm;
    }

    public void setPgbm(String pgbm) {
        this.pgbm = pgbm;
    }

    public String getCorpRanking() {
        return corpRanking;
    }

    public void setCorpRanking(String corpRanking) {
        this.corpRanking = corpRanking;
    }

    public String getCorpKeyent() {
        return corpKeyent;
    }

    public void setCorpKeyent(String corpKeyent) {
        this.corpKeyent = corpKeyent;
    }

    public String getCorpCpdata() {
        return corpCpdata;
    }

    public void setCorpCpdata(String corpCpdata) {
        this.corpCpdata = corpCpdata;
    }

    public String getCorpDldata() {
        return corpDldata;
    }

    public void setCorpDldata(String corpDldata) {
        this.corpDldata = corpDldata;
    }

    public String getCorpYjdata() {
        return corpYjdata;
    }

    public void setCorpYjdata(String corpYjdata) {
        this.corpYjdata = corpYjdata;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCrankname() {
        return crankname;
    }

    public void setCrankname(String crankname) {
        this.crankname = crankname;
    }

    public String getCrankname1() {
        return crankname1;
    }

    public void setCrankname1(String crankname1) {
        this.crankname1 = crankname1;
    }

    public String getCrankname2() {
        return crankname2;
    }

    public void setCrankname2(String crankname2) {
        this.crankname2 = crankname2;
    }

    public String getCranksource() {
        return cranksource;
    }

    public void setCranksource(String cranksource) {
        this.cranksource = cranksource;
    }

    public String getCranksource1() {
        return cranksource1;
    }

    public void setCranksource1(String cranksource1) {
        this.cranksource1 = cranksource1;
    }

    public String getCranksource2() {
        return cranksource2;
    }

    public void setCranksource2(String cranksource2) {
        this.cranksource2 = cranksource2;
    }

    public String getCranktype() {
        return cranktype;
    }

    public void setCranktype(String cranktype) {
        this.cranktype = cranktype;
    }

    public String getCranktype1() {
        return cranktype1;
    }

    public void setCranktype1(String cranktype1) {
        this.cranktype1 = cranktype1;
    }

    public String getCranktype2() {
        return cranktype2;
    }

    public void setCranktype2(String cranktype2) {
        this.cranktype2 = cranktype2;
    }

    public String getIsFinishedProductData() {
        return isFinishedProductData;
    }

    public void setIsFinishedProductData(String isFinishedProductData) {
        this.isFinishedProductData = isFinishedProductData;
    }
}
