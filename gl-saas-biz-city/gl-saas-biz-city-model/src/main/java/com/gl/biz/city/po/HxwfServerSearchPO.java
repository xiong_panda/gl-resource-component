package com.gl.biz.city.po;

import java.util.Date;

public class HxwfServerSearchPO {
    private String id;
    private String userName;
    private String userField;
    private String userIndustry;
    private String userType;
    private String userArea;
    private String userId;
    private String searchKeyword;
    private String serviceOrigin;
    private String sourcesName;
    private String sourcesOrigin;
    private String sourcesId;
    private String flag;
    private Date useTime;
    private String collectSource;
    private Date collectTime;
    private String collectType;
    private String userIndustryId;
    private String userFieldId;
    private String remoteFlag;

    public String getRemoteFlag() {
        return remoteFlag;
    }

    public void setRemoteFlag(String remoteFlag) {
        this.remoteFlag = remoteFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserField() {
        return userField;
    }

    public void setUserField(String userField) {
        this.userField = userField;
    }

    public String getUserIndustry() {
        return userIndustry;
    }

    public void setUserIndustry(String userIndustry) {
        this.userIndustry = userIndustry;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserArea() {
        return userArea;
    }

    public void setUserArea(String userArea) {
        this.userArea = userArea;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getServiceOrigin() {
        return serviceOrigin;
    }

    public void setServiceOrigin(String serviceOrigin) {
        this.serviceOrigin = serviceOrigin;
    }

    public String getSourcesName() {
        return sourcesName;
    }

    public void setSourcesName(String sourcesName) {
        this.sourcesName = sourcesName;
    }

    public String getSourcesOrigin() {
        return sourcesOrigin;
    }

    public void setSourcesOrigin(String sourcesOrigin) {
        this.sourcesOrigin = sourcesOrigin;
    }

    public String getSourcesId() {
        return sourcesId;
    }

    public void setSourcesId(String sourcesId) {
        this.sourcesId = sourcesId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Date getUseTime() {
        return useTime;
    }

    public void setUseTime(Date useTime) {
        this.useTime = useTime;
    }

    public String getCollectSource() {
        return collectSource;
    }

    public void setCollectSource(String collectSource) {
        this.collectSource = collectSource;
    }

    public Date getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(Date collectTime) {
        this.collectTime = collectTime;
    }

    public String getCollectType() {
        return collectType;
    }

    public void setCollectType(String collectType) {
        this.collectType = collectType;
    }

    public String getUserIndustryId() {
        return userIndustryId;
    }

    public void setUserIndustryId(String userIndustryId) {
        this.userIndustryId = userIndustryId;
    }

    public String getUserFieldId() {
        return userFieldId;
    }

    public void setUserFieldId(String userFieldId) {
        this.userFieldId = userFieldId;
    }
}
