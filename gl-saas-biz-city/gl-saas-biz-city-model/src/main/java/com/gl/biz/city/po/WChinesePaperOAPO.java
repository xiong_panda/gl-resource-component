package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


//核心资源数据 中文期刊
public class WChinesePaperOAPO {
    @FieldOrder(order = 1)	private String 	FullText;
    @FieldOrder(order = 2)	private String 	ID;
    @FieldOrder(order = 3)	private String 	F_ID;
    @FieldOrder(order = 4)	private String 	TITLE;
    @FieldOrder(order = 5)	private String 	ABSTRACT;
    @FieldOrder(order = 6)	private String 	CAB;
    @FieldOrder(order = 7)	private String 	AUTHOR;
    @FieldOrder(order = 8)	private String 	AUTHOR_AUTHOR_ANYNAME;
    @FieldOrder(order = 9)	private String 	AUTHOR_ANYNAME;
    @FieldOrder(order = 10)	private String 	KEYWORD;
    @FieldOrder(order = 11)	private String 	CID;
    @FieldOrder(order = 12)	private String 	ORG;
    @FieldOrder(order = 13)	private String 	ORG_ORG_ANYNAME;
    @FieldOrder(order = 14)	private String 	ORG_ANYNAME;
    @FieldOrder(order = 15)	private String 	DATE;
    @FieldOrder(order = 16)	private String 	YEAR;
    @FieldOrder(order = 17)	private String 	IID;
    @FieldOrder(order = 18)	private String 	DOI;
    @FieldOrder(order = 19)	private String 	ISSN;
    @FieldOrder(order = 20)	private String 	EISSN;
    @FieldOrder(order = 21)	private String 	AU;
    @FieldOrder(order = 22)	private String 	JOUCN;
    @FieldOrder(order = 23)	private String 	FJOUCN;
    @FieldOrder(order = 24)	private String 	VOL;
    @FieldOrder(order = 25)	private String 	PER;
    @FieldOrder(order = 26)	private String 	PG;
    @FieldOrder(order = 27)	private String 	FORGC;
    @FieldOrder(order = 28)	private String 	DOWNLOAD;
    @FieldOrder(order = 29)	private String 	PUBTYPE;
    @FieldOrder(order = 30)	private String 	YNFREE;
    @FieldOrder(order = 31)	private String 	DATALINK;
    @FieldOrder(order = 32)	private String 	SELFFL;
    @FieldOrder(order = 33)	private String 	IISSNE;
    @FieldOrder(order = 34)	private String 	IISSNP;
    @FieldOrder(order = 35)	private String 	ZCID;
    @FieldOrder(order = 36)	private String 	DID;
    @FieldOrder(order = 37)	private String 	TZCID;
    @FieldOrder(order = 38)	private String 	SZCID;
    @FieldOrder(order = 39)	private String 	DZCID;
    @FieldOrder(order = 40)	private String 	SCORE;
    @FieldOrder(order = 41)	private String 	RN;
    @FieldOrder(order = 42)	private String 	BN;
    @FieldOrder(order = 43)	private String 	DN;
    @FieldOrder(order = 44)	private String 	OTS;

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getCAB() {
        return CAB;
    }

    public void setCAB(String CAB) {
        this.CAB = CAB;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAUTHOR_AUTHOR_ANYNAME() {
        return AUTHOR_AUTHOR_ANYNAME;
    }

    public void setAUTHOR_AUTHOR_ANYNAME(String AUTHOR_AUTHOR_ANYNAME) {
        this.AUTHOR_AUTHOR_ANYNAME = AUTHOR_AUTHOR_ANYNAME;
    }

    public String getAUTHOR_ANYNAME() {
        return AUTHOR_ANYNAME;
    }

    public void setAUTHOR_ANYNAME(String AUTHOR_ANYNAME) {
        this.AUTHOR_ANYNAME = AUTHOR_ANYNAME;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getISSN() {
        return ISSN;
    }

    public void setISSN(String ISSN) {
        this.ISSN = ISSN;
    }

    public String getEISSN() {
        return EISSN;
    }

    public void setEISSN(String EISSN) {
        this.EISSN = EISSN;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getJOUCN() {
        return JOUCN;
    }

    public void setJOUCN(String JOUCN) {
        this.JOUCN = JOUCN;
    }

    public String getFJOUCN() {
        return FJOUCN;
    }

    public void setFJOUCN(String FJOUCN) {
        this.FJOUCN = FJOUCN;
    }

    public String getVOL() {
        return VOL;
    }

    public void setVOL(String VOL) {
        this.VOL = VOL;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getPG() {
        return PG;
    }

    public void setPG(String PG) {
        this.PG = PG;
    }

    public String getFORGC() {
        return FORGC;
    }

    public void setFORGC(String FORGC) {
        this.FORGC = FORGC;
    }

    public String getDOWNLOAD() {
        return DOWNLOAD;
    }

    public void setDOWNLOAD(String DOWNLOAD) {
        this.DOWNLOAD = DOWNLOAD;
    }

    public String getPUBTYPE() {
        return PUBTYPE;
    }

    public void setPUBTYPE(String PUBTYPE) {
        this.PUBTYPE = PUBTYPE;
    }

    public String getYNFREE() {
        return YNFREE;
    }

    public void setYNFREE(String YNFREE) {
        this.YNFREE = YNFREE;
    }

    public String getDATALINK() {
        return DATALINK;
    }

    public void setDATALINK(String DATALINK) {
        this.DATALINK = DATALINK;
    }

    public String getSELFFL() {
        return SELFFL;
    }

    public void setSELFFL(String SELFFL) {
        this.SELFFL = SELFFL;
    }

    public String getIISSNE() {
        return IISSNE;
    }

    public void setIISSNE(String IISSNE) {
        this.IISSNE = IISSNE;
    }

    public String getIISSNP() {
        return IISSNP;
    }

    public void setIISSNP(String IISSNP) {
        this.IISSNP = IISSNP;
    }

    public String getZCID() {
        return ZCID;
    }

    public void setZCID(String ZCID) {
        this.ZCID = ZCID;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getSZCID() {
        return SZCID;
    }

    public void setSZCID(String SZCID) {
        this.SZCID = SZCID;
    }

    public String getDZCID() {
        return DZCID;
    }

    public void setDZCID(String DZCID) {
        this.DZCID = DZCID;
    }

    public String getSCORE() {
        return SCORE;
    }

    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    public String getRN() {
        return RN;
    }

    public void setRN(String RN) {
        this.RN = RN;
    }

    public String getBN() {
        return BN;
    }

    public void setBN(String BN) {
        this.BN = BN;
    }

    public String getDN() {
        return DN;
    }

    public void setDN(String DN) {
        this.DN = DN;
    }

    public String getOTS() {
        return OTS;
    }

    public void setOTS(String OTS) {
        this.OTS = OTS;
    }
}
