package com.gl.biz.city.out.dto;


/**
 *汽车专利清单
 * 
 */
public class CarPatentBomOutDto  {

	/** 主键 **/
	private String fPkid;

	/** 配件名称 **/
	private String iteminfoItemname;

	/** 配件规格 **/
	private String specs;

	/** 车品牌 **/
	private String carBrand;

	/** 车型 **/
	private String motorcycleType;

	/** 专利所属单位 **/
	private String patentOwnedUnits;

	/** 供应商 **/
	private String supplier;

	/** 专利数 **/
	private Integer patentsNum;
	
	private String isMake ; 
	public String getfPkid() {
		return fPkid;
	}

	public String getIsMake() {
		return isMake;
	}

	public void setIsMake(String isMake) {
		this.isMake = isMake;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getIteminfoItemname() {
		return iteminfoItemname;
	}

	public void setIteminfoItemname(String iteminfoItemname) {
		this.iteminfoItemname = iteminfoItemname;
	}

	public String getSpecs() {
		return specs;
	}

	public void setSpecs(String specs) {
		this.specs = specs;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getMotorcycleType() {
		return motorcycleType;
	}

	public void setMotorcycleType(String motorcycleType) {
		this.motorcycleType = motorcycleType;
	}

	public String getPatentOwnedUnits() {
		return patentOwnedUnits;
	}

	public void setPatentOwnedUnits(String patentOwnedUnits) {
		this.patentOwnedUnits = patentOwnedUnits;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Integer getPatentsNum() {
		return patentsNum;
	}

	public void setPatentsNum(Integer patentsNum) {
		this.patentsNum = patentsNum;
	}

	
}
