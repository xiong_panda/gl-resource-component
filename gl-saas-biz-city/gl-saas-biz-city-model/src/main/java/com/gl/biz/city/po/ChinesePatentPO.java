package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据
public class ChinesePatentPO {
    @FieldOrder(order = 1)
    private String orderId;
    @FieldOrder(order = 1000)
    private String id;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String fullText;
    @FieldOrder(order = 4)
    private String fullName;
    @FieldOrder(order = 5)
    private String Abstract;
    @FieldOrder(order = 6)
    private String orgAgency;
    @FieldOrder(order = 7)
    private String agent;
    @FieldOrder(order = 8)
    private String publicationNo;
    @FieldOrder(order = 9)
    private Date publicationDate;
    @FieldOrder(order = 10)
    private String author;
    @FieldOrder(order = 11)
    private String fAuthor;
    @FieldOrder(order = 12)
    private String author2;
    @FieldOrder(order = 13)
    private String authorName;
    @FieldOrder(order = 14)
    private String authorName2;
    @FieldOrder(order = 15)
    private String cate;
    @FieldOrder(order = 16)
    private String cdId;
    @FieldOrder(order = 17)
    private Date createDate;
    @FieldOrder(order = 18)
    private String classCode;
    @FieldOrder(order = 19)
    private String countryCode;
    @FieldOrder(order = 20)
    private Date date;
    @FieldOrder(order = 21)
    private String databaseIdentity;
    @FieldOrder(order = 22)
    private Date dec;
    @FieldOrder(order = 23)
    private String disciplineClass;
    @FieldOrder(order = 24)
    private String indCla;
    @FieldOrder(order = 25)
    private String lawStatus;
    @FieldOrder(order = 26)
    private String ls;
    @FieldOrder(order = 27)
    private String orgNormName;
    @FieldOrder(order = 28)
    private String intApp;
    @FieldOrder(order = 29)
    private String intPush;
    @FieldOrder(order = 30)
    private String invPat;
    @FieldOrder(order = 31)
    private String keyWord;
    @FieldOrder(order = 32)
    private String mainClassCode;
    @FieldOrder(order = 33)
    private String cls;
    @FieldOrder(order = 34)
    private String clsLevel2;
    @FieldOrder(order = 35)
    private String originRequestNumber;
    @FieldOrder(order = 36)
    private String org;
    @FieldOrder(order = 37)
    private String orgNameSearch;
    @FieldOrder(order = 38)
    private String orgAnyname2Name;
    @FieldOrder(order = 39)
    private String orgNormName1;
    @FieldOrder(order = 40)
    private String orgType;
    @FieldOrder(order = 41)
    private String patentNumberName;
    @FieldOrder(order = 42)
    private String patentTypeName;
    @FieldOrder(order = 43)
    private String pages;
    @FieldOrder(order = 44)
    private String priority;
    @FieldOrder(order = 45)
    private String pubPath;
    @FieldOrder(order = 46)
    private String reference;
    @FieldOrder(order = 47)
    private String requestAddress;
    @FieldOrder(order = 48)
    private String requestNumber;
    @FieldOrder(order = 49)
    private String requestNo;
    @FieldOrder(order = 50)
    private String requestPeople;
    @FieldOrder(order = 51)
    private String reviewer;
    @FieldOrder(order = 52)
    private String serverAddress;
    @FieldOrder(order = 53)
    private String dataSource;
    @FieldOrder(order = 54)
    private String title1;
    @FieldOrder(order = 55)
    private String title2;
    @FieldOrder(order = 56)
    private Date yannodate;
    @FieldOrder(order = 57)
    private String date1;
    @FieldOrder(order = 58)
    private String mainClassCode2;
    @FieldOrder(order = 59)
    private String smcls;
    @FieldOrder(order = 60)
    private String tmcls;
    @FieldOrder(order = 61)
    private String ckey;
    @FieldOrder(order = 62)
    private String orgId;
    @FieldOrder(order = 63)
    private String orgHierarchyId;
    @FieldOrder(order = 64)
    private String orgProvince;
    @FieldOrder(order = 65)
    private String orgCity;
    @FieldOrder(order = 66)
    private String endOrgType;
    @FieldOrder(order = 67)
    private String fOrgId;
    @FieldOrder(order = 68)
    private String orgFristFinalId;
    @FieldOrder(order = 69)
    private String orgFristHierarchyId;
    @FieldOrder(order = 70)
    private String orgFristProvince;
    @FieldOrder(order = 71)
    private String orgFristProvince2;
    @FieldOrder(order = 72)
    private String orgFristFinalCity;
    @FieldOrder(order = 73)
    private String orgFristCity;
    @FieldOrder(order = 74)
    private String orgFristFinalType;
    @FieldOrder(order = 75)
    private String orgFristType;
    @FieldOrder(order = 76)
    private String authorId;
    @FieldOrder(order = 77)
    private String orgNum;
    @FieldOrder(order = 78)
    private String literatureWeight;
    @FieldOrder(order = 79)
    private String quarter;
    @FieldOrder(order = 80)
    private String halfyear;
    @FieldOrder(order = 81)
    private String errorCode;
    @FieldOrder(order = 82)
    private String author1;
    @FieldOrder(order = 83)
    private String authorInfoName;
    @FieldOrder(order = 84)
    private String authorInfoOrder;
    @FieldOrder(order = 85)
    private String authorInfoUnit;
    @FieldOrder(order = 86)
    private String authorInfoUnitOrgLevel1;
    @FieldOrder(order = 87)
    private String authorInfoUnitOrgLevel2;
    @FieldOrder(order = 88)
    private String authorInfoUnitType;
    @FieldOrder(order = 89)
    private String authorInfoUnitProvince;
    @FieldOrder(order = 90)
    private String authorInfoUnitCity;
    @FieldOrder(order = 91)
    private String authorInfoUnitCounty;
    @FieldOrder(order = 92)
    private String authorInfoId;
    @FieldOrder(order = 93)
    private String authorInfoUnitId;
    @FieldOrder(order = 94)
    private String orgInfo;
    @FieldOrder(order = 95)
    private String orgInfoName;
    @FieldOrder(order = 96)
    private String orgInfoOrder;
    @FieldOrder(order = 97)
    private String orgInfoType;
    @FieldOrder(order = 98)
    private String orgInfoProvince;
    @FieldOrder(order = 99)
    private String orgInfoCity;
    @FieldOrder(order = 100)
    private String orgInfoCounty;
    @FieldOrder(order = 101)
    private String orgInfoHierarchy;
    @FieldOrder(order = 102)
    private String orgInfoLevel1;
    @FieldOrder(order = 103)
    private String orgInfoLevel2;
    @FieldOrder(order = 104)
    private String orgInfoLevel3;
    @FieldOrder(order = 105)
    private String orgInfoLevel4;
    @FieldOrder(order = 106)
    private String orgInfoLevel5;
    @FieldOrder(order = 107)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getOrgAgency() {
        return orgAgency;
    }

    public void setOrgAgency(String orgAgency) {
        this.orgAgency = orgAgency;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getPublicationNo() {
        return publicationNo;
    }

    public void setPublicationNo(String publicationNo) {
        this.publicationNo = publicationNo;
    }



    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getfAuthor() {
        return fAuthor;
    }

    public void setfAuthor(String fAuthor) {
        this.fAuthor = fAuthor;
    }

    public String getAuthor2() {
        return author2;
    }

    public void setAuthor2(String author2) {
        this.author2 = author2;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorName2() {
        return authorName2;
    }

    public void setAuthorName2(String authorName2) {
        this.authorName2 = authorName2;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getCdId() {
        return cdId;
    }

    public void setCdId(String cdId) {
        this.cdId = cdId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDatabaseIdentity() {
        return databaseIdentity;
    }

    public void setDatabaseIdentity(String databaseIdentity) {
        this.databaseIdentity = databaseIdentity;
    }

    public Date getDec() {
        return dec;
    }

    public void setDec(Date dec) {
        this.dec = dec;
    }

    public String getDisciplineClass() {
        return disciplineClass;
    }

    public void setDisciplineClass(String disciplineClass) {
        this.disciplineClass = disciplineClass;
    }

    public String getIndCla() {
        return indCla;
    }

    public void setIndCla(String indCla) {
        this.indCla = indCla;
    }

    public String getLawStatus() {
        return lawStatus;
    }

    public void setLawStatus(String lawStatus) {
        this.lawStatus = lawStatus;
    }

    public String getLs() {
        return ls;
    }

    public void setLs(String ls) {
        this.ls = ls;
    }

    public String getOrgNormName() {
        return orgNormName;
    }

    public void setOrgNormName(String orgNormName) {
        this.orgNormName = orgNormName;
    }

    public String getIntApp() {
        return intApp;
    }

    public void setIntApp(String intApp) {
        this.intApp = intApp;
    }

    public String getIntPush() {
        return intPush;
    }

    public void setIntPush(String intPush) {
        this.intPush = intPush;
    }

    public String getInvPat() {
        return invPat;
    }

    public void setInvPat(String invPat) {
        this.invPat = invPat;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getMainClassCode() {
        return mainClassCode;
    }

    public void setMainClassCode(String mainClassCode) {
        this.mainClassCode = mainClassCode;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getClsLevel2() {
        return clsLevel2;
    }

    public void setClsLevel2(String clsLevel2) {
        this.clsLevel2 = clsLevel2;
    }

    public String getOriginRequestNumber() {
        return originRequestNumber;
    }

    public void setOriginRequestNumber(String originRequestNumber) {
        this.originRequestNumber = originRequestNumber;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getOrgNameSearch() {
        return orgNameSearch;
    }

    public void setOrgNameSearch(String orgNameSearch) {
        this.orgNameSearch = orgNameSearch;
    }

    public String getOrgAnyname2Name() {
        return orgAnyname2Name;
    }

    public void setOrgAnyname2Name(String orgAnyname2Name) {
        this.orgAnyname2Name = orgAnyname2Name;
    }

    public String getOrgNormName1() {
        return orgNormName1;
    }

    public void setOrgNormName1(String orgNormName1) {
        this.orgNormName1 = orgNormName1;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getPatentNumberName() {
        return patentNumberName;
    }

    public void setPatentNumberName(String patentNumberName) {
        this.patentNumberName = patentNumberName;
    }

    public String getPatentTypeName() {
        return patentTypeName;
    }

    public void setPatentTypeName(String patentTypeName) {
        this.patentTypeName = patentTypeName;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPubPath() {
        return pubPath;
    }

    public void setPubPath(String pubPath) {
        this.pubPath = pubPath;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRequestAddress() {
        return requestAddress;
    }

    public void setRequestAddress(String requestAddress) {
        this.requestAddress = requestAddress;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getRequestPeople() {
        return requestPeople;
    }

    public void setRequestPeople(String requestPeople) {
        this.requestPeople = requestPeople;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getYannodate() {
        return yannodate;
    }

    public void setYannodate(Date yannodate) {
        this.yannodate = yannodate;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getMainClassCode2() {
        return mainClassCode2;
    }

    public void setMainClassCode2(String mainClassCode2) {
        this.mainClassCode2 = mainClassCode2;
    }

    public String getSmcls() {
        return smcls;
    }

    public void setSmcls(String smcls) {
        this.smcls = smcls;
    }

    public String getTmcls() {
        return tmcls;
    }

    public void setTmcls(String tmcls) {
        this.tmcls = tmcls;
    }

    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgHierarchyId() {
        return orgHierarchyId;
    }

    public void setOrgHierarchyId(String orgHierarchyId) {
        this.orgHierarchyId = orgHierarchyId;
    }

    public String getOrgProvince() {
        return orgProvince;
    }

    public void setOrgProvince(String orgProvince) {
        this.orgProvince = orgProvince;
    }

    public String getOrgCity() {
        return orgCity;
    }

    public void setOrgCity(String orgCity) {
        this.orgCity = orgCity;
    }

    public String getEndOrgType() {
        return endOrgType;
    }

    public void setEndOrgType(String endOrgType) {
        this.endOrgType = endOrgType;
    }

    public String getfOrgId() {
        return fOrgId;
    }

    public void setfOrgId(String fOrgId) {
        this.fOrgId = fOrgId;
    }

    public String getOrgFristFinalId() {
        return orgFristFinalId;
    }

    public void setOrgFristFinalId(String orgFristFinalId) {
        this.orgFristFinalId = orgFristFinalId;
    }

    public String getOrgFristHierarchyId() {
        return orgFristHierarchyId;
    }

    public void setOrgFristHierarchyId(String orgFristHierarchyId) {
        this.orgFristHierarchyId = orgFristHierarchyId;
    }

    public String getOrgFristProvince() {
        return orgFristProvince;
    }

    public void setOrgFristProvince(String orgFristProvince) {
        this.orgFristProvince = orgFristProvince;
    }

    public String getOrgFristProvince2() {
        return orgFristProvince2;
    }

    public void setOrgFristProvince2(String orgFristProvince2) {
        this.orgFristProvince2 = orgFristProvince2;
    }

    public String getOrgFristFinalCity() {
        return orgFristFinalCity;
    }

    public void setOrgFristFinalCity(String orgFristFinalCity) {
        this.orgFristFinalCity = orgFristFinalCity;
    }

    public String getOrgFristCity() {
        return orgFristCity;
    }

    public void setOrgFristCity(String orgFristCity) {
        this.orgFristCity = orgFristCity;
    }

    public String getOrgFristFinalType() {
        return orgFristFinalType;
    }

    public void setOrgFristFinalType(String orgFristFinalType) {
        this.orgFristFinalType = orgFristFinalType;
    }

    public String getOrgFristType() {
        return orgFristType;
    }

    public void setOrgFristType(String orgFristType) {
        this.orgFristType = orgFristType;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getOrgNum() {
        return orgNum;
    }

    public void setOrgNum(String orgNum) {
        this.orgNum = orgNum;
    }

    public String getLiteratureWeight() {
        return literatureWeight;
    }

    public void setLiteratureWeight(String literatureWeight) {
        this.literatureWeight = literatureWeight;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getHalfyear() {
        return halfyear;
    }

    public void setHalfyear(String halfyear) {
        this.halfyear = halfyear;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getAuthor1() {
        return author1;
    }

    public void setAuthor1(String author1) {
        this.author1 = author1;
    }

    public String getAuthorInfoName() {
        return authorInfoName;
    }

    public void setAuthorInfoName(String authorInfoName) {
        this.authorInfoName = authorInfoName;
    }

    public String getAuthorInfoOrder() {
        return authorInfoOrder;
    }

    public void setAuthorInfoOrder(String authorInfoOrder) {
        this.authorInfoOrder = authorInfoOrder;
    }

    public String getAuthorInfoUnit() {
        return authorInfoUnit;
    }

    public void setAuthorInfoUnit(String authorInfoUnit) {
        this.authorInfoUnit = authorInfoUnit;
    }

    public String getAuthorInfoUnitOrgLevel1() {
        return authorInfoUnitOrgLevel1;
    }

    public void setAuthorInfoUnitOrgLevel1(String authorInfoUnitOrgLevel1) {
        this.authorInfoUnitOrgLevel1 = authorInfoUnitOrgLevel1;
    }

    public String getAuthorInfoUnitOrgLevel2() {
        return authorInfoUnitOrgLevel2;
    }

    public void setAuthorInfoUnitOrgLevel2(String authorInfoUnitOrgLevel2) {
        this.authorInfoUnitOrgLevel2 = authorInfoUnitOrgLevel2;
    }

    public String getAuthorInfoUnitType() {
        return authorInfoUnitType;
    }

    public void setAuthorInfoUnitType(String authorInfoUnitType) {
        this.authorInfoUnitType = authorInfoUnitType;
    }

    public String getAuthorInfoUnitProvince() {
        return authorInfoUnitProvince;
    }

    public void setAuthorInfoUnitProvince(String authorInfoUnitProvince) {
        this.authorInfoUnitProvince = authorInfoUnitProvince;
    }

    public String getAuthorInfoUnitCity() {
        return authorInfoUnitCity;
    }

    public void setAuthorInfoUnitCity(String authorInfoUnitCity) {
        this.authorInfoUnitCity = authorInfoUnitCity;
    }

    public String getAuthorInfoUnitCounty() {
        return authorInfoUnitCounty;
    }

    public void setAuthorInfoUnitCounty(String authorInfoUnitCounty) {
        this.authorInfoUnitCounty = authorInfoUnitCounty;
    }

    public String getAuthorInfoId() {
        return authorInfoId;
    }

    public void setAuthorInfoId(String authorInfoId) {
        this.authorInfoId = authorInfoId;
    }

    public String getAuthorInfoUnitId() {
        return authorInfoUnitId;
    }

    public void setAuthorInfoUnitId(String authorInfoUnitId) {
        this.authorInfoUnitId = authorInfoUnitId;
    }

    public String getOrgInfo() {
        return orgInfo;
    }

    public void setOrgInfo(String orgInfo) {
        this.orgInfo = orgInfo;
    }

    public String getOrgInfoName() {
        return orgInfoName;
    }

    public void setOrgInfoName(String orgInfoName) {
        this.orgInfoName = orgInfoName;
    }

    public String getOrgInfoOrder() {
        return orgInfoOrder;
    }

    public void setOrgInfoOrder(String orgInfoOrder) {
        this.orgInfoOrder = orgInfoOrder;
    }

    public String getOrgInfoType() {
        return orgInfoType;
    }

    public void setOrgInfoType(String orgInfoType) {
        this.orgInfoType = orgInfoType;
    }

    public String getOrgInfoProvince() {
        return orgInfoProvince;
    }

    public void setOrgInfoProvince(String orgInfoProvince) {
        this.orgInfoProvince = orgInfoProvince;
    }

    public String getOrgInfoCity() {
        return orgInfoCity;
    }

    public void setOrgInfoCity(String orgInfoCity) {
        this.orgInfoCity = orgInfoCity;
    }

    public String getOrgInfoCounty() {
        return orgInfoCounty;
    }

    public void setOrgInfoCounty(String orgInfoCounty) {
        this.orgInfoCounty = orgInfoCounty;
    }

    public String getOrgInfoHierarchy() {
        return orgInfoHierarchy;
    }

    public void setOrgInfoHierarchy(String orgInfoHierarchy) {
        this.orgInfoHierarchy = orgInfoHierarchy;
    }

    public String getOrgInfoLevel1() {
        return orgInfoLevel1;
    }

    public void setOrgInfoLevel1(String orgInfoLevel1) {
        this.orgInfoLevel1 = orgInfoLevel1;
    }

    public String getOrgInfoLevel2() {
        return orgInfoLevel2;
    }

    public void setOrgInfoLevel2(String orgInfoLevel2) {
        this.orgInfoLevel2 = orgInfoLevel2;
    }

    public String getOrgInfoLevel3() {
        return orgInfoLevel3;
    }

    public void setOrgInfoLevel3(String orgInfoLevel3) {
        this.orgInfoLevel3 = orgInfoLevel3;
    }

    public String getOrgInfoLevel4() {
        return orgInfoLevel4;
    }

    public void setOrgInfoLevel4(String orgInfoLevel4) {
        this.orgInfoLevel4 = orgInfoLevel4;
    }

    public String getOrgInfoLevel5() {
        return orgInfoLevel5;
    }

    public void setOrgInfoLevel5(String orgInfoLevel5) {
        this.orgInfoLevel5 = orgInfoLevel5;
    }

    @Override
    public String toString() {
        return "ChinesePatentPO{" +
                "orderId='" + orderId + '\'' +
                ", id='" + id + '\'' +
                ", fullText='" + fullText + '\'' +
                ", fullName='" + fullName + '\'' +
                ", Abstract='" + Abstract + '\'' +
                ", orgAgency='" + orgAgency + '\'' +
                ", agent='" + agent + '\'' +
                ", publicationNo='" + publicationNo + '\'' +
                ", publicationDate=" + publicationDate +
                ", author='" + author + '\'' +
                ", fAuthor='" + fAuthor + '\'' +
                ", author2='" + author2 + '\'' +
                ", authorName='" + authorName + '\'' +
                ", authorName2='" + authorName2 + '\'' +
                ", cate='" + cate + '\'' +
                ", cdId='" + cdId + '\'' +
                ", createDate=" + createDate +
                ", classCode='" + classCode + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", date=" + date +
                ", databaseIdentity='" + databaseIdentity + '\'' +
                ", dec=" + dec +
                ", disciplineClass='" + disciplineClass + '\'' +
                ", indCla='" + indCla + '\'' +
                ", lawStatus='" + lawStatus + '\'' +
                ", ls='" + ls + '\'' +
                ", orgNormName='" + orgNormName + '\'' +
                ", intApp='" + intApp + '\'' +
                ", intPush='" + intPush + '\'' +
                ", invPat='" + invPat + '\'' +
                ", keyWord='" + keyWord + '\'' +
                ", mainClassCode='" + mainClassCode + '\'' +
                ", cls='" + cls + '\'' +
                ", clsLevel2='" + clsLevel2 + '\'' +
                ", originRequestNumber='" + originRequestNumber + '\'' +
                ", org='" + org + '\'' +
                ", orgNameSearch='" + orgNameSearch + '\'' +
                ", orgAnyname2Name='" + orgAnyname2Name + '\'' +
                ", orgNormName1='" + orgNormName1 + '\'' +
                ", orgType='" + orgType + '\'' +
                ", patentNumberName='" + patentNumberName + '\'' +
                ", patentTypeName='" + patentTypeName + '\'' +
                ", pages='" + pages + '\'' +
                ", priority='" + priority + '\'' +
                ", pubPath='" + pubPath + '\'' +
                ", reference='" + reference + '\'' +
                ", requestAddress='" + requestAddress + '\'' +
                ", requestNumber='" + requestNumber + '\'' +
                ", requestNo='" + requestNo + '\'' +
                ", requestPeople='" + requestPeople + '\'' +
                ", reviewer='" + reviewer + '\'' +
                ", serverAddress='" + serverAddress + '\'' +
                ", dataSource='" + dataSource + '\'' +
                ", title1='" + title1 + '\'' +
                ", title2='" + title2 + '\'' +
                ", yannodate=" + yannodate +
                ", date1='" + date1 + '\'' +
                ", mainClassCode2='" + mainClassCode2 + '\'' +
                ", smcls='" + smcls + '\'' +
                ", tmcls='" + tmcls + '\'' +
                ", ckey='" + ckey + '\'' +
                ", orgId='" + orgId + '\'' +
                ", orgHierarchyId='" + orgHierarchyId + '\'' +
                ", orgProvince='" + orgProvince + '\'' +
                ", orgCity='" + orgCity + '\'' +
                ", endOrgType='" + endOrgType + '\'' +
                ", fOrgId='" + fOrgId + '\'' +
                ", orgFristFinalId='" + orgFristFinalId + '\'' +
                ", orgFristHierarchyId='" + orgFristHierarchyId + '\'' +
                ", orgFristProvince='" + orgFristProvince + '\'' +
                ", orgFristProvince2='" + orgFristProvince2 + '\'' +
                ", orgFristFinalCity='" + orgFristFinalCity + '\'' +
                ", orgFristCity='" + orgFristCity + '\'' +
                ", orgFristFinalType='" + orgFristFinalType + '\'' +
                ", orgFristType='" + orgFristType + '\'' +
                ", authorId='" + authorId + '\'' +
                ", orgNum='" + orgNum + '\'' +
                ", literatureWeight='" + literatureWeight + '\'' +
                ", quarter='" + quarter + '\'' +
                ", halfyear='" + halfyear + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", author1='" + author1 + '\'' +
                ", authorInfoName='" + authorInfoName + '\'' +
                ", authorInfoOrder='" + authorInfoOrder + '\'' +
                ", authorInfoUnit='" + authorInfoUnit + '\'' +
                ", authorInfoUnitOrgLevel1='" + authorInfoUnitOrgLevel1 + '\'' +
                ", authorInfoUnitOrgLevel2='" + authorInfoUnitOrgLevel2 + '\'' +
                ", authorInfoUnitType='" + authorInfoUnitType + '\'' +
                ", authorInfoUnitProvince='" + authorInfoUnitProvince + '\'' +
                ", authorInfoUnitCity='" + authorInfoUnitCity + '\'' +
                ", authorInfoUnitCounty='" + authorInfoUnitCounty + '\'' +
                ", authorInfoId='" + authorInfoId + '\'' +
                ", authorInfoUnitId='" + authorInfoUnitId + '\'' +
                ", orgInfo='" + orgInfo + '\'' +
                ", orgInfoName='" + orgInfoName + '\'' +
                ", orgInfoOrder='" + orgInfoOrder + '\'' +
                ", orgInfoType='" + orgInfoType + '\'' +
                ", orgInfoProvince='" + orgInfoProvince + '\'' +
                ", orgInfoCity='" + orgInfoCity + '\'' +
                ", orgInfoCounty='" + orgInfoCounty + '\'' +
                ", orgInfoHierarchy='" + orgInfoHierarchy + '\'' +
                ", orgInfoLevel1='" + orgInfoLevel1 + '\'' +
                ", orgInfoLevel2='" + orgInfoLevel2 + '\'' +
                ", orgInfoLevel3='" + orgInfoLevel3 + '\'' +
                ", orgInfoLevel4='" + orgInfoLevel4 + '\'' +
                ", orgInfoLevel5='" + orgInfoLevel5 + '\'' +
                '}';
    }
}
