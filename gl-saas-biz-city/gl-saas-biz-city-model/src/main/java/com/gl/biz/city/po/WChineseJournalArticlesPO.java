package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;

import java.util.Date;


//万方原始数据
public class WChineseJournalArticlesPO {
    @FieldOrder(order = 1)
    private String FullText;
    @FieldOrder(order = 2)
    private String ZHUTI;
    @FieldOrder(order = 3)
    private String ID;
    @FieldOrder(order = 4)
    private String F_ID;
    @FieldOrder(order = 5)
    private String DOI;
    @FieldOrder(order = 6)
    private String WID;
    @FieldOrder(order = 7)
    private String VID;
    @FieldOrder(order = 8)
    private String NID;
    @FieldOrder(order = 9)
    private String LID;
    @FieldOrder(order = 10)
    private String TI;
    @FieldOrder(order = 11)
    private String TIE;
    @FieldOrder(order = 12)
    private String TITLE;
    @FieldOrder(order = 13)
    private String AUID;
    @FieldOrder(order = 14)
    private String AU;
    @FieldOrder(order = 15)
    private String AUE;
    @FieldOrder(order = 16)
    private String FN;
    @FieldOrder(order = 17)
    private String FAUID;
    @FieldOrder(order = 18)
    private String FAU;
    @FieldOrder(order = 19)
    private String FAUE;
    @FieldOrder(order = 20)
    private String AUNUM;
    @FieldOrder(order = 21)
    private String AUTHOR;
    @FieldOrder(order = 22)
    private String AUTHOR_ANYNAME;
    @FieldOrder(order = 23)
    private String ORGS;
    @FieldOrder(order = 24)
    private String ORGC;
    @FieldOrder(order = 25)
    private String ORGE;
    @FieldOrder(order = 26)
    private String FORG;
    @FieldOrder(order = 27)
    private String FORGC;
    @FieldOrder(order = 28)
    private String FORGE;
    @FieldOrder(order = 29)
    private String ORG;
    @FieldOrder(order = 30)
    private String ORG_ANYNAME;
    @FieldOrder(order = 35)
    private String JOUID;
    @FieldOrder(order = 36)
    private String ISSN;
    @FieldOrder(order = 37)
    private String JOUCN;
    @FieldOrder(order = 38)
    private String JOUEN;
    @FieldOrder(order = 39)
    private String JOURNAL;
    @FieldOrder(order = 41)
    private String JOURNAL_ANYNAME;
    @FieldOrder(order = 42)
    private String FJOUCN;
    @FieldOrder(order = 44)
    private String FJOUCN_ANYNAME;
    @FieldOrder(order = 45)
    private Date DATE;
    @FieldOrder(order = 46)
    private String YEAR;
    @FieldOrder(order = 47)
    private String VOL;
    @FieldOrder(order = 48)
    private String PER;
    @FieldOrder(order = 49)
    private String PG;
    @FieldOrder(order = 50)
    private String PN;
    @FieldOrder(order = 51)
    private String COLCN;
    @FieldOrder(order = 52)
    private String COLEN;
    @FieldOrder(order = 53)
    private String LAN;
    @FieldOrder(order = 54)
    private String CID;
    @FieldOrder(order = 55)
    private String DID;
    @FieldOrder(order = 56)
    private String DOCID;
    @FieldOrder(order = 57)
    private String MCID;
    @FieldOrder(order = 58)
    private String ZCID;
    @FieldOrder(order = 59)
    private String SZCID;
    @FieldOrder(order = 60)
    private String TZCID;
    @FieldOrder(order = 61)
    private String DZCID;
    @FieldOrder(order = 62)
    private String IID;
    @FieldOrder(order = 63)
    private String CKEY;
    @FieldOrder(order = 64)
    private String KEYWORD;
    @FieldOrder(order = 65)
    private String EKEY;
    @FieldOrder(order = 66)
    private String MKEY;
    @FieldOrder(order = 67)
    private String CAB;
    @FieldOrder(order = 68)
    private String ABSTRACT;
    @FieldOrder(order = 69)
    private String EAB;
    @FieldOrder(order = 70)
    private String FAB;
    @FieldOrder(order = 71)
    private String FUND;
    @FieldOrder(order = 73)
    private String FUND_ANYNAME;
    @FieldOrder(order = 74)
    private String FFUND;
    @FieldOrder(order = 75)
    private String FPN;
    @FieldOrder(order = 76)
    private String FUNDSUPPORT;
    @FieldOrder(order = 77)
    private String RN;
    @FieldOrder(order = 78)
    private String CORE;
    @FieldOrder(order = 79)
    private String SCORE;
    @FieldOrder(order = 80)
    private String ECORE;
    @FieldOrder(order = 81)
    private String SOURCE;
    @FieldOrder(order = 82)
    private String ORGSTRUCID;
    @FieldOrder(order = 83)
    private String ORGID;
    @FieldOrder(order = 84)
    private String ORGPROVINCE;
    @FieldOrder(order = 85)
    private String ORGCITY;
    @FieldOrder(order = 86)
    private String ORGTYPE;
    @FieldOrder(order = 87)
    private String FORGID;
    @FieldOrder(order = 88)
    private String ENDORGTYPE;
    @FieldOrder(order = 89)
    private String FENDORGID;
    @FieldOrder(order = 90)
    private String FORGSTRUCID;
    @FieldOrder(order = 91)
    private String FORGCITY;
    @FieldOrder(order = 92)
    private String FENDORGCITY;
    @FieldOrder(order = 93)
    private String FORGPROVINCE;
    @FieldOrder(order = 94)
    private String FENDORGPROVINCE;
    @FieldOrder(order = 95)
    private String FENDORGTYPE;
    @FieldOrder(order = 96)
    private String FORGTYPE;
    @FieldOrder(order = 97)
    private String AUIDS;
    @FieldOrder(order = 98)
    private String ORGNUM;
    @FieldOrder(order = 99)
    private String WEIGHT;
    @FieldOrder(order = 100)
    private String QUARTER;
    @FieldOrder(order = 101)
    private String HALFYEAR;
    @FieldOrder(order = 102)
    private String ErrCode;
    @FieldOrder(order = 103)
    private String AUTHORINFO;
    @FieldOrder(order = 115)
    private String ORGINFO;

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getZHUTI() {
        return ZHUTI;
    }

    public void setZHUTI(String ZHUTI) {
        this.ZHUTI = ZHUTI;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getWID() {
        return WID;
    }

    public void setWID(String WID) {
        this.WID = WID;
    }

    public String getVID() {
        return VID;
    }

    public void setVID(String VID) {
        this.VID = VID;
    }

    public String getNID() {
        return NID;
    }

    public void setNID(String NID) {
        this.NID = NID;
    }

    public String getLID() {
        return LID;
    }

    public void setLID(String LID) {
        this.LID = LID;
    }

    public String getTI() {
        return TI;
    }

    public void setTI(String TI) {
        this.TI = TI;
    }

    public String getTIE() {
        return TIE;
    }

    public void setTIE(String TIE) {
        this.TIE = TIE;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getAUID() {
        return AUID;
    }

    public void setAUID(String AUID) {
        this.AUID = AUID;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getAUE() {
        return AUE;
    }

    public void setAUE(String AUE) {
        this.AUE = AUE;
    }

    public String getFN() {
        return FN;
    }

    public void setFN(String FN) {
        this.FN = FN;
    }

    public String getFAUID() {
        return FAUID;
    }

    public void setFAUID(String FAUID) {
        this.FAUID = FAUID;
    }

    public String getFAU() {
        return FAU;
    }

    public void setFAU(String FAU) {
        this.FAU = FAU;
    }

    public String getFAUE() {
        return FAUE;
    }

    public void setFAUE(String FAUE) {
        this.FAUE = FAUE;
    }

    public String getAUNUM() {
        return AUNUM;
    }

    public void setAUNUM(String AUNUM) {
        this.AUNUM = AUNUM;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAUTHOR_ANYNAME() {
        return AUTHOR_ANYNAME;
    }

    public void setAUTHOR_ANYNAME(String AUTHOR_ANYNAME) {
        this.AUTHOR_ANYNAME = AUTHOR_ANYNAME;
    }

    public String getORGS() {
        return ORGS;
    }

    public void setORGS(String ORGS) {
        this.ORGS = ORGS;
    }

    public String getORGC() {
        return ORGC;
    }

    public void setORGC(String ORGC) {
        this.ORGC = ORGC;
    }

    public String getORGE() {
        return ORGE;
    }

    public void setORGE(String ORGE) {
        this.ORGE = ORGE;
    }

    public String getFORG() {
        return FORG;
    }

    public void setFORG(String FORG) {
        this.FORG = FORG;
    }

    public String getFORGC() {
        return FORGC;
    }

    public void setFORGC(String FORGC) {
        this.FORGC = FORGC;
    }

    public String getFORGE() {
        return FORGE;
    }

    public void setFORGE(String FORGE) {
        this.FORGE = FORGE;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getJOUID() {
        return JOUID;
    }

    public void setJOUID(String JOUID) {
        this.JOUID = JOUID;
    }

    public String getISSN() {
        return ISSN;
    }

    public void setISSN(String ISSN) {
        this.ISSN = ISSN;
    }

    public String getJOUCN() {
        return JOUCN;
    }

    public void setJOUCN(String JOUCN) {
        this.JOUCN = JOUCN;
    }

    public String getJOUEN() {
        return JOUEN;
    }

    public void setJOUEN(String JOUEN) {
        this.JOUEN = JOUEN;
    }

    public String getJOURNAL() {
        return JOURNAL;
    }

    public void setJOURNAL(String JOURNAL) {
        this.JOURNAL = JOURNAL;
    }

    public String getJOURNAL_ANYNAME() {
        return JOURNAL_ANYNAME;
    }

    public void setJOURNAL_ANYNAME(String JOURNAL_ANYNAME) {
        this.JOURNAL_ANYNAME = JOURNAL_ANYNAME;
    }

    public String getFJOUCN() {
        return FJOUCN;
    }

    public void setFJOUCN(String FJOUCN) {
        this.FJOUCN = FJOUCN;
    }

    public String getFJOUCN_ANYNAME() {
        return FJOUCN_ANYNAME;
    }

    public void setFJOUCN_ANYNAME(String FJOUCN_ANYNAME) {
        this.FJOUCN_ANYNAME = FJOUCN_ANYNAME;
    }

    public Date getDATE() {
        return DATE;
    }

    public void setDATE(Date DATE) {
        this.DATE = DATE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getVOL() {
        return VOL;
    }

    public void setVOL(String VOL) {
        this.VOL = VOL;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getPG() {
        return PG;
    }

    public void setPG(String PG) {
        this.PG = PG;
    }

    public String getPN() {
        return PN;
    }

    public void setPN(String PN) {
        this.PN = PN;
    }

    public String getCOLCN() {
        return COLCN;
    }

    public void setCOLCN(String COLCN) {
        this.COLCN = COLCN;
    }

    public String getCOLEN() {
        return COLEN;
    }

    public void setCOLEN(String COLEN) {
        this.COLEN = COLEN;
    }

    public String getLAN() {
        return LAN;
    }

    public void setLAN(String LAN) {
        this.LAN = LAN;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getDOCID() {
        return DOCID;
    }

    public void setDOCID(String DOCID) {
        this.DOCID = DOCID;
    }

    public String getMCID() {
        return MCID;
    }

    public void setMCID(String MCID) {
        this.MCID = MCID;
    }

    public String getZCID() {
        return ZCID;
    }

    public void setZCID(String ZCID) {
        this.ZCID = ZCID;
    }

    public String getSZCID() {
        return SZCID;
    }

    public void setSZCID(String SZCID) {
        this.SZCID = SZCID;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getDZCID() {
        return DZCID;
    }

    public void setDZCID(String DZCID) {
        this.DZCID = DZCID;
    }

    public String getIID() {
        return IID;
    }

    public void setIID(String IID) {
        this.IID = IID;
    }

    public String getCKEY() {
        return CKEY;
    }

    public void setCKEY(String CKEY) {
        this.CKEY = CKEY;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getEKEY() {
        return EKEY;
    }

    public void setEKEY(String EKEY) {
        this.EKEY = EKEY;
    }

    public String getMKEY() {
        return MKEY;
    }

    public void setMKEY(String MKEY) {
        this.MKEY = MKEY;
    }

    public String getCAB() {
        return CAB;
    }

    public void setCAB(String CAB) {
        this.CAB = CAB;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getEAB() {
        return EAB;
    }

    public void setEAB(String EAB) {
        this.EAB = EAB;
    }

    public String getFAB() {
        return FAB;
    }

    public void setFAB(String FAB) {
        this.FAB = FAB;
    }

    public String getFUND() {
        return FUND;
    }

    public void setFUND(String FUND) {
        this.FUND = FUND;
    }

    public String getFUND_ANYNAME() {
        return FUND_ANYNAME;
    }

    public void setFUND_ANYNAME(String FUND_ANYNAME) {
        this.FUND_ANYNAME = FUND_ANYNAME;
    }

    public String getFFUND() {
        return FFUND;
    }

    public void setFFUND(String FFUND) {
        this.FFUND = FFUND;
    }

    public String getFPN() {
        return FPN;
    }

    public void setFPN(String FPN) {
        this.FPN = FPN;
    }

    public String getFUNDSUPPORT() {
        return FUNDSUPPORT;
    }

    public void setFUNDSUPPORT(String FUNDSUPPORT) {
        this.FUNDSUPPORT = FUNDSUPPORT;
    }

    public String getRN() {
        return RN;
    }

    public void setRN(String RN) {
        this.RN = RN;
    }

    public String getCORE() {
        return CORE;
    }

    public void setCORE(String CORE) {
        this.CORE = CORE;
    }

    public String getSCORE() {
        return SCORE;
    }

    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    public String getECORE() {
        return ECORE;
    }

    public void setECORE(String ECORE) {
        this.ECORE = ECORE;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    public String getORGSTRUCID() {
        return ORGSTRUCID;
    }

    public void setORGSTRUCID(String ORGSTRUCID) {
        this.ORGSTRUCID = ORGSTRUCID;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getORGPROVINCE() {
        return ORGPROVINCE;
    }

    public void setORGPROVINCE(String ORGPROVINCE) {
        this.ORGPROVINCE = ORGPROVINCE;
    }

    public String getORGCITY() {
        return ORGCITY;
    }

    public void setORGCITY(String ORGCITY) {
        this.ORGCITY = ORGCITY;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getENDORGTYPE() {
        return ENDORGTYPE;
    }

    public void setENDORGTYPE(String ENDORGTYPE) {
        this.ENDORGTYPE = ENDORGTYPE;
    }

    public String getFENDORGID() {
        return FENDORGID;
    }

    public void setFENDORGID(String FENDORGID) {
        this.FENDORGID = FENDORGID;
    }

    public String getFORGSTRUCID() {
        return FORGSTRUCID;
    }

    public void setFORGSTRUCID(String FORGSTRUCID) {
        this.FORGSTRUCID = FORGSTRUCID;
    }

    public String getFORGCITY() {
        return FORGCITY;
    }

    public void setFORGCITY(String FORGCITY) {
        this.FORGCITY = FORGCITY;
    }

    public String getFENDORGCITY() {
        return FENDORGCITY;
    }

    public void setFENDORGCITY(String FENDORGCITY) {
        this.FENDORGCITY = FENDORGCITY;
    }

    public String getFORGPROVINCE() {
        return FORGPROVINCE;
    }

    public void setFORGPROVINCE(String FORGPROVINCE) {
        this.FORGPROVINCE = FORGPROVINCE;
    }

    public String getFENDORGPROVINCE() {
        return FENDORGPROVINCE;
    }

    public void setFENDORGPROVINCE(String FENDORGPROVINCE) {
        this.FENDORGPROVINCE = FENDORGPROVINCE;
    }

    public String getFENDORGTYPE() {
        return FENDORGTYPE;
    }

    public void setFENDORGTYPE(String FENDORGTYPE) {
        this.FENDORGTYPE = FENDORGTYPE;
    }

    public String getFORGTYPE() {
        return FORGTYPE;
    }

    public void setFORGTYPE(String FORGTYPE) {
        this.FORGTYPE = FORGTYPE;
    }

    public String getAUIDS() {
        return AUIDS;
    }

    public void setAUIDS(String AUIDS) {
        this.AUIDS = AUIDS;
    }

    public String getORGNUM() {
        return ORGNUM;
    }

    public void setORGNUM(String ORGNUM) {
        this.ORGNUM = ORGNUM;
    }

    public String getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(String WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public String getQUARTER() {
        return QUARTER;
    }

    public void setQUARTER(String QUARTER) {
        this.QUARTER = QUARTER;
    }

    public String getHALFYEAR() {
        return HALFYEAR;
    }

    public void setHALFYEAR(String HALFYEAR) {
        this.HALFYEAR = HALFYEAR;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getAUTHORINFO() {
        return AUTHORINFO;
    }

    public void setAUTHORINFO(String AUTHORINFO) {
        this.AUTHORINFO = AUTHORINFO;
    }

    public String getORGINFO() {
        return ORGINFO;
    }

    public void setORGINFO(String ORGINFO) {
        this.ORGINFO = ORGINFO;
    }
}
