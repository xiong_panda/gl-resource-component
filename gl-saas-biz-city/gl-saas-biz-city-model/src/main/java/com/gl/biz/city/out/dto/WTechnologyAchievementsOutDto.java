package com.gl.biz.city.out.dto;

import com.gl.biz.city.anno.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 科技成果
 *
 * @author code_generator
 */
@Data
public class WTechnologyAchievementsOutDto implements java.io.Serializable {

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 1)
    private String fulltext;

    /**
     * 主题全文检索(标题、关键词、摘要)
     **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    @FieldOrder(order = 2)
    private String fullName;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 3)
    private String wfId;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 4)
    private String orderid;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    @FieldOrder(order = 5)
    private String Abstract;

    /**
     * 鉴定部门
     **/
    @ApiModelProperty(value = "鉴定部门")
    @FieldOrder(order = 6)
    private String authorizedDepartment;

    /**
     * 专利申请号
     **/
    @ApiModelProperty(value = "专利申请号")
    @FieldOrder(order = 7)
    private String patentapplicationId;

    /**
     * 完成人
     **/
    @ApiModelProperty(value = "完成人")
    @FieldOrder(order = 8)
    private String personsInvolved;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 9)
    private String fau;

    /**
     * 完成人
     **/
    @ApiModelProperty(value = "完成人")
    @FieldOrder(order = 10)
    private String personsInvolved2;

    /**
     * 获奖情况
     **/
    @ApiModelProperty(value = "获奖情况")
    @FieldOrder(order = 11)
    private String awards;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 12)
    private String awardsName;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 13)
    private String awardsAnyname;

    /**
     * 应用行业名称
     **/
    @ApiModelProperty(value = "应用行业名称")
    @FieldOrder(order = 14)
    private String applicationIndustryName;

    /**
     * 中图分类号
     **/
    @ApiModelProperty(value = "中图分类号")
    @FieldOrder(order = 15)
    private String clsCode;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 16)
    private String compby;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 17)
    private String compid;

    /**
     * 联系地址
     **/
    @ApiModelProperty(value = "联系地址")
    @FieldOrder(order = 18)
    private String contactAddress;

    /**
     * 联系单位名称
     **/
    @ApiModelProperty(value = "联系单位名称")
    @FieldOrder(order = 19)
    private String contactUnitName;

    /**
     * 联系人
     **/
    @ApiModelProperty(value = "联系人")
    @FieldOrder(order = 20)
    private String contact;

    /**
     * 联系电话
     **/
    @ApiModelProperty(value = "联系电话")
    @FieldOrder(order = 21)
    private String contactPhoneNumber;

    /**
     * 省市
     **/
    @ApiModelProperty(value = "省市")
    @FieldOrder(order = 22)
    private String provincesCity;

    /**
     * 资料公布日期
     **/
    @ApiModelProperty(value = "资料公布日期")
    @FieldOrder(order = 23)
    private String datareleasedDate;

    /**
     * 工作起止时间
     **/
    @ApiModelProperty(value = "工作起止时间")
    @FieldOrder(order = 24)
    private String workStart;

    /**
     * 工作起止时间
     **/
    @ApiModelProperty(value = "工作起止时间")
    @FieldOrder(order = 25)
    private String workEnd;

    /**
     * 申报日期
     **/
    @ApiModelProperty(value = "申报日期")
    @FieldOrder(order = 26)
    private String declareDate;

    /**
     * 学科分类
     **/
    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 27)
    private String disciplineClass;

    /**
     * 申报单位名
     **/
    @ApiModelProperty(value = "申报单位名")
    @FieldOrder(order = 28)
    private String declareUnitName;

    /**
     * 成果公布日期
     **/
    @ApiModelProperty(value = "成果公布日期")
    @FieldOrder(order = 29)
    private String resultspublishedDate;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 30)
    private String dura;

    /**
     * 创汇
     **/
    @ApiModelProperty(value = "创汇")
    @FieldOrder(order = 31)
    private String foreignExchangeEarning;

    /**
     * 信箱
     **/
    @ApiModelProperty(value = "信箱")
    @FieldOrder(order = 32)
    private String mail;

    /**
     * 传真
     **/
    @ApiModelProperty(value = "传真")
    @FieldOrder(order = 33)
    private String fax;

    /**
     * 成果类别
     **/
    @ApiModelProperty(value = "成果类别")
    @FieldOrder(order = 34)
    private String resultsType;

    /**
     * 成果水平
     **/
    @ApiModelProperty(value = "成果水平")
    @FieldOrder(order = 35)
    private String resultsLevel;

    /**
     * 完成单位
     **/
    @ApiModelProperty(value = "完成单位")
    @FieldOrder(order = 36)
    private String completeUnit;

    /**
     * 投资金额
     **/
    @ApiModelProperty(value = "投资金额")
    @FieldOrder(order = 37)
    private String investmentAmount;

    /**
     * 鉴定日期
     **/
    @ApiModelProperty(value = "鉴定日期")
    @FieldOrder(order = 38)
    private String identificationDate;

    /**
     * 应用行业码
     **/
    @ApiModelProperty(value = "应用行业码")
    @FieldOrder(order = 39)
    private String applicationIndustryCode;

    /**
     * 投资说明
     **/
    @ApiModelProperty(value = "投资说明")
    @FieldOrder(order = 40)
    private String investmentInstructions;

    /**
     * 投资注释
     **/
    @ApiModelProperty(value = "投资注释")
    @FieldOrder(order = 41)
    private String investmentAnnotation;

    /**
     * 成果简介
     **/
    @ApiModelProperty(value = "成果简介")
    @FieldOrder(order = 42)
    private String resultsSummary;

    /**
     * 主题词
     **/
    @ApiModelProperty(value = "主题词")
    @FieldOrder(order = 43)
    private String keyword;

    /**
     * 备注
     **/
    @ApiModelProperty(value = "备注")
    @FieldOrder(order = 44)
    private String note;

    /**
     * 列入时间
     **/
    @ApiModelProperty(value = "列入时间")
    @FieldOrder(order = 45)
    private String included;

    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 46)
    private String orgName;

    /**
     * 机构名称(全文检索)
     **/
    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 47)
    private String orgNameSearch;

    /**
     * 完成单位:规范单位名称
     **/
    @ApiModelProperty(value = "完成单位:规范单位名称")
    @FieldOrder(order = 48)
    private String specificationUnitName;

    /**
     * 完成单位
     **/
    @ApiModelProperty(value = "完成单位")
    @FieldOrder(order = 49)
    private String completionunit;

    /**
     * 机构类型
     **/
    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 50)
    private String orgType;

    /**
     * 推广情况说明
     **/
    @ApiModelProperty(value = "推广情况说明")
    @FieldOrder(order = 51)
    private String promotionInfo;

    /**
     * 推广方式
     **/
    @ApiModelProperty(value = "推广方式")
    @FieldOrder(order = 52)
    private String promotionWay;

    /**
     * 推广范围
     **/
    @ApiModelProperty(value = "推广范围")
    @FieldOrder(order = 53)
    private String promotionScope;

    /**
     * 推广跟踪
     **/
    @ApiModelProperty(value = "推广跟踪")
    @FieldOrder(order = 54)
    private String promotionTracking;

    /**
     * 产值
     **/
    @ApiModelProperty(value = "产值")
    @FieldOrder(order = 55)
    private String outputValue;

    /**
     * 推广的必要性及推广预测
     **/
    @ApiModelProperty(value = "推广的必要性及推广预测")
    @FieldOrder(order = 56)
    private String promotionNecessityPromotionForecast;

    /**
     * 计划名称
     **/
    @ApiModelProperty(value = "计划名称")
    @FieldOrder(order = 57)
    private String projectName;

    /**
     * 成果密级
     **/
    @ApiModelProperty(value = "成果密级")
    @FieldOrder(order = 58)
    private String achievementConfidentialityLevel;

    /**
     * 成果类型
     **/
    @ApiModelProperty(value = "成果类型")
    @FieldOrder(order = 59)
    private String achievementType;

    /**
     * 专利项数
     **/
    @ApiModelProperty(value = "专利项数")
    @FieldOrder(order = 60)
    private String patentNumber;

    /**
     * 公布刊物名、页数
     **/
    @ApiModelProperty(value = "公布刊物名、页数")
    @FieldOrder(order = 61)
    private String publicationNamePageNumber;

    /**
     * 专利授权号
     **/
    @ApiModelProperty(value = "专利授权号")
    @FieldOrder(order = 62)
    private String patentAuthorizationNumber;

    /**
     * 登记日期
     **/
    @ApiModelProperty(value = "登记日期")
    @FieldOrder(order = 63)
    private String registrationDate;

    /**
     * 记录类型
     **/
    @ApiModelProperty(value = "记录类型")
    @FieldOrder(order = 64)
    private String recordDate;

    /**
     * 推荐日期
     **/
    @ApiModelProperty(value = "推荐日期")
    @FieldOrder(order = 65)
    private String recommendedDate;

    /**
     * 限制使用
     **/
    @ApiModelProperty(value = "限制使用")
    @FieldOrder(order = 66)
    private String limitToUse;

    /**
     * 登记号
     **/
    @ApiModelProperty(value = "登记号")
    @FieldOrder(order = 67)
    private String registrationNo;

    /**
     * 记录状态
     **/
    @ApiModelProperty(value = "记录状态")
    @FieldOrder(order = 68)
    private String recordStatus;

    /**
     * 登记部门
     **/
    @ApiModelProperty(value = "登记部门")
    @FieldOrder(order = 69)
    private String registrationDepartment;

    /**
     * 推荐部门
     **/
    @ApiModelProperty(value = "推荐部门")
    @FieldOrder(order = 70)
    private String recommendDepartment;

    /**
     * 发布单位
     **/
    @ApiModelProperty(value = "发布单位")
    @FieldOrder(order = 71)
    private String releaseNnit;

    /**
     * 规范单位名称
     **/
    @ApiModelProperty(value = "规范单位名称")
    @FieldOrder(order = 72)
    private String specificationUnitName2;

    /**
     * 信息来源
     **/
    @ApiModelProperty(value = "信息来源")
    @FieldOrder(order = 73)
    private String sourceOfInfo;

    /**
     * 节资
     **/
    @ApiModelProperty(value = "节资")
    @FieldOrder(order = 74)
    private String saveMoney;

    /**
     * 中图分类二级
     **/
    @ApiModelProperty(value = "中图分类二级")
    @FieldOrder(order = 75)
    private String clsLevel2;

    /**
     * 中图分类(三级)
     **/
    @ApiModelProperty(value = "中图分类(三级)")
    @FieldOrder(order = 76)
    private String clsLevel3;

    /**
     * 转让条件
     **/
    @ApiModelProperty(value = "转让条件")
    @FieldOrder(order = 77)
    private String transferConditions;

    /**
     * 转让费
     **/
    @ApiModelProperty(value = "转让费")
    @FieldOrder(order = 78)
    private String transferFee;

    /**
     * 转让内容
     **/
    @ApiModelProperty(value = "转让内容")
    @FieldOrder(order = 79)
    private String transferContent;

    /**
     * 成果中文名称
     **/
    @ApiModelProperty(value = "成果中文名称")
    @FieldOrder(order = 80)
    private String chineseName;

    /**
     * 转让注释
     **/
    @ApiModelProperty(value = "转让注释")
    @FieldOrder(order = 81)
    private String transferAnnotation;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 82)
    private String ischarge;

    /**
     * "登记部门码"
     **/
    @ApiModelProperty(value = "登记部门码")
    @FieldOrder(order = 83)
    private String registrationDepartmentCode;

    /**
     * "推荐部门码"
     **/
    @ApiModelProperty(value = "推荐部门码")
    @FieldOrder(order = 84)
    private String recommendedDepartmentCode;

    /**
     * 转让方式
     **/
    @ApiModelProperty(value = "转让方式")
    @FieldOrder(order = 85)
    private String transferWay;

    /**
     * 转让范围
     **/
    @ApiModelProperty(value = "转让范围")
    @FieldOrder(order = 86)
    private String transferScope;

    /**
     * 利税
     **/
    @ApiModelProperty(value = "利税")
    @FieldOrder(order = 87)
    private String tax;

    /**
     * 中图分类顶级
     **/
    @ApiModelProperty(value = "中图分类顶级")
    @FieldOrder(order = 88)
    private String clsTop;

    /**
     * 资料公布日期
     **/
    @ApiModelProperty(value = "资料公布日期")
    @FieldOrder(order = 89)
    private String releasedDate;

    /**
     * 中图分类号
     **/
    @ApiModelProperty(value = "中图分类号")
    @FieldOrder(order = 90)
    private String clsCode2;

    /**
     * 邮政编码
     **/
    @ApiModelProperty(value = "邮政编码")
    @FieldOrder(order = 91)
    private String postCode;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 92)
    private String otherId;

    /**
     * "主题词"
     **/
    @ApiModelProperty(value = "主题词")
    @FieldOrder(order = 93)
    private String keyword2;

    /**
     * 机构ID
     **/
    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 94)
    private String orgId;

    /**
     * 机构层级ID
     **/
    @ApiModelProperty(value = "机构层级ID")
    @FieldOrder(order = 95)
    private String orgHierarchyId;

    /**
     * 机构所在省
     **/
    @ApiModelProperty(value = "机构所在省")
    @FieldOrder(order = 96)
    private String orgProvince;

    /**
     * 机构所在市
     **/
    @ApiModelProperty(value = "机构所在市")
    @FieldOrder(order = 97)
    private String orgCity;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 98)
    private String orgFinal;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 99)
    private String orgFinal2;

    /**
     * 第一机构终级机构ID
     **/
    @ApiModelProperty(value = "第一机构终级机构ID")
    @FieldOrder(order = 100)
    private String orgFristFinalId;

    /**
     * 第一机构层级机构ID
     **/
    @ApiModelProperty(value = "第一机构层级机构ID")
    @FieldOrder(order = 101)
    private String orgFristHierarchyId;

    /**
     * 第一机构所在省
     **/
    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 102)
    private String orgFristProvince;

    /**
     * 第一机构所在省
     **/
    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 103)
    private String orgFristProvince2;

    /**
     * 第一机构终级机构所在市
     **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
    @FieldOrder(order = 104)
    private String orgFristFinalCity;

    /**
     * 第一机构所在市
     **/
    @ApiModelProperty(value = "第一机构所在市")
    @FieldOrder(order = 105)
    private String orgFristCity;

    /**
     * 第一机构终级机构类型
     **/
    @ApiModelProperty(value = "第一机构终级机构类型")
    @FieldOrder(order = 106)
    private String orgFristFinalType;

    /**
     * 第一机构类型
     **/
    @ApiModelProperty(value = "第一机构类型")
    @FieldOrder(order = 107)
    private String orgFristType;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 108)
    private String awShort;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 109)
    private String awLevel;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 110)
    private String quarter;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 111)
    private String halfYear;

    /**
     * 文献权重
     **/
    @ApiModelProperty(value = "文献权重")
    @FieldOrder(order = 112)
    private String literatureWeight;

    /**
     * 索引管理字段(无用)
     **/
    @ApiModelProperty(value = "索引管理字段(无用)")
    @FieldOrder(order = 113)
    private String errorCode;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 114)
    private String author;

    /**
     * 作者信息.姓名
     **/
    @ApiModelProperty(value = "作者信息.姓名")
    @FieldOrder(order = 115)
    private String authorInfoName;

    /**
     * 作者信息.作者次序
     **/
    @ApiModelProperty(value = "作者信息.作者次序")
    @FieldOrder(order = 116)
    private String authorInfoOrder;

    /**
     * 作者信息.工作单位
     **/
    @ApiModelProperty(value = "作者信息.工作单位")
    @FieldOrder(order = 117)
    private String authorInfoUnit;

    /**
     * 作者信息.工作单位一级机构
     **/
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
    @FieldOrder(order = 118)
    private String authorInfoUnitOrgLevel1;

    /**
     * 作者信息.工作单位二级机构
     **/
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
    @FieldOrder(order = 119)
    private String authorInfoUnitOrgLevel2;

    /**
     * 作者信息.工作单位类型
     **/
    @ApiModelProperty(value = "作者信息.工作单位类型")
    @FieldOrder(order = 120)
    private String authorInfoUnitType;

    /**
     * 作者信息.工作单位所在省
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在省")
    @FieldOrder(order = 121)
    private String authorInfoUnitProvince;

    /**
     * 作者信息.工作单位所在市
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在市")
    @FieldOrder(order = 122)
    private String authorInfoUnitCity;

    /**
     * 作者信息.工作单位所在县
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在县")
    @FieldOrder(order = 123)
    private String authorInfoUnitCounty;

    /**
     * 作者信息.唯一ID
     **/
    @ApiModelProperty(value = "作者信息.唯一ID")
    @FieldOrder(order = 124)
    private String authorInfoId;

    /**
     * 作者信息.工作单位唯一ID
     **/
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
    @FieldOrder(order = 125)
    private String authorInfoUnitId;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 126)
    private String orgInfo;

    /**
     * 机构信息.机构名称
     **/
    @ApiModelProperty(value = "机构信息.机构名称")
    @FieldOrder(order = 127)
    private String orgInfoName;

    /**
     * 机构信息.机构次序
     **/
    @ApiModelProperty(value = "机构信息.机构次序")
    @FieldOrder(order = 128)
    private String orgInfoOrder;

    /**
     * 机构信息.机构类型
     **/
    @ApiModelProperty(value = "机构信息.机构类型")
    @FieldOrder(order = 129)
    private String orgInfoType;

    /**
     * 机构信息.省
     **/
    @ApiModelProperty(value = "机构信息.省")
    @FieldOrder(order = 130)
    private String orgInfoProvince;

    /**
     * 机构信息.市
     **/
    @ApiModelProperty(value = "机构信息.市")
    @FieldOrder(order = 131)
    private String orgInfoCity;

    /**
     * 机构信息.县
     **/
    @ApiModelProperty(value = "机构信息.县")
    @FieldOrder(order = 132)
    private String orgInfoCounty;

    /**
     * 机构信息.五级机构层级码
     **/
    @ApiModelProperty(value = "机构信息.五级机构层级码")
    @FieldOrder(order = 133)
    private String orgInfoHierarchy;

    /**
     * 机构信息.1级机构名称
     **/
    @ApiModelProperty(value = "机构信息.1级机构名称")
    @FieldOrder(order = 134)
    private String orgInfoLevel1;

    /**
     * 机构信息.2级机构名称
     **/
    @ApiModelProperty(value = "机构信息.2级机构名称")
    @FieldOrder(order = 135)
    private String orgInfoLevel2;

    /**
     * 机构信息.3级机构名称
     **/
    @ApiModelProperty(value = "机构信息.3级机构名称")
    @FieldOrder(order = 136)
    private String orgInfoLevel3;

    /**
     * 机构信息.4级机构名称
     **/
    @ApiModelProperty(value = "机构信息.4级机构名称")
    @FieldOrder(order = 137)
    private String orgInfoLevel4;

    /**
     * 机构信息.5级机构名称
     **/
    @ApiModelProperty(value = "机构信息.5级机构名称")
    @FieldOrder(order = 138)
    private String orgInfoLevel5;


}
