package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


//万方原始数据
public class WScientificOrgPO {
    @FieldOrder(order = 1)
    private String 	F_ID;
    @FieldOrder(order = 2)
    private String 	ID;
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	PROV;
    @FieldOrder(order = 5)
    private String 	CITY;
    @FieldOrder(order = 6)
    private String 	COUN;
    @FieldOrder(order = 7)
    private String 	NAME;
    @FieldOrder(order = 8)
    private String 	NAME_NAME_ANYNAME;
    @FieldOrder(order = 9)
    private String 	NAME_ANYNAME;
    @FieldOrder(order = 10)
    private String 	ACNAME;
    @FieldOrder(order = 11)
    private String 	CNAME;
    @FieldOrder(order = 12)
    private String 	ANAME;
    @FieldOrder(order = 13)
    private String 	ONAME;
    @FieldOrder(order = 14)
    private String 	RELY;
    @FieldOrder(order = 15)
    private String 	RELY_RELY_ANYNAME;
    @FieldOrder(order = 16)
    private String 	RELY_ANYNAME;
    @FieldOrder(order = 17)
    private String 	COMPUNIT;
    @FieldOrder(order = 18)
    private String 	INORG;
    @FieldOrder(order = 19)
    private String 	SUBORG;
    @FieldOrder(order = 20)
    private String 	KEYNAME;
    @FieldOrder(order = 21)
    private String 	EMAIL;
    @FieldOrder(order = 22)
    private String 	URL;
    @FieldOrder(order = 23)
    private String 	PERSON;
    @FieldOrder(order = 24)
    private String 	MULTIPERSON;
    @FieldOrder(order = 25)
    private String 	ADDR;
    @FieldOrder(order = 26)
    private String 	TEL;
    @FieldOrder(order = 27)
    private String 	FAX;
    @FieldOrder(order = 28)
    private String 	YEAR;
    @FieldOrder(order = 29)
    private String 	INTRO;
    @FieldOrder(order = 30)
    private String 	EMPS;
    @FieldOrder(order = 31)
    private String 	TECHS;
    @FieldOrder(order = 32)
    private String 	LEADRESH;
    @FieldOrder(order = 33)
    private String 	AWARDS;
    @FieldOrder(order = 34)
    private String 	PATENTS;
    @FieldOrder(order = 35)
    private String 	PATENT;
    @FieldOrder(order = 36)
    private String 	PROFIT;
    @FieldOrder(order = 37)
    private String 	SCIASSET;
    @FieldOrder(order = 38)
    private String 	RESHDIS;
    @FieldOrder(order = 39)
    private String 	MATTER;
    @FieldOrder(order = 40)
    private String 	PROJECT;
    @FieldOrder(order = 41)
    private String 	INFO;
    @FieldOrder(order = 42)
    private String 	JOUR;
    @FieldOrder(order = 43)
    private String 	ORGTYPE;
    @FieldOrder(order = 44)
    private String 	PCODE;
    @FieldOrder(order = 45)
    private String 	DIS;
    @FieldOrder(order = 46)
    private String 	DID;
    @FieldOrder(order = 47)
    private String 	FDID;
    @FieldOrder(order = 48)
    private String 	KEYWORD;
    @FieldOrder(order = 49)
    private String 	ORGID;
    @FieldOrder(order = 50)
    private String 	ORGLIST;
    @FieldOrder(order = 51)
    private String 	FPD;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getPROV() {
        return PROV;
    }

    public void setPROV(String PROV) {
        this.PROV = PROV;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCOUN() {
        return COUN;
    }

    public void setCOUN(String COUN) {
        this.COUN = COUN;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getNAME_NAME_ANYNAME() {
        return NAME_NAME_ANYNAME;
    }

    public void setNAME_NAME_ANYNAME(String NAME_NAME_ANYNAME) {
        this.NAME_NAME_ANYNAME = NAME_NAME_ANYNAME;
    }

    public String getNAME_ANYNAME() {
        return NAME_ANYNAME;
    }

    public void setNAME_ANYNAME(String NAME_ANYNAME) {
        this.NAME_ANYNAME = NAME_ANYNAME;
    }

    public String getACNAME() {
        return ACNAME;
    }

    public void setACNAME(String ACNAME) {
        this.ACNAME = ACNAME;
    }

    public String getCNAME() {
        return CNAME;
    }

    public void setCNAME(String CNAME) {
        this.CNAME = CNAME;
    }

    public String getANAME() {
        return ANAME;
    }

    public void setANAME(String ANAME) {
        this.ANAME = ANAME;
    }

    public String getONAME() {
        return ONAME;
    }

    public void setONAME(String ONAME) {
        this.ONAME = ONAME;
    }

    public String getRELY() {
        return RELY;
    }

    public void setRELY(String RELY) {
        this.RELY = RELY;
    }

    public String getRELY_RELY_ANYNAME() {
        return RELY_RELY_ANYNAME;
    }

    public void setRELY_RELY_ANYNAME(String RELY_RELY_ANYNAME) {
        this.RELY_RELY_ANYNAME = RELY_RELY_ANYNAME;
    }

    public String getRELY_ANYNAME() {
        return RELY_ANYNAME;
    }

    public void setRELY_ANYNAME(String RELY_ANYNAME) {
        this.RELY_ANYNAME = RELY_ANYNAME;
    }

    public String getCOMPUNIT() {
        return COMPUNIT;
    }

    public void setCOMPUNIT(String COMPUNIT) {
        this.COMPUNIT = COMPUNIT;
    }

    public String getINORG() {
        return INORG;
    }

    public void setINORG(String INORG) {
        this.INORG = INORG;
    }

    public String getSUBORG() {
        return SUBORG;
    }

    public void setSUBORG(String SUBORG) {
        this.SUBORG = SUBORG;
    }

    public String getKEYNAME() {
        return KEYNAME;
    }

    public void setKEYNAME(String KEYNAME) {
        this.KEYNAME = KEYNAME;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getPERSON() {
        return PERSON;
    }

    public void setPERSON(String PERSON) {
        this.PERSON = PERSON;
    }

    public String getMULTIPERSON() {
        return MULTIPERSON;
    }

    public void setMULTIPERSON(String MULTIPERSON) {
        this.MULTIPERSON = MULTIPERSON;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getINTRO() {
        return INTRO;
    }

    public void setINTRO(String INTRO) {
        this.INTRO = INTRO;
    }

    public String getEMPS() {
        return EMPS;
    }

    public void setEMPS(String EMPS) {
        this.EMPS = EMPS;
    }

    public String getTECHS() {
        return TECHS;
    }

    public void setTECHS(String TECHS) {
        this.TECHS = TECHS;
    }

    public String getLEADRESH() {
        return LEADRESH;
    }

    public void setLEADRESH(String LEADRESH) {
        this.LEADRESH = LEADRESH;
    }

    public String getAWARDS() {
        return AWARDS;
    }

    public void setAWARDS(String AWARDS) {
        this.AWARDS = AWARDS;
    }

    public String getPATENTS() {
        return PATENTS;
    }

    public void setPATENTS(String PATENTS) {
        this.PATENTS = PATENTS;
    }

    public String getPATENT() {
        return PATENT;
    }

    public void setPATENT(String PATENT) {
        this.PATENT = PATENT;
    }

    public String getPROFIT() {
        return PROFIT;
    }

    public void setPROFIT(String PROFIT) {
        this.PROFIT = PROFIT;
    }

    public String getSCIASSET() {
        return SCIASSET;
    }

    public void setSCIASSET(String SCIASSET) {
        this.SCIASSET = SCIASSET;
    }

    public String getRESHDIS() {
        return RESHDIS;
    }

    public void setRESHDIS(String RESHDIS) {
        this.RESHDIS = RESHDIS;
    }

    public String getMATTER() {
        return MATTER;
    }

    public void setMATTER(String MATTER) {
        this.MATTER = MATTER;
    }

    public String getPROJECT() {
        return PROJECT;
    }

    public void setPROJECT(String PROJECT) {
        this.PROJECT = PROJECT;
    }

    public String getINFO() {
        return INFO;
    }

    public void setINFO(String INFO) {
        this.INFO = INFO;
    }

    public String getJOUR() {
        return JOUR;
    }

    public void setJOUR(String JOUR) {
        this.JOUR = JOUR;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getPCODE() {
        return PCODE;
    }

    public void setPCODE(String PCODE) {
        this.PCODE = PCODE;
    }

    public String getDIS() {
        return DIS;
    }

    public void setDIS(String DIS) {
        this.DIS = DIS;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getFDID() {
        return FDID;
    }

    public void setFDID(String FDID) {
        this.FDID = FDID;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getORGLIST() {
        return ORGLIST;
    }

    public void setORGLIST(String ORGLIST) {
        this.ORGLIST = ORGLIST;
    }

    public String getFPD() {
        return FPD;
    }

    public void setFPD(String FPD) {
        this.FPD = FPD;
    }
}
