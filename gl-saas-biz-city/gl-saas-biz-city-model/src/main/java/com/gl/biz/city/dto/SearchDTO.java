package com.gl.biz.city.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

@ApiModel("SearchDTO")
public class SearchDTO {
    @ApiModelProperty("服务平台id,《1万方，2东方，//3宁波》")
    private String serviceID;
    @ApiModelProperty("资源类别")
    private String resourceCls;
    @ApiModelProperty("搜索关键字，多关键字空格分割")
    private String keyword;
    @ApiModelProperty("搜索（字段：值&字段：值）")
    private String field;
    @ApiModelProperty("pageNum")
    private String from;
    @ApiModelProperty("万方ids")
    private String ids;
    @ApiModelProperty(value = "pageSize",hidden = true)
    private String pageSize;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }




    public String getPageSize() {
        if (StringUtils.isEmpty(pageSize)) {
            return "10";
        }
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getFrom() {
        if (StringUtils.isEmpty(from)) {
            return "1";
        }
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getServiceID() {
        return serviceID==null?"1":serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getResourceCls() {
        return resourceCls;
    }

    public void setResourceCls(String resourceCls) {
        this.resourceCls = resourceCls;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
