package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class ScientificOrgPO {
    @FieldOrder(order = 1)
    private String  OrderID;
    @FieldOrder(order = 1000)
    private String  ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String  FullText;
    @FieldOrder(order = 4)
    private String  Province;
    @FieldOrder(order = 5)
    private String  City;
    @FieldOrder(order = 6)
    private String  County;
    @FieldOrder(order = 7)
    private String  ORG_Name_Norm;
    @FieldOrder(order = 8)
    private String  Name_Anyname1;
    @FieldOrder(order = 9)
    private String  Name_Anyname2;
    @FieldOrder(order = 10)
    private String  Acname;
    @FieldOrder(order = 11)
    private String  ORG_Name;
    @FieldOrder(order = 12)
    private String  Norm_Name;
    @FieldOrder(order = 13)
    private String  ORG_Name_Other;
    @FieldOrder(order = 14)
    private String  Rely;
    @FieldOrder(order = 15)
    private String  Rely_Anyname1;
    @FieldOrder(order = 16)
    private String  Rely_Anyname2;
    @FieldOrder(order = 17)
    private String  Compunit;
    @FieldOrder(order = 18)
    private String  ORG_Name_Internal;
    @FieldOrder(order = 19)
    private String  ORG_Name_Sub;
    @FieldOrder(order = 20)
    private String  Keyname;
    @FieldOrder(order = 21)
    private String  Email;
    @FieldOrder(order = 22)
    private String  Url;
    @FieldOrder(order = 23)
    private String  Person;
    @FieldOrder(order = 24)
    private String  Multiperson;
    @FieldOrder(order = 25)
    private String  Address;
    @FieldOrder(order = 26)
    private String  Telphone;
    @FieldOrder(order = 27)
    private String  Fax;
    @FieldOrder(order = 28)
    private String  Year;
    @FieldOrder(order = 29)
    private String  ORG_Intro;
    @FieldOrder(order = 30)
    private String  Emps;
    @FieldOrder(order = 31)
    private String  Techs;
    @FieldOrder(order = 32)
    private String  Leadresh;
    @FieldOrder(order = 33)
    private String  Awards;
    @FieldOrder(order = 34)
    private String  Patents;
    @FieldOrder(order = 35)
    private String  Patent;
    @FieldOrder(order = 36)
    private String  Profit;
    @FieldOrder(order = 37)
    private String  Sciasset;
    @FieldOrder(order = 38)
    private String  Reshdis;
    @FieldOrder(order = 39)
    private String  Matter;
    @FieldOrder(order = 40)
    private String  Project;
    @FieldOrder(order = 41)
    private String  w_Scientific_ORG;
    @FieldOrder(order = 42)
    private String  Jour;
    @FieldOrder(order = 43)
    private String  ORG_Type;
    @FieldOrder(order = 44)
    private String  Place_Code;
    @FieldOrder(order = 45)
    private String  Discipline_Class;
    @FieldOrder(order = 46)
    private String  Discipline_Code;
    @FieldOrder(order = 47)
    private String  Discipline_Code2;
    @FieldOrder(order = 48)
    private String  KeyWord;
    @FieldOrder(order = 49)
    private String  ORG_ID;
    @FieldOrder(order = 50)
    private String  ORG_Recommendations;
    @FieldOrder(order = 51)
    private String  Finished_Product;
    @FieldOrder(order = 52)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getORG_Name_Norm() {
        return ORG_Name_Norm;
    }

    public void setORG_Name_Norm(String ORG_Name_Norm) {
        this.ORG_Name_Norm = ORG_Name_Norm;
    }

    public String getName_Anyname1() {
        return Name_Anyname1;
    }

    public void setName_Anyname1(String name_Anyname1) {
        Name_Anyname1 = name_Anyname1;
    }

    public String getName_Anyname2() {
        return Name_Anyname2;
    }

    public void setName_Anyname2(String name_Anyname2) {
        Name_Anyname2 = name_Anyname2;
    }

    public String getAcname() {
        return Acname;
    }

    public void setAcname(String acname) {
        Acname = acname;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getNorm_Name() {
        return Norm_Name;
    }

    public void setNorm_Name(String norm_Name) {
        Norm_Name = norm_Name;
    }

    public String getORG_Name_Other() {
        return ORG_Name_Other;
    }

    public void setORG_Name_Other(String ORG_Name_Other) {
        this.ORG_Name_Other = ORG_Name_Other;
    }

    public String getRely() {
        return Rely;
    }

    public void setRely(String rely) {
        Rely = rely;
    }

    public String getRely_Anyname1() {
        return Rely_Anyname1;
    }

    public void setRely_Anyname1(String rely_Anyname1) {
        Rely_Anyname1 = rely_Anyname1;
    }

    public String getRely_Anyname2() {
        return Rely_Anyname2;
    }

    public void setRely_Anyname2(String rely_Anyname2) {
        Rely_Anyname2 = rely_Anyname2;
    }

    public String getCompunit() {
        return Compunit;
    }

    public void setCompunit(String compunit) {
        Compunit = compunit;
    }

    public String getORG_Name_Internal() {
        return ORG_Name_Internal;
    }

    public void setORG_Name_Internal(String ORG_Name_Internal) {
        this.ORG_Name_Internal = ORG_Name_Internal;
    }

    public String getORG_Name_Sub() {
        return ORG_Name_Sub;
    }

    public void setORG_Name_Sub(String ORG_Name_Sub) {
        this.ORG_Name_Sub = ORG_Name_Sub;
    }

    public String getKeyname() {
        return Keyname;
    }

    public void setKeyname(String keyname) {
        Keyname = keyname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getPerson() {
        return Person;
    }

    public void setPerson(String person) {
        Person = person;
    }

    public String getMultiperson() {
        return Multiperson;
    }

    public void setMultiperson(String multiperson) {
        Multiperson = multiperson;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getTelphone() {
        return Telphone;
    }

    public void setTelphone(String telphone) {
        Telphone = telphone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getORG_Intro() {
        return ORG_Intro;
    }

    public void setORG_Intro(String ORG_Intro) {
        this.ORG_Intro = ORG_Intro;
    }

    public String getEmps() {
        return Emps;
    }

    public void setEmps(String emps) {
        Emps = emps;
    }

    public String getTechs() {
        return Techs;
    }

    public void setTechs(String techs) {
        Techs = techs;
    }

    public String getLeadresh() {
        return Leadresh;
    }

    public void setLeadresh(String leadresh) {
        Leadresh = leadresh;
    }

    public String getAwards() {
        return Awards;
    }

    public void setAwards(String awards) {
        Awards = awards;
    }

    public String getPatents() {
        return Patents;
    }

    public void setPatents(String patents) {
        Patents = patents;
    }

    public String getPatent() {
        return Patent;
    }

    public void setPatent(String patent) {
        Patent = patent;
    }

    public String getProfit() {
        return Profit;
    }

    public void setProfit(String profit) {
        Profit = profit;
    }

    public String getSciasset() {
        return Sciasset;
    }

    public void setSciasset(String sciasset) {
        Sciasset = sciasset;
    }

    public String getReshdis() {
        return Reshdis;
    }

    public void setReshdis(String reshdis) {
        Reshdis = reshdis;
    }

    public String getMatter() {
        return Matter;
    }

    public void setMatter(String matter) {
        Matter = matter;
    }

    public String getProject() {
        return Project;
    }

    public void setProject(String project) {
        Project = project;
    }

    public String getW_Scientific_ORG() {
        return w_Scientific_ORG;
    }

    public void setW_Scientific_ORG(String w_Scientific_ORG) {
        this.w_Scientific_ORG = w_Scientific_ORG;
    }

    public String getJour() {
        return Jour;
    }

    public void setJour(String jour) {
        Jour = jour;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getPlace_Code() {
        return Place_Code;
    }

    public void setPlace_Code(String place_Code) {
        Place_Code = place_Code;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getDiscipline_Code() {
        return Discipline_Code;
    }

    public void setDiscipline_Code(String discipline_Code) {
        Discipline_Code = discipline_Code;
    }

    public String getDiscipline_Code2() {
        return Discipline_Code2;
    }

    public void setDiscipline_Code2(String discipline_Code2) {
        Discipline_Code2 = discipline_Code2;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getORG_Recommendations() {
        return ORG_Recommendations;
    }

    public void setORG_Recommendations(String ORG_Recommendations) {
        this.ORG_Recommendations = ORG_Recommendations;
    }

    public String getFinished_Product() {
        return Finished_Product;
    }

    public void setFinished_Product(String finished_Product) {
        Finished_Product = finished_Product;
    }

    @Override
    public String toString() {
        return "ScientificOrgPO{" +
                "OrderID='" + OrderID + '\'' +
                ", ID='" + ID + '\'' +
                ", FullText='" + FullText + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", County='" + County + '\'' +
                ", ORG_Name_Norm='" + ORG_Name_Norm + '\'' +
                ", Name_Anyname1='" + Name_Anyname1 + '\'' +
                ", Name_Anyname2='" + Name_Anyname2 + '\'' +
                ", Acname='" + Acname + '\'' +
                ", ORG_Name='" + ORG_Name + '\'' +
                ", Norm_Name='" + Norm_Name + '\'' +
                ", ORG_Name_Other='" + ORG_Name_Other + '\'' +
                ", Rely='" + Rely + '\'' +
                ", Rely_Anyname1='" + Rely_Anyname1 + '\'' +
                ", Rely_Anyname2='" + Rely_Anyname2 + '\'' +
                ", Compunit='" + Compunit + '\'' +
                ", ORG_Name_Internal='" + ORG_Name_Internal + '\'' +
                ", ORG_Name_Sub='" + ORG_Name_Sub + '\'' +
                ", Keyname='" + Keyname + '\'' +
                ", Email='" + Email + '\'' +
                ", Url='" + Url + '\'' +
                ", Person='" + Person + '\'' +
                ", Multiperson='" + Multiperson + '\'' +
                ", Address='" + Address + '\'' +
                ", Telphone='" + Telphone + '\'' +
                ", Fax='" + Fax + '\'' +
                ", Year='" + Year + '\'' +
                ", ORG_Intro='" + ORG_Intro + '\'' +
                ", Emps='" + Emps + '\'' +
                ", Techs='" + Techs + '\'' +
                ", Leadresh='" + Leadresh + '\'' +
                ", Awards='" + Awards + '\'' +
                ", Patents='" + Patents + '\'' +
                ", Patent='" + Patent + '\'' +
                ", Profit='" + Profit + '\'' +
                ", Sciasset='" + Sciasset + '\'' +
                ", Reshdis='" + Reshdis + '\'' +
                ", Matter='" + Matter + '\'' +
                ", Project='" + Project + '\'' +
                ", w_Scientific_ORG='" + w_Scientific_ORG + '\'' +
                ", Jour='" + Jour + '\'' +
                ", ORG_Type='" + ORG_Type + '\'' +
                ", Place_Code='" + Place_Code + '\'' +
                ", Discipline_Class='" + Discipline_Class + '\'' +
                ", Discipline_Code='" + Discipline_Code + '\'' +
                ", Discipline_Code2='" + Discipline_Code2 + '\'' +
                ", KeyWord='" + KeyWord + '\'' +
                ", ORG_ID='" + ORG_ID + '\'' +
                ", ORG_Recommendations='" + ORG_Recommendations + '\'' +
                ", Finished_Product='" + Finished_Product + '\'' +
                '}';
    }
}
