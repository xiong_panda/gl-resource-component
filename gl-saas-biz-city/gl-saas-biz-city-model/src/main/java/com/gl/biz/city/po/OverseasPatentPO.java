package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class OverseasPatentPO {
    @FieldOrder(order = 1)	private String 	OrderID;
    @FieldOrder(order = 1000)	private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)	private String 	FullText;
    @FieldOrder(order = 4)	private String 	Full_Name;
    @FieldOrder(order = 5)	private String 	Title;
    @FieldOrder(order = 6)	private String 	Abstract;
    @FieldOrder(order = 7)	private String 	KeyWord;
    @FieldOrder(order = 8)	private String 	ORG_Name;
    @FieldOrder(order = 9)	private String 	ORG2;
    @FieldOrder(order = 10)	private String 	ORG_Name_Search;
    @FieldOrder(order = 11)	private String 	Author;
    @FieldOrder(order = 12)	private String 	Patent_No;
    @FieldOrder(order = 13)	private String 	Requset_No;
    @FieldOrder(order = 14)	private String 	Date;
    @FieldOrder(order = 15)	private String 	Request_People;
    @FieldOrder(order = 16)	private String 	Country_Code;
    @FieldOrder(order = 17)	private String 	Publication_No;
    @FieldOrder(order = 18)	private String 	Publication_Date1;
    @FieldOrder(order = 19)	private String 	Publication_Date2;
    @FieldOrder(order = 20)	private String 	Create_Date;
    @FieldOrder(order = 21)	private String 	Main_Class_Code1;
    @FieldOrder(order = 22)	private String 	Omaincls;
    @FieldOrder(order = 23)	private String 	Ocls;
    @FieldOrder(order = 24)	private String 	Main_Class_Code2;
    @FieldOrder(order = 25)	private String 	Class_Code;
    @FieldOrder(order = 26)	private String 	Appearance_Code;
    @FieldOrder(order = 27)	private String 	Country_Main_Class;
    @FieldOrder(order = 28)	private String 	Country_Next_Class;
    @FieldOrder(order = 29)	private String 	Europe_Main_Class;
    @FieldOrder(order = 30)	private String 	Europe_Next_Class;
    @FieldOrder(order = 31)	private String 	Congener;
    @FieldOrder(order = 32)	private String 	Oreqno;
    @FieldOrder(order = 33)	private String 	Cls;
    @FieldOrder(order = 34)	private String 	Year;
    @FieldOrder(order = 35)	private String 	Reqnov;
    @FieldOrder(order = 36)	private String 	Patt;
    @FieldOrder(order = 37)	private String 	International_Application;
    @FieldOrder(order = 38)	private String 	International_publish;
    @FieldOrder(order = 39)	private String 	WO_Application_No;
    @FieldOrder(order = 40)	private String 	Dec;
    @FieldOrder(order = 41)	private String 	Priority;
    @FieldOrder(order = 42)	private String 	Priority_Data;
    @FieldOrder(order = 43)	private String 	Original_Patent_No;
    @FieldOrder(order = 44)	private String 	Home_Reference;
    @FieldOrder(order = 45)	private String 	Foreign_Reference;
    @FieldOrder(order = 46)	private String 	Nonref;
    @FieldOrder(order = 47)	private String 	Independent_Claim;
    @FieldOrder(order = 48)	private String 	ORG_Type;
    @FieldOrder(order = 49)	private String 	Name;
    @FieldOrder(order = 50)	private String 	Author2;
    @FieldOrder(order = 51)	private String 	ORG_Agency;
    @FieldOrder(order = 52)	private String 	Agent;
    @FieldOrder(order = 53)	private String 	DbID;
    @FieldOrder(order = 54)	private String 	Source;
    @FieldOrder(order = 55)	private String 	Discipline_Class;
    @FieldOrder(order = 56)	private String 	Invalid_Patent;
    @FieldOrder(order = 57)	private String 	Mainclsnum;
    @FieldOrder(order = 58)	private String 	Main_Category_No;
    @FieldOrder(order = 59)	private String 	Request_Address;
    @FieldOrder(order = 60)	private String 	Reference;
    @FieldOrder(order = 61)	private String 	Reviewer;
    @FieldOrder(order = 62)	private String 	CdID;
    @FieldOrder(order = 63)	private String 	Ser_Address;
    @FieldOrder(order = 64)	private String 	Publish_Path;
    @FieldOrder(order = 65)	private String 	Error_Code;
    @FieldOrder(order = 66)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private java.util.Date CreateTime;

    public java.util.Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getFull_Name() {
        return Full_Name;
    }

    public void setFull_Name(String full_Name) {
        Full_Name = full_Name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG2() {
        return ORG2;
    }

    public void setORG2(String ORG2) {
        this.ORG2 = ORG2;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getPatent_No() {
        return Patent_No;
    }

    public void setPatent_No(String patent_No) {
        Patent_No = patent_No;
    }

    public String getRequset_No() {
        return Requset_No;
    }

    public void setRequset_No(String requset_No) {
        Requset_No = requset_No;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getRequest_People() {
        return Request_People;
    }

    public void setRequest_People(String request_People) {
        Request_People = request_People;
    }

    public String getCountry_Code() {
        return Country_Code;
    }

    public void setCountry_Code(String country_Code) {
        Country_Code = country_Code;
    }

    public String getPublication_No() {
        return Publication_No;
    }

    public void setPublication_No(String publication_No) {
        Publication_No = publication_No;
    }

    public String getPublication_Date1() {
        return Publication_Date1;
    }

    public void setPublication_Date1(String publication_Date1) {
        Publication_Date1 = publication_Date1;
    }

    public String getPublication_Date2() {
        return Publication_Date2;
    }

    public void setPublication_Date2(String publication_Date2) {
        Publication_Date2 = publication_Date2;
    }

    public String getCreate_Date() {
        return Create_Date;
    }

    public void setCreate_Date(String create_Date) {
        Create_Date = create_Date;
    }

    public String getMain_Class_Code1() {
        return Main_Class_Code1;
    }

    public void setMain_Class_Code1(String main_Class_Code1) {
        Main_Class_Code1 = main_Class_Code1;
    }

    public String getOmaincls() {
        return Omaincls;
    }

    public void setOmaincls(String omaincls) {
        Omaincls = omaincls;
    }

    public String getOcls() {
        return Ocls;
    }

    public void setOcls(String ocls) {
        Ocls = ocls;
    }

    public String getMain_Class_Code2() {
        return Main_Class_Code2;
    }

    public void setMain_Class_Code2(String main_Class_Code2) {
        Main_Class_Code2 = main_Class_Code2;
    }

    public String getClass_Code() {
        return Class_Code;
    }

    public void setClass_Code(String class_Code) {
        Class_Code = class_Code;
    }

    public String getAppearance_Code() {
        return Appearance_Code;
    }

    public void setAppearance_Code(String appearance_Code) {
        Appearance_Code = appearance_Code;
    }

    public String getCountry_Main_Class() {
        return Country_Main_Class;
    }

    public void setCountry_Main_Class(String country_Main_Class) {
        Country_Main_Class = country_Main_Class;
    }

    public String getCountry_Next_Class() {
        return Country_Next_Class;
    }

    public void setCountry_Next_Class(String country_Next_Class) {
        Country_Next_Class = country_Next_Class;
    }

    public String getEurope_Main_Class() {
        return Europe_Main_Class;
    }

    public void setEurope_Main_Class(String europe_Main_Class) {
        Europe_Main_Class = europe_Main_Class;
    }

    public String getEurope_Next_Class() {
        return Europe_Next_Class;
    }

    public void setEurope_Next_Class(String europe_Next_Class) {
        Europe_Next_Class = europe_Next_Class;
    }

    public String getCongener() {
        return Congener;
    }

    public void setCongener(String congener) {
        Congener = congener;
    }

    public String getOreqno() {
        return Oreqno;
    }

    public void setOreqno(String oreqno) {
        Oreqno = oreqno;
    }

    public String getCls() {
        return Cls;
    }

    public void setCls(String cls) {
        Cls = cls;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getReqnov() {
        return Reqnov;
    }

    public void setReqnov(String reqnov) {
        Reqnov = reqnov;
    }

    public String getPatt() {
        return Patt;
    }

    public void setPatt(String patt) {
        Patt = patt;
    }

    public String getInternational_Application() {
        return International_Application;
    }

    public void setInternational_Application(String international_Application) {
        International_Application = international_Application;
    }

    public String getInternational_publish() {
        return International_publish;
    }

    public void setInternational_publish(String international_publish) {
        International_publish = international_publish;
    }

    public String getWO_Application_No() {
        return WO_Application_No;
    }

    public void setWO_Application_No(String WO_Application_No) {
        this.WO_Application_No = WO_Application_No;
    }

    public String getDec() {
        return Dec;
    }

    public void setDec(String dec) {
        Dec = dec;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getPriority_Data() {
        return Priority_Data;
    }

    public void setPriority_Data(String priority_Data) {
        Priority_Data = priority_Data;
    }

    public String getOriginal_Patent_No() {
        return Original_Patent_No;
    }

    public void setOriginal_Patent_No(String original_Patent_No) {
        Original_Patent_No = original_Patent_No;
    }

    public String getHome_Reference() {
        return Home_Reference;
    }

    public void setHome_Reference(String home_Reference) {
        Home_Reference = home_Reference;
    }

    public String getForeign_Reference() {
        return Foreign_Reference;
    }

    public void setForeign_Reference(String foreign_Reference) {
        Foreign_Reference = foreign_Reference;
    }

    public String getNonref() {
        return Nonref;
    }

    public void setNonref(String nonref) {
        Nonref = nonref;
    }

    public String getIndependent_Claim() {
        return Independent_Claim;
    }

    public void setIndependent_Claim(String independent_Claim) {
        Independent_Claim = independent_Claim;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAuthor2() {
        return Author2;
    }

    public void setAuthor2(String author2) {
        Author2 = author2;
    }

    public String getORG_Agency() {
        return ORG_Agency;
    }

    public void setORG_Agency(String ORG_Agency) {
        this.ORG_Agency = ORG_Agency;
    }

    public String getAgent() {
        return Agent;
    }

    public void setAgent(String agent) {
        Agent = agent;
    }

    public String getDbID() {
        return DbID;
    }

    public void setDbID(String dbID) {
        DbID = dbID;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getDiscipline_Class() {
        return Discipline_Class;
    }

    public void setDiscipline_Class(String discipline_Class) {
        Discipline_Class = discipline_Class;
    }

    public String getInvalid_Patent() {
        return Invalid_Patent;
    }

    public void setInvalid_Patent(String invalid_Patent) {
        Invalid_Patent = invalid_Patent;
    }

    public String getMainclsnum() {
        return Mainclsnum;
    }

    public void setMainclsnum(String mainclsnum) {
        Mainclsnum = mainclsnum;
    }

    public String getMain_Category_No() {
        return Main_Category_No;
    }

    public void setMain_Category_No(String main_Category_No) {
        Main_Category_No = main_Category_No;
    }

    public String getRequest_Address() {
        return Request_Address;
    }

    public void setRequest_Address(String request_Address) {
        Request_Address = request_Address;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getReviewer() {
        return Reviewer;
    }

    public void setReviewer(String reviewer) {
        Reviewer = reviewer;
    }

    public String getCdID() {
        return CdID;
    }

    public void setCdID(String cdID) {
        CdID = cdID;
    }

    public String getSer_Address() {
        return Ser_Address;
    }

    public void setSer_Address(String ser_Address) {
        Ser_Address = ser_Address;
    }

    public String getPublish_Path() {
        return Publish_Path;
    }

    public void setPublish_Path(String publish_Path) {
        Publish_Path = publish_Path;
    }

    public String getError_Code() {
        return Error_Code;
    }

    public void setError_Code(String error_Code) {
        Error_Code = error_Code;
    }
}
