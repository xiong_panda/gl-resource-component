package com.gl.biz.city.po;

import java.util.Date;

public class WashConfigPO {
    private String id;
    private Integer jsonCollectionsToString;
    private Integer xmlToJson;
    private Integer dataReasonable;
    private Integer phoneReasonable;
    private Integer addrReasonable;
    private Date createAt;
    private Date updateAt;

    public String getId() {
        return id;
    }

    public Integer getAddrReasonable() {
        return addrReasonable;
    }

    public void setAddrReasonable(Integer addrReasonable) {
        this.addrReasonable = addrReasonable;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getJsonCollectionsToString() {
        return jsonCollectionsToString;
    }

    public void setJsonCollectionsToString(Integer jsonCollectionsToString) {
        this.jsonCollectionsToString = jsonCollectionsToString;
    }

    public Integer getXmlToJson() {
        return xmlToJson;
    }

    public void setXmlToJson(Integer xmlToJson) {
        this.xmlToJson = xmlToJson;
    }

    public Integer getDataReasonable() {
        return dataReasonable;
    }

    public void setDataReasonable(Integer dataReasonable) {
        this.dataReasonable = dataReasonable;
    }

    public Integer getPhoneReasonable() {
        return phoneReasonable;
    }

    public void setPhoneReasonable(Integer phoneReasonable) {
        this.phoneReasonable = phoneReasonable;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
