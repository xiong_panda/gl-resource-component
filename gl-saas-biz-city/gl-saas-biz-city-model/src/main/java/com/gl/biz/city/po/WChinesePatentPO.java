package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


import java.util.Date;

//万方原始数据
public class WChinesePatentPO {
    @FieldOrder(order = 1)
    private String F_ID;
    @FieldOrder(order = 2)
    private String ID;
    @FieldOrder(order = 3)
    private String FullText;
    @FieldOrder(order = 4)
    private String ZHUTI;
    @FieldOrder(order = 5)
    private String ABSTRACT;
    @FieldOrder(order = 6)
    private String AGENCY;
    @FieldOrder(order = 7)
    private String AGENT;
    @FieldOrder(order = 8)
    private String ANNO;
    @FieldOrder(order = 9)
    private Date ANNODATE;
    @FieldOrder(order = 10)
    private String AU;
    @FieldOrder(order = 11)
    private String FAU;
    @FieldOrder(order = 12)
    private String AUTHOR;
    @FieldOrder(order = 13)
    private String AUTHOR_ANYNAME;
    @FieldOrder(order = 14)
    private String AUTHOR_ANYNAME2;
    @FieldOrder(order = 15)
    private String CATE;
    @FieldOrder(order = 16)
    private String CDID;
    @FieldOrder(order = 17)
    private Date CERTDATE;
    @FieldOrder(order = 18)
    private String CLS;
    @FieldOrder(order = 19)
    private String COUNTRY;
    @FieldOrder(order = 20)
    private Date DATE;
    @FieldOrder(order = 21)
    private String DBID;
    @FieldOrder(order = 22)
    private Date DEC;
    @FieldOrder(order = 23)
    private String DID;
    @FieldOrder(order = 24)
    private String DOM;
    @FieldOrder(order = 25)
    private String FLS;
    @FieldOrder(order = 26)
    private String LS;
    @FieldOrder(order = 27)
    private String FORGC;
    @FieldOrder(order = 28)
    private String INTAPP;
    @FieldOrder(order = 29)
    private String INTPUB;
    @FieldOrder(order = 30)
    private String INVPAT;
    @FieldOrder(order = 31)
    private String KEYWORD;
    @FieldOrder(order = 32)
    private String MAINCLS;
    @FieldOrder(order = 33)
    private String CID;
    @FieldOrder(order = 34)
    private String MCID;
    @FieldOrder(order = 35)
    private String OREQNO;
    @FieldOrder(order = 36)
    private String ORG;
    @FieldOrder(order = 37)
    private String ORG_ANYNAME;
    @FieldOrder(order = 38)
    private String ORG_ANYNAME2;
    @FieldOrder(order = 39)
    private String ORGC;
    @FieldOrder(order = 40)
    private String ORGTYPE;
    @FieldOrder(order = 41)
    private String PATNO;
    @FieldOrder(order = 42)
    private String PATT;
    @FieldOrder(order = 43)
    private String PN;
    @FieldOrder(order = 44)
    private String PRIORITY;
    @FieldOrder(order = 45)
    private String PUBPATH;
    @FieldOrder(order = 46)
    private String REF;
    @FieldOrder(order = 47)
    private String REQADDR;
    @FieldOrder(order = 48)
    private String REQNO;
    @FieldOrder(order = 49)
    private String REQNOV;
    @FieldOrder(order = 50)
    private String REQPEP;
    @FieldOrder(order = 51)
    private String REVIEWER;
    @FieldOrder(order = 52)
    private String SERADDR;
    @FieldOrder(order = 53)
    private String SOURCE;
    @FieldOrder(order = 54)
    private String TI;
    @FieldOrder(order = 55)
    private String TITLE;
    @FieldOrder(order = 56)
    private Date YANNODATE;
    @FieldOrder(order = 57)
    private String YEAR;
    @FieldOrder(order = 58)
    private String ZMCLS;
    @FieldOrder(order = 59)
    private String SMCLS;
    @FieldOrder(order = 60)
    private String TMCLS;
    @FieldOrder(order = 61)
    private String CKEY;
    @FieldOrder(order = 62)
    private String ORGID;
    @FieldOrder(order = 63)
    private String ORGSTRUCID;
    @FieldOrder(order = 64)
    private String ORGPROVINCE;
    @FieldOrder(order = 65)
    private String ORGCITY;
    @FieldOrder(order = 66)
    private String ENDORGTYPE;
    @FieldOrder(order = 67)
    private String FORGID;
    @FieldOrder(order = 68)
    private String FENDORGID;
    @FieldOrder(order = 69)
    private String FORGSTRUCID;
    @FieldOrder(order = 70)
    private String FENDORGPROVINCE;
    @FieldOrder(order = 71)
    private String FORGPROVINCE;
    @FieldOrder(order = 72)
    private String FENDORGCITY;
    @FieldOrder(order = 73)
    private String FORGCITY;
    @FieldOrder(order = 74)
    private String FENDORGTYPE;
    @FieldOrder(order = 75)
    private String FORGTYPE;
    @FieldOrder(order = 76)
    private String AUTHORID;
    @FieldOrder(order = 77)
    private String ORGNUM;
    @FieldOrder(order = 78)
    private String WEIGHT;
    @FieldOrder(order = 79)
    private String QUARTER;
    @FieldOrder(order = 80)
    private String HALFYEAR;
    @FieldOrder(order = 81)
    private String ErrCode;
    @FieldOrder(order = 82)
    private String AUTHORINFO;
    @FieldOrder(order = 83)
    private String NAME;
    @FieldOrder(order = 84)
    private String CX;
    @FieldOrder(order = 85)
    private String ORG3;
    @FieldOrder(order = 86)
    private String FUNIT;
    @FieldOrder(order = 87)
    private String SUNIT;
    @FieldOrder(order = 88)
    private String JGLX;
    @FieldOrder(order = 89)
    private String ORGPROVINCE2;
    @FieldOrder(order = 90)
    private String ORGCITY2;
    @FieldOrder(order = 91)
    private String ORGCOUNTRY;
    @FieldOrder(order = 92)
    private String AUID;
    @FieldOrder(order = 93)
    private String ORGID2;
    @FieldOrder(order = 94)
    private String ORGINFO;
    @FieldOrder(order = 95)
    private String ORG2;
    @FieldOrder(order = 96)
    private String CX2;
    @FieldOrder(order = 97)
    private String ORGTYPE2;
    @FieldOrder(order = 98)
    private String SHENG;
    @FieldOrder(order = 99)
    private String SHI;
    @FieldOrder(order = 100)
    private String XIAN;
    @FieldOrder(order = 101)
    private String RELATION;
    @FieldOrder(order = 102)
    private String ORGLEVEL1;
    @FieldOrder(order = 103)
    private String ORGLEVEL2;
    @FieldOrder(order = 104)
    private String ORGLEVEL3;
    @FieldOrder(order = 105)
    private String ORGLEVEL4;
    @FieldOrder(order = 106)
    private String ORGLEVEL5;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getZHUTI() {
        return ZHUTI;
    }

    public void setZHUTI(String ZHUTI) {
        this.ZHUTI = ZHUTI;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getAGENCY() {
        return AGENCY;
    }

    public void setAGENCY(String AGENCY) {
        this.AGENCY = AGENCY;
    }

    public String getAGENT() {
        return AGENT;
    }

    public void setAGENT(String AGENT) {
        this.AGENT = AGENT;
    }

    public String getANNO() {
        return ANNO;
    }

    public void setANNO(String ANNO) {
        this.ANNO = ANNO;
    }

    public Date getANNODATE() {
        return ANNODATE;
    }

    public void setANNODATE(Date ANNODATE) {
        this.ANNODATE = ANNODATE;
    }

    public String getAU() {
        return AU;
    }

    public void setAU(String AU) {
        this.AU = AU;
    }

    public String getFAU() {
        return FAU;
    }

    public void setFAU(String FAU) {
        this.FAU = FAU;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAUTHOR_ANYNAME() {
        return AUTHOR_ANYNAME;
    }

    public void setAUTHOR_ANYNAME(String AUTHOR_ANYNAME) {
        this.AUTHOR_ANYNAME = AUTHOR_ANYNAME;
    }

    public String getAUTHOR_ANYNAME2() {
        return AUTHOR_ANYNAME2;
    }

    public void setAUTHOR_ANYNAME2(String AUTHOR_ANYNAME2) {
        this.AUTHOR_ANYNAME2 = AUTHOR_ANYNAME2;
    }

    public String getCATE() {
        return CATE;
    }

    public void setCATE(String CATE) {
        this.CATE = CATE;
    }

    public String getCDID() {
        return CDID;
    }

    public void setCDID(String CDID) {
        this.CDID = CDID;
    }

    public Date getCERTDATE() {
        return CERTDATE;
    }

    public void setCERTDATE(Date CERTDATE) {
        this.CERTDATE = CERTDATE;
    }

    public String getCLS() {
        return CLS;
    }

    public void setCLS(String CLS) {
        this.CLS = CLS;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public Date getDATE() {
        return DATE;
    }

    public void setDATE(Date DATE) {
        this.DATE = DATE;
    }

    public String getDBID() {
        return DBID;
    }

    public void setDBID(String DBID) {
        this.DBID = DBID;
    }

    public Date getDEC() {
        return DEC;
    }

    public void setDEC(Date DEC) {
        this.DEC = DEC;
    }

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getDOM() {
        return DOM;
    }

    public void setDOM(String DOM) {
        this.DOM = DOM;
    }

    public String getFLS() {
        return FLS;
    }

    public void setFLS(String FLS) {
        this.FLS = FLS;
    }

    public String getLS() {
        return LS;
    }

    public void setLS(String LS) {
        this.LS = LS;
    }

    public String getFORGC() {
        return FORGC;
    }

    public void setFORGC(String FORGC) {
        this.FORGC = FORGC;
    }

    public String getINTAPP() {
        return INTAPP;
    }

    public void setINTAPP(String INTAPP) {
        this.INTAPP = INTAPP;
    }

    public String getINTPUB() {
        return INTPUB;
    }

    public void setINTPUB(String INTPUB) {
        this.INTPUB = INTPUB;
    }

    public String getINVPAT() {
        return INVPAT;
    }

    public void setINVPAT(String INVPAT) {
        this.INVPAT = INVPAT;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getMAINCLS() {
        return MAINCLS;
    }

    public void setMAINCLS(String MAINCLS) {
        this.MAINCLS = MAINCLS;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getMCID() {
        return MCID;
    }

    public void setMCID(String MCID) {
        this.MCID = MCID;
    }

    public String getOREQNO() {
        return OREQNO;
    }

    public void setOREQNO(String OREQNO) {
        this.OREQNO = OREQNO;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getORG_ANYNAME2() {
        return ORG_ANYNAME2;
    }

    public void setORG_ANYNAME2(String ORG_ANYNAME2) {
        this.ORG_ANYNAME2 = ORG_ANYNAME2;
    }

    public String getORGC() {
        return ORGC;
    }

    public void setORGC(String ORGC) {
        this.ORGC = ORGC;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getPATNO() {
        return PATNO;
    }

    public void setPATNO(String PATNO) {
        this.PATNO = PATNO;
    }

    public String getPATT() {
        return PATT;
    }

    public void setPATT(String PATT) {
        this.PATT = PATT;
    }

    public String getPN() {
        return PN;
    }

    public void setPN(String PN) {
        this.PN = PN;
    }

    public String getPRIORITY() {
        return PRIORITY;
    }

    public void setPRIORITY(String PRIORITY) {
        this.PRIORITY = PRIORITY;
    }

    public String getPUBPATH() {
        return PUBPATH;
    }

    public void setPUBPATH(String PUBPATH) {
        this.PUBPATH = PUBPATH;
    }

    public String getREF() {
        return REF;
    }

    public void setREF(String REF) {
        this.REF = REF;
    }

    public String getREQADDR() {
        return REQADDR;
    }

    public void setREQADDR(String REQADDR) {
        this.REQADDR = REQADDR;
    }

    public String getREQNO() {
        return REQNO;
    }

    public void setREQNO(String REQNO) {
        this.REQNO = REQNO;
    }

    public String getREQNOV() {
        return REQNOV;
    }

    public void setREQNOV(String REQNOV) {
        this.REQNOV = REQNOV;
    }

    public String getREQPEP() {
        return REQPEP;
    }

    public void setREQPEP(String REQPEP) {
        this.REQPEP = REQPEP;
    }

    public String getREVIEWER() {
        return REVIEWER;
    }

    public void setREVIEWER(String REVIEWER) {
        this.REVIEWER = REVIEWER;
    }

    public String getSERADDR() {
        return SERADDR;
    }

    public void setSERADDR(String SERADDR) {
        this.SERADDR = SERADDR;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    public String getTI() {
        return TI;
    }

    public void setTI(String TI) {
        this.TI = TI;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public Date getYANNODATE() {
        return YANNODATE;
    }

    public void setYANNODATE(Date YANNODATE) {
        this.YANNODATE = YANNODATE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getZMCLS() {
        return ZMCLS;
    }

    public void setZMCLS(String ZMCLS) {
        this.ZMCLS = ZMCLS;
    }

    public String getSMCLS() {
        return SMCLS;
    }

    public void setSMCLS(String SMCLS) {
        this.SMCLS = SMCLS;
    }

    public String getTMCLS() {
        return TMCLS;
    }

    public void setTMCLS(String TMCLS) {
        this.TMCLS = TMCLS;
    }

    public String getCKEY() {
        return CKEY;
    }

    public void setCKEY(String CKEY) {
        this.CKEY = CKEY;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getORGSTRUCID() {
        return ORGSTRUCID;
    }

    public void setORGSTRUCID(String ORGSTRUCID) {
        this.ORGSTRUCID = ORGSTRUCID;
    }

    public String getORGPROVINCE() {
        return ORGPROVINCE;
    }

    public void setORGPROVINCE(String ORGPROVINCE) {
        this.ORGPROVINCE = ORGPROVINCE;
    }

    public String getORGCITY() {
        return ORGCITY;
    }

    public void setORGCITY(String ORGCITY) {
        this.ORGCITY = ORGCITY;
    }

    public String getENDORGTYPE() {
        return ENDORGTYPE;
    }

    public void setENDORGTYPE(String ENDORGTYPE) {
        this.ENDORGTYPE = ENDORGTYPE;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getFENDORGID() {
        return FENDORGID;
    }

    public void setFENDORGID(String FENDORGID) {
        this.FENDORGID = FENDORGID;
    }

    public String getFORGSTRUCID() {
        return FORGSTRUCID;
    }

    public void setFORGSTRUCID(String FORGSTRUCID) {
        this.FORGSTRUCID = FORGSTRUCID;
    }

    public String getFENDORGPROVINCE() {
        return FENDORGPROVINCE;
    }

    public void setFENDORGPROVINCE(String FENDORGPROVINCE) {
        this.FENDORGPROVINCE = FENDORGPROVINCE;
    }

    public String getFORGPROVINCE() {
        return FORGPROVINCE;
    }

    public void setFORGPROVINCE(String FORGPROVINCE) {
        this.FORGPROVINCE = FORGPROVINCE;
    }

    public String getFENDORGCITY() {
        return FENDORGCITY;
    }

    public void setFENDORGCITY(String FENDORGCITY) {
        this.FENDORGCITY = FENDORGCITY;
    }

    public String getFORGCITY() {
        return FORGCITY;
    }

    public void setFORGCITY(String FORGCITY) {
        this.FORGCITY = FORGCITY;
    }

    public String getFENDORGTYPE() {
        return FENDORGTYPE;
    }

    public void setFENDORGTYPE(String FENDORGTYPE) {
        this.FENDORGTYPE = FENDORGTYPE;
    }

    public String getFORGTYPE() {
        return FORGTYPE;
    }

    public void setFORGTYPE(String FORGTYPE) {
        this.FORGTYPE = FORGTYPE;
    }

    public String getAUTHORID() {
        return AUTHORID;
    }

    public void setAUTHORID(String AUTHORID) {
        this.AUTHORID = AUTHORID;
    }

    public String getORGNUM() {
        return ORGNUM;
    }

    public void setORGNUM(String ORGNUM) {
        this.ORGNUM = ORGNUM;
    }

    public String getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(String WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public String getQUARTER() {
        return QUARTER;
    }

    public void setQUARTER(String QUARTER) {
        this.QUARTER = QUARTER;
    }

    public String getHALFYEAR() {
        return HALFYEAR;
    }

    public void setHALFYEAR(String HALFYEAR) {
        this.HALFYEAR = HALFYEAR;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getAUTHORINFO() {
        return AUTHORINFO;
    }

    public void setAUTHORINFO(String AUTHORINFO) {
        this.AUTHORINFO = AUTHORINFO;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getCX() {
        return CX;
    }

    public void setCX(String CX) {
        this.CX = CX;
    }

    public String getORG3() {
        return ORG3;
    }

    public void setORG3(String ORG3) {
        this.ORG3 = ORG3;
    }

    public String getFUNIT() {
        return FUNIT;
    }

    public void setFUNIT(String FUNIT) {
        this.FUNIT = FUNIT;
    }

    public String getSUNIT() {
        return SUNIT;
    }

    public void setSUNIT(String SUNIT) {
        this.SUNIT = SUNIT;
    }

    public String getJGLX() {
        return JGLX;
    }

    public void setJGLX(String JGLX) {
        this.JGLX = JGLX;
    }

    public String getORGPROVINCE2() {
        return ORGPROVINCE2;
    }

    public void setORGPROVINCE2(String ORGPROVINCE2) {
        this.ORGPROVINCE2 = ORGPROVINCE2;
    }

    public String getORGCITY2() {
        return ORGCITY2;
    }

    public void setORGCITY2(String ORGCITY2) {
        this.ORGCITY2 = ORGCITY2;
    }

    public String getORGCOUNTRY() {
        return ORGCOUNTRY;
    }

    public void setORGCOUNTRY(String ORGCOUNTRY) {
        this.ORGCOUNTRY = ORGCOUNTRY;
    }

    public String getAUID() {
        return AUID;
    }

    public void setAUID(String AUID) {
        this.AUID = AUID;
    }

    public String getORGID2() {
        return ORGID2;
    }

    public void setORGID2(String ORGID2) {
        this.ORGID2 = ORGID2;
    }

    public String getORGINFO() {
        return ORGINFO;
    }

    public void setORGINFO(String ORGINFO) {
        this.ORGINFO = ORGINFO;
    }

    public String getORG2() {
        return ORG2;
    }

    public void setORG2(String ORG2) {
        this.ORG2 = ORG2;
    }

    public String getCX2() {
        return CX2;
    }

    public void setCX2(String CX2) {
        this.CX2 = CX2;
    }

    public String getORGTYPE2() {
        return ORGTYPE2;
    }

    public void setORGTYPE2(String ORGTYPE2) {
        this.ORGTYPE2 = ORGTYPE2;
    }

    public String getSHENG() {
        return SHENG;
    }

    public void setSHENG(String SHENG) {
        this.SHENG = SHENG;
    }

    public String getSHI() {
        return SHI;
    }

    public void setSHI(String SHI) {
        this.SHI = SHI;
    }

    public String getXIAN() {
        return XIAN;
    }

    public void setXIAN(String XIAN) {
        this.XIAN = XIAN;
    }

    public String getRELATION() {
        return RELATION;
    }

    public void setRELATION(String RELATION) {
        this.RELATION = RELATION;
    }

    public String getORGLEVEL1() {
        return ORGLEVEL1;
    }

    public void setORGLEVEL1(String ORGLEVEL1) {
        this.ORGLEVEL1 = ORGLEVEL1;
    }

    public String getORGLEVEL2() {
        return ORGLEVEL2;
    }

    public void setORGLEVEL2(String ORGLEVEL2) {
        this.ORGLEVEL2 = ORGLEVEL2;
    }

    public String getORGLEVEL3() {
        return ORGLEVEL3;
    }

    public void setORGLEVEL3(String ORGLEVEL3) {
        this.ORGLEVEL3 = ORGLEVEL3;
    }

    public String getORGLEVEL4() {
        return ORGLEVEL4;
    }

    public void setORGLEVEL4(String ORGLEVEL4) {
        this.ORGLEVEL4 = ORGLEVEL4;
    }

    public String getORGLEVEL5() {
        return ORGLEVEL5;
    }

    public void setORGLEVEL5(String ORGLEVEL5) {
        this.ORGLEVEL5 = ORGLEVEL5;
    }
}
