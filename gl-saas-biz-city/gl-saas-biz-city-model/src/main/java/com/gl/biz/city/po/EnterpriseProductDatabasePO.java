package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class EnterpriseProductDatabasePO {
    @FieldOrder(order = 1)
    private String 	OrderID;
    @FieldOrder(order = 1000)
    private String 	ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	Corp_FullName;
    @FieldOrder(order = 5)
    private String 	TITLE_ANYNAME;
    @FieldOrder(order = 6)
    private String 	ANYNAME;
    @FieldOrder(order = 7)
    private String 	ACNAME;
    @FieldOrder(order = 8)
    private String 	Corp_Name;
    @FieldOrder(order = 9)
    private String 	Corp_Nameplate;
    @FieldOrder(order = 10)
    private String 	Corp_SHORTNAME;
    @FieldOrder(order = 11)
    private String 	Corp_OLDNAME;
    @FieldOrder(order = 12)
    private String 	Corp_PERSON;
    @FieldOrder(order = 13)
    private String 	Corp_MULTIPERSON;
    @FieldOrder(order = 14)
    private String 	Corp_PROV;
    @FieldOrder(order = 15)
    private String 	Corp_CITY;
    @FieldOrder(order = 16)
    private String 	Corp_COUN;
    @FieldOrder(order = 17)
    private String 	Corp_REGION;
    @FieldOrder(order = 18)
    private String 	Corp_ADDR;
    @FieldOrder(order = 19)
    private String 	Telphone;
    @FieldOrder(order = 20)
    private String 	Corp_RECODE;
    @FieldOrder(order = 21)
    private String 	Corp_FAX;
    @FieldOrder(order = 22)
    private String 	Postal_Code;
    @FieldOrder(order = 23)
    private String 	Corp_EMAIL;
    @FieldOrder(order = 24)
    private String 	Corp_URL;
    @FieldOrder(order = 25)
    private String 	Corp_FOUND;
    @FieldOrder(order = 26)
    private String 	Corp_Money;
    @FieldOrder(order = 27)
    private String 	Corp_ASSET;
    @FieldOrder(order = 28)
    private String 	Corp_EMPS;
    @FieldOrder(order = 29)
    private String 	Corp_TECHS;
    @FieldOrder(order = 30)
    private String 	Corp_TURNOVER;
    @FieldOrder(order = 31)
    private String 	Corp_TAX;
    @FieldOrder(order = 32)
    private String 	Corp_EXCHANGE;
    @FieldOrder(order = 33)
    private String 	Corp_IMPEXP;
    @FieldOrder(order = 34)
    private String 	Corp_LEVEL;
    @FieldOrder(order = 35)
    private String 	Corp_STOCK;
    @FieldOrder(order = 36)
    private String 	ORG_Type;
    @FieldOrder(order = 37)
    private String 	Corp_INTRO;
    @FieldOrder(order = 38)
    private String 	Corp_SPACE;
    @FieldOrder(order = 39)
    private String 	Corp_AREA;
    @FieldOrder(order = 40)
    private String 	Corp_COMPUNIT;
    @FieldOrder(order = 41)
    private String 	Corp;
    @FieldOrder(order = 42)
    private String 	IGBM;
    @FieldOrder(order = 43)
    private String 	IGBM_AnyName;
    @FieldOrder(order = 44)
    private String 	IGBM_AnyName2;
    @FieldOrder(order = 45)
    private String 	FIGBM;
    @FieldOrder(order = 46)
    private String 	FIGBM_AnyName;
    @FieldOrder(order = 47)
    private String 	ORG_Name;
    @FieldOrder(order = 48)
    private String 	ORG_Name_Search;
    @FieldOrder(order = 49)
    private String 	ORG_FullText_Search;
    @FieldOrder(order = 50)
    private String 	ISIC;
    @FieldOrder(order = 51)
    private String 	Brand;
    @FieldOrder(order = 52)
    private String 	Product_Info;
    @FieldOrder(order = 53)
    private String 	Business_Project;
    @FieldOrder(order = 54)
    private String 	Business_Project_en;
    @FieldOrder(order = 55)
    private String 	FullKeyWord;
    @FieldOrder(order = 56)
    private String 	Product_KeyWord;
    @FieldOrder(order = 57)
    private String 	English_Product_KeyWord;
    @FieldOrder(order = 58)
    private String 	PSIC;
    @FieldOrder(order = 59)
    private String 	PGBM;
    @FieldOrder(order = 60)
    private String 	Corp_Ranking;
    @FieldOrder(order = 61)
    private String 	Corp_KEYENT;
    @FieldOrder(order = 62)
    private String 	Corp_CPDATA;
    @FieldOrder(order = 63)
    private String 	Corp_DLDATA;
    @FieldOrder(order = 64)
    private String 	Corp_YJDATA;
    @FieldOrder(order = 65)
    private String 	ORG_ID;
    @FieldOrder(order = 66)
    private String 	Crankname;
    @FieldOrder(order = 67)
    private String 	Crankname1;
    @FieldOrder(order = 68)
    private String 	Crankname2;
    @FieldOrder(order = 69)
    private String 	Cranksource;
    @FieldOrder(order = 70)
    private String 	Cranksource1;
    @FieldOrder(order = 71)
    private String 	Cranksource2;
    @FieldOrder(order = 72)
    private String 	Cranktype;
    @FieldOrder(order = 73)
    private String 	Cranktype1;
    @FieldOrder(order = 74)
    private String 	Cranktype2;
    @FieldOrder(order = 75)
    private String 	Is_Finished_Product_Data;
    @FieldOrder(order = 76)
    @JSONField(format ="yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getCorp_FullName() {
        return Corp_FullName;
    }

    public void setCorp_FullName(String corp_FullName) {
        Corp_FullName = corp_FullName;
    }

    public String getTITLE_ANYNAME() {
        return TITLE_ANYNAME;
    }

    public void setTITLE_ANYNAME(String TITLE_ANYNAME) {
        this.TITLE_ANYNAME = TITLE_ANYNAME;
    }

    public String getANYNAME() {
        return ANYNAME;
    }

    public void setANYNAME(String ANYNAME) {
        this.ANYNAME = ANYNAME;
    }

    public String getACNAME() {
        return ACNAME;
    }

    public void setACNAME(String ACNAME) {
        this.ACNAME = ACNAME;
    }

    public String getCorp_Name() {
        return Corp_Name;
    }

    public void setCorp_Name(String corp_Name) {
        Corp_Name = corp_Name;
    }

    public String getCorp_Nameplate() {
        return Corp_Nameplate;
    }

    public void setCorp_Nameplate(String corp_Nameplate) {
        Corp_Nameplate = corp_Nameplate;
    }

    public String getCorp_SHORTNAME() {
        return Corp_SHORTNAME;
    }

    public void setCorp_SHORTNAME(String corp_SHORTNAME) {
        Corp_SHORTNAME = corp_SHORTNAME;
    }

    public String getCorp_OLDNAME() {
        return Corp_OLDNAME;
    }

    public void setCorp_OLDNAME(String corp_OLDNAME) {
        Corp_OLDNAME = corp_OLDNAME;
    }

    public String getCorp_PERSON() {
        return Corp_PERSON;
    }

    public void setCorp_PERSON(String corp_PERSON) {
        Corp_PERSON = corp_PERSON;
    }

    public String getCorp_MULTIPERSON() {
        return Corp_MULTIPERSON;
    }

    public void setCorp_MULTIPERSON(String corp_MULTIPERSON) {
        Corp_MULTIPERSON = corp_MULTIPERSON;
    }

    public String getCorp_PROV() {
        return Corp_PROV;
    }

    public void setCorp_PROV(String corp_PROV) {
        Corp_PROV = corp_PROV;
    }

    public String getCorp_CITY() {
        return Corp_CITY;
    }

    public void setCorp_CITY(String corp_CITY) {
        Corp_CITY = corp_CITY;
    }

    public String getCorp_COUN() {
        return Corp_COUN;
    }

    public void setCorp_COUN(String corp_COUN) {
        Corp_COUN = corp_COUN;
    }

    public String getCorp_REGION() {
        return Corp_REGION;
    }

    public void setCorp_REGION(String corp_REGION) {
        Corp_REGION = corp_REGION;
    }

    public String getCorp_ADDR() {
        return Corp_ADDR;
    }

    public void setCorp_ADDR(String corp_ADDR) {
        Corp_ADDR = corp_ADDR;
    }

    public String getTelphone() {
        return Telphone;
    }

    public void setTelphone(String telphone) {
        Telphone = telphone;
    }

    public String getCorp_RECODE() {
        return Corp_RECODE;
    }

    public void setCorp_RECODE(String corp_RECODE) {
        Corp_RECODE = corp_RECODE;
    }

    public String getCorp_FAX() {
        return Corp_FAX;
    }

    public void setCorp_FAX(String corp_FAX) {
        Corp_FAX = corp_FAX;
    }

    public String getPostal_Code() {
        return Postal_Code;
    }

    public void setPostal_Code(String postal_Code) {
        Postal_Code = postal_Code;
    }

    public String getCorp_EMAIL() {
        return Corp_EMAIL;
    }

    public void setCorp_EMAIL(String corp_EMAIL) {
        Corp_EMAIL = corp_EMAIL;
    }

    public String getCorp_URL() {
        return Corp_URL;
    }

    public void setCorp_URL(String corp_URL) {
        Corp_URL = corp_URL;
    }

    public String getCorp_FOUND() {
        return Corp_FOUND;
    }

    public void setCorp_FOUND(String corp_FOUND) {
        Corp_FOUND = corp_FOUND;
    }

    public String getCorp_Money() {
        return Corp_Money;
    }

    public void setCorp_Money(String corp_Money) {
        Corp_Money = corp_Money;
    }

    public String getCorp_ASSET() {
        return Corp_ASSET;
    }

    public void setCorp_ASSET(String corp_ASSET) {
        Corp_ASSET = corp_ASSET;
    }

    public String getCorp_EMPS() {
        return Corp_EMPS;
    }

    public void setCorp_EMPS(String corp_EMPS) {
        Corp_EMPS = corp_EMPS;
    }

    public String getCorp_TECHS() {
        return Corp_TECHS;
    }

    public void setCorp_TECHS(String corp_TECHS) {
        Corp_TECHS = corp_TECHS;
    }

    public String getCorp_TURNOVER() {
        return Corp_TURNOVER;
    }

    public void setCorp_TURNOVER(String corp_TURNOVER) {
        Corp_TURNOVER = corp_TURNOVER;
    }

    public String getCorp_TAX() {
        return Corp_TAX;
    }

    public void setCorp_TAX(String corp_TAX) {
        Corp_TAX = corp_TAX;
    }

    public String getCorp_EXCHANGE() {
        return Corp_EXCHANGE;
    }

    public void setCorp_EXCHANGE(String corp_EXCHANGE) {
        Corp_EXCHANGE = corp_EXCHANGE;
    }

    public String getCorp_IMPEXP() {
        return Corp_IMPEXP;
    }

    public void setCorp_IMPEXP(String corp_IMPEXP) {
        Corp_IMPEXP = corp_IMPEXP;
    }

    public String getCorp_LEVEL() {
        return Corp_LEVEL;
    }

    public void setCorp_LEVEL(String corp_LEVEL) {
        Corp_LEVEL = corp_LEVEL;
    }

    public String getCorp_STOCK() {
        return Corp_STOCK;
    }

    public void setCorp_STOCK(String corp_STOCK) {
        Corp_STOCK = corp_STOCK;
    }

    public String getORG_Type() {
        return ORG_Type;
    }

    public void setORG_Type(String ORG_Type) {
        this.ORG_Type = ORG_Type;
    }

    public String getCorp_INTRO() {
        return Corp_INTRO;
    }

    public void setCorp_INTRO(String corp_INTRO) {
        Corp_INTRO = corp_INTRO;
    }

    public String getCorp_SPACE() {
        return Corp_SPACE;
    }

    public void setCorp_SPACE(String corp_SPACE) {
        Corp_SPACE = corp_SPACE;
    }

    public String getCorp_AREA() {
        return Corp_AREA;
    }

    public void setCorp_AREA(String corp_AREA) {
        Corp_AREA = corp_AREA;
    }

    public String getCorp_COMPUNIT() {
        return Corp_COMPUNIT;
    }

    public void setCorp_COMPUNIT(String corp_COMPUNIT) {
        Corp_COMPUNIT = corp_COMPUNIT;
    }

    public String getCorp() {
        return Corp;
    }

    public void setCorp(String corp) {
        Corp = corp;
    }

    public String getIGBM() {
        return IGBM;
    }

    public void setIGBM(String IGBM) {
        this.IGBM = IGBM;
    }

    public String getIGBM_AnyName() {
        return IGBM_AnyName;
    }

    public void setIGBM_AnyName(String IGBM_AnyName) {
        this.IGBM_AnyName = IGBM_AnyName;
    }

    public String getIGBM_AnyName2() {
        return IGBM_AnyName2;
    }

    public void setIGBM_AnyName2(String IGBM_AnyName2) {
        this.IGBM_AnyName2 = IGBM_AnyName2;
    }

    public String getFIGBM() {
        return FIGBM;
    }

    public void setFIGBM(String FIGBM) {
        this.FIGBM = FIGBM;
    }

    public String getFIGBM_AnyName() {
        return FIGBM_AnyName;
    }

    public void setFIGBM_AnyName(String FIGBM_AnyName) {
        this.FIGBM_AnyName = FIGBM_AnyName;
    }

    public String getORG_Name() {
        return ORG_Name;
    }

    public void setORG_Name(String ORG_Name) {
        this.ORG_Name = ORG_Name;
    }

    public String getORG_Name_Search() {
        return ORG_Name_Search;
    }

    public void setORG_Name_Search(String ORG_Name_Search) {
        this.ORG_Name_Search = ORG_Name_Search;
    }

    public String getORG_FullText_Search() {
        return ORG_FullText_Search;
    }

    public void setORG_FullText_Search(String ORG_FullText_Search) {
        this.ORG_FullText_Search = ORG_FullText_Search;
    }

    public String getISIC() {
        return ISIC;
    }

    public void setISIC(String ISIC) {
        this.ISIC = ISIC;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getProduct_Info() {
        return Product_Info;
    }

    public void setProduct_Info(String product_Info) {
        Product_Info = product_Info;
    }

    public String getBusiness_Project() {
        return Business_Project;
    }

    public void setBusiness_Project(String business_Project) {
        Business_Project = business_Project;
    }

    public String getBusiness_Project_en() {
        return Business_Project_en;
    }

    public void setBusiness_Project_en(String business_Project_en) {
        Business_Project_en = business_Project_en;
    }

    public String getFullKeyWord() {
        return FullKeyWord;
    }

    public void setFullKeyWord(String fullKeyWord) {
        FullKeyWord = fullKeyWord;
    }

    public String getProduct_KeyWord() {
        return Product_KeyWord;
    }

    public void setProduct_KeyWord(String product_KeyWord) {
        Product_KeyWord = product_KeyWord;
    }

    public String getEnglish_Product_KeyWord() {
        return English_Product_KeyWord;
    }

    public void setEnglish_Product_KeyWord(String english_Product_KeyWord) {
        English_Product_KeyWord = english_Product_KeyWord;
    }

    public String getPSIC() {
        return PSIC;
    }

    public void setPSIC(String PSIC) {
        this.PSIC = PSIC;
    }

    public String getPGBM() {
        return PGBM;
    }

    public void setPGBM(String PGBM) {
        this.PGBM = PGBM;
    }

    public String getCorp_Ranking() {
        return Corp_Ranking;
    }

    public void setCorp_Ranking(String corp_Ranking) {
        Corp_Ranking = corp_Ranking;
    }

    public String getCorp_KEYENT() {
        return Corp_KEYENT;
    }

    public void setCorp_KEYENT(String corp_KEYENT) {
        Corp_KEYENT = corp_KEYENT;
    }

    public String getCorp_CPDATA() {
        return Corp_CPDATA;
    }

    public void setCorp_CPDATA(String corp_CPDATA) {
        Corp_CPDATA = corp_CPDATA;
    }

    public String getCorp_DLDATA() {
        return Corp_DLDATA;
    }

    public void setCorp_DLDATA(String corp_DLDATA) {
        Corp_DLDATA = corp_DLDATA;
    }

    public String getCorp_YJDATA() {
        return Corp_YJDATA;
    }

    public void setCorp_YJDATA(String corp_YJDATA) {
        Corp_YJDATA = corp_YJDATA;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getCrankname() {
        return Crankname;
    }

    public void setCrankname(String crankname) {
        Crankname = crankname;
    }

    public String getCrankname1() {
        return Crankname1;
    }

    public void setCrankname1(String crankname1) {
        Crankname1 = crankname1;
    }

    public String getCrankname2() {
        return Crankname2;
    }

    public void setCrankname2(String crankname2) {
        Crankname2 = crankname2;
    }

    public String getCranksource() {
        return Cranksource;
    }

    public void setCranksource(String cranksource) {
        Cranksource = cranksource;
    }

    public String getCranksource1() {
        return Cranksource1;
    }

    public void setCranksource1(String cranksource1) {
        Cranksource1 = cranksource1;
    }

    public String getCranksource2() {
        return Cranksource2;
    }

    public void setCranksource2(String cranksource2) {
        Cranksource2 = cranksource2;
    }

    public String getCranktype() {
        return Cranktype;
    }

    public void setCranktype(String cranktype) {
        Cranktype = cranktype;
    }

    public String getCranktype1() {
        return Cranktype1;
    }

    public void setCranktype1(String cranktype1) {
        Cranktype1 = cranktype1;
    }

    public String getCranktype2() {
        return Cranktype2;
    }

    public void setCranktype2(String cranktype2) {
        Cranktype2 = cranktype2;
    }

    public String getIs_Finished_Product_Data() {
        return Is_Finished_Product_Data;
    }

    public void setIs_Finished_Product_Data(String is_Finished_Product_Data) {
        Is_Finished_Product_Data = is_Finished_Product_Data;
    }
}
