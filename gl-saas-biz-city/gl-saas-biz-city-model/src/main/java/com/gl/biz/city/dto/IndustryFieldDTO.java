package com.gl.biz.city.dto;

import lombok.Data;

@Data
public class IndustryFieldDTO implements java.io.Serializable{

    /**
     * 领域行业id
     */
    String fieldIndustryId;

    /**
     * 领域行业父id
     */
    String pFieldIndustryId;

    /**
     * 用户关联id
     */
    String userId;

    /**
     * 领域
     */
    String pFieldIndustry;

    /**
     * 行业
     */
    String fieldIndustry;

    /**
     * 行业英文
     */
    String EnfieldIndustry;

    /**
     * 领域英文英文
     */
    String PEnfieldIndustry;




}
