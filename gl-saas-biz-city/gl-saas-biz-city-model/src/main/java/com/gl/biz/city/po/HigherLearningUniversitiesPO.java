package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//核心资源数据 中文期刊
public class HigherLearningUniversitiesPO {
    @FieldOrder(order = 1)
    private String OrderID;
    @FieldOrder(order = 1000)
    private String ID;
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }
    @FieldOrder(order = 3)
    private String FullText;
    @FieldOrder(order = 4)
    private String Name;
    @FieldOrder(order = 5)
    private String Name_Anyname1;
    @FieldOrder(order = 6)
    private String Name_Anyname2;
    @FieldOrder(order = 7)
    private String School_Name;
    @FieldOrder(order = 8)
    private String Ohther_Name;
    @FieldOrder(order = 9)
    private String Compunit;
    @FieldOrder(order = 10)
    private String Compunit_Iko;
    @FieldOrder(order = 11)
    private String Iko;
    @FieldOrder(order = 12)
    private String Province;
    @FieldOrder(order = 13)
    private String City;
    @FieldOrder(order = 14)
    private String County;
    @FieldOrder(order = 15)
    private String Acode;
    @FieldOrder(order = 16)
    private String Region;
    @FieldOrder(order = 17)
    private String Postal_Code;
    @FieldOrder(order = 18)
    private String Address;
    @FieldOrder(order = 19)
    private String Telphone;
    @FieldOrder(order = 20)
    private String Fax;
    @FieldOrder(order = 21)
    private String Url;
    @FieldOrder(order = 22)
    private String Techs;
    @FieldOrder(order = 23)
    private String Email;
    @FieldOrder(order = 24)
    private String Space;
    @FieldOrder(order = 25)
    private String Levels;
    @FieldOrder(order = 26)
    private String Person;
    @FieldOrder(order = 27)
    private String School_Type;
    @FieldOrder(order = 28)
    private String KeyDiscipline;
    @FieldOrder(order = 29)
    private String Special_School;
    @FieldOrder(order = 30)
    private String Intro;
    @FieldOrder(order = 31)
    private String Campus;
    @FieldOrder(order = 32)
    private String ORG_Setting;
    @FieldOrder(order = 33)
    private String History;
    @FieldOrder(order = 34)
    private String Sub_School;
    @FieldOrder(order = 35)
    private String Gross;
    @FieldOrder(order = 36)
    private String ORG_ID;
    @FieldOrder(order = 37)
    private String Featurecollege;
    @FieldOrder(order = 38)
    private String Featurecollege_Anyname1;
    @FieldOrder(order = 39)
    private String Featurecollege_Anyname2;
    @FieldOrder(order = 40)
    private String College_Type;
    @FieldOrder(order = 41)
    private String Finished_Product_Data;
    @FieldOrder(order = 42)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date CreateTime;

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getName_Anyname1() {
        return Name_Anyname1;
    }

    public void setName_Anyname1(String name_Anyname1) {
        Name_Anyname1 = name_Anyname1;
    }

    public String getName_Anyname2() {
        return Name_Anyname2;
    }

    public void setName_Anyname2(String name_Anyname2) {
        Name_Anyname2 = name_Anyname2;
    }

    public String getSchool_Name() {
        return School_Name;
    }

    public void setSchool_Name(String school_Name) {
        School_Name = school_Name;
    }

    public String getOhther_Name() {
        return Ohther_Name;
    }

    public void setOhther_Name(String ohther_Name) {
        Ohther_Name = ohther_Name;
    }

    public String getCompunit() {
        return Compunit;
    }

    public void setCompunit(String compunit) {
        Compunit = compunit;
    }

    public String getCompunit_Iko() {
        return Compunit_Iko;
    }

    public void setCompunit_Iko(String compunit_Iko) {
        Compunit_Iko = compunit_Iko;
    }

    public String getIko() {
        return Iko;
    }

    public void setIko(String iko) {
        Iko = iko;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getAcode() {
        return Acode;
    }

    public void setAcode(String acode) {
        Acode = acode;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getPostal_Code() {
        return Postal_Code;
    }

    public void setPostal_Code(String postal_Code) {
        Postal_Code = postal_Code;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getTelphone() {
        return Telphone;
    }

    public void setTelphone(String telphone) {
        Telphone = telphone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getTechs() {
        return Techs;
    }

    public void setTechs(String techs) {
        Techs = techs;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSpace() {
        return Space;
    }

    public void setSpace(String space) {
        Space = space;
    }

    public String getLevels() {
        return Levels;
    }

    public void setLevels(String levels) {
        Levels = levels;
    }

    public String getPerson() {
        return Person;
    }

    public void setPerson(String person) {
        Person = person;
    }

    public String getSchool_Type() {
        return School_Type;
    }

    public void setSchool_Type(String school_Type) {
        School_Type = school_Type;
    }

    public String getKeyDiscipline() {
        return KeyDiscipline;
    }

    public void setKeyDiscipline(String keyDiscipline) {
        KeyDiscipline = keyDiscipline;
    }

    public String getSpecial_School() {
        return Special_School;
    }

    public void setSpecial_School(String special_School) {
        Special_School = special_School;
    }

    public String getIntro() {
        return Intro;
    }

    public void setIntro(String intro) {
        Intro = intro;
    }

    public String getCampus() {
        return Campus;
    }

    public void setCampus(String campus) {
        Campus = campus;
    }

    public String getORG_Setting() {
        return ORG_Setting;
    }

    public void setORG_Setting(String ORG_Setting) {
        this.ORG_Setting = ORG_Setting;
    }

    public String getHistory() {
        return History;
    }

    public void setHistory(String history) {
        History = history;
    }

    public String getSub_School() {
        return Sub_School;
    }

    public void setSub_School(String sub_School) {
        Sub_School = sub_School;
    }

    public String getGross() {
        return Gross;
    }

    public void setGross(String gross) {
        Gross = gross;
    }

    public String getORG_ID() {
        return ORG_ID;
    }

    public void setORG_ID(String ORG_ID) {
        this.ORG_ID = ORG_ID;
    }

    public String getFeaturecollege() {
        return Featurecollege;
    }

    public void setFeaturecollege(String featurecollege) {
        Featurecollege = featurecollege;
    }

    public String getFeaturecollege_Anyname1() {
        return Featurecollege_Anyname1;
    }

    public void setFeaturecollege_Anyname1(String featurecollege_Anyname1) {
        Featurecollege_Anyname1 = featurecollege_Anyname1;
    }

    public String getFeaturecollege_Anyname2() {
        return Featurecollege_Anyname2;
    }

    public void setFeaturecollege_Anyname2(String featurecollege_Anyname2) {
        Featurecollege_Anyname2 = featurecollege_Anyname2;
    }

    public String getCollege_Type() {
        return College_Type;
    }

    public void setCollege_Type(String college_Type) {
        College_Type = college_Type;
    }

    public String getFinished_Product_Data() {
        return Finished_Product_Data;
    }

    public void setFinished_Product_Data(String finished_Product_Data) {
        Finished_Product_Data = finished_Product_Data;
    }
}
