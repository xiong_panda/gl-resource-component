package com.gl.biz.city.out.dto;

import com.gl.biz.city.anno.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 外文期刊论文
 *
 * @author code_generator
 */
@Data
public class WForeignPeriodicalPaperOutDto implements java.io.Serializable {

    /**
     * 唯一编号
     **/
    @FieldOrder(order = 1000)
    @ApiModelProperty(value = "唯一编号")
    private String id;

    @FieldOrder(order = 1)
    @ApiModelProperty(value = "万方id")
    private String wfId;

    /**
     * 记录顺序号
     **/
    @FieldOrder(order = 2)
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * 全文索引
     **/
    @FieldOrder(order = 3)
    @ApiModelProperty(value = "全文索引")
    private String fulltext;

    /**
     * 主题全文检索(标题、关键词、摘要)
     **/
    @FieldOrder(order = 4)
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    private String fullName;

    /**
     * Title_Title
     **/
    @FieldOrder(order = 5)
    @ApiModelProperty(value = "Title_Title")
    private String title;

    /**
     * 摘要
     **/
    @FieldOrder(order = 6)
    @ApiModelProperty(value = "摘要")
    private String Abstract;

    /**
     * 作者
     **/
    @FieldOrder(order = 7)
    @ApiModelProperty(value = "作者")
    private String author;

    /**
     * 作者名称(全文检索)
     **/
    @FieldOrder(order = 8)
    @ApiModelProperty(value = "作者名称(全文检索)")
    private String authorName;

    /**
     * 作者名称(全文检索)
     **/
    @FieldOrder(order = 9)
    @ApiModelProperty(value = "作者名称(全文检索)")
    private String authorName2;

    /**
     * Discipline_Keywords
     **/
    @FieldOrder(order = 10)
    @ApiModelProperty(value = "Discipline_Keywords")
    private String keyword;

    /**
     * 机构名称
     **/
    @FieldOrder(order = 11)
    @ApiModelProperty(value = "机构名称")
    private String orgName;

    /**
     * 机构名称(全文检索)
     **/
    @FieldOrder(order = 12)
    @ApiModelProperty(value = "机构名称(全文检索)")
    private String orgNameSearch;

    /**
     * Creator_ORG
     **/
    @FieldOrder(order = 13)
    @ApiModelProperty(value = "Creator_ORG")
    private String creatorOrg;

    /**
     * Date_Issued
     **/
    @FieldOrder(order = 14)
    @ApiModelProperty(value = "Date_Issued")
    private Date date;

    /**
     * Date_Issued
     **/
    @FieldOrder(order = 15)
    @ApiModelProperty(value = "Date_Issued")
    private String year;

    /**
     *
     **/
    @FieldOrder(order = 16)
    @ApiModelProperty(value = "")
    private String iid;

    /**
     * IDentifier_DOI
     **/
    @FieldOrder(order = 17)
    @ApiModelProperty(value = "IDentifier_DOI")
    private String doi;

    /**
     * IDentifier_ISSNp
     **/
    @FieldOrder(order = 18)
    @ApiModelProperty(value = "IDentifier_ISSNp")
    private String issn;

    /**
     * IDentifier_ISSNe
     **/
    @FieldOrder(order = 19)
    @ApiModelProperty(value = "IDentifier_ISSNe")
    private String eissn;

    /**
     * Creator_Creator
     **/
    @FieldOrder(order = 20)
    @ApiModelProperty(value = "Creator_Creator")
    private String author2;

    /**
     * Source_Source
     **/
    @FieldOrder(order = 21)
    @ApiModelProperty(value = "Source_Source")
    private String joucn;

    /**
     * Source_Source
     **/
    @FieldOrder(order = 22)
    @ApiModelProperty(value = "Source_Source")
    private String fjoucn;

    /**
     * Source_Vol
     **/
    @FieldOrder(order = 23)
    @ApiModelProperty(value = "Source_Vol")
    private String vol;

    /**
     * Source_Issue
     **/
    @FieldOrder(order = 24)
    @ApiModelProperty(value = "Source_Issue")
    private String per;

    /**
     * Source_Page
     **/
    @FieldOrder(order = 25)
    @ApiModelProperty(value = "Source_Page")
    private String pg;

    /**
     * Creator_ORG
     **/
    @FieldOrder(order = 26)
    @ApiModelProperty(value = "Creator_ORG")
    private String creatorOrg2;

    /**
     * Date_Download
     **/
    @FieldOrder(order = 27)
    @ApiModelProperty(value = "Date_Download")
    private Date downloadDate;

    /**
     * Publisher_Publisher
     **/
    @FieldOrder(order = 28)
    @ApiModelProperty(value = "Publisher_Publisher")
    private String pubtype;

    /**
     * Yn_Free
     **/
    @FieldOrder(order = 29)
    @ApiModelProperty(value = "Yn_Free")
    private String ynfree;

    /**
     * Discipline_SelfFL
     **/
    @FieldOrder(order = 30)
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String cid;

    /**
     * Discipline_SelfFL
     **/
    @FieldOrder(order = 31)
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String zcid;

    /**
     * Discipline_SelfFL
     **/
    @FieldOrder(order = 32)
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String tzcid;

    /**
     * Discipline_SelfFL
     **/
    @FieldOrder(order = 33)
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String szcid;

    /**
     * 中图分类(三级)
     **/
    @FieldOrder(order = 34)
    @ApiModelProperty(value = "中图分类(三级)")
    private String clsLevel3;

    /**
     * 学科分类
     **/
    @FieldOrder(order = 35)
    @ApiModelProperty(value = "学科分类")
    private String disciplineClass;

    /**
     *
     **/
    @FieldOrder(order = 36)
    @ApiModelProperty(value = "")
    private String score;

    /**
     *
     **/
    @FieldOrder(order = 37)
    @ApiModelProperty(value = "")
    private String rn;

    /**
     *
     **/
    @FieldOrder(order = 38)
    @ApiModelProperty(value = "")
    private String bn;

    /**
     *
     **/
    @FieldOrder(order = 39)
    @ApiModelProperty(value = "")
    private String dn;

    /**
     *
     **/
    @FieldOrder(order = 40)
    @ApiModelProperty(value = "")
    private String ots;

    /**
     * Core_SCI
     **/
    @FieldOrder(order = 41)
    @ApiModelProperty(value = "Core_SCI")
    private String sci;

    /**
     * Core_EI
     **/
    @FieldOrder(order = 42)
    @ApiModelProperty(value = "Core_EI")
    private String ei;

    /**
     * 基金
     **/
    @FieldOrder(order = 43)
    @ApiModelProperty(value = "基金")
    private String fund;

    /**
     * 基金全文检索
     **/
    @FieldOrder(order = 44)
    @ApiModelProperty(value = "基金全文检索")
    private String fundFulltextSearch;

    /**
     * Fund_info
     **/
    @FieldOrder(order = 45)
    @ApiModelProperty(value = "Fund_info")
    private String fundAnyname2;

    /**
     * Fund_info
     **/
    @FieldOrder(order = 46)
    @ApiModelProperty(value = "Fund_info")
    private String fFund;

    /**
     * Core_EI:Core_Sci
     **/
    @FieldOrder(order = 47)
    @ApiModelProperty(value = "Core_EI:Core_Sci")
    private String core;

    /**
     *
     **/
    @FieldOrder(order = 48)
    @ApiModelProperty(value = "")
    private String dataLink;

    /**
     * Fund_info
     **/
    @FieldOrder(order = 49)
    @ApiModelProperty(value = "Fund_info")
    private String fundSupport;
}
