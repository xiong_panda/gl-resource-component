package com.gl.biz.city.dto;

import com.gl.biz.city.CityOutResult;
import lombok.Data;

/**
 * @author ludaye
 * @version 1.0.0
 * @ClassName PlatformResult.java
 * @Description TODO
 * @createTime 2020年07月07日 14:33:00
 */
@Data
public class PlatformResult {
	private String code;
	private CityOutResult data;
	private String msg;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public com.gl.biz.city.CityOutResult getData() {
		return data;
	}

	public void setData(CityOutResult data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
