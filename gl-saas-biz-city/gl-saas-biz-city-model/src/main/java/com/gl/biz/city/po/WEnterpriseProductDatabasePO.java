package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


public class WEnterpriseProductDatabasePO {
    @FieldOrder(order = 1)
    private String 	F_ID;
    @FieldOrder(order = 2)
    private String 	ID;
    @FieldOrder(order = 3)
    private String 	FullText;
    @FieldOrder(order = 4)
    private String 	TITLE;
    @FieldOrder(order = 5)
    private String 	TITLE_TITLE_ANYNAME;
    @FieldOrder(order = 6)
    private String 	TITLE_ANYNAME;
    @FieldOrder(order = 7)
    private String 	ACNAME;
    @FieldOrder(order = 8)
    private String 	CORPNAME;
    @FieldOrder(order = 9)
    private String 	NAMEPLATE;
    @FieldOrder(order = 10)
    private String 	SHORTNAME;
    @FieldOrder(order = 11)
    private String 	OLDNAME;
    @FieldOrder(order = 12)
    private String 	PERSON;
    @FieldOrder(order = 13)
    private String 	MULTIPERSON;
    @FieldOrder(order = 14)
    private String 	PROV;
    @FieldOrder(order = 15)
    private String 	CITY;
    @FieldOrder(order = 16)
    private String 	COUN;
    @FieldOrder(order = 17)
    private String 	REGION;
    @FieldOrder(order = 18)
    private String 	ADDR;
    @FieldOrder(order = 19)
    private String 	TEL;
    @FieldOrder(order = 20)
    private String 	RECODE;
    @FieldOrder(order = 21)
    private String 	FAX;
    @FieldOrder(order = 22)
    private String 	ZIP;
    @FieldOrder(order = 23)
    private String 	EMAIL;
    @FieldOrder(order = 24)
    private String 	URL;
    @FieldOrder(order = 25)
    private String 	FOUND;
    @FieldOrder(order = 26)
    private String 	FUND;
    @FieldOrder(order = 27)
    private String 	ASSET;
    @FieldOrder(order = 28)
    private String 	EMPS;
    @FieldOrder(order = 29)
    private String 	TECHS;
    @FieldOrder(order = 30)
    private String 	TURNOVER;
    @FieldOrder(order = 31)
    private String 	TAX;
    @FieldOrder(order = 32)
    private String 	EXCHANGE;
    @FieldOrder(order = 33)
    private String 	IMPEXP;
    @FieldOrder(order = 34)
    private String 	LEVEL;
    @FieldOrder(order = 35)
    private String 	STOCK;
    @FieldOrder(order = 36)
    private String 	ORGTYPE;
    @FieldOrder(order = 37)
    private String 	INTRO;
    @FieldOrder(order = 38)
    private String 	SPACE;
    @FieldOrder(order = 39)
    private String 	AREA;
    @FieldOrder(order = 40)
    private String 	COMPUNIT;
    @FieldOrder(order = 41)
    private String 	AGENCY;
    @FieldOrder(order = 42)
    private String 	IGBM;
    @FieldOrder(order = 43)
    private String 	IGBM_IGBM_ANYNAME;
    @FieldOrder(order = 44)
    private String 	IGBM_ANYNAME;
    @FieldOrder(order = 45)
    private String 	FIGBM;
    @FieldOrder(order = 46)
    private String 	FIGBM_FIGBM_ANYNAME;
    @FieldOrder(order = 47)
    private String 	ORG;
    @FieldOrder(order = 48)
    private String 	ORG_ORG_ANYNAME;
    @FieldOrder(order = 49)
    private String 	ORG_ANYNAME;
    @FieldOrder(order = 50)
    private String 	ISIC;
    @FieldOrder(order = 51)
    private String 	BRAND;
    @FieldOrder(order = 52)
    private String 	INFO;
    @FieldOrder(order = 53)
    private String 	OPER;
    @FieldOrder(order = 54)
    private String 	OPERE;
    @FieldOrder(order = 55)
    private String 	KEYWORD;
    @FieldOrder(order = 56)
    private String 	CKEY;
    @FieldOrder(order = 57)
    private String 	EKEY;
    @FieldOrder(order = 58)
    private String 	PSIC;
    @FieldOrder(order = 59)
    private String 	PGBM;
    @FieldOrder(order = 60)
    private String 	RANKING;
    @FieldOrder(order = 61)
    private String 	KEYENT;
    @FieldOrder(order = 62)
    private String 	CPDATA;
    @FieldOrder(order = 63)
    private String 	DLDATA;
    @FieldOrder(order = 64)
    private String 	YJDATA;
    @FieldOrder(order = 65)
    private String 	ORGID;
    @FieldOrder(order = 66)
    private String 	CRANKNAME;
    @FieldOrder(order = 67)
    private String 	CRANKNAME_CRANKNAME_ANYNAME;
    @FieldOrder(order = 68)
    private String 	CRANKNAME_ANYNAME;
    @FieldOrder(order = 69)
    private String 	CRANKSOURCE;
    @FieldOrder(order = 70)
    private String 	CRANKSOURCE_CRANKSOURCE_ANYNAME;
    @FieldOrder(order = 71)
    private String 	CRANKSOURCE_ANYNAME;
    @FieldOrder(order = 72)
    private String 	CRANKTYPE;
    @FieldOrder(order = 73)
    private String 	CRANKTYPE_CRANKTYPE_ANYNAME;
    @FieldOrder(order = 74)
    private String 	CRANKTYPE_ANYNAME;
    @FieldOrder(order = 75)
    private String 	FPD;
    @FieldOrder(order = 76)
    private String 	CORE;
    @FieldOrder(order = 77)
    private String 	SCORE;
    @FieldOrder(order = 78)
    private String 	ECORE;
    @FieldOrder(order = 79)
    private String 	SOURCE;
    @FieldOrder(order = 80)
    private String 	ORGSTRUCID;
    @FieldOrder(order = 81)
    private String 	ORGID2;
    @FieldOrder(order = 82)
    private String 	ORGPROVINCE;
    @FieldOrder(order = 83)
    private String 	ORGCITY;
    @FieldOrder(order = 84)
    private String 	ORGTYPE2;
    @FieldOrder(order = 85)
    private String 	FORGID;
    @FieldOrder(order = 86)
    private String 	ENDORGTYPE;
    @FieldOrder(order = 87)
    private String 	FENDORGID;
    @FieldOrder(order = 88)
    private String 	FORGSTRUCID;
    @FieldOrder(order = 89)
    private String 	FORGCITY;
    @FieldOrder(order = 90)
    private String 	FENDORGCITY;
    @FieldOrder(order = 91)
    private String 	FORGPROVINCE;
    @FieldOrder(order = 92)
    private String 	FENDORGPROVINCE;
    @FieldOrder(order = 93)
    private String 	FENDORGTYPE;
    @FieldOrder(order = 94)
    private String 	FORGTYPE;
    @FieldOrder(order = 95)
    private String 	AUIDS;
    @FieldOrder(order = 96)
    private String 	ORGNUM;
    @FieldOrder(order = 97)
    private String 	WEIGHT;
    @FieldOrder(order = 98)
    private String 	QUARTER;
    @FieldOrder(order = 99)
    private String 	HALFYEAR;
    @FieldOrder(order = 100)
    private String 	ErrCode;
    @FieldOrder(order = 101)
    private String 	AUTHORINFO;
    @FieldOrder(order = 102)
    private String 	AUTHORINFO_NAME;
    @FieldOrder(order = 103)
    private String 	AUTHORINFO_CX;
    @FieldOrder(order = 104)
    private String 	AUTHORINFO_ORG;
    @FieldOrder(order = 105)
    private String 	AUTHORINFO_FUNIT;
    @FieldOrder(order = 106)
    private String 	AUTHORINFO_SUNIT;
    @FieldOrder(order = 107)
    private String 	AUTHORINFO_JGLX;
    @FieldOrder(order = 108)
    private String 	AUTHORINFO_ORGPROVINCE;
    @FieldOrder(order = 109)
    private String 	AUTHORINFO_ORGCITY;
    @FieldOrder(order = 110)
    private String 	AUTHORINFO_ORGCOUNTRY;
    @FieldOrder(order = 111)
    private String 	AUTHORINFO_AUID;
    @FieldOrder(order = 112)
    private String 	AUTHORINFO_ORGID;
    @FieldOrder(order = 113)
    private String 	ORGINFO;
    @FieldOrder(order = 114)
    private String 	ORGINFO_ORG;
    @FieldOrder(order = 115)
    private String 	ORGINFO_CX;
    @FieldOrder(order = 116)
    private String 	ORGINFO_ORGTYPE;
    @FieldOrder(order = 117)
    private String 	ORGINFO_SHENG;
    @FieldOrder(order = 118)
    private String 	ORGINFO_SHI;
    @FieldOrder(order = 119)
    private String 	ORGINFO_XIAN;
    @FieldOrder(order = 120)
    private String 	ORGINFO_RELATION;
    @FieldOrder(order = 121)
    private String 	ORGINFO_ORGLEVEL1;
    @FieldOrder(order = 122)
    private String 	ORGINFO_ORGLEVEL2;
    @FieldOrder(order = 123)
    private String 	ORGINFO_ORGLEVEL3;
    @FieldOrder(order = 124)
    private String 	ORGINFO_ORGLEVEL4;
    @FieldOrder(order = 125)
    private String 	ORGINFO_ORGLEVEL5;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getTITLE_TITLE_ANYNAME() {
        return TITLE_TITLE_ANYNAME;
    }

    public void setTITLE_TITLE_ANYNAME(String TITLE_TITLE_ANYNAME) {
        this.TITLE_TITLE_ANYNAME = TITLE_TITLE_ANYNAME;
    }

    public String getTITLE_ANYNAME() {
        return TITLE_ANYNAME;
    }

    public void setTITLE_ANYNAME(String TITLE_ANYNAME) {
        this.TITLE_ANYNAME = TITLE_ANYNAME;
    }

    public String getACNAME() {
        return ACNAME;
    }

    public void setACNAME(String ACNAME) {
        this.ACNAME = ACNAME;
    }

    public String getCORPNAME() {
        return CORPNAME;
    }

    public void setCORPNAME(String CORPNAME) {
        this.CORPNAME = CORPNAME;
    }

    public String getNAMEPLATE() {
        return NAMEPLATE;
    }

    public void setNAMEPLATE(String NAMEPLATE) {
        this.NAMEPLATE = NAMEPLATE;
    }

    public String getSHORTNAME() {
        return SHORTNAME;
    }

    public void setSHORTNAME(String SHORTNAME) {
        this.SHORTNAME = SHORTNAME;
    }

    public String getOLDNAME() {
        return OLDNAME;
    }

    public void setOLDNAME(String OLDNAME) {
        this.OLDNAME = OLDNAME;
    }

    public String getPERSON() {
        return PERSON;
    }

    public void setPERSON(String PERSON) {
        this.PERSON = PERSON;
    }

    public String getMULTIPERSON() {
        return MULTIPERSON;
    }

    public void setMULTIPERSON(String MULTIPERSON) {
        this.MULTIPERSON = MULTIPERSON;
    }

    public String getPROV() {
        return PROV;
    }

    public void setPROV(String PROV) {
        this.PROV = PROV;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getCOUN() {
        return COUN;
    }

    public void setCOUN(String COUN) {
        this.COUN = COUN;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getRECODE() {
        return RECODE;
    }

    public void setRECODE(String RECODE) {
        this.RECODE = RECODE;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getFOUND() {
        return FOUND;
    }

    public void setFOUND(String FOUND) {
        this.FOUND = FOUND;
    }

    public String getFUND() {
        return FUND;
    }

    public void setFUND(String FUND) {
        this.FUND = FUND;
    }

    public String getASSET() {
        return ASSET;
    }

    public void setASSET(String ASSET) {
        this.ASSET = ASSET;
    }

    public String getEMPS() {
        return EMPS;
    }

    public void setEMPS(String EMPS) {
        this.EMPS = EMPS;
    }

    public String getTECHS() {
        return TECHS;
    }

    public void setTECHS(String TECHS) {
        this.TECHS = TECHS;
    }

    public String getTURNOVER() {
        return TURNOVER;
    }

    public void setTURNOVER(String TURNOVER) {
        this.TURNOVER = TURNOVER;
    }

    public String getTAX() {
        return TAX;
    }

    public void setTAX(String TAX) {
        this.TAX = TAX;
    }

    public String getEXCHANGE() {
        return EXCHANGE;
    }

    public void setEXCHANGE(String EXCHANGE) {
        this.EXCHANGE = EXCHANGE;
    }

    public String getIMPEXP() {
        return IMPEXP;
    }

    public void setIMPEXP(String IMPEXP) {
        this.IMPEXP = IMPEXP;
    }

    public String getLEVEL() {
        return LEVEL;
    }

    public void setLEVEL(String LEVEL) {
        this.LEVEL = LEVEL;
    }

    public String getSTOCK() {
        return STOCK;
    }

    public void setSTOCK(String STOCK) {
        this.STOCK = STOCK;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getINTRO() {
        return INTRO;
    }

    public void setINTRO(String INTRO) {
        this.INTRO = INTRO;
    }

    public String getSPACE() {
        return SPACE;
    }

    public void setSPACE(String SPACE) {
        this.SPACE = SPACE;
    }

    public String getAREA() {
        return AREA;
    }

    public void setAREA(String AREA) {
        this.AREA = AREA;
    }

    public String getCOMPUNIT() {
        return COMPUNIT;
    }

    public void setCOMPUNIT(String COMPUNIT) {
        this.COMPUNIT = COMPUNIT;
    }

    public String getAGENCY() {
        return AGENCY;
    }

    public void setAGENCY(String AGENCY) {
        this.AGENCY = AGENCY;
    }

    public String getIGBM() {
        return IGBM;
    }

    public void setIGBM(String IGBM) {
        this.IGBM = IGBM;
    }

    public String getIGBM_IGBM_ANYNAME() {
        return IGBM_IGBM_ANYNAME;
    }

    public void setIGBM_IGBM_ANYNAME(String IGBM_IGBM_ANYNAME) {
        this.IGBM_IGBM_ANYNAME = IGBM_IGBM_ANYNAME;
    }

    public String getIGBM_ANYNAME() {
        return IGBM_ANYNAME;
    }

    public void setIGBM_ANYNAME(String IGBM_ANYNAME) {
        this.IGBM_ANYNAME = IGBM_ANYNAME;
    }

    public String getFIGBM() {
        return FIGBM;
    }

    public void setFIGBM(String FIGBM) {
        this.FIGBM = FIGBM;
    }

    public String getFIGBM_FIGBM_ANYNAME() {
        return FIGBM_FIGBM_ANYNAME;
    }

    public void setFIGBM_FIGBM_ANYNAME(String FIGBM_FIGBM_ANYNAME) {
        this.FIGBM_FIGBM_ANYNAME = FIGBM_FIGBM_ANYNAME;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getISIC() {
        return ISIC;
    }

    public void setISIC(String ISIC) {
        this.ISIC = ISIC;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    public String getINFO() {
        return INFO;
    }

    public void setINFO(String INFO) {
        this.INFO = INFO;
    }

    public String getOPER() {
        return OPER;
    }

    public void setOPER(String OPER) {
        this.OPER = OPER;
    }

    public String getOPERE() {
        return OPERE;
    }

    public void setOPERE(String OPERE) {
        this.OPERE = OPERE;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getCKEY() {
        return CKEY;
    }

    public void setCKEY(String CKEY) {
        this.CKEY = CKEY;
    }

    public String getEKEY() {
        return EKEY;
    }

    public void setEKEY(String EKEY) {
        this.EKEY = EKEY;
    }

    public String getPSIC() {
        return PSIC;
    }

    public void setPSIC(String PSIC) {
        this.PSIC = PSIC;
    }

    public String getPGBM() {
        return PGBM;
    }

    public void setPGBM(String PGBM) {
        this.PGBM = PGBM;
    }

    public String getRANKING() {
        return RANKING;
    }

    public void setRANKING(String RANKING) {
        this.RANKING = RANKING;
    }

    public String getKEYENT() {
        return KEYENT;
    }

    public void setKEYENT(String KEYENT) {
        this.KEYENT = KEYENT;
    }

    public String getCPDATA() {
        return CPDATA;
    }

    public void setCPDATA(String CPDATA) {
        this.CPDATA = CPDATA;
    }

    public String getDLDATA() {
        return DLDATA;
    }

    public void setDLDATA(String DLDATA) {
        this.DLDATA = DLDATA;
    }

    public String getYJDATA() {
        return YJDATA;
    }

    public void setYJDATA(String YJDATA) {
        this.YJDATA = YJDATA;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getCRANKNAME() {
        return CRANKNAME;
    }

    public void setCRANKNAME(String CRANKNAME) {
        this.CRANKNAME = CRANKNAME;
    }

    public String getCRANKNAME_CRANKNAME_ANYNAME() {
        return CRANKNAME_CRANKNAME_ANYNAME;
    }

    public void setCRANKNAME_CRANKNAME_ANYNAME(String CRANKNAME_CRANKNAME_ANYNAME) {
        this.CRANKNAME_CRANKNAME_ANYNAME = CRANKNAME_CRANKNAME_ANYNAME;
    }

    public String getCRANKNAME_ANYNAME() {
        return CRANKNAME_ANYNAME;
    }

    public void setCRANKNAME_ANYNAME(String CRANKNAME_ANYNAME) {
        this.CRANKNAME_ANYNAME = CRANKNAME_ANYNAME;
    }

    public String getCRANKSOURCE() {
        return CRANKSOURCE;
    }

    public void setCRANKSOURCE(String CRANKSOURCE) {
        this.CRANKSOURCE = CRANKSOURCE;
    }

    public String getCRANKSOURCE_CRANKSOURCE_ANYNAME() {
        return CRANKSOURCE_CRANKSOURCE_ANYNAME;
    }

    public void setCRANKSOURCE_CRANKSOURCE_ANYNAME(String CRANKSOURCE_CRANKSOURCE_ANYNAME) {
        this.CRANKSOURCE_CRANKSOURCE_ANYNAME = CRANKSOURCE_CRANKSOURCE_ANYNAME;
    }

    public String getCRANKSOURCE_ANYNAME() {
        return CRANKSOURCE_ANYNAME;
    }

    public void setCRANKSOURCE_ANYNAME(String CRANKSOURCE_ANYNAME) {
        this.CRANKSOURCE_ANYNAME = CRANKSOURCE_ANYNAME;
    }

    public String getCRANKTYPE() {
        return CRANKTYPE;
    }

    public void setCRANKTYPE(String CRANKTYPE) {
        this.CRANKTYPE = CRANKTYPE;
    }

    public String getCRANKTYPE_CRANKTYPE_ANYNAME() {
        return CRANKTYPE_CRANKTYPE_ANYNAME;
    }

    public void setCRANKTYPE_CRANKTYPE_ANYNAME(String CRANKTYPE_CRANKTYPE_ANYNAME) {
        this.CRANKTYPE_CRANKTYPE_ANYNAME = CRANKTYPE_CRANKTYPE_ANYNAME;
    }

    public String getCRANKTYPE_ANYNAME() {
        return CRANKTYPE_ANYNAME;
    }

    public void setCRANKTYPE_ANYNAME(String CRANKTYPE_ANYNAME) {
        this.CRANKTYPE_ANYNAME = CRANKTYPE_ANYNAME;
    }

    public String getFPD() {
        return FPD;
    }

    public void setFPD(String FPD) {
        this.FPD = FPD;
    }

    public String getCORE() {
        return CORE;
    }

    public void setCORE(String CORE) {
        this.CORE = CORE;
    }

    public String getSCORE() {
        return SCORE;
    }

    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    public String getECORE() {
        return ECORE;
    }

    public void setECORE(String ECORE) {
        this.ECORE = ECORE;
    }

    public String getSOURCE() {
        return SOURCE;
    }

    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    public String getORGSTRUCID() {
        return ORGSTRUCID;
    }

    public void setORGSTRUCID(String ORGSTRUCID) {
        this.ORGSTRUCID = ORGSTRUCID;
    }

    public String getORGID2() {
        return ORGID2;
    }

    public void setORGID2(String ORGID2) {
        this.ORGID2 = ORGID2;
    }

    public String getORGPROVINCE() {
        return ORGPROVINCE;
    }

    public void setORGPROVINCE(String ORGPROVINCE) {
        this.ORGPROVINCE = ORGPROVINCE;
    }

    public String getORGCITY() {
        return ORGCITY;
    }

    public void setORGCITY(String ORGCITY) {
        this.ORGCITY = ORGCITY;
    }

    public String getORGTYPE2() {
        return ORGTYPE2;
    }

    public void setORGTYPE2(String ORGTYPE2) {
        this.ORGTYPE2 = ORGTYPE2;
    }

    public String getFORGID() {
        return FORGID;
    }

    public void setFORGID(String FORGID) {
        this.FORGID = FORGID;
    }

    public String getENDORGTYPE() {
        return ENDORGTYPE;
    }

    public void setENDORGTYPE(String ENDORGTYPE) {
        this.ENDORGTYPE = ENDORGTYPE;
    }

    public String getFENDORGID() {
        return FENDORGID;
    }

    public void setFENDORGID(String FENDORGID) {
        this.FENDORGID = FENDORGID;
    }

    public String getFORGSTRUCID() {
        return FORGSTRUCID;
    }

    public void setFORGSTRUCID(String FORGSTRUCID) {
        this.FORGSTRUCID = FORGSTRUCID;
    }

    public String getFORGCITY() {
        return FORGCITY;
    }

    public void setFORGCITY(String FORGCITY) {
        this.FORGCITY = FORGCITY;
    }

    public String getFENDORGCITY() {
        return FENDORGCITY;
    }

    public void setFENDORGCITY(String FENDORGCITY) {
        this.FENDORGCITY = FENDORGCITY;
    }

    public String getFORGPROVINCE() {
        return FORGPROVINCE;
    }

    public void setFORGPROVINCE(String FORGPROVINCE) {
        this.FORGPROVINCE = FORGPROVINCE;
    }

    public String getFENDORGPROVINCE() {
        return FENDORGPROVINCE;
    }

    public void setFENDORGPROVINCE(String FENDORGPROVINCE) {
        this.FENDORGPROVINCE = FENDORGPROVINCE;
    }

    public String getFENDORGTYPE() {
        return FENDORGTYPE;
    }

    public void setFENDORGTYPE(String FENDORGTYPE) {
        this.FENDORGTYPE = FENDORGTYPE;
    }

    public String getFORGTYPE() {
        return FORGTYPE;
    }

    public void setFORGTYPE(String FORGTYPE) {
        this.FORGTYPE = FORGTYPE;
    }

    public String getAUIDS() {
        return AUIDS;
    }

    public void setAUIDS(String AUIDS) {
        this.AUIDS = AUIDS;
    }

    public String getORGNUM() {
        return ORGNUM;
    }

    public void setORGNUM(String ORGNUM) {
        this.ORGNUM = ORGNUM;
    }

    public String getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(String WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public String getQUARTER() {
        return QUARTER;
    }

    public void setQUARTER(String QUARTER) {
        this.QUARTER = QUARTER;
    }

    public String getHALFYEAR() {
        return HALFYEAR;
    }

    public void setHALFYEAR(String HALFYEAR) {
        this.HALFYEAR = HALFYEAR;
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getAUTHORINFO() {
        return AUTHORINFO;
    }

    public void setAUTHORINFO(String AUTHORINFO) {
        this.AUTHORINFO = AUTHORINFO;
    }

    public String getAUTHORINFO_NAME() {
        return AUTHORINFO_NAME;
    }

    public void setAUTHORINFO_NAME(String AUTHORINFO_NAME) {
        this.AUTHORINFO_NAME = AUTHORINFO_NAME;
    }

    public String getAUTHORINFO_CX() {
        return AUTHORINFO_CX;
    }

    public void setAUTHORINFO_CX(String AUTHORINFO_CX) {
        this.AUTHORINFO_CX = AUTHORINFO_CX;
    }

    public String getAUTHORINFO_ORG() {
        return AUTHORINFO_ORG;
    }

    public void setAUTHORINFO_ORG(String AUTHORINFO_ORG) {
        this.AUTHORINFO_ORG = AUTHORINFO_ORG;
    }

    public String getAUTHORINFO_FUNIT() {
        return AUTHORINFO_FUNIT;
    }

    public void setAUTHORINFO_FUNIT(String AUTHORINFO_FUNIT) {
        this.AUTHORINFO_FUNIT = AUTHORINFO_FUNIT;
    }

    public String getAUTHORINFO_SUNIT() {
        return AUTHORINFO_SUNIT;
    }

    public void setAUTHORINFO_SUNIT(String AUTHORINFO_SUNIT) {
        this.AUTHORINFO_SUNIT = AUTHORINFO_SUNIT;
    }

    public String getAUTHORINFO_JGLX() {
        return AUTHORINFO_JGLX;
    }

    public void setAUTHORINFO_JGLX(String AUTHORINFO_JGLX) {
        this.AUTHORINFO_JGLX = AUTHORINFO_JGLX;
    }

    public String getAUTHORINFO_ORGPROVINCE() {
        return AUTHORINFO_ORGPROVINCE;
    }

    public void setAUTHORINFO_ORGPROVINCE(String AUTHORINFO_ORGPROVINCE) {
        this.AUTHORINFO_ORGPROVINCE = AUTHORINFO_ORGPROVINCE;
    }

    public String getAUTHORINFO_ORGCITY() {
        return AUTHORINFO_ORGCITY;
    }

    public void setAUTHORINFO_ORGCITY(String AUTHORINFO_ORGCITY) {
        this.AUTHORINFO_ORGCITY = AUTHORINFO_ORGCITY;
    }

    public String getAUTHORINFO_ORGCOUNTRY() {
        return AUTHORINFO_ORGCOUNTRY;
    }

    public void setAUTHORINFO_ORGCOUNTRY(String AUTHORINFO_ORGCOUNTRY) {
        this.AUTHORINFO_ORGCOUNTRY = AUTHORINFO_ORGCOUNTRY;
    }

    public String getAUTHORINFO_AUID() {
        return AUTHORINFO_AUID;
    }

    public void setAUTHORINFO_AUID(String AUTHORINFO_AUID) {
        this.AUTHORINFO_AUID = AUTHORINFO_AUID;
    }

    public String getAUTHORINFO_ORGID() {
        return AUTHORINFO_ORGID;
    }

    public void setAUTHORINFO_ORGID(String AUTHORINFO_ORGID) {
        this.AUTHORINFO_ORGID = AUTHORINFO_ORGID;
    }

    public String getORGINFO() {
        return ORGINFO;
    }

    public void setORGINFO(String ORGINFO) {
        this.ORGINFO = ORGINFO;
    }

    public String getORGINFO_ORG() {
        return ORGINFO_ORG;
    }

    public void setORGINFO_ORG(String ORGINFO_ORG) {
        this.ORGINFO_ORG = ORGINFO_ORG;
    }

    public String getORGINFO_CX() {
        return ORGINFO_CX;
    }

    public void setORGINFO_CX(String ORGINFO_CX) {
        this.ORGINFO_CX = ORGINFO_CX;
    }

    public String getORGINFO_ORGTYPE() {
        return ORGINFO_ORGTYPE;
    }

    public void setORGINFO_ORGTYPE(String ORGINFO_ORGTYPE) {
        this.ORGINFO_ORGTYPE = ORGINFO_ORGTYPE;
    }

    public String getORGINFO_SHENG() {
        return ORGINFO_SHENG;
    }

    public void setORGINFO_SHENG(String ORGINFO_SHENG) {
        this.ORGINFO_SHENG = ORGINFO_SHENG;
    }

    public String getORGINFO_SHI() {
        return ORGINFO_SHI;
    }

    public void setORGINFO_SHI(String ORGINFO_SHI) {
        this.ORGINFO_SHI = ORGINFO_SHI;
    }

    public String getORGINFO_XIAN() {
        return ORGINFO_XIAN;
    }

    public void setORGINFO_XIAN(String ORGINFO_XIAN) {
        this.ORGINFO_XIAN = ORGINFO_XIAN;
    }

    public String getORGINFO_RELATION() {
        return ORGINFO_RELATION;
    }

    public void setORGINFO_RELATION(String ORGINFO_RELATION) {
        this.ORGINFO_RELATION = ORGINFO_RELATION;
    }

    public String getORGINFO_ORGLEVEL1() {
        return ORGINFO_ORGLEVEL1;
    }

    public void setORGINFO_ORGLEVEL1(String ORGINFO_ORGLEVEL1) {
        this.ORGINFO_ORGLEVEL1 = ORGINFO_ORGLEVEL1;
    }

    public String getORGINFO_ORGLEVEL2() {
        return ORGINFO_ORGLEVEL2;
    }

    public void setORGINFO_ORGLEVEL2(String ORGINFO_ORGLEVEL2) {
        this.ORGINFO_ORGLEVEL2 = ORGINFO_ORGLEVEL2;
    }

    public String getORGINFO_ORGLEVEL3() {
        return ORGINFO_ORGLEVEL3;
    }

    public void setORGINFO_ORGLEVEL3(String ORGINFO_ORGLEVEL3) {
        this.ORGINFO_ORGLEVEL3 = ORGINFO_ORGLEVEL3;
    }

    public String getORGINFO_ORGLEVEL4() {
        return ORGINFO_ORGLEVEL4;
    }

    public void setORGINFO_ORGLEVEL4(String ORGINFO_ORGLEVEL4) {
        this.ORGINFO_ORGLEVEL4 = ORGINFO_ORGLEVEL4;
    }

    public String getORGINFO_ORGLEVEL5() {
        return ORGINFO_ORGLEVEL5;
    }

    public void setORGINFO_ORGLEVEL5(String ORGINFO_ORGLEVEL5) {
        this.ORGINFO_ORGLEVEL5 = ORGINFO_ORGLEVEL5;
    }
}


