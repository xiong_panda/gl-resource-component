package com.gl.biz.city.po;

import java.sql.Date;

public class PatentPO {
    private String id;
    private String orderId;
    private String fullText;
    private String fullName;
    private String abstractName;
    private String orgAgency;
    private String agent;
    private String publicationNo;
    private Date publicationDate;
    private String author;
    private String authorName;
    private String cate;
    private String cdId;
    private Date createDate;
    private String classCode;
    private String countryCode;
    private Date date;
    private String databaseIdentity;
    private Date dec;
    private String disciplineClass;
    private String indCla;
    private String lawStatus;
    private String intApp;
    private String intPush;
    private String invPat;
    private String keyWord;
    private String originRequestNumber;
    private String org;
    private String orgType;
    private String patentNumberName;
    private String patentTypeName;
    private String pagination;
    private String priority;
    private String pubPath;
    private String requestAddress;
    private String mainClassCode2;
    private String authorInfoName;
    private String authorInfoOrder;
    private String authorInfoUnit;
    private String authorInfoId;
    private String authorInfoUnitId;
    private String orgInfoName;
    private String orgInfoOrder;
    private String orgInfoType;
    private String orgInfoProvince;
    private String orgInfoCity;
    private String orgInfoCounty;
    //国内专利还是国外专利
    private String patentFrom;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAbstractName() {
        return abstractName;
    }

    public void setAbstractName(String abstractName) {
        this.abstractName = abstractName;
    }

    public String getOrgAgency() {
        return orgAgency;
    }

    public void setOrgAgency(String orgAgency) {
        this.orgAgency = orgAgency;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getPublicationNo() {
        return publicationNo;
    }

    public void setPublicationNo(String publicationNo) {
        this.publicationNo = publicationNo;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getCdId() {
        return cdId;
    }

    public void setCdId(String cdId) {
        this.cdId = cdId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDatabaseIdentity() {
        return databaseIdentity;
    }

    public void setDatabaseIdentity(String databaseIdentity) {
        this.databaseIdentity = databaseIdentity;
    }

    public Date getDec() {
        return dec;
    }

    public void setDec(Date dec) {
        this.dec = dec;
    }

    public String getDisciplineClass() {
        return disciplineClass;
    }

    public void setDisciplineClass(String disciplineClass) {
        this.disciplineClass = disciplineClass;
    }

    public String getIndCla() {
        return indCla;
    }

    public void setIndCla(String indCla) {
        this.indCla = indCla;
    }

    public String getLawStatus() {
        return lawStatus;
    }

    public void setLawStatus(String lawStatus) {
        this.lawStatus = lawStatus;
    }

    public String getIntApp() {
        return intApp;
    }

    public void setIntApp(String intApp) {
        this.intApp = intApp;
    }

    public String getIntPush() {
        return intPush;
    }

    public void setIntPush(String intPush) {
        this.intPush = intPush;
    }

    public String getInvPat() {
        return invPat;
    }

    public void setInvPat(String invPat) {
        this.invPat = invPat;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getOriginRequestNumber() {
        return originRequestNumber;
    }

    public void setOriginRequestNumber(String originRequestNumber) {
        this.originRequestNumber = originRequestNumber;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getPatentNumberName() {
        return patentNumberName;
    }

    public void setPatentNumberName(String patentNumberName) {
        this.patentNumberName = patentNumberName;
    }

    public String getPatentTypeName() {
        return patentTypeName;
    }

    public void setPatentTypeName(String patentTypeName) {
        this.patentTypeName = patentTypeName;
    }

    public String getPagination() {
        return pagination;
    }

    public void setPagination(String pagination) {
        this.pagination = pagination;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPubPath() {
        return pubPath;
    }

    public void setPubPath(String pubPath) {
        this.pubPath = pubPath;
    }

    public String getRequestAddress() {
        return requestAddress;
    }

    public void setRequestAddress(String requestAddress) {
        this.requestAddress = requestAddress;
    }

    public String getMainClassCode2() {
        return mainClassCode2;
    }

    public void setMainClassCode2(String mainClassCode2) {
        this.mainClassCode2 = mainClassCode2;
    }

    public String getAuthorInfoName() {
        return authorInfoName;
    }

    public void setAuthorInfoName(String authorInfoName) {
        this.authorInfoName = authorInfoName;
    }

    public String getAuthorInfoOrder() {
        return authorInfoOrder;
    }

    public void setAuthorInfoOrder(String authorInfoOrder) {
        this.authorInfoOrder = authorInfoOrder;
    }

    public String getAuthorInfoUnit() {
        return authorInfoUnit;
    }

    public void setAuthorInfoUnit(String authorInfoUnit) {
        this.authorInfoUnit = authorInfoUnit;
    }

    public String getAuthorInfoId() {
        return authorInfoId;
    }

    public void setAuthorInfoId(String authorInfoId) {
        this.authorInfoId = authorInfoId;
    }

    public String getAuthorInfoUnitId() {
        return authorInfoUnitId;
    }

    public void setAuthorInfoUnitId(String authorInfoUnitId) {
        this.authorInfoUnitId = authorInfoUnitId;
    }

    public String getOrgInfoName() {
        return orgInfoName;
    }

    public void setOrgInfoName(String orgInfoName) {
        this.orgInfoName = orgInfoName;
    }

    public String getOrgInfoOrder() {
        return orgInfoOrder;
    }

    public void setOrgInfoOrder(String orgInfoOrder) {
        this.orgInfoOrder = orgInfoOrder;
    }

    public String getOrgInfoType() {
        return orgInfoType;
    }

    public void setOrgInfoType(String orgInfoType) {
        this.orgInfoType = orgInfoType;
    }

    public String getOrgInfoProvince() {
        return orgInfoProvince;
    }

    public void setOrgInfoProvince(String orgInfoProvince) {
        this.orgInfoProvince = orgInfoProvince;
    }

    public String getOrgInfoCity() {
        return orgInfoCity;
    }

    public void setOrgInfoCity(String orgInfoCity) {
        this.orgInfoCity = orgInfoCity;
    }

    public String getOrgInfoCounty() {
        return orgInfoCounty;
    }

    public void setOrgInfoCounty(String orgInfoCounty) {
        this.orgInfoCounty = orgInfoCounty;
    }
}
