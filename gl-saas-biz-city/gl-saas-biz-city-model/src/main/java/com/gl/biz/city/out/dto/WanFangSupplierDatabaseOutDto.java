package com.gl.biz.city.out.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 供应商筛选--聚合企业、专利字段
 * @author code_generator
 */
@Data
public class WanFangSupplierDatabaseOutDto implements java.io.Serializable{

	/** 记录顺序号 **/
	@ApiModelProperty(value = "记录顺序号")
	private String orderid ;

	/** 唯一编号 **/
	@ApiModelProperty(value = "唯一编号")
	private String id ;


	/** 全文索引 **/
	@ApiModelProperty(value = "全文索引")
	private String fulltext ;

	/** 企业名称:铭牌:简称:曾用名 **/
	@ApiModelProperty(value = "企业名称:铭牌:简称:曾用名")
	private String corpFullname ;

	/**  **/
	@ApiModelProperty(value = "")
	private String titleAnyname ;

	/**  **/
	@ApiModelProperty(value = "")
	private String anyname ;

	/**  **/

	@ApiModelProperty(value = "")
	private String acname ;

	/** 企业名称 **/
	@ApiModelProperty(value = "企业名称")
	private String corpName ;

	/** 铭牌 **/
	@ApiModelProperty(value = "铭牌")
	private String corpNameplate ;

	/** 简称 **/
	@ApiModelProperty(value = "简称")
	private String corpShortname ;

	/** 曾用名 **/
	@ApiModelProperty(value = "曾用名")
	private String corpOldname ;

	/** 负责人姓名 **/
	@ApiModelProperty(value = "负责人姓名")
	private String corpPerson ;

	/** 负责人 **/
	@ApiModelProperty(value = "负责人")
	private String corpMultiperson ;

	/** 省名 **/
	@ApiModelProperty(value = "省名")
	private String corpProv ;

	/** 市名 **/
	@ApiModelProperty(value = "市名")
	private String corpCity ;

	/** 县名 **/
	@ApiModelProperty(value = "县名")
	private String corpCoun ;

	/** 行政区代码 **/
	@ApiModelProperty(value = "行政区代码")
	private String corpRegion ;

	/** 地址 **/
	@ApiModelProperty(value = "地址")
	private String corpAddr ;

	/** 电话 **/
	@ApiModelProperty(value = "电话")
	private String telphone ;

	/** 区位号 **/
	@ApiModelProperty(value = "区位号")
	private String corpRecode ;

	/** 传真 **/
	@ApiModelProperty(value = "传真")
	private String corpFax ;

	/** 邮码 **/
	@ApiModelProperty(value = "邮码")
	private String postalCode ;

	/** 电子邮件 **/
	@ApiModelProperty(value = "电子邮件")
	private String corpEmail ;

	/** 网址 **/
	@ApiModelProperty(value = "网址")
	private String corpUrl ;

	/** 成立年代 **/
	@ApiModelProperty(value = "成立年代")
	private String corpFound ;

	/** 注册资金 **/
	@ApiModelProperty(value = "注册资金")
	private String corpMoney ;

	/** 固定资产 **/
	@ApiModelProperty(value = "固定资产")
	private String corpAsset ;

	/** 职工人数 **/
	@ApiModelProperty(value = "职工人数")
	private String corpEmps ;

	/** 技术人员数 **/
	@ApiModelProperty(value = "技术人员数")
	private String corpTechs ;

	/** 营业额 **/
	@ApiModelProperty(value = "营业额")
	private String corpTurnover ;

	/** 利税 **/
	@ApiModelProperty(value = "利税")
	private String corpTax ;

	/** 创汇额 **/
	@ApiModelProperty(value = "创汇额")
	private String corpExchange ;

	/** 进出口权 **/
	@ApiModelProperty(value = "进出口权")
	private String corpImpexp ;

	/** 性质与级别 **/
	@ApiModelProperty(value = "性质与级别")
	private String corpLevel ;

	/** 股票代码 **/
	@ApiModelProperty(value = "股票代码")
	private String corpStock ;

	/** 机构类型 **/
	@ApiModelProperty(value = "机构类型")
	private String orgType ;

	/** 企业简介 **/
	@ApiModelProperty(value = "企业简介")
	private String corpIntro ;

	/** 企业占地面积 **/
	@ApiModelProperty(value = "企业占地面积")
	private String corpSpace ;

	/** 厂房办公面积 **/
	@ApiModelProperty(value = "厂房办公面积")
	private String corpArea ;

	/** 主管单位 **/
	@ApiModelProperty(value = "主管单位")
	private String corpCompunit ;

	/** 派出机构 **/
	@ApiModelProperty(value = "派出机构")
	private String corp ;

	/** 行业GBM **/
	@ApiModelProperty(value = "行业GBM")
	private String igbm ;

	/**  **/
	@ApiModelProperty(value = "")
	private String igbmAnyname ;

	/**  **/
	@ApiModelProperty(value = "")
	private String igbmAnyname2 ;

	/** 行业GBM **/
	@ApiModelProperty(value = "行业GBM")
	private String figbm ;

	/**  **/
	@ApiModelProperty(value = "")
	private String figbmAnyname ;

	/** 机构名称 **/
	@ApiModelProperty(value = "机构名称")
	private String orgName ;

	/** 机构名称(全文检索) **/
	@ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ;

	/** 机构全文检索 **/
	@ApiModelProperty(value = "机构全文检索")
	private String orgFulltextSearch ;

	/** 行业SIC **/
	@ApiModelProperty(value = "行业SIC")
	private String isic ;

	/** 商标 **/
	@ApiModelProperty(value = "商标")
	private String brand ;

	/** 产品信息 **/
	@ApiModelProperty(value = "产品信息")
	private String productInfo ;

	/** 经营项目 **/
	@ApiModelProperty(value = "经营项目")
	private String businessProject ;

	/** 经营项目英 **/
	@ApiModelProperty(value = "经营项目英")
	private String businessProjectEn ;

	/** 产品关键词:英文产品关键词 **/
	@ApiModelProperty(value = "产品关键词:英文产品关键词")
	private String fullkeyword ;

	/** 产品关键词 **/
	@ApiModelProperty(value = "产品关键词")
	private String productKeyword ;

	/** 英文产品关键词 **/
	@ApiModelProperty(value = "英文产品关键词")
	private String englishProductKeyword ;

	/** 产品SIC **/
	@ApiModelProperty(value = "产品SIC")
	private String psic ;

	/** 产品GBM **/
	@ApiModelProperty(value = "产品GBM")
	private String pgbm ;

	/** 企业排名 **/
	@ApiModelProperty(value = "企业排名")
	private String corpRanking ;

	/** 重点行业 **/
	@ApiModelProperty(value = "重点行业")
	private String corpKeyent ;

	/** 成品数据 **/
	@ApiModelProperty(value = "成品数据")
	private String corpCpdata ;

	/** 电力数据 **/
	@ApiModelProperty(value = "电力数据")
	private String corpDldata ;

	/** 冶金数据 **/
	@ApiModelProperty(value = "冶金数据")
	private String corpYjdata ;

	/** 机构ID **/
	@ApiModelProperty(value = "机构ID")
	private String orgId ;

	/** --企业排名 **/
	@ApiModelProperty(value = "--企业排名")
	private String crankname ;

	/**  **/
	@ApiModelProperty(value = "")
	private String crankname1 ;

	/**  **/
	@ApiModelProperty(value = "")
	private String crankname2 ;

	/** --企业排名 **/
	@ApiModelProperty(value = "--企业排名")
	private String cranksource ;

	/**  **/
	@ApiModelProperty(value = "")
	private String cranksource1 ;

	/**  **/
	@ApiModelProperty(value = "")
	private String cranksource2 ;

	/** --企业排名 **/
	@ApiModelProperty(value = "--企业排名")
	private String cranktype ;

	/**  **/
	@ApiModelProperty(value = "")
	private String cranktype1 ;

	/**  **/
	@ApiModelProperty(value = "")
	private String cranktype2 ;

	/** 是否成品数据 **/
	@ApiModelProperty(value = "是否成品数据")
	private String isFinishedProductData ;


	//---------专利字段

	/** 记录顺序号 **/
	@ApiModelProperty(value = "记录顺序号")
	private String cpOrderid ;//

	/** 唯一编号 **/
	@ApiModelProperty(value = "唯一编号")
	private String cpId ;//

	/** 全文索引 **/
	@ApiModelProperty(value = "全文索引")
	private String cpFulltext ;//

	/** 主题全文检索(标题、关键词、摘要) **/
	@ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ;

	/** 摘要 **/
	@ApiModelProperty(value = "摘要")
	private String Abstract;

	/** 专利代理机构 **/
	@ApiModelProperty(value = "专利代理机构")
	private String orgAgency ;

	/** 代理人 **/
	@ApiModelProperty(value = "代理人")
	private String agent ;

	/** 公开（公告）号 **/
	@ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ;

	/** 公开（公告）日 **/
	@ApiModelProperty(value = "公开（公告）日")
	private Date publicationDate ;

	/** 发明（设计）人 **/
	@ApiModelProperty(value = "发明（设计）人")
	private String author ;

	/**  **/
	@ApiModelProperty(value = "")
	private String fAuthor ;

	/** 发明（设计）人 **/
	@ApiModelProperty(value = "发明（设计）人")
	private String author2 ;

	/** 作者名称(全文检索) **/
	@ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ;

	/** 作者名称(全文检索) **/
	@ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ;

	/** 范畴分类 **/
	@ApiModelProperty(value = "范畴分类")
	private String cate ;

	/** 光盘号 **/
	@ApiModelProperty(value = "光盘号")
	private String cdId ;

	/** 颁证日 **/
	@ApiModelProperty(value = "颁证日")
	private Date createDate ;

	/** 分类号 **/
	@ApiModelProperty(value = "分类号")
	private String classCode ;

	/** 国省代码 **/
	@ApiModelProperty(value = "国省代码")
	private String countryCode ;

	/** 申请日 **/
	@ApiModelProperty(value = "申请日")
	private Date date ;

	/** 数据库标识 **/
	@ApiModelProperty(value = "数据库标识")
	private String databaseIdentity ;

	/** 进入国家日期 **/
	@ApiModelProperty(value = "进入国家日期")
	private Date dec ;

	/** 学科分类 **/
	@ApiModelProperty(value = "学科分类")
	private String disciplineClass ;

	/** 主权项 **/
	@ApiModelProperty(value = "主权项")
	private String indCla ;

	/** 法律状态F_LawStatus **/
	@ApiModelProperty(value = "法律状态F_LawStatus")
	private String lawStatus ;

	/**  **/
	@ApiModelProperty(value = "")
	private String ls ;

	/** 规范单位名称 **/
	@ApiModelProperty(value = "规范单位名称")
	private String orgNormName ;

	/** 国际申请 **/
	@ApiModelProperty(value = "国际申请")
	private String intApp ;

	/** 国际公布 **/
	@ApiModelProperty(value = "国际公布")
	private String intPush ;

	/** 失效专利 **/
	@ApiModelProperty(value = "失效专利")
	private String invPat ;

	/** 关键词 **/
	@ApiModelProperty(value = "关键词")
	private String keyword ;

	/** 主分类号 **/
	@ApiModelProperty(value = "主分类号")
	private String mainClassCode ;

	/** 中图分类 **/
	@ApiModelProperty(value = "中图分类")
	private String cls ;

	/** 中图分类(二级) **/
	@ApiModelProperty(value = "中图分类(二级)")
	private String clsLevel2 ;

	/** 分案原申请号 **/
	@ApiModelProperty(value = "分案原申请号")
	private String originRequestNumber ;

	/** 申请（专利权）人 **/
	@ApiModelProperty(value = "申请（专利权）人")
	private String org ;

	/** 机构名称(全文检索) **/
	@ApiModelProperty(value = "机构名称(全文检索)")
	private String cpOrgNameSearch ;//

	/** 申请（专利权）人 **/
	@ApiModelProperty(value = "申请（专利权）人")
	private String orgAnyname2Name ;

	/** 规范单位名称 **/
	@ApiModelProperty(value = "规范单位名称")
	private String orgNormName2 ;

	/** 机构类型 **/
	@ApiModelProperty(value = "机构类型")
	private String cpOrgType ;//

	/** 专利号 **/
	@ApiModelProperty(value = "专利号")
	private String patentnumberName ;

	/** 专利类型 **/
	@ApiModelProperty(value = "专利类型")
	private String patenttypeName ;

	/** 页数 **/
	@ApiModelProperty(value = "页数")
	private String pages ;

	/** 优先权 **/
	@ApiModelProperty(value = "优先权")
	private String priority ;

	/** 发布路径 **/
	@ApiModelProperty(value = "发布路径")
	private String pubPath ;

	/** 参考文献 **/
	@ApiModelProperty(value = "参考文献")
	private String reference ;

	/** 地址 **/
	@ApiModelProperty(value = "地址")
	private String requestAddress ;

	/** 申请号 **/
	@ApiModelProperty(value = "申请号")
	private String requestNumber ;

	/** 申请号wjy **/
	@ApiModelProperty(value = "申请号wjy")
	private String requestNo ;

	/** 申请（专利权）人 **/
	@ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ;

	/** 审查员 **/
	@ApiModelProperty(value = "审查员")
	private String reviewer ;

	/** 服务器地址 **/
	@ApiModelProperty(value = "服务器地址")
	private String serverAddress ;

	/** 数据来源 **/
	@ApiModelProperty(value = "数据来源")
	private String dataSource ;

	/** 名称 **/
	@ApiModelProperty(value = "名称")
	private String title1 ;

	/** 名称 **/
	@ApiModelProperty(value = "名称")
	private String title2 ;

	/**  **/
	@ApiModelProperty(value = "")
	private Date yannodate ;

	/** 申请日 **/
	@ApiModelProperty(value = "申请日")
	private String date2 ;

	/** 主分类号 **/
	@ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ;

	/**  **/
	@ApiModelProperty(value = "")
	private String smcls ;

	/**  **/
	@ApiModelProperty(value = "")
	private String tmcls ;

	/**  **/
	@ApiModelProperty(value = "")
	private String ckey ;

	/** 机构ID **/
	@ApiModelProperty(value = "机构ID")
	private String cpOrgId ;//

	/** 机构层级ID **/
	@ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ;

	/** 机构所在省 **/
	@ApiModelProperty(value = "机构所在省")
	private String orgProvince ;

	/** 机构所在市 **/
	@ApiModelProperty(value = "机构所在市")
	private String orgCity ;

	/**  **/
	@ApiModelProperty(value = "")
	private String endOrgType ;

	/**  **/
	@ApiModelProperty(value = "")
	private String fOrgId ;

	/** 第一机构终级机构ID **/
	@ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ;

	/** 第一机构层级机构ID **/
	@ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ;

	/** 第一机构所在省 **/
	@ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ;

	/** 第一机构所在省 **/
	@ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ;

	/** 第一机构终级机构所在市 **/
	@ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ;

	/** 第一机构所在市 **/
	@ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ;

	/** 第一机构终级机构类型 **/
	@ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ;

	/** 第一机构类型 **/
	@ApiModelProperty(value = "第一机构类型")
	private String orgFristType ;

	/**  **/
	@ApiModelProperty(value = "")
	private String authorId ;

	/**  **/
	@ApiModelProperty(value = "")
	private String orgNum ;

	/** 文献权重 **/
	@ApiModelProperty(value = "文献权重")
	private String literatureWeight ;

	/**  **/
	@ApiModelProperty(value = "")
	private String quarter ;

	/**  **/
	@ApiModelProperty(value = "")
	private String halfyear ;

	/** 索引管理字段(无用) **/
	@ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ;

	/**  **/
	@ApiModelProperty(value = "")
	private String authorInfo ;

	/** 作者信息.姓名 **/
	@ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ;

	/** 作者信息.作者次序 **/
	@ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ;

	/** 作者信息.工作单位 **/
	@ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ;

	/** 作者信息.工作单位一级机构 **/
	@ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ;

	/** 作者信息.工作单位二级机构 **/
	@ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ;

	/** 作者信息.工作单位类型 **/
	@ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ;

	/** 作者信息.工作单位所在省 **/
	@ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ;

	/** 作者信息.工作单位所在市 **/
	@ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ;

	/** 作者信息.工作单位所在县 **/
	@ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ;

	/** 作者信息.唯一ID **/
	@ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ;

	/** 作者信息.工作单位唯一ID **/
	@ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ;

	/**  **/
	@ApiModelProperty(value = "")
	private String orgInfo ;

	/** 机构信息.机构名称 **/
	@ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ;

	/** 机构信息.机构次序 **/
	@ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ;

	/** 机构信息.机构类型 **/
	@ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ;

	/** 机构信息.省 **/
	@ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ;

	/** 机构信息.市 **/
	@ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ;

	/** 机构信息.县 **/
	@ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ;

	/** 机构信息.五级机构层级码 **/
	@ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ;

	/** 机构信息.1级机构名称 **/
	@ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ;

	/** 机构信息.2级机构名称 **/
	@ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ;

	/** 机构信息.3级机构名称 **/
	@ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ;

	/** 机构信息.4级机构名称 **/
	@ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ;

	/** 机构信息.5级机构名称 **/
	@ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ;

}
