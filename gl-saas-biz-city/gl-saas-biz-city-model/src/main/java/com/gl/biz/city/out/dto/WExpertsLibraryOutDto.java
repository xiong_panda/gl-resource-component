package com.gl.biz.city.out.dto;

import com.gl.biz.city.anno.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 专家库
 */
@Data
public class WExpertsLibraryOutDto implements java.io.Serializable {

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;

    /**
     * 姓名
     **/
    @ApiModelProperty(value = "姓名")
    @FieldOrder(order = 4)
    private String name;

    /**
     * 性别
     **/
    @ApiModelProperty(value = "性别")
    @FieldOrder(order = 5)
    private String sex;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 6)
    private String age;

    /**
     * 出生日期
     **/
    @ApiModelProperty(value = "出生日期")
    @FieldOrder(order = 7)
    private String birthDate;

    /**
     * 出生地点
     **/
    @ApiModelProperty(value = "出生地点")
    @FieldOrder(order = 8)
    private String birthAddress;

    /**
     * 民族
     **/
    @ApiModelProperty(value = "民族")
    @FieldOrder(order = 9)
    private String national;

    /**
     * 工作单位:其它工作单位
     **/
    @ApiModelProperty(value = "工作单位:其它工作单位")
    @FieldOrder(order = 10)
    private String otherUnit;

    /**
     * 其它工作单位
     **/
    @ApiModelProperty(value = "其它工作单位")
    @FieldOrder(order = 11)
    private String otherUnit2;

    /**
     * 工作单位
     **/
    @ApiModelProperty(value = "工作单位")
    @FieldOrder(order = 12)
    private String unit;

    /**
     * 其它工作单位
     **/
    @ApiModelProperty(value = "其它工作单位")
    @FieldOrder(order = 13)
    private String otherUnit3;

    /**
     * 教育背景
     **/
    @ApiModelProperty(value = "教育背景")
    @FieldOrder(order = 14)
    private String educationBackground;

    /**
     * 外语语种
     **/
    @ApiModelProperty(value = "外语语种")
    @FieldOrder(order = 15)
    private String foreignLanguages;

    /**
     * 工作简历
     **/
    @ApiModelProperty(value = "工作简历")
    @FieldOrder(order = 16)
    private String jobResume;

    /**
     * 国内外学术或专业团体任职情况
     **/
    @ApiModelProperty(value = "国内外学术或专业团体任职情况")
    @FieldOrder(order = 17)
    private String coEmployment;

    /**
     * 工作职务
     **/
    @ApiModelProperty(value = "工作职务")
    @FieldOrder(order = 18)
    private String jobPosition;

    /**
     * 专家荣誉
     **/
    @ApiModelProperty(value = "专家荣誉")
    @FieldOrder(order = 19)
    private String expertsHonor;

    /**
     * 技术职称
     **/
    @ApiModelProperty(value = "技术职称")
    @FieldOrder(order = 20)
    private String technicalTitles;

    /**
     * 通讯地址
     **/
    @ApiModelProperty(value = "通讯地址")
    @FieldOrder(order = 21)
    private String address;

    /**
     * 邮码
     **/
    @ApiModelProperty(value = "邮码")
    @FieldOrder(order = 22)
    private String postalCode;

    /**
     * 区位
     **/
    @ApiModelProperty(value = "区位")
    @FieldOrder(order = 23)
    private String zoneBit;

    /**
     * 省
     **/
    @ApiModelProperty(value = "省")
    @FieldOrder(order = 24)
    private String province;

    /**
     * 市
     **/
    @ApiModelProperty(value = "市")
    @FieldOrder(order = 25)
    private String city;

    /**
     * 县
     **/
    @ApiModelProperty(value = "县")
    @FieldOrder(order = 26)
    private String county;

    /**
     * 行政码
     **/
    @ApiModelProperty(value = "行政码")
    @FieldOrder(order = 27)
    private String administrativeCode;

    /**
     * 电话
     **/
    @ApiModelProperty(value = "电话")
    @FieldOrder(order = 28)
    private String telphone;

    /**
     * 传真
     **/
    @ApiModelProperty(value = "传真")
    @FieldOrder(order = 29)
    private String fax;

    /**
     * 电子信箱
     **/
    @ApiModelProperty(value = "电子信箱")
    @FieldOrder(order = 30)
    private String email;

    /**
     * 专业领域与研究方向
     **/
    @ApiModelProperty(value = "专业领域与研究方向")
    @FieldOrder(order = 31)
    private String professionalResearch;

    /**
     * 专业领域与研究方向
     **/
    @ApiModelProperty(value = "专业领域与研究方向")
    @FieldOrder(order = 32)
    private String professionalResearch2;

    /**
     * 院士
     **/
    @ApiModelProperty(value = "院士")
    @FieldOrder(order = 33)
    private String academician;

    /**
     * 院士
     **/
    @ApiModelProperty(value = "院士")
    @FieldOrder(order = 34)
    private String academician2;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 35)
    private String awards;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 36)
    private String awardt;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 37)
    private String foreign;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 38)
    private String exptype;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 39)
    private String byind;

    /**
     * 学科分类名
     **/
    @ApiModelProperty(value = "学科分类名")
    @FieldOrder(order = 40)
    private String disciplineClass;

    /**
     * 学科分类名
     **/
    @ApiModelProperty(value = "学科分类名")
    @FieldOrder(order = 41)
    private String disciplineClass2;

    /**
     * 学科分类码
     **/
    @ApiModelProperty(value = "学科分类码")
    @FieldOrder(order = 42)
    private String disciplineClassCode;

    /**
     * 中图分类
     **/
    @ApiModelProperty(value = "中图分类")
    @FieldOrder(order = 43)
    private String cls;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 44)
    private String cls2;
}
