package com.gl.biz.city.po;

import com.gl.biz.city.anno.FieldOrder;


//核心资源数据 中文期刊
public class WAuthorLibraryPO {
    @FieldOrder(order = 1)
    private String F_ID;
    @FieldOrder(order = 2)
    private String ID;
    @FieldOrder(order = 3)
    private String _ID;
    @FieldOrder(order = 4)
    private String FullText;
    @FieldOrder(order = 5)
    private String NAME;
    @FieldOrder(order = 6)
    private String ORG;
    @FieldOrder(order = 7)
    private String ORG_ORG_ANYNAME;
    @FieldOrder(order = 8)
    private String ORG_ANYNAME;
    @FieldOrder(order = 9)
    private String ORGALL;
    @FieldOrder(order = 10)
    private String ORGALL_ORGALL_ANYNAME;
    @FieldOrder(order = 11)
    private String ORGALL_ANYNAME;
    @FieldOrder(order = 12)
    private String AORG;
    @FieldOrder(order = 13)
    private String BORG;
    @FieldOrder(order = 14)
    private String CORG;
    @FieldOrder(order = 15)
    private String EORG;
    @FieldOrder(order = 16)
    private String TITLE;
    @FieldOrder(order = 17)
    private String ABSTRACT;
    @FieldOrder(order = 18)
    private String INTRO;
    @FieldOrder(order = 19)
    private String CNAME;
    @FieldOrder(order = 20)
    private String ENAME;
    @FieldOrder(order = 21)
    private String TUTOR;
    @FieldOrder(order = 22)
    private String REGION;
    @FieldOrder(order = 23)
    private String ZIP;
    @FieldOrder(order = 24)
    private String ADDR;
    @FieldOrder(order = 25)
    private String EMAIL;
    @FieldOrder(order = 26)
    private String ORGTYPE;
    @FieldOrder(order = 27)
    private String DATE;
    @FieldOrder(order = 28)
    private String MEMO;
    @FieldOrder(order = 29)
    private String ORGID;
    @FieldOrder(order = 30)
    private String FREQ;
    @FieldOrder(order = 31)
    private String YFREQ;
    @FieldOrder(order = 32)
    private String KEYWORD;
    @FieldOrder(order = 33)
    private String KEYWORD_KEYWORD_ANYNAME;
    @FieldOrder(order = 34)
    private String KEYWORD_ANYNAME;
    @FieldOrder(order = 35)
    private String RELJOUR;
    @FieldOrder(order = 36)
    private String RELAU;
    @FieldOrder(order = 37)
    private String TZCID;
    @FieldOrder(order = 38)
    private String CID;
    @FieldOrder(order = 39)
    private String RELFUND;
    @FieldOrder(order = 40)
    private String QCODE;
    @FieldOrder(order = 41)
    private String GENDER;
    @FieldOrder(order = 42)
    private String BIRTHDATE;
    @FieldOrder(order = 43)
    private String ONJOB;
    @FieldOrder(order = 44)
    private String BIRTHPLACE;
    @FieldOrder(order = 45)
    private String NATION;
    @FieldOrder(order = 46)
    private String JOBTITLE;
    @FieldOrder(order = 47)
    private String ADMINCODE;
    @FieldOrder(order = 48)
    private String TEL;
    @FieldOrder(order = 49)
    private String FAX;
    @FieldOrder(order = 50)
    private String EDU;
    @FieldOrder(order = 51)
    private String DEGREE;
    @FieldOrder(order = 52)
    private String FOREIGHLANGUAGE;
    @FieldOrder(order = 53)
    private String LOCATION;
    @FieldOrder(order = 54)
    private String DIRECTION;
    @FieldOrder(order = 55)
    private String PRESENT;
    @FieldOrder(order = 56)
    private String ACADEMICIAN;
    @FieldOrder(order = 57)
    private String HORNOR;
    @FieldOrder(order = 58)
    private String HORNORS;
    @FieldOrder(order = 59)
    private String CHARACTER;
    @FieldOrder(order = 60)
    private String CHARACTER_CHARACTER_ANYNAME;
    @FieldOrder(order = 61)
    private String CHARACTER_ANYNAME;
    @FieldOrder(order = 62)
    private String CATE;
    @FieldOrder(order = 63)
    private String CATECODE;
    @FieldOrder(order = 64)
    private String CATEFCODE;
    @FieldOrder(order = 65)
    private String RESUME;
    @FieldOrder(order = 66)
    private String DATASOURCE;
    @FieldOrder(order = 67)
    private String H;
    @FieldOrder(order = 68)
    private String journalCount;
    @FieldOrder(order = 69)
    private String quoteCount;

    public String getF_ID() {
        return F_ID;
    }

    public void setF_ID(String f_ID) {
        F_ID = f_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getORG() {
        return ORG;
    }

    public void setORG(String ORG) {
        this.ORG = ORG;
    }

    public String getORG_ORG_ANYNAME() {
        return ORG_ORG_ANYNAME;
    }

    public void setORG_ORG_ANYNAME(String ORG_ORG_ANYNAME) {
        this.ORG_ORG_ANYNAME = ORG_ORG_ANYNAME;
    }

    public String getORG_ANYNAME() {
        return ORG_ANYNAME;
    }

    public void setORG_ANYNAME(String ORG_ANYNAME) {
        this.ORG_ANYNAME = ORG_ANYNAME;
    }

    public String getORGALL() {
        return ORGALL;
    }

    public void setORGALL(String ORGALL) {
        this.ORGALL = ORGALL;
    }

    public String getORGALL_ORGALL_ANYNAME() {
        return ORGALL_ORGALL_ANYNAME;
    }

    public void setORGALL_ORGALL_ANYNAME(String ORGALL_ORGALL_ANYNAME) {
        this.ORGALL_ORGALL_ANYNAME = ORGALL_ORGALL_ANYNAME;
    }

    public String getORGALL_ANYNAME() {
        return ORGALL_ANYNAME;
    }

    public void setORGALL_ANYNAME(String ORGALL_ANYNAME) {
        this.ORGALL_ANYNAME = ORGALL_ANYNAME;
    }

    public String getAORG() {
        return AORG;
    }

    public void setAORG(String AORG) {
        this.AORG = AORG;
    }

    public String getBORG() {
        return BORG;
    }

    public void setBORG(String BORG) {
        this.BORG = BORG;
    }

    public String getCORG() {
        return CORG;
    }

    public void setCORG(String CORG) {
        this.CORG = CORG;
    }

    public String getEORG() {
        return EORG;
    }

    public void setEORG(String EORG) {
        this.EORG = EORG;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getABSTRACT() {
        return ABSTRACT;
    }

    public void setABSTRACT(String ABSTRACT) {
        this.ABSTRACT = ABSTRACT;
    }

    public String getINTRO() {
        return INTRO;
    }

    public void setINTRO(String INTRO) {
        this.INTRO = INTRO;
    }

    public String getCNAME() {
        return CNAME;
    }

    public void setCNAME(String CNAME) {
        this.CNAME = CNAME;
    }

    public String getENAME() {
        return ENAME;
    }

    public void setENAME(String ENAME) {
        this.ENAME = ENAME;
    }

    public String getTUTOR() {
        return TUTOR;
    }

    public void setTUTOR(String TUTOR) {
        this.TUTOR = TUTOR;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getORGTYPE() {
        return ORGTYPE;
    }

    public void setORGTYPE(String ORGTYPE) {
        this.ORGTYPE = ORGTYPE;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getMEMO() {
        return MEMO;
    }

    public void setMEMO(String MEMO) {
        this.MEMO = MEMO;
    }

    public String getORGID() {
        return ORGID;
    }

    public void setORGID(String ORGID) {
        this.ORGID = ORGID;
    }

    public String getFREQ() {
        return FREQ;
    }

    public void setFREQ(String FREQ) {
        this.FREQ = FREQ;
    }

    public String getYFREQ() {
        return YFREQ;
    }

    public void setYFREQ(String YFREQ) {
        this.YFREQ = YFREQ;
    }

    public String getKEYWORD() {
        return KEYWORD;
    }

    public void setKEYWORD(String KEYWORD) {
        this.KEYWORD = KEYWORD;
    }

    public String getKEYWORD_KEYWORD_ANYNAME() {
        return KEYWORD_KEYWORD_ANYNAME;
    }

    public void setKEYWORD_KEYWORD_ANYNAME(String KEYWORD_KEYWORD_ANYNAME) {
        this.KEYWORD_KEYWORD_ANYNAME = KEYWORD_KEYWORD_ANYNAME;
    }

    public String getKEYWORD_ANYNAME() {
        return KEYWORD_ANYNAME;
    }

    public void setKEYWORD_ANYNAME(String KEYWORD_ANYNAME) {
        this.KEYWORD_ANYNAME = KEYWORD_ANYNAME;
    }

    public String getRELJOUR() {
        return RELJOUR;
    }

    public void setRELJOUR(String RELJOUR) {
        this.RELJOUR = RELJOUR;
    }

    public String getRELAU() {
        return RELAU;
    }

    public void setRELAU(String RELAU) {
        this.RELAU = RELAU;
    }

    public String getTZCID() {
        return TZCID;
    }

    public void setTZCID(String TZCID) {
        this.TZCID = TZCID;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getRELFUND() {
        return RELFUND;
    }

    public void setRELFUND(String RELFUND) {
        this.RELFUND = RELFUND;
    }

    public String getQCODE() {
        return QCODE;
    }

    public void setQCODE(String QCODE) {
        this.QCODE = QCODE;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getBIRTHDATE() {
        return BIRTHDATE;
    }

    public void setBIRTHDATE(String BIRTHDATE) {
        this.BIRTHDATE = BIRTHDATE;
    }

    public String getONJOB() {
        return ONJOB;
    }

    public void setONJOB(String ONJOB) {
        this.ONJOB = ONJOB;
    }

    public String getBIRTHPLACE() {
        return BIRTHPLACE;
    }

    public void setBIRTHPLACE(String BIRTHPLACE) {
        this.BIRTHPLACE = BIRTHPLACE;
    }

    public String getNATION() {
        return NATION;
    }

    public void setNATION(String NATION) {
        this.NATION = NATION;
    }

    public String getJOBTITLE() {
        return JOBTITLE;
    }

    public void setJOBTITLE(String JOBTITLE) {
        this.JOBTITLE = JOBTITLE;
    }

    public String getADMINCODE() {
        return ADMINCODE;
    }

    public void setADMINCODE(String ADMINCODE) {
        this.ADMINCODE = ADMINCODE;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getEDU() {
        return EDU;
    }

    public void setEDU(String EDU) {
        this.EDU = EDU;
    }

    public String getDEGREE() {
        return DEGREE;
    }

    public void setDEGREE(String DEGREE) {
        this.DEGREE = DEGREE;
    }

    public String getFOREIGHLANGUAGE() {
        return FOREIGHLANGUAGE;
    }

    public void setFOREIGHLANGUAGE(String FOREIGHLANGUAGE) {
        this.FOREIGHLANGUAGE = FOREIGHLANGUAGE;
    }

    public String getLOCATION() {
        return LOCATION;
    }

    public void setLOCATION(String LOCATION) {
        this.LOCATION = LOCATION;
    }

    public String getDIRECTION() {
        return DIRECTION;
    }

    public void setDIRECTION(String DIRECTION) {
        this.DIRECTION = DIRECTION;
    }

    public String getPRESENT() {
        return PRESENT;
    }

    public void setPRESENT(String PRESENT) {
        this.PRESENT = PRESENT;
    }

    public String getACADEMICIAN() {
        return ACADEMICIAN;
    }

    public void setACADEMICIAN(String ACADEMICIAN) {
        this.ACADEMICIAN = ACADEMICIAN;
    }

    public String getHORNOR() {
        return HORNOR;
    }

    public void setHORNOR(String HORNOR) {
        this.HORNOR = HORNOR;
    }

    public String getHORNORS() {
        return HORNORS;
    }

    public void setHORNORS(String HORNORS) {
        this.HORNORS = HORNORS;
    }

    public String getCHARACTER() {
        return CHARACTER;
    }

    public void setCHARACTER(String CHARACTER) {
        this.CHARACTER = CHARACTER;
    }

    public String getCHARACTER_CHARACTER_ANYNAME() {
        return CHARACTER_CHARACTER_ANYNAME;
    }

    public void setCHARACTER_CHARACTER_ANYNAME(String CHARACTER_CHARACTER_ANYNAME) {
        this.CHARACTER_CHARACTER_ANYNAME = CHARACTER_CHARACTER_ANYNAME;
    }

    public String getCHARACTER_ANYNAME() {
        return CHARACTER_ANYNAME;
    }

    public void setCHARACTER_ANYNAME(String CHARACTER_ANYNAME) {
        this.CHARACTER_ANYNAME = CHARACTER_ANYNAME;
    }

    public String getCATE() {
        return CATE;
    }

    public void setCATE(String CATE) {
        this.CATE = CATE;
    }

    public String getCATECODE() {
        return CATECODE;
    }

    public void setCATECODE(String CATECODE) {
        this.CATECODE = CATECODE;
    }

    public String getCATEFCODE() {
        return CATEFCODE;
    }

    public void setCATEFCODE(String CATEFCODE) {
        this.CATEFCODE = CATEFCODE;
    }

    public String getRESUME() {
        return RESUME;
    }

    public void setRESUME(String RESUME) {
        this.RESUME = RESUME;
    }

    public String getDATASOURCE() {
        return DATASOURCE;
    }

    public void setDATASOURCE(String DATASOURCE) {
        this.DATASOURCE = DATASOURCE;
    }

    public String getH() {
        return H;
    }

    public void setH(String h) {
        H = h;
    }

    public String getJournalCount() {
        return journalCount;
    }

    public void setJournalCount(String journalCount) {
        this.journalCount = journalCount;
    }

    public String getQuoteCount() {
        return quoteCount;
    }

    public void setQuoteCount(String quoteCount) {
        this.quoteCount = quoteCount;
    }
}
