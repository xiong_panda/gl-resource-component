package com.gl.biz.city.out.dto;

import java.util.Date;

/**
 * 	协作关系码值获取状态
 * @author Administrator
 *
 */
public class SysCodeOutDto {

	
	/**
	 * 	协作类型
	 */
	private String typeCode;

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	/** 字典主键ID **/
	private String fPkid ;

	/** 字典值 **/
	private String valCode ;

	/** 字典值说明 **/
	private String valName ;

	/** 备注 **/
	private String remark ;

	/** 删除标记（1：已删除，0：正常） **/
	private String fIsdelete ;

	/** 启用状态：1：禁用，0：启用 **/
	private String fEnable ;

	/** 创建人 **/
	private String fInputId ;

	/** 创建时间 **/
	private Date fInputTime ;

	/** 更新人 **/
	private String fEndId ;

	/** 更新时间 **/
	private Date fEndTime ;

	public String getValCode() {
		return valCode;
	}

	public void setValCode(String valCode) {
		this.valCode = valCode;
	}

	public String getValName() {
		return valName;
	}

	public void setValName(String valName) {
		this.valName = valName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfInputId() {
		return fInputId;
	}

	public void setfInputId(String fInputId) {
		this.fInputId = fInputId;
	}

	public Date getfInputTime() {
		return fInputTime;
	}

	public void setfInputTime(Date fInputTime) {
		this.fInputTime = fInputTime;
	}

	public String getfEndId() {
		return fEndId;
	}

	public void setfEndId(String fEndId) {
		this.fEndId = fEndId;
	}

	public Date getfEndTime() {
		return fEndTime;
	}

	public void setfEndTime(Date fEndTime) {
		this.fEndTime = fEndTime;
	}
	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}
}
