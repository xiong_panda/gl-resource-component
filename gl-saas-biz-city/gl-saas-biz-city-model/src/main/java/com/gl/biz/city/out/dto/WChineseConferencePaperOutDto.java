package com.gl.biz.city.out.dto;

import com.gl.biz.city.anno.FieldOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文会议论文
 *
 * @author code_generator
 */
@Data
public class WChineseConferencePaperOutDto implements java.io.Serializable {

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 1)
    private String fulltext;

    /**
     * 主题全文检索(标题、关键词、摘要)
     **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    @FieldOrder(order = 2)
    private String topic;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 3)
    private String wfId;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 4)
    private String orderid;

    /**
     * DOI
     **/
    @ApiModelProperty(value = "DOI")
    @FieldOrder(order = 5)
    private String doi;

    /**
     * w_ID
     **/
    @ApiModelProperty(value = "w_ID")
    @FieldOrder(order = 6)
    private String wid;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 7)
    private String nid;

    /**
     * 论文题名
     **/
    @ApiModelProperty(value = "论文题名")
    @FieldOrder(order = 8)
    private String chineseTitle;

    /**
     * 英文论文题名
     **/
    @ApiModelProperty(value = "英文论文题名")
    @FieldOrder(order = 9)
    private String englishTitle;

    /**
     * 论文题名:英文论文题名
     **/
    @ApiModelProperty(value = "论文题名:英文论文题名")
    @FieldOrder(order = 10)
    private String cETitle;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 11)
    private String authorId;

    /**
     * 作者姓名
     **/
    @ApiModelProperty(value = "作者姓名")
    @FieldOrder(order = 12)
    private String author;

    /**
     * 作者译名
     **/
    @ApiModelProperty(value = "作者译名")
    @FieldOrder(order = 13)
    private String authorTranslatio;

    /**
     * 第一作者
     **/
    @ApiModelProperty(value = "第一作者")
    @FieldOrder(order = 14)
    private String firstAuthorId;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 15)
    private String fau;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 16)
    private String faue;

    /**
     * 作者姓名:作者译名
     **/
    @ApiModelProperty(value = "作者姓名:作者译名")
    @FieldOrder(order = 17)
    private String tNAuthor;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 18)
    private String authorName;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 19)
    private String authorName2;

    /**
     * 作者单位名称
     **/
    @ApiModelProperty(value = "作者单位名称")
    @FieldOrder(order = 20)
    private String authorUnitName;

    /**
     * 作者单位规范名称
     **/
    @ApiModelProperty(value = "作者单位规范名称")
    @FieldOrder(order = 21)
    private String authorUnitSpecificationName;

    /**
     * 作者单位译名
     **/
    @ApiModelProperty(value = "作者单位译名")
    @FieldOrder(order = 22)
    private String authorUnitTranslation;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 23)
    private String forg;

    /**
     * 作者单位名称
     **/
    @ApiModelProperty(value = "作者单位名称")
    @FieldOrder(order = 24)
    private String forgUnitName;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 25)
    private String forge;

    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    @FieldOrder(order = 26)
    private String orgName;

    /**
     * 机构名称(全文检索)
     **/
    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 27)
    private String orgNameSearch;

    /**
     * 作者单位名称:作者单位规范名称:作者单位译名
     **/
    @ApiModelProperty(value = "作者单位名称:作者单位规范名称:作者单位译名")
    @FieldOrder(order = 28)
    private String uSAuthorAnyname;

    /**
     * C_ID
     **/
    @ApiModelProperty(value = "C_ID")
    @FieldOrder(order = 29)
    private String cid;

    /**
     * 机标分类号
     **/
    @ApiModelProperty(value = "机标分类号")
    @FieldOrder(order = 30)
    private String machineLabelClassCode;

    /**
     * 中图分类号
     **/
    @ApiModelProperty(value = "中图分类号")
    @FieldOrder(order = 31)
    private String clsCode;

    /**
     * 中图分类二级
     **/
    @ApiModelProperty(value = "中图分类二级")
    @FieldOrder(order = 32)
    private String clsLevel2;

    /**
     * 中图分类顶级
     **/
    @ApiModelProperty(value = "中图分类顶级")
    @FieldOrder(order = 33)
    private String clsTop;

    /**
     * 中图分类(三级)
     **/
    @ApiModelProperty(value = "中图分类(三级)")
    @FieldOrder(order = 34)
    private String clsLevel3;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 35)
    private String iid;

    /**
     * 学科分类:学科分类机标
     **/
    @ApiModelProperty(value = "学科分类:学科分类机标")
    @FieldOrder(order = 36)
    private String disciplineClassCode;

    /**
     * 协会级别
     **/
    @ApiModelProperty(value = "协会级别")
    @FieldOrder(order = 37)
    private String associationLevel;

    /**
     * 中文关键词
     **/
    @ApiModelProperty(value = "中文关键词")
    @FieldOrder(order = 38)
    private String chineseKeyword;

    /**
     * 英文关键词
     **/
    @ApiModelProperty(value = "英文关键词")
    @FieldOrder(order = 39)
    private String englishKeyword;

    /**
     * 机标关键词
     **/
    @ApiModelProperty(value = "机标关键词")
    @FieldOrder(order = 40)
    private String machineKeyword;

    /**
     * 中文关键词:英文关键词:机标关键词
     **/
    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
    @FieldOrder(order = 41)
    private String cEKeyword;

    /**
     * 中文摘要
     **/
    @ApiModelProperty(value = "中文摘要")
    @FieldOrder(order = 42)
    private String chineseAbstract;

    /**
     * 英文摘要
     **/
    @ApiModelProperty(value = "英文摘要")
    @FieldOrder(order = 43)
    private String englishAbstract;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 44)
    private String fAbstract;

    /**
     * 中文摘要:英文摘要
     **/
    @ApiModelProperty(value = "中文摘要:英文摘要")
    @FieldOrder(order = 45)
    private String cEAbstract;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 46)
    private String lan;

    /**
     * 母体文献
     **/
    @ApiModelProperty(value = "母体文献")
    @FieldOrder(order = 47)
    private String parentLiterature;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 48)
    private String parentLiteratureName;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 49)
    private String parentLiteratureName2;

    /**
     * 学会名称
     **/
    @ApiModelProperty(value = "学会名称")
    @FieldOrder(order = 50)
    private String instituteName;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 51)
    private String instituteNameAnyname;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 52)
    private String snAnyname;

    /**
     * 会议名称
     **/
    @ApiModelProperty(value = "会议名称")
    @FieldOrder(order = 53)
    private String conferenceTitle;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 54)
    private String conferenceTitleAnyname;

    /**
     * 会议届次
     **/
    @ApiModelProperty(value = "会议届次")
    @FieldOrder(order = 55)
    private String conferenceSessions;

    /**
     * 会议地点
     **/
    @ApiModelProperty(value = "会议地点")
    @FieldOrder(order = 56)
    private String conferenceMeetingPlace;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 57)
    private String hid;

    /**
     * 主办单位
     **/
    @ApiModelProperty(value = "主办单位")
    @FieldOrder(order = 58)
    private String organizer;

    /**
     * 基金
     **/
    @ApiModelProperty(value = "基金")
    @FieldOrder(order = 59)
    private String fund;

    /**
     * 基金全文检索
     **/
    @ApiModelProperty(value = "基金全文检索")
    @FieldOrder(order = 60)
    private String fundFulltextSearch;

    /**
     * 基金
     **/
    @ApiModelProperty(value = "基金")
    @FieldOrder(order = 61)
    private String fFund;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 62)
    private String fpn;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 63)
    private String fundsupport;

    /**
     * 会议时间
     **/
    @ApiModelProperty(value = "会议时间")
    @FieldOrder(order = 64)
    private String startMeetingDate;

    /**
     * 会议时间
     **/
    @ApiModelProperty(value = "会议时间")
    @FieldOrder(order = 65)
    private String endMeetingDate;

    /**
     * 出版时间
     **/
    @ApiModelProperty(value = "出版时间")
    @FieldOrder(order = 66)
    private String startPublishedDate;

    /**
     * 出版时间
     **/
    @ApiModelProperty(value = "出版时间")
    @FieldOrder(order = 67)
    private String endPublishedDate;

    /**
     * 数据来源
     **/
    @ApiModelProperty(value = "数据来源")
    @FieldOrder(order = 68)
    private String dataSource;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 69)
    private String orgnum;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 70)
    private String rn;

    /**
     * 机构层级ID
     **/
    @ApiModelProperty(value = "机构层级ID")
    @FieldOrder(order = 71)
    private String orgHierarchyId;

    /**
     * 作者单位ID
     **/
    @ApiModelProperty(value = "作者单位ID")
    @FieldOrder(order = 72)
    private String authorUnitId;

    /**
     * 机构所在省
     **/
    @ApiModelProperty(value = "机构所在省")
    @FieldOrder(order = 73)
    private String orgProvince;

    /**
     * 机构所在市
     **/
    @ApiModelProperty(value = "机构所在市")
    @FieldOrder(order = 74)
    private String orgCity;

    /**
     * 机构类型
     **/
    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 75)
    private String orgType;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 76)
    private String forgid;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 77)
    private String endorgtype;

    /**
     * 第一机构终级机构ID
     **/
    @ApiModelProperty(value = "第一机构终级机构ID")
    @FieldOrder(order = 78)
    private String orgFristFinalId;

    /**
     * 第一机构层级机构ID
     **/
    @ApiModelProperty(value = "第一机构层级机构ID")
    @FieldOrder(order = 79)
    private String orgFristHierarchyId;

    /**
     * 第一机构所在市
     **/
    @ApiModelProperty(value = "第一机构所在市")
    @FieldOrder(order = 80)
    private String orgFristCity;

    /**
     * 第一机构终级机构所在市
     **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
    @FieldOrder(order = 81)
    private String orgFristFinalCity;

    /**
     * 第一机构所在省
     **/
    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 82)
    private String orgFristProvince;

    /**
     * 第一机构所在省
     **/
    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 83)
    private String orgFristProvince2;

    /**
     * 第一机构终级机构类型
     **/
    @ApiModelProperty(value = "第一机构终级机构类型")
    @FieldOrder(order = 84)
    private String orgFristFinalType;

    /**
     * 第一机构类型
     **/
    @ApiModelProperty(value = "第一机构类型")
    @FieldOrder(order = 85)
    private String orgFristType;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 86)
    private String quarter;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 87)
    private String halfyear;

    /**
     * 文献权重
     **/
    @ApiModelProperty(value = "文献权重")
    @FieldOrder(order = 88)
    private String literatureWeight;

    /**
     * 索引管理字段(无用)
     **/
    @ApiModelProperty(value = "索引管理字段(无用)")
    @FieldOrder(order = 89)
    private String errorCode;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 90)
    private String authorInfo;

    /**
     * 作者信息.姓名
     **/
    @ApiModelProperty(value = "作者信息.姓名")
    @FieldOrder(order = 91)
    private String authorInfoName;

    /**
     * 作者信息.作者次序
     **/
    @ApiModelProperty(value = "作者信息.作者次序")
    @FieldOrder(order = 92)
    private String authorInfoOrder;

    /**
     * 作者信息.工作单位
     **/
    @ApiModelProperty(value = "作者信息.工作单位")
    @FieldOrder(order = 93)
    private String authorInfoUnit;

    /**
     * 作者信息.工作单位一级机构
     **/
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
    @FieldOrder(order = 94)
    private String authorInfoUnitOrgLevel1;

    /**
     * 作者信息.工作单位二级机构
     **/
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
    @FieldOrder(order = 95)
    private String authorInfoUnitOrgLevel2;

    /**
     * 作者信息.工作单位类型
     **/
    @ApiModelProperty(value = "作者信息.工作单位类型")
    @FieldOrder(order = 96)
    private String authorInfoUnitType;

    /**
     * 作者信息.工作单位所在省
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在省")
    @FieldOrder(order = 97)
    private String authorInfoUnitProvince;

    /**
     * 作者信息.工作单位所在市
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在市")
    @FieldOrder(order = 98)
    private String authorInfoUnitCity;

    /**
     * 作者信息.工作单位所在县
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在县")
    @FieldOrder(order = 99)
    private String authorInfoUnitCounty;

    /**
     * 作者信息.唯一ID
     **/
    @ApiModelProperty(value = "作者信息.唯一ID")
    @FieldOrder(order = 100)
    private String authorInfoId;

    /**
     * 作者信息.工作单位唯一ID
     **/
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
    @FieldOrder(order = 101)
    private String authorInfoUnitId;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 102)
    private String orgInfo;

    /**
     * 机构信息.机构名称
     **/
    @ApiModelProperty(value = "机构信息.机构名称")
    @FieldOrder(order = 103)
    private String orgInfoName;

    /**
     * 机构信息.机构次序
     **/
    @ApiModelProperty(value = "机构信息.机构次序")
    @FieldOrder(order = 104)
    private String orgInfoOrder;

    /**
     * 机构信息.机构类型
     **/
    @ApiModelProperty(value = "机构信息.机构类型")
    @FieldOrder(order = 105)
    private String orgInfoType;

    /**
     * 机构信息.省
     **/
    @ApiModelProperty(value = "机构信息.省")
    @FieldOrder(order = 106)
    private String orgInfoProvince;

    /**
     * 机构信息.市
     **/
    @ApiModelProperty(value = "机构信息.市")
    @FieldOrder(order = 107)
    private String orgInfoCity;

    /**
     * 机构信息.县
     **/
    @ApiModelProperty(value = "机构信息.县")
    @FieldOrder(order = 108)
    private String orgInfoCounty;

    /**
     * 机构信息.五级机构层级码
     **/
    @ApiModelProperty(value = "机构信息.五级机构层级码")
    @FieldOrder(order = 109)
    private String orgInfoHierarchy;

    /**
     * 机构信息.1级机构名称
     **/
    @ApiModelProperty(value = "机构信息.1级机构名称")
    @FieldOrder(order = 110)
    private String orgInfoLevel1;

    /**
     * 机构信息.2级机构名称
     **/
    @ApiModelProperty(value = "机构信息.2级机构名称")
    @FieldOrder(order = 111)
    private String orgInfoLevel2;

    /**
     * 机构信息.3级机构名称
     **/
    @ApiModelProperty(value = "机构信息.3级机构名称")
    @FieldOrder(order = 112)
    private String orgInfoLevel3;

    /**
     * 机构信息.4级机构名称
     **/
    @ApiModelProperty(value = "机构信息.4级机构名称")
    @FieldOrder(order = 113)
    private String orgInfoLevel4;

    /**
     * 机构信息.5级机构名称
     **/
    @ApiModelProperty(value = "机构信息.5级机构名称")
    @FieldOrder(order = 114)
    private String orgInfoLevel5;


}
