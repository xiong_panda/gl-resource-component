package com.gl.biz.city.out.dto;

import lombok.Data;

import java.util.Date;

/**
 * 用户使用统计
 * 接收访问的服务 和 使用的数据
 */
@Data
public class HxWfServerSearchOutDto implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/** 主键id **/
	private String id ;

	/** 用户名称 **/
	private String userName ;

	/** 用户性别 **/
	private String userSex ;

	/** 用户领域 **/
	private String userField ;

	/** 用户行业 **/
	private String userIndustry ;

	/** 用户类型 **/
	private String userType ;

	/** 用户地区 **/
	private String userArea ;

	/** 平台用户主键标识 **/
	private String userId ;

	/** 搜索关键字 **/
	private String searchKeyword ;

	/** 服务信息 **/
	private String serviceName ;

	/** 资源来源 **/
	private String serviceOrigin ;

	/** 资源名称 **/
	private String sourcesName ;

	/** 资源来源 **/
	private String sourcesOrigin ;

	/** 资源标识 **/
	private String sourcesId ;

	/** 来源为服务、资源标识(0服务，1资源) **/
	private String flag ;

	/** 使用时间 **/
	private Date useTime ;

	/** 采集来源（哈长、万方等城市群） **/
	private String collectSource ;

	/** 采集时间 **/
	private Date collectTime ;

	/** 采集方式（0：调度平台采集、1：实时接口采集） **/
	private String collectType ;
}
