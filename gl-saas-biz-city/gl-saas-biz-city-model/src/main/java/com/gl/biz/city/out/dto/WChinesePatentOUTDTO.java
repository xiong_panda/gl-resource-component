package com.gl.biz.city.out.dto;

import com.gl.biz.city.anno.FieldOrder;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中国专利
 *
 * @author code_generator
 */

public class WChinesePatentOUTDTO implements java.io.Serializable {

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    @FieldOrder(order = 1)
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    @FieldOrder(order = 1000)
    private String id;

    @ApiModelProperty(value = "万方id")
    @FieldOrder(order = 2)
    private String wfId;

    public String getWfId() {
        return wfId;
    }

    public void setWfId(String wfId) {
        this.wfId = wfId;
    }

    /**
     * 全文索引
     **/
    @ApiModelProperty(value = "全文索引")
    @FieldOrder(order = 3)
    private String fulltext;

    /**
     * 主题全文检索(标题、关键词、摘要)
     **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    @FieldOrder(order = 4)
    private String fullName;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    @FieldOrder(order = 5)
    private String Abstract;

    /**
     * 专利代理机构
     **/
    @ApiModelProperty(value = "专利代理机构")
    @FieldOrder(order = 6)
    private String orgAgency;

    /**
     * 代理人
     **/
    @ApiModelProperty(value = "代理人")
    @FieldOrder(order = 7)
    private String agent;

    /**
     * 公开（公告）号
     **/
    @ApiModelProperty(value = "公开（公告）号")
    @FieldOrder(order = 8)
    private String publicationNo;

    /**
     * 公开（公告）日
     **/
    @ApiModelProperty(value = "公开（公告）日")
    @FieldOrder(order = 9)
    private Date publicationDate;

    /**
     * 发明（设计）人
     **/
    @ApiModelProperty(value = "发明（设计）人")
    @FieldOrder(order = 10)
    private String author;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 11)
    private String fAuthor;

    /**
     * 发明（设计）人
     **/
    @ApiModelProperty(value = "发明（设计）人")
    @FieldOrder(order = 12)
    private String author2;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 13)
    private String authorName;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    @FieldOrder(order = 14)
    private String authorName2;

    /**
     * 范畴分类
     **/
    @ApiModelProperty(value = "范畴分类")
    @FieldOrder(order = 15)
    private String cate;

    /**
     * 光盘号
     **/
    @ApiModelProperty(value = "光盘号")
    @FieldOrder(order = 16)
    private String cdId;

    /**
     * 颁证日
     **/
    @ApiModelProperty(value = "颁证日")
    @FieldOrder(order = 17)
    private Date createDate;

    /**
     * 分类号
     **/
    @ApiModelProperty(value = "分类号")
    @FieldOrder(order = 18)
    private String classCode;

    /**
     * 国省代码
     **/
    @ApiModelProperty(value = "国省代码")
    @FieldOrder(order = 19)
    private String countryCode;

    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    @FieldOrder(order = 20)
    private Date date;

    /**
     * 数据库标识
     **/
    @ApiModelProperty(value = "数据库标识")
    @FieldOrder(order = 21)
    private String databaseIdentity;

    /**
     * 进入国家日期
     **/
    @ApiModelProperty(value = "进入国家日期")
    @FieldOrder(order = 22)
    private Date dec;

    /**
     * 学科分类
     **/
    @ApiModelProperty(value = "学科分类")
    @FieldOrder(order = 23)
    private String disciplineClass;

    /**
     * 主权项
     **/
    @ApiModelProperty(value = "主权项")
    @FieldOrder(order = 24)
    private String indCla;

    /**
     * 法律状态F_LawStatus
     **/
    @ApiModelProperty(value = "法律状态F_LawStatus")
    @FieldOrder(order = 25)
    private String lawStatus;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 26)
    private String ls;

    /**
     * 规范单位名称
     **/
    @ApiModelProperty(value = "规范单位名称")
    @FieldOrder(order = 27)
    private String orgNormName;

    /**
     * 国际申请
     **/
    @ApiModelProperty(value = "国际申请")
    @FieldOrder(order = 28)
    private String intApp;

    /**
     * 国际公布
     **/
    @ApiModelProperty(value = "国际公布")
    @FieldOrder(order = 29)
    private String intPush;

    /**
     * 失效专利
     **/
    @ApiModelProperty(value = "失效专利")
    @FieldOrder(order = 30)
    private String invPat;

    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    @FieldOrder(order = 31)
    private String keyword;

    /**
     * 主分类号
     **/
    @ApiModelProperty(value = "主分类号")
    @FieldOrder(order = 32)
    private String mainClassCode;

    /**
     * 中图分类
     **/
    @ApiModelProperty(value = "中图分类")
    @FieldOrder(order = 33)
    private String cls;

    /**
     * 中图分类(二级)
     **/
    @ApiModelProperty(value = "中图分类(二级)")
    @FieldOrder(order = 34)
    private String clsLevel2;

    /**
     * 分案原申请号
     **/
    @ApiModelProperty(value = "分案原申请号")
    @FieldOrder(order = 35)
    private String originRequestNumber;

    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    @FieldOrder(order = 36)
    private String org;

    /**
     * 机构名称(全文检索)
     **/
    @ApiModelProperty(value = "机构名称(全文检索)")
    @FieldOrder(order = 37)
    private String orgNameSearch;

    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    @FieldOrder(order = 38)
    private String orgAnyname2Name;

    /**
     * 规范单位名称
     **/
    @ApiModelProperty(value = "规范单位名称")
    @FieldOrder(order = 39)
    private String orgNormName2;

    /**
     * 机构类型
     **/
    @ApiModelProperty(value = "机构类型")
    @FieldOrder(order = 40)
    private String orgType;

    /**
     * 专利号
     **/
    @ApiModelProperty(value = "专利号")
    @FieldOrder(order = 41)
    private String patentnumberName;

    /**
     * 专利类型
     **/
    @ApiModelProperty(value = "专利类型")
    @FieldOrder(order = 42)
    private String patenttypeName;

    /**
     * 页数
     **/
    @ApiModelProperty(value = "页数")
    @FieldOrder(order = 43)
    private String pages;

    /**
     * 优先权
     **/
    @ApiModelProperty(value = "优先权")
    @FieldOrder(order = 44)
    private String priority;

    /**
     * 发布路径
     **/
    @ApiModelProperty(value = "发布路径")
    @FieldOrder(order = 45)
    private String pubPath;

    /**
     * 参考文献
     **/
    @ApiModelProperty(value = "参考文献")
    @FieldOrder(order = 46)
    private String reference;

    /**
     * 地址
     **/
    @ApiModelProperty(value = "地址")
    @FieldOrder(order = 47)
    private String requestAddress;

    /**
     * 申请号
     **/
    @ApiModelProperty(value = "申请号")
    @FieldOrder(order = 48)
    private String requestNumber;

    /**
     * 申请号wjy
     **/
    @ApiModelProperty(value = "申请号wjy")
    @FieldOrder(order = 49)
    private String requestNo;

    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    @FieldOrder(order = 50)
    private String requestPeople;

    /**
     * 审查员
     **/
    @ApiModelProperty(value = "审查员")
    @FieldOrder(order = 51)
    private String reviewer;

    /**
     * 服务器地址
     **/
    @ApiModelProperty(value = "服务器地址")
    @FieldOrder(order = 52)
    private String serverAddress;

    /**
     * 数据来源
     **/
    @ApiModelProperty(value = "数据来源")
    @FieldOrder(order = 53)
    private String dataSource;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    @FieldOrder(order = 54)
    private String title1;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    @FieldOrder(order = 55)
    private String title2;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 56)
    private Date yannodate;

    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    @FieldOrder(order = 57)
    private String date2;

    /**
     * 主分类号
     **/
    @ApiModelProperty(value = "主分类号")
    @FieldOrder(order = 58)
    private String mainClassCode2;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 59)
    private String smcls;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 60)
    private String tmcls;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 61)
    private String ckey;

    /**
     * 机构ID
     **/
    @ApiModelProperty(value = "机构ID")
    @FieldOrder(order = 62)
    private String orgId;

    /**
     * 机构层级ID
     **/
    @ApiModelProperty(value = "机构层级ID")
    @FieldOrder(order = 63)
    private String orgHierarchyId;

    /**
     * 机构所在省
     **/
    @ApiModelProperty(value = "机构所在省")
    @FieldOrder(order = 64)
    private String orgProvince;

    /**
     * 机构所在市
     **/
    @ApiModelProperty(value = "机构所在市")
    @FieldOrder(order = 65)
    private String orgCity;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 66)
    private String endOrgType;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 67)
    private String fOrgId;

    /**
     * 第一机构终级机构ID
     **/
    @ApiModelProperty(value = "第一机构终级机构ID")
    @FieldOrder(order = 68)
    private String orgFristFinalId;

    /**
     * 第一机构层级机构ID
     **/
    @ApiModelProperty(value = "第一机构层级机构ID")
    @FieldOrder(order = 69)
    private String orgFristHierarchyId;

    /**
     * 第一机构所在省
     **/
    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 70)
    private String orgFristProvince;

    /**
     * 第一机构所在省
     **/
    @ApiModelProperty(value = "第一机构所在省")
    @FieldOrder(order = 71)
    private String orgFristProvince2;

    /**
     * 第一机构终级机构所在市
     **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
    @FieldOrder(order = 72)
    private String orgFristFinalCity;

    /**
     * 第一机构所在市
     **/
    @ApiModelProperty(value = "第一机构所在市")
    @FieldOrder(order = 73)
    private String orgFristCity;

    /**
     * 第一机构终级机构类型
     **/
    @ApiModelProperty(value = "第一机构终级机构类型")
    @FieldOrder(order = 74)
    private String orgFristFinalType;

    /**
     * 第一机构类型
     **/
    @ApiModelProperty(value = "第一机构类型")
    @FieldOrder(order = 75)
    private String orgFristType;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 76)
    private String authorId;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 77)
    private String orgNum;

    /**
     * 文献权重
     **/
    @ApiModelProperty(value = "文献权重")
    @FieldOrder(order = 78)
    private String literatureWeight;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 79)
    private String quarter;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 80)
    private String halfyear;

    /**
     * 索引管理字段(无用)
     **/
    @ApiModelProperty(value = "索引管理字段(无用)")
    @FieldOrder(order = 81)
    private String errorCode;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 82)
    private String authorInfo;

    /**
     * 作者信息.姓名
     **/
    @ApiModelProperty(value = "作者信息.姓名")
    @FieldOrder(order = 83)
    private String authorInfoName;

    /**
     * 作者信息.作者次序
     **/
    @ApiModelProperty(value = "作者信息.作者次序")
    @FieldOrder(order = 84)
    private String authorInfoOrder;

    /**
     * 作者信息.工作单位
     **/
    @ApiModelProperty(value = "作者信息.工作单位")
    @FieldOrder(order = 85)
    private String authorInfoUnit;

    /**
     * 作者信息.工作单位一级机构
     **/
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
    @FieldOrder(order = 86)
    private String authorInfoUnitOrgLevel1;

    /**
     * 作者信息.工作单位二级机构
     **/
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
    @FieldOrder(order = 87)
    private String authorInfoUnitOrgLevel2;

    /**
     * 作者信息.工作单位类型
     **/
    @ApiModelProperty(value = "作者信息.工作单位类型")
    @FieldOrder(order = 88)
    private String authorInfoUnitType;

    /**
     * 作者信息.工作单位所在省
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在省")
    @FieldOrder(order = 89)
    private String authorInfoUnitProvince;

    /**
     * 作者信息.工作单位所在市
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在市")
    @FieldOrder(order = 90)
    private String authorInfoUnitCity;

    /**
     * 作者信息.工作单位所在县
     **/
    @ApiModelProperty(value = "作者信息.工作单位所在县")
    @FieldOrder(order = 91)
    private String authorInfoUnitCounty;

    /**
     * 作者信息.唯一ID
     **/
    @ApiModelProperty(value = "作者信息.唯一ID")
    @FieldOrder(order = 92)
    private String authorInfoId;

    /**
     * 作者信息.工作单位唯一ID
     **/
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
    @FieldOrder(order = 93)
    private String authorInfoUnitId;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    @FieldOrder(order = 94)
    private String orgInfo;

    /**
     * 机构信息.机构名称
     **/
    @ApiModelProperty(value = "机构信息.机构名称")
    @FieldOrder(order = 95)
    private String orgInfoName;

    /**
     * 机构信息.机构次序
     **/
    @ApiModelProperty(value = "机构信息.机构次序")
    @FieldOrder(order = 96)
    private String orgInfoOrder;

    /**
     * 机构信息.机构类型
     **/
    @ApiModelProperty(value = "机构信息.机构类型")
    @FieldOrder(order = 97)
    private String orgInfoType;

    /**
     * 机构信息.省
     **/
    @ApiModelProperty(value = "机构信息.省")
    @FieldOrder(order = 98)
    private String orgInfoProvince;

    /**
     * 机构信息.市
     **/
    @ApiModelProperty(value = "机构信息.市")
    @FieldOrder(order = 99)
    private String orgInfoCity;

    /**
     * 机构信息.县
     **/
    @ApiModelProperty(value = "机构信息.县")
    @FieldOrder(order = 100)
    private String orgInfoCounty;

    /**
     * 机构信息.五级机构层级码
     **/
    @ApiModelProperty(value = "机构信息.五级机构层级码")
    @FieldOrder(order = 101)
    private String orgInfoHierarchy;

    /**
     * 机构信息.1级机构名称
     **/
    @ApiModelProperty(value = "机构信息.1级机构名称")
    @FieldOrder(order = 102)
    private String orgInfoLevel1;

    /**
     * 机构信息.2级机构名称
     **/
    @ApiModelProperty(value = "机构信息.2级机构名称")
    @FieldOrder(order = 103)
    private String orgInfoLevel2;

    /**
     * 机构信息.3级机构名称
     **/
    @ApiModelProperty(value = "机构信息.3级机构名称")
    @FieldOrder(order = 104)
    private String orgInfoLevel3;

    /**
     * 机构信息.4级机构名称
     **/
    @ApiModelProperty(value = "机构信息.4级机构名称")
    @FieldOrder(order = 105)
    private String orgInfoLevel4;

    /**
     * 机构信息.5级机构名称
     **/
    @ApiModelProperty(value = "机构信息.5级机构名称")
    @FieldOrder(order = 106)
    private String orgInfoLevel5;


    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFulltext() {
        return fulltext;
    }

    public void setFulltext(String fulltext) {
        this.fulltext = fulltext;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getOrgAgency() {
        return orgAgency;
    }

    public void setOrgAgency(String orgAgency) {
        this.orgAgency = orgAgency;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getPublicationNo() {
        return publicationNo;
    }

    public void setPublicationNo(String publicationNo) {
        this.publicationNo = publicationNo;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getfAuthor() {
        return fAuthor;
    }

    public void setfAuthor(String fAuthor) {
        this.fAuthor = fAuthor;
    }

    public String getAuthor2() {
        return author2;
    }

    public void setAuthor2(String author2) {
        this.author2 = author2;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorName2() {
        return authorName2;
    }

    public void setAuthorName2(String authorName2) {
        this.authorName2 = authorName2;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getCdId() {
        return cdId;
    }

    public void setCdId(String cdId) {
        this.cdId = cdId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDatabaseIdentity() {
        return databaseIdentity;
    }

    public void setDatabaseIdentity(String databaseIdentity) {
        this.databaseIdentity = databaseIdentity;
    }

    public Date getDec() {
        return dec;
    }

    public void setDec(Date dec) {
        this.dec = dec;
    }

    public String getDisciplineClass() {
        return disciplineClass;
    }

    public void setDisciplineClass(String disciplineClass) {
        this.disciplineClass = disciplineClass;
    }

    public String getIndCla() {
        return indCla;
    }

    public void setIndCla(String indCla) {
        this.indCla = indCla;
    }

    public String getLawStatus() {
        return lawStatus;
    }

    public void setLawStatus(String lawStatus) {
        this.lawStatus = lawStatus;
    }

    public String getLs() {
        return ls;
    }

    public void setLs(String ls) {
        this.ls = ls;
    }

    public String getOrgNormName() {
        return orgNormName;
    }

    public void setOrgNormName(String orgNormName) {
        this.orgNormName = orgNormName;
    }

    public String getIntApp() {
        return intApp;
    }

    public void setIntApp(String intApp) {
        this.intApp = intApp;
    }

    public String getIntPush() {
        return intPush;
    }

    public void setIntPush(String intPush) {
        this.intPush = intPush;
    }

    public String getInvPat() {
        return invPat;
    }

    public void setInvPat(String invPat) {
        this.invPat = invPat;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getMainClassCode() {
        return mainClassCode;
    }

    public void setMainClassCode(String mainClassCode) {
        this.mainClassCode = mainClassCode;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getClsLevel2() {
        return clsLevel2;
    }

    public void setClsLevel2(String clsLevel2) {
        this.clsLevel2 = clsLevel2;
    }

    public String getOriginRequestNumber() {
        return originRequestNumber;
    }

    public void setOriginRequestNumber(String originRequestNumber) {
        this.originRequestNumber = originRequestNumber;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getOrgNameSearch() {
        return orgNameSearch;
    }

    public void setOrgNameSearch(String orgNameSearch) {
        this.orgNameSearch = orgNameSearch;
    }

    public String getOrgAnyname2Name() {
        return orgAnyname2Name;
    }

    public void setOrgAnyname2Name(String orgAnyname2Name) {
        this.orgAnyname2Name = orgAnyname2Name;
    }

    public String getOrgNormName2() {
        return orgNormName2;
    }

    public void setOrgNormName2(String orgNormName2) {
        this.orgNormName2 = orgNormName2;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getPatentnumberName() {
        return patentnumberName;
    }

    public void setPatentnumberName(String patentnumberName) {
        this.patentnumberName = patentnumberName;
    }

    public String getPatenttypeName() {
        return patenttypeName;
    }

    public void setPatenttypeName(String patenttypeName) {
        this.patenttypeName = patenttypeName;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPubPath() {
        return pubPath;
    }

    public void setPubPath(String pubPath) {
        this.pubPath = pubPath;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRequestAddress() {
        return requestAddress;
    }

    public void setRequestAddress(String requestAddress) {
        this.requestAddress = requestAddress;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getRequestPeople() {
        return requestPeople;
    }

    public void setRequestPeople(String requestPeople) {
        this.requestPeople = requestPeople;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public Date getYannodate() {
        return yannodate;
    }

    public void setYannodate(Date yannodate) {
        this.yannodate = yannodate;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getMainClassCode2() {
        return mainClassCode2;
    }

    public void setMainClassCode2(String mainClassCode2) {
        this.mainClassCode2 = mainClassCode2;
    }

    public String getSmcls() {
        return smcls;
    }

    public void setSmcls(String smcls) {
        this.smcls = smcls;
    }

    public String getTmcls() {
        return tmcls;
    }

    public void setTmcls(String tmcls) {
        this.tmcls = tmcls;
    }

    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgHierarchyId() {
        return orgHierarchyId;
    }

    public void setOrgHierarchyId(String orgHierarchyId) {
        this.orgHierarchyId = orgHierarchyId;
    }

    public String getOrgProvince() {
        return orgProvince;
    }

    public void setOrgProvince(String orgProvince) {
        this.orgProvince = orgProvince;
    }

    public String getOrgCity() {
        return orgCity;
    }

    public void setOrgCity(String orgCity) {
        this.orgCity = orgCity;
    }

    public String getEndOrgType() {
        return endOrgType;
    }

    public void setEndOrgType(String endOrgType) {
        this.endOrgType = endOrgType;
    }

    public String getfOrgId() {
        return fOrgId;
    }

    public void setfOrgId(String fOrgId) {
        this.fOrgId = fOrgId;
    }

    public String getOrgFristFinalId() {
        return orgFristFinalId;
    }

    public void setOrgFristFinalId(String orgFristFinalId) {
        this.orgFristFinalId = orgFristFinalId;
    }

    public String getOrgFristHierarchyId() {
        return orgFristHierarchyId;
    }

    public void setOrgFristHierarchyId(String orgFristHierarchyId) {
        this.orgFristHierarchyId = orgFristHierarchyId;
    }

    public String getOrgFristProvince() {
        return orgFristProvince;
    }

    public void setOrgFristProvince(String orgFristProvince) {
        this.orgFristProvince = orgFristProvince;
    }

    public String getOrgFristProvince2() {
        return orgFristProvince2;
    }

    public void setOrgFristProvince2(String orgFristProvince2) {
        this.orgFristProvince2 = orgFristProvince2;
    }

    public String getOrgFristFinalCity() {
        return orgFristFinalCity;
    }

    public void setOrgFristFinalCity(String orgFristFinalCity) {
        this.orgFristFinalCity = orgFristFinalCity;
    }

    public String getOrgFristCity() {
        return orgFristCity;
    }

    public void setOrgFristCity(String orgFristCity) {
        this.orgFristCity = orgFristCity;
    }

    public String getOrgFristFinalType() {
        return orgFristFinalType;
    }

    public void setOrgFristFinalType(String orgFristFinalType) {
        this.orgFristFinalType = orgFristFinalType;
    }

    public String getOrgFristType() {
        return orgFristType;
    }

    public void setOrgFristType(String orgFristType) {
        this.orgFristType = orgFristType;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getOrgNum() {
        return orgNum;
    }

    public void setOrgNum(String orgNum) {
        this.orgNum = orgNum;
    }

    public String getLiteratureWeight() {
        return literatureWeight;
    }

    public void setLiteratureWeight(String literatureWeight) {
        this.literatureWeight = literatureWeight;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getHalfyear() {
        return halfyear;
    }

    public void setHalfyear(String halfyear) {
        this.halfyear = halfyear;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getAuthorInfo() {
        return authorInfo;
    }

    public void setAuthorInfo(String authorInfo) {
        this.authorInfo = authorInfo;
    }

    public String getAuthorInfoName() {
        return authorInfoName;
    }

    public void setAuthorInfoName(String authorInfoName) {
        this.authorInfoName = authorInfoName;
    }

    public String getAuthorInfoOrder() {
        return authorInfoOrder;
    }

    public void setAuthorInfoOrder(String authorInfoOrder) {
        this.authorInfoOrder = authorInfoOrder;
    }

    public String getAuthorInfoUnit() {
        return authorInfoUnit;
    }

    public void setAuthorInfoUnit(String authorInfoUnit) {
        this.authorInfoUnit = authorInfoUnit;
    }

    public String getAuthorInfoUnitOrgLevel1() {
        return authorInfoUnitOrgLevel1;
    }

    public void setAuthorInfoUnitOrgLevel1(String authorInfoUnitOrgLevel1) {
        this.authorInfoUnitOrgLevel1 = authorInfoUnitOrgLevel1;
    }

    public String getAuthorInfoUnitOrgLevel2() {
        return authorInfoUnitOrgLevel2;
    }

    public void setAuthorInfoUnitOrgLevel2(String authorInfoUnitOrgLevel2) {
        this.authorInfoUnitOrgLevel2 = authorInfoUnitOrgLevel2;
    }

    public String getAuthorInfoUnitType() {
        return authorInfoUnitType;
    }

    public void setAuthorInfoUnitType(String authorInfoUnitType) {
        this.authorInfoUnitType = authorInfoUnitType;
    }

    public String getAuthorInfoUnitProvince() {
        return authorInfoUnitProvince;
    }

    public void setAuthorInfoUnitProvince(String authorInfoUnitProvince) {
        this.authorInfoUnitProvince = authorInfoUnitProvince;
    }

    public String getAuthorInfoUnitCity() {
        return authorInfoUnitCity;
    }

    public void setAuthorInfoUnitCity(String authorInfoUnitCity) {
        this.authorInfoUnitCity = authorInfoUnitCity;
    }

    public String getAuthorInfoUnitCounty() {
        return authorInfoUnitCounty;
    }

    public void setAuthorInfoUnitCounty(String authorInfoUnitCounty) {
        this.authorInfoUnitCounty = authorInfoUnitCounty;
    }

    public String getAuthorInfoId() {
        return authorInfoId;
    }

    public void setAuthorInfoId(String authorInfoId) {
        this.authorInfoId = authorInfoId;
    }

    public String getAuthorInfoUnitId() {
        return authorInfoUnitId;
    }

    public void setAuthorInfoUnitId(String authorInfoUnitId) {
        this.authorInfoUnitId = authorInfoUnitId;
    }

    public String getOrgInfo() {
        return orgInfo;
    }

    public void setOrgInfo(String orgInfo) {
        this.orgInfo = orgInfo;
    }

    public String getOrgInfoName() {
        return orgInfoName;
    }

    public void setOrgInfoName(String orgInfoName) {
        this.orgInfoName = orgInfoName;
    }

    public String getOrgInfoOrder() {
        return orgInfoOrder;
    }

    public void setOrgInfoOrder(String orgInfoOrder) {
        this.orgInfoOrder = orgInfoOrder;
    }

    public String getOrgInfoType() {
        return orgInfoType;
    }

    public void setOrgInfoType(String orgInfoType) {
        this.orgInfoType = orgInfoType;
    }

    public String getOrgInfoProvince() {
        return orgInfoProvince;
    }

    public void setOrgInfoProvince(String orgInfoProvince) {
        this.orgInfoProvince = orgInfoProvince;
    }

    public String getOrgInfoCity() {
        return orgInfoCity;
    }

    public void setOrgInfoCity(String orgInfoCity) {
        this.orgInfoCity = orgInfoCity;
    }

    public String getOrgInfoCounty() {
        return orgInfoCounty;
    }

    public void setOrgInfoCounty(String orgInfoCounty) {
        this.orgInfoCounty = orgInfoCounty;
    }

    public String getOrgInfoHierarchy() {
        return orgInfoHierarchy;
    }

    public void setOrgInfoHierarchy(String orgInfoHierarchy) {
        this.orgInfoHierarchy = orgInfoHierarchy;
    }

    public String getOrgInfoLevel1() {
        return orgInfoLevel1;
    }

    public void setOrgInfoLevel1(String orgInfoLevel1) {
        this.orgInfoLevel1 = orgInfoLevel1;
    }

    public String getOrgInfoLevel2() {
        return orgInfoLevel2;
    }

    public void setOrgInfoLevel2(String orgInfoLevel2) {
        this.orgInfoLevel2 = orgInfoLevel2;
    }

    public String getOrgInfoLevel3() {
        return orgInfoLevel3;
    }

    public void setOrgInfoLevel3(String orgInfoLevel3) {
        this.orgInfoLevel3 = orgInfoLevel3;
    }

    public String getOrgInfoLevel4() {
        return orgInfoLevel4;
    }

    public void setOrgInfoLevel4(String orgInfoLevel4) {
        this.orgInfoLevel4 = orgInfoLevel4;
    }

    public String getOrgInfoLevel5() {
        return orgInfoLevel5;
    }

    public void setOrgInfoLevel5(String orgInfoLevel5) {
        this.orgInfoLevel5 = orgInfoLevel5;
    }
}
