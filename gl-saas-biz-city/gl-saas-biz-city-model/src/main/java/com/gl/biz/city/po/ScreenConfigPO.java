package com.gl.biz.city.po;



import java.util.Date;


public class ScreenConfigPO {
    private String id;
    private String tableName;
    private String classifyField;
    private Date createAt;
    private Date updateAt;

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getClassifyField() {
        return classifyField;
    }

    public void setClassifyField(String classifyField) {
        this.classifyField = classifyField;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
