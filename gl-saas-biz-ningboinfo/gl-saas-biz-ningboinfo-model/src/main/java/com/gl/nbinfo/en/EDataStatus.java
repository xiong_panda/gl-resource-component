package com.gl.nbinfo.en;

public enum EDataStatus {
    NEW(1, "最新"),
    OLD(2, "历史");

    public Integer code;
    public String value;

    EDataStatus(Integer code, String value) {
        this.code = code;
        this.value = value;
    }
}
