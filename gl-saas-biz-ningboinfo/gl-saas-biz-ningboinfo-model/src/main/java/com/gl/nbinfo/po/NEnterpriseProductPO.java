package com.gl.nbinfo.po;

import com.gl.nbinfo.annotation.FieldOrder;

public class NEnterpriseProductPO {

    @FieldOrder(order = 1)
    private String ID;
    @FieldOrder(order = 2)
    private String ApprovedYear;
    @FieldOrder(order = 3)
    private String CreditCode;
    @FieldOrder(order = 4)
    private String IdentificationNumber;
    @FieldOrder(order = 5)
    private String UnitName;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getApprovedYear() {
        return ApprovedYear;
    }

    public void setApprovedYear(String approvedYear) {
        ApprovedYear = approvedYear;
    }

    public String getCreditCode() {
        return CreditCode;
    }

    public void setCreditCode(String creditCode) {
        CreditCode = creditCode;
    }

    public String getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        IdentificationNumber = identificationNumber;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }
}
