package com.gl.nbinfo.po;

import com.gl.nbinfo.annotation.FieldOrder;

public class NPatentPO {
    @FieldOrder(order = 1)
    private String  ID;
    @FieldOrder(order = 2)
    private String  Address;
    @FieldOrder(order = 3)
    private String  AgencyName;
    @FieldOrder(order = 4)
    private String  ApplyDate;
    @FieldOrder(order = 5)
    private String  ApplyNumber;
    @FieldOrder(order = 6)
    private String  ClassNumber;
    @FieldOrder(order = 7)
    private String  DistrictName;
    @FieldOrder(order = 8)
    private String  ExpireDate;
    @FieldOrder(order = 9)
    private String  MainClassNumber;
    @FieldOrder(order = 10)
    private String  PatentCategory;
    @FieldOrder(order = 11)
    private String  PatentId;
    @FieldOrder(order = 12)
    private String  PatentName;
    @FieldOrder(order = 13)
    private String  PatentValue;
    @FieldOrder(order = 14)
    private String  PersonName;
    @FieldOrder(order = 15)
    private String  PublicDate;
    @FieldOrder(order = 16)
    private String  PublicNumber;
    @FieldOrder(order = 17)
    private String  StreetName;
    @FieldOrder(order = 18)
    private String  ValuationScore;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAgencyName() {
        return AgencyName;
    }

    public void setAgencyName(String agencyName) {
        AgencyName = agencyName;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getApplyNumber() {
        return ApplyNumber;
    }

    public void setApplyNumber(String applyNumber) {
        ApplyNumber = applyNumber;
    }

    public String getClassNumber() {
        return ClassNumber;
    }

    public void setClassNumber(String classNumber) {
        ClassNumber = classNumber;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public String getMainClassNumber() {
        return MainClassNumber;
    }

    public void setMainClassNumber(String mainClassNumber) {
        MainClassNumber = mainClassNumber;
    }

    public String getPatentCategory() {
        return PatentCategory;
    }

    public void setPatentCategory(String patentCategory) {
        PatentCategory = patentCategory;
    }

    public String getPatentId() {
        return PatentId;
    }

    public void setPatentId(String patentId) {
        PatentId = patentId;
    }

    public String getPatentName() {
        return PatentName;
    }

    public void setPatentName(String patentName) {
        PatentName = patentName;
    }

    public String getPatentValue() {
        return PatentValue;
    }

    public void setPatentValue(String patentValue) {
        PatentValue = patentValue;
    }

    public String getPersonName() {
        return PersonName;
    }

    public void setPersonName(String personName) {
        PersonName = personName;
    }

    public String getPublicDate() {
        return PublicDate;
    }

    public void setPublicDate(String publicDate) {
        PublicDate = publicDate;
    }

    public String getPublicNumber() {
        return PublicNumber;
    }

    public void setPublicNumber(String publicNumber) {
        PublicNumber = publicNumber;
    }

    public String getStreetName() {
        return StreetName;
    }

    public void setStreetName(String streetName) {
        StreetName = streetName;
    }

    public String getValuationScore() {
        return ValuationScore;
    }

    public void setValuationScore(String valuationScore) {
        ValuationScore = valuationScore;
    }
}
