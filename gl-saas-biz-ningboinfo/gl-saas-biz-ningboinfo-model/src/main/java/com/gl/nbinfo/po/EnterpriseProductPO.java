package com.gl.nbinfo.po;

import com.gl.nbinfo.annotation.FieldOrder;

public class EnterpriseProductPO {

    @FieldOrder(order = 1)
    private String ID;
    @FieldOrder(order = 2)
    private String ApprovedYear;
    @FieldOrder(order = 3)
    private String CreditCode;
    @FieldOrder(order = 4)
    private String IdentificationNumber;
    @FieldOrder(order = 5)
    private String CorpName;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getApprovedYear() {
        return ApprovedYear;
    }

    public void setApprovedYear(String approvedYear) {
        ApprovedYear = approvedYear;
    }

    public String getCreditCode() {
        return CreditCode;
    }

    public void setCreditCode(String creditCode) {
        CreditCode = creditCode;
    }

    public String getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        IdentificationNumber = identificationNumber;
    }

    public String getCorpName() {
        return CorpName;
    }

    public void setCorpName(String corpName) {
        CorpName = corpName;
    }
}
