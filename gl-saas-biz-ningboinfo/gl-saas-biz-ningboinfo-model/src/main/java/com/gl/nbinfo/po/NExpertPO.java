package com.gl.nbinfo.po;


import com.gl.nbinfo.annotation.FieldOrder;

public class NExpertPO {

@FieldOrder(order = 1)
private String ID;
@FieldOrder(order = 2)
private String Address;
@FieldOrder(order = 3)
private String BaseInfo;
@FieldOrder(order = 4)
private String Book;
@FieldOrder(order = 5)
private String Class;
@FieldOrder(order = 6)
private String Class_1;
@FieldOrder(order = 7)
private String Focus;
@FieldOrder(order = 8)
private String Honor;
@FieldOrder(order = 9)
private String Id;
@FieldOrder(order = 10)
private String PersonName;
@FieldOrder(order = 11)
private String ProfessionName;
@FieldOrder(order = 12)
private String ResearchDirection;
@FieldOrder(order = 13)
private String ResearchField;
@FieldOrder(order = 14)
private String Resume;
@FieldOrder(order = 15)
private String Summary;
@FieldOrder(order = 16)
private String UnitName;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBaseInfo() {
        return BaseInfo;
    }

    public void setBaseInfo(String baseInfo) {
        BaseInfo = baseInfo;
    }

    public String getBook() {
        return Book;
    }

    public void setBook(String book) {
        Book = book;
    }

    public String getClassInfo() {
        return Class;
    }

    public void setClass(String aClass) {
        Class = aClass;
    }

    public String getClass_1() {
        return Class_1;
    }

    public void setClass_1(String class_1) {
        Class_1 = class_1;
    }

    public String getFocus() {
        return Focus;
    }

    public void setFocus(String focus) {
        Focus = focus;
    }

    public String getHonor() {
        return Honor;
    }

    public void setHonor(String honor) {
        Honor = honor;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPersonName() {
        return PersonName;
    }

    public void setPersonName(String personName) {
        PersonName = personName;
    }

    public String getProfessionName() {
        return ProfessionName;
    }

    public void setProfessionName(String professionName) {
        ProfessionName = professionName;
    }

    public String getResearchDirection() {
        return ResearchDirection;
    }

    public void setResearchDirection(String researchDirection) {
        ResearchDirection = researchDirection;
    }

    public String getResearchField() {
        return ResearchField;
    }

    public void setResearchField(String researchField) {
        ResearchField = researchField;
    }

    public String getResume() {
        return Resume;
    }

    public void setResume(String resume) {
        Resume = resume;
    }

    public String getSummary() {
        return Summary;
    }

    public void setSummary(String summary) {
        Summary = summary;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }
}
