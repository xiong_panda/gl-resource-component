package com.gl.nbinfo.po;

import com.gl.nbinfo.annotation.FieldOrder;

public class PatentPO {

    @FieldOrder(order = 1)   private String  ID;
    @FieldOrder(order = 2)   private String  Address;
    @FieldOrder(order = 3)   private String  AgencyName;
    @FieldOrder(order = 4)   private String  ApplyDate;
    @FieldOrder(order = 5)   private String  ApplyCode;
    @FieldOrder(order = 6)   private String  ClassCode;
    @FieldOrder(order = 7)   private String  DistrictName;
    @FieldOrder(order = 8)   private String  EndDate;
    @FieldOrder(order = 9)   private String  MainClassCode;
    @FieldOrder(order = 10)  private String  PatentCategory;
    @FieldOrder(order = 11)  private String  PatentId;
    @FieldOrder(order = 12)  private String  TITLE;
    @FieldOrder(order = 13)  private String  Abstract;
    @FieldOrder(order = 14)  private String  PersonName;
    @FieldOrder(order = 15)  private String  PublicDate;
    @FieldOrder(order = 16)  private String  PublicCode;
    @FieldOrder(order = 17)  private String  StreetName;
    @FieldOrder(order = 18)  private String  ValuationScore;
    @FieldOrder(order = 2000)
    private String resourceFrom;
    @FieldOrder(order = 2001)
    private String resourceLogo;

    public String getResourceFrom() {
        return resourceFrom;
    }

    public void setResourceFrom(String resourceFrom) {
        this.resourceFrom = resourceFrom;
    }

    public String getResourceLogo() {
        return resourceLogo;
    }

    public void setResourceLogo(String resourceLogo) {
        this.resourceLogo = resourceLogo;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAgencyName() {
        return AgencyName;
    }

    public void setAgencyName(String agencyName) {
        AgencyName = agencyName;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getApplyCode() {
        return ApplyCode;
    }

    public void setApplyCode(String applyCode) {
        ApplyCode = applyCode;
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setClassCode(String classCode) {
        ClassCode = classCode;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getMainClassCode() {
        return MainClassCode;
    }

    public void setMainClassCode(String mainClassCode) {
        MainClassCode = mainClassCode;
    }

    public String getPatentCategory() {
        return PatentCategory;
    }

    public void setPatentCategory(String patentCategory) {
        PatentCategory = patentCategory;
    }

    public String getPatentId() {
        return PatentId;
    }

    public void setPatentId(String patentId) {
        PatentId = patentId;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getPersonName() {
        return PersonName;
    }

    public void setPersonName(String personName) {
        PersonName = personName;
    }

    public String getPublicDate() {
        return PublicDate;
    }

    public void setPublicDate(String publicDate) {
        PublicDate = publicDate;
    }

    public String getPublicCode() {
        return PublicCode;
    }

    public void setPublicCode(String publicCode) {
        PublicCode = publicCode;
    }

    public String getStreetName() {
        return StreetName;
    }

    public void setStreetName(String streetName) {
        StreetName = streetName;
    }

    public String getValuationScore() {
        return ValuationScore;
    }

    public void setValuationScore(String valuationScore) {
        ValuationScore = valuationScore;
    }
}
