package com.gl.nbinfo.po;

import com.gl.nbinfo.annotation.FieldOrder;

public class NPatentServicePO {
    @FieldOrder(order = 1)
    private String  ID;
    @FieldOrder(order = 2)
    private String  Address;
    @FieldOrder(order = 3)
    private String  AgencyCod;
    @FieldOrder(order = 4)
    private String  AgencyName;
    @FieldOrder(order = 5)
    private String  Email;
    @FieldOrder(order = 6)
    private String  Fax;
    @FieldOrder(order = 7)
    private String  Id;
    @FieldOrder(order = 8)
    private String  LegalPerson;
    @FieldOrder(order = 9)
    private String  Phone;
    @FieldOrder(order = 10)
    private String  Shareholder;
    @FieldOrder(order = 11)
    private String  State;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAgencyCod() {
        return AgencyCod;
    }

    public void setAgencyCod(String agencyCod) {
        AgencyCod = agencyCod;
    }

    public String getAgencyName() {
        return AgencyName;
    }

    public void setAgencyName(String agencyName) {
        AgencyName = agencyName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getLegalPerson() {
        return LegalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        LegalPerson = legalPerson;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getShareholder() {
        return Shareholder;
    }

    public void setShareholder(String shareholder) {
        Shareholder = shareholder;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }
}
