package com.gl.nbinfo.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * HttpClient发送GET、POST请求
 * @Author libin
 * @CreateDate 2018.5.28 16:56
 */
public class HttpClientService {

    /**
     * 返回成功状态码
     */
    private static final int SUCCESS_CODE = 200;

    /**
     * 发送GET请求
     *
     * @param response
     * @param request
     * @param url   请求url
     * @return JSON或者字符串
     * @throws Exception
     */
    public static String sendGet(HttpServletResponse response, HttpServletRequest request, String url, String token) throws Exception{
        String urls="http://csjzyc.nbinfo.cn"+url;

        JSONObject jsonObject = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse resp = null;
        try{
            /**
             * 创建HttpClient对象
             */
            client = HttpClients.createDefault();
            /**
             * 创建URIBuilder
             */
            URIBuilder uriBuilder = new URIBuilder(urls);
            /**
             * 创建HttpGet
             */
            HttpGet httpGet = new HttpGet(uriBuilder.build());
            /**
             * 设置请求头部编码
             */
            httpGet.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
            /**
             * 设置返回编码
             */
            httpGet.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));

            httpGet.setHeader(new BasicHeader("Authorization", token));
            /**
             * 请求服务
             */
            resp = client.execute(httpGet);
            /**
             * 获取响应吗
             */
            int statusCode = resp.getStatusLine().getStatusCode();

            if (SUCCESS_CODE == statusCode){
                /**
                 * 获取返回对象
                 */
                HttpEntity entity = resp.getEntity();
                /**
                 * 通过EntityUitls获取返回内容
                 */
                String result = EntityUtils.toString(entity,"UTF-8");
                /**
                 * 转换成json,根据合法性返回json或者字符串
                 */
                try{
                    jsonObject = JSONObject.parseObject("{\"Message\":\"\",\"Data\":[{\"Honor\":\"adsf\",\"PersonName\":\"fdads\",\"Address\":\"erqe\",\"ResearchDirection\":\"fadsf\",\"BaseInfo\":\"asdf\",\"UnitName\":\"afsads\",\"ProfessionName\":\"adff\",\"Class_1\":\"dasd\",\"Book\":\"fwe\",\"Focus\":\"fad\",\"Summary\":\"fasdf\",\"Class\":\"asdf\",\"Id\":\"2312\",\"ResearchField\":\"fasd\",\"Resume\":\"afds\"},{\"Honor\":\"adsf\",\"PersonName\":\"fdads\",\"Address\":\"erqe\",\"ResearchDirection\":\"fadsf\",\"BaseInfo\":\"asdf\",\"UnitName\":\"afsads\",\"ProfessionName\":\"adff\",\"Class_1\":\"dasd\",\"Book\":\"fwe\",\"Focus\":\"fad\",\"Summary\":\"fasdf\",\"Class\":\"asdf\",\"Id\":\"2342342\",\"ResearchField\":\"fasd\",\"Resume\":\"afds\"}],\"Code\":\"0\",\"Result\":true}");
                    System.out.println(jsonObject.toJSONString());
                    return jsonObject.toJSONString();
                }catch (Exception e){
                    return result;
                }
            }else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            resp.close();
            client.close();
        }
        return null;
    }

    /**
     * 发送POST请求
     * @param url
     * @param nameValuePairList
     * @return JSON或者字符串
     * @throws Exception
     */
    public static Object sendPost(String url, List<NameValuePair> nameValuePairList) throws Exception{
        JSONObject jsonObject = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try{
            /**
             *  创建一个httpclient对象
             */
            client = HttpClients.createDefault();
            /**
             * 创建一个post对象
             */
            HttpPost post = new HttpPost(url);
            /**
             * 包装成一个Entity对象
             */
            StringEntity entity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
            /**
             * 设置请求的内容
             */
            post.setEntity(entity);
            /**
             * 设置请求的报文头部的编码
             */
            post.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"));
            /**
             * 设置请求的报文头部的编码
             */
            post.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
            /**
             * 执行post请求
             */
            response = client.execute(post);
            /**
             * 获取响应码
             */
            int statusCode = response.getStatusLine().getStatusCode();
            if (SUCCESS_CODE == statusCode){
                /**
                 * 通过EntityUitls获取返回内容
                 */
                String result = EntityUtils.toString(response.getEntity(),"UTF-8");
                /**
                 * 转换成json,根据合法性返回json或者字符串
                 */
                try{
                    jsonObject = JSONObject.parseObject(result);
                    return jsonObject;
                }catch (Exception e){
                    return result;
                }
            }else{
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            response.close();
            client.close();
        }
        return null;
    }
}