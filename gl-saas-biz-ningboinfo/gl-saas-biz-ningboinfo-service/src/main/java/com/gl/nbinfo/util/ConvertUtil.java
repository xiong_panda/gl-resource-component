package com.gl.nbinfo.util;

/**
 * xiaweijie
 * 处理值转换
 */
public class ConvertUtil {

    /**
     * 处理返回值中的特殊字符
     */
    public static String rmSpecialChar(String param){
        String result = param.replaceAll("\\^A", "")
                .replaceAll("\\^B", ";")
                .replaceAll("%", ";")
                .replaceAll("\\^C", ";")
                .replaceAll("\\^D", ";");
        return result;
    }

}
