package com.gl.nbinfo;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableSwagger2Doc
@MapperScan(basePackages = "com.gl.nbinfo.mapper.*")
public class NBInfoApp {
    public static void main(String[] args) {
        SpringApplication.run(NBInfoApp.class);
        System.out.println("-------启动成功---------");
    }
}
