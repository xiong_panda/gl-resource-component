package com.gl.nbinfo.service;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.util.IdWorker;
import com.gl.nbinfo.annotation.FieldOrder;
import com.gl.nbinfo.en.EDataSource;
import com.gl.nbinfo.en.EDataStatus;
import com.gl.nbinfo.mapper.core.PatentServiceMapper;
import com.gl.nbinfo.mapper.dictionary.MarkDataMapper;
import com.gl.nbinfo.po.PatentServicePO;
import com.gl.nbinfo.po.MarkDataPO;
import com.gl.nbinfo.po.NPatentServicePO;
import net.sf.json.xml.XMLSerializer;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.*;

//资源构件
@Service
public class PatentServiceService {
    @Autowired
    private PatentServiceMapper patentServiceMapper;
    @Autowired
    private MarkDataMapper markDataMapper;

    private static final IdWorker idWorker = new IdWorker();

    //标记构件
    public  List<PatentServicePO> flagStatusComponent(String resultJson) throws IllegalAccessException, DocumentException {
        List<Map> dataList = JsonTOList(resultJson);
        //进入字典库对照,如果都匹配不上,则不作后续处理,直接返回
        List<String> fieldList=patentServiceMapper.PSselectField();
        Set fieldSet = dataList.get(0).keySet();
        //如果一个字段都对应不上,则作废
        boolean i=true;

        for (Object field : fieldSet) {
            if(fieldList.contains(field)){
                i=false;
                break;
            }
        }

        if (i) {
            return null;
        }
        List<NPatentServicePO> poList = getPOList(dataList);
        List<PatentServicePO> patentServicePOList = standardComponent(poList);
        return patentServicePOList;
    }
    //标记构件-格式化数据
    public List<Map> JsonTOList(String resultJson){
        //数据转换
        Map<String, Object> resultMap = JSONObject.parseObject(resultJson, Map.class);
        List<Map> dataList=null;
        if(resultMap.get("data")!=null){
            String mainDataJson = resultMap.get("data").toString();
           dataList = JSONObject.parseArray(mainDataJson, Map.class);
        }
        if (dataList == null || dataList.size() < 1) {
            return null;
        }
        return dataList;
    }
    public List<NPatentServicePO> getPOList(List<Map> dataList){

        List<MarkDataPO> markList = new ArrayList<>();
        List<NPatentServicePO> nPatentServicePOList= new ArrayList<>();

        for (Map data : dataList) {
            NPatentServicePO nPatentServicePO = JSONObject.parseObject(
                    JSONObject.toJSONString(data), NPatentServicePO.class);
            //创建id
            String id = Long.toString(idWorker.nextId());
            nPatentServicePO.setID(id);
            //创建对应的标记
            MarkDataPO markDataPO = new MarkDataPO(Long.toString(idWorker.nextId()), id, EDataStatus.NEW.code, EDataSource.NING_BO.code, "3");
            markList.add(markDataPO);
            nPatentServicePOList.add(nPatentServicePO);
        }
        markDataMapper.batchInsertToCore(markList);
        return nPatentServicePOList;
        //进入标准化构件
//        standardComponent(wDongfangChinesePatentPOList, markList);
    }


    //标准化构件
    @Transactional(rollbackFor = Exception.class)
    public List<PatentServicePO> standardComponent(List<NPatentServicePO> list) throws IllegalAccessException, DocumentException {
        List<PatentServicePO> patentServicePOList = new ArrayList<>();
        for (NPatentServicePO nPatentServicePO : list) {

            Field[] wFields = nPatentServicePO.getClass().getDeclaredFields();

            PatentServicePO patentServicePO = new PatentServicePO();
            Field[] gFields = patentServicePO.getClass().getDeclaredFields();

            //利用反射填充核心资源库数据
            for (Field field : gFields) {
                field.setAccessible(true);
                for (Field wfield : wFields) {
                    wfield.setAccessible(true);
                    if (field.getAnnotation(FieldOrder.class).equals(wfield.getAnnotation(FieldOrder.class))) {
                        field.set(patentServicePO, wfield.get(nPatentServicePO));
                    }
                }
            }
            patentServicePO.setResourceFrom(EDataSource.NING_BO.value);
            patentServicePO.setResourceLogo(EDataSource.NING_BO.logo);
            patentServicePOList.add(patentServicePO);
        }
        patentServiceMapper.PSbatchInsertToCore(patentServicePOList);
        return patentServicePOList;

        //进入清洗构件
        //washComponent(chinesePatentList);
    }

    //清洗构件
    private void washComponent(List<PatentServicePO> list) throws IllegalAccessException, DocumentException {
        for (PatentServicePO PatentServicePO : list) {
            //反射字段类型为字符串的数据,进行清洗
            Field[] fields = PatentServicePO.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(PatentServicePO);

                if (value != null && value instanceof String ) {
                    //判断json是否为集合,如果是,去除json的格式,留下数据,用逗号分隔
                    if (jsonType(value) == 2) {
                        StringBuilder stringBuilder = new StringBuilder();
                        List<String> stringList = JSONObject.parseArray(value.toString(), String.class);

                        for (int i=0; i < stringList.size(); i++) {
                            if (i == stringList.size()-1) {
                                stringBuilder.append(stringList.get(i));
                                break;
                            }
                            stringBuilder.append(stringList.get(i)+",");
                        }

                        field.set(PatentServicePO,stringBuilder.toString());
                    }
                    
                    //是否为xml数据,将xml数据转换为普通json
                    if (jsonType(value) == 3) {
                        XMLSerializer xmlSerializer = new XMLSerializer();
                        String xmlString = value.toString();
                        //int i = xmlString.indexOf(">");
                        //xmlString.substring(i+1)
                        String result = xmlSerializer.read(xmlString).toString();

                        field.set(PatentServicePO,JSONObject.toJSONString(result));
                    }
                }

                //判断时间是否正常,不正常进行剔除
                if (value != null && value instanceof Date) {
                    Date date = (Date) value;
                    if (date.getTime() > System.currentTimeMillis()) {
                        field.set(PatentServicePO,null);
                    }
                }
            }
        }
        //进入筛选构件
        screenComponent(list);
    }

    //筛选构件
    private void screenComponent(List<PatentServicePO> list) {
//        //按mainClassCode2(主分类号)分类,标准以IPC为标准
//        Map<String, List<PatentServicePO>> mapByMainClass = list.stream().
//                collect(Collectors.groupingBy(
////                        e -> e.getIPC() != null ? e.getIPC().substring(0, 1) : "unknow")
//                );
//        //将空的分类号标识为未知分类
//        mapByMainClass.get("unknow").stream().forEach(e->e.setIPC("unknow"));
//
//
//        if (mapByMainClass.size() > 1) {
//            Set<String> keySet = mapByMainClass.keySet();
//            keySet.remove("unknow");
//
//            //将不在IPC分类标准中的数据也划分到未知分类中
//            for (String key : keySet) {
//                if (!EIPCClassify.keyList().contains(key)) {
//                    List<PatentServicePO> unknowList=mapByMainClass.get(key);
//                    mapByMainClass.get(key).addAll(unknowList);
//                    mapByMainClass.remove(key);
//                }
//            }
//        }


        //进入集成构件
//        integrateComponent(mapByMainClass);
    }

    //集成构件
    private void integrateComponent(Map<String, List<PatentServicePO>> mapByMainClass) {
        //去重,专利去重的标准为申请号
        for (Map.Entry<String, List<PatentServicePO>> entry : mapByMainClass.entrySet()) {
            List<PatentServicePO> list = entry.getValue();

        }
        //todo 存入主题资源库
        //进入融合构件
        //mergeComponent(list);
    }

    //融合构件
    private List<?> mergeComponent(List<?> list) {

        //todo 存入标准资源库
        //返回数据
        return list;
    }


    //返回顺序字段
    private List<Field> getOrderedField(Field[] fields) {
        List<Field> fieldList = new ArrayList<>();
        for (Field field : fields) {
            if (field.getAnnotation(FieldOrder.class) != null) {
                fieldList.add(field);
            }
        }
        fieldList.sort(Comparator.comparing(
                e -> e.getAnnotation(FieldOrder.class).order()
        ));
        return fieldList;
    }

    public int jsonType(Object object){
        /*
         * return 0:既不是array也不是object
         * return 1：object
         * return 2 ：Array
         * return 3 ：xml
         */

        try {
            JSONObject.parseObject(object.toString());
            return 1;
        } catch (Exception e) {// 抛错 说明JSON字符不是数组或根本就不是JSON
            try {
                JSONObject.parseArray(object.toString());
                return 2;
            } catch (Exception ee) {// 抛错 说明JSON字符根本就不是JSON
                try {
                    DocumentHelper.parseText(object.toString());
                    return 3;
                }catch (Exception eee) {
                    return 0;
                }
            }
        }

    }
}
