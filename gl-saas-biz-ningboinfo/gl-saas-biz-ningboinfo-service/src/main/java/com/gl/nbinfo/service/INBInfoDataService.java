package com.gl.nbinfo.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface INBInfoDataService {
    //检索列表查询
    String apiExpertListSearch( HttpServletRequest request, HttpServletResponse response) throws Exception;
    //检索列表数据爬取
    String apiListDataSearch( HttpServletRequest request, HttpServletResponse response);

    String requestData(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid);

    String apiHignTechListSearch(HttpServletRequest request, HttpServletResponse response);

    String apiPatentServiceListSearch(HttpServletRequest request, HttpServletResponse response);

    String apiPatentDetailListSearch(HttpServletRequest request, HttpServletResponse response);
}
