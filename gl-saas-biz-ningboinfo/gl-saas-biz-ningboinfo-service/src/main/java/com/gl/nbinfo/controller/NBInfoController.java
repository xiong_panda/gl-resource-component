package com.gl.nbinfo.controller;


import com.gl.basis.common.log.LogUtil;

import com.gl.nbinfo.po.EnterpriseProductPO;
import com.gl.nbinfo.po.ExpertPO;
import com.gl.nbinfo.po.PatentPO;
import com.gl.nbinfo.po.PatentServicePO;
import com.gl.nbinfo.service.*;
import com.gl.nbinfo.util.MD5Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping(value = "/nbInfo")
@Api(value = "东方",tags = {"宁波信息院专利搜索"})
public class NBInfoController {

    @Autowired
    INBInfoDataService inbInfoDataService;
    @Autowired
    ExpertService expertService;

    @Autowired
    EnterpriseProductService enterpriseProductService;

    @Autowired
    PatentService patentService;
    @Autowired
    PatentServiceService patentServiceService;

    @ApiOperation(value = "匹配列表检索--专家信息")
    @GetMapping("/apiExpertListSearch")
    @ResponseBody
    public List<ExpertPO>  ApiExpertListSearch(HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        List<ExpertPO>  expertPOList=new ArrayList<>();
        try {
            String data = inbInfoDataService.apiExpertListSearch(request, response);
            expertPOList = expertService.flagStatusComponent(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return expertPOList;
    }
    @ApiOperation(value = "匹配列表检索--企业数据")
    @GetMapping("/apiCorpListSearch")
    @ResponseBody
    public List<EnterpriseProductPO>  ApiCorpListSearch(HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        List<EnterpriseProductPO>  enterpriseProductPOList=new ArrayList<>();
        try {
            String data = inbInfoDataService.apiExpertListSearch(request, response);
            enterpriseProductPOList = enterpriseProductService.flagStatusComponent(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return enterpriseProductPOList;
    }

    @ApiOperation(value = "匹配列表检索--专利代理服务机构名录数据")
    @GetMapping("/apiOrgSearch")
    @ResponseBody
    public List<PatentPO>  ApiOrgSearch(HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        List<PatentPO>  patentPOList=new ArrayList<>();
        try {
            String data = inbInfoDataService.apiExpertListSearch(request, response);
            patentPOList = patentService.flagStatusComponent(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return patentPOList;
    }

    @ApiOperation(value = "匹配列表检索--专利信息")
    @GetMapping("/apiPatentListSearch")
    @ResponseBody
    public List<PatentPO>  ApiPatentListSearch(HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        List<PatentPO>  patentPOList=new ArrayList<>();
        try {
            String data = inbInfoDataService.apiExpertListSearch(request, response);
            patentPOList = patentService.flagStatusComponent(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return patentPOList;
    }


    @ApiOperation(value = "匹配列表检索 数据定时执行")
    @GetMapping("/apiListDataSearch")
    @ResponseBody
    public List<PatentServicePO> ApiListDataSearch(HttpServletResponse response, HttpServletRequest request) throws DocumentException, IllegalAccessException {
        LogUtil.info("匹配列表检索","开始匹配检索---------------------->");
        List<PatentServicePO>  patentServicePOList=new ArrayList<>();
        try {
            String data = inbInfoDataService.apiExpertListSearch(request, response);
            patentServicePOList = patentServiceService.flagStatusComponent(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return patentServicePOList;
    }

    /**
     * 企业销售情况
     *
     * @return
     */
    @ApiOperation(value = "其他")
    @GetMapping("/getTime")
    @ResponseBody
    public String getTime(String args,String parm) throws Exception {
        String data=(Calendar.getInstance().getTimeInMillis()+100000)+"";
//        if(args!=null||args!=""){
//            data=args;
//        }
//        String us2="AppId=2008&Ts="+data+"&PersonName=王五&PageIndex=123b0b2faad4f42e184e0dda70f459431";
        StringBuilder snstr=new StringBuilder();
        StringBuilder url=new StringBuilder();
        snstr.append("2008|")
                .append(args+"|")
                .append(data+"|").append("23b0b2faad4f42e184e0dda70f459431");
        String sn= MD5Util.getMD5Str(snstr.toString());
        url.append("http://csjzyc.nbinfo.cn/CommonApi/Expert/DetailList?AppId=2008&Ts=")
                .append(data+"&Sn="+sn)
                .append(parm==null?"":parm);
        return url+"//"+data+"//";
    }

}
