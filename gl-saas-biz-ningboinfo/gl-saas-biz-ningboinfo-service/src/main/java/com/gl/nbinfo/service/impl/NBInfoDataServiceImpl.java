package com.gl.nbinfo.service.impl;

import com.gl.basis.util.RedisUtil;
import com.gl.nbinfo.mapper.core.PageInfoMapper;
import com.gl.nbinfo.po.PageInfoPO;
import com.gl.nbinfo.service.INBInfoDataService;
import com.gl.nbinfo.util.HttpClientService;
import com.gl.nbinfo.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

@Service
public class NBInfoDataServiceImpl implements INBInfoDataService {

    @Autowired
    RedisUtil redisUtil;
    @Autowired
    PageInfoMapper pageInfoMapper;

    @Override
    @Transactional
    public String apiExpertListSearch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String appid = request.getParameter("AppId");
        String ts  =Calendar.getInstance().getTimeInMillis()+"";//索引编码，逗号分隔
        String personName  = request.getParameter("PersonName");
//        String pageIndex  =request.getParameter("PageIndex");
        StringBuilder snStr  =new StringBuilder(); // 需要加密

        PageInfoPO pip=new PageInfoPO();
        Integer page = pageInfoMapper.SelectPageNum("expert");
        if(page==null){
            pip.setIndexname("expert");
            pip.setSize(10);
            pip.setPage(1);
            pip.setCurrentData(0);
            pip.setCount(2317722);
            pip.setFrom(3);
            pageInfoMapper.insertData(pip);
        }
        snStr.append(appid==null?"2008|":appid)
                .append(personName==null?"":personName+"|")
                .append(page==null?"":page+"|")
                .append(ts+"|")
                .append("23b0b2faad4f42e184e0dda70f459431");
        String sn= MD5Util.getMD5Str(snStr.toString());
        //1、读取初始URL
        String urlNameString = "/CommonApi/Expert/DetailList";
        StringBuffer sb = new StringBuffer();

        sb.append(appid==null?"?AppId=2008":"?AppId="+appid)
                .append("&Ts="+ts)
                .append("&Sn="+sn)
                .append(personName==null?"":"&PersonName="+personName)
                .append(page==null?"":"&PageIndex="+page);

        //是否是调度平台调用接口
        String data= requestData(response,request,sb.toString(),urlNameString,null);
        pip=new PageInfoPO();
        pip.setIndexname("expert");
        //更新执行的位置
        pip.setCurrentData((page)*10);//todo： 调用统计接口跟新count数据
        pip.setPage(++page);
        pip.setSize(10);
        pageInfoMapper.batchUpdate(pip);

        return data;
    }
    @Override
    @Transactional

    public String apiHignTechListSearch(HttpServletRequest request, HttpServletResponse response) {
        String appid = request.getParameter("AppId");
        String ts  =Calendar.getInstance().getTimeInMillis()+"";//索引编码，逗号分隔
        String creditCode  = request.getParameter("CreditCode");
        String identificattionnumber  = request.getParameter("Identificattionnumber");
        String unitName  = request.getParameter("UnitName");
//        String pageIndex  =request.getParameter("PageIndex");
        StringBuilder snStr  =new StringBuilder(); // 需要加密

        PageInfoPO pip=new PageInfoPO();
        Integer page = pageInfoMapper.SelectPageNum("higntech");
        if(page==null){
            pip.setIndexname("higntech");
            pip.setSize(10);
            pip.setPage(1);
            pip.setCurrentData(0);
            pip.setCount(2317722);
            pip.setFrom(3);
            pageInfoMapper.insertData(pip);
        }
        snStr.append(appid==null?"2008|":appid)
                .append(creditCode==null?"":creditCode+"|")
                .append(identificattionnumber==null?"":identificattionnumber+"|")
                .append(unitName==null?"":unitName+"|")
                .append(page==null?"":page+"|")
                .append(ts+"|")
                .append("23b0b2faad4f42e184e0dda70f459431");
        String sn= MD5Util.getMD5Str(snStr.toString());
        //1、读取初始URL
        String urlNameString = "/CommonApi/HignTech/DetailList";
        StringBuffer sb = new StringBuffer();

        sb.append(appid==null?"?AppId=2008":"?AppId="+appid)
                .append("&Ts="+ts)
                .append("&Sn="+sn)
                .append(creditCode==null?"":"&CreditCode="+creditCode)
                .append(identificattionnumber==null?"":"&Identificattionnumber="+identificattionnumber)
                .append(unitName==null?"":"&UnitName="+unitName)
                .append(page==null?"":"&PageIndex="+page);

        //是否是调度平台调用接口
        String data= requestData(response,request,sb.toString(),urlNameString,null);
        pip=new PageInfoPO();
        pip.setIndexname("higntech");
        //更新执行的位置
        pip.setCurrentData((page)*10);//todo： 调用统计接口跟新count数据
        pip.setPage(++page);
        pip.setSize(10);
        pageInfoMapper.batchUpdate(pip);
        return data;
    }

    @Override
    @Transactional
    public String apiPatentServiceListSearch(HttpServletRequest request, HttpServletResponse response) {
        String appid = request.getParameter("AppId");
        String ts  =Calendar.getInstance().getTimeInMillis()+"";//索引编码，逗号分隔
        String agencyName  = request.getParameter("AgencyName");
//        String pageIndex  =request.getParameter("PageIndex");
        StringBuilder snStr  =new StringBuilder(); // 需要加密

        PageInfoPO pip=new PageInfoPO();
        Integer page = pageInfoMapper.SelectPageNum("patentService");
        if(page==null){
            pip.setIndexname("patentService");
            pip.setSize(10);
            pip.setPage(1);
            pip.setCurrentData(0);
            pip.setCount(2317722);
            pip.setFrom(3);
            pageInfoMapper.insertData(pip);
        }
        snStr.append(appid==null?"2008|":appid)
                .append(agencyName==null?"":agencyName+"|")
                .append(page==null?"":page+"|")
                .append(ts+"|")
                .append("23b0b2faad4f42e184e0dda70f459431");
        String sn= MD5Util.getMD5Str(snStr.toString());
        //1、读取初始URL
        String urlNameString = "/CommonApi/PatentService/DetailList";
        StringBuffer sb = new StringBuffer();

        sb.append(appid==null?"?AppId=2008":"?AppId="+appid)
                .append("&Ts="+ts)
                .append("&Sn="+sn)
                .append(agencyName==null?"":"&AgencyName="+agencyName)
                .append(page==null?"":"&PageIndex="+page);

        //是否是调度平台调用接口
        String data= requestData(response,request,sb.toString(),urlNameString,null);
        pip=new PageInfoPO();
        pip.setIndexname("patentService");
        //更新执行的位置
        pip.setCurrentData((page)*10);//todo： 调用统计接口跟新count数据
        pip.setPage(++page);
        pip.setSize(10);
        pageInfoMapper.batchUpdate(pip);
        return data;
    }

    @Override
    @Transactional

    public String apiPatentDetailListSearch(HttpServletRequest request, HttpServletResponse response) {
        String appid = request.getParameter("AppId");
        String ts  =Calendar.getInstance().getTimeInMillis()+"";//索引编码，逗号分隔
//        String ApplyNumber  = request.getParameter("AgencyName");
//        String pageIndex  =request.getParameter("PageIndex");
        StringBuilder snStr  =new StringBuilder(); // 需要加密

        PageInfoPO pip=new PageInfoPO();
        Integer page = pageInfoMapper.SelectPageNum("patentDetail");
        if(page==null){
            pip.setIndexname("patentDetail");
            pip.setSize(10);
            pip.setPage(1);
            pip.setCurrentData(0);
            pip.setCount(2317722);
            pip.setFrom(3);
            pageInfoMapper.insertData(pip);
        }
        snStr.append(appid==null?"2008|":appid)
//                .append(agencyName==null?"":agencyName+"|")
                .append(page==null?"":page+"|")
                .append(ts+"|")
                .append("23b0b2faad4f42e184e0dda70f459431");
        String sn= MD5Util.getMD5Str(snStr.toString());
        //1、读取初始URL
        String urlNameString = "/CommonApi/PatentDetail/DetailList";
        StringBuffer sb = new StringBuffer();

        sb.append(appid==null?"?AppId=2008":"?AppId="+appid)
                .append("&Ts="+ts)
                .append("&Sn="+sn)
//                .append(agencyName==null?"":"&AgencyName="+agencyName)
                .append(page==null?"":"&PageIndex="+page);

        //是否是调度平台调用接口
        String data= requestData(response,request,sb.toString(),urlNameString,null);
        pip=new PageInfoPO();
        pip.setIndexname("patentDetail");
        //更新执行的位置
        pip.setCurrentData((page)*10);//todo： 调用统计接口跟新count数据
        pip.setPage(++page);
        pip.setSize(10);
        pageInfoMapper.batchUpdate(pip);
        return data;
    }


    @Override
    public String apiListDataSearch(HttpServletRequest request, HttpServletResponse response) {
        return null;
    }



    //获取数据，发送请求  进入构件
    @Override
    public String requestData(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid){

        String data = null;
//        if(null!=redisUtil.get(param)){
//            data=redisUtil.get(param).toString();
//            Long count=String.valueOf(redisUtil.get("count"+param))=="null"?1:Long.valueOf(String.valueOf(redisUtil.get("count"+param)))+1;
//            redisUtil.set("count"+param,count,604800L);
//        }else {
        try {
            data = HttpClientService.sendGet(response,request,url + param, null);
            //进入构件 存取数据
            //将数据存入redis
            //todo ： 将标准化后的数据存入redis并返回
            redisUtil.set(param, data, 36000L);
            Long count=String.valueOf(redisUtil.get("count"+param))=="null"?1:Long.valueOf(String.valueOf(redisUtil.get("count"+param)))+1;
            redisUtil.set("count"+param,count,604800L);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        }
        return data;
    }

}
