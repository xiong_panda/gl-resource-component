package com.gl.nbinfo.mapper.dictionary;

import com.gl.nbinfo.po.MarkDataPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MarkDataMapper {
    int batchInsertToCore(List<MarkDataPO> list);

    List<String> selectField();

    Integer selectResourceNum();
}
