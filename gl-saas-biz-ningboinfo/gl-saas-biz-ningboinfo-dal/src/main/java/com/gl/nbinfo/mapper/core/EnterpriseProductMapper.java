package com.gl.nbinfo.mapper.core;

import com.gl.nbinfo.po.EnterpriseProductPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface EnterpriseProductMapper {

    //核心资源库 宁波信息院数据
    int EPbatchInsertToCore(@Param("list") List<EnterpriseProductPO> list);
    //核心资源库宁波信息院数据
    List<String> EPselectField();
}
