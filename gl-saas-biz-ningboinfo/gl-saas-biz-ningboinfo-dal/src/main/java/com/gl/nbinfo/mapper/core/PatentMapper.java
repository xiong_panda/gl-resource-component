package com.gl.nbinfo.mapper.core;

import com.gl.nbinfo.po.PatentPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface PatentMapper {
    //核心资源库 宁波信息院数据
    int PbatchInsertToCore(@Param("list") List<PatentPO> list);
    //核心资源库宁波信息院数据
    List<String> PselectField();
}
