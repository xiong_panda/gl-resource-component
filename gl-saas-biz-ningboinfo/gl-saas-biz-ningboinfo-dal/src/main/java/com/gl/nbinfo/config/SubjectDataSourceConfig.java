package com.gl.nbinfo.config;


import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@MapperScan(
        basePackages = "com.gl.nbinfo.mapper.subject",
        sqlSessionFactoryRef = "sqlSessionFactory3"
)
@EnableConfigurationProperties(JdbcThirdlyProperties.class)
@EnableTransactionManagement
public class SubjectDataSourceConfig {

    private static Logger logger = LoggerFactory.getLogger(SubjectDataSourceConfig.class);

    @Autowired
    private JdbcThirdlyProperties jdbcThirdlyProperties;

    /**
     * 创建主数据源
     *
     * @return
     * @throws SQLException
     */
    @Bean(value="dataSource3",initMethod = "init", destroyMethod = "close")
    public DataSource dataSource3() {
        logger.info("===>>>开始初始化subject数据源:[dataSource]");
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(jdbcThirdlyProperties.getDriverClassName());
        druidDataSource.setUrl(jdbcThirdlyProperties.getUrl());
        druidDataSource.setUsername(jdbcThirdlyProperties.getUsername());
        druidDataSource.setPassword(jdbcThirdlyProperties.getPassword());
        druidDataSource.setInitialSize(jdbcThirdlyProperties.getInitialSize());
        druidDataSource.setMinIdle(jdbcThirdlyProperties.getMinIdle());
        druidDataSource.setMaxActive(jdbcThirdlyProperties.getMaxActive());
        druidDataSource.setMaxWait(jdbcThirdlyProperties.getMaxWait());
        druidDataSource.setTimeBetweenEvictionRunsMillis(jdbcThirdlyProperties.getTimeBetweenEvictionRunsMillis());
        druidDataSource.setMinEvictableIdleTimeMillis(jdbcThirdlyProperties.getMinEvictableIdleTimeMillis());
        druidDataSource.setValidationQuery(jdbcThirdlyProperties.getValidationQuery());
        druidDataSource.setTestWhileIdle(jdbcThirdlyProperties.isTestWhileIdle());
        druidDataSource.setTestOnBorrow(jdbcThirdlyProperties.isTestOnBorrow());
        druidDataSource.setTestOnReturn(jdbcThirdlyProperties.isTestOnReturn());
        druidDataSource.setPoolPreparedStatements(jdbcThirdlyProperties.isPoolPreparedStatements());
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(
                jdbcThirdlyProperties.getMaxPoolPreparedStatementPerConnectionSize());
        druidDataSource.setRemoveAbandoned(jdbcThirdlyProperties.isRemoveAbandoned());
        druidDataSource.setRemoveAbandonedTimeout(jdbcThirdlyProperties.getRemoveAbandonedTimeout());
        druidDataSource.setLogAbandoned(jdbcThirdlyProperties.isLogAbandoned());

        return druidDataSource;
    }

    /**
     * 事务管理器
     *
     * @return
     */
    @Bean(name = "transactionManager3")
    public PlatformTransactionManager transactionManager3() {
        return new DataSourceTransactionManager(dataSource3());
    }



    /**
     * 配置sqlSessionFactory 在mybatis的配置文件中配置分页拦截器
     *
     * @return
     * @throws Exception
     */
    @Bean(name = "sqlSessionFactory3")
    public SqlSessionFactory sqlSessionFactoryBean3() {
        logger.info("===>>>开始初始化subject数据源mybatis的SqlSessionFactory");

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource3());
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setConfigLocation(resolver.getResource("classpath:config/mybatis/mybatis-config.xml"));
        try {
            sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:**/mapper/subject/*.xml"));
            return sqlSessionFactoryBean.getObject();
        } catch (Exception e) {
            logger.error("===>>>初始化subject数据源mybatis的sqlSessionFactory失败:" + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * 配置SqlSessionTemplate
     *
     * @return
     */
    @Bean(name = "sqlSessionTemplate3")
    public SqlSessionTemplate sqlSessionTemplate3() {
        return new SqlSessionTemplate(sqlSessionFactoryBean3());
    }
}
