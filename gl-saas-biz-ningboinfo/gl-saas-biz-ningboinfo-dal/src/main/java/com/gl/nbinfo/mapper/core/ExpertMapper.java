package com.gl.nbinfo.mapper.core;

import com.gl.nbinfo.po.ExpertPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface ExpertMapper {
    //核心资源库东方数据
    int EbatchInsertToCore(@Param("list") List<ExpertPO> list);
    //核心资源库东方数据
    List<String> EselectField();
}
