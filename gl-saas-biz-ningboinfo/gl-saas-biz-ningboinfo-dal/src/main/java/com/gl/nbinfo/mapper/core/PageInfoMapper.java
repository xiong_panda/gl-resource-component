package com.gl.nbinfo.mapper.core;

import com.gl.nbinfo.po.PageInfoPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface PageInfoMapper {
    //核心资源库
    Integer SelectPageNum(String indexname);
    //更新数据
    void batchUpdate(PageInfoPO pageInfoPO);
    //插入一条数据
    void insertData(PageInfoPO pageInfoPO);

}
