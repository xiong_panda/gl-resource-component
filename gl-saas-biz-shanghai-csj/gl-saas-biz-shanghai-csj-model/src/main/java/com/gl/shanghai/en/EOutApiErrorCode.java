package com.gl.shanghai.en;

/**
 * 公共错误码
 */
public enum EOutApiErrorCode {
    JSON_PARSE_ERR("300","JSON解析异常"),
    DATABASE_ERR("301","数据库错误"),
    REMOTE_ERR("302","远程调用错误"),
    CLASS_CAST_ERR("303","类型转换错误"),
    PARAM_ERR("304","接口请求参数错误"),
    UNKNOW_ERR("305","未知错误"),
    FIELD_ERR("306","查询字段无法匹配"),
    ;

    public String code;

    public String msg;

    EOutApiErrorCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
