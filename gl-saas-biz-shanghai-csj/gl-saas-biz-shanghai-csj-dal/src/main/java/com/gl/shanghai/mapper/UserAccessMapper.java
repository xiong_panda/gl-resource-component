package com.gl.shanghai.mapper;

import com.gl.shanghai.po.UserAccessPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserAccessMapper {
    //用户访问数据插入
    int ALbatchInsertToCore(@Param("list") List<UserAccessPo> list);

}
