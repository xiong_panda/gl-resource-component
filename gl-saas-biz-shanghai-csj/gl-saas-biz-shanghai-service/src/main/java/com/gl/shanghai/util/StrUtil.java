package com.gl.shanghai.util;


public class StrUtil {

    /*
     * 去掉首尾指定字符串
     */
    public static String trimBothEndsChars(String srcStr, String splitter) {
        String regex = "^" + splitter + "*|" + splitter + "*$";
        return srcStr.replaceAll(regex, "");
    }

}
