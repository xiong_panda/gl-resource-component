package com.gl.shanghai.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gl.shanghai.mapper.UserAccessMapper;
import com.gl.shanghai.po.UserAccessPo;
import com.gl.shanghai.service.IUserAccessService;
import com.gl.shanghai.util.HttpClientService;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserAccessService implements IUserAccessService {

    @Autowired
    UserAccessMapper userAccessMapper;

    @Override
    @Transactional
    public int ALDataSyn(String startTime,String endTime) throws Exception {
//        List<NameValuePair> params=new ArrayList<NameValuePair>();
        //建立一个NameValuePair数组，用于存储欲传送的参数
        Map<String,Object> params=new HashMap<>();
        params.put("startTime",startTime);
        params.put("endTime",endTime);
        params.put("platFrom","5BB7CDAAEB94738204EF801E6244E096");
//        params.add(new BasicNameValuePair("startTime",startTime));
//        params.add(new BasicNameValuePair("endTime",endTime));
//        params.add(new BasicNameValuePair("platFrom","5BB7CDAAEB94738204EF801E6244E096"));
        //添加参数

        String jsonMap = HttpClientService.doPost("", params);
        //json转po
        // json返回数据校验
        if(jsonMap==""){
            return 0;
        }
        //数据转换
        Map<String, Object> resultMap = JSONObject.parseObject(jsonMap, Map.class);
        String data = resultMap.get("data").toString();
        List<Map>  dataList = JSONObject.parseArray(data, Map.class);
        List<UserAccessPo> userAccessPos = new ArrayList<>();
        for (Map map:dataList){
            UserAccessPo userAccessPo = JSONObject.parseObject(
                    JSONObject.toJSONString(map), UserAccessPo.class);
            userAccessPos.add(userAccessPo);
        }
        userAccessMapper.ALbatchInsertToCore(userAccessPos);
        return userAccessPos.size();
    }
}
