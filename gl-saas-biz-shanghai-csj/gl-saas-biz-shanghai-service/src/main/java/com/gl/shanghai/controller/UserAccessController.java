package com.gl.shanghai.controller;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.util.StringUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CommBean;
import com.gl.shanghai.en.EOutApiErrorCode;
import com.gl.shanghai.service.impl.UserAccessService;
import com.gl.shanghai.util.ConvertUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/csj/back")
@Api(value = "数据同步",tags = {"长三角城市群用户访问资源回调"})
public class UserAccessController {
    @Autowired
    UserAccessService userAccessService;


    @ApiOperation(value = "用户访问数据回调")
    @GetMapping("/userdata")
    @ResponseBody
    public CityOutResult ALDataCallback(@RequestBody CommBean commBean) throws Exception {
        String startTime;
        String endTime;
        CityOutResult cityOutResult;
        if (commBean != null && !StringUtils.isEmpty(commBean.getBz_data())) {
            cityOutResult = new CityOutResult();
            startTime = JSONObject.parseObject(commBean.getBz_data(), Map.class).get("startTime").toString();
            endTime = JSONObject.parseObject(commBean.getBz_data(), Map.class).get("endTime").toString();
            if (endTime == null || endTime=="") {
                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                endTime = ft.format(new Date());
            }else if (startTime == null || startTime=="") {
                startTime= ConvertUtil.ZeroTime().get("yesterday");
            }
        } else {
            throw new BusinessException(EOutApiErrorCode.PARAM_ERR.msg+"-查询条件为空(json)", EOutApiErrorCode.PARAM_ERR.code);
        }
        int size = userAccessService.ALDataSyn(startTime,endTime);
        cityOutResult.setResult(size);
        cityOutResult.setTotalSize(size);
        return cityOutResult;
    }

}
