package com.gl.shanghai.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * HttpClient发送GET、POST请求
 * @Author libin
 * @CreateDate  2018.5.28 16:56
 */
public class HttpClientService {

    /**
     * 返回成功状态码
     */
    private static final int SUCCESS_CODE = 200;

    private static final Logger logger = LoggerFactory.getLogger(HttpClientService.class);

    /**
     * 发送GET请求
     * @param url   请求url
     * @return JSON或者字符串
     * @throws Exception
     */
    public static String sendGet(String url, String token) throws Exception{
        String urls="http://119.3.210.47/csjsite-admin/tsp/log/query"+url;

        JSONObject jsonObject = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try{
            /**
             * 创建HttpClient对象
             */
            client = HttpClients.createDefault();
            /**
             * 创建URIBuilder
             */
            URIBuilder uriBuilder = new URIBuilder(urls);
            /**
             * 创建HttpGet
             */
            HttpGet httpGet = new HttpGet(uriBuilder.build());
            /**
             * 设置请求头部编码
             */
            httpGet.setHeader(new BasicHeader("Content-Type", "application/json; charset=UTF-8"));
            /**
             * 设置返回编码
             */
            httpGet.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));

            httpGet.setHeader(new BasicHeader("Authorization", token));
            /**
             * 请求服务
             */
            response = client.execute(httpGet);
            /**
             * 获取响应吗
             */
            int statusCode = response.getStatusLine().getStatusCode();

            logger.info("statusCode:"+statusCode);

            if (SUCCESS_CODE == statusCode){
                /**
                 * 获取返回对象
                 */
                HttpEntity entity = response.getEntity();
                /**
                 * 通过EntityUitls获取返回内容
                 */
                String result = EntityUtils.toString(entity,"UTF-8");
                /**
                 * 转换成json,根据合法性返回json或者字符串
                 */
                try{
                    jsonObject = JSONObject.parseObject(result);
                    return jsonObject.toJSONString();
                }catch (Exception e){
                    return result;
                }
            }else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            response.close();
            client.close();
        }
        return null;
    }

    /**
     * 发送POST请求
     * @param url
     * @param nameValuePairList
     * @return JSON或者字符串
     * @throws Exception
     */
    public static String sendPost(String url, List<NameValuePair> nameValuePairList) throws Exception{
        String urls="http://119.3.210.47/csjsite-admin/tsp/log/query"+url;

        JSONObject jsonObject = null;
        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;
        try{
            /**
             *  创建一个httpclient对象
             */
            client = HttpClients.createDefault();
            /**
             * 创建一个post对象
             */
            HttpPost post = new HttpPost(urls);
            /**
             * 包装成一个Entity对象
             */
            StringEntity entity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
            /**
             * 设置请求的内容
             */
            post.setEntity(entity);
            /**
             * 设置请求的报文头部的编码
             */
            post.setHeader(new BasicHeader("Content-Type", "application/json; charset=UTF-8"));
            /**
             * 设置请求的报文头部的编码
             */
            post.setHeader(new BasicHeader("Accept", "text/plain;charset=utf-8"));
            /**
             * 执行post请求
             */
            response = client.execute(post);
            /**
             * 获取响应码
             */
            int statusCode = response.getStatusLine().getStatusCode();
            if (SUCCESS_CODE == statusCode){
                /**
                 * 通过EntityUitls获取返回内容
                 */
                String result = EntityUtils.toString(response.getEntity(),"UTF-8");
                /**
                 * 转换成json,根据合法性返回json或者字符串
                 */
                try{
                    jsonObject = JSONObject.parseObject(result);
                    return jsonObject.toJSONString();
                }catch (Exception e){
                    return result;
                }
            }else{
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            response.close();
            client.close();
        }
        return null;
    }

    /**
     * 使用HttpClient4.5版本对指定接口路径发送POST请求
     * @param httpUrl   指定接口路径
     * @param params    接口路径需要携带的参数集
     * @return  接口返回的消息
     */
    public static String doPost(String httpUrl, Map<String,Object> params){
        String urls="http://119.3.210.47/csjsite-admin/tsp/log/query"+httpUrl;

        String result = null;
        //声明httpClient,httpResponse于try...catch语句外.方便最后关闭资源
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse httpResponse = null;
        try{
            httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(urls);
            //HttpEntity是一个接口,所有参数均是通过其子类携带
            //声明HttpEntity的实现子类UrlEncodedFormEntity用于携带参数
            //UrlEncodedFormEntity构造函数只接受BasicNameValuePair对象,符合程序逻辑
            UrlEncodedFormEntity formEntity = null;
            if(!params.isEmpty()){
                //获取Map集合的键集合
                Set<String> keySet = params.keySet();
                Iterator iterator = keySet.iterator();
                List<NameValuePair> list = new ArrayList<>();
                while (iterator.hasNext()){
                    String key = (String)iterator.next();
                    Object value = params.get(key);
                    //BasicNameValuePair通常是用来封装post请求中的参数名称和值
                    BasicNameValuePair basicNameValuePair = new BasicNameValuePair(key,value.toString());
                    list.add(basicNameValuePair);
                }
                //构造器方式注入Entity
                formEntity = new UrlEncodedFormEntity(list);
            }
            //将绑定了参数的HttpEntity子类实现类存入HttpPost对象中
            httpPost.setEntity(formEntity);
            //设置请求头
            httpPost.setHeader("Content-type","application/x-www-form-urlencoded");
            //向指定接口发送POST请求
            httpResponse = httpClient.execute(httpPost);
            if(httpResponse.getStatusLine().getStatusCode()==200){
                result = EntityUtils.toString(httpResponse.getEntity());
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            //逐一关闭使用过的资源
            try{
                if(httpResponse!=null){
                    httpResponse.close();
                }
                if(httpClient!=null){
                    httpClient.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }
}