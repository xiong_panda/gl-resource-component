package com.gl.shanghai.util;

import com.alibaba.fastjson.JSON;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * xiaweijie
 * 处理值转换
 */
public class ConvertUtil {

    /**
     * 处理返回值中的特殊字符
     */
    public static String rmSpecialChar(String param){
        String result = param.replaceAll("\\^A", "")
                .replaceAll("\\^B", ";")
                .replaceAll("%", ";")
                .replaceAll("\\^C", ";")
                .replaceAll("\\^D", ";");
        return result;
    }

    //或取json的内容
    public static  String hebing(String data){
        Map map= (Map) JSON.parse(data);
        String sources="";
        if (map != null && map.get("code") != null) {
            if("200".equals(map.get("code")+"")){
                Map map2 = (Map)map.get("data");
                sources=map2.get("sources").toString();
            }
        }
        return sources;
    }

    //获取今天or 昨天的0点时间
    public static Map<String, String> ZeroTime() {
        SimpleDateFormat Spdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        Date zero = cal.getTime();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)-1, 0, 0, 0);
        Date yzero  = cal.getTime();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String today = ft.format(zero);
        String yesterday = ft.format(yzero);

        Map<String, String> map = new HashMap<>();
        map.put("today", today);
        map.put("yesterday", yesterday);
        return map;
    }
}
