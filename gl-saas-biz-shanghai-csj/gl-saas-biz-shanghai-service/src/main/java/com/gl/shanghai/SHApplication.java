package com.gl.shanghai;


import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableSwagger2Doc
@EnableEurekaClient
//@EnableApolloConfig
@MapperScan(basePackages = "com.gl.shanghai.*")
public class SHApplication {
    public static void main(String[] args) {
        SpringApplication.run(SHApplication.class, args);
    }
}
