package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class EasyWAuthorLibraryOutDto {

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    private String wId;

    /**
     * 工作单位
     **/
    @ApiModelProperty(value = "工作单位")
    private String unit;

    /**
     * 一级单位
     **/
    @ApiModelProperty(value = "一级单位")
    private String level1Unit;

    /**
     * 二级单位
     **/
    @ApiModelProperty(value = "二级单位")
    private String level2Unit;

    /**
     * 单位
     **/
    @ApiModelProperty(value = "单位")
    private String wUnit;

    /**
     * 职称
     **/
    @ApiModelProperty(value = "职称")
    private String title;
    /**
     * 简介
     **/
    @ApiModelProperty(value = "简介")
    private String intro;
    /**
     * 姓名
     **/
    @ApiModelProperty(value = "姓名")
    private String name;
    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    private String keyword;
    /**
     * 性别
     **/
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 出生日期
     **/
    @ApiModelProperty(value = "出生日期")
    private Date birthDate;
    /**
     * 民族
     **/
    @ApiModelProperty(value = "民族")
    private String national;
}
