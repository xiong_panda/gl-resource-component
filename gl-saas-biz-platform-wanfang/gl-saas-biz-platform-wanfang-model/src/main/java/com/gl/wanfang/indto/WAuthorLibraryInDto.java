package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 作者库
 * @author code_generator
 */
@Data
public class WAuthorLibraryInDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String wId ; 

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 姓名:英文名 **/
    @ApiModelProperty(value = "姓名:英文名")
	private String aEnglishName ; 

	/** 工作单位 **/
    @ApiModelProperty(value = "工作单位")
	private String unit ; 

	/** 工作单位(全文检索) **/
    @ApiModelProperty(value = "工作单位(全文检索)")
	private String unitFulltextSearch ; 

	/** 工作单位:统计单位名称:英文单位名称 **/
    @ApiModelProperty(value = "工作单位:统计单位名称:英文单位名称")
	private String sunEnglishUnitName ; 

	/** 统计单位名称 **/
    @ApiModelProperty(value = "统计单位名称")
	private String statisticalUnitName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgallAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgallAnyname2 ; 

	/** 一级单位 **/
    @ApiModelProperty(value = "一级单位")
	private String level1Unit ; 

	/** 二级单位 **/
    @ApiModelProperty(value = "二级单位")
	private String level2Unit ; 

	/** 单位 **/
    @ApiModelProperty(value = "单位")
	private String wUnit ; 

	/** 英文单位名称 **/
    @ApiModelProperty(value = "英文单位名称")
	private String englishUnitName ; 

	/** 职称 **/
    @ApiModelProperty(value = "职称")
	private String title ; 

	/** 简介:备注:工作简历 **/
    @ApiModelProperty(value = "简介:备注:工作简历")
	private String introductionJobResume ; 

	/** 简介 **/
    @ApiModelProperty(value = "简介")
	private String intro ; 

	/** 姓名 **/
    @ApiModelProperty(value = "姓名")
	private String name ; 

	/** 英文名 **/
    @ApiModelProperty(value = "英文名")
	private String englishName ; 

	/** 导师 **/
    @ApiModelProperty(value = "导师")
	private String mentor ; 

	/** 地区 **/
    @ApiModelProperty(value = "地区")
	private String region ; 

	/** 邮编 **/
    @ApiModelProperty(value = "邮编")
	private String postcode ; 

	/** 地址 **/
    @ApiModelProperty(value = "地址")
	private String address ; 

	/** 邮件地址 **/
    @ApiModelProperty(value = "邮件地址")
	private String email ; 

	/** 机构类型 **/
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 日期 **/
    @ApiModelProperty(value = "日期")
	private Date date ; 

	/** 备注 **/
    @ApiModelProperty(value = "备注")
	private String note ; 

	/** 机构ID **/
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 频次 **/
    @ApiModelProperty(value = "频次")
	private String frequency ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String yfreq ; 

	/** 关键词 **/
    @ApiModelProperty(value = "关键词")
	private String keyword ;
//	private List<String> keyword ;

	/**  **/
    @ApiModelProperty(value = "")
	private String keywordAnyname ; 

	/** 相关关键词 **/
    @ApiModelProperty(value = "相关关键词")
	private String relatedKeyword ; 

	/** 相关期刊 **/
    @ApiModelProperty(value = "相关期刊")
	private String relatedJournals ; 

	/** 相关人物 **/
    @ApiModelProperty(value = "相关人物")
	private String relatedFigure ; 

	/** 相关学科 **/
    @ApiModelProperty(value = "相关学科")
	private String relatedDiscipline ; 

	/** 相关学科 **/
    @ApiModelProperty(value = "相关学科")
	private String relatedDiscipline2 ; 

	/** 相关基金 **/
    @ApiModelProperty(value = "相关基金")
	private String relatedFund ; 

	/** qcode **/
    @ApiModelProperty(value = "qcode")
	private String qcode ; 

	/** 性别 **/
    @ApiModelProperty(value = "性别")
	private String sex ; 

	/** 出生日期 **/
    @ApiModelProperty(value = "出生日期")
	private Date birthDate ; 

	/** 在职年月 **/
    @ApiModelProperty(value = "在职年月")
	private Date workingDays ; 

	/** 出生地点 **/
    @ApiModelProperty(value = "出生地点")
	private String birthAddress ; 

	/** 民族 **/
    @ApiModelProperty(value = "民族")
	private String national ; 

	/** 工作职务 **/
    @ApiModelProperty(value = "工作职务")
	private String jobPosition ; 

	/** 行政码 **/
    @ApiModelProperty(value = "行政码")
	private String administrativeCode ; 

	/** 电话 **/
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 传真 **/
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 教育背景 **/
    @ApiModelProperty(value = "教育背景")
	private String educationBackground ; 

	/** 最高学位 **/
    @ApiModelProperty(value = "最高学位")
	private String highestDegree ; 

	/** 外语语种 **/
    @ApiModelProperty(value = "外语语种")
	private String foreignLanguages ; 

	/** 区位 **/
    @ApiModelProperty(value = "区位")
	private String zoneBit ; 

	/** 专业领域与研究方向 **/
    @ApiModelProperty(value = "专业领域与研究方向")
	private String professionalResearch ; 

	/** 国内外学术或专业团体任职情况 **/
    @ApiModelProperty(value = "国内外学术或专业团体任职情况")
	private String coEmployment ; 

	/** 院士 **/
    @ApiModelProperty(value = "院士")
	private String academician ; 

	/** 专家荣誉 **/
    @ApiModelProperty(value = "专家荣誉")
	private String expertsHonor ; 

	/** 专家荣誉 **/
    @ApiModelProperty(value = "专家荣誉")
	private String expertsHonor2 ; 

	/** 专家特色 **/
    @ApiModelProperty(value = "专家特色")
	private String expertsCharacteristics ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String characterAnyname ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String characterAnyname2 ; 

	/** 学科分类名 **/
    @ApiModelProperty(value = "学科分类名")
	private String disciplineClass ; 

	/** 学科分类码 **/
    @ApiModelProperty(value = "学科分类码")
	private String disciplineClassCode ; 

	/** 学科分类码 **/
    @ApiModelProperty(value = "学科分类码")
	private String disciplineClassCode2 ; 

	/** 工作简历 **/
    @ApiModelProperty(value = "工作简历")
	private String jobResume ; 

	/** 数据来源 **/
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String h ; 

	/** 中文期刊第一作者发文数 **/
    @ApiModelProperty(value = "中文期刊第一作者发文数")
	private String journalcount ; 

	/** 总被引次数 **/
    @ApiModelProperty(value = "总被引次数")
	private String quotecount ; 

}
