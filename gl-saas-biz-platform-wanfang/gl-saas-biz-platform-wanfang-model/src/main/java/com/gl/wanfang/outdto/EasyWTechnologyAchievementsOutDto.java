package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 科技成果
 */
@Data
public class EasyWTechnologyAchievementsOutDto {
    private static final long serialVersionUID = 1L;
    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;


    /**
     * 唯一编号
     */
    private String otherId;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    private String Abstract;

    /**
     * 专利申请号
     **/
    @ApiModelProperty(value = "专利申请号")
    private String patentapplicationId;

    /**
     * 应用行业名称
     **/
    @ApiModelProperty(value = "应用行业名称")
    private String applicationIndustryName;

    /**
     * 资料公布日期
     **/
    @ApiModelProperty(value = "资料公布日期")
    private String datareleasedDate;


    /**
     * 公布时间
     */
    private String releasedDate;

    /**
     * 成果类别
     **/
    @ApiModelProperty(value = "成果类别")
    private String resultsType;

    /**
     * 成果水平
     **/
    @ApiModelProperty(value = "成果水平")
    private String resultsLevel;


    /**
     * 成果简介
     **/
    @ApiModelProperty(value = "成果简介")
    private String resultsSummary;

    /**
     * 主题词
     **/
    @ApiModelProperty(value = "主题词")
    private String keyword;


    /**
     * 成果类型
     **/
    @ApiModelProperty(value = "成果类型")
    private String achievementType;


    /**
     * 记录类型
     **/
    @ApiModelProperty(value = "记录类型")
    private String recordDate;

    /**
     * 登记号
     **/
    @ApiModelProperty(value = "登记号")
    private String registrationNo;


    /**
     * 信息来源
     **/
    @ApiModelProperty(value = "信息来源")
    private String sourceOfInfo;

    /**
     * 成果中文名称
     **/
    @ApiModelProperty(value = "成果中文名称")
    private String chineseName;

    /**
     * 转让注释
     **/
    @ApiModelProperty(value = "转让注释")
    private String transferAnnotation;

    /**
     * "主题词"
     **/
    @ApiModelProperty(value = "主题词")
    private String keyword2;

}

