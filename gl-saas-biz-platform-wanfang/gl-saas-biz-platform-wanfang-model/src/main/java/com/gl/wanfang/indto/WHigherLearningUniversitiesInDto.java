package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 高等院校
 * @author code_generator
 */
@Data
public class WHigherLearningUniversitiesInDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
//	private Integer id ;
	private String id ;

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 学校名称 **/
    @ApiModelProperty(value = "学校名称")
	private String name ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String nameAnyname1 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String nameAnyname2 ; 

	/** 学校名称 **/
    @ApiModelProperty(value = "学校名称")
	private String schoolName ; 

	/** 曾用名 **/
    @ApiModelProperty(value = "曾用名")
	private String ohtherName ; 

	/** 主管单位 **/
    @ApiModelProperty(value = "主管单位")
	private String compunit ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String compunitIko ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String iko ; 

	/** 省名 **/
    @ApiModelProperty(value = "省名")
	private String province ; 

	/** 市名 **/
    @ApiModelProperty(value = "市名")
	private String city ; 

	/** 县名 **/
    @ApiModelProperty(value = "县名")
	private String county ; 

	/** 区号 **/
    @ApiModelProperty(value = "区号")
	private String acode ; 

	/** 行政区代码 **/
    @ApiModelProperty(value = "行政区代码")
	private String region ; 

	/** 邮码 **/
    @ApiModelProperty(value = "邮码")
	private String postalCode ; 

	/** 地址 **/
    @ApiModelProperty(value = "地址")
	private String address ; 

	/** 电话 **/
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 传真 **/
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 域名地址 **/
    @ApiModelProperty(value = "域名地址")
	private String url ; 

	/** 专职教师数 **/
    @ApiModelProperty(value = "专职教师数")
	private String techs ; 

	/** 电子邮件 **/
    @ApiModelProperty(value = "电子邮件")
	private String email ; 

	/** 占地面积(平方米) **/
    @ApiModelProperty(value = "占地面积(平方米)")
	private String space ; 

	/** 办学层次 **/
    @ApiModelProperty(value = "办学层次")
	private String levels ; 

	/** 负责人 **/
    @ApiModelProperty(value = "负责人")
	private String person ; 

	/** 办学类型 **/
    @ApiModelProperty(value = "办学类型")
	private String schoolType ; 

	/** 重点学科 **/
    @ApiModelProperty(value = "重点学科")
	private String keydiscipline ; 

	/** 特色高校 **/
    @ApiModelProperty(value = "特色高校")
	private String specialSchool ; 

	/** 学校简介 **/
    @ApiModelProperty(value = "学校简介")
	private String intro ; 

	/** 分校及校区 **/
    @ApiModelProperty(value = "分校及校区")
	private String campus ; 

	/** 教学机构设置 **/
    @ApiModelProperty(value = "教学机构设置")
	private String orgSetting ; 

	/** 历史沿革 **/
    @ApiModelProperty(value = "历史沿革")
	private String history ; 

	/** 并入从属院校 **/
    @ApiModelProperty(value = "并入从属院校")
	private String subSchool ; 

	/** 教学科研仪器总值(万元) **/
    @ApiModelProperty(value = "教学科研仪器总值(万元)")
	private String gross ; 

	/** 机构ID **/
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String featurecollege ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String featurecollegeAnyname1 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String featurecollegeAnyname2 ; 

	/** 高校类型 **/
    @ApiModelProperty(value = "高校类型")
	private String collegeType ; 

	/** 是否成品数据 **/
    @ApiModelProperty(value = "是否成品数据")
	private String finishedProductData ; 
}
