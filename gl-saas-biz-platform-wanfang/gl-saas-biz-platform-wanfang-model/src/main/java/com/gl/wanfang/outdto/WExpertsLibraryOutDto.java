package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 专家库
 */
@Data
public class WExpertsLibraryOutDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 姓名 **/
    @ApiModelProperty(value = "姓名")
	private String name ; 

	/** 性别 **/
    @ApiModelProperty(value = "性别")
	private String sex ;

	/**  **/
    @ApiModelProperty(value = "")
	private String age ; 

	/** 出生日期 **/
    @ApiModelProperty(value = "出生日期")
	private String birthDate ; 

	/** 出生地点 **/
    @ApiModelProperty(value = "出生地点")
	private String birthAddress ; 

	/** 民族 **/
    @ApiModelProperty(value = "民族")
	private String national ; 

	/** 工作单位:其它工作单位 **/
    @ApiModelProperty(value = "工作单位:其它工作单位")
	private String otherUnit ; 

	/** 其它工作单位 **/
    @ApiModelProperty(value = "其它工作单位")
	private String otherUnit2 ; 

	/** 工作单位 **/
    @ApiModelProperty(value = "工作单位")
	private String unit ; 

	/** 其它工作单位 **/
    @ApiModelProperty(value = "其它工作单位")
	private String otherUnit3 ; 

	/** 教育背景 **/
    @ApiModelProperty(value = "教育背景")
	private String educationBackground ; 

	/** 外语语种 **/
    @ApiModelProperty(value = "外语语种")
	private String foreignLanguages ; 

	/** 工作简历 **/
    @ApiModelProperty(value = "工作简历")
	private String jobResume ; 

	/** 国内外学术或专业团体任职情况 **/
    @ApiModelProperty(value = "国内外学术或专业团体任职情况")
	private String coEmployment ; 

	/** 工作职务 **/
    @ApiModelProperty(value = "工作职务")
	private String jobPosition ; 

	/** 专家荣誉 **/
    @ApiModelProperty(value = "专家荣誉")
	private String expertsHonor ; 

	/** 技术职称 **/
    @ApiModelProperty(value = "技术职称")
	private String technicalTitles ; 

	/** 通讯地址 **/
    @ApiModelProperty(value = "通讯地址")
	private String address ; 

	/** 邮码 **/
    @ApiModelProperty(value = "邮码")
	private String postalCode ; 

	/** 区位 **/
    @ApiModelProperty(value = "区位")
	private String zoneBit ; 

	/** 省 **/
    @ApiModelProperty(value = "省")
	private String province ; 

	/** 市 **/
    @ApiModelProperty(value = "市")
	private String city ; 

	/** 县 **/
    @ApiModelProperty(value = "县")
	private String county ; 

	/** 行政码 **/
    @ApiModelProperty(value = "行政码")
	private String administrativeCode ; 

	/** 电话 **/
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 传真 **/
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 电子信箱 **/
    @ApiModelProperty(value = "电子信箱")
	private String email ; 

	/** 专业领域与研究方向 **/
    @ApiModelProperty(value = "专业领域与研究方向")
	private String professionalResearch ; 

	/** 专业领域与研究方向 **/
    @ApiModelProperty(value = "专业领域与研究方向")
	private String professionalResearch2 ; 

	/** 院士 **/
    @ApiModelProperty(value = "院士")
	private String academician ; 

	/** 院士 **/
    @ApiModelProperty(value = "院士")
	private String academician2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String awards ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String awardt ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String foreign ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String exptype ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String byind ; 

	/** 学科分类名 **/
    @ApiModelProperty(value = "学科分类名")
	private String disciplineClass ; 

	/** 学科分类名 **/
    @ApiModelProperty(value = "学科分类名")
	private String disciplineClass2 ; 

	/** 学科分类码 **/
    @ApiModelProperty(value = "学科分类码")
	private String disciplineClassCode ; 

	/** 中图分类 **/
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String cls2 ; 
}
