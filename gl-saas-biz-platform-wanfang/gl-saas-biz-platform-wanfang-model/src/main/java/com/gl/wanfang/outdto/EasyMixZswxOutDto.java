package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 整合知识文献 
 * @author code_generator
 */

@Data
public class EasyMixZswxOutDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private String id ;

	/** 摘要 **/
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String org ;

	/** 专利类型 **/
    @ApiModelProperty(value = "专利类型")
	private String patenttypeName ;

	/** 名称 **/
    @ApiModelProperty(value = "名称")
	private String title1 ;

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private String date2 ;

	/** 中文标题:英文标题 **/
    @ApiModelProperty(value = "中文标题:英文标题")
	private String cETitle ;

	/** 中文作者 **/
    @ApiModelProperty(value = "中文作者")
	private String chineseAuthor ;

	/** 作者FID **/
    @ApiModelProperty(value = "作者FID")
	private String authorFid ;

	/** 年 **/
    @ApiModelProperty(value = "年")
	private String year ;

	/** 期 **/
    @ApiModelProperty(value = "期")
	private String period ;

	/** 中文摘要 **/
    @ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ;
}
