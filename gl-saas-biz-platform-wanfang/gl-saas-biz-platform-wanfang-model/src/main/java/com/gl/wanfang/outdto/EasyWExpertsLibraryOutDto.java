package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWExpertsLibraryOutDto {

    /** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
    private String id ;

    /** 姓名 **/
    @ApiModelProperty(value = "姓名")
    private String name ;

    /** 性别 **/
    @ApiModelProperty(value = "性别")
    private String sex ;
    /**  **/
    @ApiModelProperty(value = "")
    private String age ;
    /** 工作单位:其它工作单位 **/
    @ApiModelProperty(value = "工作单位:其它工作单位")
    private String otherUnit ;
    /** 其它工作单位 **/
    @ApiModelProperty(value = "其它工作单位")
    private String otherUnit2 ;
    /** 工作单位 **/
    @ApiModelProperty(value = "工作单位")
    private String unit ;
    /** 其它工作单位 **/
    @ApiModelProperty(value = "其它工作单位")
    private String otherUnit3 ;


    /** 工作简历 **/
    @ApiModelProperty(value = "工作简历")
    private String jobResume ;


    /** 工作职务 **/
    @ApiModelProperty(value = "工作职务")
    private String jobPosition ;

    /** 专家荣誉 **/
    @ApiModelProperty(value = "专家荣誉")
    private String expertsHonor ;

    /** 技术职称 **/
    @ApiModelProperty(value = "技术职称")
    private String technicalTitles ;

    /** 院士 **/
    @ApiModelProperty(value = "院士")
    private String academician ;





}
