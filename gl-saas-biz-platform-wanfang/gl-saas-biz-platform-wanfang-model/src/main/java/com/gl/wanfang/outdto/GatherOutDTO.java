package com.gl.wanfang.outdto;


/**
 * @auther Qinye
 * @date 2020/5/11 14:02
 * @description
 */
public class GatherOutDTO {
    /**主键id**/
    private String id;
    /**标题id**/
    private String resourceId;
    /**标题**/
    private String title;
    /**访问量**/
    private Integer amount;
    /**模块标识**/
    private String sourcetype;
    /**用户姓名**/
    private String userName;
    /**用户性别**/
    private String userSex;
    /**用户领域**/
    private String userField;
    /**用户行业**/
    private String userIndustry;
    /**用户地区**/
    private String userArea;
    /**服务名称**/
    private String serviceName;
    /**服务来源**/
    private String serviceOrigin;
    /**资源名称**/
    private String sourcesName;
    /**资源来源**/
    private String sourcesOrigin;
    /**资源标识**/
    private String sourcesId;
    /**来源标识：服务？资源？**/
    private String flag;
    /**采集信息**/
    private String collectSource;
    /**采集时间**/
    private String collectTime;
    /**采集方式 0：调度平台采集、1：实时接口采集**/
    private String collectType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserField() {
        return userField;
    }

    public void setUserField(String userField) {
        this.userField = userField;
    }

    public String getUserIndustry() {
        return userIndustry;
    }

    public void setUserIndustry(String userIndustry) {
        this.userIndustry = userIndustry;
    }

    public String getUserArea() {
        return userArea;
    }

    public void setUserArea(String userArea) {
        this.userArea = userArea;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceOrigin() {
        return serviceOrigin;
    }

    public void setServiceOrigin(String serviceOrigin) {
        this.serviceOrigin = serviceOrigin;
    }

    public String getSourcesName() {
        return sourcesName;
    }

    public void setSourcesName(String sourcesName) {
        this.sourcesName = sourcesName;
    }

    public String getSourcesOrigin() {
        return sourcesOrigin;
    }

    public void setSourcesOrigin(String sourcesOrigin) {
        this.sourcesOrigin = sourcesOrigin;
    }

    public String getSourcesId() {
        return sourcesId;
    }

    public void setSourcesId(String sourcesId) {
        this.sourcesId = sourcesId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCollectSource() {
        return collectSource;
    }

    public void setCollectSource(String collectSource) {
        this.collectSource = collectSource;
    }

    public String getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(String collectTime) {
        this.collectTime = collectTime;
    }

    public String getCollectType() {
        return collectType;
    }

    public void setCollectType(String collectType) {
        this.collectType = collectType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getSourcetype() {
        return sourcetype;
    }

    public void setSourcetype(String sourcetype) {
        this.sourcetype = sourcetype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
