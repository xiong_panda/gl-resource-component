package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 机构
 */
@Data
public class EasyWScientificOrgOutDto {
    private static final long serialVersionUID = 1L;

    /** 记录顺序号 **/

    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** 唯一编号 **/

    @ApiModelProperty(value = "唯一编号")
    private String id ;

    /** 全文索引 **/

    @ApiModelProperty(value = "全文索引")
    private String fulltext ;
    /** 通信地址 **/

    @ApiModelProperty(value = "通信地址")
    private String address ;

    /** 机构名称:规范名称:机构曾用名 **/

    @ApiModelProperty(value = "机构名称:规范名称:机构曾用名")
    private String orgNameNorm ;
    /** 机构类别 **/

    @ApiModelProperty(value = "机构类别")
    private String orgType ;

    @ApiModelProperty(value = "学科分类")
    private String disciplineClass ;

    /** 机构名称 **/

    @ApiModelProperty(value = "机构名称")
    private String orgName ;


    /** 机构简介 **/

    @ApiModelProperty(value = "机构简介")
    private String orgIntro ;

    /** 关键词 **/

    @ApiModelProperty(value = "关键词")
    private String keyword ;

}
