package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 科技成果
 * @author code_generator
 */
@Data
public class WTechnologyAchievementsInDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 摘要 **/
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 鉴定部门 **/
    @ApiModelProperty(value = "鉴定部门")
	private String authorizedDepartment ; 

	/** 专利申请号 **/
    @ApiModelProperty(value = "专利申请号")
	private String patentapplicationId ; 

	/** 完成人 **/
    @ApiModelProperty(value = "完成人")
	private String personsInvolved ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fau ; 

	/** 完成人 **/
    @ApiModelProperty(value = "完成人")
	private String personsInvolved2 ; 

	/** 获奖情况 **/
    @ApiModelProperty(value = "获奖情况")
	private String awards ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String awardsName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String awardsAnyname ; 

	/** 应用行业名称 **/
    @ApiModelProperty(value = "应用行业名称")
	private String applicationIndustryName ; 

	/** 中图分类号 **/
    @ApiModelProperty(value = "中图分类号")
	private String clsCode ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String compby ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String compid ; 

	/** 联系地址 **/
    @ApiModelProperty(value = "联系地址")
	private String contactAddress ; 

	/** 联系单位名称 **/
    @ApiModelProperty(value = "联系单位名称")
	private String contactUnitName ; 

	/** 联系人 **/
    @ApiModelProperty(value = "联系人")
	private String contact ; 

	/** 联系电话 **/
    @ApiModelProperty(value = "联系电话")
	private String contactPhoneNumber ; 

	/** 省市 **/
    @ApiModelProperty(value = "省市")
	private String provincesCity ; 

	/** 资料公布日期 **/
    @ApiModelProperty(value = "资料公布日期")
	private String datareleasedDate ; 

	/** 工作起止时间 **/
    @ApiModelProperty(value = "工作起止时间")
	private String workStart ; 

	/** 工作起止时间 **/
    @ApiModelProperty(value = "工作起止时间")
	private String workEnd ; 

	/** 申报日期 **/
    @ApiModelProperty(value = "申报日期")
	private String declareDate ; 

	/** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 申报单位名 **/
    @ApiModelProperty(value = "申报单位名")
	private String declareUnitName ; 

	/** 成果公布日期 **/
    @ApiModelProperty(value = "成果公布日期")
	private String resultspublishedDate ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String dura ; 

	/** 创汇 **/
    @ApiModelProperty(value = "创汇")
	private String foreignExchangeEarning ; 

	/** 信箱 **/
    @ApiModelProperty(value = "信箱")
	private String mail ; 

	/** 传真 **/
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 成果类别 **/
    @ApiModelProperty(value = "成果类别")
	private String resultsType ; 

	/** 成果水平 **/
    @ApiModelProperty(value = "成果水平")
	private String resultsLevel ; 

	/** 完成单位 **/
    @ApiModelProperty(value = "完成单位")
	private String completeUnit ; 

	/** 投资金额 **/
    @ApiModelProperty(value = "投资金额")
	private String investmentAmount ; 

	/** 鉴定日期 **/
    @ApiModelProperty(value = "鉴定日期")
	private String identificationDate ; 

	/** 应用行业码 **/
    @ApiModelProperty(value = "应用行业码")
	private String applicationIndustryCode ; 

	/** 投资说明 **/
    @ApiModelProperty(value = "投资说明")
	private String investmentInstructions ; 

	/** 投资注释 **/
    @ApiModelProperty(value = "投资注释")
	private String investmentAnnotation ; 

	/** 成果简介 **/
    @ApiModelProperty(value = "成果简介")
	private String resultsSummary ; 

	/** 主题词 **/
    @ApiModelProperty(value = "主题词")
	private String keyword ; 

	/** 备注 **/
    @ApiModelProperty(value = "备注")
	private String note ; 

	/** 列入时间 **/
    @ApiModelProperty(value = "列入时间")
	private String included ; 

	/** 机构名称 **/
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 完成单位:规范单位名称 **/
    @ApiModelProperty(value = "完成单位:规范单位名称")
	private String specificationUnitName ; 

	/** 完成单位 **/
    @ApiModelProperty(value = "完成单位")
	private String completionunit ; 

	/** 机构类型 **/
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 推广情况说明 **/
    @ApiModelProperty(value = "推广情况说明")
	private String promotionInfo ; 

	/** 推广方式 **/
    @ApiModelProperty(value = "推广方式")
	private String promotionWay ; 

	/** 推广范围 **/
    @ApiModelProperty(value = "推广范围")
	private String promotionScope ; 

	/** 推广跟踪 **/
    @ApiModelProperty(value = "推广跟踪")
	private String promotionTracking ; 

	/** 产值 **/
    @ApiModelProperty(value = "产值")
	private String outputValue ; 

	/** 推广的必要性及推广预测 **/
    @ApiModelProperty(value = "推广的必要性及推广预测")
	private String promotionNecessityPromotionForecast ;

	/** 计划名称 **/
    @ApiModelProperty(value = "计划名称")
	private String projectName ; 

	/** 成果密级 **/
    @ApiModelProperty(value = "成果密级")
	private String achievementConfidentialityLevel ; 

	/** 成果类型 **/
    @ApiModelProperty(value = "成果类型")
	private String achievementType ; 

	/** 专利项数 **/
    @ApiModelProperty(value = "专利项数")
	private String patentNumber ; 

	/** 公布刊物名、页数 **/
    @ApiModelProperty(value = "公布刊物名、页数")
	private String publicationNamePageNumber ; 

	/** 专利授权号 **/
    @ApiModelProperty(value = "专利授权号")
	private String patentAuthorizationNumber ; 

	/** 登记日期 **/
    @ApiModelProperty(value = "登记日期")
	private String registrationDate ; 

	/** 记录类型 **/
    @ApiModelProperty(value = "记录类型")
	private String recordDate ; 

	/** 推荐日期 **/
    @ApiModelProperty(value = "推荐日期")
	private String recommendedDate ; 

	/** 限制使用 **/
    @ApiModelProperty(value = "限制使用")
	private String limitToUse ; 

	/** 登记号 **/
    @ApiModelProperty(value = "登记号")
	private String registrationNo ; 

	/** 记录状态 **/
    @ApiModelProperty(value = "记录状态")
	private String recordStatus ; 

	/** 登记部门 **/
    @ApiModelProperty(value = "登记部门")
	private String registrationDepartment ; 

	/** 推荐部门 **/
    @ApiModelProperty(value = "推荐部门")
	private String recommendDepartment ; 

	/** 发布单位 **/
    @ApiModelProperty(value = "发布单位")
	private String releaseNnit ; 

	/** 规范单位名称 **/
    @ApiModelProperty(value = "规范单位名称")
	private String specificationUnitName2 ; 

	/** 信息来源 **/
    @ApiModelProperty(value = "信息来源")
	private String sourceOfInfo ; 

	/** 节资 **/
    @ApiModelProperty(value = "节资")
	private String saveMoney ;

	/** 中图分类二级 **/
    @ApiModelProperty(value = "中图分类二级")
	private String clsLevel2 ; 

	/** 中图分类(三级) **/
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/** 转让条件 **/
    @ApiModelProperty(value = "转让条件")
	private String transferConditions ; 

	/** 转让费 **/
    @ApiModelProperty(value = "转让费")
	private String transferFee ; 

	/** 转让内容 **/
    @ApiModelProperty(value = "转让内容")
	private String transferContent ; 

	/** 成果中文名称 **/
    @ApiModelProperty(value = "成果中文名称")
	private String chineseName ; 

	/** 转让注释 **/
    @ApiModelProperty(value = "转让注释")
	private String transferAnnotation ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ischarge ; 

	/** "登记部门码" **/
    @ApiModelProperty(value = "登记部门码")
	private String registrationDepartmentCode ; 

	/** "推荐部门码" **/
    @ApiModelProperty(value = "推荐部门码")
	private String recommendedDepartmentCode ; 

	/** 转让方式 **/
    @ApiModelProperty(value = "转让方式")
	private String transferWay ; 

	/** 转让范围 **/
    @ApiModelProperty(value = "转让范围")
	private String transferScope ; 

	/** 利税 **/
    @ApiModelProperty(value = "利税")
	private String tax ; 

	/** 中图分类顶级 **/
    @ApiModelProperty(value = "中图分类顶级")
	private String clsTop ; 

	/** 资料公布日期 **/
    @ApiModelProperty(value = "资料公布日期")
	private String releasedDate ; 

	/** 中图分类号 **/
    @ApiModelProperty(value = "中图分类号")
	private String clsCode2 ; 

	/** 邮政编码 **/
    @ApiModelProperty(value = "邮政编码")
	private String postCode ;

	/**  **/
    @ApiModelProperty(value = "")
	private String otherId ; 

	/** "主题词" **/
    @ApiModelProperty(value = "主题词")
	private String keyword2 ; 

	/** 机构ID **/
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构层级ID **/
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ;

	/**  **/
    @ApiModelProperty(value = "")
	private String orgFinal ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgFinal2 ; 

	/** 第一机构终级机构ID **/
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String awShort ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String awLevel ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String halfYear ; 

	/** 文献权重 **/
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/** 索引管理字段(无用) **/
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ;

	/**  **/
    @ApiModelProperty(value = "")
	private String author ; 

	/** 作者信息.姓名 **/
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ;

	/** 作者信息.工作单位二级机构 **/
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgInfo ;

	/** 机构信息.机构名称 **/
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ;

	/** 机构信息.3级机构名称 **/
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 


}
