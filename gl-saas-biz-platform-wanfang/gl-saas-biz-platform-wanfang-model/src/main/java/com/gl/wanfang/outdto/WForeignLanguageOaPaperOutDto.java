package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 外文OA论文
 * @author code_generator
 */
@Data
public class WForeignLanguageOaPaperOutDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** Title_Title **/
    @ApiModelProperty(value = "Title_Title")
	private String title ; 

	/** 摘要 **/
    @ApiModelProperty(value = "摘要")
	private String Abstract;

	/** 作者 **/
    @ApiModelProperty(value = "作者")
	private String author ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** Discipline_Keywords **/
    @ApiModelProperty(value = "Discipline_Keywords")
	private String keywords ; 

	/** 中图分类 **/
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 机构名称 **/
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** Creator_ORG **/
    @ApiModelProperty(value = "Creator_ORG")
	private String orgCreator ; 

	/** Date_Issued **/
    @ApiModelProperty(value = "Date_Issued")
	private String issuedDate ; 

	/** Date_Issued **/
    @ApiModelProperty(value = "Date_Issued")
	private String issuedYear ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String iid ; 

	/** IDentifier_DOI **/
    @ApiModelProperty(value = "IDentifier_DOI")
	private String doi ; 

	/** IDentifier_ISSNp **/
    @ApiModelProperty(value = "IDentifier_ISSNp")
	private String issnp ; 

	/** IDentifier_ISSNe **/
    @ApiModelProperty(value = "IDentifier_ISSNe")
	private String issne ; 

	/** Creator_Creator **/
    @ApiModelProperty(value = "Creator_Creator")
	private String authorName3 ; 

	/** Source_Source **/
    @ApiModelProperty(value = "Source_Source")
	private String joucn ; 

	/** Source_Source **/
    @ApiModelProperty(value = "Source_Source")
	private String fjoucn ; 

	/** Source_Vol **/
    @ApiModelProperty(value = "Source_Vol")
	private String sourceVol ; 

	/** Source_Issue **/
    @ApiModelProperty(value = "Source_Issue")
	private String sourceIssue ; 

	/** Source_Page **/
    @ApiModelProperty(value = "Source_Page")
	private String sourcePage ; 

	/** Creator_ORG **/
    @ApiModelProperty(value = "Creator_ORG")
	private String creatorOrg ; 

	/** Date_Download **/
    @ApiModelProperty(value = "Date_Download")
	private String downloadDate ; 

	/** Publisher_Publisher **/
    @ApiModelProperty(value = "Publisher_Publisher")
	private String publisherPublisher ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String wchinesePaperOaYnfree ; 

	/** datalink **/
    @ApiModelProperty(value = "datalink")
	private String dataLink ; 

	/** Discipline_SelfFL **/
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String selffl ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String zcid ;

	/** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String tzcid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String szcid ; 

	/** 中图分类(三级) **/
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String rn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String bn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String dn ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ots ; 


}
