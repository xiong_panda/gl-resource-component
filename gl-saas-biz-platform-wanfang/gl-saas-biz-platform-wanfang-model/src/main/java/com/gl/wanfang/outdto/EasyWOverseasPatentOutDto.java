package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWOverseasPatentOutDto {

    private static final long serialVersionUID = 1L;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    private String title;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    private String Abstract;

    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    private String keyword;

    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    private String orgName;

    /**
     * 专利号
     **/
    @ApiModelProperty(value = "专利号")
    private String patentNo;

    /**
     * 申请号
     **/
    @ApiModelProperty(value = "申请号")
    private String requsetNo;

    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    private String date;

    /**
     * 公开（公告）号
     **/
    @ApiModelProperty(value = "公开（公告）号")
    private String publicationNo;

    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    private String year;

    /**
     * 进入国家日期
     **/
    @ApiModelProperty(value = "进入国家日期")
    private String dec;


}
