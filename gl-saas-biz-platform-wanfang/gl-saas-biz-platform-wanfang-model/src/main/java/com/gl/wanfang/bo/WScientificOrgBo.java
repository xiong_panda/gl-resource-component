package com.gl.wanfang.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 科研机构
 * @author code_generator
 */

@Data
public class WScientificOrgBo implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/

	@ApiModelProperty(value = "记录顺序号")
	private String orderid ;

	/** 唯一编号 **/

	@ApiModelProperty(value = "唯一编号")
	private String id ;

	/** 全文索引 **/

	@ApiModelProperty(value = "全文索引")
	private String fulltext ;

	/** 省 **/

	@ApiModelProperty(value = "省")
	private String province ;

	/** 市 **/

	@ApiModelProperty(value = "市")
	private String city ;

	/** 县 **/

	@ApiModelProperty(value = "县")
	private String county ;

	/** 机构名称:规范名称:机构曾用名 **/

	@ApiModelProperty(value = "机构名称:规范名称:机构曾用名")
	private String orgNameNorm ;

	/**  **/

	@ApiModelProperty(value = "")
	private String nameAnyname1 ;

	/**  **/

	@ApiModelProperty(value = "")
	private String nameAnyname2 ;

	/**  **/

	@ApiModelProperty(value = "")
	private String acname ;

	/** 机构名称 **/

	@ApiModelProperty(value = "机构名称")
	private String orgName ;

	/** 规范名称 **/

	@ApiModelProperty(value = "规范名称")
	private String normName ;

	/** 机构曾用名 **/

	@ApiModelProperty(value = "机构曾用名")
	private String orgNameOther ;

	/** 依托单位 **/

	@ApiModelProperty(value = "依托单位")
	private String rely ;

	/**  **/

	@ApiModelProperty(value = "")
	private String relyAnyname1 ;

	/**  **/

	@ApiModelProperty(value = "")
	private String relyAnyname2 ;

	/** 上级主管单位 **/

	@ApiModelProperty(value = "上级主管单位")
	private String compunit ;

	/** 内部机构名称 **/

	@ApiModelProperty(value = "内部机构名称")
	private String orgNameInternal ;

	/** 下属机构名称 **/

	@ApiModelProperty(value = "下属机构名称")
	private String orgNameSub ;

	/** 重点实验室名称 **/

	@ApiModelProperty(value = "重点实验室名称")
	private String keyname ;

	/** Email **/

	@ApiModelProperty(value = "Email")
	private String email ;

	/** Internet **/

	@ApiModelProperty(value = "Internet")
	private String url ;

	/** 负责人 **/

	@ApiModelProperty(value = "负责人")
	private String person ;

	/** 负责人 **/

	@ApiModelProperty(value = "负责人")
	private String multiperson ;

	/** 通信地址 **/

	@ApiModelProperty(value = "通信地址")
	private String address ;

	/** 电话 **/

	@ApiModelProperty(value = "电话")
	private String telphone ;

	/** 传真 **/

	@ApiModelProperty(value = "传真")
	private String fax ;

	/** 成立年代 **/

	@ApiModelProperty(value = "成立年代")
	private Date year ;

	/** 机构简介 **/

	@ApiModelProperty(value = "机构简介")
	private String orgIntro ;

	/** 职工人数 **/

	@ApiModelProperty(value = "职工人数")
	private String emps ;

	/** 科研人数 **/

	@ApiModelProperty(value = "科研人数")
	private String techs ;

	/** 主要研究人员 **/

	@ApiModelProperty(value = "主要研究人员")
	private String leadresh ;

	/** 获奖情况 **/

	@ApiModelProperty(value = "获奖情况")
	private String awards ;

	/** 拥有专利数 **/

	@ApiModelProperty(value = "拥有专利数")
	private String patents ;

	/** 科研成果 **/

	@ApiModelProperty(value = "科研成果")
	private String patent ;

	/** 拥有专利 **/

	@ApiModelProperty(value = "拥有专利")
	private String profit ;

	/** 科研设备 **/

	@ApiModelProperty(value = "科研设备")
	private String sciasset ;

	/** 学科研究范围 **/

	@ApiModelProperty(value = "学科研究范围")
	private String reshdis ;

	/** 进展中课题 **/

	@ApiModelProperty(value = "进展中课题")
	private String matter ;

	/** 推广技术与项目 **/

	@ApiModelProperty(value = "推广技术与项目")
	private String project ;

	/** 产品信息 **/

	@ApiModelProperty(value = "产品信息")
	private String wScientificOrg ;

	/** 出版刊物 **/

	@ApiModelProperty(value = "出版刊物")
	private String jour ;

	/** 机构类别 **/

	@ApiModelProperty(value = "机构类别")
	private String orgType ;

	/** 所在地代码 **/

	@ApiModelProperty(value = "所在地代码")
	private String placeCode ;

	/** 学科分类 **/

	@ApiModelProperty(value = "学科分类")
	private String disciplineClass ;

	/** 学科代码 **/

	@ApiModelProperty(value = "学科代码")
	private String disciplineCode ;

	/** 学科代码 **/

	@ApiModelProperty(value = "学科代码")
	private String disciplineCode2 ;

	/** 关键词 **/

	@ApiModelProperty(value = "关键词")
	private String keyword ;

	/** 机构ID **/

	@ApiModelProperty(value = "机构ID")
	private String orgId ;

	/** 科研机构推介信息 **/

	@ApiModelProperty(value = "科研机构推介信息")
	private String orgRecommendations ;

	/** 是否成品数据 **/

	@ApiModelProperty(value = "是否成品数据")
	private String finishedProduct ;


}
