package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWanFangEnterpriseProductDatabaseOutDto {
    private static final long serialVersionUID = 1L;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 企业名称:铭牌:简称:曾用名
     **/
    @ApiModelProperty(value = "企业名称:铭牌:简称:曾用名")
    private String corpFullname;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    private String titleAnyname;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    private String anyname;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    private String acname;

    /**
     * 企业名称
     **/
    @ApiModelProperty(value = "企业名称")
    private String corpName;

    /**
     * 铭牌
     **/
    @ApiModelProperty(value = "铭牌")
    private String corpNameplate;

    /**
     * 简称
     **/
    @ApiModelProperty(value = "简称")
    private String corpShortname;

    /**
     * 曾用名
     **/
    @ApiModelProperty(value = "曾用名")
    private String corpOldname;


    /**
     * 地址 w
     **/
    @ApiModelProperty(value = "地址")
    private String corpAddr;


    /**
     * 性质与级别  s
     **/
    @ApiModelProperty(value = "性质与级别")
    private String corpLevel;

    /**
     * 机构类型
     **/
    @ApiModelProperty(value = "机构类型")
    private String orgType;

    /**
     * 企业简介  s
     **/
    @ApiModelProperty(value = "企业简介")
    private String corpIntro;

    /**
     * 行业GBM
     **/
    @ApiModelProperty(value = "行业GBM")
    private String figbm;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    private String figbmAnyname;

    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    private String orgName;

    /**
     * 行业SIC
     **/
    @ApiModelProperty(value = "行业SIC")
    private String isic;

    /**
     * 产品信息
     **/
    @ApiModelProperty(value = "产品关键词:英文产品关键词")
    private String productInfo;

    /**
     * 产品关键词:英文产品关键词
     **/
    @ApiModelProperty(value = "产品关键词:英文产品关键词")
    private String fullkeyword;

    /**
     * 机构ID
     **/
    @ApiModelProperty(value = "机构ID")
    private String orgId;

}
