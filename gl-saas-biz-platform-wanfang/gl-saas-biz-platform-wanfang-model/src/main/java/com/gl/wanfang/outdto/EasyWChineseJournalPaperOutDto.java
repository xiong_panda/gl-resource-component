package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 中文期刊论文
 */
@Data
public class EasyWChineseJournalPaperOutDto {

    /**
     * 唯一编号
     **/

    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 记录顺序号
     **/

    @ApiModelProperty(value = "记录顺序号")
    private String orderid;


    /**
     * w_ID
     **/
    @ApiModelProperty(value = "w_ID")
    private String wId;

    /**
     * C_ID
     **/

    @ApiModelProperty(value = "C_ID")
    private String cId;

    /**
     * L_ID
     **/

    @ApiModelProperty(value = "L_ID")
    private String lId;

    /**
     * 中文标题
     **/

    @ApiModelProperty(value = "中文标题")
    private String chineseTitle;

    /**
     * 英文标题
     **/
    @ApiModelProperty(value = "英文标题")
    private String englishTitle;

    /**
     * 中文作者
     **/

    @ApiModelProperty(value = "中文作者")
    private String chineseAuthor;


    /**
     * 作者FID
     **/

    @ApiModelProperty(value = "作者FID")
    private String authorFid;


    /**
     * 中文刊名
     **/

    @ApiModelProperty(value = "中文刊名")
    private String chineseJournalName;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    private String journal;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    private String journalAnyname;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    private String journalAnyname2;

    /**
     * 中文刊名
     **/

    @ApiModelProperty(value = "中文刊名")
    private String chineseJournalName2;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    private String fjoucnAnyname;

    /**
     *
     **/

    @ApiModelProperty(value = "")
    private String fjoucnAnyname2;

    /**
     * 日期
     **/

    @ApiModelProperty(value = "日期")
    private String date;


    /**
     * 期
     **/

    @ApiModelProperty(value = "期")
    private String period;


    /**
     * 中文关键词
     **/

    @ApiModelProperty(value = "中文关键词")
    private String chineseKeyword;


    /**
     * 机标关键词
     **/

    @ApiModelProperty(value = "机标关键词")
    private String machineKeyword;

    /**
     * 中文摘要
     **/

    @ApiModelProperty(value = "中文摘要")
    private String chineseAbstract;


    /**
     * 数据来源
     **/

    @ApiModelProperty(value = "数据来源")
    private String dataSource;

    /**
     * 标题
     */
    private String cETitle;


    /**
     * 发表时间
     */
    private String year;

    /**
     * 摘要
     */
    private String cParagraph;
}
