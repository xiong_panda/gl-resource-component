package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 中文专利
 */
@Data
public class EasyWChinesePatentOUTDTO {
    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    private String Abstract;

    /**
     * 公开（公告）号
     **/
    @ApiModelProperty(value = "公开（公告）号")
    private String publicationNo;

    /**
     * 公开（公告）日
     **/
    @ApiModelProperty(value = "公开（公告）日")
    private Date publicationDate;

    /**
     * 发明（设计）人
     **/
    @ApiModelProperty(value = "发明（设计）人")
    private String author;

    /**
     * 发明（设计）人
     **/
    @ApiModelProperty(value = "发明（设计）人")
    private String author2;


    /**
     * 颁证日
     **/
    @ApiModelProperty(value = "颁证日")
    private Date createDate;


    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    private Date date;


    /**
     * 国际申请
     **/
    @ApiModelProperty(value = "国际申请")
    private String intApp;

    /**
     * 国际公布
     **/
    @ApiModelProperty(value = "国际公布")
    private String intPush;

    /**
     * 失效专利
     **/
    @ApiModelProperty(value = "失效专利")
    private String invPat;

    /**
     * 关键词
     **/
    @ApiModelProperty(value = "关键词")
    private String keyword;



    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    private String org;

    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    private String orgAnyname2Name;

    /**
     * 规范单位名称
     **/
    @ApiModelProperty(value = "规范单位名称")
    private String orgNormName2;

    /**
     * 专利号
     **/
    @ApiModelProperty(value = "专利号")
    private String patentnumberName;

    /**
     * 专利类型
     **/
    @ApiModelProperty(value = "专利类型")
    private String patenttypeName;


    /**
     * 发布路径
     **/
    @ApiModelProperty(value = "发布路径")
    private String pubPath;


    /**
     * 申请号
     **/
    @ApiModelProperty(value = "申请号")
    private String requestNumber;

    /**
     * 申请号wjy
     **/
    @ApiModelProperty(value = "申请号wjy")
    private String requestNo;

    /**
     * 申请（专利权）人
     **/
    @ApiModelProperty(value = "申请（专利权）人")
    private String requestPeople;


    /**
     * 数据来源
     **/
    @ApiModelProperty(value = "数据来源")
    private String dataSource;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    private String title1;

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    private String title2;


    /**
     * 申请日
     **/
    @ApiModelProperty(value = "申请日")
    private String date2;


}
