package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文会议引文 
 * @author code_generator
 */
@Data
public class WChineseConferenceCitationsInDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	/** 全文索引 **/
	@ApiModelProperty(value = "全文索引")
	private String fulltext ;

	/** 主题全文检索(标题、关键词、摘要) **/
	@ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String topic ;

	/** 唯一编号 **/
	@ApiModelProperty(value = "唯一编号")
	private String id ;

	/** 记录顺序号 **/
	@ApiModelProperty(value = "记录顺序号")
	private String orderid ;

	/** DOI **/
	@ApiModelProperty(value = "DOI")
	private String doi ;

	/** w_ID **/
	@ApiModelProperty(value = "w_ID")
	private String wid ;

	/**  **/
	@ApiModelProperty(value = "")
	private String nid ;

	/** 论文题名 **/
	@ApiModelProperty(value = "论文题名")
	private String chineseTitle ;

	/** 英文论文题名 **/
	@ApiModelProperty(value = "英文论文题名")
	private String englishTitle ;

	/** 论文题名:英文论文题名 **/
	@ApiModelProperty(value = "论文题名:英文论文题名")
	private String cETitle ;

	/**  **/
	@ApiModelProperty(value = "")
	private String authorId ;

	/** 作者姓名 **/
	@ApiModelProperty(value = "作者姓名")
	private String author ;

	/** 作者译名 **/
	@ApiModelProperty(value = "作者译名")
	private String authorTranslatio ;

	/** 第一作者 **/
	@ApiModelProperty(value = "第一作者")
	private String firstAuthorId ;

	/**  **/
	@ApiModelProperty(value = "")
	private String fau ;

	/**  **/
	@ApiModelProperty(value = "")
	private String faue ;

	/** 作者姓名:作者译名 **/
	@ApiModelProperty(value = "作者姓名:作者译名")
	private String tNAuthor ;

	/** 作者名称(全文检索) **/
	@ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ;

	/** 作者名称(全文检索) **/
	@ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ;

	/** 作者单位名称 **/
	@ApiModelProperty(value = "作者单位名称")
	private String authorUnitName ;

	/** 作者单位规范名称 **/
	@ApiModelProperty(value = "作者单位规范名称")
	private String authorUnitSpecificationName ;

	/** 作者单位译名 **/
	@ApiModelProperty(value = "作者单位译名")
	private String authorUnitTranslation ;

	/**  **/
	@ApiModelProperty(value = "")
	private String forg ;

	/** 作者单位名称 **/
	@ApiModelProperty(value = "作者单位名称")
	private String forgUnitName ;

	/**  **/
	@ApiModelProperty(value = "")
	private String forge ;

	/** 机构名称 **/
	@ApiModelProperty(value = "机构名称")
	private String orgName ;

	/** 机构名称(全文检索) **/
	@ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ;

	/** 作者单位名称:作者单位规范名称:作者单位译名 **/
	@ApiModelProperty(value = "作者单位名称:作者单位规范名称:作者单位译名")
	private String uSAuthorAnyname ;

	/** C_ID **/
	@ApiModelProperty(value = "C_ID")
	private String cid ;

	/** 机标分类号 **/
	@ApiModelProperty(value = "机标分类号")
	private String machineLabelClassCode ;

	/** 中图分类号 **/
	@ApiModelProperty(value = "中图分类号")
	private String clsCode ;

	/** 中图分类二级 **/
	@ApiModelProperty(value = "中图分类二级")
	private String clsLevel2 ;

	/** 中图分类顶级 **/
	@ApiModelProperty(value = "中图分类顶级")
	private String clsTop ;

	/** 中图分类(三级) **/
	@ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ;

	/**  **/
	@ApiModelProperty(value = "")
	private String iid ;

	/** 学科分类:学科分类机标 **/
	@ApiModelProperty(value = "学科分类:学科分类机标")
	private String disciplineClassCode ;

	/** 协会级别 **/
	@ApiModelProperty(value = "协会级别")
	private String associationLevel ;

	/** 中文关键词 **/
	@ApiModelProperty(value = "中文关键词")
	private String chineseKeyword ;

	/** 英文关键词 **/
	@ApiModelProperty(value = "英文关键词")
	private String englishKeyword ;

	/** 机标关键词 **/
	@ApiModelProperty(value = "机标关键词")
	private String machineKeyword ;

	/** 中文关键词:英文关键词:机标关键词 **/
	@ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String cEKeyword ;

	/** 中文摘要 **/
	@ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ;

	/** 英文摘要 **/
	@ApiModelProperty(value = "英文摘要")
	private String englishAbstract ;

	/**  **/
	@ApiModelProperty(value = "")
	private String fAbstract ;

	/** 中文摘要:英文摘要 **/
	@ApiModelProperty(value = "中文摘要:英文摘要")
	private String cEAbstract ;

	/**  **/
	@ApiModelProperty(value = "")
	private String lan ;

	/** 母体文献 **/
	@ApiModelProperty(value = "母体文献")
	private String parentLiterature ;

	/**  **/
	@ApiModelProperty(value = "")
	private String parentLiteratureName ;

	/**  **/
	@ApiModelProperty(value = "")
	private String parentLiteratureName2 ;

	/** 学会名称 **/
	@ApiModelProperty(value = "学会名称")
	private String instituteName ;

	/**  **/
	@ApiModelProperty(value = "")
	private String instituteNameAnyname ;

	/**  **/
	@ApiModelProperty(value = "")
	private String snAnyname ;

	/** 会议名称 **/
	@ApiModelProperty(value = "会议名称")
	private String conferenceTitle ;

	/**  **/
	@ApiModelProperty(value = "")
	private String conferenceTitleAnyname ;

	/** 会议届次 **/
	@ApiModelProperty(value = "会议届次")
	private String conferenceSessions ;

	/** 会议地点 **/
	@ApiModelProperty(value = "会议地点")
	private String conferenceMeetingPlace ;

	/**  **/
	@ApiModelProperty(value = "")
	private String hid ;

	/** 主办单位 **/
	@ApiModelProperty(value = "主办单位")
	private String organizer ;

	/** 基金 **/
	@ApiModelProperty(value = "基金")
	private String fund ;

	/** 基金全文检索 **/
	@ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ;

	/** 基金 **/
	@ApiModelProperty(value = "基金")
	private String fFund ;

	/**  **/
	@ApiModelProperty(value = "")
	private String fpn ;

	/**  **/
	@ApiModelProperty(value = "")
	private String fundsupport ;

	/** 会议时间 **/
	@ApiModelProperty(value = "会议时间")
	private String startMeetingDate ;

	/** 会议时间 **/
	@ApiModelProperty(value = "会议时间")
	private String endMeetingDate ;

	/** 出版时间 **/
	@ApiModelProperty(value = "出版时间")
	private String startPublishedDate ;

	/** 出版时间 **/
	@ApiModelProperty(value = "出版时间")
	private String endPublishedDate ;

	/** 数据来源 **/
	@ApiModelProperty(value = "数据来源")
	private String dataSource ;

	/**  **/
	@ApiModelProperty(value = "")
	private String orgnum ;

	/**  **/
	@ApiModelProperty(value = "")
	private String rn ;

	/** 机构层级ID **/
	@ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ;

	/** 作者单位ID **/
	@ApiModelProperty(value = "作者单位ID")
	private String authorUnitId ;

	/** 机构所在省 **/
	@ApiModelProperty(value = "机构所在省")
	private String orgProvince ;

	/** 机构所在市 **/
	@ApiModelProperty(value = "机构所在市")
	private String orgCity ;

	/** 机构类型 **/
	@ApiModelProperty(value = "机构类型")
	private String orgType ;

	/**  **/
	@ApiModelProperty(value = "")
	private String forgid ;

	/**  **/
	@ApiModelProperty(value = "")
	private String endorgtype ;

	/** 第一机构终级机构ID **/
	@ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ;

	/** 第一机构层级机构ID **/
	@ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ;

	/** 第一机构所在市 **/
	@ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ;

	/** 第一机构终级机构所在市 **/
	@ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ;

	/** 第一机构所在省 **/
	@ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ;

	/** 第一机构所在省 **/
	@ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ;

	/** 第一机构终级机构类型 **/
	@ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ;

	/** 第一机构类型 **/
	@ApiModelProperty(value = "第一机构类型")
	private String orgFristType ;

	/**  **/
	@ApiModelProperty(value = "")
	private String quarter ;

	/**  **/
	@ApiModelProperty(value = "")
	private String halfyear ;

	/** 文献权重 **/
	@ApiModelProperty(value = "文献权重")
	private String literatureWeight ;

	/** 索引管理字段(无用) **/
	@ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ;

	/**  **/
	@ApiModelProperty(value = "")
	private String authorInfo ;

	/** 作者信息.姓名 **/
	@ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ;

	/** 作者信息.作者次序 **/
	@ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ;

	/** 作者信息.工作单位 **/
	@ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ;

	/** 作者信息.工作单位一级机构 **/
	@ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ;

	/** 作者信息.工作单位二级机构 **/
	@ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ;

	/** 作者信息.工作单位类型 **/
	@ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ;

	/** 作者信息.工作单位所在省 **/
	@ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ;

	/** 作者信息.工作单位所在市 **/
	@ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ;

	/** 作者信息.工作单位所在县 **/
	@ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ;

	/** 作者信息.唯一ID **/
	@ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ;

	/** 作者信息.工作单位唯一ID **/
	@ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ;

	/**  **/
	@ApiModelProperty(value = "")
	private String orgInfo ;

	/** 机构信息.机构名称 **/
	@ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ;

	/** 机构信息.机构次序 **/
	@ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ;

	/** 机构信息.机构类型 **/
	@ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ;

	/** 机构信息.省 **/
	@ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ;

	/** 机构信息.市 **/
	@ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ;

	/** 机构信息.县 **/
	@ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ;

	/** 机构信息.五级机构层级码 **/
	@ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ;

	/** 机构信息.1级机构名称 **/
	@ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ;

	/** 机构信息.2级机构名称 **/
	@ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ;

	/** 机构信息.3级机构名称 **/
	@ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ;

	/** 机构信息.4级机构名称 **/
	@ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ;

	/** 机构信息.5级机构名称 **/
	@ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ;


}
