package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWHigherLearningUniversitiesOutDto {
    private static final long serialVersionUID = 1L;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 学校名称
     **/
    @ApiModelProperty(value = "学校名称")
    private String name;

    /**
     * 学校名称
     **/
    @ApiModelProperty(value = "学校名称")
    private String schoolName;

    /**
     * 负责人
     **/
    @ApiModelProperty(value = "负责人")
    private String person;

    /**
     * 曾用名
     **/
    @ApiModelProperty(value = "曾用名")
    private String ohtherName;

    /**
     * 主管单位
     **/
    @ApiModelProperty(value = "主管单位")
    private String compunit;

    /**
     *
     **/
    @ApiModelProperty(value = "")
    private String compunitIko;


    /**
     * 地址
     **/
    @ApiModelProperty(value = "地址")
    private String address;


    /**
     * 办学类型
     **/
    @ApiModelProperty(value = "办学类型")
    private String schoolType;

    /**
     * 重点学科
     **/
    @ApiModelProperty(value = "重点学科")
    private String keydiscipline;

    /**
     * 特色高校
     **/
    @ApiModelProperty(value = "特色高校")
    private String specialSchool;

    /**
     * 学校简介
     **/
    @ApiModelProperty(value = "学校简介")
    private String intro;

    /**
     * 机构ID
     **/
    @ApiModelProperty(value = "机构ID")
    private String orgId;
    /**
     * 办学层次
     **/
    @ApiModelProperty(value = "办学层次")
    private String levels;

    /**
     * 高校类型
     **/
    @ApiModelProperty(value = "高校类型")
    private String collegeType;

}


