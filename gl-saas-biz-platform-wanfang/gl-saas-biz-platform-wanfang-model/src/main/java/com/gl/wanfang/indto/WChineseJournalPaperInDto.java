package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文期刊论文 
 * @author code_generator
 */

@Data
public class WChineseJournalPaperInDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/

    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/

    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String topic ; 

	/** 唯一编号 **/

    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/

    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** DOI **/

    @ApiModelProperty(value = "DOI")
	private String doi ; 

	/** w_ID **/

    @ApiModelProperty(value = "w_ID")
	private String wId ; 

	/** V_ID **/

    @ApiModelProperty(value = "V_ID")
	private String vId ; 

	/** C_ID **/

    @ApiModelProperty(value = "C_ID")
	private String cId ; 

	/** L_ID **/

    @ApiModelProperty(value = "L_ID")
	private String lId ; 

	/** 中文标题 **/

    @ApiModelProperty(value = "中文标题")
	private String chineseTitle ; 

	/** 英文标题 **/

    @ApiModelProperty(value = "英文标题")
	private String englishTitle ; 

	/** 中文标题:英文标题 **/

    @ApiModelProperty(value = "中文标题:英文标题")
	private String cETitle ; 

	/** 作者ID **/

    @ApiModelProperty(value = "作者ID")
	private String authorId ; 

	/** 中文作者 **/

    @ApiModelProperty(value = "中文作者")
	private String chineseAuthor ; 

	/** 英文作者 **/

    @ApiModelProperty(value = "英文作者")
	private String englishAuthor ; 

	/** 作者个数 **/

    @ApiModelProperty(value = "作者个数")
	private String numberAuthors ; 

	/** 作者FID **/

    @ApiModelProperty(value = "作者FID")
	private String authorFid ; 

	/** 中文第一作者 **/

    @ApiModelProperty(value = "中文第一作者")
	private String chineseFirstAuthor ; 

	/** 英文第一作者 **/

    @ApiModelProperty(value = "英文第一作者")
	private String englishFirstAuthor ; 

	/** 作者个数 **/

    @ApiModelProperty(value = "作者个数")
	private String authorNumber ; 

	/** 中文作者:英文作者 **/

    @ApiModelProperty(value = "中文作者:英文作者")
	private String chineseEnglishAuthor ; 

	/** 作者名称(全文检索) **/

    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/

    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** 规范单位名称 **/

    @ApiModelProperty(value = "规范单位名称")
	private String authorityUnitName ; 

	/** 中文作者单位 **/

    @ApiModelProperty(value = "中文作者单位")
	private String chineseAuthorUnit ; 

	/** 英文作者单位 **/

    @ApiModelProperty(value = "英文作者单位")
	private String englishAuthorUnit ; 

	/** 中文第一作者单位 **/

    @ApiModelProperty(value = "中文第一作者单位")
	private String chineseFirstAuthorUnit ; 

	/** 中文第一作者单位一级名称 **/

    @ApiModelProperty(value = "中文第一作者单位一级名称")
	private String chineseFirstAuthorUnitName ; 

	/** 英文第一作者单位 **/

    @ApiModelProperty(value = "英文第一作者单位")
	private String firstEnglishAuthor ; 

	/** 机构名称 **/

    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/

    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 中文作者单位:英文作者单位:规范单位名称 **/

    @ApiModelProperty(value = "中文作者单位:英文作者单位:规范单位名称")
	private String cESAuthorUnit ; 

	/** QKID **/

    @ApiModelProperty(value = "QKID")
	private String qkid ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String issn ; 

	/** 中文刊名 **/

    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName ; 

	/** 英文刊名 **/

    @ApiModelProperty(value = "英文刊名")
	private String englishJournalName ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String journal ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String journalAnyname ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String journalAnyname2 ; 

	/** 中文刊名 **/

    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName2 ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String fjoucnAnyname ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String fjoucnAnyname2 ; 

	/** 日期 **/

    @ApiModelProperty(value = "日期")
	private String date ; 

	/** 年 **/

    @ApiModelProperty(value = "年")
	private String year ; 

	/** 卷 **/

    @ApiModelProperty(value = "卷")
	private String volume ; 

	/** 期 **/

    @ApiModelProperty(value = "期")
	private String period ; 

	/** 页码 **/

    @ApiModelProperty(value = "页码")
	private String pageNumber ; 

	/** 页数 **/

    @ApiModelProperty(value = "页数")
	private String pages ; 

	/** 中文栏目名称 **/

    @ApiModelProperty(value = "中文栏目名称")
	private String chineseColumnName ; 

	/** 英文栏目名称 **/

    @ApiModelProperty(value = "英文栏目名称")
	private String englishColumnName ; 

	/** 语种 **/

    @ApiModelProperty(value = "语种")
	private String language ; 

	/** 中图分类号:机标分类号 **/

    @ApiModelProperty(value = "中图分类号:机标分类号")
	private String clsMachineStandardCode ; 

	/** 学科分类 **/

    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 文献标识码 **/

    @ApiModelProperty(value = "文献标识码")
	private String literatureIdentificationCode ; 

	/** 机标分类号 **/

    @ApiModelProperty(value = "机标分类号")
	private String machineLabelClassCode ; 

	/** 中图分类号 **/

    @ApiModelProperty(value = "中图分类号")
	private String clsCode ; 

	/** 中图分类二级 **/

    @ApiModelProperty(value = "中图分类二级")
	private String clsLevel2 ; 

	/** 中图分类顶级 **/

    @ApiModelProperty(value = "中图分类顶级")
	private String clsTop ; 

	/** 中图分类(三级) **/

    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String iid ; 

	/** 中文关键词 **/

    @ApiModelProperty(value = "中文关键词")
	private String chineseKeyword ; 

	/** 中文关键词:英文关键词:机标关键词 **/

    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String ceKeyword ; 

	/** 英文关键词 **/

    @ApiModelProperty(value = "英文关键词")
	private String englishKeyword ; 

	/** 机标关键词 **/
	
    @ApiModelProperty(value = "机标关键词")
	private String machineKeyword ; 

	/** 中文摘要 **/

    @ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ; 

	/** 中文摘要:英文摘要:正文首段 **/

    @ApiModelProperty(value = "中文摘要:英文摘要:正文首段")
	private String cParagraph ; 

	/** 英文摘要 **/

    @ApiModelProperty(value = "英文摘要")
	private String englishAbstract ; 

	/** 正文首段 **/

    @ApiModelProperty(value = "正文首段")
	private String firstParagraph ; 

	/** 基金 **/

    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/

    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/** 基金名称 **/

    @ApiModelProperty(value = "基金名称")
	private String fundName ; 

	/** 基金名称 **/

    @ApiModelProperty(value = "基金名称")
	private String fundName2 ; 

	/** 基金项目 **/

    @ApiModelProperty(value = "基金项目")
	private String fundProject ; 

	/** 基金名称 **/

    @ApiModelProperty(value = "基金名称")
	private String fundName3 ; 

	/** BYCS **/

    @ApiModelProperty(value = "BYCS")
	private String bycs ; 

	/** 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊 **/

    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
	private String core ; 

	/** 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊 **/

    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
	private String score ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String ecore ; 

	/** 数据来源 **/

    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/** 机构层级ID **/

    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构ID **/

    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构所在省 **/

    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/

    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/** 机构类型 **/

    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String forgid ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String endorgtype ; 

	/** 第一机构终级机构ID **/

    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/

    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在市 **/

    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构所在市 **/

    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在省 **/

    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/

    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构类型 **/

    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/

    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/** 全部作者ID **/

    @ApiModelProperty(value = "全部作者ID")
	private String allAuthorId ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String orgNumber ; 

	/** 文献权重 **/

    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String halfyear ; 

	/** 索引管理字段(无用) **/

    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String authorInfo ; 

	/** 作者信息.姓名 **/

    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/

    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/

    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/

    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/

    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/

    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/

    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/

    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/

    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/

    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/

    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/

    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/

    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/

    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/

    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
	
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
	
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
	
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
	
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
	
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
	
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
	
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
	
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
	
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 


}
