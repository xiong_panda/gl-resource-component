package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 万方外文期刊论文
 */
@Data
public class EasyWForeignPeriodicalPaperOutDto {
    /**
     * 唯一编号
     **/
    @ApiModelProperty(value = "唯一编号")
    private String id;

    /**
     * 记录顺序号
     **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid;

    /**
     * Title_Title
     **/
    @ApiModelProperty(value = "Title_Title")
    private String title;
    /**
     * 摘要
     **/
    @ApiModelProperty(value = "摘要")
    private String Abstract;

    /**
     * 作者
     **/
    @ApiModelProperty(value = "作者")
    private String author;

    /**
     * 作者名称(全文检索)
     **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    private String authorName;
    /**
     * Discipline_Keywords
     **/
    @ApiModelProperty(value = "Discipline_Keywords")
    private String keyword;
    /**
     * 机构名称
     **/
    @ApiModelProperty(value = "机构名称")
    private String orgName;

    /**
     * Date_Issued
     **/
    @ApiModelProperty(value = "Date_Issued")
    private Date date;
    /**
     * Date_Issued
     **/
    @ApiModelProperty(value = "Date_Issued")
    private Date year;

    /**
     * 基金
     **/
    @ApiModelProperty(value = "基金")
    private String fund;
    /**
     * Fund_info
     **/
    @ApiModelProperty(value = "Fund_info")
    private String fFund;
    /**
     * Core_EI:Core_Sci
     **/
    @ApiModelProperty(value = "Core_EI:Core_Sci")
    private String core;
    /**
     *
     **/
    @ApiModelProperty(value = "")
    private String dataLink;

}
