package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWForeignLanguageOaPaperOutDto {
    /** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
    private String id ;

    /** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** Title_Title **/
    @ApiModelProperty(value = "Title_Title")
    private String title ;

    /** 摘要 **/
    @ApiModelProperty(value = "摘要")
    private String Abstract;

    /** 作者 **/
    @ApiModelProperty(value = "作者")
    private String author ;


    /** Discipline_Keywords **/
    @ApiModelProperty(value = "Discipline_Keywords")
    private String keywords ;


    /** Date_Issued **/
    @ApiModelProperty(value = "Date_Issued")
    private String issuedDate ;

    /** Date_Issued **/
    @ApiModelProperty(value = "Date_Issued")
    private String issuedYear ;

    /**  **/
    @ApiModelProperty(value = "")
    private String iid ;


}
