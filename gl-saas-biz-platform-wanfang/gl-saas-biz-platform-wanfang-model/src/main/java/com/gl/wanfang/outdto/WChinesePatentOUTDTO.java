package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中国专利 
 * @author code_generator
 */
@Data
public class WChinesePatentOUTDTO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ;

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 摘要 **/
    @ApiModelProperty(value = "摘要")
	private String Abstract;

	/** 专利代理机构 **/
    @ApiModelProperty(value = "专利代理机构")
	private String orgAgency ; 

	/** 代理人 **/
    @ApiModelProperty(value = "代理人")
	private String agent ; 

	/** 公开（公告）号 **/
    @ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ; 

	/** 公开（公告）日 **/
    @ApiModelProperty(value = "公开（公告）日")
	private Date publicationDate ; 

	/** 发明（设计）人 **/
    @ApiModelProperty(value = "发明（设计）人")
	private String author ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fAuthor ; 

	/** 发明（设计）人 **/
    @ApiModelProperty(value = "发明（设计）人")
	private String author2 ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** 范畴分类 **/
    @ApiModelProperty(value = "范畴分类")
	private String cate ; 

	/** 光盘号 **/
    @ApiModelProperty(value = "光盘号")
	private String cdId ; 

	/** 颁证日 **/
    @ApiModelProperty(value = "颁证日")
	private Date createDate ; 

	/** 分类号 **/
    @ApiModelProperty(value = "分类号")
	private String classCode ; 

	/** 国省代码 **/
    @ApiModelProperty(value = "国省代码")
	private String countryCode ; 

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private Date date ; 

	/** 数据库标识 **/
    @ApiModelProperty(value = "数据库标识")
	private String databaseIdentity ; 

	/** 进入国家日期 **/
    @ApiModelProperty(value = "进入国家日期")
	private Date dec ; 

	/** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 主权项 **/
    @ApiModelProperty(value = "主权项")
	private String indCla ; 

	/** 法律状态F_LawStatus **/
    @ApiModelProperty(value = "法律状态F_LawStatus")
	private String lawStatus ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ls ; 

	/** 规范单位名称 **/
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName ; 

	/** 国际申请 **/
    @ApiModelProperty(value = "国际申请")
	private String intApp ; 

	/** 国际公布 **/
    @ApiModelProperty(value = "国际公布")
	private String intPush ; 

	/** 失效专利 **/
    @ApiModelProperty(value = "失效专利")
	private String invPat ; 

	/** 关键词 **/
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 主分类号 **/
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode ; 

	/** 中图分类 **/
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 中图分类(二级) **/
    @ApiModelProperty(value = "中图分类(二级)")
	private String clsLevel2 ; 

	/** 分案原申请号 **/
    @ApiModelProperty(value = "分案原申请号")
	private String originRequestNumber ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String org ; 

	/** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String orgAnyname2Name ; 

	/** 规范单位名称 **/
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName2 ; 

	/** 机构类型 **/
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 专利号 **/
    @ApiModelProperty(value = "专利号")
	private String patentnumberName ; 

	/** 专利类型 **/
    @ApiModelProperty(value = "专利类型")
	private String patenttypeName ; 

	/** 页数 **/
    @ApiModelProperty(value = "页数")
	private String pages ; 

	/** 优先权 **/
    @ApiModelProperty(value = "优先权")
	private String priority ; 

	/** 发布路径 **/
    @ApiModelProperty(value = "发布路径")
	private String pubPath ; 

	/** 参考文献 **/
    @ApiModelProperty(value = "参考文献")
	private String reference ; 

	/** 地址 **/
    @ApiModelProperty(value = "地址")
	private String requestAddress ; 

	/** 申请号 **/
    @ApiModelProperty(value = "申请号")
	private String requestNumber ; 

	/** 申请号wjy **/
    @ApiModelProperty(value = "申请号wjy")
	private String requestNo ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ; 

	/** 审查员 **/
    @ApiModelProperty(value = "审查员")
	private String reviewer ; 

	/** 服务器地址 **/
    @ApiModelProperty(value = "服务器地址")
	private String serverAddress ; 

	/** 数据来源 **/
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/** 名称 **/
    @ApiModelProperty(value = "名称")
	private String title1 ; 

	/** 名称 **/
    @ApiModelProperty(value = "名称")
	private String title2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private Date yannodate ; 

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private String date2 ; 

	/** 主分类号 **/
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String smcls ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String tmcls ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ckey ; 

	/** 机构ID **/
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构层级ID **/
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String endOrgType ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String fOrgId ; 

	/** 第一机构终级机构ID **/
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String authorId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgNum ; 

	/** 文献权重 **/
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String halfyear ; 

	/** 索引管理字段(无用) **/
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String authorInfo ; 

	/** 作者信息.姓名 **/
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 


}
