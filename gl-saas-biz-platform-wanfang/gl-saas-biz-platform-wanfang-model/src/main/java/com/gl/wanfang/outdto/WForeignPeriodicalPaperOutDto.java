package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 外文期刊论文
 *
 * @author code_generator
 */
@Data
public class WForeignPeriodicalPaperOutDto implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
    private String id ;

    /** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
    private String fulltext ;

    /** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    private String fullName ;

    /** Title_Title **/
    @ApiModelProperty(value = "Title_Title")
    private String title ;

    /** 摘要 **/
    @ApiModelProperty(value = "摘要")
    private String Abstract ;

    /** 作者 **/
    @ApiModelProperty(value = "作者")
    private String author ;

    /** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    private String authorName ;

    /** 作者名称(全文检索) **/
    @ApiModelProperty(value = "作者名称(全文检索)")
    private String authorName2 ;

    /** Discipline_Keywords **/
    @ApiModelProperty(value = "Discipline_Keywords")
    private String keyword ;

    /** 机构名称 **/
    @ApiModelProperty(value = "机构名称")
    private String orgName ;

    /** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
    private String orgNameSearch ;

    /** Creator_ORG **/
    @ApiModelProperty(value = "Creator_ORG")
    private String creatorOrg ;

    /** Date_Issued **/
    @ApiModelProperty(value = "Date_Issued")
    private Date date ;

    /** Date_Issued **/
    @ApiModelProperty(value = "Date_Issued")
    private Date year ;

    /**  **/
    @ApiModelProperty(value = "")
    private String iid ;

    /** IDentifier_DOI **/
    @ApiModelProperty(value = "IDentifier_DOI")
    private String doi ;

    /** IDentifier_ISSNp **/
    @ApiModelProperty(value = "IDentifier_ISSNp")
    private String issn ;

    /** IDentifier_ISSNe **/
    @ApiModelProperty(value = "IDentifier_ISSNe")
    private String eissn ;

    /** Creator_Creator **/
    @ApiModelProperty(value = "Creator_Creator")
    private String author2 ;

    /** Source_Source **/
    @ApiModelProperty(value = "Source_Source")
    private String joucn ;

    /** Source_Source **/
    @ApiModelProperty(value = "Source_Source")
    private String fjoucn ;

    /** Source_Vol **/
    @ApiModelProperty(value = "Source_Vol")
    private String vol ;

    /** Source_Issue **/
    @ApiModelProperty(value = "Source_Issue")
    private String per ;

    /** Source_Page **/
    @ApiModelProperty(value = "Source_Page")
    private String pg ;

    /** Creator_ORG **/
    @ApiModelProperty(value = "Creator_ORG")
    private String creatorOrg2 ;

    /** Date_Download **/
    @ApiModelProperty(value = "Date_Download")
    private Date downloadDate ;

    /** Publisher_Publisher **/
    @ApiModelProperty(value = "Publisher_Publisher")
    private String pubtype ;

    /** Yn_Free **/
    @ApiModelProperty(value = "Yn_Free")
    private String ynfree ;

    /** Discipline_SelfFL **/
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String cid ;

    /** Discipline_SelfFL **/
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String zcid ;

    /** Discipline_SelfFL **/
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String tzcid ;

    /** Discipline_SelfFL **/
    @ApiModelProperty(value = "Discipline_SelfFL")
    private String szcid ;

    /** 中图分类(三级) **/
    @ApiModelProperty(value = "中图分类(三级)")
    private String clsLevel3 ;

    /** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
    private String disciplineClass ;

    /**  **/
    @ApiModelProperty(value = "")
    private String score ;

    /**  **/
    @ApiModelProperty(value = "")
    private String rn ;

    /**  **/
    @ApiModelProperty(value = "")
    private String bn ;

    /**  **/
    @ApiModelProperty(value = "")
    private String dn ;

    /**  **/
    @ApiModelProperty(value = "")
    private String ots ;

    /** Core_SCI **/
    @ApiModelProperty(value = "Core_SCI")
    private String sci ;

    /** Core_EI **/
    @ApiModelProperty(value = "Core_EI")
    private String ei ;

    /** 基金 **/
    @ApiModelProperty(value = "基金")
    private String fund ;

    /** Fund_info **/
    @ApiModelProperty(value = "Fund_info")
    private String fFund ;

    /** Core_EI:Core_Sci **/
    @ApiModelProperty(value = "Core_EI:Core_Sci")
    private String core ;

    /**  **/
    @ApiModelProperty(value = "")
    private String dataLink ;

    /** Fund_info **/
    @ApiModelProperty(value = "Fund_info")
    private String fundSupport ;

}
