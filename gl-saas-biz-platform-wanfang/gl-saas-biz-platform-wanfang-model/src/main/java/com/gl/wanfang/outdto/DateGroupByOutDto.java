package com.gl.wanfang.outdto;

import lombok.Data;

/**
 * 聚合outDto
 */
@Data
public class DateGroupByOutDto {
    /**
     * 聚合的数据
     */
    String groupData;

    /**
     * 聚合数量
     */
//    Long countNum;

}
