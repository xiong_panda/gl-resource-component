package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;



/**
 * 法律法规
 */
@Data
public class WLawsRegulationsInDTO implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    /** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** 唯一编号  **/
    @ApiModelProperty(value = "唯一编号 ")
    private String id ;

    /** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
    private String fulltext ;

    /** 标题:英文标题 **/
    @ApiModelProperty(value = "标题:英文标题")
    private String title2 ;

    /** 法规正文 **/
    @ApiModelProperty(value = "法规正文")
    private String rulesText ;

    /** 关键词 **/
    @ApiModelProperty(value = "关键词")
    private String keyword ;

    /** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
    private String proclaimDepartment ;

    /** 颁布部门(全文检索) **/
    @ApiModelProperty(value = "颁布部门(全文检索)")
    private String proclaimDepartmentFulltext ;

    /** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
    private String proclaimDepartment2 ;

    /**  **/
    @ApiModelProperty(value = "")
    private String author ;

    /** 中图分类 **/
    @ApiModelProperty(value = "中图分类")
    private String cls ;

    /** 颁布日期 **/
    @ApiModelProperty(value = "颁布日期")
    private String proclaimDate ;

    /** 终审日期 **/
    @ApiModelProperty(value = "终审日期")
    private String finalDate ;

    /**  **/
    @ApiModelProperty(value = "")
    private String rid ;

    /** 标题 **/
    @ApiModelProperty(value = "标题")
    private String title ;

    /** 英文标题 **/
    @ApiModelProperty(value = "英文标题")
    private String englishTitle ;

    /** 发文文号 **/
    @ApiModelProperty(value = "发文文号")
    private String postDocumentCode ;

    /** 机构ID **/
    @ApiModelProperty(value = "机构ID")
    private String orgId ;

    /** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
    private String proclaimDepartment3 ;

    /** 行业地区码_部门代码 **/
    @ApiModelProperty(value = "行业地区码_部门代码")
    private String industrycodeDepartmentCode ;

    /** 效力级别 **/
    @ApiModelProperty(value = "效力级别")
    private String valueLevel ;

    /** 效力代码 **/
    @ApiModelProperty(value = "效力代码")
    private String valueCode ;

    /** 时效性 **/
    @ApiModelProperty(value = "时效性")
    private String timeliness ;

    /** 批准日期 **/
    @ApiModelProperty(value = "批准日期")
    private String approvalDate ;

    /** 签字日期 **/
    @ApiModelProperty(value = "签字日期")
    private String signatureDate ;

    /**  **/
    @ApiModelProperty(value = "")
    private String yapprdate ;

    /**  **/
    @ApiModelProperty(value = "")
    private String ysigndate ;

    /** 实施日期 **/
    @ApiModelProperty(value = "实施日期")
    private String implementationDate ;

    /** 失效日期 **/
    @ApiModelProperty(value = "失效日期")
    private String invalidDate ;

    /** 终审法院 **/
    @ApiModelProperty(value = "终审法院")
    private String finalCourt ;

    /** 终审日期 **/
    @ApiModelProperty(value = "终审日期")
    private String finalDate2 ;

    /** 调解日期 **/
    @ApiModelProperty(value = "调解日期")
    private String mediateDate ;

    /** 内容分类 **/
    @ApiModelProperty(value = "内容分类")
    private String cintentClass ;

    /** 内容分类码 **/
    @ApiModelProperty(value = "内容分类码")
    private String cintentClassCode ;

    /** URL **/
    @ApiModelProperty(value = "URL")
    private String url ;

    /** PDF全文 **/
    @ApiModelProperty(value = "PDF全文")
    private String pdfFulltext ;

    /** 相关链接 **/
    @ApiModelProperty(value = "相关链接")
    private String relevantLink ;

    /** 历史链接 **/
    @ApiModelProperty(value = "历史链接")
    private String historyLink ;

    /** 库别代码 **/
    @ApiModelProperty(value = "库别代码")
    private String libraryCode ;

    /** 制作日期 **/
    @ApiModelProperty(value = "制作日期")
    private String makeDate ;

    /** 行业分类 **/
    @ApiModelProperty(value = "行业分类")
    private String industryClass ;

    /** 行业分类码 **/
    @ApiModelProperty(value = "行业分类码")
    private String industryClassCode ;

    /** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
    private String disciplineClass ;

    /** 索引管理字段(无用) **/
    @ApiModelProperty(value = "索引管理字段(无用)")
    private String errorCode ;

}
