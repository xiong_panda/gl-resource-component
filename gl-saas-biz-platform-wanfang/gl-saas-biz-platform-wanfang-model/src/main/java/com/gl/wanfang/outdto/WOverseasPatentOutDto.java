package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 海外专利 
 * @author code_generator
 */
@Data
public class WOverseasPatentOutDto implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 名称 **/
    @ApiModelProperty(value = "名称")
	private String title ; 

	/** 摘要 **/
    @ApiModelProperty(value = "摘要")
	private String Abstract;

	/** 关键词 **/
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 机构名称 **/
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String org2 ; 

	/** 机构名称(全文检索) **/
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 发明（设计）人 **/
    @ApiModelProperty(value = "发明（设计）人")
	private String author ; 

	/** 专利号 **/
    @ApiModelProperty(value = "专利号")
	private String patentNo ; 

	/** 申请号 **/
    @ApiModelProperty(value = "申请号")
	private String requsetNo ; 

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private String date ; 

	/** 申请（专利权）人 **/
    @ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ; 

	/** 国省代码 **/
    @ApiModelProperty(value = "国省代码")
	private String countryCode ; 

	/** 公开（公告）号 **/
    @ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ; 

	/** 公开（公告）日 **/
    @ApiModelProperty(value = "公开（公告）日")
	private String publicationDate1 ; 

	/** 公开（公告）日 **/
    @ApiModelProperty(value = "公开（公告）日")
	private String publicationDate2 ; 

	/** 颁证日 **/
    @ApiModelProperty(value = "颁证日")
	private String createDate ; 

	/** 主分类号 **/
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode1 ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String omaincls ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String ocls ; 

	/** 主分类号 **/
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ; 

	/** 分类号 **/
    @ApiModelProperty(value = "分类号")
	private String classCode ; 

	/** 外观分类 **/
    @ApiModelProperty(value = "外观分类")
	private String appearanceCode ; 

	/** 本国主分类 **/
    @ApiModelProperty(value = "本国主分类")
	private String countryMainClass ; 

	/** 本国副分类 **/
    @ApiModelProperty(value = "本国副分类")
	private String countryNextClass ; 

	/** 欧洲主分类 **/
    @ApiModelProperty(value = "欧洲主分类")
	private String europeMainClass ; 

	/** 附加分类 **/
    @ApiModelProperty(value = "附加分类")
	private String europeNextClass ; 

	/** 同族专利项 **/
    @ApiModelProperty(value = "同族专利项")
	private String congener ; 

	/** 分案原申请号 **/
    @ApiModelProperty(value = "分案原申请号")
	private String oreqno ; 

	/** 中图分类 **/
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 申请日 **/
    @ApiModelProperty(value = "申请日")
	private String year ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String reqnov ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String patt ; 

	/** 国际申请 **/
    @ApiModelProperty(value = "国际申请")
	private String internationalApplication ; 

	/** 国际公布 **/
    @ApiModelProperty(value = "国际公布")
	private String internationalPublish ; 

	/** WO申请号 **/
    @ApiModelProperty(value = "WO申请号")
	private String woApplicationNo ; 

	/** 进入国家日期 **/
    @ApiModelProperty(value = "进入国家日期")
	private String dec ; 

	/** 优先权 **/
    @ApiModelProperty(value = "优先权")
	private String priority ; 

	/** 优先权日 **/
    @ApiModelProperty(value = "优先权日")
	private String priorityData ; 

	/** 再版原专利 **/
    @ApiModelProperty(value = "再版原专利")
	private String originalPatentNo ; 

	/** 本国参考文献 **/
    @ApiModelProperty(value = "本国参考文献")
	private String homeReference ; 

	/** 国外参考文献 **/
    @ApiModelProperty(value = "国外参考文献")
	private String foreignReference ; 

	/** 非专利参考文献 **/
    @ApiModelProperty(value = "非专利参考文献")
	private String nonref ; 

	/** 主权项 **/
    @ApiModelProperty(value = "主权项")
	private String independentClaim ; 

	/** 机构类型 **/
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 名称 **/
    @ApiModelProperty(value = "名称")
	private String name ; 

	/** 发明（设计）人 **/
    @ApiModelProperty(value = "发明（设计）人")
	private String author2 ; 

	/** 专利代理机构 **/
    @ApiModelProperty(value = "专利代理机构")
	private String orgAgency ; 

	/** 代理人 **/
    @ApiModelProperty(value = "代理人")
	private String agent ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String dbid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String source ; 

	/** 学科分类 **/
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 失效专利 **/
    @ApiModelProperty(value = "失效专利")
	private String invalidPatent ; 

	/** 主分类号显示数 **/
    @ApiModelProperty(value = "主分类号显示数")
	private String mainclsnum ; 

	/** 分类号显示数 **/
    @ApiModelProperty(value = "分类号显示数")
	private String mainCategoryNo ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String requestAddress ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String reference ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String reviewer ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String cdid ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String serAddress ; 

	/**  **/
    @ApiModelProperty(value = "")
	private String publishPath ; 

	/** 索引管理字段(无用) **/
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ;


}
