package com.gl.wanfang.indto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 数据暴露接口使用资源
 */
@Data
public class HxWfServerSearchTimeInDto implements java.io.Serializable{

	private static final long serialVersionUID = 1L;


	/**
	 * 开始时间
	 */
	private String startTime;

	/**
	 * 结束时间
	 */
	private String endTime;

	/**
	 * 校验标识
	 */
	private String collect;


}
