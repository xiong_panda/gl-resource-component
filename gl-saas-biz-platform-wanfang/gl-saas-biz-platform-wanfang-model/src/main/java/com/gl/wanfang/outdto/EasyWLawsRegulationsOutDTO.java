package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWLawsRegulationsOutDTO {

    /** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** 唯一编号  **/
    @ApiModelProperty(value = "唯一编号 ")
    private String id ;

    /** 法规正文 **/
    @ApiModelProperty(value = "法规正文")
    private String rulesText ;

    /** 关键词 **/
    @ApiModelProperty(value = "关键词")
    private String keyword ;

    /** 颁布部门 **/
    @ApiModelProperty(value = "颁布部门")
    private String proclaimDepartment ;


    /**  **/
    @ApiModelProperty(value = "")
    private String author ;

    /** 颁布日期 **/
    @ApiModelProperty(value = "颁布日期")
    private String proclaimDate ;

    /** 终审日期 **/
    @ApiModelProperty(value = "终审日期")
    private String finalDate ;

    /**  **/
    @ApiModelProperty(value = "")
    private String rid ;

    /** 标题 **/
    @ApiModelProperty(value = "标题")
    private String title ;

    /** 发文文号 **/
    @ApiModelProperty(value = "发文文号")
    private String postDocumentCode ;

    /** 效力级别 **/
    @ApiModelProperty(value = "效力级别")
    private String valueLevel ;

    /** 效力代码 **/
    @ApiModelProperty(value = "效力代码")
    private String valueCode ;

    /** 批准日期 **/
    @ApiModelProperty(value = "批准日期")
    private String approvalDate ;

    /** 签字日期 **/
    @ApiModelProperty(value = "签字日期")
    private String signatureDate ;

    /**  **/
    @ApiModelProperty(value = "")
    private String yapprdate ;

    /**  **/
    @ApiModelProperty(value = "")
    private String ysigndate ;

    /** 实施日期 **/
    @ApiModelProperty(value = "实施日期")
    private String implementationDate ;

    /** 终审法院 **/
    @ApiModelProperty(value = "终审法院")
    private String finalCourt ;


    /** 库别代码 **/
    @ApiModelProperty(value = "库别代码")
    private String libraryCode ;

    /** 制作日期 **/
    @ApiModelProperty(value = "制作日期")
    private String makeDate ;



}
