package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 中文OA论文
 */
@Data
public class EasyWChinesePaperOaOutDto {

    private static final long serialVersionUID = 1L;

    /** 唯一编号 **/

    @ApiModelProperty(value = "唯一编号")
    private String id ;


    /** Title_Title **/

    @ApiModelProperty(value = "Title_Title")
    private String title ;

    /** 摘要 **/

    @ApiModelProperty(value = "摘要")
    private String Abstract ;

    /** 作者 **/

    @ApiModelProperty(value = "作者")
    private String author ;

    /** Discipline_Keywords **/

    @ApiModelProperty(value = "Discipline_Keywords")
    private String keyword ;

    /** Date_Issued **/

    @ApiModelProperty(value = "Date_Issued")
    private String issuedYear ;



}
