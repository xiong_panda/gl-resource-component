package com.gl.wanfang.indto;

import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 前台用户表
 *
 * @author code_generator
 */
public class SysUserInDto implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     **/
    private String fPkid;

    /**
     * 手机号码
     **/
    private String mobile;


    /**
     * 用户昵称
     **/
    private String userNickname;

    /**
     * 用户姓名
     **/
    private String userName;

    /**
     * 性别 (1:男；0：女)
     **/
    private String sex;

    /**
     * 所属企业ID
     **/
    private String fMidCompanyid;

    /**
     * 个人用户类型 码值：车主、潜客
     **/
    private String userType;

    /**
     * 企业用户类型 值码：企业超级用户、企业普通用户
     **/
    private String companyUserType;

    /**
     * 用户来源 码值：platform_type
     **/
    private String platformType;

    /**
     * 身份证号
     **/
    private String idNumber;

    /**
     * 生日
     **/
    private String birthday;

    /**
     * 签发机关
     **/
    private String issuingAuthority;

    /**
     * 证件签发日期
     **/
    private String issuingStartTime;

    /**
     * 证件有效截至日期
     **/
    private String issuingEndTime;

    /**
     * 身份证住址
     **/
    private String idCardAddr;

    /**
     * 民族
     **/
    private String nationality;

    /**
     * qq号
     **/
    private String qq;

    /**
     * 支付宝账号
     **/
    private String alipayAccount;

    /**
     * 微信号
     **/
    private String microsignal;

    /**
     * 邮箱
     **/
    private String email;

    /**
     * 邮编
     **/
    private String postcode;

    /**
     * 现居住地址
     **/
    private String contactAddress;

    /**
     * 积分
     **/
    private Integer integral;

    /**
     * 岗位id
     **/
    private String fMidPostid;

    /**
     * 角色ID
     **/
    private String fMidRoleid;

    /**
     * 部门ID 可能有多个部门
     **/
    private String fMidDeptid;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 删除标记（1：已删除，0：正常）
     **/
    private String fIsdelete;

    /**
     * 启用状态
     **/
    private String fEnable;

    /**
     * 乐观锁
     **/
    private String revision;

    /**
     * 版本号
     **/
    private String version;

    /**
     * 创建人
     **/
    private String fInputid;

    /**
     * 创建时间
     **/
    private Date fInputtime;

    /**
     * 更新人
     **/
    private String fEndid;

    /**
     * 更新时间
     **/
    private Date fEndtime;

    /**
     * 时态标记
     **/
    private Integer timestateFlag;

    /**
     * 时态名称
     **/
    private String timestateName;

    /**
     * 可信等级
     **/
    private String creditLevel;

    /**
     * 可信比例
     **/
    private float creditScore;

    /**
     * 传输状态
     **/
    private Integer fTransmission;

    /**
     * 创建人姓名
     **/
    private String fInputname;

    /**
     * 更新人姓名
     **/
    private String fEndname;


    public String getfPkid() {
        return fPkid;
    }

    public void setfPkid(String fPkid) {
        this.fPkid = fPkid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getfMidCompanyid() {
        return fMidCompanyid;
    }

    public void setfMidCompanyid(String fMidCompanyid) {
        this.fMidCompanyid = fMidCompanyid;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCompanyUserType() {
        return companyUserType;
    }

    public void setCompanyUserType(String companyUserType) {
        this.companyUserType = companyUserType;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIssuingAuthority() {
        return issuingAuthority;
    }

    public void setIssuingAuthority(String issuingAuthority) {
        this.issuingAuthority = issuingAuthority;
    }

    public String getIssuingStartTime() {
        return issuingStartTime;
    }

    public void setIssuingStartTime(String issuingStartTime) {
        this.issuingStartTime = issuingStartTime;
    }

    public String getIssuingEndTime() {
        return issuingEndTime;
    }

    public void setIssuingEndTime(String issuingEndTime) {
        this.issuingEndTime = issuingEndTime;
    }

    public String getIdCardAddr() {
        return idCardAddr;
    }

    public void setIdCardAddr(String idCardAddr) {
        this.idCardAddr = idCardAddr;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    public String getMicrosignal() {
        return microsignal;
    }

    public void setMicrosignal(String microsignal) {
        this.microsignal = microsignal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public String getfMidPostid() {
        return fMidPostid;
    }

    public void setfMidPostid(String fMidPostid) {
        this.fMidPostid = fMidPostid;
    }

    public String getfMidRoleid() {
        return fMidRoleid;
    }

    public void setfMidRoleid(String fMidRoleid) {
        this.fMidRoleid = fMidRoleid;
    }

    public String getfMidDeptid() {
        return fMidDeptid;
    }

    public void setfMidDeptid(String fMidDeptid) {
        this.fMidDeptid = fMidDeptid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getfIsdelete() {
        return fIsdelete;
    }

    public void setfIsdelete(String fIsdelete) {
        this.fIsdelete = fIsdelete;
    }

    public String getfEnable() {
        return fEnable;
    }

    public void setfEnable(String fEnable) {
        this.fEnable = fEnable;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getfInputid() {
        return fInputid;
    }

    public void setfInputid(String fInputid) {
        this.fInputid = fInputid;
    }

    public Date getfInputtime() {
        return fInputtime;
    }

    public void setfInputtime(Date fInputtime) {
        this.fInputtime = fInputtime;
    }

    public String getfEndid() {
        return fEndid;
    }

    public void setfEndid(String fEndid) {
        this.fEndid = fEndid;
    }

    public Date getfEndtime() {
        return fEndtime;
    }

    public void setfEndtime(Date fEndtime) {
        this.fEndtime = fEndtime;
    }

    public Integer getTimestateFlag() {
        return timestateFlag;
    }

    public void setTimestateFlag(Integer timestateFlag) {
        this.timestateFlag = timestateFlag;
    }

    public String getTimestateName() {
        return timestateName;
    }

    public void setTimestateName(String timestateName) {
        this.timestateName = timestateName;
    }

    public String getCreditLevel() {
        return creditLevel;
    }

    public void setCreditLevel(String creditLevel) {
        this.creditLevel = creditLevel;
    }

    public float getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(float creditScore) {
        this.creditScore = creditScore;
    }

    public Integer getfTransmission() {
        return fTransmission;
    }

    public void setfTransmission(Integer fTransmission) {
        this.fTransmission = fTransmission;
    }

    public String getfInputname() {
        return fInputname;
    }

    public void setfInputname(String fInputname) {
        this.fInputname = fInputname;
    }

    public String getfEndname() {
        return fEndname;
    }

    public void setfEndname(String fEndname) {
        this.fEndname = fEndname;
    }


    @Override
    public String toString() {
        return "SysUserInDto{" +
                "fPkid='" + fPkid + '\'' +
                ", mobile='" + mobile + '\'' +
                ", userNickname='" + userNickname + '\'' +
                ", userName='" + userName + '\'' +
                ", sex='" + sex + '\'' +
                ", fMidCompanyid='" + fMidCompanyid + '\'' +
                ", userType='" + userType + '\'' +
                ", companyUserType='" + companyUserType + '\'' +
                ", platformType='" + platformType + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", birthday='" + birthday + '\'' +
                ", issuingAuthority='" + issuingAuthority + '\'' +
                ", issuingStartTime='" + issuingStartTime + '\'' +
                ", issuingEndTime='" + issuingEndTime + '\'' +
                ", idCardAddr='" + idCardAddr + '\'' +
                ", nationality='" + nationality + '\'' +
                ", qq='" + qq + '\'' +
                ", alipayAccount='" + alipayAccount + '\'' +
                ", microsignal='" + microsignal + '\'' +
                ", email='" + email + '\'' +
                ", postcode='" + postcode + '\'' +
                ", contactAddress='" + contactAddress + '\'' +
                ", integral=" + integral +
                ", fMidPostid='" + fMidPostid + '\'' +
                ", fMidRoleid='" + fMidRoleid + '\'' +
                ", fMidDeptid='" + fMidDeptid + '\'' +
                ", remark='" + remark + '\'' +
                ", fIsdelete='" + fIsdelete + '\'' +
                ", fEnable='" + fEnable + '\'' +
                ", revision='" + revision + '\'' +
                ", version='" + version + '\'' +
                ", fInputid='" + fInputid + '\'' +
                ", fInputtime=" + fInputtime +
                ", fEndid='" + fEndid + '\'' +
                ", fEndtime=" + fEndtime +
                ", timestateFlag=" + timestateFlag +
                ", timestateName='" + timestateName + '\'' +
                ", creditLevel='" + creditLevel + '\'' +
                ", creditScore=" + creditScore +
                ", fTransmission=" + fTransmission +
                ", fInputname='" + fInputname + '\'' +
                ", fEndname='" + fEndname + '\'' +
                '}';
    }
}
