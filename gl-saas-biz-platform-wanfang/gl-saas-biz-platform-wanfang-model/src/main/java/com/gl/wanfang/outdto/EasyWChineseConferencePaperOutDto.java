package com.gl.wanfang.outdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EasyWChineseConferencePaperOutDto {
    private static final long serialVersionUID = 1L;

    /** 主题全文检索(标题、关键词、摘要) **/
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
    private String topic ;

    /** 唯一编号 **/
    @ApiModelProperty(value = "唯一编号")
    private String id ;

    /** 记录顺序号 **/
    @ApiModelProperty(value = "记录顺序号")
    private String orderid ;

    /** DOI **/
    @ApiModelProperty(value = "DOI")
    private String doi ;

    /** w_ID **/
    @ApiModelProperty(value = "w_ID")
    private String wid ;

    /**  **/
    @ApiModelProperty(value = "")
    private String nid ;

    /** 论文题名 **/
    @ApiModelProperty(value = "论文题名")
    private String chineseTitle ;

    /** 作者姓名 **/
    @ApiModelProperty(value = "作者姓名")
    private String author ;

    /**  **/
    @ApiModelProperty(value = "")
    private String fau ;



    /** C_ID **/
    @ApiModelProperty(value = "C_ID")
    private String cid ;

    /** 中文关键词 **/
    @ApiModelProperty(value = "中文关键词")
    private String chineseKeyword ;

    /** 中文摘要 **/
    @ApiModelProperty(value = "中文摘要")
    private String chineseAbstract ;

    /**  **/
    @ApiModelProperty(value = "")
    private String fAbstract ;

    /** 母体文献 **/
    @ApiModelProperty(value = "母体文献")
    private String parentLiterature ;

    /** 学会名称 **/
    @ApiModelProperty(value = "学会名称")
    private String instituteName ;

    /**  **/
    @ApiModelProperty(value = "")
    private String instituteNameAnyname ;

    /**  **/
    @ApiModelProperty(value = "")
    private String snAnyname ;

    /** 会议名称 **/
    @ApiModelProperty(value = "会议名称")
    private String conferenceTitle ;

    /**  **/
    @ApiModelProperty(value = "")
    private String conferenceTitleAnyname ;

    /** 会议届次 **/
    @ApiModelProperty(value = "会议届次")
    private String conferenceSessions ;

    /** 会议地点 **/
    @ApiModelProperty(value = "会议地点")
    private String conferenceMeetingPlace ;

    /**  **/
    @ApiModelProperty(value = "")
    private String hid ;

    /** 基金 **/
    @ApiModelProperty(value = "基金")
    private String fund ;

    /** 基金 **/
    @ApiModelProperty(value = "基金")
    private String fFund ;

    /** 会议时间 **/
    @ApiModelProperty(value = "会议时间")
    private String startMeetingDate ;

    /** 会议时间 **/
    @ApiModelProperty(value = "会议时间")
    private String endMeetingDate ;

    /** 出版时间 **/
    @ApiModelProperty(value = "出版时间")
    private String startPublishedDate ;

    /** 出版时间 **/
    @ApiModelProperty(value = "出版时间")
    private String endPublishedDate ;

    /** 数据来源 **/
    @ApiModelProperty(value = "数据来源")
    private String dataSource ;

    /**  **/
    @ApiModelProperty(value = "")
    private String forgid ;

    /**  **/
    @ApiModelProperty(value = "")
    private String authorInfo ;

    /** 作者信息.姓名 **/
    @ApiModelProperty(value = "作者信息.姓名")
    private String authorInfoName ;



}
