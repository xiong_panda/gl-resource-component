package com.gl.wanfang.util;

import com.gl.basis.common.util.DateUtils;
import com.gl.basis.common.util.Md5Utils;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.ApplyNumProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class CitySignUtils {

    @Autowired
    ApplyNumProperties applyNumProperties;


    /**
     *
     * @param s
     * @param method 请求的方法路径
     * @param params 请求的参数（转为json）
     *               timestamp               String curDate = DateUtils.getCurDate("yyyy-MM-dd HH:mm:ss");
     * @return
     */
    public String sign(String method, String params, String timestamp) {
        Map<String, String> map = new HashMap<>();
        map.put("method", method);
        map.put("timestamp", timestamp);
        map.put("appkey", applyNumProperties.getAppkey());
        map.put("format", applyNumProperties.getFormat());
        map.put("sign_method", applyNumProperties.getSignMethod());
//        map.put("version", applyNumProperties.getVersion());
        map.put("bz_data", params);
        return Md5Utils.signShaHex(map, applyNumProperties.getSignKey());
    }
}
