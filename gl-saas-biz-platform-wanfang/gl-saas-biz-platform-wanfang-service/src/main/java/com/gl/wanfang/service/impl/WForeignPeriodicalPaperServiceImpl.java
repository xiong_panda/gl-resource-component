package com.gl.wanfang.service.impl;


import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.wanfang.indto.WForeignPeriodicalPaperInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WForeignPeriodicalPaper;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWForeignPeriodicalPaperOutDto;
import com.gl.wanfang.outdto.WForeignLanguageOaPaperOutDto;
import com.gl.wanfang.outdto.WForeignPeriodicalPaperOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWForeignPeriodicalPaperDalService;
import com.gl.wanfang.service.IWForeignPeriodicalPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class WForeignPeriodicalPaperServiceImpl implements IWForeignPeriodicalPaperService {

    @Autowired
    IWForeignPeriodicalPaperDalService foreignPeriodicalPaperDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;


    @Override
    public Page<WForeignPeriodicalPaperOutDto> getListByPage(Page<WForeignPeriodicalPaperInDto> page) {
        List<WForeignPeriodicalPaper> listByPage = foreignPeriodicalPaperDalService.getListByPage(page.getParams());

        //转换
        Page<WForeignPeriodicalPaperOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WForeignPeriodicalPaper> pager = (com.github.pagehelper.Page) listByPage;
            List<WForeignPeriodicalPaperOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WForeignPeriodicalPaperOutDto outDto = ConverBeanUtils.dtoToDo(e, WForeignPeriodicalPaperOutDto.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());

        }
        return dtoPage;
    }

    @Override
    public List<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Year", "w_foreign_periodical_paper");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public WForeignPeriodicalPaperOutDto getOneByParams(Map<String, Object> params) {
        return foreignPeriodicalPaperDalService.getOneByParams(params);
    }

    @Override
    public Page<?> getListByPage2(Page<WForeignPeriodicalPaperInDto> page) {
        List<EasyWForeignPeriodicalPaperOutDto> list = foreignPeriodicalPaperDalService.getListByPage2(page.getParams());
        page.setResult(list);
        return page;

    }
}
