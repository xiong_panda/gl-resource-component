package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.MixZswxInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.MixZswx;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyMixZswxOutDto;
import com.gl.wanfang.outdto.MixZswxOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWKnowledgeLiteratureDalService;
import com.gl.wanfang.service.IWKnowledgeLiteratureService;
import com.gl.wanfang.validator.CommonValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class WKnowledgeLiteratureServiceImpl implements IWKnowledgeLiteratureService {

    @Autowired
    IWKnowledgeLiteratureDalService knowledgeLiteratureDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;

    @Autowired
    private CommonValidator commonValidator;

    /**
     * 知识文件检索
     * @param params
     * @return
     */
    @Override
    public Page<MixZswxOutDto> getListByPage(Page<MixZswxInDto> params) {
        List<MixZswx>   listByPage = knowledgeLiteratureDalService.getListByPage(params.getParams());

        //转换
        Page<MixZswxOutDto> dtoPage=new Page<>();
        if(listByPage instanceof com.github.pagehelper.Page){
            com.github.pagehelper.Page<MixZswx> pager = (com.github.pagehelper.Page) listByPage;
            List<MixZswxOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                MixZswxOutDto outDto = ConverBeanUtils.dtoToDo(e, MixZswxOutDto.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(params.getOrder());
            dtoPage.setIsPage(params.getIsPage());
            dtoPage.setParams(params.getParams());
            dtoPage.setPageNo(params.getPageNo());
            dtoPage.setPageSize(params.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());

        }
        return dtoPage;

    }

    @Override
    public List<DateGroupByOutDto> getGroupChliteratureByDataFlag() {
        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("data_flag", "mix_zswx");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dataGroup.forEach(v->{
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        commonValidator.validator(page);
        List<EasyMixZswxOutDto> listByPage2 = knowledgeLiteratureDalService.getListByPage2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

    @Override
    public CityOutResult getOneByParams(Map<String, Object> params) {
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(knowledgeLiteratureDalService.getOneByParams(params));
        return cityOutResult;
    }


}
