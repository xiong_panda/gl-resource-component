package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.service.IWKnowledgeLiteratureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * xiaweijie
 * 万方知识文献搜索
 */
@RestController
@Api(value = "万方", tags = {"万方知识文献搜索"})
@RequestMapping("/chliteratureData")
@Slf4j
public class WanFangChliteratureController {

    @Autowired
    IWKnowledgeLiteratureService knowledgeLiteratureService;

    /**
     * @author QinYe
     * @date 2020/6/22 9:45
     * @description 万方知识文献简略搜索
    */
    @ApiOperation(value = "万方知识文献简略搜索")
    @PostMapping("/matchChliteratureInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFChliterature:select")
    public CityOutResult matchChliteratureInfoByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("知识文献检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        try {
            cityListByParams = knowledgeLiteratureService.getListByParams(cityInPage);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("知识文献检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.CHLITERATURE_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHLITERATURE_SEARCH_ERRO.getCode());
        }
        return cityListByParams;

    }

    /**
     * @author QinYe
     * @date 2020/6/22 11:06
     * @description 万方知识文献详情搜索
    */
    @ApiOperation(value = "万方知识文献详情搜索")
    @PostMapping("/matchChliteratureDetailedInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog


//    @RequiresPermissions("WFChliterature:select")
    public CityOutResult matchChliteratureDetailedInfoByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("知识文献检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        if(ObjectUtils.isEmpty(cityInPage.getParams().get("id"))){
            log.info("知识文献检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        try {
            cityListByParams.setResult(knowledgeLiteratureService.getOneByParams(cityInPage.getParams()));
        } catch (Exception e) {
            log.info("知识文献检索异常:{}", e);
            throw new BusinessException(CommonSearchErroCode.CHLITERATURE_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHLITERATURE_SEARCH_ERRO.getCode());
        }
        return cityListByParams;

    }

    /**
     * 万方知识文献聚合搜索
     *
     * @return
     */
    @ApiOperation(value = "万方知识文献聚合搜索")
    @PostMapping("/getGroupChliteratureByDataFlag")
    @ResponseBody
    public Page<DateGroupByOutDto> getGroupChliteratureByDataFlag() {
        List<DateGroupByOutDto> groupChliteratureByDataFlag = knowledgeLiteratureService.getGroupChliteratureByDataFlag();
        Page<DateGroupByOutDto> page = new Page<>();
        page.setList(groupChliteratureByDataFlag);
        return page;
    }
}
