package com.gl.wanfang.controller;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.indto.WanFangEnterpriseProductDatabaseInDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.service.IWExpertsLibraryService;
import com.gl.wanfang.service.IWScientificOrgService;
import com.gl.wanfang.service.IWanFangDBDataService;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 检验检测-企业检索
 * 返回企业信息
 * 对应的专利信息
 * 专家信息
 * @author xwj
 */
@RestController
@RequestMapping("/inspection")
@Api(tags = "检验检测")
public class WanFangInspectionController {
	@Autowired
	private IWScientificOrgService wScientificOrgService;

	@Autowired
	private IWanFangEnterpriseProductDatabaseService iWanFangEnterpriseProductDatabaseService;

	/**
	* 检验检测分页数据--企业检索
	*/
	@ApiModelProperty(value = "检验检测-企业查询",notes = "/getInspectionBySupplier")
	@PostMapping("/getInspectionData")
	@ResponseBody
	@UserSearchLog
	@UserClickLog
	//    @RequiresPermissions("Inspection:select")
	public Page<WanFangEnterpriseProductDatabaseOutDto> getInspectionData(@RequestBody Page<WanFangEnterpriseProductDatabaseInDto> page){
//		return wScientificOrgService.getForm(page);
//		return wScientificOrgService.getInspectionBySupplier(page);
		return iWanFangEnterpriseProductDatabaseService.getInspectionBySupplier(page);
	}

	/**
	 *
	 * 聚合检验检测地区
	 *
	 */
/*	@ApiModelProperty(value = "聚合检验检测地区")
	@PostMapping("/getInspectionGroupOrgType")
	@ResponseBody
	public Page<DateGroupByOutDto> getInspectionGroupOrgType(){
		List<DateGroupByOutDto> groupOrgType = wScientificOrgService.getGroupOrgType();
		Page<DateGroupByOutDto> page=new Page<>();
		page.setList(groupOrgType);
		return page;
	}*/



	/**
	 *检验检测-对应专利
	 * @return
	 */
	@PostMapping("/getChinesePatentByInspection")
	//    @RequiresPermissions("Inspection:select")
	public Page<WChinesePatentOUTDTO> getChinesePatentByInspection(@RequestBody  Page<WChinesePatentINDTO> page){
		Page<WChinesePatentOUTDTO> patentInfoByDB = wScientificOrgService.getChinesePatentByInspection(page);
		return patentInfoByDB;
	}



	/**
	 *检验检测-对应专家
	 * @return
	 */
	/**
	 * 万方专家信息详情本地检索
	 * @return
	 */
	@ApiOperation(value = "万方专家信息详情本地检索")
	@PostMapping("/getExpertByInspection")
	@ResponseBody
	@UserSearchLog
	@UserClickLog
	//    @RequiresPermissions("Inspection:select")
	public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
		return wScientificOrgService.getExpertByInspection(page);
	}



}
