package com.gl.wanfang.service;

import com.gl.wanfang.outdto.HxWfServerSearchOriginalOutDto;
import com.gl.wanfang.outdto.HxWfServerSearchOutDto;

import java.util.List;
import java.util.Map;

public interface IWFDataHanDleService {
    /**
     * 根据参数获取核心层数据
     * @param params
     * @return
     */
    List<HxWfServerSearchOriginalOutDto> getListByParam(Object params);

    /**
     * 保存用户使用数据记录
     * @param params
     * @return
     */
    void saveUserWFUseRecord(Map<String, Object> params);
}
