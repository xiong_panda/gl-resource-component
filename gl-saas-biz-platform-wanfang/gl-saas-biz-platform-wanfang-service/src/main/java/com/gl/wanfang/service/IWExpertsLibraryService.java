package com.gl.wanfang.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.service.IBaseService;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.model.WExpertsLibrary;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWExpertsLibraryService  {

    /**
     * 分页模糊匹配专家
     *
     * @param page
     * @return
     */
    Page<WExpertsLibraryOutDto> getExpertInfoByDB(Page<WExpertsLibraryInDto> page);

    /**
     * 万方专家年份字段聚合
     *
     * @return
     */
    List<DateGroupByOutDto> getCompanyTypeGroup();



    /**
     * 获取专家信息 列表
     *
     * @param page
     * @return
     */
    Page<?> getExpertInfoByDB2(Page<WExpertsLibraryInDto> page);

    /**
     * 获取专家 详情
     *
     * @param params
     * @return
     */
    WExpertsLibraryOutDto getOneByParams(Map<String, Object> params);

    CityOutResult  getCityExpertInfoByDB2(CityPage cityInPage);
	CityOutResult getCityOneByParams(CityPage cityInPage);
}
