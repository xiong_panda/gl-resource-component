package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChinesePaperOaInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WChineseJournalPaper;
import com.gl.wanfang.model.WChinesePaperOa;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWChinesePaperOaDalService;
import com.gl.wanfang.service.IWChinesePaperOaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * service业务处理层
 *
 * @author code_generator
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class WChinesePaperOaServiceImpl implements IWChinesePaperOaService {
    @Autowired
    IWChinesePaperOaDalService wChinesePaperOaDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;

    /**
     * 中文oa
     *
     * @param params
     * @return
     */
    @Override
    public Page<WChinesePaperOaOutDto> getForm(Page<WChinesePaperOaInDto> params) {
        List<WChinesePaperOa> listByPage = wChinesePaperOaDalService.getListByPage(params.getParams());

        //转换
        Page<WChinesePaperOaOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WChinesePaperOa> pager = (com.github.pagehelper.Page) listByPage;
            List<WChinesePaperOaOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WChinesePaperOaOutDto outDto = ConverBeanUtils.dtoToDo(e, WChinesePaperOaOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(params.getOrder());
            dtoPage.setIsPage(params.getIsPage());
            dtoPage.setParams(params.getParams());
            dtoPage.setPageNo(params.getPageNo());
            dtoPage.setPageSize(params.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;


    }

    /**
     * 时间聚合
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Issued_Year", "w_chinese_paper_oa");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }


    @Override
    public Page<?> getForm2(Page<WChinesePaperOaInDto> page) {
        List<EasyWChinesePaperOaOutDto> listByPage = wChinesePaperOaDalService.getListByPage2(page.getParams());
        page.setResult(listByPage);
        return page;
    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        List<EasyWChinesePaperOaOutDto> listByPage2 = wChinesePaperOaDalService.getListByPage2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

    @Override
    public WChinesePaperOaOutDto getOneByParams(Map<String, Object> params) {
        return wChinesePaperOaDalService.getOneByParams(params);
    }
}
