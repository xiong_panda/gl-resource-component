package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.service.IBaseService;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChineseJournalPaperInDto;
import com.gl.wanfang.model.WChineseJournalPaper;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWChineseJournalPaperService extends IBaseService<WChineseJournalPaper> {

    /**
     * 中文期刊
     *
     * @param params
     * @return
     */
    Page<WChineseJournalPaperOutDto> getListByPage(Page<WChineseJournalPaperInDto> params);

    /**
     * 中文期刊时间聚合
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupDate();

    /**
     * 中文期刊简略检索
     *
     * @param page
     * @return
     */
    Page<?> getListByPage2(Page<WChineseJournalPaperInDto> page);

    /**
     * @author QinYe 城市群的中文期刊简略检索
     * @date 2020/6/18 15:38
     * @description
    */
    CityOutResult getListByParams(CityPage page);

    /**
     * 中文期刊详细检索
     *
     * @param params
     * @return
     */
    WChineseJournalPaperOutDto getOneByParams(Map<String, Object> params);
}
