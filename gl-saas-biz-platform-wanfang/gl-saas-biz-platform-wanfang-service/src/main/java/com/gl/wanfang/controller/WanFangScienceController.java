package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.service.IWTechnologyAchievementsService;
import com.gl.wanfang.service.IWanFangDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 科技成果搜索
 * 10006：科技成果
 */
@RestController
@Api(value = "万方", tags = {"万方科技成果搜索"})
@RequestMapping("/fangScienceDate")
@Slf4j
public class WanFangScienceController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangScienceController.class);
    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWTechnologyAchievementsService technologyAchievementsService;


    /**
     * 万方科技成果简略搜索
     * EasyWTechnologyAchievementsOutDto
     *
     * @return
     */
    @ApiOperation(value = "科技成果本地搜索")
    @PostMapping("/scholarteThnologySearchByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFscholarteThnology:select")
    public CityOutResult scholarteThnologySearchByDB(@RequestBody CommBean commBean) {
        logger.info("科技成果检索:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("科技成果检索JSON解析异常:{}", JSON.toJSONString(commBean.getBz_data()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return technologyAchievementsService.getCityListByPage2(cityInPage);
    }

    /**
     * 万方科技成果详情搜索
     *
     * @return
     */
    @ApiOperation(value = "科技成果本地搜索")
    @PostMapping("/scholarteThnologyDetailSearchByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFscholarteThnology:select")
    public CityOutResult scholarteThnologyDetailSearchByDB(@RequestBody CommBean commBean) {
        logger.info("科技成果详情:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("科技成果详情JSON解析异常:{}", JSON.toJSONString(e));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return technologyAchievementsService.getCityOneByParams((cityInPage));

    }


    /**
     * 科技成果时间聚合
     *
     * @return
     */
    @ApiOperation(value = "科技成果时间聚合")
    @PostMapping("/scholarteThnologySearchGroup")
    @ResponseBody
    public Page<DateGroupByOutDto> getScholarteThnologySearchGroup() {
        List<DateGroupByOutDto> groupDate = technologyAchievementsService.getGroupDate();
        Page<DateGroupByOutDto> page = new Page<>();
        page.setList(groupDate);
        return page;

    }


}
