package com.gl.wanfang.util;

/**
 * 常量字段
 */
public class Constant {

    public static final String AESC_BIT_KEY="1234567890123456"; //加密常量key
    public static final String AESC_BYTES_IV="0000000000000000";//加密常量iv
}
