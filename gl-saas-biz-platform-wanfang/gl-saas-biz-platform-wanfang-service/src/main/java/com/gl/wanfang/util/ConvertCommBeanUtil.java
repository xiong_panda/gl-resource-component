package com.gl.wanfang.util;

import com.gl.basis.common.util.Md5Utils;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.ApplyNumProperties;
import com.gl.wanfang.constant.CityConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ConvertCommBeanUtil {

    @Autowired
    private ApplyNumProperties applyNumProperties;

    public  String getSign(String method,String bzData){
        Map<String,String> map = new HashMap<>();
        map.put("method",method);
        map.put("sign",null);
        map.put("timestamp",CityConstant.TIME_NOW);
        map.put("appkey",applyNumProperties.getAppkey());
        //map.put("version",CityConstant.VERSION);
        map.put("sign_method",applyNumProperties.getSignMethod());
        map.put("format",applyNumProperties.getFormat());
        map.put("bz_data",bzData);

        String sign = Md5Utils.signShaHex(map, applyNumProperties.getSignKey());
        return sign;
    }

    public CommBean ConvertCommBean(String method, String bzData, String sign){
        CommBean commBean=new CommBean();
        commBean.setMethod(method);
        commBean.setTimestamp(CityConstant.TIME_NOW);
        commBean.setAppkey(applyNumProperties.getAppkey());
        commBean.setSign_method(applyNumProperties.getSignMethod());
        commBean.setFormat(applyNumProperties.getFormat());
        commBean.setBz_data(bzData);

        return commBean;
    }
}
