package com.gl.wanfang.validator;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommonSearchErroCode;
import com.gl.wanfang.service.impl.WTechnologyAchievementsServiceImpl;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 参数公共校验层
 */
@Data
@Component
public class CommonValidator {
    private static final Logger logger = LoggerFactory.getLogger(CommonValidator.class);

    public void validator(CityPage cityInPage) {
        if (cityInPage == null || cityInPage.getParams() == null) {
            logger.info("检索参数对象为空:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        if (!(cityInPage.getParams() instanceof Map)) {
            logger.info("检索参数格式错误", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_PARSE_ERRO.getMsg(), CityCommonErroCode.PARAMS_PARSE_ERRO.getCode());
        }
    }
}
