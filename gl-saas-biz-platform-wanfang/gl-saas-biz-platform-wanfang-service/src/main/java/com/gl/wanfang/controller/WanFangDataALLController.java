//package com.gl.wanfang.controller;
//
//import com.gl.wanfang.service.IWanFangDataService;
//import com.gl.wanfang.util.WanFangTokenUtil;
//import io.swagger.annotations.Api;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import springfox.documentation.annotations.ApiIgnore;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//
///**
// * 针对于全部检索，走万方接口
// */
//@RestController
//@RequestMapping(value = "/wanfang")
//@Api(value = "万方",tags = {"万方搜索"})
//@ApiIgnore
//public class WanFangDataALLController {
//
//    @Autowired
//    IWanFangDataService wanFangDataService;
//
//    /**
//     *
//     * @param response
//     * @param request
//     * @return
//     */
//    @RequestMapping("/getAllByWF")
//    public String getAll(HttpServletResponse response, HttpServletRequest request){
//        return wanFangDataService.getWFAllData(request,response);
//    }
//
//
//}
