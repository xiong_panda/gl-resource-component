package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.service.IWAuthorLibraryService;
import com.gl.wanfang.service.IWForeignLanguageOaPaperService;
import com.gl.wanfang.service.IWScientificOrgService;
import com.gl.wanfang.service.IWanFangDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 万方OA中文论文搜索
 */
@RestController
@Api(value = "万方", tags = {"万方OA外文论文搜索"})
@RequestMapping("/enoAPeriodicalData")
@Slf4j
public class WanFangENOAPeriodicalController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    IWForeignLanguageOaPaperService languageOaPaperService;
    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWAuthorLibraryService authorLibraryService;


    /**
     * 万方OA外文论文搜索  列表
     * EasyWForeignLanguageOaPaperOutDto
     *
     * @return
     */
    @ApiOperation(value = "万方OA外文论文搜索")
    @PostMapping("/matchENOAPeriodicalByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFENOAPeriodical:select")
    public CityOutResult matchENOAPeriodicalByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("外文OA检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        try {
            cityListByParams = languageOaPaperService.getListByParams(cityInPage);
        } catch (Exception e) {
            log.info("外文OA检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.EN_OA_SEARCH_ERRO.getMsg(), CommonSearchErroCode.EN_OA_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * 万方OA外文论文搜索 详情
     *
     * @return
     */
    @ApiOperation(value = "万方OA外文论文搜索")
    @PostMapping("/matchENOAPeriodicalDetailByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFENOAPeriodical:select")
    public CityOutResult matchENOAPeriodicalDetailByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("外文OA检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        if (ObjectUtils.isEmpty(cityInPage.getParams().get("id"))) {
            log.info("外文OA检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        try {
            cityListByParams.setResult(languageOaPaperService.getOneByParams(cityInPage.getParams()));
        } catch (Exception e) {
            e.printStackTrace();
            log.info("外文OA检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.EN_OA_SEARCH_ERRO.getMsg(), CommonSearchErroCode.EN_OA_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * OA外文论文时间聚合
     *
     * @return
     */
    @ApiOperation(value = "OA外文论文时间聚合")
    @PostMapping("/getGroupDateByOA")
    @ResponseBody
    public Page<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByOutDto> groupDate = languageOaPaperService.getGroupDate();
        Page page = new Page();
        page.setList(groupDate);
        return page;
    }

    /**
     * 对应专家
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertByInspection")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    //    @RequiresPermissions("Inspection:select")
    public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
        return wScientificOrgService.getExpertByInspection(page);
    }

    /**
     * 万方作者信息详情检索
     *
     * @return
     */
    @ApiOperation(value = "万方作者信息本地检索")
    @PostMapping("/matchAuthorInfoByDB")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    //    @RequiresPermissions("WFaAuthor:select")
    public Page<?> MatchAuthorInfoByDB(@RequestBody Page<WAuthorLibraryInDto> page) {
        return authorLibraryService.getListByPage(page);
    }

}
