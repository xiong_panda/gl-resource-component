package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.*;
import com.gl.wanfang.outdto.*;
import com.gl.wanfang.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//import com.gl.wanfang.feign.GuoLongClient;

/**
 * xiaweijie
 * 万方专家搜索
 */
@RestController
@Api(value = "万方", tags = {"万方专家搜索"})
@RequestMapping("/expertData")
public class WanFangExpertDataController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangExpertDataController.class);
    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWExpertsLibraryService expertsLibraryService;

    @Autowired
    private IWScientificOrgService wScientificOrgService;

    @Autowired
    private IWChineseJournalPaperService iwChineseJournalPaperService;

    @Autowired
    private IWChineseConferenceCitationsService iwChineseConferenceCitationsService;

    @Autowired
    private IWChinesePaperOaService iwChinesePaperOaService;

    @Autowired
    private IWTechnologyAchievementsService iwTechnologyAchievementsService;
    @Autowired
    private IWForeignLanguageOaPaperService languageOaPaperService;

    /**
     * 万方专家信息详情检索
     * @param response
     * @param request
     * @return
     */


    /**
     * 万方专家信息简略信息检索
     * EasyWExpertsLibraryOutDto
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/matchExpertInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFExpert:select")
    public CityOutResult matchExpertInfoByDB(@RequestBody CommBean commBean) {
        logger.info("查询专家{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("专家检索JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return expertsLibraryService.getCityExpertInfoByDB2(cityInPage);
    }


    /**
     * 万方专家信息详情
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/matchExpertDetailInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFExpert:select")
    public CityOutResult matchExpertDetailInfoByDB(@RequestBody CommBean commBean) {
        logger.info("万方专家信息详情本地检索:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("专家详情JSON解析异常:{}", JSON.toJSONString(commBean.getBz_data()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return expertsLibraryService.getCityOneByParams(cityInPage);
    }


    /**
     * 万方专家年份
     *
     * @return
     */
    @ApiOperation(value = "万方专家年份字段聚合")
    @PostMapping("/getExpertGroup")
    @ResponseBody
    public Page<DateGroupByOutDto> getExpertGroup() {
        logger.info("万方专家年份字段聚合:{}");
        List<DateGroupByOutDto> companyOrgTypeGroup = expertsLibraryService.getCompanyTypeGroup();
        Page<DateGroupByOutDto> page = new Page();
        page.setList(companyOrgTypeGroup);
        return page;
    }

    /**
     * 对应专利
     *
     * @return
     */
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    @RequestMapping(value = "/getChinesePatentByInspection", method = RequestMethod.POST)
    //    @RequiresPermissions("Inspection:select")
    public Page<WChinesePatentOUTDTO> getChinesePatentByInspection(@RequestBody Page<WChinesePatentINDTO> page) {
        Page<WChinesePatentOUTDTO> patentInfoByDB = wScientificOrgService.getChinesePatentByInspection(page);
        return patentInfoByDB;
    }

    /**
     * @auther Qinye
     * @Description 根据作者获取中文期刊
     * @date 2020/5/29 15:02
     */
    @RequestMapping(value = "/getPeriodicalByAuthor", method = RequestMethod.POST)
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    public Page<WChineseJournalPaperOutDto> getPeriodicalByAuthor(@RequestBody Page<WChineseJournalPaperInDto> page) {
        return iwChineseJournalPaperService.getListByPage(page);
    }

    /**
     * @auther Qinye
     * @Description 根据作者获取中文会议论文
     * @date 2020/5/29 15:26
     */
    @RequestMapping(value = "/getWanFangCNMeetingByAuthor", method = RequestMethod.POST)
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    public Page<WChineseConferencePaperOutDto> getWanFangCNMeetingByAuthor(@RequestBody Page<WChineseConferencePaperInDto> page) {
        return iwChineseConferenceCitationsService.getListByPage(page);
    }

    /**
     * @auther Qinye
     * @Description 根据作者获取中文OA
     * @date 2020/5/29 15:30
     */
    @RequestMapping(value = "/getScientificOrgByAuthor", method = RequestMethod.POST)
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    public Page<WChinesePaperOaOutDto> getScientificOrgByAuthor(@RequestBody Page<WChinesePaperOaInDto> page) {
        return iwChinesePaperOaService.getForm(page);
    }

    /**
     * @auther Qinye
     * @Description 对应科技成果
     * @date 2020/6/1 16:01
     */
    @RequestMapping(value = "/getScholarteThnology", method = RequestMethod.POST)
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    public Page<WTechnologyAchievementsOutDto> getScholarteThnology(@RequestBody Page<WTechnologyAchievementsInDto> page) {
        return iwTechnologyAchievementsService.getListByPage(page);
    }

    /**
     * 万方OA外文论文搜索
     *
     * @return
     */
    @ApiOperation(value = "万方OA外文论文搜索")
    @PostMapping("/matchENOAPeriodicalByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFENOAPeriodical:select")
    public Page<WForeignLanguageOaPaperOutDto> matchENOAPeriodicalByDB(@RequestBody Page<WForeignLanguageOaPaperInDto> page) {
        Page<WForeignLanguageOaPaperOutDto> wForeignLanguageOaPaperByDB = languageOaPaperService.getWForeignLanguageOaPaperByDB(page);
        return wForeignLanguageOaPaperByDB;
    }
}
