package com.gl.wanfang.validator;

import com.gl.basis.common.exception.AuthException;
import com.gl.wanfang.bo.GatherBO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @auther Qinye
 * @date 2020/5/11 17:35
 * @description 热搜/
 */
@Component
public class HotAndRecommendValidate {

    public GatherBO validate(GatherBO bo) {

        if (!StringUtils.isBlank(bo.getArea())){
            String[] s = bo.getArea().split("省");
            bo.setArea(s[0]);
            return bo;
        }
        return bo;
    }
}
