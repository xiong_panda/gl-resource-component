package com.gl.wanfang.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.context.ContextUtils;
import com.gl.basis.common.util.BeanUtil;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.common.util.StringUtils;
import com.gl.basis.util.RedisUtil;
import com.gl.biz.city.CityOutResult;
import com.gl.wanfang.indto.SysCodeInDto;
import com.gl.wanfang.outdto.IndustryFieldOutDTO;
import com.gl.wanfang.outdto.SysCodeOutDto;
import com.gl.wanfang.service.ISysCodeService;
import com.gl.wanfang.service.IUserAnalysisService;
import com.gl.wanfang.util.UserAnalysisUtils;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author QinYe 记录用户搜索关键字和浏览资源信息
 * @date 2020/6/19 11:18
 * @description
 */
@Aspect
@Component
@Slf4j
public class UserClickConfig {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private IUserAnalysisService userAnalysisService;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private ISysCodeService sysCodeService;

    @Pointcut("@annotation(com.gl.wanfang.config.UserClickLog)")
    public void pointcut() { }

    @AfterReturning(value="pointcut()",returning = "returnValue")
    public Object around(JoinPoint point, CityOutResult returnValue) throws Throwable {

        //截获参数进行解析
        Object[] args = point.getArgs();
        Map<String, Object> keyAndValue = getKeyAndValue(args[0]);
        Map<String,Object> bzData = JSONObject.parseObject(String.valueOf(keyAndValue.get("bz_data")),Map.class);
        Object filter = null;
        if (null!=bzData.get("params")||!ObjectUtils.isEmpty(bzData.get("params"))) {
            filter = bzData.get("params");
        }

        //解析参数
        String paramStr = JSON.toJSONString(filter);
        Map<String, Object> params =new HashMap<>();
        if(!StringUtils.isEmpty(paramStr)) {
            Gson gson = new Gson();
            params = gson.fromJson(paramStr, params.getClass());
        }
        //搜索关键字
        String keyWord=null;
        params.remove("__pageNo__");
        params.remove("__pageSize__");
        Set<Map.Entry<String, Object>> set = params.entrySet();
        for (Map.Entry mapKey : set) {
            if(!"id".equals(mapKey.getKey())) {
                if (!"".equals(mapKey.getValue())) {
                    keyWord = mapKey.getValue().toString();
                }
            }
        }

        //资源id
        String sourcesId=null;
        if (null!=params.get("id")||!ObjectUtils.isEmpty(params.get("id"))) {
            sourcesId = String.valueOf(params.get("id"));
        }

        //只对带参数或获取详情的操作进行记录
        if(null==params.get("id")||ObjectUtils.isEmpty(params.get("id"))) {
            if (null == keyWord || StringUtils.isEmpty(keyWord)) {
                return point.getSignature();
            }
        }

        //ID
        String id = idWorker.nextId()+"";
        //获取用户id
        String userId= UserAnalysisUtils.getUserInfo();
        if(null==userId|| StringUtils.isEmpty(userId)){
            return point.getSignature();
        }
        //获取用户行业领域
        IndustryFieldOutDTO userIndustry = userAnalysisService.getIndustryFiledByLogin();

        //用户领域
        String userField =null;
        //用户行业
        String industry =null;
        if(null!=userIndustry) {
            userField=userIndustry.getPFieldIndustry();
            industry = userIndustry.getFieldIndustry();
        }
        //获取访问的URL
        HttpServletRequest request = ContextUtils.getRequest();
        //服务来源
        String requestURI = request.getRequestURI();

        SysCodeInDto sysCodeInDto = new SysCodeInDto();
        sysCodeInDto.setValName(requestURI);
        SysCodeOutDto code=sysCodeService.translateCode(BeanUtil.transBean2Map(sysCodeInDto));
        Map<String, Object> codeMap = new HashMap<>();
        if(!ObjectUtils.isEmpty(code)) {
            codeMap = BeanUtil.transBean2Map(code);
        }

        //服务名
        String serviceName = null;
        if(null!=codeMap.get("remark")||!ObjectUtils.isEmpty(codeMap.get("remark"))) {
            serviceName=String.valueOf(codeMap.get("remark"));
        }

        //使用时间
        Date useTime =new Date(System.currentTimeMillis());

        //采集方式（0：调度平台采集、1：实时接口采集）
        String collectType = "1";
        //采集时间,为定时任务存取的时间
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY,2);
        Date collectTime = calendar.getTime();

        //解析返回值
        String sourcesName=null;
        String sourcesOrigin =null;
        String flag="1";
        if(StringUtils.isEmpty(keyWord)){
            flag="0";
            String result = returnValue.gtResultString();
            Map<String,Object> resultMap = new HashMap<>();
            if (null!=result||!StringUtils.isEmpty(result)) {
                Gson gson = new Gson();
                resultMap = gson.fromJson(result, resultMap.getClass());
            }

            //资源来源
            if(null!=resultMap.get("dataSource")||!ObjectUtils.isEmpty(resultMap.get("dataSource"))) {
                sourcesOrigin=String.valueOf(resultMap.get("dataSource"));
            }
        }

        Map<String,Object> logMap=new HashMap<>();
        logMap.put("id",id);
        logMap.put("userName",null);
        logMap.put("userSex",null);
        logMap.put("userField",userField);
        logMap.put("userIndustry",industry);
        logMap.put("userType",null);
        logMap.put("userArea",null);
        logMap.put("userId",userId);
        logMap.put("searchKeyword",keyWord);
        logMap.put("serviceName",serviceName);
        logMap.put("serviceOrigin",requestURI);
        logMap.put("sourcesName",sourcesName);
        logMap.put("sourcesOrigin",sourcesOrigin);
        logMap.put("sourcesId",sourcesId);
        logMap.put("flag",flag);
        logMap.put("useTime",useTime);
        logMap.put("collectSource",null);
        logMap.put("collectTime",collectTime);
        logMap.put("collectType",collectType);

        setUserSearch(userId+requestURI+keyWord,logMap);
        saveUserSearch(userId,requestURI,keyWord,logMap);

        return point.getSignature();
    }

    /**
     * 用户搜索缓存记录
     * @param key
     * @param value
     */
    private void setUserSearch(String key,Object value){

        redisTemplate.opsForList().remove(key,-1,value);
        redisUtil.setListLeft(key,value);
        redisTemplate.opsForList().rightPop(key);
    }

    /**
     * 落库数据
     * @return
     * @throws Throwable
     */
    private void saveUserSearch(String userId,String requestURI,String keyWord,Map<String,Object> map){
        //排除重复搜索
//        redisTemplate.delete("userClick");
        List<Map<String,Object>> resultList=new ArrayList<>();
        List<Map<String,Object>> data = redisTemplate.opsForList().range("userClick",0,-1);
        if (data.size()>0) {
            for (Map maps : data) {
                if (maps.containsValue(userId)&&maps.containsValue(requestURI)&&maps.containsValue(keyWord)) {
                    return;
                }
            }
        }

        resultList.add(map);
        redisTemplate.opsForList().rightPushAll("userClick",resultList);
    }


    /**
     * 解析切面字段
     * @param obj
     * @return
     */
    private  Map<String, Object> getKeyAndValue(Object obj) {
        Map<String, Object> map = new HashMap<>();
        // 得到类对象
        Class userCla = (Class) obj.getClass();
        /* 得到类中的所有属性集合 */
        Field[] fs = userCla.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            Field f = fs[i];
            f.setAccessible(true); // 设置些属性是可以访问的
            Object val = new Object();
            try {
                val = f.get(obj);
                // 得到此属性的值
                map.put(f.getName(), val);// 设置键值
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return map;
    }

}