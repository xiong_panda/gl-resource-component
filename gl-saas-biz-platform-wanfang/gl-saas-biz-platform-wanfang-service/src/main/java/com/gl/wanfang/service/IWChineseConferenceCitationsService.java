package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChineseConferencePaperInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWChineseConferencePaperOutDto;
import com.gl.wanfang.outdto.WChineseConferencePaperOutDto;

import java.util.List;
import java.util.Map;

public interface IWChineseConferenceCitationsService {
    /**
     * 分页匹配模糊查询
     *
     * @return
     */
    Page<WChineseConferencePaperOutDto> getListByPage(Page<WChineseConferencePaperInDto> wChineseConferenceCitationsInDtoPage);

    /**
     * 万方中文会议时间聚合
     *
     * @return
     */
    List<DateGroupByOutDto> wanFangCNMeetingGroupDate();

    /**
     * 获取简略列表信息
     *
     * @param page
     * @return
     */
    Page<?> getListByPage2(Page<WChineseConferencePaperInDto> page);


    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WChineseConferencePaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * @author QinYe 城市群的中文会议简略检索
     * @date 2020/6/18 15:38
     * @description
     */
    CityOutResult getListByParams(CityPage page);
}
