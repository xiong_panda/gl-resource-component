package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;

import com.gl.biz.city.*;
import com.gl.wanfang.indto.WLawsRegulationsInDTO;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WKnowledgeLiterature;
import com.gl.wanfang.model.WLawsRegulations;
import com.gl.wanfang.outdto.*;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWLawsRegulationsDalService;
import com.gl.wanfang.service.IWLawsRegulationsService;
import com.gl.wanfang.validator.CommonValidator;
import com.netflix.discovery.converters.Auto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
@Service
public class WLawsRegulationsServiceImpl implements IWLawsRegulationsService {
    private static final Logger log = LoggerFactory.getLogger(WLawsRegulationsServiceImpl.class);

    @Autowired
    CommonValidator commonValidator;

    @Autowired
    IWLawsRegulationsDalService lawsRegulationsDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;

    /**
     * 法律法规
     * 分页模糊匹配
     *
     * @return
     */
    public Page<WLawsRegulationsOutDTO> getListByPag(Page<WLawsRegulationsInDTO> page) {
        List<WLawsRegulations> listByPage = lawsRegulationsDalService.getListByPage(page.getParams());
        page.setResult(listByPage);

        //转换
        Page<WLawsRegulationsOutDTO> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WKnowledgeLiterature> pager = (com.github.pagehelper.Page) listByPage;
            List<WLawsRegulationsOutDTO> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WLawsRegulationsOutDTO outDto = ConverBeanUtils.dtoToDo(e, WLawsRegulationsOutDTO.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());

        }
        return dtoPage;


    }


    /**
     * 万方法律法规效力级别聚合检索
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getGroupValueLevel() {
        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("Value_Level", "w_laws_regulations");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dataGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public Page<?> getListByPag2(Page<WLawsRegulationsInDTO> page) {
        List<EasyWLawsRegulationsOutDTO> list = lawsRegulationsDalService.getListByPage2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public WLawsRegulationsOutDTO getOneByParams(Map<String, Object> params) {
        return lawsRegulationsDalService.getOneByParams(params);
    }

    @Override
    public  CityOutResult getCityListByPag2(CityPage cityInPage){
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        List<EasyWLawsRegulationsOutDTO> list = null;
        try {
            list = lawsRegulationsDalService.getListByPage2(cityInPage.getParams());
        } catch (Exception e) {
            log.info("法律法规检索异常:{}", JSON.toJSONString(e));
            throw  new BusinessException(CommonSearchErroCode.LAW_SEARCH_ERRO.getMsg(),CommonSearchErroCode.LAW_SEARCH_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }


    @Override
    public  CityOutResult getCityOneByParams(CityPage cityInPage){
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        WLawsRegulationsOutDTO list = null;
        try {
            list = lawsRegulationsDalService.getOneByParams(cityInPage.getParams());
        } catch (Exception e) {
            log.info("法律法规检索异常:{}", JSON.toJSONString(e));
            throw  new BusinessException(CommonSearchErroCode.LAW_DETAIL_ERRO.getMsg(),CommonSearchErroCode.LAW_DETAIL_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

}
