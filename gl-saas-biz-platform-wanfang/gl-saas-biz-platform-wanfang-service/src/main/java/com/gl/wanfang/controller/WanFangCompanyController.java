package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.indto.WTechnologyAchievementsInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WTechnologyAchievementsOutDto;
import com.gl.wanfang.service.IWScientificOrgService;
import com.gl.wanfang.service.IWTechnologyAchievementsService;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 面向个人的公司查询
 */
@RequestMapping("/company")
@RestController
public class WanFangCompanyController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangCompanyController.class);
    @Autowired
    private IWanFangEnterpriseProductDatabaseService iWanFangEnterpriseProductDatabaseService;
    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWTechnologyAchievementsService iwTechnologyAchievementsService;

    /**
     * 查询企业
     *
     * @return
     */
    @ApiOperation(notes = "查询企业", value = "getListEnterpriseProductDatabase")
    @PostMapping("/getCompanyByDB")
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFSignleEnterpriseProduct:select")
    public CityOutResult getCompanyByDB(@RequestBody CommBean commBean) {
        logger.info("查询企业:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("企业检索JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return iWanFangEnterpriseProductDatabaseService.getListEnterpriseProductDatabase2(cityInPage);
    }

    /**
     * 查询企业详情
     * WanFangEnterpriseProductDatabaseOutDto
     *
     * @return
     */
    @ApiOperation(notes = "查询企业", value = "getCompanyDetailByDB")
    @PostMapping("/getCompanyDetailByDB")
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFSignleEnterpriseProduct:select")
    public CityOutResult getCompanyDetailByDB(@RequestBody CommBean commBean) {
        logger.info("查询企业:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("企业详情JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return iWanFangEnterpriseProductDatabaseService.getOneByParams(cityInPage);
    }


    /**
     * 企业类型聚合数据
     *
     * @return
     */
    @ApiOperation(notes = "企业类型聚合数据", value = "getCompanyGroup")
    @PostMapping("/getCompanyOrgTypeGroup")
    public Page<DateGroupByOutDto> getCompanyOrgTypeGroup() {
        logger.info("企业类型聚合数据:{}");
        List<DateGroupByOutDto> companyOrgTypeGroup = iWanFangEnterpriseProductDatabaseService.getCompanyTypeGroup();

        Page<DateGroupByOutDto> page = new Page();
        page.setList(companyOrgTypeGroup);
        return page;
    }

    /**
     * 对应专家
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertByInspection")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    //    @RequiresPermissions("Inspection:select")
    public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
        return wScientificOrgService.getExpertByInspection(page);
    }

    /**
     * @auther Qinye
     * @Description 对应科技成果
     * @date 2020/6/1 16:01
     */
    @RequestMapping(value = "/getScholarteThnology", method = RequestMethod.POST)
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    public Page<WTechnologyAchievementsOutDto> getScholarteThnology(@RequestBody Page<WTechnologyAchievementsInDto> page) {
        return iwTechnologyAchievementsService.getListByPage(page);
    }

    /**
     * 对应专利
     *
     * @return
     */
//    @UserClickLog
//    @UserSearchLog
    @ResponseBody
    @RequestMapping(value = "/getChinesePatentByInspection", method = RequestMethod.POST)
    //    @RequiresPermissions("Inspection:select")
    public Page<WChinesePatentOUTDTO> getChinesePatentByInspection(@RequestBody Page<WChinesePatentINDTO> page) {
        Page<WChinesePatentOUTDTO> patentInfoByDB = wScientificOrgService.getChinesePatentByInspection(page);
        return patentInfoByDB;
    }
}
