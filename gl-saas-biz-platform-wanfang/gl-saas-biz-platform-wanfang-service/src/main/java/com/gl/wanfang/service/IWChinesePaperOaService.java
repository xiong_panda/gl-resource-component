package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.service.IBaseService;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChinesePaperOaInDto;
import com.gl.wanfang.model.WChinesePaperOa;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWChinesePaperOaService extends IBaseService<WChinesePaperOa> {
    /*
     * 查询数据的方法
     * */
    Page<WChinesePaperOaOutDto> getForm(Page<WChinesePaperOaInDto> params);

    /**
     * 时间聚合
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupDate();

    /**
     * 中文OA论文详情
     *
     * @param params
     * @return
     */
    WChinesePaperOaOutDto getOneByParams(Map<String, Object> params);

    /**
     * 中文OA论文检索
     *
     * @param page
     * @return
     */
    Page<?> getForm2(Page<WChinesePaperOaInDto> page);

    /**
     * @author QinYe 城市群的中文OA简略检索
     * @date 2020/6/18 15:38
     * @description
     */
    CityOutResult getListByParams(CityPage page);
}
