package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.biz.city.CommonSearchErroCode;
import com.gl.wanfang.bo.WScientificOrgBo;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.*;
import com.gl.wanfang.outdto.*;
import com.gl.wanfang.service.IWOverseasPatentService;
import com.gl.wanfang.service.IWScientificOrgService;
import com.gl.wanfang.service.IWTechnologyAchievementsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * WScientificOrg服务暴露接口的实现层
 *
 * @author code_generator
 */
@RestController
@RequestMapping("/wScientificOrg")
@Api(tags = "科研机构")
@Slf4j
public class WScientificOrgController {
    private static final Logger logger = LoggerFactory.getLogger(WScientificOrgController.class);

    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWTechnologyAchievementsService iwTechnologyAchievementsService;
    @Autowired
    private IWOverseasPatentService overseasPatentService;

    /**
     * 查询科研机构分页数据
     * EasyWScientificOrgOutDto
     */
    @ApiModelProperty(value = "科研机构数据查询", notes = "/wScientificOrg")
    @PostMapping("/GetScientificOrg")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFScientificOrg:select")
    public CityOutResult getScientificOrg(@RequestBody CommBean commBean) {
        log.info("机构检索：{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("机构检索JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return wScientificOrgService.getForm2(cityInPage);
    }

    /**
     * 查询科研机构详情数据
     */
    @ApiModelProperty(value = "科研机构数据查询", notes = "/wScientificOrg")
    @PostMapping("/getScientificOrgDetail")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFScientificOrg:select")
    public CityOutResult getScientificOrgDetail(@RequestBody CommBean commBean) {
        log.info("机构检索详情:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("机构详情JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        return wScientificOrgService.getOneParams(cityInPage);

    }


    /**
     * 聚合科研机构地区
     */
    @ApiModelProperty(value = "聚合科研机构地区")
    @PostMapping("/getGroupOrgType")
    @ResponseBody
    public Page<DateGroupByOutDto> getGroupOrgType() {
        List<DateGroupByOutDto> groupOrgType = wScientificOrgService.getGroupOrgType();
        Page<DateGroupByOutDto> page = new Page<>();
        page.setList(groupOrgType);
        return page;
    }

    /**
     * 对应专家
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertByInspection")
    @ResponseBody
    @UserClickLog
    //    @RequiresPermissions("Inspection:select")
    public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
        return wScientificOrgService.getExpertByInspection(page);
    }

    /**
     * @auther Qinye
     * @Description 对应科技成果
     * @date 2020/6/1 16:01
     */
    @RequestMapping(value = "/getScholarteThnology", method = RequestMethod.POST)
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    public Page<WTechnologyAchievementsOutDto> getScholarteThnology(@RequestBody Page<WTechnologyAchievementsInDto> page) {
        return iwTechnologyAchievementsService.getListByPage(page);
    }

    /**
     * 对应中文专利
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getChinesePatentByInspection", method = RequestMethod.POST)
    //    @RequiresPermissions("Inspection:select")
    @UserClickLog
    public Page<WChinesePatentOUTDTO> getChinesePatentByInspection(@RequestBody Page<WChinesePatentINDTO> page) {
        Page<WChinesePatentOUTDTO> patentInfoByDB = wScientificOrgService.getChinesePatentByInspection(page);
        return patentInfoByDB;
    }

    /**
     * 海外专利本地搜索
     *
     * @return
     */
    @ApiOperation(value = "海外专利本地搜索")
    @PostMapping("/matchENPatentInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFENPatent:select")
    public Page<WOverseasPatentOutDto> matchENPatentInfoByDB(@RequestBody Page<WOverseasPatentInDto> page) {
        return overseasPatentService.getListByPage(page);

    }

}
