package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WOverseasPatentInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WOverseasPatent;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWOverseasPatentOutDto;
import com.gl.wanfang.outdto.WLawsRegulationsOutDTO;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWOverseasPatentDalService;
import com.gl.wanfang.service.IWOverseasPatentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
@Service
public class WOverseasPatentServiceImpl implements IWOverseasPatentService {

    @Autowired
    IWOverseasPatentDalService wOverseasPatentServiceDal;
    @Autowired
    IDateGroupDalService dateGroupDalService;
//    @Autowired
//    private GuoLongClient guoLongClient;

    /**
     * 海外专利 模糊匹配
     *
     * @return
     */
    @Override
    public Page<WOverseasPatentOutDto> getListByPage(Page<WOverseasPatentInDto> page) {
        List<WOverseasPatent> listByPage = wOverseasPatentServiceDal.getListByPage(page.getParams());
        page.setResult(listByPage);

        //转换
        Page<WOverseasPatentOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WOverseasPatent> pager = (com.github.pagehelper.Page) listByPage;
            List<WOverseasPatentOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WOverseasPatentOutDto outDto = ConverBeanUtils.dtoToDo(e, WOverseasPatentOutDto.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());

        }
        return dtoPage;


    }

    /**
     * 时间聚合
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Year", "w_overseas_patent");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public Page<?> getListByPage2(Page<WOverseasPatentInDto> page) {
        List<EasyWOverseasPatentOutDto> listByPage = wOverseasPatentServiceDal.getListByPage2(page.getParams());

        page.setResult(listByPage);
        return page;

    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        List<EasyWOverseasPatentOutDto> listByPage2 = wOverseasPatentServiceDal.getListByPage2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

    @Override
    public WOverseasPatentOutDto getOneByParams(Map<String, Object> params) {

        return wOverseasPatentServiceDal.getOneByParams(params);
    }
}
