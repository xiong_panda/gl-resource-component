//package com.gl.wanfang.feign;
//
//
//import com.gl.biz.city.CityOutResult;
//import com.gl.biz.city.CommBean;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.*;
//
//@FeignClient(value = "saas-platform-wanfang",url = "/outApi/data")
//public interface GuoLongClient {
//
//    /**
//     * @author QinYe
//     * @date 2020/6/22 17:22
//     * @description 综合服务调用-简略信息搜索
//    */
//    @RequestMapping(value = "/mutiDataInfoSearch",method = RequestMethod.POST)
//    @ResponseBody
//    CityOutResult mutiDataInfoSearch(@RequestBody CommBean commBean);
//
//    /**
//     * @author QinYe
//     * @date 2020/6/22 17:22
//     * @description 综合服务调用-详情信息搜索
//    */
//    @RequestMapping(value = "/mutiDataIdsSearch",method = RequestMethod.POST)
//    @ResponseBody
//    CityOutResult mutiDataIdsSearch(@RequestBody CommBean commBean);
//
//}
