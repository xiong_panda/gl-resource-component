package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChineseConferencePaperInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WChineseConferenceCitations;
import com.gl.wanfang.model.WChineseConferencePaper;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChineseConferencePaperOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWChineseConferenceCitationsDalService;
import com.gl.wanfang.service.IWChineseConferenceCitationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class WChineseConferenceCitationsServiceImpl implements IWChineseConferenceCitationsService {

    @Autowired
    IWChineseConferenceCitationsDalService chineseConferenceCitationsServiceDal;
    @Autowired
    IDateGroupDalService dateGroupDalService;

    /**
     * 中文会议引文模糊匹配查询-分页
     *
     * @param params
     * @return
     */
    @Override
    public Page<WChineseConferencePaperOutDto> getListByPage(Page<WChineseConferencePaperInDto> page) {
        List<WChineseConferencePaper> listByPage = chineseConferenceCitationsServiceDal.getListByPage(page.getParams());
//        Object obj = new Object();
//        Page<WChineseConferencePaperOutDto> pageWanfang=guoLongClient.matchChinaConferencePaperPOInfo(obj);
//        if(pageWanfang.getSuccess()){
//            pageWanfang.setIsPage(page.getIsPage());
//            pageWanfang.setPageNo(page.getPageNo());
//            pageWanfang.setParams(page.getParams());
//            return pageWanfang;
//        }

        Page<WChineseConferencePaperOutDto> dtoPage = new Page<>();

        //转换
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WChineseConferenceCitations> pager = (com.github.pagehelper.Page) listByPage;
            List<WChineseConferencePaperOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WChineseConferencePaperOutDto outDto = ConverBeanUtils.dtoToDo(e, WChineseConferencePaperOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;


    }

    @Override
    public List<DateGroupByOutDto> wanFangCNMeetingGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("End_Meeting_Date", "w_chinese_conference_paper");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }


    @Override
    public Page<?> getListByPage2(Page<WChineseConferencePaperInDto> page) {
        page.setResult(chineseConferenceCitationsServiceDal.getListByPage2(page.getParams()));
        return page;
    }

    @Override
    public WChineseConferencePaperOutDto getOneByParams(Map<String, Object> params) {
        return chineseConferenceCitationsServiceDal.getOneByParams(params);
    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        List<WChineseConferencePaperOutDto> listByPage2 = chineseConferenceCitationsServiceDal.getListByPage2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }
}
