package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.*;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WChinesePaperOa;
import com.gl.wanfang.model.WExpertsLibrary;
import com.gl.wanfang.outdto.*;

import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWExpertsLibraryService;

import com.gl.wanfang.validator.CommonValidator;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * service业务处理层
 *
 * @author code_generator
 */

@Slf4j
@Service
public class WExpertsLibraryServiceImpl implements IWExpertsLibraryService {
    private static final Logger logger = LoggerFactory.getLogger(WExpertsLibraryServiceImpl.class);
    @Autowired
    private WExpertsLibraryDalServiceImpl wExpertsLibraryDalService;
    @Autowired
    IDateGroupDalService dateGroupDalService;

    @Autowired
    CommonValidator commonValidator;

    /**
     * 分页模糊匹配专家
     *
     * @param page
     * @return
     */
    @Override
    public Page<WExpertsLibraryOutDto> getExpertInfoByDB(Page<WExpertsLibraryInDto> page) {
        List<WExpertsLibrary> listByPage = wExpertsLibraryDalService.getListByPage(page.getParams());

        //转换
        Page<WExpertsLibraryOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WExpertsLibrary> pager = (com.github.pagehelper.Page) listByPage;
            List<WExpertsLibraryOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WExpertsLibraryOutDto outDto = ConverBeanUtils.dtoToDo(e, WExpertsLibraryOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;

    }

    @Override
    public List<DateGroupByOutDto> getCompanyTypeGroup() {
        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("Birth_Date", "w_experts_library");
        //转换
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        dataGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }


    @Override
    public Page<?> getExpertInfoByDB2(Page<WExpertsLibraryInDto> page) {
        List<EasyWExpertsLibraryOutDto> list = wExpertsLibraryDalService.getExpertInfoByDB2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public WExpertsLibraryOutDto getOneByParams(Map<String, Object> params) {
        return wExpertsLibraryDalService.getOneByParams(params);
    }


    @Override
    public CityOutResult getCityExpertInfoByDB2(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        List<EasyWExpertsLibraryOutDto> list = null;
        try {
            list = wExpertsLibraryDalService.getExpertInfoByDB2(cityInPage.getParams());
        } catch (Exception e) {
            logger.info("专家检索JSON解析异常:", e);
            throw new BusinessException(CommonSearchErroCode.EXPERT_SEARCH_ERRO.getMsg(), CommonSearchErroCode.EXPERT_SEARCH_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

    @Override
    public CityOutResult getCityOneByParams(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();

        WExpertsLibraryOutDto list = null;
        try {
            list = wExpertsLibraryDalService.getOneByParams(cityInPage.getParams());
        } catch (Exception e) {
            logger.info("专家详情查询异常:", e);
            throw new BusinessException(CommonSearchErroCode.EXPERT_DETAIL_ERRO.getMsg(), CommonSearchErroCode.EXPERT_DETAIL_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

}
