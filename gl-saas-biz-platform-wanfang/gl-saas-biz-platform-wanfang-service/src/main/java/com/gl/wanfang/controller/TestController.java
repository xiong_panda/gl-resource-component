package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.util.DateUtils;
import com.gl.biz.api.fegin.PatentFegin;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.ApplyNumProperties;
import com.gl.wanfang.util.CitySignUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    ApplyNumProperties applyNumProperties;

    @Autowired
    CitySignUtils citySignUtils;

    @Autowired
    PatentFegin patentFegin;

    @PostMapping("/t")
    public String MatchAuthorInfoByDB(@RequestBody CommBean commBean) {
        // 解析JSON字符串 得到参数对象
        CityPage cityPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);

        String curDate = DateUtils.getCurDate("yyyy-MM-dd HH:mm:ss");
        String sign = citySignUtils.sign("/patent/search",  JSON.toJSONString(cityPage), curDate);
        CommBean commBean1 = new CommBean();
        commBean1.setMethod("/patent/search");
        commBean1.setTimestamp(curDate);
        commBean1.setAppkey(applyNumProperties.getAppkey());
        commBean1.setSign_method(applyNumProperties.getSignMethod());
        commBean1.setSign(sign);
        commBean1.setBz_data(JSON.toJSONString(cityPage));
        commBean1.setFormat(applyNumProperties.getFormat());
//        return patentFegin.MatchAuthorInfoByDB(commBean1);
        return null;
    }

}
