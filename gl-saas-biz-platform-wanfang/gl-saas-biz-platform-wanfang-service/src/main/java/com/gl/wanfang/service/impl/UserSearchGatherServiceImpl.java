package com.gl.wanfang.service.impl;

import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.wanfang.bo.GatherBO;
import com.gl.wanfang.model.BzHotAndRecommend;
import com.gl.wanfang.outdto.GatherOutDTO;
import com.gl.wanfang.service.UserSearchGatherDalService;
import com.gl.wanfang.service.UserSearchGatherService;
import com.gl.wanfang.util.UserAnalysisUtils;
import com.gl.wanfang.validator.HotAndRecommendValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther Qinye
 * @date 2020/5/11 14:12
 * @description
 */
@Service
public class UserSearchGatherServiceImpl implements UserSearchGatherService {
    @Autowired
    private UserSearchGatherDalService userSearchGatherDalService;
    @Autowired
    private HotAndRecommendValidate hotAndRecommendValidate;

    @Override
    public List<GatherOutDTO> getHotAndRecommend(GatherBO bo) {
        GatherBO validateBo = hotAndRecommendValidate.validate(bo);
        return userSearchGatherDalService.getHotAndRecommend(ConverBeanUtils.doToDto(validateBo, BzHotAndRecommend.class));
    }
}
