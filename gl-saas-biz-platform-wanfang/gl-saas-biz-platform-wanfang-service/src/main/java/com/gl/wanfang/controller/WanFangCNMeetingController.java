package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.indto.WChineseConferencePaperInDto;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.outdto.*;
import com.gl.wanfang.service.IWAuthorLibraryService;
import com.gl.wanfang.service.IWChineseConferenceCitationsService;
import com.gl.wanfang.service.IWScientificOrgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 万方中文会议搜索
 */
@RestController
@Api(value = "万方", tags = {"万方中文会议搜索"})
@RequestMapping("/cnmData")
@Slf4j
public class WanFangCNMeetingController {
/*
    @Autowired
    IWanFangDataService wanFangDataService;*/

    @Autowired
    IWChineseConferenceCitationsService chineseConferenceCitationsService;
    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWAuthorLibraryService authorLibraryService;

    /**
     * 万方中文会议搜索
     * EasyWChineseConferencePaperOutDto
     *
     * @return
     */
    @ApiOperation(value = "中文会议本地搜索")
    @PostMapping("/wanFangCNMeetingByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFCNMeeting:select")
    public CityOutResult wanFangCNMeetingByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("中文会议检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        try {
            cityListByParams = chineseConferenceCitationsService.getListByParams(cityInPage);
        } catch (Exception e) {
            log.info("中文会议检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.CHINESE_CONFERENCE_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHINESE_CONFERENCE_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * 万方中文会议  详情
     * WChineseConferencePaperOutDto
     *
     * @return
     */
    @ApiOperation(value = "中文会议本地搜索")
    @PostMapping("/wanFangCNMeetingDetailByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFCNMeeting:select")
    public CityOutResult wanFangCNMeetingDetailByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("中文会议检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        if(ObjectUtils.isEmpty(cityInPage.getParams().get("id"))){
            log.info("中文会议检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        try {
            cityListByParams.setResult(chineseConferenceCitationsService.getOneByParams(cityInPage.getParams()));
        } catch (Exception e) {
            log.info("中文会议检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.CHINESE_CONFERENCE_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHINESE_CONFERENCE_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * 万方中文会议时间聚合
     *
     * @return
     */
    @ApiOperation(value = "万方中文会议时间聚合")
    @PostMapping("/wanFangCNMeetingGroupDate")
    @ResponseBody
    public Page<DateGroupByOutDto> wanFangCNMeetingGroupDate() {
        List<DateGroupByOutDto> groupData = chineseConferenceCitationsService.wanFangCNMeetingGroupDate();
        Page<DateGroupByOutDto> page = new Page();
        page.setList(groupData);
        return page;
    }

    /**
     * 对应专家
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertByInspection")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    //    @RequiresPermissions("Inspection:select")
    public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
        return wScientificOrgService.getExpertByInspection(page);
    }

    /**
     * 万方作者信息详情检索
     *
     * @return
     */
    @ApiOperation(value = "万方作者信息本地检索")
    @PostMapping("/matchAuthorInfoByDB")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    //    @RequiresPermissions("WFaAuthor:select")
    public Page<?> MatchAuthorInfoByDB(@RequestBody Page<WAuthorLibraryInDto> page) {
        return authorLibraryService.getListByPage(page);
    }
}
