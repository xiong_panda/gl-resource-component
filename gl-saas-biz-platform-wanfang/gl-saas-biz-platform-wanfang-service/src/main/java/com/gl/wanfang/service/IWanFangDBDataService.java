package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;

import java.util.List;
import java.util.Map;

public interface IWanFangDBDataService {

    /**
     * 专利查询
     *
     * @param page
     * @return
     */
    Page<WChinesePatentOUTDTO> getPatentInfoByDB(Page<WChinesePatentINDTO> page);

    /**
     * 万方专利类型聚合检索
     *
     * @return
     */
    List<DateGroupByOutDto> getCompanyTypeGroup();

    /**
     * 中文专利检索 简略信息
     *
     * @param page
     * @return
     */
    Page<?> getPatentInfoByDB2(Page<WChinesePatentINDTO> page);

    /**
     * 获取中文专利详情信息
     *
     * @param page
     * @return
     */
    WChinesePatentOUTDTO getOneByParams(Map<String, Object> params);

    CityOutResult  getCityPatentInfoByDB2(CityPage page);


    CityOutResult  getCityOneByParams(CityPage page);
}
