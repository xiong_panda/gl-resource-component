package com.gl.wanfang.controller;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.service.ICollaborativeDesignDataService;
import com.gl.wanfang.service.IWanFangDBDataService;
import com.gl.wanfang.service.IWanFangDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * xiaweijie
 * 协同设计
 * 万方-专利检索
 * 只面向汽车行业
 */
@RestController
@Api(value = "万方",tags = {"协同设计"})
@RequestMapping("/collaborativeDesign")
public class WanFangCollaborativeDesignController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangCollaborativeDesignController.class);


    @Autowired
    ICollaborativeDesignDataService wanFangDBDataService;



    /**
     * 协同设计-万方专利信息详情本地检索
     * @return
     */
    @ApiOperation(value = "协同设计详情检索")
    @PostMapping("/getCollaborativeDesignByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("collaborativeDesign:select")
    public Page<WChinesePatentOUTDTO> getCollaborativeDesignByDB(@RequestBody  Page<WChinesePatentINDTO> page) {
        Page<WChinesePatentOUTDTO> outdtoPage = wanFangDBDataService.getPatentInfoByDB(page);
        return outdtoPage;
    }

    /**
     * 协同设计-万方专利类型聚合检索
     * @return
     */
    @ApiOperation(value = "协同设计类型聚合检索")
    @PostMapping("/getcollaborativeDesignGroup")
    @ResponseBody
    public Page<DateGroupByOutDto> getcollaborativeDesignGroup() {
        logger.info("万方专利类型聚合检索:{}");
        List<DateGroupByOutDto> companyOrgTypeGroup = wanFangDBDataService.getCompanyTypeGroup();
        Page<DateGroupByOutDto> page =new Page();
        page.setList(companyOrgTypeGroup);
        return page;
    }
}
