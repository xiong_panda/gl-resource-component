package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.bo.HxWfServerSearchBO;
import com.gl.wanfang.indto.HxWfServerSearchInDto;
import com.gl.wanfang.outdto.HxWfServerSearchOutDto;

import java.util.List;

public interface IUserClickService {

    /**
     * @auther Qinye
     * @Description 用户浏览记录
     * @date 2020/6/11 19:06
    */
    void saveUserClick(List<HxWfServerSearchBO> list);

    /**
     * @auther Qinye
     * @Description 用户访问数据采集提供
     * @date 2020/6/15 14:59
     */
    Page<HxWfServerSearchOutDto> getListByCollectTime(Page<HxWfServerSearchInDto> page);

}
