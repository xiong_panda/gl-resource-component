package com.gl.wanfang.service;


import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.MixZswxInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.MixZswxOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWKnowledgeLiteratureService {

    /**
     * 知识文件检索
     *
     * @param params
     * @return
     */
    Page<MixZswxOutDto> getListByPage(Page<MixZswxInDto> params);


    /**
     * 知识文献聚合来源分类
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupChliteratureByDataFlag();

    /**
     * @author QinYe
     * @date 2020/6/22 9:54
     * @description 城市群的知识文献简略检索
    */
    CityOutResult getListByParams(CityPage page);

    /**
     * @author QinYe
     * @date 2020/6/22 11:12
     * @description 知识文献详情
    */
    CityOutResult getOneByParams(Map<String, Object> params);

    
}
