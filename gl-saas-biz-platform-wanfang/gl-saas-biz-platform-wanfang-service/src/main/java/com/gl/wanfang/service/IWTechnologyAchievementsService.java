package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WTechnologyAchievementsInDto;
import com.gl.wanfang.model.WTechnologyAchievements;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WTechnologyAchievementsOutDto;

import java.util.List;
import java.util.Map;

public interface IWTechnologyAchievementsService {
    /**
     * 分页匹配模糊查询
     *
     * @param params
     * @return
     */
    Page<WTechnologyAchievementsOutDto> getListByPage(Page<WTechnologyAchievementsInDto> page);

    /**
     * 科技成果时间聚合
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupDate();

    /**
     * 科技成果简略检索
     *
     * @param page
     * @return
     */
    Page<?> getListByPage2(Page<WTechnologyAchievementsInDto> page);

    /**
     * 科技成果详情检索
     *
     * @param params
     * @return
     */
    WTechnologyAchievementsOutDto getOneByParams(Map<String, Object> params);


    CityOutResult getCityListByPage2( CityPage cityInPage);

    CityOutResult getCityOneByParams(CityPage cityInPage);
}
