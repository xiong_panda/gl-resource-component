package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;

import java.util.List;

/**
 * 协同设计检索
 */
public interface ICollaborativeDesignDataService {

    /**
     * 专利查询
     * @param page
     * @return
     */
    Page<WChinesePatentOUTDTO> getPatentInfoByDB(Page<WChinesePatentINDTO> page);

    /**
     * 万方专利类型聚合检索
     * @return
     */
    List<DateGroupByOutDto> getCompanyTypeGroup();
}
