package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.*;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.model.WAuthorLibrary;
import com.gl.wanfang.model.WanFangEnterpriseProductDatabase;
import com.gl.wanfang.outdto.EasyWAuthorLibraryOutDto;
import com.gl.wanfang.outdto.WAuthorLibraryOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWAuthorLibraryDalService;
import com.gl.wanfang.service.IWAuthorLibraryService;
import com.gl.wanfang.validator.CommonValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class WAuthorLibraryServiceImpl implements IWAuthorLibraryService {

    @Autowired
    IWAuthorLibraryDalService authorLibraryServiceDal;


    @Autowired
    IDateGroupDalService dateGroupDalService;

    @Autowired
    CommonValidator commonValidator;

    @Override
    public Page<?> getListByPage2(Page<WAuthorLibraryInDto> page) {
        List<EasyWAuthorLibraryOutDto> listByPage = authorLibraryServiceDal.getListByPage2(page.getParams());

        page.setResult(listByPage);
        return page;
    }

    @Override
    public WAuthorLibraryOutDto getListById(Map<String, Object> params) {

        return authorLibraryServiceDal.getOne(params);
    }

    @Override
    public Page<?> getListByPage(Page<WAuthorLibraryInDto> page) {
        List<WAuthorLibrary> listByPage = authorLibraryServiceDal.getListByPage(page.getParams());
        page.setResult(listByPage);
        return page;
    }


    @Override
    public CityOutResult getCityListByParams(CityPage page) {
        commonValidator.validator(page);
        CityOutResult cityOutResult = new CityOutResult();
        List<EasyWAuthorLibraryOutDto> listByPage2 = null;
        try {
            listByPage2 = authorLibraryServiceDal.getListByPage2(page.getParams());
        } catch (Exception e) {
            log.info("作者检索异常:", e);
            throw new BusinessException(CommonSearchErroCode.AUTHOR_SEARCH_ERRO.getMsg(), CommonSearchErroCode.AUTHOR_SEARCH_ERRO.getCode());
        }
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

    @Override
    public CityOutResult getCityDetailByParams(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        WAuthorLibraryOutDto one = null;
        try {
            one = authorLibraryServiceDal.getOne(cityInPage.getParams());
        } catch (Exception e) {
            log.info("作者详情异常:", e);
            throw new BusinessException(CommonSearchErroCode.AUTHOR_DETAIL_ERRO.getMsg(), CommonSearchErroCode.AUTHOR_DETAIL_ERRO.getCode());
        }
        cityOutResult.setResult(one);
        return cityOutResult;
    }


}
