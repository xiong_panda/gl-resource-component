package com.gl.wanfang.controller;

import com.gl.basis.common.context.ContextUtils;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.util.RedisUtil;
import com.gl.wanfang.util.UserAnalysisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 从缓存中获取用户的搜索记录
 */
@RestController
@RequestMapping("/histroy")
public class UserSearchHistroy {
    @Autowired
    RedisUtil redisUtil;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 获取用户搜索记录
     * @param httpServletRequest
     * @return
     */
    @GetMapping("/getUserSearchHistroy")
    public Page<List<String>> getUserSearchHistroy(HttpServletRequest httpServletRequest) {
     /*   String id = UserAnalysisUtils.getUserInfo();
        String requestName = httpServletRequest.getParameter("requestName");
        List list=new ArrayList();
        int count=5;
        if(!"".equals(id) && !"".equals(requestName)){
            Map map = (Map)redisUtil.get(id+requestName);
            if(!"".equals(map) && null!=map){
                Set<String> set = (LinkedHashSet)map.get(requestName);
                for (int i=set.size()-1;i>=0;i--) {
                    if(count>0){
                        Object[] objects = set.toArray();
                        Object object = objects[i];
                        list.add(object);
                        count--;
                    }else{
                        break;
                    }

                }
                return list;
            }
        }
        return null;
    }*/

        String userId = UserAnalysisUtils.getUserInfo();
        String requestName = httpServletRequest.getParameter("requestName");
        List range = redisTemplate.opsForList().range(userId + requestName, 0, -1);
        Page page=new Page();
        page.setSuccess(true);
        page.setResult(range);
        return page;
    }
}