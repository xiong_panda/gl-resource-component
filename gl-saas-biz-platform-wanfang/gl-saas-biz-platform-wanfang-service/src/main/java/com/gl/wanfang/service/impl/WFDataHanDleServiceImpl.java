package com.gl.wanfang.service.impl;

import com.gl.basis.common.util.AesCBCUtil;
import com.gl.basis.common.util.BeanUtil;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.indto.HxWfServerSearchTimeInDto;
import com.gl.wanfang.model.HxWfServerSearch;
import com.gl.wanfang.model.HxWfServerSearchOriginal;
import com.gl.wanfang.outdto.HxWfServerSearchOriginalOutDto;
import com.gl.wanfang.outdto.HxWfServerSearchOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;
import com.gl.wanfang.service.IWFDataHanDleDalService;
import com.gl.wanfang.service.IWFDataHanDleService;
import com.gl.wanfang.util.Constant;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class WFDataHanDleServiceImpl  implements IWFDataHanDleService {

    @Autowired
    IWFDataHanDleDalService dataHanDleDalService;

    @Autowired
    IdWorker idWorker;

    /**
     * 获取原始库数据list
     */
    @Override
    public List<HxWfServerSearchOriginalOutDto> getListByParam(Object params) {

        String result = String.valueOf(params);
        List<HxWfServerSearchOriginalOutDto> list =new ArrayList<>();
        try {
            /*解密转换*/
            String decrypt = AesCBCUtil.decrypt(result, "utf-8", Constant.AESC_BIT_KEY, Constant.AESC_BYTES_IV);
            Gson gson=new Gson();
            HxWfServerSearchTimeInDto hxWfServerSearchTimeInDto = gson.fromJson(decrypt, HxWfServerSearchTimeInDto.class);
            if(hxWfServerSearchTimeInDto.getCollect().equals("cj")){
                Map<String, Object> map= new HashMap();
                map.put("startTime",hxWfServerSearchTimeInDto.getStartTime());
                map.put("endTime",hxWfServerSearchTimeInDto.getEndTime());
                List<HxWfServerSearchOriginal> listByParam = dataHanDleDalService.getListByParam(map);
                //转换
                listByParam.forEach(e -> {
                    HxWfServerSearchOriginalOutDto outDto = ConverBeanUtils.dtoToDo(e, HxWfServerSearchOriginalOutDto.class);
                    list.add(outDto);
                });
            }

        } catch (Exception e) {
           throw new RuntimeException(e);
        }
        return list;
    }

    /**
     * 保存用户使用数据记录
     * @param params
     * @return
     */
    @Override
    public void saveUserWFUseRecord(Map<String, Object> params) {

        //生成对应的id和时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date.getTime());
        String id = String.valueOf(idWorker.nextId());
        params.put("useTime",format);
        params.put("id",id);
        dataHanDleDalService.saveUserWFUseRecord(params);
    }
}
