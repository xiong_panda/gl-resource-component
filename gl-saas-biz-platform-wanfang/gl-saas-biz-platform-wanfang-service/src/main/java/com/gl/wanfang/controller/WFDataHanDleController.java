package com.gl.wanfang.controller;

import com.gl.basis.common.pojo.Page;
//import com.gl.wanfang.feign.GuoLongClient;
import com.gl.wanfang.indto.HxWfServerSearchInDto;
import com.gl.wanfang.outdto.HxWfServerSearchOriginalOutDto;
import com.gl.wanfang.service.IWFDataHanDleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户记录使用相关   采集用户数据
 */
@RestController
@RequestMapping("/WFData")
@Slf4j
public class WFDataHanDleController {

    @Autowired
    IWFDataHanDleService dataHanDleService;

//    @Autowired
//    GuoLongClient guoLongClient;

    /**
     * 获取原始库数据list
     */
    @PostMapping("/getWFYSKList")
    public Page<HxWfServerSearchOriginalOutDto> getWfYSKDataList(@RequestBody Page<String> page) throws Exception {
        List<HxWfServerSearchOriginalOutDto> list = dataHanDleService.getListByParam(page.getResult());
        Page<HxWfServerSearchOriginalOutDto> dtoPage = new Page<>();
        dtoPage.setList(list);
        return dtoPage;
    }


    /**
     * 用户使用记录存储
     * 接收前端异步传输数据接口，不需要返回任何值
     */
    @RequestMapping("/saveUserWFUseRecord")
    public void saveUserWFUseRecord(@RequestBody Page<HxWfServerSearchInDto> page) {
        dataHanDleService.saveUserWFUseRecord(page.getParams());
    }

}
