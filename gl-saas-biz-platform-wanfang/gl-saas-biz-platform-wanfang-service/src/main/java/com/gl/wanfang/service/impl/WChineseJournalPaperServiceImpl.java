package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChineseJournalPaperInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WChineseJournalPaper;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWChineseJournalPaperDalService;
import com.gl.wanfang.service.IWChineseJournalPaperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * service业务处理层
 *
 * @author code_generator
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class WChineseJournalPaperServiceImpl implements IWChineseJournalPaperService {

    @Autowired
    IWChineseJournalPaperDalService chineseJournalPaperDalService;
    @Autowired
    IDateGroupDalService dateGroupDalService;

    /**
     * 中文期刊
     *
     * @return
     */
    @Override
    public Page<WChineseJournalPaperOutDto> getListByPage(Page<WChineseJournalPaperInDto> page) {
        List<WChineseJournalPaper> listByPage = chineseJournalPaperDalService.getListByPage(page.getParams());

        //转换
        Page<WChineseJournalPaperOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WChineseJournalPaper> pager = (com.github.pagehelper.Page) listByPage;
            List<WChineseJournalPaperOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WChineseJournalPaperOutDto outDto = ConverBeanUtils.dtoToDo(e, WChineseJournalPaperOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;


    }

    @Override
    public List<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Year", "w_chinese_journal_paper");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public Page<?> getListByPage2(Page<WChineseJournalPaperInDto> page) {
        List<EasyWChineseJournalPaperOutDto> list = chineseJournalPaperDalService.getListByPage2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        List<EasyWChineseJournalPaperOutDto> listByPage2 = chineseJournalPaperDalService.getListByPage2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

    @Override
    public WChineseJournalPaperOutDto getOneByParams(Map<String, Object> params) {
        return chineseJournalPaperDalService.getOneByParams(params);
    }
}
