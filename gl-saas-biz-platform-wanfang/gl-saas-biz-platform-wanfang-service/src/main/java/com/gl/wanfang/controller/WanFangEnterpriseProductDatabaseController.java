package com.gl.wanfang.controller;


import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.indto.WanFangSupplierDatabaseInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WanFangSupplierDatabaseOutDto;
import com.gl.wanfang.service.IWExpertsLibraryService;
import com.gl.wanfang.service.IWanFangDBDataService;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 万方供应商搜索  仅供应商筛选功能点
 *
 * @文浩 2020-4-8
 */
@RestController
@RequestMapping("/getSupplier")
@Api(value = "万方", tags = {"供应商搜索"})
public class WanFangEnterpriseProductDatabaseController {

    @Autowired
    private IWanFangEnterpriseProductDatabaseService iWanFangEnterpriseProductDatabaseService;

    @Autowired
    private IWanFangDBDataService wanFangDBDataService;

    @Autowired
    IWExpertsLibraryService expertsLibraryService;

    /**
     *查询供应商
     * 没有用
     * @return
     */
/*
    @ApiOperation(notes = "查询供应商",value = "getListEnterpriseProductDatabase")
    @PostMapping("/getListEnterpriseProductDatabase")
    @UserSearchLog
    @Deprecated
    public Page<WanFangEnterpriseProductDatabaseOutDto> getListEnterpriseProductDatabase(@RequestBody Page<WanFangEnterpriseProductDatabaseInDto>  page){
       return iWanFangEnterpriseProductDatabaseService.getListEnterpriseProductDatabase(page);
    }
*/

    /**
     * 供应商查询
     *
     * @return
     */
    @ApiOperation(notes = "根据用户查询供应商", value = "getEnterpriseProductDatabase")
    @PostMapping("/getEnterpriseProductDatabase")
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFEnterpriseProduct:select")
    public Page<WanFangSupplierDatabaseOutDto> getEnterpriseProductDatabase(@RequestBody Page<WanFangSupplierDatabaseInDto> page) {
        return iWanFangEnterpriseProductDatabaseService.getEnterpriseProductDatabase(page);
    }


    /**
     * 企业类型聚合数据
     *
     * @return
     */
    @ApiOperation(notes = "企业类型聚合数据", value = "getCompanyGroup")
    @PostMapping("/getEPTypeGroup")
    @RequiresPermissions("WFEnterpriseProduct:select")
    public Page<DateGroupByOutDto> getEPTypeGroup() {
        List<DateGroupByOutDto> companyOrgTypeGroup = iWanFangEnterpriseProductDatabaseService.getCompanyTypeGroup();
        Page<DateGroupByOutDto> page = new Page();
        page.setList(companyOrgTypeGroup);
        return page;
    }

    /**
     * 供应商筛选-对应专利
     *
     * @return
     */
    @PostMapping("/getChinesePatentBySupplier")
   // @RequiresPermissions("WFEnterpriseProduct:select")
    @UserSearchLog
    @UserClickLog
    public Page<WChinesePatentOUTDTO> getChinesePatentBySupplier(@RequestBody Page<WChinesePatentINDTO> page) {
        Page<WChinesePatentOUTDTO> patentInfoByDB = wanFangDBDataService.getPatentInfoByDB(page);
        return patentInfoByDB;
    }


    /**
     *供应商筛选-对应专家
     * @return
     */
    /**
     * 万方专家信息详情本地检索
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertBySupplier")
    @ResponseBody
    @RequiresPermissions("WFEnterpriseProduct:select")
    @UserSearchLog
    @UserClickLog
    public Page<WExpertsLibraryOutDto> getExpertBySupplier(@RequestBody Page<WExpertsLibraryInDto> page) {
        return expertsLibraryService.getExpertInfoByDB(page);
    }

}
