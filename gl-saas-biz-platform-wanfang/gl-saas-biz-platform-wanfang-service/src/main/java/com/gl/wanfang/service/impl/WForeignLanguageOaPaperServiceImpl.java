package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WForeignLanguageOaPaperInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WForeignLanguageOaPaper;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWForeignLanguageOaPaperOutDto;
import com.gl.wanfang.outdto.WForeignLanguageOaPaperOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWForeignLanguageOaPaperDalService;
import com.gl.wanfang.service.IWForeignLanguageOaPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class WForeignLanguageOaPaperServiceImpl implements IWForeignLanguageOaPaperService {
    @Autowired
    IWForeignLanguageOaPaperDalService foreignLanguageOaPaperDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;

    /**
     * 外文oa模糊匹配
     *
     * @return
     */
    @Override
    public Page<WForeignLanguageOaPaperOutDto> getWForeignLanguageOaPaperByDB(Page<WForeignLanguageOaPaperInDto> page) {
        List<WForeignLanguageOaPaper> wForeignLanguageOaPaperByDB = foreignLanguageOaPaperDalService.getWForeignLanguageOaPaperByDB(page.getParams());

        //转换
        Page<WForeignLanguageOaPaperOutDto> dtoPage = new Page<>();
        if (wForeignLanguageOaPaperByDB instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WForeignLanguageOaPaper> pager = (com.github.pagehelper.Page<WForeignLanguageOaPaper>) wForeignLanguageOaPaperByDB;
            List<WForeignLanguageOaPaperOutDto> outlist = new ArrayList<>();
            wForeignLanguageOaPaperByDB.forEach(e -> {
                WForeignLanguageOaPaperOutDto outDto = ConverBeanUtils.dtoToDo(e, WForeignLanguageOaPaperOutDto.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());

        }
        return dtoPage;
    }

    /**
     * 时间聚合
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Issued_Year", "w_foreign_language_oa_paper");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public Page<?> getWForeignLanguageOaPaperByDB2(Page<WForeignLanguageOaPaperInDto> page) {
        List<EasyWForeignLanguageOaPaperOutDto> list = foreignLanguageOaPaperDalService.getWForeignLanguageOaPaperByDB2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public WForeignLanguageOaPaperOutDto getOneByParams(Map<String, Object> params) {
        return foreignLanguageOaPaperDalService.getOneByParams(params);
    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        List<EasyWForeignLanguageOaPaperOutDto> listByPage2 = foreignLanguageOaPaperDalService.getWForeignLanguageOaPaperByDB2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

}
