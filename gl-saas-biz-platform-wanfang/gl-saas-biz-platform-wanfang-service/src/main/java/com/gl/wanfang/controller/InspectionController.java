/*
package com.gl.wanfang.controller;

import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.service.IWanFangDataService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

*/
/**
 * 检验检测场景
 *//*

@RestController
public class InspectionController {

    @Autowired
    IWanFangDataService wanFangDataService;


    */
/**
     * 检验检测业务场景提供专家专利检索
     * @param response
     * @param request
     * @return
     *//*

    @ApiOperation(value = "检验检测业务场景")
    @GetMapping("/inspectionExpertAndPatent")
    @ResponseBody
    @UserSearchLog
    @Deprecated
//    @RequiresPermissions(value = "wfuser:select")
    public String InspectionExpertAndPatent(HttpServletResponse response, HttpServletRequest request) {
        String indexName = request.getParameter("indexName")==null?"10004,10014":request.getParameter("indexName");
        String data=wanFangDataService.matchSeach(response,request,indexName);//作者简略信息匹配
        return  Optional.ofNullable(data).map(value -> {
            */
/*处理简略信息，根据简略id，获取到对应的详细信息数据字段*//*

            return wanFangDataService.IdsInfo(response, request, value, indexName);
        }).orElse("");
    }
}
*/
