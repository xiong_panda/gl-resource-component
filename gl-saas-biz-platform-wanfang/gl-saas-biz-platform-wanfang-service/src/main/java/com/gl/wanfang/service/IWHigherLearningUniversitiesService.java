package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WHigherLearningUniversitiesInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWHigherLearningUniversitiesOutDto;
import com.gl.wanfang.outdto.WHigherLearningUniversitiesOutDto;

import java.util.List;
import java.util.Map;


public interface IWHigherLearningUniversitiesService {

    /**
     * 高等院校模糊匹配
     *
     * @param page
     * @return
     */
    Page<WHigherLearningUniversitiesOutDto> getWHigherLearningUniversitiesByDB(Page<WHigherLearningUniversitiesInDto> page);

    List<DateGroupByOutDto> getWanFangCAUAGroupLeve();

    /**
     * 通过名字查询一个
     *
     * @param params
     * @return
     */
    WHigherLearningUniversitiesOutDto selectOneByName(Map<String, Object> params);

    /**
     * 获取高校列表
     *
     * @param page
     * @return
     */
    Page<?> getWHigherLearningUniversitiesByDB2(Page<WHigherLearningUniversitiesInDto> page);


    /**
     * 获取详情信息
     *
     * @param params
     * @return
     */
    WHigherLearningUniversitiesOutDto getDetailByParams(Map<String, Object> params);

    /**
     * @author QinYe 城市群的高等院校简略检索
     * @date 2020/6/18 15:38
     * @description
     */
    CityOutResult getListByParams(CityPage page);
}
