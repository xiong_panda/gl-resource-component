package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.indto.WanFangEnterpriseProductDatabaseInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * xiaweijie
 * 中文专利搜索
 */
@RestController
@Api(value = "万方", tags = {"万方专利搜索"})
@RequestMapping("/patentData")
public class WanFangPatentDataController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangPatentDataController.class);
    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWanFangDBDataService wanFangDBDataService;
    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWAuthorLibraryService authorLibraryService;
    @Autowired
    private IWanFangEnterpriseProductDatabaseService iWanFangEnterpriseProductDatabaseService;




    /**
     * 万方专利信息详情本地检索
     *
     * @return EasyWChinesePatentOUTDTO
     */
    @ApiOperation(value = "万方专利信息详情检索")
    @PostMapping("/matchPatentInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFCNPatent:select")
    public CityOutResult matchPatentInfoByDB(@RequestBody CommBean commBean) {
        logger.info("专利简略查询:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("专利检索JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return wanFangDBDataService.getCityPatentInfoByDB2(cityInPage);
    }


    /**
     * 中文专利信息详情
     *
     * @return EasyWChinesePatentOUTDTO
     */
    @ApiOperation(value = "万方专利信息详情检索")
    @PostMapping("/matchPatentDetailInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFCNPatent:select")
    public CityOutResult  matchPatentDetailInfoByDB(@RequestBody CommBean commBean) {
        logger.info("万方专利详情查询:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("专利详情JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        return wanFangDBDataService.getCityOneByParams(cityInPage);
    }

    /**
     * 万方专利类型聚合检索
     *
     * @return
     */
    @ApiOperation(value = "万方专利类型聚合检索")
    @PostMapping("/getPatentGroup")
    @ResponseBody
    public Page<DateGroupByOutDto> getPatentGroup() {
        logger.info("万方专利类型聚合检索:{}");
        List<DateGroupByOutDto> companyOrgTypeGroup = wanFangDBDataService.getCompanyTypeGroup();
        Page<DateGroupByOutDto> page = new Page();
        page.setList(companyOrgTypeGroup);
        return page;
    }

    /**
     * 对应专家
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertByInspection")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("Inspection:select")
    public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
        return wScientificOrgService.getExpertByInspection(page);
    }

    /**
     * 万方作者信息详情检索
     *
     * @return
     */
    @ApiOperation(value = "万方作者信息本地检索")
    @PostMapping("/matchAuthorInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFaAuthor:select")
    public Page<?> MatchAuthorInfoByDB(@RequestBody Page<WAuthorLibraryInDto> page) {
        return authorLibraryService.getListByPage(page);
    }

    /**
     * 查询企业
     *
     * @return
     */
    @ApiOperation(notes = "查询企业", value = "getListEnterpriseProductDatabase")
    @PostMapping("/getCompanyByDB")
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFSignleEnterpriseProduct:select")
    public Page<WanFangEnterpriseProductDatabaseOutDto> getCompanyByDB(@RequestBody Page<WanFangEnterpriseProductDatabaseInDto> page) {
        logger.info("查询企业:{}");
        return iWanFangEnterpriseProductDatabaseService.getListEnterpriseProductDatabase(page);
    }
}
