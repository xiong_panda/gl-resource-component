package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WChineseConferencePaperInDto;
import com.gl.wanfang.indto.WChineseJournalPaperInDto;
import com.gl.wanfang.indto.WChinesePaperOaInDto;
import com.gl.wanfang.indto.WHigherLearningUniversitiesInDto;
import com.gl.wanfang.outdto.WChineseConferencePaperOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;
import com.gl.wanfang.outdto.WHigherLearningUniversitiesOutDto;
import com.gl.wanfang.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * xiaweijie
 * 万方作者搜索   城市群
 */
@RestController
@Api(value = "万方", tags = {"万方作者搜索"})
@RequestMapping("/authorData")
@Slf4j
public class WanFangAuthorDataController {

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWAuthorLibraryService authorLibraryService;

    @Autowired
    private IWChineseJournalPaperService iwChineseJournalPaperService;

    @Autowired
    private IWChineseConferenceCitationsService iwChineseConferenceCitationsService;

    @Autowired
    private IWChinesePaperOaService iwChinesePaperOaService;

    @Autowired
    IWHigherLearningUniversitiesService higherLearningUniversitiesService;

    /**
     * 万方作者信息详情检索
     *
     * @return
     */
    @ApiOperation(value = "万方作者信息本地检索")
    @PostMapping("/matchAuthorInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    // @RequiresPermissions("WFaAuthor:select")
    public CityOutResult MatchAuthorInfoByDB(@RequestBody CommBean commBean) {
        log.info("作者检索：{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("作者检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getCode(), CityCommonErroCode.JSON_PARSE_ERRO.getMsg());
        }


        return authorLibraryService.getCityListByParams(cityInPage);
    }


    /**
     * 作者详细信息接口
     *
     * @param page
     * @return
     */
    @ApiOperation(value = "万方作者信息本地检索")
    @PostMapping("/matchAuthorDetailInfoByDB")
    @ResponseBody
	@UserSearchLog
	@UserClickLog
    // @RequiresPermissions("WFaAuthor:select")
    public CityOutResult matchAuthorDetailInfoByDB(@RequestBody CommBean commBean) {
        log.info("作者详情：{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("作者详情JSON解析异常:", e);
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return authorLibraryService.getCityDetailByParams(cityInPage);
    }

    /**
     * 作者联查高等院校
     *
     * @return
     */
    @ApiOperation(value = "万方高等院校本地搜索")
    @PostMapping("/wanFangCAUAByDB")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    // @RequiresPermissions("WFCAUA:select")
    public Page<?>   wanFangCAUAByDB(@RequestBody Page<WHigherLearningUniversitiesInDto> page){
        WHigherLearningUniversitiesOutDto outDto = higherLearningUniversitiesService.selectOneByName(page.getParams());
        page.setResult(outDto);
        return page;
    }

    /**
     * @auther Qinye
     * @Description 根据作者获取中文期刊
     * @date 2020/5/29 15:02
     */
    @RequestMapping(value = "/getPeriodicalByAuthor", method = RequestMethod.POST)
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    public Page<WChineseJournalPaperOutDto> getPeriodicalByAuthor(@RequestBody Page<WChineseJournalPaperInDto> page) {
        return iwChineseJournalPaperService.getListByPage(page);
    }

    /**
     * @auther Qinye
     * @Description 根据作者获取中文会议论文
     * @date 2020/5/29 15:26
     */
    @RequestMapping(value = "/getWanFangCNMeetingByAuthor", method = RequestMethod.POST)
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    public Page<WChineseConferencePaperOutDto> getWanFangCNMeetingByAuthor(@RequestBody Page<WChineseConferencePaperInDto> page) {
        return iwChineseConferenceCitationsService.getListByPage(page);
    }

    /**
     * @auther Qinye
     * @Description 根据作者获取中文OA
     * @date 2020/5/29 15:30
     */
    @RequestMapping(value = "/getScientificOrgByAuthor", method = RequestMethod.POST)
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    public Page<WChinesePaperOaOutDto> getScientificOrgByAuthor(@RequestBody Page<WChinesePaperOaInDto> page) {
        return iwChinesePaperOaService.getForm(page);
    }
}
