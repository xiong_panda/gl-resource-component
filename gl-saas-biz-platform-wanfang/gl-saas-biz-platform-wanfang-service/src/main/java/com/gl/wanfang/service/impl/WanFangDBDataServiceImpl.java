package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.api.fegin.PatentFegin;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.biz.city.CommonSearchErroCode;
import com.gl.wanfang.bo.SearchBO;
import com.gl.wanfang.constant.CityConstant;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WChinesePatent;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWanFangDBDataDalService;
import com.gl.wanfang.service.IWanFangDBDataService;
import com.gl.wanfang.util.ConvertCommBeanUtil;
import com.gl.wanfang.validator.CommonValidator;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@Slf4j
public class WanFangDBDataServiceImpl implements IWanFangDBDataService {
    private static final Logger logger = LoggerFactory.getLogger(WanFangDBDataServiceImpl.class);
    @Autowired
    IWanFangDBDataDalService wanFangDBDataServiceDal;

    @Autowired
    private IDateGroupDalService dateGroupDalService;

    @Autowired
    private CommonValidator commonValidator;

    @Autowired
    private PatentFegin patentFegin;

    @Autowired
    private ConvertCommBeanUtil convertCommBeanUtil;

    /**
     * 专利查询
     *
     * @param page
     * @return
     */
    @Override
    public Page<WChinesePatentOUTDTO> getPatentInfoByDB(Page<WChinesePatentINDTO> page) {


        List<WChinesePatent> patentInfoByDB = wanFangDBDataServiceDal.getPatentInfoByDB(page.getParams());

        Page<WChinesePatentOUTDTO> dtoPage = new Page<>();
        //转换
        if (patentInfoByDB instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WChinesePatent> pager = (com.github.pagehelper.Page<WChinesePatent>) patentInfoByDB;
            List<WChinesePatentOUTDTO> outlist = new ArrayList<>();
            patentInfoByDB.forEach(e -> {
                WChinesePatentOUTDTO outDto = ConverBeanUtils.dtoToDo(e, WChinesePatentOUTDTO.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    /**
     * 专利类型聚合
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getCompanyTypeGroup() {

        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("PatentType_Name ", "w_chinese_patent");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        Gson gson = new Gson();
        dataGroup.forEach(v -> {
            String groupData = v.getGroupData();
            List list = gson.fromJson(groupData, List.class);
            String o = String.valueOf(list.get(0));
            v.setGroupData(o);
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public Page<?> getPatentInfoByDB2(Page<WChinesePatentINDTO> page) {
        List<EasyWChinesePatentOUTDTO> list = wanFangDBDataServiceDal.getPatentInfoByDB2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public WChinesePatentOUTDTO getOneByParams(Map<String, Object> params) {
        return wanFangDBDataServiceDal.getOneByParams(params);
    }


    @Override
    public CityOutResult getCityPatentInfoByDB2(CityPage page) {
        commonValidator.validator(page);
        CityOutResult cityOutResult = new CityOutResult();

        List<EasyWChinesePatentOUTDTO> list = null;
        try {
            list = wanFangDBDataServiceDal.getPatentInfoByDB2(page.getParams());
        } catch (Exception e) {
            logger.info("专利详情JSON解析异常:", e);
            throw new BusinessException(CommonSearchErroCode.CPATENT_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CPATENT_SEARCH_ERRO.getCode());
        }

        //如果本地条数小于最小阀值，则调用万方的接口
        if (list.size() < CityConstant.LOCAL_MIN) {
            //万方接口参数构造
            SearchBO searchBO = new SearchBO();
            searchBO.setResourceCls(CityConstant.CHINESE_PATENT_CODE);

            Map<String, Object> params =new HashMap<>();
            params=page.getParams();
            params.remove("id");
            params.remove("__pageSize__");
            params.remove("__pageNo__");
            Set<Map.Entry<String, Object>> set = params.entrySet();
            String filed = "";
            if (set.size() > 0) {
                if (set.size() > 1) {
                    for (Map.Entry mapKey : set) {
                        if (!"".equals(mapKey.getValue())) {
                            filed += mapKey.getKey() + ":" + mapKey.getValue().toString() + "&";
                        }
                    }
                } else {
                    for (Map.Entry mapKey : set) {
                        if (!"".equals(mapKey.getValue())) {
                            searchBO.setKeyword(mapKey.getValue().toString());
                        }
                    }
                }
            }
            searchBO.setFiled(filed);
            searchBO.setFrom(page.getPageNo() + "");
            String sign = convertCommBeanUtil.getSign(CityConstant.WANFANG_JIANLUE, JSONObject.toJSONString(searchBO));

            CommBean commBean = convertCommBeanUtil.ConvertCommBean(CityConstant.WANFANG_JIANLUE, JSONObject.toJSONString(searchBO), sign);

            //万方接口
            try {
                CityOutResult wfCityOutResult = patentFegin.MatchAuthorInfoByDB(commBean);

                cityOutResult.setResult(wfCityOutResult.getResult());
                return cityOutResult;
            }catch(Exception e){
                log.info("",e);
                cityOutResult.setResult(list);
            }
        }

        cityOutResult.setResult(list);
        return cityOutResult;
    }


    @Override
    public CityOutResult getCityOneByParams(CityPage page) {
        commonValidator.validator(page);
        CityOutResult cityOutResult = new CityOutResult();

        WChinesePatentOUTDTO list = null;
        try {
            list = wanFangDBDataServiceDal.getOneByParams(page.getParams());
        } catch (Exception e) {
            logger.info("专利详情JSON解析异常:", e);
            throw new BusinessException(CommonSearchErroCode.CPATENT_DETAIL_ERRO.getMsg(), CommonSearchErroCode.CPATENT_DETAIL_ERRO.getCode());
        }

        //如果list为null，则走万方接口
        if(null==list){
            //万方接口参数构造
            SearchBO searchBO = new SearchBO();
            searchBO.setResourceCls(CityConstant.CHINESE_PATENT_CODE);
            searchBO.setFiled("id:"+String.valueOf(page.getParams().get("id")));
            searchBO.setFrom(page.getPageNo() + "");
            String sign = convertCommBeanUtil.getSign(CityConstant.WANFANG_XAINGQING, JSONObject.toJSONString(searchBO));

            CommBean commBean = convertCommBeanUtil.ConvertCommBean(CityConstant.WANFANG_XAINGQING, JSONObject.toJSONString(searchBO), sign);

            //万方接口
            try {
                CityOutResult wfCityOutResult = patentFegin.MatchAuthorInfoByDB(commBean);

                cityOutResult.setResult(wfCityOutResult.getResult());
                return cityOutResult;
            }catch(Exception e){
                log.info("",e);
                cityOutResult.setResult(list);
            }
        }

        cityOutResult.setResult(list);
        return cityOutResult;
    }
}
