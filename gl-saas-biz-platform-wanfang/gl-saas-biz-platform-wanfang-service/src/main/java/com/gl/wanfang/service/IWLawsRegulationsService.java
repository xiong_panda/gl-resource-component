package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommBean;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WLawsRegulationsInDTO;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WLawsRegulations;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WLawsRegulationsOutDTO;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWLawsRegulationsService {

    /**
     * 法律法规
     * 分页模糊匹配
     */
    Page<WLawsRegulationsOutDTO> getListByPag(Page<WLawsRegulationsInDTO> page);

    /**
     * "万方法律法规效力级别聚合检索"
     *
     * @param page
     * @return
     */
    List<DateGroupByOutDto> getGroupValueLevel();

    /**
     * 简略信息列表
     *
     * @param page
     * @return
     */
    Page<?> getListByPag2(Page<WLawsRegulationsInDTO> page);

    /**
     * 详情接口
     *
     * @param params
     * @return
     */
    WLawsRegulationsOutDTO getOneByParams(Map<String, Object> params);

    /**
     * 简略信息列表
     *
     * @param page
     * @return
     */
    CityOutResult getCityListByPag2(CityPage cityInPage);

    /**
     * 详情接口
     *
     * @param params
     * @return
     */
    CityOutResult getCityOneByParams(CityPage cityInPage);
}
