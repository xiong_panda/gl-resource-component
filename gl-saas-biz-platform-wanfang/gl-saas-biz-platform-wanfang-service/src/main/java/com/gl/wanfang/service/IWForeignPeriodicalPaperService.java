package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.indto.WChineseConferenceCitationsInDto;
import com.gl.wanfang.indto.WForeignPeriodicalPaperInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChineseConferenceCitationsOutDto;
import com.gl.wanfang.outdto.WForeignPeriodicalPaperOutDto;

import java.util.List;
import java.util.Map;

public interface IWForeignPeriodicalPaperService {
    /**
     * 分页匹配模糊查询
     *
     * @return
     */
    Page<WForeignPeriodicalPaperOutDto> getListByPage(Page<WForeignPeriodicalPaperInDto> page);

    /**
     * OA外文期刊时间聚合
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupDate();

    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WForeignPeriodicalPaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * 获取简略信息 列表
     *
     * @param page
     * @return
     */
    Page<?> getListByPage2(Page<WForeignPeriodicalPaperInDto> page);
}
