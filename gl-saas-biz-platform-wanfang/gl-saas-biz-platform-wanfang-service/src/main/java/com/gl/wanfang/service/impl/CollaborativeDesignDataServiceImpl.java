package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WChinesePatent;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.service.ICollaborativeDesignDataService;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWanFangDBDataDalService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CollaborativeDesignDataServiceImpl implements ICollaborativeDesignDataService {

    @Autowired
    IWanFangDBDataDalService wanFangDBDataServiceDal;

    @Autowired
    private IDateGroupDalService dateGroupDalService;
    /**
     * 系统设计-专利查询
     * @param page
     * @return
     */
    @Override
    public Page<WChinesePatentOUTDTO> getPatentInfoByDB(Page<WChinesePatentINDTO> page) {

        List<WChinesePatent> patentInfoByDB = wanFangDBDataServiceDal.getPatentInfoByDB(page.getParams());

        Page<WChinesePatentOUTDTO> dtoPage = new Page<>();
        //转换
        if ( patentInfoByDB instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WChinesePatent> pager = (com.github.pagehelper.Page<WChinesePatent>) patentInfoByDB;
            List<WChinesePatentOUTDTO> outlist = new ArrayList<>();
            patentInfoByDB.forEach(e -> {
                WChinesePatentOUTDTO outDto = ConverBeanUtils.dtoToDo(e, WChinesePatentOUTDTO.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    /**
     * 专利类型聚合
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getCompanyTypeGroup() {

        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("PatentType_Name ", "w_chinese_patent");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        Gson gson=new Gson();
        dataGroup.forEach(v->{
            String groupData = v.getGroupData();
            List list = gson.fromJson(groupData, List.class);
            String o = String.valueOf(list.get(0));
            v.setGroupData(o);
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }
}
