package com.gl.wanfang.service.impl;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WHigherLearningUniversitiesInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WHigherLearningUniversities;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWHigherLearningUniversitiesOutDto;
import com.gl.wanfang.outdto.WHigherLearningUniversitiesOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWHigherLearningUniversitiesDalService;
import com.gl.wanfang.service.IWHigherLearningUniversitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * service业务处理层
 *
 * @author code_generator
 */

@Service
public class WHigherLearningUniversitiesServiceImpl implements IWHigherLearningUniversitiesService {

    @Autowired
    IWHigherLearningUniversitiesDalService higherLearningUniversitiesDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;

    /**
     * 高等院校模糊匹配
     *
     * @param page
     * @return
     */
    @Override
    public Page<WHigherLearningUniversitiesOutDto> getWHigherLearningUniversitiesByDB(Page<WHigherLearningUniversitiesInDto> page) {
        List<WHigherLearningUniversities> wHigherLearningUniversitiesByDB = higherLearningUniversitiesDalService.getWHigherLearningUniversitiesByDB(page.getParams());

        //转换
        Page<WHigherLearningUniversitiesOutDto> dtoPage = new Page<>();
        if (wHigherLearningUniversitiesByDB instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WHigherLearningUniversities> pager = (com.github.pagehelper.Page) wHigherLearningUniversitiesByDB;
            List<WHigherLearningUniversitiesOutDto> outlist = new ArrayList<>();
            wHigherLearningUniversitiesByDB.forEach(e -> {
                WHigherLearningUniversitiesOutDto outDto = ConverBeanUtils.dtoToDo(e, WHigherLearningUniversitiesOutDto.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());

        }
        return dtoPage;


    }

    @Override
    public List<DateGroupByOutDto> getWanFangCAUAGroupLeve() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Levels", "w_higher_learning_universities");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public WHigherLearningUniversitiesOutDto selectOneByName(Map<String, Object> params) {
        return ConverBeanUtils.dtoToDo(higherLearningUniversitiesDalService.selectOneByName(params), WHigherLearningUniversitiesOutDto.class);
    }


    @Override
    public Page<?> getWHigherLearningUniversitiesByDB2(Page<WHigherLearningUniversitiesInDto> page) {
        List<EasyWHigherLearningUniversitiesOutDto> wHigherLearningUniversitiesByDB = higherLearningUniversitiesDalService.getWHigherLearningUniversitiesByDB2(page.getParams());
        page.setResult(wHigherLearningUniversitiesByDB);
        return page;
    }

    @Override
    public WHigherLearningUniversitiesOutDto getDetailByParams(Map<String, Object> params) {
        return higherLearningUniversitiesDalService.getDetailByParams(params);
    }

    @Override
    public CityOutResult getListByParams(CityPage page) {
        List<EasyWHigherLearningUniversitiesOutDto> listByPage2 = higherLearningUniversitiesDalService.getWHigherLearningUniversitiesByDB2(page.getParams());
        CityOutResult cityOutResult = new CityOutResult();
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }
}
