package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WOverseasPatentInDto;
import com.gl.wanfang.indto.WanFangEnterpriseProductDatabaseInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.service.IWOverseasPatentService;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * xiaweijie
 * 万方海外专利搜索
 */
@RestController
@Api(value = "万方", tags = {"万方海外专利检索"})
@RequestMapping("/enPatentData")
@Slf4j
public class WanFangENPatentDataController {

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWOverseasPatentService overseasPatentService;

    @Autowired
    private IWanFangEnterpriseProductDatabaseService iWanFangEnterpriseProductDatabaseService;

    /**
     * @author QinYe 海外专利搜索--城市群
     * @date 2020/6/18 16:44
     * @description
    */
    @ApiOperation(value = "海外专利本地搜索")
    @PostMapping("/matchENPatentInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFENPatent:select")
    public CityOutResult matchENPatentInfoByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("海外专利检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        try {
            cityListByParams = overseasPatentService.getListByParams(cityInPage);
        } catch (Exception e) {
            log.info("海外专利检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.OVERSEAS_PATENT_SEARCH_ERRO.getMsg(), CommonSearchErroCode.OVERSEAS_PATENT_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * @author QinYe 海外专利详情搜索--城市群
     * @date 2020/6/18 16:44
     * @description
     */
    @ApiOperation(value = "海外专利详情搜索")
    @PostMapping("/matchENPatentDetailInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @UserSearchLog
    //    @RequiresPermissions("WFENPatent:select")
    public CityOutResult matchENPatentDetailInfoByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("海外专利检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        if(ObjectUtils.isEmpty(cityInPage.getParams().get("id"))){
            log.info("海外专利检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        try {
            cityListByParams.setResult(overseasPatentService.getOneByParams(cityInPage.getParams()));
        } catch (Exception e) {
            log.info("海外专利检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.OVERSEAS_PATENT_SEARCH_ERRO.getMsg(), CommonSearchErroCode.OVERSEAS_PATENT_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }

    //


    /**
     * 海外专利时间聚合
     *
     * @return
     */
    @ApiOperation(value = "海外专利时间聚合")
    @PostMapping("/getENPatentGroup")
    @ResponseBody
    public Page<DateGroupByOutDto> getENPatentGroup() {
        List<DateGroupByOutDto> groupDate = overseasPatentService.getGroupDate();
        Page<DateGroupByOutDto> page = new Page<>();
        page.setList(groupDate);
        return page;

    }

    /**
     * 查询企业
     *
     * @return
     */
    @ApiOperation(notes = "查询企业", value = "getListEnterpriseProductDatabase")
    @PostMapping("/getCompanyByDB")
//    @UserSearchLog
//    @UserClickLog
//    @RequiresPermissions("WFSignleEnterpriseProduct:select")
    public Page<WanFangEnterpriseProductDatabaseOutDto> getCompanyByDB(@RequestBody Page<WanFangEnterpriseProductDatabaseInDto> page) {
        return iWanFangEnterpriseProductDatabaseService.getListEnterpriseProductDatabase(page);
    }
}
