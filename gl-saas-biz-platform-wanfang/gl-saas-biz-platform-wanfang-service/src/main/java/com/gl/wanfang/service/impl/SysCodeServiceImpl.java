package com.gl.wanfang.service.impl;

import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.basis.common.util.IdWorker;
import com.gl.wanfang.outdto.SysCodeOutDto;
import com.gl.wanfang.service.ISysCodeDalService;
import com.gl.wanfang.service.ISysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 	 	字典信息获取
 * @author Administrator
 *
 */

/**
 * @author admin
 *
 */
@Service
public class SysCodeServiceImpl implements ISysCodeService {

	@Autowired
	private ISysCodeDalService sysCodeDalService;

	@Override
	public SysCodeOutDto translateCode(Map<String, Object> params) {
		return ConverBeanUtils.doToDto(sysCodeDalService.translateCode(params),SysCodeOutDto.class);
	}

}
