package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.service.IBaseService;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.indto.WScientificOrgInDto;
import com.gl.wanfang.indto.WanFangEnterpriseProductDatabaseInDto;
import com.gl.wanfang.model.WScientificOrg;
import com.gl.wanfang.outdto.*;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWScientificOrgService extends IBaseService<WScientificOrg> {

	/*
	 * 查询科研机构分页数据
	 */
	Page<WScientificOrgOutDto> getForm(Page<WScientificOrgInDto> page);

	/**
	 * 聚合科研机构地区
	 *
	 * @return
	 */
	List<DateGroupByOutDto> getGroupOrgType();

	/**
	 * 检验检测分页查询
	 * 
	 * @param page
	 * @return
	 */
//    Page<WanFangEnterpriseProductDatabaseOutDto> getInspectionBySupplier(Page<WanFangEnterpriseProductDatabaseInDto> page);

	/**
	 * 检验检测-专利查询
	 *
	 * @param page
	 * @return
	 */
	Page<WChinesePatentOUTDTO> getChinesePatentByInspection(Page<WChinesePatentINDTO> page);

	/**
	 * 检验检测-专家查询
	 *
	 * @param page
	 * @return
	 */
	Page<WExpertsLibraryOutDto> getExpertByInspection(Page<WExpertsLibraryInDto> page);

	/**
	 * 机构检索
	 *
	 * @param page
	 * @return
	 */
	Page<?> getForm2(Page<WScientificOrgInDto> page);

	/**
	 * 机构检索详情
	 *
	 * @param page
	 * @return
	 */
	WScientificOrgOutDto getOneParams(Map<String, Object> params);

	/**
	 * 城市群接口
	 * 
	 * @param cityInPage
	 * @return
	 */
	CityOutResult getForm2(CityPage cityInPage);

	CityOutResult getOneParams(CityPage cityInPage);
}
