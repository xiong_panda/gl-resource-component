package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.indto.WChineseJournalPaperInDto;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.service.IWAuthorLibraryService;
import com.gl.wanfang.service.IWChineseJournalPaperService;
import com.gl.wanfang.service.IWScientificOrgService;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.config.UserSearchLog;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import sun.rmi.runtime.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 中文期刊搜索
 * 10001：中文期刊
 */
@RestController
@Api(value = "万方", tags = {"万方中文期刊搜索"})
@RequestMapping("/periodicalData")
@Slf4j
public class WanFangPeriodicalController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangPeriodicalController.class);

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWChineseJournalPaperService chineseJournalPaperService;
    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWAuthorLibraryService authorLibraryService;

    /**
     * @author QinYe 万方中文期刊简略检索-城市群
     * @date 2020/6/18 15:26
     * @description
     */
    @ApiOperation(value = "万方中文期刊详情本地检索")
    @PostMapping("/chineseJournalPapersByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFCNMechanism:select")
    public CityOutResult matchMechanismInfoByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("中文期刊检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        try {
            cityListByParams = chineseJournalPaperService.getListByParams(cityInPage);
        } catch (Exception e) {
            log.info("中文期刊检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.CHINESE_JOURNAL_PAPERS_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHINESE_JOURNAL_PAPERS_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }

    /**
     * @author QinYe 万方中文期刊详情检索-城市群
     * @date 2020/6/18 15:26
     * @description
     */
    @ApiOperation(value = "万方中文期刊详情检索")
    @PostMapping("/matchMechanismDetailInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFCNMechanism:select")
    public CityOutResult matchMechanismDetailInfoByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("中文期刊检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        if(ObjectUtils.isEmpty(cityInPage.getParams().get("id"))){
            log.info("中文期刊检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        try {
            cityListByParams.setResult(chineseJournalPaperService.getOneByParams(cityInPage.getParams()));
        } catch (Exception e) {
            log.info("中文期刊检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.CHINESE_JOURNAL_PAPERS_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHINESE_JOURNAL_PAPERS_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }

    /**
     * 中文期刊时间聚合
     *
     * @return
     */
    @ApiOperation(value = "中文期刊时间聚合")
    @PostMapping("/chineseJournalPapersGroup")
    @ResponseBody
    public Page<DateGroupByOutDto> getChineseJournalPapersGroup() {
        List<DateGroupByOutDto> groupDate = chineseJournalPaperService.getGroupDate();
        Page<DateGroupByOutDto> page = new Page<>();
        page.setList(groupDate);
        return page;

    }

    /**
     * 对应专家
     *
     * @return
     */
    @ApiOperation(value = "万方专家信息详情本地检索")
    @PostMapping("/getExpertByInspection")
    @ResponseBody
    @UserClickLog
    //    @RequiresPermissions("Inspection:select")
    public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
        return wScientificOrgService.getExpertByInspection(page);
    }

    /**
     * 万方作者信息详情检索
     *
     * @return
     */
    @ApiOperation(value = "万方作者信息本地检索")
    @PostMapping("/matchAuthorInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFaAuthor:select")
    public Page<?> MatchAuthorInfoByDB(@RequestBody Page<WAuthorLibraryInDto> page) {
        return authorLibraryService.getListByPage(page);
    }
}
