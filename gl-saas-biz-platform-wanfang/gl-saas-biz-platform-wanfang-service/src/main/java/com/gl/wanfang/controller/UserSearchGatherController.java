package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.basis.util.RedisUtil;
import com.gl.wanfang.bo.GatherBO;
import com.gl.wanfang.indto.GatherInDTO;
import com.gl.wanfang.indto.HxWfServerSearchInDto;
import com.gl.wanfang.outdto.GatherOutDTO;
import com.gl.wanfang.outdto.HxWfServerSearchOutDto;
import com.gl.wanfang.service.IUserClickService;
import com.gl.wanfang.service.UserSearchGatherService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 从缓存中获取用户的搜索记录
 */
@RestController
@RequestMapping("/gather")
@Slf4j
public class UserSearchGatherController {
    private static final Logger logger = LoggerFactory.getLogger(UserSearchGatherController.class);

    @Autowired
    private UserSearchGatherService userSearchGatherService;
    @Autowired
    private IUserClickService userClickService;

    /**
     * @auther Qinye
     * @Description 热搜，推荐数据读取
     * @date 2020/5/11 14:00
     */
    @ResponseBody
    @RequestMapping(value = "/getHotAndRecommend",method = RequestMethod.POST)
    public Page<GatherOutDTO> getAllHotAndRecommend(@RequestBody Page<GatherInDTO> page){
        logger.info("总热搜/推荐查询：{}",JSON.toJSON(page));
        List<GatherOutDTO> list = userSearchGatherService.getHotAndRecommend(ConverBeanUtils.dtoToDo(page.getT(),GatherBO.class));
        Page<GatherOutDTO> newPage = new Page<GatherOutDTO>();
        newPage.setResult(list);
        return newPage;
    }

    /**
     * @auther Qinye
     * @Description 用户访问数据采集提供
     * @date 2020/6/15 14:59
    */
    @ResponseBody
    @RequestMapping(value="/gatherUserViewInfo",method = RequestMethod.POST)
    public Page<HxWfServerSearchOutDto> gatherUserViewInfo(@RequestBody Page<HxWfServerSearchInDto> page){
        logger.info("用户访问数据采集提供：{}",JSON.toJSON(page));
        Page<HxWfServerSearchOutDto> newPage = userClickService.getListByCollectTime(page);
        newPage.setMsg("查询成功");
        return newPage;
    }
}