package com.gl.wanfang.controller;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.indto.WForeignPeriodicalPaperInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WForeignPeriodicalPaperOutDto;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;
import com.gl.wanfang.service.IWForeignPeriodicalPaperService;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.config.UserSearchLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

/**
 * 万方外文期刊论文搜索
 */
@RestController
@Api(value = "万方", tags = {"万方外文期刊论文搜索"})
@RequestMapping("/enPeriodical")
public class WanFangENPeriodicalController {

    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWForeignPeriodicalPaperService foreignPeriodicalPaperService;

    /**
     * 万方外文期刊详情检索
     * @param response
     * @param request
     * @return
     */
/*
    @ApiOperation(value = "万方外文期刊详情检索")
    @GetMapping("/matchENJournalPapersInfo")
    @ResponseBody
    @UserSearchLog
    @Deprecated
    public String MatchENJournalPapersInfo(HttpServletResponse response, HttpServletRequest request) {
        String indexName = request.getParameter("indexName")==null?"10021": request.getParameter("indexName");
        String data=wanFangDataService.matchSeach(response,request,indexName);//外文期刊简略信息匹配

        return  Optional.ofNullable(data).map(value -> {
            */
    /*处理简略信息，根据简略id，获取到对应的详细信息数据字段*//*

            return wanFangDataService.IdsInfo(response, request, value, indexName);
        }).orElse("");
    }
*/


    /**
     * 万方外文期刊本地检索 简略信息
     * com.gl.wanfang.outdto.EasyWForeignPeriodicalPaperOutDto
     *
     * @return
     */
    @ApiOperation(value = "万方外文期刊本地检索")
    @PostMapping("/matchENJournalPapersInfoDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFENJournalPapers:select")
    public Page<?> matchENJournalPapersInfoDB(@RequestBody Page<WForeignPeriodicalPaperInDto> page) {
        return foreignPeriodicalPaperService.getListByPage2(page);
    }


    /**
     * 万方外文期刊本地检索 详情
     *
     * @return
     */
    @ApiOperation(value = "万方外文期刊本地检索")
    @PostMapping("/matchENJournalPapersDetailInfo")
    @ResponseBody
//    @UserSearchLog
//    @UserClickLog
    //    @RequiresPermissions("WFENJournalPapers:select")
    public Page<?> matchENJournalPapersDetailInfo(@RequestBody Page<WForeignPeriodicalPaperInDto> page) {
        WForeignPeriodicalPaperOutDto s = foreignPeriodicalPaperService.getOneByParams(page.getParams());
        page.setResult(s);
        return page;
    }


    /**
     * 万方外文期刊时间聚合
     *
     * @return
     */
    @ApiOperation(value = "万方外文期刊时间聚合")
    @PostMapping("/getGroupDateByFPP")
    @ResponseBody
    public Page<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByOutDto> groupDate = foreignPeriodicalPaperService.getGroupDate();
        Page page = new Page();
        page.setList(groupDate);
        return page;
    }
}
