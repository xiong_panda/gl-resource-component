package com.gl.wanfang.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "gl.wanfang.apply.num")
@PropertySource(value = {"classpath:META-INF/app.properties"}, encoding = "UTF-8")
public class ApplyNumProperties {
    /**
     * 多少数据量就请求本地
     */
    @Value("${gl.wanfang.apply.num.max}")
    private String maxnum;

    @Value("${gl.wanfang.apply.appkey}")
    private String appkey;

    @Value("${gl.wanfang.apply.version}")
    private String version;

    @Value("${gl.wanfang.apply.sign_method}")
    private String signMethod;

    @Value("${gl.wanfang.apply.format}")
    private String format;

    @Value("${gl.wanfang.apply.signKey}")
    private String signKey;


    public String getMaxnum() {
        return maxnum;
    }

    public void setMaxnum(String maxnum) {
        this.maxnum = maxnum;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSignMethod() {
        return signMethod;
    }

    public void setSignMethod(String signMethod) {
        this.signMethod = signMethod;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getSignKey() {
        return signKey;
    }

    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }
}
