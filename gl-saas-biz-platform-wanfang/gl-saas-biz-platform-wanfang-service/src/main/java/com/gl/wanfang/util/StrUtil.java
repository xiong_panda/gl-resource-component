package com.gl.wanfang.util;


public class StrUtil {

    /*
     * 去掉首尾指定字符串
     */
    public static String trimBothEndsChars(String srcStr, String splitter) {
        String regex = "^" + splitter + "*|" + splitter + "*$";
        return srcStr.replaceAll(regex, "");
    }

    /*
     * 索引库限制
     * {"10001","10002","10016","10004","10006","10003","10012"}
     */
    public static String IndexLibrary(String[] indexN, String[] indexNameArr) {
        String indexName="";
        for(int i=0;i<indexN.length;i++){
            for (int j=0;j<indexNameArr.length;j++){
                if(indexN[i].equals(indexNameArr[j])){
                    indexName+=indexNameArr[j]+",";
                }
            }
        }
        return  StrUtil.trimBothEndsChars(indexName, ",");

    }

}
