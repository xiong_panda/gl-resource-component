//package com.gl.wanfang.feign;
//
//import com.gl.basis.common.pojo.Page;
//import com.gl.wanfang.indto.SysCodeInDto;
//import com.gl.wanfang.indto.SysUserInDto;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * @auther Qinye
// * @date 2020/6/11 14:44
// * @description 平台认证服务
// */
//@FeignClient(value = "saas-platform-authority")
//public interface AuthorityClient {
//
//    /**
//     * @auther Qinye
//     * @Description 码值对照
//     * @date 2020/6/11 15:25
//    */
//    @RequestMapping(value = "/sysCode/translateCode")
//    @ResponseBody
//    Page<SysCodeInDto> translateCode(@RequestBody Page<SysCodeInDto> page);
//
//    /**
//     * @auther Qinye
//     * @Description 查询用户详情
//     * @date 2020/6/11 15:26
//     */
//    @ResponseBody
//    @RequestMapping(value = "/sysUser/getOne", method = RequestMethod.POST)
//    Page<SysUserInDto> getOne(@RequestBody Page<SysUserInDto> page);
//}
