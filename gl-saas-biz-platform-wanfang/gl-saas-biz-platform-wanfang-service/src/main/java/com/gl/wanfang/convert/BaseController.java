//package com.gl.wanfang.convert;
//
//import lombok.Data;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Data
//public class BaseController {
//    protected HttpServletRequest request;
//    protected HttpServletResponse response;
//    protected String mobile="12321312321";
//    protected String userId="3232323243";
//    protected String username="测试";
//    protected String fMidCompanyid="32322";
//    protected String companyUserType = "32323";
//    protected String userType="3232";
//
//    public HttpServletRequest getRequest() {
//        return request;
//    }
//
//    public void setRequest(HttpServletRequest request) {
//        this.request = request;
//    }
//
//    public HttpServletResponse getResponse() {
//        return response;
//    }
//
//    public void setResponse(HttpServletResponse response) {
//        this.response = response;
//    }
//
//    public String getMobile() {
//        return mobile;
//    }
//
//    public void setMobile(String mobile) {
//        this.mobile = mobile;
//    }
//
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getfMidCompanyid() {
//        return fMidCompanyid;
//    }
//
//    public void setfMidCompanyid(String fMidCompanyid) {
//        this.fMidCompanyid = fMidCompanyid;
//    }
//
//    public String getCompanyUserType() {
//        return companyUserType;
//    }
//
//    public void setCompanyUserType(String companyUserType) {
//        this.companyUserType = companyUserType;
//    }
//
//    public String getUserType() {
//        return userType;
//    }
//
//    public void setUserType(String userType) {
//        this.userType = userType;
//    }
//}
