package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.service.IBaseService;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WOverseasPatentInDto;
import com.gl.wanfang.model.WOverseasPatent;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWOverseasPatentOutDto;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWOverseasPatentService extends IBaseService<WOverseasPatent> {

    /**
     * 海外专利 模糊匹配
     *
     * @return
     */
    Page<WOverseasPatentOutDto> getListByPage(Page<WOverseasPatentInDto> page);

    /**
     * 海外专利 时间聚合检索
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupDate();


    /**
     * 获取简略列表
     *
     * @param page
     * @return
     */
    Page<?> getListByPage2(Page<WOverseasPatentInDto> page);

    /**
     * @author QinYe 海外专利搜索--城市群
     * @date 2020/6/18 16:44
     * @description
     */
    CityOutResult getListByParams(CityPage page);

    /**
     * 获取海外专利详情
     *
     * @param params
     * @return
     */
    WOverseasPatentOutDto getOneByParams(Map<String, Object> params);
}
