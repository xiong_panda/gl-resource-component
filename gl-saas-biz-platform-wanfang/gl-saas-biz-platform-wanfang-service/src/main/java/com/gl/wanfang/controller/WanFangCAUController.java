package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.service.IWHigherLearningUniversitiesService;
import com.gl.wanfang.service.IWanFangDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 万方高等院校搜索
 */
@RestController
@Api(value = "万方", tags = {"万方高等院校搜索"})
@RequestMapping("/cauData")
@Slf4j
public class WanFangCAUController {

    @Autowired
    IWanFangDataService wanFangDataService;
    @Autowired
    IWHigherLearningUniversitiesService higherLearningUniversitiesService;

    /**
     * 万方高等院校搜索
     *
     * @return
     */
    @ApiOperation(value = "万方高等院校本地搜索")
    @PostMapping("/wanFangCAUAByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
//    @RequiresPermissions("WFCAUA:select")
    public CityOutResult wanFangCAUAByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("高等院校检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(),CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        try {
            cityListByParams = higherLearningUniversitiesService.getListByParams(cityInPage);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("高等院校检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.HIGHER_LEARNING_UNIVERSITY_SEARCH_ERRO.getMsg(), CommonSearchErroCode.HIGHER_LEARNING_UNIVERSITY_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * 万方高等院校详情
     *
     * @return
     */
    @ApiOperation(value = "万方高等院校详情")
    @PostMapping("/wanFangCAUADetailByDB")
    @ResponseBody
    @UserClickLog
    @UserSearchLog
//    @RequiresPermissions("WFCAUA:select")
    public CityOutResult wanFangCAUADetailByDB(@RequestBody CommBean commBean) {
        CityOutResult cityListByParams = new CityOutResult();
        CityPage cityInPage = null;
        try {
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            log.info("高等院校检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(),CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        if(ObjectUtils.isEmpty(cityInPage.getParams().get("id"))){
            log.info("高等院校检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
            throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
        }

        try {
            cityListByParams.setResult(higherLearningUniversitiesService.getDetailByParams(cityInPage.getParams()));
        } catch (Exception e) {
            log.info("高等院校检索异常:{}", JSON.toJSONString(e.getStackTrace()));
            throw new BusinessException(CommonSearchErroCode.HIGHER_LEARNING_UNIVERSITY_SEARCH_ERRO.getMsg(), CommonSearchErroCode.HIGHER_LEARNING_UNIVERSITY_SEARCH_ERRO.getCode());
        }
        return cityListByParams;
    }


    /**
     * 万方高等院校办学层次聚合
     *
     * @return
     */
    @ApiOperation(value = "万方高等院校办学层次聚合")
    @PostMapping("/wanFangCAUAGroupLeve")
    @ResponseBody
    public Page<DateGroupByOutDto> wanFangCAUAGroupLeve() {
        List<DateGroupByOutDto> groupData = higherLearningUniversitiesService.getWanFangCAUAGroupLeve();
        Page<DateGroupByOutDto> page = new Page();
        page.setList(groupData);
        return page;
    }
}
