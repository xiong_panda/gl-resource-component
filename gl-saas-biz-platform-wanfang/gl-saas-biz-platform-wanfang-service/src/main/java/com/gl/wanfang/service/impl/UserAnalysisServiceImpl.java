package com.gl.wanfang.service.impl;

import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.wanfang.model.IndustryFieldDTO;
import com.gl.wanfang.outdto.IndustryFieldOutDTO;
import com.gl.wanfang.service.IUserAnalysisDalService;
import com.gl.wanfang.service.IUserAnalysisService;
import com.gl.wanfang.util.UserAnalysisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserAnalysisServiceImpl implements IUserAnalysisService {

    @Autowired
    IUserAnalysisDalService userAnalysisServiceDal;

    /**
     * 从登陆信息中获取用户对应的行业领域
     */
    @Override
    public IndustryFieldOutDTO getIndustryFiledByLogin() {
        String userId = UserAnalysisUtils.getUserInfo();
        IndustryFieldDTO industryFiledByLogin = userAnalysisServiceDal.getIndustryFiledByLogin(userId);

        IndustryFieldOutDTO industryFieldOutDTO = ConverBeanUtils.dtoToDo(industryFiledByLogin, IndustryFieldOutDTO.class);

        return industryFieldOutDTO;
    }

    /**
     * 所有的领域行业树数据
     * @return
     */
    @Override
    public List<Map<String, Object>> getIndustryFieldAll() {
        return userAnalysisServiceDal.getIndustryFieldAll();
    }
}
