package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommonSearchErroCode;
import com.gl.wanfang.indto.WTechnologyAchievementsInDto;
import com.gl.wanfang.mapper.WTechnologyAchievementsMapper;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WScientificOrg;
import com.gl.wanfang.model.WTechnologyAchievements;
import com.gl.wanfang.outdto.*;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWTechnologyAchievementsService;

import com.gl.wanfang.validator.CommonValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class WTechnologyAchievementsServiceImpl implements IWTechnologyAchievementsService {

    private static final Logger logger = LoggerFactory.getLogger(WTechnologyAchievementsServiceImpl.class);
    @Autowired
    WTechnologyAchievementsMapper wTechnologyAchievementsMapper;

    @Autowired
    IDateGroupDalService dateGroupDalService;


    @Autowired
    CommonValidator commonValidator;

    /**
     * 分页匹配模糊查询
     *
     * @return
     */
    public Page<WTechnologyAchievementsOutDto> getListByPage(Page<WTechnologyAchievementsInDto> page) {
        List<WTechnologyAchievements> listByPage = wTechnologyAchievementsMapper.getListByPage(page.getParams());
        //转换
        Page<WTechnologyAchievementsOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WTechnologyAchievements> pager = (com.github.pagehelper.Page) listByPage;
            List<WTechnologyAchievementsOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WTechnologyAchievementsOutDto outDto = ConverBeanUtils.dtoToDo(e, WTechnologyAchievementsOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }


    /**
     * 时间聚合
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getGroupDate() {
        List<DateGroupByEntity> dateGroup = dateGroupDalService.getDataGroup("Released_Date", "w_technology_achievements");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        //转换
        dateGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    @Override
    public Page<?> getListByPage2(Page<WTechnologyAchievementsInDto> page) {
        List<EasyWTechnologyAchievementsOutDto> list = wTechnologyAchievementsMapper.getListByPage2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public WTechnologyAchievementsOutDto getOneByParams(Map<String, Object> params) {
        return wTechnologyAchievementsMapper.getOneByParams(params);
    }


    @Override
    public CityOutResult getCityListByPage2(CityPage cityInPage) {
        CityOutResult cityOutResult = new CityOutResult();
        commonValidator.validator(cityInPage);
        List<EasyWTechnologyAchievementsOutDto> list = null;
        try {
            list = wTechnologyAchievementsMapper.getListByPage2(cityInPage.getParams());
        } catch (Exception e) {
            logger.info("科技成果检索异常:{}", JSON.toJSONString(e));
            throw new BusinessException(CommonSearchErroCode.ACHIEVEMENT_SEARCH_ERRO.getMsg(), CommonSearchErroCode.ACHIEVEMENT_DEATAIL_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

    @Override
    public CityOutResult getCityOneByParams(CityPage cityInPage) {
        CityOutResult cityOutResult = new CityOutResult();
        commonValidator.validator(cityInPage);
        WTechnologyAchievementsOutDto list = null;
        try {
            list = wTechnologyAchievementsMapper.getOneByParams(cityInPage.getParams());
        } catch (Exception e) {
            logger.info("科技成果详情异常:{}", JSON.toJSONString(e));
            throw new BusinessException(CommonSearchErroCode.ACHIEVEMENT_DEATAIL_ERRO.getMsg(), CommonSearchErroCode.ACHIEVEMENT_DEATAIL_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

}
