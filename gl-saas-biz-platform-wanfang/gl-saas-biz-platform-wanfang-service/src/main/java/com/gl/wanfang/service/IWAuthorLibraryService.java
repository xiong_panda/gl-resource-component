package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.outdto.WAuthorLibraryOutDto;

import java.util.Map;

public interface IWAuthorLibraryService {


    /**
     * 列表分页模糊查询 拆分返回详细信息
     *
     * @return
     */
    Page<?> getListByPage2(Page<WAuthorLibraryInDto> page);


    /**
     * 作者详细信息接口
     *
     * @param params
     * @return
     */
    WAuthorLibraryOutDto getListById(Map<String, Object> params);
    Page<?> getListByPage(Page<WAuthorLibraryInDto> page);

    /**
     * 城市群的作者检索
     *
     * @param cityInPage
     * @return
     */
    //CityOutResult getCityListByParams(Page<WAuthorLibraryInDto> page);
    CityOutResult getCityListByParams(CityPage page);

    /**
     * 城市群的作者详情
     *
     * @param cityInPage
     * @return
     */
    CityOutResult getCityDetailByParams(CityPage cityInPage);


}
