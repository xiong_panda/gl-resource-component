package com.gl.wanfang.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gl.basis.common.exception.BusinessException;
import com.gl.biz.city.CommonSearchErroCode;
import com.gl.wanfang.validator.CommonValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WanFangEnterpriseProductDatabaseInDto;
import com.gl.wanfang.indto.WanFangSupplierDatabaseInDto;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WanFangEnterpriseProductDatabase;
import com.gl.wanfang.model.WanFangSupplierDatabase;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.EasyWanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.outdto.WanFangSupplierDatabaseOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseDalService;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseService;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WanFangEnterpriseProductDatabaseServiceImpl implements IWanFangEnterpriseProductDatabaseService {


    @Autowired
    private IWanFangEnterpriseProductDatabaseDalService iWanFangEnterpriseProductDatabaseServiceDal;

    @Autowired
    private IDateGroupDalService dateGroupDalService;


    @Autowired
    private CommonValidator commonValidator;

//    @Autowired
//    private GuoLongClient guoLongClient;

    /**
     * 企业模糊匹配
     *
     * @param page
     * @return
     */
    @Override
    public Page<WanFangEnterpriseProductDatabaseOutDto> getListEnterpriseProductDatabase(
            Page<WanFangEnterpriseProductDatabaseInDto> page) {
//        Object obj = new Object();
//        Page<WanFangEnterpriseProductDatabaseOutDto> pageWanfang=guoLongClient.matchCompanyInfo(obj);
//        if(pageWanfang.getSuccess()){
//            pageWanfang.setIsPage(page.getIsPage());
//            pageWanfang.setPageNo(page.getPageNo());
//            pageWanfang.setParams(page.getParams());
//            return pageWanfang;
//        }

        List<WanFangEnterpriseProductDatabase> list = iWanFangEnterpriseProductDatabaseServiceDal
                .getListEnterpriseProductDatabase(page.getParams());
        // 转换
        Page<WanFangEnterpriseProductDatabaseOutDto> dtoPage = new Page<>();
        // 转换
        if (list instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WanFangEnterpriseProductDatabase> pager = (com.github.pagehelper.Page<WanFangEnterpriseProductDatabase>) list;
            List<WanFangEnterpriseProductDatabaseOutDto> outlist = new ArrayList<>();
            list.forEach(e -> {
                WanFangEnterpriseProductDatabaseOutDto outDto = ConverBeanUtils.dtoToDo(e,
                        WanFangEnterpriseProductDatabaseOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    /**
     * 供应商筛选
     * <p>
     * SELECT a.*,b.* from w_enterprise_product_database a LEFT JOIN
     * w_chinese_patent b ON b.ORG_Norm_Name2=a.Corp_Name
     *
     * @param page
     * @return
     */
    @Override
    public Page<WanFangSupplierDatabaseOutDto> getEnterpriseProductDatabase(Page<WanFangSupplierDatabaseInDto> page) {
        List<WanFangSupplierDatabase> list = iWanFangEnterpriseProductDatabaseServiceDal
                .getEnterpriseProductDatabase(page.getParams());
        // 转换
        Page<WanFangSupplierDatabaseOutDto> dtoPage = new Page<>();

        if (list instanceof com.github.pagehelper.Page) {

            com.github.pagehelper.Page<WanFangSupplierDatabaseOutDto> pager = (com.github.pagehelper.Page) list;
            List<WanFangSupplierDatabaseOutDto> outlist = new ArrayList<>();
            list.forEach(e -> {
                WanFangSupplierDatabaseOutDto outDto = ConverBeanUtils.dtoToDo(e, WanFangSupplierDatabaseOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    @Override
    public List<DateGroupByOutDto> getCompanyTypeGroup() {

        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("Corp_LEVEL ",
                "w_enterprise_product_database");
        List<DateGroupByOutDto> outlist = new ArrayList<>();
        // 转换
        Gson gson = new Gson();
        dataGroup.forEach(v -> {
            String groupData = v.getGroupData();
            List list = gson.fromJson(groupData, List.class);
            String o = String.valueOf(list.get(0));
            v.setGroupData(o);
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });

        return outlist;
    }

    /**
     * 企业全字段匹配
     *
     * @param page
     * @return
     */
    @Override
    public Page<WanFangEnterpriseProductDatabaseOutDto> getInspectionBySupplier(
            Page<WanFangEnterpriseProductDatabaseInDto> page) {
        List<WanFangEnterpriseProductDatabase> list = new ArrayList<>();
        Map<String, Object> params = page.getParams();
        if (params.containsKey("corpName") && !params.get("corpName").equals("")) {
            list = iWanFangEnterpriseProductDatabaseServiceDal.getList(params);
        }
        // 转换
        Page<WanFangEnterpriseProductDatabaseOutDto> dtoPage = new Page<>();
        if (list instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WanFangEnterpriseProductDatabase> pager = (com.github.pagehelper.Page<WanFangEnterpriseProductDatabase>) list;
            List<WanFangEnterpriseProductDatabaseOutDto> outlist = new ArrayList<>();
            list.forEach(e -> {
                WanFangEnterpriseProductDatabaseOutDto outDto = ConverBeanUtils.dtoToDo(e,
                        WanFangEnterpriseProductDatabaseOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    @Override
    public Page<?> getListEnterpriseProductDatabase2(Page<WanFangEnterpriseProductDatabaseInDto> page) {
        page.setResult(iWanFangEnterpriseProductDatabaseServiceDal.getListEnterpriseProductDatabase2(page.getParams()));
        return page;
    }

    @Override
    public WanFangEnterpriseProductDatabaseOutDto getOneByParams(Map<String, Object> params) {
        return iWanFangEnterpriseProductDatabaseServiceDal.getOneByParams(params);
    }

    @Override
    public CityOutResult getListEnterpriseProductDatabase2(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        List<EasyWanFangEnterpriseProductDatabaseOutDto> listByPage2 = null;
        try {
            listByPage2 = iWanFangEnterpriseProductDatabaseServiceDal
                    .getListEnterpriseProductDatabase2(cityInPage.getParams());
        } catch (Exception e) {
            log.info("企业检索异常:", e);
            throw new BusinessException(CommonSearchErroCode.COMPANY_SEARCH_ERRO.getMsg(), CommonSearchErroCode.COMPANY_SEARCH_ERRO.getCode());
        }
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }

    @Override
    public CityOutResult getOneByParams(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();

        WanFangEnterpriseProductDatabaseOutDto listByPage2 = null;
        try {
            listByPage2 = iWanFangEnterpriseProductDatabaseServiceDal
                    .getOneByParams(cityInPage.getParams());
        } catch (Exception e) {
            log.info("企业详情异常:", e);
            throw new BusinessException(CommonSearchErroCode.COMPANY__DETAIL_ERRO.getMsg(), CommonSearchErroCode.COMPANY__DETAIL_ERRO.getCode());
        }
        cityOutResult.setResult(listByPage2);
        return cityOutResult;
    }
}
