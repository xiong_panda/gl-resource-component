package com.gl.wanfang.service;

import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.wanfang.indto.WForeignLanguageOaPaperInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WForeignLanguageOaPaperOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWForeignLanguageOaPaperService {

    /**
     * 外文oa模糊匹配
     *
     * @return
     */
    Page<WForeignLanguageOaPaperOutDto> getWForeignLanguageOaPaperByDB(Page<WForeignLanguageOaPaperInDto> page);

    /**
     * OA外文论文时间聚合
     *
     * @return
     */
    List<DateGroupByOutDto> getGroupDate();

    /**
     * 获取简略列表
     *
     * @param page
     * @return
     */
    Page<?> getWForeignLanguageOaPaperByDB2(Page<WForeignLanguageOaPaperInDto> page);


    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WForeignLanguageOaPaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * @author QinYe 城市群的外文oa简略检索
     * @date 2020/6/18 15:38
     * @description
     */
    CityOutResult getListByParams(CityPage page);
}
