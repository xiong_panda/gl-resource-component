package com.gl.wanfang;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2Doc
@EnableFeignClients(basePackages = "com.gl.biz.api.fegin")//扫接口的包
//@EnableApolloConfig
public class AppWanFangation {
    public static void main(String[] args) {
        SpringApplication.run(AppWanFangation.class, args);
    }
}
