package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WLawsRegulationsInDTO;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WLawsRegulationsOutDTO;
import com.gl.wanfang.service.IWLawsRegulationsService;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.service.impl.WAuthorLibraryServiceImpl;
import com.gl.wanfang.config.UserSearchLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

/**
 * 万方法律法规搜索
 * 10012：法律法规
 */
@RestController
@Api(value = "万方", tags = {"万方法律法规搜索"})
@RequestMapping("/lawData")
public class WanFangLawController {
    private static final Logger logger = LoggerFactory.getLogger(WanFangLawController.class);
    @Autowired
    IWanFangDataService wanFangDataService;

    @Autowired
    IWLawsRegulationsService lawsRegulationsService;

  /*  @ApiOperation(value = "万方法律法规详情检索")
    @GetMapping("/lawRegulationsSearch")
    @ResponseBody
    @UserSearchLog
    @Deprecated
    public String MatchMechanismInfo(HttpServletResponse response, HttpServletRequest request) {
        String indexName = request.getParameter("indexName")==null?"10012": request.getParameter("indexName");
        String data=wanFangDataService.matchSeach(response,request,indexName);//法律法规简略信息匹配

        return  Optional.ofNullable(data).map(value -> {
            *//*处理简略信息，根据简略id，获取到对应的详细信息数据字段*//*
            return wanFangDataService.IdsInfo(response, request, value, indexName);
        }).orElse("");

    }*/

    /***
     *  法律法规 简略检索
     *  EasyWLawsRegulationsOutDTO
     * @param page
     * @return
     */
    @ApiOperation(value = "万方法律法规详情本地检索")
    @PostMapping("/matchMechanismInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFMechanism:select")
    public CityOutResult matchMechanismInfoByDB(@RequestBody CommBean commBean) {
        logger.info("查询法律法规:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("法律法规检索JSON解析异常:{}", JSON.toJSONString(commBean.getBz_data()));
           throw  new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(),CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }

        return lawsRegulationsService.getCityListByPag2(cityInPage);
    }


    /**
     * 法律法规详情信息
     *
     * @param
     * @return
     */
    @ApiOperation(value = "万方法律法规详情本地检索")
    @PostMapping("/matchMechanismDetailInfoByDB")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFMechanism:select")
    public CityOutResult matchMechanismDetailInfoByDB(@RequestBody CommBean commBean) {
        logger.info("查询法律法规详情:{}", JSON.toJSONString(commBean));
        CityPage cityInPage = null;
        try {
            // 解析JSON字符串 得到参数对象
            cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
        } catch (Exception e) {
            logger.info("法律法规详情检索JSON解析异常:{}",JSON.toJSONString(e));
            	throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
        }
        return lawsRegulationsService.getCityOneByParams(cityInPage);
    }




    /**
     * Value_Level
     *
     * @return
     */
    @ApiOperation(value = "万方法律法规效力级别聚合检索")
    @PostMapping("/getGroupValueLevel")
    @ResponseBody
    public Page<DateGroupByOutDto> getGroupValueLevel() {
        List<DateGroupByOutDto> groupData = lawsRegulationsService.getGroupValueLevel();
        Page<DateGroupByOutDto> page = new Page<>();
        page.setList(groupData);
        return page;
    }

}
