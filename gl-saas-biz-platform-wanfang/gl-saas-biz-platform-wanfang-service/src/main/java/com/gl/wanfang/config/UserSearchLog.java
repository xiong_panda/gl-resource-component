package com.gl.wanfang.config;

import com.gl.basis.common.pojo.Page;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.LinkedList;
import java.util.List;

/**
 * 用于记录用户搜索的注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserSearchLog {
    String value() default "";
}