package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.wanfang.service.IWanFangDataService;
import com.gl.wanfang.util.HttpSenService;
import com.gl.wanfang.util.WanFangConstant;
import com.gl.wanfang.util.WanFangTokenUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 万方数据接口调用
 */
@Service
public class WanFangDataService implements IWanFangDataService {

    /**
     * 全部检索简略信息
     * @param request
     * @param response
     * @return
     */
    @Override
    public String getWFAllData(HttpServletRequest request, HttpServletResponse response) {
        StringBuilder param=new StringBuilder();
        String title = request.getParameter("filter");//
        String excludes  = request.getParameter("excludes");// 返回结果排除字段，逗号分隔
        String from  = request.getParameter("from");
        String mustNot  = request.getParameter("mustNot");

        String noExistFields  = request.getParameter("noExistFields");// 不存在的字段，逗号分隔
        String Range = request.getParameter("Range");//  范围查询，标准 json 格式字 符串：如： {YEAR:{gte:2018,lte:2019}}。 gt 表示大于，gte 大于等于， lt 小于，lte 小于等于。
        String scroll  = request.getParameter("scroll ");//
        String scrollId   = request.getParameter("scrollId  ");//

        /*
         *  排序字段，逗号分隔，DESC 则加.D，如：YEAR.D。默认 按照_source（相关性）排序， 如果不需要排序，只是想得 到匹配结果，可对_doc 排 序，可以非常大的提高检索 性能。如：_doc。
         */
        String sortField  = request.getParameter("sortField");
        String indexName="10001,10002,10003,10004,10005,10006,10011,10012,10013,10014,10016,10020";

        param.append(title==null?"":"&filter="+title.replace("&","%26")).
                append("&indexName="+indexName).
                append(excludes==null?"":"&excludes="+excludes).
                append(from==null?"":"&from="+from).
                append(mustNot==null?"":"&mustNot="+mustNot.replace("\"","%22")).
                append(noExistFields==null?"":"&noExistFields="+noExistFields)
                .append(Range==null?"":"&Range="+Range)
                .append(sortField==null?"":"&sortField="+sortField)
                .append(scroll ==null?"":"&scroll="+scroll )
                .append(scrollId ==null?"":"&scrollId ="+scrollId )
                .replace(0,1,"?");
        String data = requestData(response,request,param.toString(), WanFangConstant.PUBLIC_URL,"99001");
        return data;
    }

    @Override
    public String getWFAllDetailData(HttpServletRequest request, HttpServletResponse response) {

    return null;
    }






    //获取数据，发送请求
    public String requestData(HttpServletResponse response, HttpServletRequest request,String param,String url,String appid){
        String data=null;
        try {
            Gson gson = new Gson();
            String tokens = WanFangTokenUtil.gettoken(WanFangConstant.WanFangSendURL + appid);//获取token
           if(tokens!=null && tokens.equals("")){
               Map map = gson.fromJson(tokens, new TypeToken<Map<String, String>>() {}.getType());
               String token = map.get("token").toString();
               data = HttpSenService.sendGet(url + param, token);
           }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
