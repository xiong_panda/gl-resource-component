package com.gl.wanfang.controller;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.biz.city.*;
import com.gl.wanfang.config.UserClickLog;
import com.gl.wanfang.config.UserSearchLog;
import com.gl.wanfang.indto.WAuthorLibraryInDto;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.outdto.DateGroupByOutDto;
import com.gl.wanfang.outdto.WAuthorLibraryOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.service.IWAuthorLibraryService;
import com.gl.wanfang.service.IWChinesePaperOaService;
import com.gl.wanfang.service.IWScientificOrgService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 中文OA论文
 * WChinesePaperOa服务暴露接口的实现层
 *
 * @author code_generator
 */
@RestController
@RequestMapping("/wChinesePaperOa")
@Slf4j
public class WChinesePaperOaController {

    @Autowired
    private IWChinesePaperOaService wChinesePaperOaService;
    @Autowired
    private IWScientificOrgService wScientificOrgService;
    @Autowired
    private IWAuthorLibraryService authorLibraryService;

    /**
     * @author QinYe 中文oa数据查询--城市群
     * @date 2020/6/18 17:54
     * @description
    */
    @ApiModelProperty(value = "中文oa数据查询", notes = "/getBywChinesePaperOa")
    @PostMapping("/getBywChinesePaperOa")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFChinesePaperOa:select")
    public CityOutResult getScientificOrg(@RequestBody CommBean commBean) {
		CityOutResult cityListByParams = new CityOutResult();
		CityPage cityInPage = null;
		try {
			cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
		} catch (Exception e) {
			log.info("中文oa检索异常:{}", JSON.toJSONString(e.getStackTrace()));
			throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
		}

		try {
			cityListByParams = wChinesePaperOaService.getListByParams(cityInPage);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("中文oa检索异常:{}", JSON.toJSONString(e.getStackTrace()));
			throw new BusinessException(CommonSearchErroCode.CHINESE_PAPER_OA_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHINESE_PAPER_OA_SEARCH_ERRO.getCode());
		}
		return cityListByParams;
    }


	/**
	 * @author QinYe 中文oa详情查询--城市群
	 * @date 2020/6/18 17:54
	 * @description
	 */
    @ApiModelProperty(value = "中文oa数据查询", notes = "/getBywChinesePaperOa")
    @PostMapping("/getScientificOrgDetailInfo")
    @ResponseBody
    @UserSearchLog
    @UserClickLog
    //    @RequiresPermissions("WFChinesePaperOa:select")
    public CityOutResult getScientificOrgDetailInfo(@RequestBody CommBean commBean) {
		CityOutResult cityListByParams = new CityOutResult();
		CityPage cityInPage = null;
		try {
			cityInPage = JSON.parseObject(commBean.getBz_data(), CityPage.class);
		} catch (Exception e) {
			log.info("中文oa检索异常:{}", JSON.toJSONString(e.getStackTrace()));
			throw new BusinessException(CityCommonErroCode.JSON_PARSE_ERRO.getMsg(), CityCommonErroCode.JSON_PARSE_ERRO.getCode());
		}

		if(ObjectUtils.isEmpty(cityInPage.getParams().get("id"))){
			log.info("中文oa检索异常:{}", JSON.toJSONString(cityInPage.getParams()));
			throw new BusinessException(CityCommonErroCode.PARAMS_NULL_ERRO.getMsg(), CityCommonErroCode.PARAMS_NULL_ERRO.getCode());
		}

		try {
			cityListByParams.setResult(wChinesePaperOaService.getOneByParams(cityInPage.getParams()));
		} catch (Exception e) {
			log.info("中文oa检索异常:{}", JSON.toJSONString(e.getStackTrace()));
			throw new BusinessException(CommonSearchErroCode.CHINESE_PAPER_OA_SEARCH_ERRO.getMsg(), CommonSearchErroCode.CHINESE_PAPER_OA_SEARCH_ERRO.getCode());
		}
		return cityListByParams;
    }


	/**
	 * 中文oa时间聚合
	 * @return
	 */
	@ApiOperation(value = "中文oa时间聚合")
	@PostMapping("/getBywChinesePaperOaGroup")
	@ResponseBody
	public Page<DateGroupByOutDto> getBywChinesePaperOaGroup() {
		List<DateGroupByOutDto> groupDate = wChinesePaperOaService.getGroupDate();
		Page<DateGroupByOutDto> page=new Page<>();
		page.setList(groupDate);
		return page;
	}

	/**
	 * 对应专家
	 * @return
	 */
	@ApiOperation(value = "万方专家信息详情本地检索")
	@PostMapping("/getExpertByInspection")
	@ResponseBody
    @UserClickLog
	//    @RequiresPermissions("Inspection:select")
	public Page<WExpertsLibraryOutDto> getExpertByInspection(@RequestBody Page<WExpertsLibraryInDto> page) {
		return wScientificOrgService.getExpertByInspection(page);
	}

	/**
	 * 万方作者信息详情检索
	 *
	 * @return
	 */
	@ApiOperation(value = "万方作者信息本地检索")
	@PostMapping("/matchAuthorInfoByDB")
	@ResponseBody
    @UserClickLog
	@UserSearchLog
	//    @RequiresPermissions("WFaAuthor:select")
	public Page<?> MatchAuthorInfoByDB(@RequestBody Page<WAuthorLibraryInDto> page) {
		return authorLibraryService.getListByPage(page);
	}
	

}
