package com.gl.wanfang.config;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class QuartZConfig {
    @Bean
    public JobDetail saveUserCacheJob() {
        return JobBuilder.newJob(UserPersistenceQuartZConfig.class).storeDurably().build();
    }
    @Bean
    public Trigger saveCacheTrigger() {
        //cron方式执行
        return TriggerBuilder.newTrigger().forJob(saveUserCacheJob())
                .withIdentity("saveUserCacheTask")
//                .withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ?"))
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 1 * * ?"))//每晚1点触发 //0 0 12 * * ?   每天中午12点触发
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?"))//每几分钟执行一次
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 */2 * * ?"))//每2小时执行一次
                .build();
    }
}
