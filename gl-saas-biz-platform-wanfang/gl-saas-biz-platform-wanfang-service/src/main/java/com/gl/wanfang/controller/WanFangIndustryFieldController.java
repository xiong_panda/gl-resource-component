package com.gl.wanfang.controller;

import com.gl.basis.common.pojo.Page;
import com.gl.wanfang.model.IndustryFieldDTO;
import com.gl.wanfang.outdto.IndustryFieldOutDTO;
import com.gl.wanfang.service.IUserAnalysisService;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.cbor.MappingJackson2CborHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@Api(value = "行业领域",tags = {"行业领域检索"})
@RequestMapping("/industryField")
public class WanFangIndustryFieldController {
    @Autowired
    private IUserAnalysisService userAnalysisService;

    /**
     * 根据用户获取所属行业领域
     * @return
     */
    @GetMapping("/getIndustryFieldByUser")
    public Page<IndustryFieldDTO> getIndustryFieldByUser(){
        Page page = new Page();
        IndustryFieldOutDTO industryFiledByLogin = userAnalysisService.getIndustryFiledByLogin();
        page.setResult(industryFiledByLogin);
        return page;
    }


    /**
     * 获取所有的行业领域
     * @return
     */
    @GetMapping("/getIndustryFieldAll")
    public Page<List<Map<String, Object>>> getIndustryFieldAll(){
        Page page = new Page();
        List<Map<String, Object>> maps= userAnalysisService.getIndustryFieldAll();
        page.setResult(maps);
        return page;
    }
}
