package com.gl.wanfang.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IWanFangDataService {

    /**
     * 全部简略信息获取
     * @param request
     * @param response
     * @return
     */
    public String getWFAllData(HttpServletRequest request, HttpServletResponse response);



    /**
     * 全部详细信息获取
     * @param request
     * @param response
     * @return
     */
    public String getWFAllDetailData(HttpServletRequest request, HttpServletResponse response);

}
