package com.gl.wanfang.service.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.ConverBeanUtils;
import com.gl.biz.city.CityCommonErroCode;
import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CityPage;
import com.gl.biz.city.CommonSearchErroCode;
import com.gl.wanfang.indto.WChinesePatentINDTO;
import com.gl.wanfang.indto.WExpertsLibraryInDto;
import com.gl.wanfang.indto.WScientificOrgInDto;
import com.gl.wanfang.indto.WanFangEnterpriseProductDatabaseInDto;
import com.gl.wanfang.model.*;
import com.gl.wanfang.outdto.*;
import com.gl.wanfang.service.*;
import com.gl.wanfang.validator.CommonValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * service业务处理层
 *
 * @author code_generator
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class WScientificOrgServiceImpl implements IWScientificOrgService {
    @Autowired
    private IWScientificOrgDalService wScientificOrgDalService;

    @Autowired
    IDateGroupDalService dateGroupDalService;

    @Autowired
    private IWanFangDBDataDalService wanFangDBDataDalService;

    @Autowired
    private IWExpertsLibraryDalService wExpertsLibraryDalService;


    @Autowired
    CommonValidator commonValidator;


    /*
     * @Autowired private IWanFangEnterpriseProductDatabaseDalService
     * iWanFangEnterpriseProductDatabaseServiceDal;
     */

    @Override
    public Page<WScientificOrgOutDto> getForm(Page<WScientificOrgInDto> page) {
        List<WScientificOrg> listByPage = wScientificOrgDalService.getListByPage(page.getParams());

        // 转换
        Page<WScientificOrgOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WScientificOrg> pager = (com.github.pagehelper.Page) listByPage;
            List<WScientificOrgOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WScientificOrgOutDto outDto = ConverBeanUtils.dtoToDo(e, WScientificOrgOutDto.class);
                outlist.add(outDto);
            });

            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;

    }

    /**
     * 聚合科研机构地区
     *
     * @return
     */
    @Override
    public List<DateGroupByOutDto> getGroupOrgType() {
        List<DateGroupByEntity> dataGroup = dateGroupDalService.getDataGroup("Province", "w_scientific_org");
        List<DateGroupByOutDto> outlist = new ArrayList<>();

        // 转换
        dataGroup.forEach(v -> {
            DateGroupByOutDto outDto = ConverBeanUtils.dtoToDo(v, DateGroupByOutDto.class);
            outlist.add(outDto);
        });
        return outlist;
    }

    /**
     * 检验检测--企业查询
     *
     * @param page
     * @return
     */
    /*
     * @Override public Page<WanFangEnterpriseProductDatabaseOutDto>
     * getInspectionBySupplier(Page<WanFangEnterpriseProductDatabaseInDto> page) {
     * List<WanFangEnterpriseProductDatabase> list=new ArrayList<>(); Map<String,
     * Object> params = page.getParams(); if(params.containsKey("corpName") &&
     * !params.get("corpName").equals("")){ list =
     * iWanFangEnterpriseProductDatabaseServiceDal.getList(params); } //转换
     * Page<WanFangEnterpriseProductDatabaseOutDto> dtoPage = new Page<>(); if
     * (list.size() > 0 && list instanceof com.github.pagehelper.Page) {
     * com.github.pagehelper.Page<WanFangEnterpriseProductDatabase> pager =
     * (com.github.pagehelper.Page<WanFangEnterpriseProductDatabase>) list;
     * List<WanFangEnterpriseProductDatabaseOutDto> outlist = new ArrayList<>();
     * list.forEach(e -> { WanFangEnterpriseProductDatabaseOutDto outDto =
     * ConverBeanUtils.dtoToDo(e, WanFangEnterpriseProductDatabaseOutDto.class);
     * outlist.add(outDto); }); dtoPage.setOrder(page.getOrder());
     * dtoPage.setIsPage(page.getIsPage()); dtoPage.setParams(page.getParams());
     * dtoPage.setPageNo(page.getPageNo()); dtoPage.setPageSize(page.getPageSize());
     * dtoPage.setList(outlist); dtoPage.setTotalSize(pager.getTotal()); } return
     * dtoPage; }
     */

    /**
     * 专利检索-检验检测 未传 参数时不会获取数据
     *
     * @param page
     * @return
     */
    @Override
    public Page<WChinesePatentOUTDTO> getChinesePatentByInspection(Page<WChinesePatentINDTO> page) {

        Map<String, Object> params = page.getParams();
        List<WChinesePatent> patentInfoByDB = new ArrayList<>();
        if (params.containsKey("orgNormName2") && !params.get("orgNormName2").equals("")) {
            patentInfoByDB = wanFangDBDataDalService.getPatentInfoByDB(params);
        }

        Page<WChinesePatentOUTDTO> dtoPage = new Page<>();
        // 转换
        if (patentInfoByDB instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WChinesePatent> pager = (com.github.pagehelper.Page<WChinesePatent>) patentInfoByDB;
            List<WChinesePatentOUTDTO> outlist = new ArrayList<>();
            patentInfoByDB.forEach(e -> {
                WChinesePatentOUTDTO outDto = ConverBeanUtils.dtoToDo(e, WChinesePatentOUTDTO.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    /**
     * 专家检索-检验检测 未传 参数时不会获取数据
     *
     * @param page
     * @return
     */
    @Override
    public Page<WExpertsLibraryOutDto> getExpertByInspection(Page<WExpertsLibraryInDto> page) {
        List<WExpertsLibrary> listByPage = new ArrayList<>();
        Map<String, Object> params = page.getParams();

        if (params.containsKey("unit") && !params.get("unit").equals("")) {
            listByPage = wExpertsLibraryDalService.getListByPage(page.getParams());
        }
        // 转换
        Page<WExpertsLibraryOutDto> dtoPage = new Page<>();
        if (listByPage instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<WExpertsLibrary> pager = (com.github.pagehelper.Page) listByPage;
            List<WExpertsLibraryOutDto> outlist = new ArrayList<>();
            listByPage.forEach(e -> {
                WExpertsLibraryOutDto outDto = ConverBeanUtils.dtoToDo(e, WExpertsLibraryOutDto.class);
                outlist.add(outDto);
            });
            dtoPage.setOrder(page.getOrder());
            dtoPage.setIsPage(page.getIsPage());
            dtoPage.setParams(page.getParams());
            dtoPage.setPageNo(page.getPageNo());
            dtoPage.setPageSize(page.getPageSize());
            dtoPage.setList(outlist);
            dtoPage.setTotalSize(pager.getTotal());
        }
        return dtoPage;
    }

    @Override
    public Page<?> getForm2(Page<WScientificOrgInDto> page) {
        List<EasyWScientificOrgOutDto> list = wScientificOrgDalService.getListByPage2(page.getParams());
        page.setResult(list);
        return page;
    }

    @Override
    public WScientificOrgOutDto getOneParams(Map<String, Object> params) {
        return wScientificOrgDalService.getOneParams(params);
    }

    @Override
    public CityOutResult getForm2(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        List<EasyWScientificOrgOutDto> list = null;
        try {
            list = wScientificOrgDalService.getListByPage2(cityInPage.getParams());
        } catch (Exception e) {
            log.info("机构检索异常:", e);
            throw new BusinessException(CommonSearchErroCode.ORGAN_SEARCH_ERRO.getMsg(), CommonSearchErroCode.ORGAN_SEARCH_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

    @Override
    public CityOutResult getOneParams(CityPage cityInPage) {
        commonValidator.validator(cityInPage);
        CityOutResult cityOutResult = new CityOutResult();
        WScientificOrgOutDto list = null;
        try {
            list = wScientificOrgDalService.getOneParams(cityInPage.getParams());
        } catch (Exception e) {
            log.info("机构异常:", e);
            throw new BusinessException(CommonSearchErroCode.ORGAN_DETAIL_ERRO.getMsg(), CommonSearchErroCode.ORGAN_DETAIL_ERRO.getCode());
        }
        cityOutResult.setResult(list);
        return cityOutResult;
    }

}
