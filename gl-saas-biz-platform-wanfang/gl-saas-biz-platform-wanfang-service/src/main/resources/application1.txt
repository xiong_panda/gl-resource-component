server.port=7505
#server.port=7517
spring.application.name = saas-platform-wanfang-fw

#spring.datasource.driver-class-name = com.mysql.jdbc.Driver
#spring.datasource.url= jdbc:mysql:///sys
#spring.datasource.username=root
#spring.datasource.password=root

#eureka.client.service-url.defaultZone = http://localhost:7508/eureka
eureka.client.service-url.defaultZone = http://192.168.2.117:8100/eureka/

swagger.title = wanfang
swagger.version = 1.1
swagger.contact.name = yangpan
swagger.contact.email = 2561091254@qq.com
swagger.base-package=com.gl.wanfang.controller

# Mybatis ORM
#mybatis.mapper-locations = classpath:**/mapper/*Mapper.xml


spring.servlet.multipart.enabled=true
spring.servlet.multipart.max-file-size=2000MB
spring.servlet.multipart.max-request-size=300MB


#任务调度
# 实例化ThreadPool时，使用的线程类为SimpleThreadPool
org.quartz.threadPool.class = org.quartz.simpl.SimpleThreadPool
# threadCount和threadPriority将以setter的形式注入ThreadPool实例
# 并发个数
org.quartz.threadPool.threadCount = 5
# 优先级
org.quartz.threadPool.threadPriority = 5
org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread = true
org.quartz.jobStore.misfireThreshold = 5000

logging.level.com.gl.wanfang.mapper=debug



