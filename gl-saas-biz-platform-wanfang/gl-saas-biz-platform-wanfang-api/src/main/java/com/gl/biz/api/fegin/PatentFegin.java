package com.gl.biz.api.fegin;

import com.gl.biz.city.CityOutResult;
import com.gl.biz.city.CommBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "just-a-name", url = "${wanfang.fegin.url}")
public interface PatentFegin {


    @PostMapping("/open")
    CityOutResult MatchAuthorInfoByDB(@RequestBody CommBean commBean);

}
