package com.gl.wanfang.mapper;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.ZtUserAreaIndustryTypeTime;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface ZtUserAreaIndustryTypeTimeMapper extends MyMapper<ZtUserAreaIndustryTypeTime> {

	List<ZtUserAreaIndustryTypeTime> getList(Map<String, Object> params);

}
