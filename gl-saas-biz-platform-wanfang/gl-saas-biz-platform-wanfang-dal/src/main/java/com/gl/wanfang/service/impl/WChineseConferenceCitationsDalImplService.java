package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WChineseConferenceCitationsMapper;
import com.gl.wanfang.model.WChineseConferenceCitations;
import com.gl.wanfang.model.WChineseConferencePaper;
import com.gl.wanfang.outdto.EasyWChineseConferencePaperOutDto;
import com.gl.wanfang.outdto.WChineseConferencePaperOutDto;
import com.gl.wanfang.service.IWChineseConferenceCitationsDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WChineseConferenceCitationsDalImplService implements IWChineseConferenceCitationsDalService {

    @Autowired
    WChineseConferenceCitationsMapper wChineseConferenceCitationsMapper;

    /**
     * 分页匹配模糊查询
     *
     * @param params
     * @return
     */
    public List<WChineseConferencePaper> getListByPage(Map<String, Object> params) {
        List<WChineseConferencePaper> listByPage = wChineseConferenceCitationsMapper.getListByPage(params);
        return listByPage;
    }

    @Override
    public List<EasyWChineseConferencePaperOutDto> getListByPage2(Map<String, Object> params) {
        return wChineseConferenceCitationsMapper.getListByPage2(params);
    }


    @Override
    public WChineseConferencePaperOutDto getOneByParams(Map<String, Object> params) {
        return wChineseConferenceCitationsMapper.getOneByParams(params);
    }

}
