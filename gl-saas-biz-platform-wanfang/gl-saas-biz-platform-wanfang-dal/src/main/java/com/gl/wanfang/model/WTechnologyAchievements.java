package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 科技成果
 * @author code_generator
 */
@Table(name = "W_TECHNOLOGY_ACHIEVEMENTS")
@Data
public class WTechnologyAchievements implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Full_Name")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 鉴定部门 **/
	@Column(name="Authorized_Department")
    @ApiModelProperty(value = "鉴定部门")
	private String authorizedDepartment ; 

	/** 专利申请号 **/
	@Column(name="PatentApplication_ID")
    @ApiModelProperty(value = "专利申请号")
	private String patentapplicationId ; 

	/** 完成人 **/
	@Column(name="Persons_Involved")
    @ApiModelProperty(value = "完成人")
	private String personsInvolved ; 

	/**  **/
	@Column(name="Fau")
    @ApiModelProperty(value = "")
	private String fau ; 

	/** 完成人 **/
	@Column(name="Persons_Involved2")
    @ApiModelProperty(value = "完成人")
	private String personsInvolved2 ; 

	/** 获奖情况 **/
	@Column(name="Awards")
    @ApiModelProperty(value = "获奖情况")
	private String awards ; 

	/**  **/
	@Column(name="Awards_Name")
    @ApiModelProperty(value = "")
	private String awardsName ; 

	/**  **/
	@Column(name="Awards_Anyname")
    @ApiModelProperty(value = "")
	private String awardsAnyname ; 

	/** 应用行业名称 **/
	@Column(name="Application_Industry_Name")
    @ApiModelProperty(value = "应用行业名称")
	private String applicationIndustryName ; 

	/** 中图分类号 **/
	@Column(name="Cls_Code")
    @ApiModelProperty(value = "中图分类号")
	private String clsCode ; 

	/**  **/
	@Column(name="Compby")
    @ApiModelProperty(value = "")
	private String compby ; 

	/**  **/
	@Column(name="CompID")
    @ApiModelProperty(value = "")
	private String compid ; 

	/** 联系地址 **/
	@Column(name="Contact_Address")
    @ApiModelProperty(value = "联系地址")
	private String contactAddress ; 

	/** 联系单位名称 **/
	@Column(name="Contact_Unit_Name")
    @ApiModelProperty(value = "联系单位名称")
	private String contactUnitName ; 

	/** 联系人 **/
	@Column(name="Contact")
    @ApiModelProperty(value = "联系人")
	private String contact ; 

	/** 联系电话 **/
	@Column(name="Contact_Phone_Number")
    @ApiModelProperty(value = "联系电话")
	private String contactPhoneNumber ; 

	/** 省市 **/
	@Column(name="Provinces_City")
    @ApiModelProperty(value = "省市")
	private String provincesCity ; 

	/** 资料公布日期 **/
	@Column(name="DataReleased_Date")
    @ApiModelProperty(value = "资料公布日期")
	private String datareleasedDate ; 

	/** 工作起止时间 **/
	@Column(name="Work_Start")
    @ApiModelProperty(value = "工作起止时间")
	private String workStart ; 

	/** 工作起止时间 **/
	@Column(name="Work_End")
    @ApiModelProperty(value = "工作起止时间")
	private String workEnd ; 

	/** 申报日期 **/
	@Column(name="Declare_Date")
    @ApiModelProperty(value = "申报日期")
	private String declareDate ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 申报单位名 **/
	@Column(name="Declare_Unit_Name")
    @ApiModelProperty(value = "申报单位名")
	private String declareUnitName ; 

	/** 成果公布日期 **/
	@Column(name="ResultsPublished_Date")
    @ApiModelProperty(value = "成果公布日期")
	private String resultspublishedDate ; 

	/**  **/
	@Column(name="Dura")
    @ApiModelProperty(value = "")
	private String dura ; 

	/** 创汇 **/
	@Column(name="Foreign_Exchange_Earning")
    @ApiModelProperty(value = "创汇")
	private String foreignExchangeEarning ; 

	/** 信箱 **/
	@Column(name="Mail")
    @ApiModelProperty(value = "信箱")
	private String mail ; 

	/** 传真 **/
	@Column(name="Fax")
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 成果类别 **/
	@Column(name="Results_Type")
    @ApiModelProperty(value = "成果类别")
	private String resultsType ; 

	/** 成果水平 **/
	@Column(name="Results_Level")
    @ApiModelProperty(value = "成果水平")
	private String resultsLevel ; 

	/** 完成单位 **/
	@Column(name="Complete_Unit")
    @ApiModelProperty(value = "完成单位")
	private String completeUnit ; 

	/** 投资金额 **/
	@Column(name="Investment_Amount")
    @ApiModelProperty(value = "投资金额")
	private String investmentAmount ; 

	/** 鉴定日期 **/
	@Column(name="IDentification_Date")
    @ApiModelProperty(value = "鉴定日期")
	private String identificationDate ; 

	/** 应用行业码 **/
	@Column(name="Application_Industry_Code")
    @ApiModelProperty(value = "应用行业码")
	private String applicationIndustryCode ; 

	/** 投资说明 **/
	@Column(name="Investment_Instructions")
    @ApiModelProperty(value = "投资说明")
	private String investmentInstructions ; 

	/** 投资注释 **/
	@Column(name="Investment_Annotation")
    @ApiModelProperty(value = "投资注释")
	private String investmentAnnotation ; 

	/** 成果简介 **/
	@Column(name="Results_Summary")
    @ApiModelProperty(value = "成果简介")
	private String resultsSummary ; 

	/** 主题词 **/
	@Column(name="Keyword")
    @ApiModelProperty(value = "主题词")
	private String keyword ; 

	/** 备注 **/
	@Column(name="Note")
    @ApiModelProperty(value = "备注")
	private String note ; 

	/** 列入时间 **/
	@Column(name="Included")
    @ApiModelProperty(value = "列入时间")
	private String included ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 完成单位:规范单位名称 **/
	@Column(name="Specification_Unit_Name")
    @ApiModelProperty(value = "完成单位:规范单位名称")
	private String specificationUnitName ; 

	/** 完成单位 **/
	@Column(name="CompletionUnit")
    @ApiModelProperty(value = "完成单位")
	private String completionunit ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 推广情况说明 **/
	@Column(name="Promotion_Info")
    @ApiModelProperty(value = "推广情况说明")
	private String promotionInfo ; 

	/** 推广方式 **/
	@Column(name="Promotion_Way")
    @ApiModelProperty(value = "推广方式")
	private String promotionWay ; 

	/** 推广范围 **/
	@Column(name="Promotion_Scope")
    @ApiModelProperty(value = "推广范围")
	private String promotionScope ; 

	/** 推广跟踪 **/
	@Column(name="Promotion_Tracking")
    @ApiModelProperty(value = "推广跟踪")
	private String promotionTracking ; 

	/** 产值 **/
	@Column(name="Output_Value")
    @ApiModelProperty(value = "产值")
	private String outputValue ; 

	/** 推广的必要性及推广预测 **/
	@Column(name="Promotion_Necessity_Promotion_Forecast")
    @ApiModelProperty(value = "推广的必要性及推广预测")
	private String promotionNecessityPromotionForecast ; 

	/** 计划名称 **/
	@Column(name="Project_Name")
    @ApiModelProperty(value = "计划名称")
	private String projectName ; 

	/** 成果密级 **/
	@Column(name="Achievement_ConfIDentiality_Level")
    @ApiModelProperty(value = "成果密级")
	private String achievementConfidentialityLevel ; 

	/** 成果类型 **/
	@Column(name="Achievement_Type")
    @ApiModelProperty(value = "成果类型")
	private String achievementType ; 

	/** 专利项数 **/
	@Column(name="Patent_Number")
    @ApiModelProperty(value = "专利项数")
	private String patentNumber ; 

	/** 公布刊物名、页数 **/
	@Column(name="Publication_Name_Page_Number")
    @ApiModelProperty(value = "公布刊物名、页数")
	private String publicationNamePageNumber ; 

	/** 专利授权号 **/
	@Column(name="Patent_Authorization_Number")
    @ApiModelProperty(value = "专利授权号")
	private String patentAuthorizationNumber ; 

	/** 登记日期 **/
	@Column(name="Registration_Date")
    @ApiModelProperty(value = "登记日期")
	private String registrationDate ; 

	/** 记录类型 **/
	@Column(name="Record_Date")
    @ApiModelProperty(value = "记录类型")
	private String recordDate ; 

	/** 推荐日期 **/
	@Column(name="Recommended_Date")
    @ApiModelProperty(value = "推荐日期")
	private String recommendedDate ; 

	/** 限制使用 **/
	@Column(name="Limit_To_Use")
    @ApiModelProperty(value = "限制使用")
	private String limitToUse ; 

	/** 登记号 **/
	@Column(name="Registration_No")
    @ApiModelProperty(value = "登记号")
	private String registrationNo ; 

	/** 记录状态 **/
	@Column(name="Record_Status")
    @ApiModelProperty(value = "记录状态")
	private String recordStatus ; 

	/** 登记部门 **/
	@Column(name="Registration_Department")
    @ApiModelProperty(value = "登记部门")
	private String registrationDepartment ; 

	/** 推荐部门 **/
	@Column(name="Recommend_Department")
    @ApiModelProperty(value = "推荐部门")
	private String recommendDepartment ; 

	/** 发布单位 **/
	@Column(name="Release_Nnit")
    @ApiModelProperty(value = "发布单位")
	private String releaseNnit ; 

	/** 规范单位名称 **/
	@Column(name="Specification_Unit_Name2")
    @ApiModelProperty(value = "规范单位名称")
	private String specificationUnitName2 ; 

	/** 信息来源 **/
	@Column(name="Source_Of_Info")
    @ApiModelProperty(value = "信息来源")
	private String sourceOfInfo ; 

	/** 节资 **/
	@Column(name="Save_Money")
    @ApiModelProperty(value = "节资")
	private String saveMoney ; 

	/** 中图分类二级 **/
	@Column(name="Cls_level2")
    @ApiModelProperty(value = "中图分类二级")
	private String clsLevel2 ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/** 转让条件 **/
	@Column(name="Transfer_Conditions")
    @ApiModelProperty(value = "转让条件")
	private String transferConditions ; 

	/** 转让费 **/
	@Column(name="Transfer_Fee")
    @ApiModelProperty(value = "转让费")
	private String transferFee ; 

	/** 转让内容 **/
	@Column(name="Transfer_Content")
    @ApiModelProperty(value = "转让内容")
	private String transferContent ; 

	/** 成果中文名称 **/
	@Column(name="Chinese_Name")
    @ApiModelProperty(value = "成果中文名称")
	private String chineseName ; 

	/** 转让注释 **/
	@Column(name="Transfer_Annotation")
    @ApiModelProperty(value = "转让注释")
	private String transferAnnotation ; 

	/**  **/
	@Column(name="Ischarge")
    @ApiModelProperty(value = "")
	private String ischarge ; 

	/** "登记部门码" **/
	@Column(name="Registration_Department_Code")
    @ApiModelProperty(value = "登记部门码")
	private String registrationDepartmentCode ; 

	/** "推荐部门码" **/
	@Column(name="Recommended_Department_Code")
    @ApiModelProperty(value = "推荐部门码")
	private String recommendedDepartmentCode ; 

	/** 转让方式 **/
	@Column(name="Transfer_Way")
    @ApiModelProperty(value = "转让方式")
	private String transferWay ; 

	/** 转让范围 **/
	@Column(name="Transfer_Scope")
    @ApiModelProperty(value = "转让范围")
	private String transferScope ; 

	/** 利税 **/
	@Column(name="Tax")
    @ApiModelProperty(value = "利税")
	private String tax ; 

	/** 中图分类顶级 **/
	@Column(name="Cls_Top")
    @ApiModelProperty(value = "中图分类顶级")
	private String clsTop ; 

	/** 资料公布日期 **/
	@Column(name="Released_Date")
    @ApiModelProperty(value = "资料公布日期")
	private String releasedDate ; 

	/** 中图分类号 **/
	@Column(name="Cls_Code2")
    @ApiModelProperty(value = "中图分类号")
	private String clsCode2 ; 

	/** 邮政编码 **/
	@Column(name="Post_Code")
    @ApiModelProperty(value = "邮政编码")
	private String postCode ; 

	/**  **/
	@Column(name="Other_ID")
    @ApiModelProperty(value = "")
	private String otherId ; 

	/** "主题词" **/
	@Column(name="KeyWord2")
    @ApiModelProperty(value = "主题词")
	private String keyword2 ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构层级ID **/
	@Column(name="ORG_Hierarchy_ID")
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
	@Column(name="ORG_Province")
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
	@Column(name="ORG_City")
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/**  **/
	@Column(name="ORG_Final")
    @ApiModelProperty(value = "")
	private String orgFinal ; 

	/**  **/
	@Column(name="ORG_Final2")
    @ApiModelProperty(value = "")
	private String orgFinal2 ; 

	/** 第一机构终级机构ID **/
	@Column(name="ORG_Frist_Final_ID")
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
	@Column(name="ORG_Frist_Hierarchy_ID")
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province2")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
	@Column(name="ORG_Frist_Final_City")
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
	@Column(name="ORG_Frist_City")
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
	@Column(name="ORG_Frist_Final_Type")
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
	@Column(name="ORG_Frist_Type")
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
	@Column(name="Aw_Short")
    @ApiModelProperty(value = "")
	private String awShort ; 

	/**  **/
	@Column(name="Aw_Level")
    @ApiModelProperty(value = "")
	private String awLevel ; 

	/**  **/
	@Column(name="Quarter")
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
	@Column(name="Half_Year")
    @ApiModelProperty(value = "")
	private String halfYear ; 

	/** 文献权重 **/
	@Column(name="Literature_Weight")
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/
	@Column(name="Author")
    @ApiModelProperty(value = "")
	private String author ; 

	/** 作者信息.姓名 **/
	@Column(name="Author_Info_Name")
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
	@Column(name="Author_Info_Order")
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
	@Column(name="Author_Info_Unit")
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level1")
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level2")
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
	@Column(name="Author_Info_Unit_Type")
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
	@Column(name="Author_Info_Unit_Province")
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
	@Column(name="Author_Info_Unit_City")
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
	@Column(name="Author_Info_Unit_County")
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
	@Column(name="Author_Info_ID")
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
	@Column(name="Author_Info_Unit_ID")
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
	@Column(name="ORG_Info")
    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/
	@Column(name="ORG_Info_Name")
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
	@Column(name="ORG_Info_Order")
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
	@Column(name="ORG_Info_Type")
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
	@Column(name="ORG_Info_Province")
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
	@Column(name="ORG_Info_City")
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
	@Column(name="ORG_Info_County")
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
	@Column(name="ORG_Info_Hierarchy")
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
	@Column(name="ORG_Info_Level1")
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
	@Column(name="ORG_Info_Level2")
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
	@Column(name="ORG_Info_Level3")
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
	@Column(name="ORG_Info_Level4")
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
	@Column(name="ORG_Info_Level5")
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 


}
