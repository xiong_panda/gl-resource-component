package com.gl.wanfang.service;


import com.gl.wanfang.model.SysCode;

import java.util.Map;

public interface ISysCodeDalService {
	SysCode translateCode(Map<String, Object> params);
}
