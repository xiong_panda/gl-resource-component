package com.gl.wanfang.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 海外专利 
 * @author code_generator
 */
@Table(name = "W_OVERSEAS_PATENT")
@Data
public class WOverseasPatent implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Full_Name")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 名称 **/
	@Column(name="Title")
    @ApiModelProperty(value = "名称")
	private String title ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract;

	/** 关键词 **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/**  **/
	@Column(name="ORG2")
    @ApiModelProperty(value = "")
	private String org2 ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 发明（设计）人 **/
	@Column(name="Author")
    @ApiModelProperty(value = "发明（设计）人")
	private String author ; 

	/** 专利号 **/
	@Column(name="Patent_No")
    @ApiModelProperty(value = "专利号")
	private String patentNo ; 

	/** 申请号 **/
	@Column(name="Requset_No")
    @ApiModelProperty(value = "申请号")
	private String requsetNo ; 

	/** 申请日 **/
	@Column(name="Date")
    @ApiModelProperty(value = "申请日")
	private String date ; 

	/** 申请（专利权）人 **/
	@Column(name="Request_People")
    @ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ; 

	/** 国省代码 **/
	@Column(name="Country_Code")
    @ApiModelProperty(value = "国省代码")
	private String countryCode ; 

	/** 公开（公告）号 **/
	@Column(name="Publication_No")
    @ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ; 

	/** 公开（公告）日 **/
	@Column(name="Publication_Date1")
    @ApiModelProperty(value = "公开（公告）日")
	private String publicationDate1 ; 

	/** 公开（公告）日 **/
	@Column(name="Publication_Date2")
    @ApiModelProperty(value = "公开（公告）日")
	private String publicationDate2 ; 

	/** 颁证日 **/
	@Column(name="Create_Date")
    @ApiModelProperty(value = "颁证日")
	private String createDate ; 

	/** 主分类号 **/
	@Column(name="Main_Class_Code1")
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode1 ; 

	/**  **/
	@Column(name="Omaincls")
    @ApiModelProperty(value = "")
	private String omaincls ; 

	/**  **/
	@Column(name="Ocls")
    @ApiModelProperty(value = "")
	private String ocls ; 

	/** 主分类号 **/
	@Column(name="Main_Class_Code2")
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ; 

	/** 分类号 **/
	@Column(name="Class_Code")
    @ApiModelProperty(value = "分类号")
	private String classCode ; 

	/** 外观分类 **/
	@Column(name="Appearance_Code")
    @ApiModelProperty(value = "外观分类")
	private String appearanceCode ; 

	/** 本国主分类 **/
	@Column(name="Country_Main_Class")
    @ApiModelProperty(value = "本国主分类")
	private String countryMainClass ; 

	/** 本国副分类 **/
	@Column(name="Country_Next_Class")
    @ApiModelProperty(value = "本国副分类")
	private String countryNextClass ; 

	/** 欧洲主分类 **/
	@Column(name="Europe_Main_Class")
    @ApiModelProperty(value = "欧洲主分类")
	private String europeMainClass ; 

	/** 附加分类 **/
	@Column(name="Europe_Next_Class")
    @ApiModelProperty(value = "附加分类")
	private String europeNextClass ; 

	/** 同族专利项 **/
	@Column(name="Congener")
    @ApiModelProperty(value = "同族专利项")
	private String congener ; 

	/** 分案原申请号 **/
	@Column(name="Oreqno")
    @ApiModelProperty(value = "分案原申请号")
	private String oreqno ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 申请日 **/
	@Column(name="Year")
    @ApiModelProperty(value = "申请日")
	private String year ; 

	/**  **/
	@Column(name="Reqnov")
    @ApiModelProperty(value = "")
	private String reqnov ; 

	/**  **/
	@Column(name="Patt")
    @ApiModelProperty(value = "")
	private String patt ; 

	/** 国际申请 **/
	@Column(name="International_Application")
    @ApiModelProperty(value = "国际申请")
	private String internationalApplication ; 

	/** 国际公布 **/
	@Column(name="International_publish")
    @ApiModelProperty(value = "国际公布")
	private String internationalPublish ; 

	/** WO申请号 **/
	@Column(name="WO_Application_No")
    @ApiModelProperty(value = "WO申请号")
	private String woApplicationNo ; 

	/** 进入国家日期 **/
	@Column(name="Dec")
    @ApiModelProperty(value = "进入国家日期")
	private String dec ; 

	/** 优先权 **/
	@Column(name="Priority")
    @ApiModelProperty(value = "优先权")
	private String priority ; 

	/** 优先权日 **/
	@Column(name="Priority_Data")
    @ApiModelProperty(value = "优先权日")
	private String priorityData ; 

	/** 再版原专利 **/
	@Column(name="Original_Patent_No")
    @ApiModelProperty(value = "再版原专利")
	private String originalPatentNo ; 

	/** 本国参考文献 **/
	@Column(name="Home_Reference")
    @ApiModelProperty(value = "本国参考文献")
	private String homeReference ; 

	/** 国外参考文献 **/
	@Column(name="Foreign_Reference")
    @ApiModelProperty(value = "国外参考文献")
	private String foreignReference ; 

	/** 非专利参考文献 **/
	@Column(name="Nonref")
    @ApiModelProperty(value = "非专利参考文献")
	private String nonref ; 

	/** 主权项 **/
	@Column(name="Independent_Claim")
    @ApiModelProperty(value = "主权项")
	private String independentClaim ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 名称 **/
	@Column(name="Name")
    @ApiModelProperty(value = "名称")
	private String name ; 

	/** 发明（设计）人 **/
	@Column(name="Author2")
    @ApiModelProperty(value = "发明（设计）人")
	private String author2 ; 

	/** 专利代理机构 **/
	@Column(name="ORG_Agency")
    @ApiModelProperty(value = "专利代理机构")
	private String orgAgency ; 

	/** 代理人 **/
	@Column(name="Agent")
    @ApiModelProperty(value = "代理人")
	private String agent ; 

	/**  **/
	@Column(name="DbID")
    @ApiModelProperty(value = "")
	private String dbid ; 

	/**  **/
	@Column(name="Source")
    @ApiModelProperty(value = "")
	private String source ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 失效专利 **/
	@Column(name="Invalid_Patent")
    @ApiModelProperty(value = "失效专利")
	private String invalidPatent ; 

	/** 主分类号显示数 **/
	@Column(name="Mainclsnum")
    @ApiModelProperty(value = "主分类号显示数")
	private String mainclsnum ; 

	/** 分类号显示数 **/
	@Column(name="Main_Category_No")
    @ApiModelProperty(value = "分类号显示数")
	private String mainCategoryNo ; 

	/**  **/
	@Column(name="Request_Address")
    @ApiModelProperty(value = "")
	private String requestAddress ; 

	/**  **/
	@Column(name="Reference")
    @ApiModelProperty(value = "")
	private String reference ; 

	/**  **/
	@Column(name="Reviewer")
    @ApiModelProperty(value = "")
	private String reviewer ; 

	/**  **/
	@Column(name="CdID")
    @ApiModelProperty(value = "")
	private String cdid ; 

	/**  **/
	@Column(name="Ser_Address")
    @ApiModelProperty(value = "")
	private String serAddress ; 

	/**  **/
	@Column(name="Publish_Path")
    @ApiModelProperty(value = "")
	private String publishPath ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 


}
