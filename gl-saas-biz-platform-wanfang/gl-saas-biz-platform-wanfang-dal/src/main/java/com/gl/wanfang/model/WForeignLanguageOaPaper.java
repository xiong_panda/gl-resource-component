package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 外文OA论文
 * @author code_generator
 */
@Table(name = "W_FOREIGN_LANGUAGE_OA_PAPER")
@Data
public class WForeignLanguageOaPaper implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** Title_Title **/
	@Column(name="Title")
    @ApiModelProperty(value = "Title_Title")
	private String title ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 作者 **/
	@Column(name="Author")
    @ApiModelProperty(value = "作者")
	private String author ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** Discipline_Keywords **/
	@Column(name="KeyWords")
    @ApiModelProperty(value = "Discipline_Keywords")
	private String keywords ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** Creator_ORG **/
	@Column(name="ORG_Creator")
    @ApiModelProperty(value = "Creator_ORG")
	private String orgCreator ; 

	/** Date_Issued **/
	@Column(name="Issued_Date")
    @ApiModelProperty(value = "Date_Issued")
	private String issuedDate ; 

	/** Date_Issued **/
	@Column(name="Issued_Year")
    @ApiModelProperty(value = "Date_Issued")
	private String issuedYear ; 

	/**  **/
	@Column(name="IID")
    @ApiModelProperty(value = "")
	private String iid ; 

	/** IDentifier_DOI **/
	@Column(name="DOI")
    @ApiModelProperty(value = "IDentifier_DOI")
	private String doi ; 

	/** IDentifier_ISSNp **/
	@Column(name="ISSNP")
    @ApiModelProperty(value = "IDentifier_ISSNp")
	private String issnp ; 

	/** IDentifier_ISSNe **/
	@Column(name="ISSNE")
    @ApiModelProperty(value = "IDentifier_ISSNe")
	private String issne ; 

	/** Creator_Creator **/
	@Column(name="Author_Name3")
    @ApiModelProperty(value = "Creator_Creator")
	private String authorName3 ; 

	/** Source_Source **/
	@Column(name="JOUCN")
    @ApiModelProperty(value = "Source_Source")
	private String joucn ; 

	/** Source_Source **/
	@Column(name="FJOUCN")
    @ApiModelProperty(value = "Source_Source")
	private String fjoucn ; 

	/** Source_Vol **/
	@Column(name="Source_Vol")
    @ApiModelProperty(value = "Source_Vol")
	private String sourceVol ; 

	/** Source_Issue **/
	@Column(name="Source_Issue")
    @ApiModelProperty(value = "Source_Issue")
	private String sourceIssue ; 

	/** Source_Page **/
	@Column(name="Source_Page")
    @ApiModelProperty(value = "Source_Page")
	private String sourcePage ; 

	/** Creator_ORG **/
	@Column(name="Creator_ORG")
    @ApiModelProperty(value = "Creator_ORG")
	private String creatorOrg ; 

	/** Date_Download **/
	@Column(name="Download_Date")
    @ApiModelProperty(value = "Date_Download")
	private String downloadDate ; 

	/** Publisher_Publisher **/
	@Column(name="Publisher_Publisher")
    @ApiModelProperty(value = "Publisher_Publisher")
	private String publisherPublisher ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_YNFREE")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaYnfree ; 

	/** datalink **/
	@Column(name="Data_Link")
    @ApiModelProperty(value = "datalink")
	private String dataLink ; 

	/** Discipline_SelfFL **/
	@Column(name="SELFFL")
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String selffl ; 

	/**  **/
	@Column(name="ZCID")
    @ApiModelProperty(value = "")
	private String zcid ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/**  **/
	@Column(name="TZCID")
    @ApiModelProperty(value = "")
	private String tzcid ; 

	/**  **/
	@Column(name="SZCID")
    @ApiModelProperty(value = "")
	private String szcid ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
	@Column(name="SCORE")
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
	@Column(name="RN")
    @ApiModelProperty(value = "")
	private String rn ; 

	/**  **/
	@Column(name="BN")
    @ApiModelProperty(value = "")
	private String bn ; 

	/**  **/
	@Column(name="DN")
    @ApiModelProperty(value = "")
	private String dn ; 

	/**  **/
	@Column(name="OTS")
    @ApiModelProperty(value = "")
	private String ots ; 


}
