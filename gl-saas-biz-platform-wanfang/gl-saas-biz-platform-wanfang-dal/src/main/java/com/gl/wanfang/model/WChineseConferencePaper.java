package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文会议论文
 * @author code_generator
 */
@Table(name = "w_chinese_conference_paper")
@Data
public class WChineseConferencePaper implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/
	@Column(name="FullText")
	@ApiModelProperty(value = "全文索引")
	private String fulltext ;

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Topic")
	@ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String topic ;

	/** 唯一编号 **/
	@Id
	@ApiModelProperty(value = "唯一编号")
	private String id ;

	/** 记录顺序号 **/
	@Column(name="OrderID")
	@ApiModelProperty(value = "记录顺序号")
	private String orderid ;

	/** DOI **/
	@Column(name="Doi")
	@ApiModelProperty(value = "DOI")
	private String doi ;

	/** w_ID **/
	@Column(name="WID")
	@ApiModelProperty(value = "w_ID")
	private String wid ;

	/**  **/
	@Column(name="NID")
	@ApiModelProperty(value = "")
	private String nid ;

	/** 论文题名 **/
	@Column(name="Chinese_Title")
	@ApiModelProperty(value = "论文题名")
	private String chineseTitle ;

	/** 英文论文题名 **/
	@Column(name="English_Title")
	@ApiModelProperty(value = "英文论文题名")
	private String englishTitle ;

	/** 论文题名:英文论文题名 **/
	@Column(name="C_E_Title")
	@ApiModelProperty(value = "论文题名:英文论文题名")
	private String cETitle ;

	/**  **/
	@Column(name="Author_ID")
	@ApiModelProperty(value = "")
	private String authorId ;

	/** 作者姓名 **/
	@Column(name="Author")
	@ApiModelProperty(value = "作者姓名")
	private String author ;

	/** 作者译名 **/
	@Column(name="Author_Translatio")
	@ApiModelProperty(value = "作者译名")
	private String authorTranslatio ;

	/** 第一作者 **/
	@Column(name="First_Author_ID")
	@ApiModelProperty(value = "第一作者")
	private String firstAuthorId ;

	/**  **/
	@Column(name="Fau")
	@ApiModelProperty(value = "")
	private String fau ;

	/**  **/
	@Column(name="Faue")
	@ApiModelProperty(value = "")
	private String faue ;

	/** 作者姓名:作者译名 **/
	@Column(name="T_N_Author")
	@ApiModelProperty(value = "作者姓名:作者译名")
	private String tNAuthor ;

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
	@ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ;

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
	@ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ;

	/** 作者单位名称 **/
	@Column(name="Author_Unit_Name")
	@ApiModelProperty(value = "作者单位名称")
	private String authorUnitName ;

	/** 作者单位规范名称 **/
	@Column(name="Author_Unit_Specification_Name")
	@ApiModelProperty(value = "作者单位规范名称")
	private String authorUnitSpecificationName ;

	/** 作者单位译名 **/
	@Column(name="Author_Unit_Translation")
	@ApiModelProperty(value = "作者单位译名")
	private String authorUnitTranslation ;

	/**  **/
	@Column(name="FORG")
	@ApiModelProperty(value = "")
	private String forg ;

	/** 作者单位名称 **/
	@Column(name="FORG_Unit_Name")
	@ApiModelProperty(value = "作者单位名称")
	private String forgUnitName ;

	/**  **/
	@Column(name="FORGe")
	@ApiModelProperty(value = "")
	private String forge ;

	/** 机构名称 **/
	@Column(name="ORG_Name")
	@ApiModelProperty(value = "机构名称")
	private String orgName ;

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
	@ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ;

	/** 作者单位名称:作者单位规范名称:作者单位译名 **/
	@Column(name="U_S_Author_Anyname")
	@ApiModelProperty(value = "作者单位名称:作者单位规范名称:作者单位译名")
	private String uSAuthorAnyname ;

	/** C_ID **/
	@Column(name="CID")
	@ApiModelProperty(value = "C_ID")
	private String cid ;

	/** 机标分类号 **/
	@Column(name="Machine_Label_Class_Code")
	@ApiModelProperty(value = "机标分类号")
	private String machineLabelClassCode ;

	/** 中图分类号 **/
	@Column(name="Cls_Code")
	@ApiModelProperty(value = "中图分类号")
	private String clsCode ;

	/** 中图分类二级 **/
	@Column(name="Cls_level2")
	@ApiModelProperty(value = "中图分类二级")
	private String clsLevel2 ;

	/** 中图分类顶级 **/
	@Column(name="Cls_Top")
	@ApiModelProperty(value = "中图分类顶级")
	private String clsTop ;

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
	@ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ;

	/**  **/
	@Column(name="IID")
	@ApiModelProperty(value = "")
	private String iid ;

	/** 学科分类:学科分类机标 **/
	@Column(name="Discipline_Class_Code")
	@ApiModelProperty(value = "学科分类:学科分类机标")
	private String disciplineClassCode ;

	/** 协会级别 **/
	@Column(name="Association_Level")
	@ApiModelProperty(value = "协会级别")
	private String associationLevel ;

	/** 中文关键词 **/
	@Column(name="Chinese_KeyWord")
	@ApiModelProperty(value = "中文关键词")
	private String chineseKeyword ;

	/** 英文关键词 **/
	@Column(name="English_KeyWord")
	@ApiModelProperty(value = "英文关键词")
	private String englishKeyword ;

	/** 机标关键词 **/
	@Column(name="Machine_KeyWord")
	@ApiModelProperty(value = "机标关键词")
	private String machineKeyword ;

	/** 中文关键词:英文关键词:机标关键词 **/
	@Column(name="C_E_Keyword")
	@ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String cEKeyword ;

	/** 中文摘要 **/
	@Column(name="Chinese_Abstract")
	@ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ;

	/** 英文摘要 **/
	@Column(name="English_Abstract")
	@ApiModelProperty(value = "英文摘要")
	private String englishAbstract ;

	/**  **/
	@Column(name="F_Abstract")
	@ApiModelProperty(value = "")
	private String fAbstract ;

	/** 中文摘要:英文摘要 **/
	@Column(name="C_E_Abstract")
	@ApiModelProperty(value = "中文摘要:英文摘要")
	private String cEAbstract ;

	/**  **/
	@Column(name="Lan")
	@ApiModelProperty(value = "")
	private String lan ;

	/** 母体文献 **/
	@Column(name="Parent_Literature")
	@ApiModelProperty(value = "母体文献")
	private String parentLiterature ;

	/**  **/
	@Column(name="Parent_Literature_Name")
	@ApiModelProperty(value = "")
	private String parentLiteratureName ;

	/**  **/
	@Column(name="Parent_Literature_Name2")
	@ApiModelProperty(value = "")
	private String parentLiteratureName2 ;

	/** 学会名称 **/
	@Column(name="Institute_Name")
	@ApiModelProperty(value = "学会名称")
	private String instituteName ;

	/**  **/
	@Column(name="Institute_Name_Anyname")
	@ApiModelProperty(value = "")
	private String instituteNameAnyname ;

	/**  **/
	@Column(name="Sn_Anyname")
	@ApiModelProperty(value = "")
	private String snAnyname ;

	/** 会议名称 **/
	@Column(name="Conference_Title")
	@ApiModelProperty(value = "会议名称")
	private String conferenceTitle ;

	/**  **/
	@Column(name="Conference_Title_Anyname")
	@ApiModelProperty(value = "")
	private String conferenceTitleAnyname ;

	/** 会议届次 **/
	@Column(name="Conference_Sessions")
	@ApiModelProperty(value = "会议届次")
	private String conferenceSessions ;

	/** 会议地点 **/
	@Column(name="Conference_Meeting_Place")
	@ApiModelProperty(value = "会议地点")
	private String conferenceMeetingPlace ;

	/**  **/
	@Column(name="HID")
	@ApiModelProperty(value = "")
	private String hid ;

	/** 主办单位 **/
	@Column(name="ORGanizer")
	@ApiModelProperty(value = "主办单位")
	private String organizer ;

	/** 基金 **/
	@Column(name="Fund")
	@ApiModelProperty(value = "基金")
	private String fund ;

	/** 基金全文检索 **/
	@Column(name="Fund_FullText_Search")
	@ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ;

	/** 基金 **/
	@Column(name="F_Fund")
	@ApiModelProperty(value = "基金")
	private String fFund ;

	/**  **/
	@Column(name="Fpn")
	@ApiModelProperty(value = "")
	private String fpn ;

	/**  **/
	@Column(name="Fundsupport")
	@ApiModelProperty(value = "")
	private String fundsupport ;

	/** 会议时间 **/
	@Column(name="Start_Meeting_Date")
	@ApiModelProperty(value = "会议时间")
	private String startMeetingDate ;

	/** 会议时间 **/
	@Column(name="End_Meeting_Date")
	@ApiModelProperty(value = "会议时间")
	private String endMeetingDate ;

	/** 出版时间 **/
	@Column(name="Start_Published_Date")
	@ApiModelProperty(value = "出版时间")
	private String startPublishedDate ;

	/** 出版时间 **/
	@Column(name="End_Published_Date")
	@ApiModelProperty(value = "出版时间")
	private String endPublishedDate ;

	/** 数据来源 **/
	@Column(name="Data_Source")
	@ApiModelProperty(value = "数据来源")
	private String dataSource ;

	/**  **/
	@Column(name="ORGnum")
	@ApiModelProperty(value = "")
	private String orgnum ;

	/**  **/
	@Column(name="Rn")
	@ApiModelProperty(value = "")
	private String rn ;

	/** 机构层级ID **/
	@Column(name="ORG_Hierarchy_ID")
	@ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ;

	/** 作者单位ID **/
	@Column(name="Author_Unit_ID")
	@ApiModelProperty(value = "作者单位ID")
	private String authorUnitId ;

	/** 机构所在省 **/
	@Column(name="ORG_Province")
	@ApiModelProperty(value = "机构所在省")
	private String orgProvince ;

	/** 机构所在市 **/
	@Column(name="ORG_City")
	@ApiModelProperty(value = "机构所在市")
	private String orgCity ;

	/** 机构类型 **/
	@Column(name="ORG_Type")
	@ApiModelProperty(value = "机构类型")
	private String orgType ;

	/**  **/
	@Column(name="FORGID")
	@ApiModelProperty(value = "")
	private String forgid ;

	/**  **/
	@Column(name="EndORGtype")
	@ApiModelProperty(value = "")
	private String endorgtype ;

	/** 第一机构终级机构ID **/
	@Column(name="ORG_Frist_Final_ID")
	@ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ;

	/** 第一机构层级机构ID **/
	@Column(name="ORG_Frist_Hierarchy_ID")
	@ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ;

	/** 第一机构所在市 **/
	@Column(name="ORG_Frist_City")
	@ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ;

	/** 第一机构终级机构所在市 **/
	@Column(name="ORG_Frist_Final_City")
	@ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ;

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province")
	@ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ;

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province2")
	@ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ;

	/** 第一机构终级机构类型 **/
	@Column(name="ORG_Frist_Final_Type")
	@ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ;

	/** 第一机构类型 **/
	@Column(name="ORG_Frist_Type")
	@ApiModelProperty(value = "第一机构类型")
	private String orgFristType ;

	/**  **/
	@Column(name="Quarter")
	@ApiModelProperty(value = "")
	private String quarter ;

	/**  **/
	@Column(name="Halfyear")
	@ApiModelProperty(value = "")
	private String halfyear ;

	/** 文献权重 **/
	@Column(name="Literature_Weight")
	@ApiModelProperty(value = "文献权重")
	private String literatureWeight ;

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
	@ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ;

	/**  **/
	@Column(name="Author_Info")
	@ApiModelProperty(value = "")
	private String authorInfo ;

	/** 作者信息.姓名 **/
	@Column(name="Author_Info_Name")
	@ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ;

	/** 作者信息.作者次序 **/
	@Column(name="Author_Info_Order")
	@ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ;

	/** 作者信息.工作单位 **/
	@Column(name="Author_Info_Unit")
	@ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ;

	/** 作者信息.工作单位一级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level1")
	@ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ;

	/** 作者信息.工作单位二级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level2")
	@ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ;

	/** 作者信息.工作单位类型 **/
	@Column(name="Author_Info_Unit_Type")
	@ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ;

	/** 作者信息.工作单位所在省 **/
	@Column(name="Author_Info_Unit_Province")
	@ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ;

	/** 作者信息.工作单位所在市 **/
	@Column(name="Author_Info_Unit_City")
	@ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ;

	/** 作者信息.工作单位所在县 **/
	@Column(name="Author_Info_Unit_County")
	@ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ;

	/** 作者信息.唯一ID **/
	@Column(name="Author_Info_ID")
	@ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ;

	/** 作者信息.工作单位唯一ID **/
	@Column(name="Author_Info_Unit_ID")
	@ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ;

	/**  **/
	@Column(name="ORG_Info")
	@ApiModelProperty(value = "")
	private String orgInfo ;

	/** 机构信息.机构名称 **/
	@Column(name="ORG_Info_Name")
	@ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ;

	/** 机构信息.机构次序 **/
	@Column(name="ORG_Info_Order")
	@ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ;

	/** 机构信息.机构类型 **/
	@Column(name="ORG_Info_Type")
	@ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ;

	/** 机构信息.省 **/
	@Column(name="ORG_Info_Province")
	@ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ;

	/** 机构信息.市 **/
	@Column(name="ORG_Info_City")
	@ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ;

	/** 机构信息.县 **/
	@Column(name="ORG_Info_County")
	@ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ;

	/** 机构信息.五级机构层级码 **/
	@Column(name="ORG_Info_Hierarchy")
	@ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ;

	/** 机构信息.1级机构名称 **/
	@Column(name="ORG_Info_Level1")
	@ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ;

	/** 机构信息.2级机构名称 **/
	@Column(name="ORG_Info_Level2")
	@ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ;

	/** 机构信息.3级机构名称 **/
	@Column(name="ORG_Info_Level3")
	@ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ;

	/** 机构信息.4级机构名称 **/
	@Column(name="ORG_Info_Level4")
	@ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ;

	/** 机构信息.5级机构名称 **/
	@Column(name="ORG_Info_Level5")
	@ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ;

	/** 创建时间 **/
	@Column(name="CreateTime")
	@ApiModelProperty(value = "创建时间")
	private Date createtime ;


}
