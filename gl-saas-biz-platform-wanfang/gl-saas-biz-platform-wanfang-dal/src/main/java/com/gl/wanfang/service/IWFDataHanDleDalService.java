package com.gl.wanfang.service;

import com.gl.wanfang.model.HxWfServerSearch;
import com.gl.wanfang.model.HxWfServerSearchOriginal;
import com.gl.wanfang.outdto.HxWfServerSearchOutDto;

import java.util.List;
import java.util.Map;

/**
 *原始库数据操作
 */
public interface IWFDataHanDleDalService {


    /**
     * 根据参数获取数据列
     * @param params
     * @return
     */
    List<HxWfServerSearchOriginal> getListByParam(Map<String, Object> params);

    /**
     * 保存用户使用数据记录
     * @param params
     * @return
     */
    void saveUserWFUseRecord(Map<String, Object> params);
}
