package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 作者库
 * @author code_generator
 */
@Table(name = "W_AUTHOR_LIBRARY")
@Data
public class WAuthorLibrary implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/**  **/
	@Column(name="w_ID")
    @ApiModelProperty(value = "")
	private String wId ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 姓名:英文名 **/
	@Column(name="A_English_Name")
    @ApiModelProperty(value = "姓名:英文名")
	private String aEnglishName ; 

	/** 工作单位 **/
	@Column(name="Unit")
    @ApiModelProperty(value = "工作单位")
	private String unit ; 

	/** 工作单位(全文检索) **/
	@Column(name="Unit_FullText_Search")
    @ApiModelProperty(value = "工作单位(全文检索)")
	private String unitFulltextSearch ; 

	/** 工作单位:统计单位名称:英文单位名称 **/
	@Column(name="Sun_English_Unit_Name")
    @ApiModelProperty(value = "工作单位:统计单位名称:英文单位名称")
	private String sunEnglishUnitName ; 

	/** 统计单位名称 **/
	@Column(name="Statistical_Unit_Name")
    @ApiModelProperty(value = "统计单位名称")
	private String statisticalUnitName ; 

	/**  **/
	@Column(name="ORGall_Anyname")
    @ApiModelProperty(value = "")
	private String orgallAnyname ; 

	/**  **/
	@Column(name="ORGall_Anyname2")
    @ApiModelProperty(value = "")
	private String orgallAnyname2 ; 

	/** 一级单位 **/
	@Column(name="level1_Unit")
    @ApiModelProperty(value = "一级单位")
	private String level1Unit ; 

	/** 二级单位 **/
	@Column(name="level2_Unit")
    @ApiModelProperty(value = "二级单位")
	private String level2Unit ; 

	/** 单位 **/
	@Column(name="w_Unit")
    @ApiModelProperty(value = "单位")
	private String wUnit ; 

	/** 英文单位名称 **/
	@Column(name="English_Unit_Name")
    @ApiModelProperty(value = "英文单位名称")
	private String englishUnitName ; 

	/** 职称 **/
	@Column(name="Title")
    @ApiModelProperty(value = "职称")
	private String title ; 

	/** 简介:备注:工作简历 **/
	@Column(name="Introduction_Job_Resume")
    @ApiModelProperty(value = "简介:备注:工作简历")
	private String introductionJobResume ; 

	/** 简介 **/
	@Column(name="Intro")
    @ApiModelProperty(value = "简介")
	private String intro ; 

	/** 姓名 **/
	@Column(name="Name")
    @ApiModelProperty(value = "姓名")
	private String name ; 

	/** 英文名 **/
	@Column(name="English_Name")
    @ApiModelProperty(value = "英文名")
	private String englishName ; 

	/** 导师 **/
	@Column(name="Mentor")
    @ApiModelProperty(value = "导师")
	private String mentor ; 

	/** 地区 **/
	@Column(name="Region")
    @ApiModelProperty(value = "地区")
	private String region ; 

	/** 邮编 **/
	@Column(name="Postcode")
    @ApiModelProperty(value = "邮编")
	private String postcode ; 

	/** 地址 **/
	@Column(name="Address")
    @ApiModelProperty(value = "地址")
	private String address ; 

	/** 邮件地址 **/
	@Column(name="Email")
    @ApiModelProperty(value = "邮件地址")
	private String email ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 日期 **/
	@Column(name="Date")
    @ApiModelProperty(value = "日期")
	private Date date ; 

	/** 备注 **/
	@Column(name="Note")
    @ApiModelProperty(value = "备注")
	private String note ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 频次 **/
	@Column(name="Frequency")
    @ApiModelProperty(value = "频次")
	private String frequency ; 

	/**  **/
	@Column(name="Yfreq")
    @ApiModelProperty(value = "")
	private String yfreq ; 

	/** 关键词 **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/**  **/
	@Column(name="Keyword_Anyname")
    @ApiModelProperty(value = "")
	private String keywordAnyname ; 

	/** 相关关键词 **/
	@Column(name="Related_Keyword")
    @ApiModelProperty(value = "相关关键词")
	private String relatedKeyword ; 

	/** 相关期刊 **/
	@Column(name="Related_Journals")
    @ApiModelProperty(value = "相关期刊")
	private String relatedJournals ; 

	/** 相关人物 **/
	@Column(name="Related_Figure")
    @ApiModelProperty(value = "相关人物")
	private String relatedFigure ; 

	/** 相关学科 **/
	@Column(name="Related_Discipline")
    @ApiModelProperty(value = "相关学科")
	private String relatedDiscipline ; 

	/** 相关学科 **/
	@Column(name="Related_Discipline2")
    @ApiModelProperty(value = "相关学科")
	private String relatedDiscipline2 ; 

	/** 相关基金 **/
	@Column(name="Related_Fund")
    @ApiModelProperty(value = "相关基金")
	private String relatedFund ; 

	/** qcode **/
	@Column(name="Qcode")
    @ApiModelProperty(value = "qcode")
	private String qcode ; 

	/** 性别 **/
	@Column(name="Sex")
    @ApiModelProperty(value = "性别")
	private String sex ; 

	/** 出生日期 **/
	@Column(name="Birth_Date")
    @ApiModelProperty(value = "出生日期")
	private Date birthDate ; 

	/** 在职年月 **/
	@Column(name="Working_Days")
    @ApiModelProperty(value = "在职年月")
	private Date workingDays ; 

	/** 出生地点 **/
	@Column(name="Birth_Address")
    @ApiModelProperty(value = "出生地点")
	private String birthAddress ; 

	/** 民族 **/
	@Column(name="National")
    @ApiModelProperty(value = "民族")
	private String national ; 

	/** 工作职务 **/
	@Column(name="Job_Position")
    @ApiModelProperty(value = "工作职务")
	private String jobPosition ; 

	/** 行政码 **/
	@Column(name="Administrative_Code")
    @ApiModelProperty(value = "行政码")
	private String administrativeCode ; 

	/** 电话 **/
	@Column(name="Telphone")
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 传真 **/
	@Column(name="Fax")
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 教育背景 **/
	@Column(name="Education_Background")
    @ApiModelProperty(value = "教育背景")
	private String educationBackground ; 

	/** 最高学位 **/
	@Column(name="Highest_Degree")
    @ApiModelProperty(value = "最高学位")
	private String highestDegree ; 

	/** 外语语种 **/
	@Column(name="Foreign_Languages")
    @ApiModelProperty(value = "外语语种")
	private String foreignLanguages ; 

	/** 区位 **/
	@Column(name="Zone_Bit")
    @ApiModelProperty(value = "区位")
	private String zoneBit ; 

	/** 专业领域与研究方向 **/
	@Column(name="Professional_Research")
    @ApiModelProperty(value = "专业领域与研究方向")
	private String professionalResearch ; 

	/** 国内外学术或专业团体任职情况 **/
	@Column(name="Co_Employment")
    @ApiModelProperty(value = "国内外学术或专业团体任职情况")
	private String coEmployment ; 

	/** 院士 **/
	@Column(name="Academician")
    @ApiModelProperty(value = "院士")
	private String academician ; 

	/** 专家荣誉 **/
	@Column(name="Experts_Honor")
    @ApiModelProperty(value = "专家荣誉")
	private String expertsHonor ; 

	/** 专家荣誉 **/
	@Column(name="Experts_Honor2")
    @ApiModelProperty(value = "专家荣誉")
	private String expertsHonor2 ; 

	/** 专家特色 **/
	@Column(name="Experts_Characteristics")
    @ApiModelProperty(value = "专家特色")
	private String expertsCharacteristics ; 

	/**  **/
	@Column(name="Character_Anyname")
    @ApiModelProperty(value = "")
	private String characterAnyname ; 

	/**  **/
	@Column(name="Character_Anyname2")
    @ApiModelProperty(value = "")
	private String characterAnyname2 ; 

	/** 学科分类名 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类名")
	private String disciplineClass ; 

	/** 学科分类码 **/
	@Column(name="Discipline_Class_Code")
    @ApiModelProperty(value = "学科分类码")
	private String disciplineClassCode ; 

	/** 学科分类码 **/
	@Column(name="Discipline_Class_Code2")
    @ApiModelProperty(value = "学科分类码")
	private String disciplineClassCode2 ; 

	/** 工作简历 **/
	@Column(name="Job_Resume")
    @ApiModelProperty(value = "工作简历")
	private String jobResume ; 

	/** 数据来源 **/
	@Column(name="Data_Source")
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/**  **/
	@Column(name="H")
    @ApiModelProperty(value = "")
	private String h ; 

	/** 中文期刊第一作者发文数 **/
	@Column(name="Journalcount")
    @ApiModelProperty(value = "中文期刊第一作者发文数")
	private String journalcount ; 

	/** 总被引次数 **/
	@Column(name="Quotecount")
    @ApiModelProperty(value = "总被引次数")
	private String quotecount ; 


}
