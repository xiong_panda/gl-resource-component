package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WChinesePatentMapper;
import com.gl.wanfang.model.WChinesePatent;
import com.gl.wanfang.outdto.EasyWChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import com.gl.wanfang.service.IWanFangDBDataDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WanFangDBDataImplDalService implements IWanFangDBDataDalService {
    @Autowired
    WChinesePatentMapper wChinesePatentMapper;

    /**
     * 专利查询
     *
     * @param page
     * @return
     */
    @Override
    public List<WChinesePatent> getPatentInfoByDB(Map page) {
        List<WChinesePatent> list = wChinesePatentMapper.getPatentInfoByDB(page);
        return list;
    }

    @Override
    public List<EasyWChinesePatentOUTDTO> getPatentInfoByDB2(Map<String, Object> params) {
        return wChinesePatentMapper.getPatentInfoByDB2(params);
    }

    @Override
    public WChinesePatentOUTDTO getOneByParams(Map<String, Object> params) {
        return wChinesePatentMapper.getOneByParams(params);
    }
}
