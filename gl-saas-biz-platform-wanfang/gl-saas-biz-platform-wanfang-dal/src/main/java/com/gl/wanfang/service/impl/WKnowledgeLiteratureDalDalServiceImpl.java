package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.MixZswxMapper;
import com.gl.wanfang.model.MixZswx;
import com.gl.wanfang.outdto.EasyMixZswxOutDto;
import com.gl.wanfang.outdto.MixZswxOutDto;
import com.gl.wanfang.service.IWKnowledgeLiteratureDalService;
import io.netty.handler.codec.mqtt.MqttMessageBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
@Service
public class WKnowledgeLiteratureDalDalServiceImpl implements IWKnowledgeLiteratureDalService {

    @Autowired
    MixZswxMapper knowledgeLiteratureMapper;

    /**
     * 知识文件检索
     * 分页模糊匹配
     * @param params
     * @return
     */
    public List<MixZswx> getListByPage(Map<String, Object> params){

        /**
         * 知识文件检索
         * @param params
         * @return
         */
        List<MixZswx> listByPage = knowledgeLiteratureMapper.getListByPage(params);
        return  listByPage;
    }

    @Override
    public List<EasyMixZswxOutDto> getListByPage2(Map<String, Object> params) {
        return knowledgeLiteratureMapper.getListByPage2(params);
    }

    @Override
    public MixZswxOutDto getOneByParams(Map<String, Object> params) {
        return knowledgeLiteratureMapper.getOneByParams(params);
    }

}
