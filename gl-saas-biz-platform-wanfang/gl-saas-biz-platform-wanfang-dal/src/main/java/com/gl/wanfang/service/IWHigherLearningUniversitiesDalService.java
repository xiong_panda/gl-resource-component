package com.gl.wanfang.service;

import com.gl.wanfang.model.WHigherLearningUniversities;
import com.gl.wanfang.outdto.EasyWHigherLearningUniversitiesOutDto;
import com.gl.wanfang.outdto.WHigherLearningUniversitiesOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWHigherLearningUniversitiesDalService {

    /**
     * 高等院校模糊匹配
     *
     * @return
     */
    List<WHigherLearningUniversities> getWHigherLearningUniversitiesByDB(Map<String, Object> params);

    /**
     * 通过名字查询
     *
     * @param params
     * @return
     */
    WHigherLearningUniversities selectOneByName(Map<String, Object> params);

    /**
     * 获取列表
     *
     * @param params
     * @return
     */
    List<EasyWHigherLearningUniversitiesOutDto> getWHigherLearningUniversitiesByDB2(Map<String, Object> params);


    /**
     * 获取详情信息
     */
    WHigherLearningUniversitiesOutDto getDetailByParams(Map<String, Object> params);
}
