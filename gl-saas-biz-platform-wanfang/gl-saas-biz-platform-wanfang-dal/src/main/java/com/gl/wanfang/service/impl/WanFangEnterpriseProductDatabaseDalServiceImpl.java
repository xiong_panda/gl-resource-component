package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WanFangEnterpriseProductDatabaseMapper;

import com.gl.wanfang.model.WanFangEnterpriseProductDatabase;
import com.gl.wanfang.model.WanFangSupplierDatabase;
import com.gl.wanfang.outdto.EasyWanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.service.IWanFangEnterpriseProductDatabaseDalService;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * service业务处理层
 * @author code_generator
 */
@Service
public class WanFangEnterpriseProductDatabaseDalServiceImpl implements IWanFangEnterpriseProductDatabaseDalService {

    @Autowired
    private WanFangEnterpriseProductDatabaseMapper wEnterpriseProductDatabaseMapper;



    @Override
    public List<WanFangEnterpriseProductDatabase> getList(Map<String, Object> params) {
        return wEnterpriseProductDatabaseMapper.getList(params);
    }


    @Override
    public List<WanFangEnterpriseProductDatabase> getListEnterpriseProductDatabase(Map<String, Object> params) {
        return wEnterpriseProductDatabaseMapper.getListEnterpriseProductDatabase(params);
    }

    @Override
    public List<WanFangSupplierDatabase> getEnterpriseProductDatabase(Map<String, Object> params) {
        return wEnterpriseProductDatabaseMapper.getSupplierByPatent(params);
    }

    @Override
    public List<EasyWanFangEnterpriseProductDatabaseOutDto> getListEnterpriseProductDatabase2(Map<String, Object> params) {
        return wEnterpriseProductDatabaseMapper.getListEnterpriseProductDatabase2(params);
    }

    @Override
    public WanFangEnterpriseProductDatabaseOutDto getOneByParams(Map<String, Object> params) {
        return wEnterpriseProductDatabaseMapper.getOneByParams(params);
    }
}
