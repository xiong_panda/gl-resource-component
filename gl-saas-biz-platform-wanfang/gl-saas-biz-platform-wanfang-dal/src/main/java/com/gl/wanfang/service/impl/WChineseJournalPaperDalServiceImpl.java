package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WChineseJournalPaperMapper;
import com.gl.wanfang.model.WChineseJournalPaper;
import com.gl.wanfang.outdto.EasyWChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;
import com.gl.wanfang.service.IWChineseJournalPaperDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WChineseJournalPaperDalServiceImpl implements IWChineseJournalPaperDalService {

    @Autowired
    WChineseJournalPaperMapper wChineseJournalPaperMapper;

    /**
     * 中文期刊
     * @param params
     * @return
     */
    @Override
    public List<WChineseJournalPaper> getListByPage(Map<String, Object> params) {
        return  wChineseJournalPaperMapper.getListByPage(params);
    }

    @Override
    public WChineseJournalPaperOutDto getOneByParams(Map<String, Object> params) {
        return wChineseJournalPaperMapper.getOneByParams(params);
    }

    @Override
    public List<EasyWChineseJournalPaperOutDto> getListByPage2(Map<String, Object> params) {
        return wChineseJournalPaperMapper.getListByPage2(params);
    }
}
