package com.gl.wanfang.service.impl;

import com.gl.wanfang.indto.WForeignLanguageOaPaperInDto;
import com.gl.wanfang.mapper.WForeignLanguageOaPaperMapper;
import com.gl.wanfang.model.WForeignLanguageOaPaper;
import com.gl.wanfang.outdto.EasyWForeignLanguageOaPaperOutDto;
import com.gl.wanfang.outdto.WForeignLanguageOaPaperOutDto;
import com.gl.wanfang.service.IWForeignLanguageOaPaperDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WForeignLanguageOaPaperDalServiceImpl implements IWForeignLanguageOaPaperDalService {

    @Autowired
    WForeignLanguageOaPaperMapper wForeignLanguageOaPaperMapper;

    /**
     * 匹配英文oa
     *
     * @param params
     * @return
     */
    @Override
    public List<WForeignLanguageOaPaper> getWForeignLanguageOaPaperByDB(Map<String, Object> params) {
        List<WForeignLanguageOaPaper> listByPage = wForeignLanguageOaPaperMapper.getListByPage(params);
        return listByPage;
    }

    @Override
    public List<EasyWForeignLanguageOaPaperOutDto> getWForeignLanguageOaPaperByDB2(Map<String, Object> params) {
        return wForeignLanguageOaPaperMapper.getWForeignLanguageOaPaperByDB2(params);
    }

    @Override
    public WForeignLanguageOaPaperOutDto getOneByParams(Map<String, Object> params) {
        return wForeignLanguageOaPaperMapper.getOneByParams(params);
    }
}
