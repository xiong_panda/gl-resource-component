package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WForeignPeriodicalPaper;
import com.gl.wanfang.outdto.EasyWForeignPeriodicalPaperOutDto;
import com.gl.wanfang.outdto.WForeignPeriodicalPaperOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WForeignPeriodicalPaperMapper extends MyMapper<WForeignPeriodicalPaper> {

    /**
     * 分页匹配
     *
     * @param params
     * @return
     */
    List<WForeignPeriodicalPaper> getList(Map<String, Object> params);

    /**
     * 外文期刊论文模糊分页匹配
     *
     * @param params
     * @return
     */
    List<WForeignPeriodicalPaper> getListByPage(Map<String, Object> params);

    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WForeignPeriodicalPaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * 获取简略信息 列表
     *
     * @param params
     * @return
     */
    List<EasyWForeignPeriodicalPaperOutDto> getListByPage2(Map<String, Object> params);
}
