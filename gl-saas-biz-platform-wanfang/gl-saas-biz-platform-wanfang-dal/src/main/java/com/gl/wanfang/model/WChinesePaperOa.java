package com.gl.wanfang.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文OA论文 
 * @author code_generator
 */
@Table(name = "W_CHINESE_PAPER_OA")
@Data
public class WChinesePaperOa implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** Title_Title **/
	@Column(name="Title")
    @ApiModelProperty(value = "Title_Title")
	private String title ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_CAB")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaCab ; 

	/** 作者 **/
	@Column(name="Author")
    @ApiModelProperty(value = "作者")
	private String author ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** Discipline_Keywords **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "Discipline_Keywords")
	private String keyword ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** Creator_ORG **/
	@Column(name="ORG_Creator")
    @ApiModelProperty(value = "Creator_ORG")
	private String orgCreator ; 

	/** Date_Issued **/
	@Column(name="Issued_Date")
    @ApiModelProperty(value = "Date_Issued")
	private String issuedDate ; 

	/** Date_Issued **/
	@Column(name="Issued_Year")
    @ApiModelProperty(value = "Date_Issued")
	private String issuedYear ; 

	/**  **/
	@Column(name="IID")
    @ApiModelProperty(value = "")
	private String iid ; 

	/** IDentifier_DOI **/
	@Column(name="DOI")
    @ApiModelProperty(value = "IDentifier_DOI")
	private String doi ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_ISSN")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaIssn ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_EISSN")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaEissn ; 

	/** Creator_Creator **/
	@Column(name="Author_Name3")
    @ApiModelProperty(value = "Creator_Creator")
	private String authorName3 ; 

	/** Source_Source **/
	@Column(name="WCHINESE_PAPER_OA_SOURCE_SOURCE")
    @ApiModelProperty(value = "Source_Source")
	private String wchinesePaperOaSourceSource ; 

	/** Source_Source **/
	@Column(name="WCHINESE_PAPER_OA_SOURCE_SOURCE2")
    @ApiModelProperty(value = "Source_Source")
	private String wchinesePaperOaSourceSource2 ; 

	/** Source_Vol **/
	@Column(name="Source_Vol")
    @ApiModelProperty(value = "Source_Vol")
	private String sourceVol ; 

	/** Source_Issue **/
	@Column(name="Source_Issue")
    @ApiModelProperty(value = "Source_Issue")
	private String sourceIssue ; 

	/** Source_Page **/
	@Column(name="Source_Page")
    @ApiModelProperty(value = "Source_Page")
	private String sourcePage ; 

	/** Creator_ORG **/
	@Column(name="Creator_ORG")
    @ApiModelProperty(value = "Creator_ORG")
	private String creatorOrg ; 

	/** Date_Download **/
	@Column(name="Download_Date")
    @ApiModelProperty(value = "Date_Download")
	private String downloadDate ; 

	/** Publisher_Publisher **/
	@Column(name="Publisher_Publisher")
    @ApiModelProperty(value = "Publisher_Publisher")
	private String publisherPublisher ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_YNFREE")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaYnfree ; 

	/** datalink **/
	@Column(name="Data_Link")
    @ApiModelProperty(value = "datalink")
	private String dataLink ; 

	/** Discipline_SelfFL **/
	@Column(name="WCHINESE_PAPER_OA_Discipline_SELFFL")
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String wchinesePaperOaDisciplineSelffl ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_IISSNE")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaIissne ; 

	/** IDentifier_ISSNp **/
	@Column(name="IDentifier_ISSNp")
    @ApiModelProperty(value = "IDentifier_ISSNp")
	private String identifierIssnp ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_ZCID")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaZcid ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_TZCID")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaTzcid ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_SZCID")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaSzcid ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_Class")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaClass ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_RN")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaRn ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_BN")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaBn ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_DN")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaDn ; 

	/**  **/
	@Column(name="WCHINESE_PAPER_OA_OTS")
    @ApiModelProperty(value = "")
	private String wchinesePaperOaOts ; 


}
