package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WScientificOrgMapper;
import com.gl.wanfang.model.WScientificOrg;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.EasyWScientificOrgOutDto;
import com.gl.wanfang.outdto.WScientificOrgOutDto;
import com.gl.wanfang.service.IWScientificOrgDalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层
 * @author code_generator
 */
@Service
@Transactional(readOnly=true)
@Slf4j
public class WScientificOrgDalServiceImpl implements IWScientificOrgDalService {
	@Autowired
	private WScientificOrgMapper wScientificOrgMapper;


	@Override
	public List<WScientificOrg> getListByPage(Map<String, Object> params) {
		return wScientificOrgMapper.getBypage(params);
	}

	@Override
	public List<EasyWScientificOrgOutDto> getListByPage2(Map<String, Object> params) {
		return wScientificOrgMapper.getListByPage2(params);
	}

	@Override
	public WScientificOrgOutDto getOneParams(Map<String, Object> params) {
		return wScientificOrgMapper.getOneParams(params);
	}


}
