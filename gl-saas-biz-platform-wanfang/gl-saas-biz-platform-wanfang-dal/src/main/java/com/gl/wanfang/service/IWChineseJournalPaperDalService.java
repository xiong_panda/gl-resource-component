package com.gl.wanfang.service;

import com.gl.wanfang.model.WChineseJournalPaper;
import com.gl.wanfang.outdto.EasyWChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWChineseJournalPaperDalService {

    /**
     * 中文期刊详情本地检索
     * 分页模糊匹配
     *
     * @param params
     * @return
     */
    List<WChineseJournalPaper> getListByPage(Map<String, Object> params);

    /**
     * 中文期刊详情信息检索
     *
     * @param params
     * @return
     */
    WChineseJournalPaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * 中文期刊简略信息检索
     *
     * @param params
     * @return
     */
    List<EasyWChineseJournalPaperOutDto> getListByPage2(Map<String, Object> params);
}
