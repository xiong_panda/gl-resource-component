package com.gl.wanfang.service;

import com.gl.wanfang.model.WForeignPeriodicalPaper;
import com.gl.wanfang.outdto.EasyWForeignPeriodicalPaperOutDto;
import com.gl.wanfang.outdto.WForeignPeriodicalPaperOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWForeignPeriodicalPaperDalService {

    //分页模糊匹配
    List<WForeignPeriodicalPaper> getListByPage(Map<String, Object> params);

    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WForeignPeriodicalPaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * 获取列表信息
     *
     * @param params
     * @return
     */
    List<EasyWForeignPeriodicalPaperOutDto> getListByPage2(Map<String, Object> params);
}
