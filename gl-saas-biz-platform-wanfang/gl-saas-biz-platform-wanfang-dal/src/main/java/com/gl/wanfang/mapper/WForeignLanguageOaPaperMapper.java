package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WForeignLanguageOaPaper;
import com.gl.wanfang.outdto.EasyWForeignLanguageOaPaperOutDto;
import com.gl.wanfang.outdto.WForeignLanguageOaPaperOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WForeignLanguageOaPaperMapper extends MyMapper<WForeignLanguageOaPaper> {

    /**
     * 外文oa匹配
     *
     * @param params
     * @return
     */
    List<WForeignLanguageOaPaper> getList(Map<String, Object> params);

    /**
     * 外文oa模糊匹配
     *
     * @param params
     * @return
     */
    List<WForeignLanguageOaPaper> getListByPage(Map<String, Object> params);

    /**
     * 获取 列表
     *
     * @param params
     * @return
     */
    List<EasyWForeignLanguageOaPaperOutDto> getWForeignLanguageOaPaperByDB2(Map<String, Object> params);

    /***
     *  获取详情
     * @param params
     * @return
     */
    WForeignLanguageOaPaperOutDto getOneByParams(Map<String, Object> params);
}
