package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WForeignPeriodicalPaperMapper;
import com.gl.wanfang.model.WForeignPeriodicalPaper;
import com.gl.wanfang.outdto.EasyWForeignPeriodicalPaperOutDto;
import com.gl.wanfang.outdto.WForeignPeriodicalPaperOutDto;
import com.gl.wanfang.service.IWForeignPeriodicalPaperDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WForeignPeriodicalPaperDalServiceImpl implements IWForeignPeriodicalPaperDalService {

    @Autowired
    WForeignPeriodicalPaperMapper wForeignPeriodicalPaperMapper;

    /**
     * 外文期刊论文模糊匹配
     *
     * @param params
     * @return
     */
    @Override
    public List<WForeignPeriodicalPaper> getListByPage(Map<String, Object> params) {
        List<WForeignPeriodicalPaper> listByPage = wForeignPeriodicalPaperMapper.getListByPage(params);
        return listByPage;
    }

    @Override
    public WForeignPeriodicalPaperOutDto getOneByParams(Map<String, Object> params) {
        return wForeignPeriodicalPaperMapper.getOneByParams(params);
    }

    @Override
    public List<EasyWForeignPeriodicalPaperOutDto> getListByPage2(Map<String, Object> params) {
        return wForeignPeriodicalPaperMapper.getListByPage2(params);
    }
}
