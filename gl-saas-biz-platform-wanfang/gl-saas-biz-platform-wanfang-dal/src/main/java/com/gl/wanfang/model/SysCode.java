package com.gl.wanfang.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 注意:注解只能加在属性字段上才会生效!
 * 系统码值表
 * @author code_generator
 */
@Table(name = "SYS_CODE")
public class SysCode implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 字典主键ID **/
	@Id
	private String fPkid ;

	/** 字典值 **/
	@Column(name="val_code")
	private String valCode ;

	/** 字典值说明 **/
	@Column(name="val_name")
	private String valName ;

	/** 字典值类型 **/
	@Column(name="type_code")
	private String typeCode ;

	/** 备注 **/
	@Column(name="remark")
	private String remark ;

	/** 删除标记（1：已删除，0：正常） **/
	@Column(name="f_isdelete")
	private String fIsdelete ;

	/** 启用状态：1：禁用，0：启用 **/
	@Column(name="f_enable")
	private String fEnable ;

	/** 创建人 **/
	@Column(name="f_input_id")
	private String fInputId ;

	/** 创建时间 **/
	@Column(name="f_input_time")
	private Date fInputTime ;

	/** 更新人 **/
	@Column(name="f_end_id")
	private String fEndId ;

	/** 更新时间 **/
	@Column(name="f_end_time")
	private Date fEndTime ;

	/** 时态标记 **/
	@Column(name="TimeState_Flag")
	private Integer timestateFlag ;

	/** 时态名称 **/
	@Column(name="TimeState_Name")
	private String timestateName ;

	/** 可信等级 **/
	@Column(name="Credit_Level")
	private String creditLevel ;

	/** 可信比例 **/
	@Column(name="Credit_Score")
	private float creditScore ;

	/** 传输状态 **/
	@Column(name="f_transmission")
	private Integer fTransmission ;

	/** 创建人姓名 **/
	@Column(name="f_inputName")
	private String fInputname ;

	/** 更新人姓名 **/
	@Column(name="f_endname")
	private String fEndname ;

	/** 预留字段1 **/
	@Column(name="f_spare1")
	private String fSpare1 ;

	/** 预留字段2 **/
	@Column(name="f_spare2")
	private String fSpare2 ;

	/** 预留字段3 **/
	@Column(name="f_spare3")
	private String fSpare3 ;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getValCode() {
		return valCode;
	}

	public void setValCode(String valCode) {
		this.valCode = valCode;
	}

	public String getValName() {
		return valName;
	}

	public void setValName(String valName) {
		this.valName = valName;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfInputId() {
		return fInputId;
	}

	public void setfInputId(String fInputId) {
		this.fInputId = fInputId;
	}

	public Date getfInputTime() {
		return fInputTime;
	}

	public void setfInputTime(Date fInputTime) {
		this.fInputTime = fInputTime;
	}

	public String getfEndId() {
		return fEndId;
	}

	public void setfEndId(String fEndId) {
		this.fEndId = fEndId;
	}

	public Date getfEndTime() {
		return fEndTime;
	}

	public void setfEndTime(Date fEndTime) {
		this.fEndTime = fEndTime;
	}

	public Integer getTimestateFlag() {
		return timestateFlag;
	}

	public void setTimestateFlag(Integer timestateFlag) {
		this.timestateFlag = timestateFlag;
	}

	public String getTimestateName() {
		return timestateName;
	}

	public void setTimestateName(String timestateName) {
		this.timestateName = timestateName;
	}

	public String getCreditLevel() {
		return creditLevel;
	}

	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel;
	}

	public float getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(float creditScore) {
		this.creditScore = creditScore;
	}

	public Integer getfTransmission() {
		return fTransmission;
	}

	public void setfTransmission(Integer fTransmission) {
		this.fTransmission = fTransmission;
	}

	public String getfInputname() {
		return fInputname;
	}

	public void setfInputname(String fInputname) {
		this.fInputname = fInputname;
	}

	public String getfEndname() {
		return fEndname;
	}

	public void setfEndname(String fEndname) {
		this.fEndname = fEndname;
	}

	public String getfSpare1() {
		return fSpare1;
	}

	public void setfSpare1(String fSpare1) {
		this.fSpare1 = fSpare1;
	}

	public String getfSpare2() {
		return fSpare2;
	}

	public void setfSpare2(String fSpare2) {
		this.fSpare2 = fSpare2;
	}

	public String getfSpare3() {
		return fSpare3;
	}

	public void setfSpare3(String fSpare3) {
		this.fSpare3 = fSpare3;
	}
}
