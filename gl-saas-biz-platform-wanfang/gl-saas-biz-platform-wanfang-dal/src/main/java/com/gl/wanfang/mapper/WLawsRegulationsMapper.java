package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WLawsRegulations;
import com.gl.wanfang.outdto.EasyWLawsRegulationsOutDTO;
import com.gl.wanfang.outdto.WLawsRegulationsOutDTO;
import org.springframework.stereotype.Component;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WLawsRegulationsMapper extends MyMapper<WLawsRegulations> {

	List<WLawsRegulations> getList(Map<String, Object> params);

	/**
	 * 法律法规模糊匹配
	 * @param params
	 * @return
	 */
	List<WLawsRegulations> getListByPage(Map<String, Object> params);

    List<EasyWLawsRegulationsOutDTO> getListByPage2(Map<String, Object> params);

	WLawsRegulationsOutDTO getOneByParams(Map<String, Object> params);
}
