package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中国专利 
 * @author code_generator
 */
@Table(name = "w_chinese_patent")
@Data
public class WChinesePatent implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Full_Name")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract;

	/** 专利代理机构 **/
	@Column(name="ORG_Agency")
    @ApiModelProperty(value = "专利代理机构")
	private String orgAgency ; 

	/** 代理人 **/
	@Column(name="Agent")
    @ApiModelProperty(value = "代理人")
	private String agent ; 

	/** 公开（公告）号 **/
	@Column(name="Publication_No")
    @ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ; 

	/** 公开（公告）日 **/
	@Column(name="Publication_Date")
    @ApiModelProperty(value = "公开（公告）日")
	private Date publicationDate ; 

	/** 发明（设计）人 **/
	@Column(name="Author")
    @ApiModelProperty(value = "发明（设计）人")
	private String author ; 

	/**  **/
	@Column(name="F_Author")
    @ApiModelProperty(value = "")
	private String fAuthor ; 

	/** 发明（设计）人 **/
	@Column(name="Author2")
    @ApiModelProperty(value = "发明（设计）人")
	private String author2 ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** 范畴分类 **/
	@Column(name="Cate")
    @ApiModelProperty(value = "范畴分类")
	private String cate ; 

	/** 光盘号 **/
	@Column(name="Cd_ID")
    @ApiModelProperty(value = "光盘号")
	private String cdId ; 

	/** 颁证日 **/
	@Column(name="Create_Date")
    @ApiModelProperty(value = "颁证日")
	private Date createDate ; 

	/** 分类号 **/
	@Column(name="Class_Code")
    @ApiModelProperty(value = "分类号")
	private String classCode ; 

	/** 国省代码 **/
	@Column(name="Country_Code")
    @ApiModelProperty(value = "国省代码")
	private String countryCode ; 

	/** 申请日 **/
	@Column(name="Date")
    @ApiModelProperty(value = "申请日")
	private Date date ; 

	/** 数据库标识 **/
	@Column(name="Database_Identity")
    @ApiModelProperty(value = "数据库标识")
	private String databaseIdentity ; 

	/** 进入国家日期 **/
	@Column(name="Dec")
    @ApiModelProperty(value = "进入国家日期")
	private Date dec ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 主权项 **/
	@Column(name="Ind_Cla")
    @ApiModelProperty(value = "主权项")
	private String indCla ; 

	/** 法律状态F_LawStatus **/
	@Column(name="Law_Status")
    @ApiModelProperty(value = "法律状态F_LawStatus")
	private String lawStatus ; 

	/**  **/
	@Column(name="Ls")
    @ApiModelProperty(value = "")
	private String ls ; 

	/** 规范单位名称 **/
	@Column(name="ORG_Norm_Name")
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName ; 

	/** 国际申请 **/
	@Column(name="Int_App")
    @ApiModelProperty(value = "国际申请")
	private String intApp ; 

	/** 国际公布 **/
	@Column(name="Int_Push")
    @ApiModelProperty(value = "国际公布")
	private String intPush ; 

	/** 失效专利 **/
	@Column(name="Inv_Pat")
    @ApiModelProperty(value = "失效专利")
	private String invPat ; 

	/** 关键词 **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 主分类号 **/
	@Column(name="Main_Class_Code")
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 中图分类(二级) **/
	@Column(name="Cls_Level2")
    @ApiModelProperty(value = "中图分类(二级)")
	private String clsLevel2 ; 

	/** 分案原申请号 **/
	@Column(name="Origin_Request_Number")
    @ApiModelProperty(value = "分案原申请号")
	private String originRequestNumber ; 

	/** 申请（专利权）人 **/
	@Column(name="ORG")
    @ApiModelProperty(value = "申请（专利权）人")
	private String org ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 申请（专利权）人 **/
	@Column(name="ORG_Anyname2_Name")
    @ApiModelProperty(value = "申请（专利权）人")
	private String orgAnyname2Name ; 

	/** 规范单位名称 **/
	@Column(name="ORG_Norm_Name2")
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName2 ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 专利号 **/
	@Column(name="PatentNumber_Name")
    @ApiModelProperty(value = "专利号")
	private String patentnumberName ; 

	/** 专利类型 **/
	@Column(name="PatentType_Name")
    @ApiModelProperty(value = "专利类型")
	private String patenttypeName ; 

	/** 页数 **/
	@Column(name="Pages")
    @ApiModelProperty(value = "页数")
	private String pages ; 

	/** 优先权 **/
	@Column(name="Priority")
    @ApiModelProperty(value = "优先权")
	private String priority ; 

	/** 发布路径 **/
	@Column(name="Pub_Path")
    @ApiModelProperty(value = "发布路径")
	private String pubPath ; 

	/** 参考文献 **/
	@Column(name="Reference")
    @ApiModelProperty(value = "参考文献")
	private String reference ; 

	/** 地址 **/
	@Column(name="Request_Address")
    @ApiModelProperty(value = "地址")
	private String requestAddress ; 

	/** 申请号 **/
	@Column(name="Request_Number")
    @ApiModelProperty(value = "申请号")
	private String requestNumber ; 

	/** 申请号wjy **/
	@Column(name="Request_No")
    @ApiModelProperty(value = "申请号wjy")
	private String requestNo ; 

	/** 申请（专利权）人 **/
	@Column(name="Request_People")
    @ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ; 

	/** 审查员 **/
	@Column(name="Reviewer")
    @ApiModelProperty(value = "审查员")
	private String reviewer ; 

	/** 服务器地址 **/
	@Column(name="Server_Address")
    @ApiModelProperty(value = "服务器地址")
	private String serverAddress ; 

	/** 数据来源 **/
	@Column(name="Data_Source")
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/** 名称 **/
	@Column(name="Title1")
    @ApiModelProperty(value = "名称")
	private String title1 ; 

	/** 名称 **/
	@Column(name="Title2")
    @ApiModelProperty(value = "名称")
	private String title2 ; 

	/**  **/
	@Column(name="Yannodate")
    @ApiModelProperty(value = "")
	private Date yannodate ; 

	/** 申请日 **/
	@Column(name="Date2")
    @ApiModelProperty(value = "申请日")
	private String date2 ; 

	/** 主分类号 **/
	@Column(name="Main_Class_Code2")
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ; 

	/**  **/
	@Column(name="Smcls")
    @ApiModelProperty(value = "")
	private String smcls ; 

	/**  **/
	@Column(name="Tmcls")
    @ApiModelProperty(value = "")
	private String tmcls ; 

	/**  **/
	@Column(name="Ckey")
    @ApiModelProperty(value = "")
	private String ckey ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构层级ID **/
	@Column(name="ORG_Hierarchy_ID")
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
	@Column(name="ORG_Province")
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
	@Column(name="ORG_City")
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/**  **/
	@Column(name="End_ORG_Type")
    @ApiModelProperty(value = "")
	private String endOrgType ; 

	/**  **/
	@Column(name="F_ORG_ID")
    @ApiModelProperty(value = "")
	private String fOrgId ; 

	/** 第一机构终级机构ID **/
	@Column(name="ORG_Frist_Final_ID")
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
	@Column(name="ORG_Frist_Hierarchy_ID")
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province2")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
	@Column(name="ORG_Frist_Final_City")
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
	@Column(name="ORG_Frist_City")
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
	@Column(name="ORG_Frist_Final_Type")
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
	@Column(name="ORG_Frist_Type")
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
	@Column(name="Author_ID")
    @ApiModelProperty(value = "")
	private String authorId ; 

	/**  **/
	@Column(name="ORG_Num")
    @ApiModelProperty(value = "")
	private String orgNum ; 

	/** 文献权重 **/
	@Column(name="Literature_Weight")
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/**  **/
	@Column(name="Quarter")
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
	@Column(name="Halfyear")
    @ApiModelProperty(value = "")
	private String halfyear ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/
	@Column(name="Author_Info")
    @ApiModelProperty(value = "")
	private String authorInfo ; 

	/** 作者信息.姓名 **/
	@Column(name="Author_Info_Name")
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
	@Column(name="Author_Info_Order")
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
	@Column(name="Author_Info_Unit")
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level1")
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level2")
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
	@Column(name="Author_Info_Unit_Type")
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
	@Column(name="Author_Info_Unit_Province")
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
	@Column(name="Author_Info_Unit_City")
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
	@Column(name="Author_Info_Unit_County")
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
	@Column(name="Author_Info_ID")
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
	@Column(name="Author_Info_Unit_ID")
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
	@Column(name="ORG_Info")
    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/
	@Column(name="ORG_Info_Name")
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
	@Column(name="ORG_Info_Order")
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
	@Column(name="ORG_Info_Type")
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
	@Column(name="ORG_Info_Province")
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
	@Column(name="ORG_Info_City")
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
	@Column(name="ORG_Info_County")
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
	@Column(name="ORG_Info_Hierarchy")
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
	@Column(name="ORG_Info_Level1")
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
	@Column(name="ORG_Info_Level2")
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
	@Column(name="ORG_Info_Level3")
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
	@Column(name="ORG_Info_Level4")
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
	@Column(name="ORG_Info_Level5")
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 


}
