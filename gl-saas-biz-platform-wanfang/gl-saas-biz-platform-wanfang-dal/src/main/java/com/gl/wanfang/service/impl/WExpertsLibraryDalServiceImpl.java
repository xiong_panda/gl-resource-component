package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WExpertsLibraryMapper;
import com.gl.wanfang.model.WExpertsLibrary;
import com.gl.wanfang.outdto.EasyWExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import com.gl.wanfang.service.IWExpertsLibraryDalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
@Slf4j
public class WExpertsLibraryDalServiceImpl implements IWExpertsLibraryDalService {
    @Autowired
    WExpertsLibraryMapper expertsLibraryMapper;

    /**
     * 分页模糊匹配专家
     *
     * @param page
     * @return
     */
    @Override
    public List<WExpertsLibrary> getListByPage(Map<String, Object> params) {
        return expertsLibraryMapper.getListByPage(params);
    }

    /**
     * 获取列表简略信息
     */
    public List<EasyWExpertsLibraryOutDto> getExpertInfoByDB2(Map<String, Object> params) {
        return expertsLibraryMapper.getExpertInfoByDB2(params);
    }

    public WExpertsLibraryOutDto getOneByParams(Map<String, Object> params) {
        return expertsLibraryMapper.getOneByParams(params);
    }
}
