package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 整合知识文献 
 * @author code_generator
 */
@Table(name = "mix_zswx")
@Data
public class MixZswx implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Full_Name")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 专利代理机构 **/
	@Column(name="ORG_Agency")
    @ApiModelProperty(value = "专利代理机构")
	private String orgAgency ; 

	/** 代理人 **/
	@Column(name="Agent")
    @ApiModelProperty(value = "代理人")
	private String agent ; 

	/** 公开（公告）号 **/
	@Column(name="Publication_No")
    @ApiModelProperty(value = "公开（公告）号")
	private String publicationNo ; 

	/** 公开（公告）日 **/
	@Column(name="Publication_Date")
    @ApiModelProperty(value = "公开（公告）日")
	private String publicationDate ; 

	/**  **/
	@Column(name="F_Author")
    @ApiModelProperty(value = "")
	private String fAuthor ; 

	/** 发明（设计）人 **/
	@Column(name="Author2")
    @ApiModelProperty(value = "发明（设计）人")
	private String author2 ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** 范畴分类 **/
	@Column(name="Cate")
    @ApiModelProperty(value = "范畴分类")
	private String cate ; 

	/** 光盘号 **/
	@Column(name="Cd_ID")
    @ApiModelProperty(value = "光盘号")
	private String cdId ; 

	/** 颁证日 **/
	@Column(name="Create_Date")
    @ApiModelProperty(value = "颁证日")
	private Date createDate ; 

	/** 分类号 **/
	@Column(name="Class_Code")
    @ApiModelProperty(value = "分类号")
	private String classCode ; 

	/** 国省代码 **/
	@Column(name="Country_Code")
    @ApiModelProperty(value = "国省代码")
	private String countryCode ; 

	/** 申请日 **/
	@Column(name="Date")
    @ApiModelProperty(value = "申请日")
	private Date date ; 

	/** 数据库标识 **/
	@Column(name="Database_Identity")
    @ApiModelProperty(value = "数据库标识")
	private String databaseIdentity ; 

	/** 进入国家日期 **/
	@Column(name="Dec")
    @ApiModelProperty(value = "进入国家日期")
	private Date dec ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 主权项 **/
	@Column(name="Ind_Cla")
    @ApiModelProperty(value = "主权项")
	private String indCla ; 

	/** 法律状态F_LawStatus **/
	@Column(name="Law_Status")
    @ApiModelProperty(value = "法律状态F_LawStatus")
	private String lawStatus ; 

	/**  **/
	@Column(name="Ls")
    @ApiModelProperty(value = "")
	private String ls ; 

	/** 规范单位名称 **/
	@Column(name="ORG_Norm_Name")
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName ; 

	/** 国际申请 **/
	@Column(name="Int_App")
    @ApiModelProperty(value = "国际申请")
	private String intApp ; 

	/** 国际公布 **/
	@Column(name="Int_Push")
    @ApiModelProperty(value = "国际公布")
	private String intPush ; 

	/** 失效专利 **/
	@Column(name="Inv_Pat")
    @ApiModelProperty(value = "失效专利")
	private String invPat ; 

	/** 关键词 **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 主分类号 **/
	@Column(name="Main_Class_Code")
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode ; 

	/** 中图分类(二级) **/
	@Column(name="Cls_Level2")
    @ApiModelProperty(value = "中图分类(二级)")
	private String clsLevel2 ; 

	/** 分案原申请号 **/
	@Column(name="Origin_Request_Number")
    @ApiModelProperty(value = "分案原申请号")
	private String originRequestNumber ; 

	/** 申请（专利权）人 **/
	@Column(name="ORG")
    @ApiModelProperty(value = "申请（专利权）人")
	private String org ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 申请（专利权）人 **/
	@Column(name="ORG_Anyname2_Name")
    @ApiModelProperty(value = "申请（专利权）人")
	private String orgAnyname2Name ; 

	/** 规范单位名称 **/
	@Column(name="ORG_Norm_Name2")
    @ApiModelProperty(value = "规范单位名称")
	private String orgNormName2 ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 专利号 **/
	@Column(name="PatentNumber_Name")
    @ApiModelProperty(value = "专利号")
	private String patentnumberName ; 

	/** 专利类型 **/
	@Column(name="PatentType_Name")
    @ApiModelProperty(value = "专利类型")
	private String patenttypeName ; 

	/** 页数 **/
	@Column(name="Pages")
    @ApiModelProperty(value = "页数")
	private String pages ; 

	/** 优先权 **/
	@Column(name="Priority")
    @ApiModelProperty(value = "优先权")
	private String priority ; 

	/** 发布路径 **/
	@Column(name="Pub_Path")
    @ApiModelProperty(value = "发布路径")
	private String pubPath ; 

	/** 参考文献 **/
	@Column(name="Reference")
    @ApiModelProperty(value = "参考文献")
	private String reference ; 

	/** 地址 **/
	@Column(name="Request_Address")
    @ApiModelProperty(value = "地址")
	private String requestAddress ; 

	/** 申请号 **/
	@Column(name="Request_Number")
    @ApiModelProperty(value = "申请号")
	private String requestNumber ; 

	/** 申请号wjy **/
	@Column(name="Request_No")
    @ApiModelProperty(value = "申请号wjy")
	private String requestNo ; 

	/** 申请（专利权）人 **/
	@Column(name="Request_People")
    @ApiModelProperty(value = "申请（专利权）人")
	private String requestPeople ; 

	/** 审查员 **/
	@Column(name="Reviewer")
    @ApiModelProperty(value = "审查员")
	private String reviewer ; 

	/** 服务器地址 **/
	@Column(name="Server_Address")
    @ApiModelProperty(value = "服务器地址")
	private String serverAddress ; 

	/** 数据来源 **/
	@Column(name="Data_Source")
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/** 名称 **/
	@Column(name="Title1")
    @ApiModelProperty(value = "名称")
	private String title1 ; 

	/**  **/
	@Column(name="Yannodate")
    @ApiModelProperty(value = "")
	private Date yannodate ; 

	/** 申请日 **/
	@Column(name="Date2")
    @ApiModelProperty(value = "申请日")
	private String date2 ; 

	/** 主分类号 **/
	@Column(name="Main_Class_Code2")
    @ApiModelProperty(value = "主分类号")
	private String mainClassCode2 ; 

	/**  **/
	@Column(name="Smcls")
    @ApiModelProperty(value = "")
	private String smcls ; 

	/**  **/
	@Column(name="Tmcls")
    @ApiModelProperty(value = "")
	private String tmcls ; 

	/**  **/
	@Column(name="Ckey")
    @ApiModelProperty(value = "")
	private String ckey ; 

	/** 机构层级ID **/
	@Column(name="ORG_Hierarchy_ID")
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
	@Column(name="ORG_Province")
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
	@Column(name="ORG_City")
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/**  **/
	@Column(name="End_ORG_Type")
    @ApiModelProperty(value = "")
	private String endOrgType ; 

	/****/
	@Column(name="F_ORG_ID")
    @ApiModelProperty(value = "")
	private String fOrgId ; 

	/** 第一机构终级机构ID **/
	@Column(name="ORG_Frist_Final_ID")
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
	@Column(name="ORG_Frist_Hierarchy_ID")
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province2")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
	@Column(name="ORG_Frist_Final_City")
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
	@Column(name="ORG_Frist_City")
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
	@Column(name="ORG_Frist_Final_Type")
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
	@Column(name="ORG_Frist_Type")
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
	@Column(name="Author_ID")
    @ApiModelProperty(value = "")
	private String authorId ; 

	/**  **/
	@Column(name="ORG_Num")
    @ApiModelProperty(value = "")
	private String orgNum ; 

	/** 文献权重 **/
	@Column(name="Literature_Weight")
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/**  **/
	@Column(name="Quarter")
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
	@Column(name="Halfyear")
    @ApiModelProperty(value = "")
	private String halfyear ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/
	@Column(name="Author_Info")
    @ApiModelProperty(value = "")
	private String authorInfo ; 

	/** 作者信息.姓名 **/
	@Column(name="Author_Info_Name")
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
	@Column(name="Author_Info_Order")
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
	@Column(name="Author_Info_Unit")
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level1")
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level2")
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
	@Column(name="Author_Info_Unit_Type")
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
	@Column(name="Author_Info_Unit_Province")
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
	@Column(name="Author_Info_Unit_City")
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
	@Column(name="Author_Info_Unit_County")
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
	@Column(name="Author_Info_ID")
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
	@Column(name="Author_Info_Unit_ID")
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
	@Column(name="ORG_Info")
    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/
	@Column(name="ORG_Info_Name")
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
	@Column(name="ORG_Info_Order")
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
	@Column(name="ORG_Info_Type")
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
	@Column(name="ORG_Info_Province")
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
	@Column(name="ORG_Info_City")
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
	@Column(name="ORG_Info_County")
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
	@Column(name="ORG_Info_Hierarchy")
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
	@Column(name="ORG_Info_Level1")
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
	@Column(name="ORG_Info_Level2")
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
	@Column(name="ORG_Info_Level3")
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
	@Column(name="ORG_Info_Level4")
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
	@Column(name="ORG_Info_Level5")
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 

	/**  **/
	@Column(name="Score")
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
	@Column(name="docId")
    @ApiModelProperty(value = "")
	private String docid ; 

	/**  **/
	@Column(name="Mid")
    @ApiModelProperty(value = "")
	private String mid ; 

	/**  **/
	@Column(name="DCK")
    @ApiModelProperty(value = "")
	private String dck ; 

	/**  **/
	@Column(name="GA")
    @ApiModelProperty(value = "")
	private String ga ; 

	/**  **/
	@Column(name="PC")
    @ApiModelProperty(value = "")
	private String pc ; 

	/**  **/
	@Column(name="PR")
    @ApiModelProperty(value = "")
	private String pr ; 

	/**  **/
	@Column(name="IPC")
    @ApiModelProperty(value = "")
	private String ipc ; 

	/**  **/
	@Column(name="IPCR")
    @ApiModelProperty(value = "")
	private String ipcr ; 

	/**  **/
	@Column(name="MIPC")
    @ApiModelProperty(value = "")
	private String mipc ; 

	/**  **/
	@Column(name="ZYFT")
    @ApiModelProperty(value = "")
	private String zyft ; 

	/**  **/
	@Column(name="QWFT")
    @ApiModelProperty(value = "")
	private String qwft ; 

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号  **/
	@Column(name="ID")
    @ApiModelProperty(value = "唯一编号 ")
	private String id ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 标题:英文标题 **/
	@Column(name="Title2")
    @ApiModelProperty(value = "标题:英文标题")
	private String title2 ; 

	/** 法规正文 **/
	@Column(name="Rules_Text")
    @ApiModelProperty(value = "法规正文")
	private String rulesText ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment ; 

	/** 颁布部门(全文检索) **/
	@Column(name="Proclaim_Department_FullText")
    @ApiModelProperty(value = "颁布部门(全文检索)")
	private String proclaimDepartmentFulltext ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department2")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment2 ; 

	/**  **/
	@Column(name="AUTHOR")
    @ApiModelProperty(value = "")
	private String author ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 颁布日期 **/
	@Column(name="Proclaim_Date")
    @ApiModelProperty(value = "颁布日期")
	private String proclaimDate ; 

	/** 终审日期 **/
	@Column(name="Final_Date")
    @ApiModelProperty(value = "终审日期")
	private String finalDate ; 

	/**  **/
	@Column(name="RID")
    @ApiModelProperty(value = "")
	private String rid ; 

	/** 标题 **/
	@Column(name="Title")
    @ApiModelProperty(value = "标题")
	private String title ; 

	/** 英文标题 **/
	@Column(name="English_Title")
    @ApiModelProperty(value = "英文标题")
	private String englishTitle ; 

	/** 发文文号 **/
	@Column(name="Post_Document_Code")
    @ApiModelProperty(value = "发文文号")
	private String postDocumentCode ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department3")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment3 ; 

	/** 行业地区码_部门代码 **/
	@Column(name="IndustryCode_Department_Code")
    @ApiModelProperty(value = "行业地区码_部门代码")
	private String industrycodeDepartmentCode ; 

	/** 效力级别 **/
	@Column(name="Value_Level")
    @ApiModelProperty(value = "效力级别")
	private String valueLevel ; 

	/** 效力代码 **/
	@Column(name="Value_Code")
    @ApiModelProperty(value = "效力代码")
	private String valueCode ; 

	/** 时效性 **/
	@Column(name="Timeliness")
    @ApiModelProperty(value = "时效性")
	private String timeliness ; 

	/** 批准日期 **/
	@Column(name="Approval_Date")
    @ApiModelProperty(value = "批准日期")
	private String approvalDate ; 

	/** 签字日期 **/
	@Column(name="Signature_Date")
    @ApiModelProperty(value = "签字日期")
	private String signatureDate ; 

	/**  **/
	@Column(name="YAPPRDATE")
    @ApiModelProperty(value = "")
	private String yapprdate ; 

	/**  **/
	@Column(name="YSIGNDATE")
    @ApiModelProperty(value = "")
	private String ysigndate ; 

	/** 实施日期 **/
	@Column(name="IMPLEMENTATION_Date")
    @ApiModelProperty(value = "实施日期")
	private String implementationDate ; 

	/** 失效日期 **/
	@Column(name="Invalid_Date")
    @ApiModelProperty(value = "失效日期")
	private String invalidDate ; 

	/** 终审法院 **/
	@Column(name="Final_Court")
    @ApiModelProperty(value = "终审法院")
	private String finalCourt ; 

	/** 终审日期 **/
	@Column(name="Final_Date2")
    @ApiModelProperty(value = "终审日期")
	private String finalDate2 ; 

	/** 调解日期 **/
	@Column(name="mediate_Date")
    @ApiModelProperty(value = "调解日期")
	private String mediateDate ; 

	/** 内容分类 **/
	@Column(name="Cintent_Class")
    @ApiModelProperty(value = "内容分类")
	private String cintentClass ; 

	/** 内容分类码 **/
	@Column(name="Cintent_Class_Code")
    @ApiModelProperty(value = "内容分类码")
	private String cintentClassCode ; 

	/** URL **/
	@Column(name="URL")
    @ApiModelProperty(value = "URL")
	private String url ; 

	/** PDF全文 **/
	@Column(name="PDF_FullText")
    @ApiModelProperty(value = "PDF全文")
	private String pdfFulltext ; 

	/** 相关链接 **/
	@Column(name="Relevant_link")
    @ApiModelProperty(value = "相关链接")
	private String relevantLink ; 

	/** 历史链接 **/
	@Column(name="History_link")
    @ApiModelProperty(value = "历史链接")
	private String historyLink ; 

	/** 库别代码 **/
	@Column(name="Library_Code")
    @ApiModelProperty(value = "库别代码")
	private String libraryCode ; 

	/** 制作日期 **/
	@Column(name="Make_Date")
    @ApiModelProperty(value = "制作日期")
	private String makeDate ; 

	/** 行业分类 **/
	@Column(name="Industry_Class")
    @ApiModelProperty(value = "行业分类")
	private String industryClass ; 

	/** 行业分类码 **/
	@Column(name="Industry_Class_Code")
    @ApiModelProperty(value = "行业分类码")
	private String industryClassCode ; 

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Topic")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String topic ; 

	/** DOI **/
	@Column(name="Doi")
    @ApiModelProperty(value = "DOI")
	private String doi ; 

	/** w_ID **/
	@Column(name="w_ID")
    @ApiModelProperty(value = "w_ID")
	private String wId ; 

	/** V_ID **/
	@Column(name="V_ID")
    @ApiModelProperty(value = "V_ID")
	private String vId ; 

	/** C_ID **/
	@Column(name="C_ID")
    @ApiModelProperty(value = "C_ID")
	private String cid ;

	/** L_ID **/
	@Column(name="L_ID")
    @ApiModelProperty(value = "L_ID")
	private String lId ; 

	/** 中文标题 **/
	@Column(name="Chinese_Title")
    @ApiModelProperty(value = "中文标题")
	private String chineseTitle ; 

	/** 中文标题:英文标题 **/
	@Column(name="C_E_Title")
    @ApiModelProperty(value = "中文标题:英文标题")
	private String cETitle ; 

	/** 中文作者 **/
	@Column(name="Chinese_Author")
    @ApiModelProperty(value = "中文作者")
	private String chineseAuthor ; 

	/** 英文作者 **/
	@Column(name="English_Author")
    @ApiModelProperty(value = "英文作者")
	private String englishAuthor ; 

	/** 作者个数 **/
	@Column(name="Number_Authors")
    @ApiModelProperty(value = "作者个数")
	private String numberAuthors ; 

	/** 作者FID **/
	@Column(name="Author_FID")
    @ApiModelProperty(value = "作者FID")
	private String authorFid ; 

	/** 中文第一作者 **/
	@Column(name="Chinese_First_Author")
    @ApiModelProperty(value = "中文第一作者")
	private String chineseFirstAuthor ; 

	/** 英文第一作者 **/
	@Column(name="English_First_Author")
    @ApiModelProperty(value = "英文第一作者")
	private String englishFirstAuthor ; 

	/** 作者个数 **/
	@Column(name="Author_Number")
    @ApiModelProperty(value = "作者个数")
	private String authorNumber ; 

	/** 中文作者:英文作者 **/
	@Column(name="Chinese_English_Author")
    @ApiModelProperty(value = "中文作者:英文作者")
	private String chineseEnglishAuthor ; 

	/** 规范单位名称 **/
	@Column(name="Authority_Unit_Name")
    @ApiModelProperty(value = "规范单位名称")
	private String authorityUnitName ; 

	/** 中文作者单位 **/
	@Column(name="Chinese_Author_Unit")
    @ApiModelProperty(value = "中文作者单位")
	private String chineseAuthorUnit ; 

	/** 英文作者单位 **/
	@Column(name="English_Author_Unit")
    @ApiModelProperty(value = "英文作者单位")
	private String englishAuthorUnit ; 

	/** 中文第一作者单位 **/
	@Column(name="Chinese_First_Author_Unit")
    @ApiModelProperty(value = "中文第一作者单位")
	private String chineseFirstAuthorUnit ; 

	/** 中文第一作者单位一级名称 **/
	@Column(name="Chinese_First_Author_Unit_Name")
    @ApiModelProperty(value = "中文第一作者单位一级名称")
	private String chineseFirstAuthorUnitName ; 

	/** 英文第一作者单位 **/
	@Column(name="First_English_Author")
    @ApiModelProperty(value = "英文第一作者单位")
	private String firstEnglishAuthor ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 中文作者单位:英文作者单位:规范单位名称 **/
	@Column(name="C_E_S_Author_Unit")
    @ApiModelProperty(value = "中文作者单位:英文作者单位:规范单位名称")
	private String cESAuthorUnit ; 

	/** QKID **/
	@Column(name="QkID")
    @ApiModelProperty(value = "QKID")
	private String qkid ; 

	/**  **/
	@Column(name="Issn")
    @ApiModelProperty(value = "")
	private String issn ; 

	/** 中文刊名 **/
	@Column(name="Chinese_Journal_Name")
    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName ; 

	/** 英文刊名 **/
	@Column(name="English_Journal_Name")
    @ApiModelProperty(value = "英文刊名")
	private String englishJournalName ; 

	/**  **/
	@Column(name="Journal")
    @ApiModelProperty(value = "")
	private String journal ; 

	/**  **/
	@Column(name="Journal_Anyname")
    @ApiModelProperty(value = "")
	private String journalAnyname ; 

	/**  **/
	@Column(name="Journal_Anyname2")
    @ApiModelProperty(value = "")
	private String journalAnyname2 ; 

	/** 中文刊名 **/
	@Column(name="Chinese_Journal_Name2")
    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName2 ; 

	/**  **/
	@Column(name="Fjoucn_Anyname")
    @ApiModelProperty(value = "")
	private String fjoucnAnyname ; 

	/**  **/
	@Column(name="Fjoucn_Anyname2")
    @ApiModelProperty(value = "")
	private String fjoucnAnyname2 ; 

	/** 年 **/
	@Column(name="Year")
    @ApiModelProperty(value = "年")
	private String year ; 

	/** 卷 **/
	@Column(name="Volume")
    @ApiModelProperty(value = "卷")
	private String volume ; 

	/** 期 **/
	@Column(name="Period")
    @ApiModelProperty(value = "期")
	private String period ; 

	/** 页码 **/
	@Column(name="Page_Number")
    @ApiModelProperty(value = "页码")
	private String pageNumber ; 

	/** 中文栏目名称 **/
	@Column(name="Chinese_Column_Name")
    @ApiModelProperty(value = "中文栏目名称")
	private String chineseColumnName ; 

	/** 英文栏目名称 **/
	@Column(name="English_Column_Name")
    @ApiModelProperty(value = "英文栏目名称")
	private String englishColumnName ; 

	/** 语种 **/
	@Column(name="Language")
    @ApiModelProperty(value = "语种")
	private String language ; 

	/** 中图分类号:机标分类号 **/
	@Column(name="Cls_Machine_Standard_Code")
    @ApiModelProperty(value = "中图分类号:机标分类号")
	private String clsMachineStandardCode ; 

	/** 文献标识码 **/
	@Column(name="Literature_IDentification_Code")
    @ApiModelProperty(value = "文献标识码")
	private String literatureIdentificationCode ; 

	/** 机标分类号 **/
	@Column(name="Machine_Label_Class_Code")
    @ApiModelProperty(value = "机标分类号")
	private String machineLabelClassCode ; 

	/** 中图分类号 **/
	@Column(name="Cls_Code")
    @ApiModelProperty(value = "中图分类号")
	private String clsCode ; 

	/** 中图分类顶级 **/
	@Column(name="Cls_Top")
    @ApiModelProperty(value = "中图分类顶级")
	private String clsTop ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
	@Column(name="IID")
    @ApiModelProperty(value = "")
	private String iid ; 

	/** 中文关键词 **/
	@Column(name="Chinese_KeyWord")
    @ApiModelProperty(value = "中文关键词")
	private String chineseKeyword ; 

	/** 中文关键词:英文关键词:机标关键词 **/
	@Column(name="Ce_KeyWord")
    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String ceKeyword ; 

	/** 英文关键词 **/
	@Column(name="English_KeyWord")
    @ApiModelProperty(value = "英文关键词")
	private String englishKeyword ; 

	/** 机标关键词 **/
	@Column(name="Machine_KeyWord")
    @ApiModelProperty(value = "机标关键词")
	private String machineKeyword ; 

	/** 中文摘要 **/
	@Column(name="Chinese_Abstract")
    @ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ; 

	/** 中文摘要:英文摘要:正文首段 **/
	@Column(name="C_Paragraph")
    @ApiModelProperty(value = "中文摘要:英文摘要:正文首段")
	private String cParagraph ; 

	/** 英文摘要 **/
	@Column(name="English_Abstract")
    @ApiModelProperty(value = "英文摘要")
	private String englishAbstract ; 

	/** 正文首段 **/
	@Column(name="First_Paragraph")
    @ApiModelProperty(value = "正文首段")
	private String firstParagraph ; 

	/** 基金 **/
	@Column(name="Fund")
    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/
	@Column(name="Fund_FullText_Search")
    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/** 基金名称 **/
	@Column(name="Fund_Name")
    @ApiModelProperty(value = "基金名称")
	private String fundName ; 

	/** 基金名称 **/
	@Column(name="Fund_Name2")
    @ApiModelProperty(value = "基金名称")
	private String fundName2 ; 

	/** 基金项目 **/
	@Column(name="Fund_Project")
    @ApiModelProperty(value = "基金项目")
	private String fundProject ; 

	/** 基金名称 **/
	@Column(name="Fund_Name3")
    @ApiModelProperty(value = "基金名称")
	private String fundName3 ; 

	/** BYCS **/
	@Column(name="Bycs")
    @ApiModelProperty(value = "BYCS")
	private String bycs ; 

	/** 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊 **/
	@Column(name="Core")
    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
	private String core ; 

	/**  **/
	@Column(name="FORGID")
    @ApiModelProperty(value = "")
	private String forgid ;

	/**  **/
	@Column(name="EndORGtype")
    @ApiModelProperty(value = "")
	private String endorgtype ; 

	/** 全部作者ID **/
	@Column(name="All_Author_ID")
    @ApiModelProperty(value = "全部作者ID")
	private String allAuthorId ; 

	/**  **/
	@Column(name="ORG_Number")
    @ApiModelProperty(value = "")
	private String orgNumber ; 


	/**  **/
	@Column(name="NID")
    @ApiModelProperty(value = "")
	private String nid ; 

	/** 作者译名 **/
	@Column(name="Author_Translatio")
    @ApiModelProperty(value = "作者译名")
	private String authorTranslatio ; 

	/** 第一作者 **/
	@Column(name="First_Author_ID")
    @ApiModelProperty(value = "第一作者")
	private String firstAuthorId ; 

	/**  **/
	@Column(name="Fau")
    @ApiModelProperty(value = "")
	private String fau ; 

	/**  **/
	@Column(name="Faue")
    @ApiModelProperty(value = "")
	private String faue ; 

	/** 作者姓名:作者译名 **/
	@Column(name="T_N_Author")
    @ApiModelProperty(value = "作者姓名:作者译名")
	private String tNAuthor ; 

	/** 作者单位名称 **/
	@Column(name="Author_Unit_Name")
    @ApiModelProperty(value = "作者单位名称")
	private String authorUnitName ; 

	/** 作者单位规范名称 **/
	@Column(name="Author_Unit_Specification_Name")
    @ApiModelProperty(value = "作者单位规范名称")
	private String authorUnitSpecificationName ; 

	/** 作者单位译名 **/
	@Column(name="Author_Unit_Translation")
    @ApiModelProperty(value = "作者单位译名")
	private String authorUnitTranslation ; 

	/**  **/
	@Column(name="FORG")
    @ApiModelProperty(value = "")
	private String forg ; 

	/** 作者单位名称 **/
	@Column(name="FORG_Unit_Name")
    @ApiModelProperty(value = "作者单位名称")
	private String forgUnitName ; 

	/**  **/
	@Column(name="FORGe")
    @ApiModelProperty(value = "")
	private String forge ; 

	/** 作者单位名称:作者单位规范名称:作者单位译名 **/
	@Column(name="U_S_Author_Anyname")
    @ApiModelProperty(value = "作者单位名称:作者单位规范名称:作者单位译名")
	private String uSAuthorAnyname ; 


	/** 学科分类:学科分类机标 **/
	@Column(name="Discipline_Class_Code")
    @ApiModelProperty(value = "学科分类:学科分类机标")
	private String disciplineClassCode ; 

	/** 协会级别 **/
	@Column(name="Association_Level")
    @ApiModelProperty(value = "协会级别")
	private String associationLevel ; 

	/** 中文关键词:英文关键词:机标关键词 **/
	@Column(name="C_E_Keyword")
    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String cEKeyword ; 

	/**  **/
	@Column(name="F_Abstract")
    @ApiModelProperty(value = "")
	private String fAbstract ; 

	/** 中文摘要:英文摘要 **/
	@Column(name="C_E_Abstract")
    @ApiModelProperty(value = "中文摘要:英文摘要")
	private String cEAbstract ; 

	/**  **/
	@Column(name="Lan")
    @ApiModelProperty(value = "")
	private String lan ; 

	/** 母体文献 **/
	@Column(name="Parent_Literature")
    @ApiModelProperty(value = "母体文献")
	private String parentLiterature ; 

	/**  **/
	@Column(name="Parent_Literature_Name")
    @ApiModelProperty(value = "")
	private String parentLiteratureName ; 

	/**  **/
	@Column(name="Parent_Literature_Name2")
    @ApiModelProperty(value = "")
	private String parentLiteratureName2 ; 

	/** 学会名称 **/
	@Column(name="Institute_Name")
    @ApiModelProperty(value = "学会名称")
	private String instituteName ; 

	/**  **/
	@Column(name="Institute_Name_Anyname")
    @ApiModelProperty(value = "")
	private String instituteNameAnyname ; 

	/**  **/
	@Column(name="Sn_Anyname")
    @ApiModelProperty(value = "")
	private String snAnyname ; 

	/** 会议名称 **/
	@Column(name="Conference_Title")
    @ApiModelProperty(value = "会议名称")
	private String conferenceTitle ; 

	/**  **/
	@Column(name="Conference_Title_Anyname")
    @ApiModelProperty(value = "")
	private String conferenceTitleAnyname ; 

	/** 会议届次 **/
	@Column(name="Conference_Sessions")
    @ApiModelProperty(value = "会议届次")
	private String conferenceSessions ; 

	/** 会议地点 **/
	@Column(name="Conference_Meeting_Place")
    @ApiModelProperty(value = "会议地点")
	private String conferenceMeetingPlace ; 

	/**  **/
	@Column(name="HID")
    @ApiModelProperty(value = "")
	private String hid ; 

	/** 主办单位 **/
	@Column(name="ORGanizer")
    @ApiModelProperty(value = "主办单位")
	private String organizer ; 

	/** 基金 **/
	@Column(name="F_Fund")
    @ApiModelProperty(value = "基金")
	private String fFund ; 

	/**  **/
	@Column(name="Fpn")
    @ApiModelProperty(value = "")
	private String fpn ; 

	/**  **/
	@Column(name="Fundsupport")
    @ApiModelProperty(value = "")
	private String fundsupport ; 

	/** 会议时间 **/
	@Column(name="Start_Meeting_Date")
    @ApiModelProperty(value = "会议时间")
	private String startMeetingDate ; 

	/** 会议时间 **/
	@Column(name="End_Meeting_Date")
    @ApiModelProperty(value = "会议时间")
	private String endMeetingDate ; 

	/** 出版时间 **/
	@Column(name="Start_Published_Date")
    @ApiModelProperty(value = "出版时间")
	private String startPublishedDate ; 

	/** 出版时间 **/
	@Column(name="End_Published_Date")
    @ApiModelProperty(value = "出版时间")
	private String endPublishedDate ; 

	/**  **/
	@Column(name="ORGnum")
    @ApiModelProperty(value = "")
	private String orgnum ; 

	/**  **/
	@Column(name="Rn")
    @ApiModelProperty(value = "")
	private String rn ; 

	/** 作者单位ID **/
	@Column(name="Author_Unit_ID")
    @ApiModelProperty(value = "作者单位ID")
	private String authorUnitId ; 

	/** 数据来源 **/
	@Column(name="data_flag")
    @ApiModelProperty(value = "数据来源")
	private String dataFlag ; 

	/**  **/
	@Column(name="Ecore")
    @ApiModelProperty(value = "")
	private String ecore ; 


}
