package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 高等院校
 * @author code_generator
 */
@Table(name = "W_HIGHER_LEARNING_UNIVERSITIES")
@Data
public class WHigherLearningUniversities implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
	@Id
	@GeneratedValue(generator = "JDBC")
    @ApiModelProperty(value = "唯一编号")
//	private Integer id ;
	private String id ;

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 学校名称 **/
	@Column(name="Name")
    @ApiModelProperty(value = "学校名称")
	private String name ; 

	/**  **/
	@Column(name="Name_Anyname1")
    @ApiModelProperty(value = "")
	private String nameAnyname1 ; 

	/**  **/
	@Column(name="Name_Anyname2")
    @ApiModelProperty(value = "")
	private String nameAnyname2 ; 

	/** 学校名称 **/
	@Column(name="School_Name")
    @ApiModelProperty(value = "学校名称")
	private String schoolName ; 

	/** 曾用名 **/
	@Column(name="Ohther_Name")
    @ApiModelProperty(value = "曾用名")
	private String ohtherName ; 

	/** 主管单位 **/
	@Column(name="Compunit")
    @ApiModelProperty(value = "主管单位")
	private String compunit ; 

	/**  **/
	@Column(name="Compunit_Iko")
    @ApiModelProperty(value = "")
	private String compunitIko ; 

	/**  **/
	@Column(name="Iko")
    @ApiModelProperty(value = "")
	private String iko ; 

	/** 省名 **/
	@Column(name="Province")
    @ApiModelProperty(value = "省名")
	private String province ; 

	/** 市名 **/
	@Column(name="City")
    @ApiModelProperty(value = "市名")
	private String city ; 

	/** 县名 **/
	@Column(name="County")
    @ApiModelProperty(value = "县名")
	private String county ; 

	/** 区号 **/
	@Column(name="Acode")
    @ApiModelProperty(value = "区号")
	private String acode ; 

	/** 行政区代码 **/
	@Column(name="Region")
    @ApiModelProperty(value = "行政区代码")
	private String region ; 

	/** 邮码 **/
	@Column(name="Postal_Code")
    @ApiModelProperty(value = "邮码")
	private String postalCode ; 

	/** 地址 **/
	@Column(name="Address")
    @ApiModelProperty(value = "地址")
	private String address ; 

	/** 电话 **/
	@Column(name="Telphone")
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 传真 **/
	@Column(name="Fax")
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 域名地址 **/
	@Column(name="Url")
    @ApiModelProperty(value = "域名地址")
	private String url ; 

	/** 专职教师数 **/
	@Column(name="Techs")
    @ApiModelProperty(value = "专职教师数")
	private String techs ; 

	/** 电子邮件 **/
	@Column(name="Email")
    @ApiModelProperty(value = "电子邮件")
	private String email ; 

	/** 占地面积(平方米) **/
	@Column(name="Space")
    @ApiModelProperty(value = "占地面积(平方米)")
	private String space ; 

	/** 办学层次 **/
	@Column(name="Levels")
    @ApiModelProperty(value = "办学层次")
	private String levels ; 

	/** 负责人 **/
	@Column(name="Person")
    @ApiModelProperty(value = "负责人")
	private String person ; 

	/** 办学类型 **/
	@Column(name="School_Type")
    @ApiModelProperty(value = "办学类型")
	private String schoolType ; 

	/** 重点学科 **/
	@Column(name="KeyDiscipline")
    @ApiModelProperty(value = "重点学科")
	private String keydiscipline ; 

	/** 特色高校 **/
	@Column(name="Special_School")
    @ApiModelProperty(value = "特色高校")
	private String specialSchool ; 

	/** 学校简介 **/
	@Column(name="Intro")
    @ApiModelProperty(value = "学校简介")
	private String intro ; 

	/** 分校及校区 **/
	@Column(name="Campus")
    @ApiModelProperty(value = "分校及校区")
	private String campus ; 

	/** 教学机构设置 **/
	@Column(name="ORG_Setting")
    @ApiModelProperty(value = "教学机构设置")
	private String orgSetting ; 

	/** 历史沿革 **/
	@Column(name="History")
    @ApiModelProperty(value = "历史沿革")
	private String history ; 

	/** 并入从属院校 **/
	@Column(name="Sub_School")
    @ApiModelProperty(value = "并入从属院校")
	private String subSchool ; 

	/** 教学科研仪器总值(万元) **/
	@Column(name="Gross")
    @ApiModelProperty(value = "教学科研仪器总值(万元)")
	private String gross ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/**  **/
	@Column(name="Featurecollege")
    @ApiModelProperty(value = "")
	private String featurecollege ; 

	/**  **/
	@Column(name="Featurecollege_Anyname1")
    @ApiModelProperty(value = "")
	private String featurecollegeAnyname1 ; 

	/**  **/
	@Column(name="Featurecollege_Anyname2")
    @ApiModelProperty(value = "")
	private String featurecollegeAnyname2 ; 

	/** 高校类型 **/
	@Column(name="College_Type")
    @ApiModelProperty(value = "高校类型")
	private String collegeType ; 

	/** 是否成品数据 **/
	@Column(name="Finished_Product_Data")
    @ApiModelProperty(value = "是否成品数据")
	private String finishedProductData ; 


}
