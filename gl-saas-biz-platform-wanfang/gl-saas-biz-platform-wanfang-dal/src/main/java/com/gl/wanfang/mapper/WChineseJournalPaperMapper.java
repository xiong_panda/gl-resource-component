package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WChineseJournalPaper;
import com.gl.wanfang.outdto.EasyWChineseJournalPaperOutDto;
import com.gl.wanfang.outdto.WChineseJournalPaperOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WChineseJournalPaperMapper extends MyMapper<WChineseJournalPaper> {

    List<WChineseJournalPaper> getList(Map<String, Object> params);


    /**
     * 中文期刊论文
     *
     * @param params
     * @return
     */
    List<WChineseJournalPaper> getListByPage(Map<String, Object> params);

    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WChineseJournalPaperOutDto getOneByParams(Map<String, Object> params);

    /**
     * 获取简略信息
     *
     * @param params
     * @return
     */
    List<EasyWChineseJournalPaperOutDto> getListByPage2(Map<String, Object> params);
}
