package com.gl.wanfang.service;

import com.gl.wanfang.model.WChineseConferenceCitations;
import com.gl.wanfang.model.WChineseConferencePaper;
import com.gl.wanfang.outdto.EasyWChineseConferencePaperOutDto;
import com.gl.wanfang.outdto.WChineseConferencePaperOutDto;

import java.util.List;
import java.util.Map;

public interface IWChineseConferenceCitationsDalService {
    /**
     * 分页匹配模糊查询
     *
     * @param params
     * @return
     */
    List<WChineseConferencePaper> getListByPage(Map<String, Object> params);


    /**
     * 获取列表详情
     *
     * @param params
     * @return
     */
    List<EasyWChineseConferencePaperOutDto> getListByPage2(Map<String, Object> params);


    /**
     * 获取详情
     *
     * @param params
     * @return
     */
    WChineseConferencePaperOutDto getOneByParams(Map<String, Object> params);
}
