package com.gl.wanfang.mapper;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.BzHotAndRecommend;
import com.gl.wanfang.outdto.GatherOutDTO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @auther Qinye
 * @date 2020/5/11 14:33
 * @description
 */
@MapperPrimary
@Component
public interface UserSearchGatherMapper extends MyMapper<BzHotAndRecommend> {
    List<GatherOutDTO> getHotAndRecommend(BzHotAndRecommend bzHotAndRecommend);
}
