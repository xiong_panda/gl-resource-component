package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.IFieldIndustryMapper;
import com.gl.wanfang.model.IndustryFieldDTO;
import com.gl.wanfang.service.IUserAnalysisDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class UserAnalysisImplDalService implements IUserAnalysisDalService {

    @Autowired
    IFieldIndustryMapper fieldIndustryMapper;

    /**
     * 根据用户信息获取领域行业信息
     * @param userId
     * @return
     */
    @Override
    public IndustryFieldDTO getIndustryFiledByLogin(String userId) {
        IndustryFieldDTO industryFiledByUserId = fieldIndustryMapper.getIndustryFiledByUserId(userId);
        return industryFiledByUserId;
    }

    /**
     * 所有领域行业
     * 处理成树结构
     * @return
     */
    @Override
    public List<Map<String, Object>> getIndustryFieldAll() {
        List<Map<String, Object>> industryFieldAll = fieldIndustryMapper.getIndustryFieldAll();
        List<Map<String, Object>> levelData = getLevelData(industryFieldAll, "0");
        return levelData;
    }


    /**
     * 将数据处理成父子节点数据
     * @param dbList
     * @param parentcode
     * @return
     */
    private List<Map<String, Object>> getLevelData(List<Map<String, Object>> dbList, String parentcode) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (Map<String, Object> data : dbList) {
            if (data.get("pid") .equals(parentcode) ) {
                List<Map<String, Object>> childList = getLevelData(dbList, data.get("id").toString());
                data.put("children", childList);
                resultList.add(data);
            }
        }
        return resultList;
    }



}
