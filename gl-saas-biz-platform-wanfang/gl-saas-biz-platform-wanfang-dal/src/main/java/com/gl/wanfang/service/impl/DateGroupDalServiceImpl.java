package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.IDateGroupMapper;
import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.outdto.EasyWChineseConferencePaperOutDto;
import com.gl.wanfang.service.IDateGroupDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DateGroupDalServiceImpl implements IDateGroupDalService {

    @Autowired
    IDateGroupMapper groupMapper;

    /**
     *
     * @param groupDate:聚合字段
     * @param tableName；表名
     * @return
     */
    @Override
    public List<DateGroupByEntity> getDataGroup(String groupDate, String tableName){
        List<DateGroupByEntity> dateGroup = groupMapper.getDataGroup(groupDate, tableName.toUpperCase());
        return dateGroup;
    }




}
