package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 中文会议引文 
 * @author code_generator
 */
@Table(name = "W_CHINESE_CONFERENCE_CITATIONS")
@Data
public class WChineseConferenceCitations implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 唯一编号 **/
	@Id
	@GeneratedValue(generator = "JDBC")
    @ApiModelProperty(value = "唯一编号")
	private Integer id ; 

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/**  **/
	@Column(name="Type")
    @ApiModelProperty(value = "")
	private String type ; 

	/**  **/
	@Column(name="LwID")
    @ApiModelProperty(value = "")
	private String lwid ; 

	/**  **/
	@Column(name="Doi")
    @ApiModelProperty(value = "")
	private String doi ; 

	/** Lw_title **/
	@Column(name="Lw_Title")
    @ApiModelProperty(value = "Lw_title")
	private String lwTitle ; 

	/** 作者 **/
	@Column(name="Author")
    @ApiModelProperty(value = "作者")
	private String author ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/**  **/
	@Column(name="AuthorID")
    @ApiModelProperty(value = "")
	private String authorid ; 

	/**  **/
	@Column(name="Tea")
    @ApiModelProperty(value = "")
	private String tea ; 

	/**  **/
	@Column(name="Tea_Tea_Anyname")
    @ApiModelProperty(value = "")
	private String teaTeaAnyname ; 

	/**  **/
	@Column(name="Tea_Anyname")
    @ApiModelProperty(value = "")
	private String teaAnyname ; 

	/** Lw_QKID **/
	@Column(name="Lw_QkID")
    @ApiModelProperty(value = "Lw_QKID")
	private String lwQkid ; 

	/** Lw_Journal **/
	@Column(name="Lw_Journal")
    @ApiModelProperty(value = "Lw_Journal")
	private String lwJournal ; 

	/**  **/
	@Column(name="Lw_Journal_Joucn_Anyname")
    @ApiModelProperty(value = "")
	private String lwJournalJoucnAnyname ; 

	/**  **/
	@Column(name="Joucn_Anyname")
    @ApiModelProperty(value = "")
	private String joucnAnyname ; 

	/** Lw_Year **/
	@Column(name="Lw_Year")
    @ApiModelProperty(value = "Lw_Year")
	private String lwYear ; 

	/** Lw_VOL **/
	@Column(name="Lw_Vol")
    @ApiModelProperty(value = "Lw_VOL")
	private String lwVol ; 

	/** Lw_Issue **/
	@Column(name="Lw_Issue")
    @ApiModelProperty(value = "Lw_Issue")
	private String lwIssue ; 

	/**  **/
	@Column(name="Degree")
    @ApiModelProperty(value = "")
	private String degree ; 

	/**  **/
	@Column(name="Rn")
    @ApiModelProperty(value = "")
	private String rn ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 机构全文检索 **/
	@Column(name="ORG_FullText_Search")
    @ApiModelProperty(value = "机构全文检索")
	private String orgFulltextSearch ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 机构层级ID **/
	@Column(name="ORG_Hierarchy_ID")
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构所在省 **/
	@Column(name="ORG_Province")
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
	@Column(name="ORG_City")
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/** 第一机构终级机构ID **/
	@Column(name="ORG_Frist_Final_ID")
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
	@Column(name="ORG_Frist_Hierarchy_ID")
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province2")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构所在市 **/
	@Column(name="ORG_Frist_Final_City")
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在市 **/
	@Column(name="ORG_Frist_City")
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构类型 **/
	@Column(name="ORG_Frist_Final_Type")
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
	@Column(name="ORG_Frist_Type")
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/**  **/
	@Column(name="ZcID")
    @ApiModelProperty(value = "")
	private String zcid ; 

	/**  **/
	@Column(name="TzcID")
    @ApiModelProperty(value = "")
	private String tzcid ; 

	/**  **/
	@Column(name="SzcID")
    @ApiModelProperty(value = "")
	private String szcid ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
	@Column(name="Ckey")
    @ApiModelProperty(value = "")
	private String ckey ; 

	/**  **/
	@Column(name="Ekey")
    @ApiModelProperty(value = "")
	private String ekey ; 

	/**  **/
	@Column(name="Mn")
    @ApiModelProperty(value = "")
	private String mn ; 

	/** 基金 **/
	@Column(name="Fund")
    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/
	@Column(name="Fund_FullText_Search")
    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/**  **/
	@Column(name="Fund_Anyname")
    @ApiModelProperty(value = "")
	private String fundAnyname ; 

	/**  **/
	@Column(name="Status")
    @ApiModelProperty(value = "")
	private String status ; 

	/**  **/
	@Column(name="Patt")
    @ApiModelProperty(value = "")
	private String patt ; 

	/**  **/
	@Column(name="Core")
    @ApiModelProperty(value = "")
	private String core ; 

	/**  **/
	@Column(name="Score")
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
	@Column(name="Ywtype")
    @ApiModelProperty(value = "")
	private String ywtype ; 

	/**  **/
	@Column(name="YwID")
    @ApiModelProperty(value = "")
	private String ywid ; 

	/**  **/
	@Column(name="Ywdoi")
    @ApiModelProperty(value = "")
	private String ywdoi ; 

	/**  **/
	@Column(name="Ywtitle")
    @ApiModelProperty(value = "")
	private String ywtitle ; 

	/**  **/
	@Column(name="Ywauthor")
    @ApiModelProperty(value = "")
	private String ywauthor ; 

	/**  **/
	@Column(name="Ywauthor_Anyname_FullText")
    @ApiModelProperty(value = "")
	private String ywauthorAnynameFulltext ; 

	/**  **/
	@Column(name="Ywauthor_Anyname")
    @ApiModelProperty(value = "")
	private String ywauthorAnyname ; 

	/**  **/
	@Column(name="YwauthorID")
    @ApiModelProperty(value = "")
	private String ywauthorid ; 

	/**  **/
	@Column(name="Ywtea")
    @ApiModelProperty(value = "")
	private String ywtea ; 

	/**  **/
	@Column(name="Ywtea_Anyname_FullText")
    @ApiModelProperty(value = "")
	private String ywteaAnynameFulltext ; 

	/**  **/
	@Column(name="Ywtea_Anyname")
    @ApiModelProperty(value = "")
	private String ywteaAnyname ; 

	/**  **/
	@Column(name="YwjouID")
    @ApiModelProperty(value = "")
	private String ywjouid ; 

	/**  **/
	@Column(name="Ywjoucn")
    @ApiModelProperty(value = "")
	private String ywjoucn ; 

	/**  **/
	@Column(name="Ywjoucn_Anyname_FullText")
    @ApiModelProperty(value = "")
	private String ywjoucnAnynameFulltext ; 

	/**  **/
	@Column(name="Ywjoucn_Anyname")
    @ApiModelProperty(value = "")
	private String ywjoucnAnyname ; 

	/**  **/
	@Column(name="Ywyear")
    @ApiModelProperty(value = "")
	private String ywyear ; 

	/**  **/
	@Column(name="Ywvol")
    @ApiModelProperty(value = "")
	private String ywvol ; 

	/**  **/
	@Column(name="Ywper")
    @ApiModelProperty(value = "")
	private String ywper ; 

	/**  **/
	@Column(name="Ywdegree")
    @ApiModelProperty(value = "")
	private String ywdegree ; 

	/**  **/
	@Column(name="Ywrn")
    @ApiModelProperty(value = "")
	private String ywrn ; 

	/**  **/
	@Column(name="YwORG")
    @ApiModelProperty(value = "")
	private String yworg ; 

	/**  **/
	@Column(name="YwORG_Anyname_FullText")
    @ApiModelProperty(value = "")
	private String yworgAnynameFulltext ; 

	/**  **/
	@Column(name="YwORG_Anyname")
    @ApiModelProperty(value = "")
	private String yworgAnyname ; 

	/**  **/
	@Column(name="YwORGID")
    @ApiModelProperty(value = "")
	private String yworgid ; 

	/**  **/
	@Column(name="YwORGtype")
    @ApiModelProperty(value = "")
	private String yworgtype ; 

	/**  **/
	@Column(name="YwzcID")
    @ApiModelProperty(value = "")
	private String ywzcid ; 

	/**  **/
	@Column(name="YwtzcID")
    @ApiModelProperty(value = "")
	private String ywtzcid ; 

	/**  **/
	@Column(name="YwszcID")
    @ApiModelProperty(value = "")
	private String ywszcid ; 

	/**  **/
	@Column(name="YwdzcID")
    @ApiModelProperty(value = "")
	private String ywdzcid ; 

	/**  **/
	@Column(name="Ywckey")
    @ApiModelProperty(value = "")
	private String ywckey ; 

	/**  **/
	@Column(name="Ywekey")
    @ApiModelProperty(value = "")
	private String ywekey ; 

	/**  **/
	@Column(name="Ywmn")
    @ApiModelProperty(value = "")
	private String ywmn ; 

	/**  **/
	@Column(name="Ywfund")
    @ApiModelProperty(value = "")
	private String ywfund ; 

	/**  **/
	@Column(name="Ywfund_Ywfund_Anyname_FullText")
    @ApiModelProperty(value = "")
	private String ywfundYwfundAnynameFulltext ; 

	/**  **/
	@Column(name="Ywfund_Ywfund_Anyname")
    @ApiModelProperty(value = "")
	private String ywfundYwfundAnyname ; 

	/**  **/
	@Column(name="Ywpatt")
    @ApiModelProperty(value = "")
	private String ywpatt ; 

	/**  **/
	@Column(name="Ywstatus")
    @ApiModelProperty(value = "")
	private String ywstatus ; 

	/**  **/
	@Column(name="Ywcore")
    @ApiModelProperty(value = "")
	private String ywcore ; 

	/**  **/
	@Column(name="Ywscore")
    @ApiModelProperty(value = "")
	private String ywscore ; 

	/**  **/
	@Column(name="YwORGstrucID")
    @ApiModelProperty(value = "")
	private String yworgstrucid ; 

	/**  **/
	@Column(name="YwORGprovince")
    @ApiModelProperty(value = "")
	private String yworgprovince ; 

	/**  **/
	@Column(name="YwORGcity")
    @ApiModelProperty(value = "")
	private String yworgcity ; 

	/**  **/
	@Column(name="YwfendORGID")
    @ApiModelProperty(value = "")
	private String ywfendorgid ; 

	/**  **/
	@Column(name="YwfORGstrucID")
    @ApiModelProperty(value = "")
	private String ywforgstrucid ; 

	/**  **/
	@Column(name="YwfendORGprovince")
    @ApiModelProperty(value = "")
	private String ywfendorgprovince ; 

	/**  **/
	@Column(name="YwfORGprovince")
    @ApiModelProperty(value = "")
	private String ywforgprovince ; 

	/**  **/
	@Column(name="YwfendORGcity")
    @ApiModelProperty(value = "")
	private String ywfendorgcity ; 

	/**  **/
	@Column(name="YwfORGcity")
    @ApiModelProperty(value = "")
	private String ywforgcity ; 

	/**  **/
	@Column(name="YwfendORGtype")
    @ApiModelProperty(value = "")
	private String ywfendorgtype ; 

	/**  **/
	@Column(name="YwfORGtype")
    @ApiModelProperty(value = "")
	private String ywforgtype ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 


}
