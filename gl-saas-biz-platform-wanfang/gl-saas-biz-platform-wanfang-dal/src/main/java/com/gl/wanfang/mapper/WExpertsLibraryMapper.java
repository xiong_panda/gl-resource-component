package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WExpertsLibrary;
import com.gl.wanfang.outdto.EasyWExpertsLibraryOutDto;
import com.gl.wanfang.outdto.WExpertsLibraryOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WExpertsLibraryMapper extends MyMapper<WExpertsLibrary> {

    List<WExpertsLibrary> getList(Map<String, Object> params);

    /**
     * 专家模糊检索、分页
     *
     * @param params
     * @return
     */
    List<WExpertsLibrary> getListByPage(Map<String, Object> params);


    /**
     * 获取简略信息  专家 列表
     */
    List<EasyWExpertsLibraryOutDto> getExpertInfoByDB2(Map<String, Object> params);

    /**
     * 获取详情信息
     *
     * @param params
     * @return
     */
    WExpertsLibraryOutDto getOneByParams(Map<String, Object> params);
}
