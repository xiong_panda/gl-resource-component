package com.gl.wanfang.service;

import com.gl.wanfang.indto.WForeignLanguageOaPaperInDto;
import com.gl.wanfang.model.WForeignLanguageOaPaper;
import com.gl.wanfang.outdto.EasyWForeignLanguageOaPaperOutDto;
import com.gl.wanfang.outdto.WForeignLanguageOaPaperOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWForeignLanguageOaPaperDalService {

    /**
     * 外文oa模糊匹配
     *
     * @param params
     * @return
     */
    List<WForeignLanguageOaPaper> getWForeignLanguageOaPaperByDB(Map<String, Object> params);

    /**
     * 获取外文oa论文 列表
     *
     * @param t
     * @return
     */
    List<EasyWForeignLanguageOaPaperOutDto> getWForeignLanguageOaPaperByDB2(Map<String, Object> params);

    /**
     * 获取外文OA论文详情
     *
     * @param params
     * @return
     */
    WForeignLanguageOaPaperOutDto getOneByParams(Map<String, Object> params);
}
