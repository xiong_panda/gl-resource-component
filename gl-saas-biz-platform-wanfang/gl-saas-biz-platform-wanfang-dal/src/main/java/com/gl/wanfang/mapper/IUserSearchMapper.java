package com.gl.wanfang.mapper;

import com.gl.common.mybatis.annotation.MapperPrimary;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@MapperPrimary
@Component
public interface IUserSearchMapper {
   void saveUserSearch(@Param("list") List<Map> list);
}
