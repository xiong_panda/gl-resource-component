package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.HxWfServerSearchMapper;
import com.gl.wanfang.model.HxWfServerSearch;
import com.gl.wanfang.service.IUserClickDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserClickImplDalService implements IUserClickDalService {

    @Autowired
    private HxWfServerSearchMapper hxWfServerSearchMapper;

    @Override
    public void saveUserClick(List<HxWfServerSearch> list) {
        hxWfServerSearchMapper.saveUserWFUserRecord(list);
    }

    @Override
    public List<HxWfServerSearch> getListByCollectTime(Map<String, Object> params) {
        return hxWfServerSearchMapper.getListByCollectTime(params);
    }
}
