package com.gl.wanfang.service;


import com.gl.wanfang.model.MixZswx;
import com.gl.wanfang.model.WKnowledgeLiterature;
import com.gl.wanfang.outdto.EasyMixZswxOutDto;
import com.gl.wanfang.outdto.MixZswxOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface IWKnowledgeLiteratureDalService {


	/**
	 * 知识文件检索
	 * @param params
	 * @return
	 */
	List<MixZswx> getListByPage(Map<String, Object> params);

	/**
	 * @author QinYe
	 * @date 2020/6/22 9:56
	 * @description 知识文献简略搜索
	*/
	List<EasyMixZswxOutDto> getListByPage2(Map<String, Object> params);

	/**
	 * @author QinYe
	 * @date 2020/6/22 11:12
	 * @description 知识文献详情
	 */
	MixZswxOutDto getOneByParams(Map<String, Object> params);

}
