package com.gl.wanfang.mapper;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.DateGroupByEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@MapperPrimary
@Component
public interface IDateGroupMapper {


    /**
     * @param groupData:聚合字段
     * @param tableName；表名
     * @return
     */
    List<DateGroupByEntity> getDataGroup(@Param("groupData") String groupData, @Param("tableName") String tableName);

    /**
     * 未调用
     * 知识文献聚合查询
     */
    List<DateGroupByEntity> getMixZSWXDataGroupByDataFlag();


}
