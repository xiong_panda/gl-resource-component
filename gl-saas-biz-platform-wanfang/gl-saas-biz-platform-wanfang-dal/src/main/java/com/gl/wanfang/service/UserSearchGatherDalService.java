package com.gl.wanfang.service;

import com.gl.wanfang.model.BzHotAndRecommend;
import com.gl.wanfang.outdto.GatherOutDTO;

import java.util.List;

/**
 * @auther Qinye
 * @date 2020/5/11 14:12
 * @description
 */
public interface UserSearchGatherDalService {
    List<GatherOutDTO> getHotAndRecommend(BzHotAndRecommend bzHotAndRecommend);
}
