package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WAuthorLibraryMapper;
import com.gl.wanfang.model.WAuthorLibrary;
import com.gl.wanfang.outdto.EasyWAuthorLibraryOutDto;
import com.gl.wanfang.outdto.WAuthorLibraryOutDto;
import com.gl.wanfang.service.IWAuthorLibraryDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WAuthorLibraryImplDalService implements IWAuthorLibraryDalService {

    @Autowired
    WAuthorLibraryMapper wAuthorLibraryMapper;

    @Override
    public List<WAuthorLibrary> getListByPage(Map<String, Object> params) {
        List<WAuthorLibrary> listByPage = wAuthorLibraryMapper.getListByPage(params);
        return listByPage;
    }


    @Override
    public List<EasyWAuthorLibraryOutDto> getListByPage2(Map<String, Object> params) {
        return wAuthorLibraryMapper.getListByPage2(params);
    }

    @Override
    public WAuthorLibraryOutDto getOne(Map<String, Object> params) {
        return wAuthorLibraryMapper.getOneByParams(params);
    }
}
