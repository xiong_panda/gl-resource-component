package com.gl.wanfang.service;

import com.gl.wanfang.model.WOverseasPatent;
import com.gl.wanfang.outdto.EasyWOverseasPatentOutDto;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWOverseasPatentDalService {

    /**
     * 海外专利 模糊匹配
     *
     * @return
     */
    List<WOverseasPatent> getListByPage(Map<String, Object> params);

    /**
     * 获取海外专利列表
     *
     * @param params
     * @return
     */
    List<EasyWOverseasPatentOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 获取海外专利详情
     *
     * @param params
     * @return
     */
    WOverseasPatentOutDto getOneByParams(Map<String, Object> params);
}
