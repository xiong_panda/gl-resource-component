package com.gl.wanfang.mapper;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WChinesePaperOa;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WChinesePaperOaMapper extends MyMapper<WChinesePaperOa> {

	List<WChinesePaperOa> getList(Map<String, Object> params);
	/*
	 * 模糊查询
	 * */

	List<WChinesePaperOa> getBypage(Map<String, Object> params);

    List<EasyWChinesePaperOaOutDto> getListByPage2(Map<String, Object> params);

	WChinesePaperOaOutDto getOneByParams(Map<String, Object> params);
}
