package com.gl.wanfang.service;

import com.gl.wanfang.model.WTechnologyAchievements;

import java.util.List;
import java.util.Map;

public interface IWTechnologyAchievementsDalService {
    /**
     * 分页匹配模糊查询
     * @param params
     * @return
     */
    List<WTechnologyAchievements> getListByPage(Map<String, Object> params);

}
