package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.HxWfServerSearchMapper;
import com.gl.wanfang.mapper.WForeignLanguageOaPaperMapper;
import com.gl.wanfang.model.HxWfServerSearch;
import com.gl.wanfang.model.HxWfServerSearchOriginal;
import com.gl.wanfang.model.WForeignLanguageOaPaper;
import com.gl.wanfang.outdto.HxWfServerSearchOutDto;
import com.gl.wanfang.service.IWFDataHanDleDalService;
import com.gl.wanfang.service.IWForeignLanguageOaPaperDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WFDataHanDleDalServiceImpl implements IWFDataHanDleDalService {

    @Autowired
    HxWfServerSearchMapper hxWfServerSearchMapper;

    @Override
    public List<HxWfServerSearchOriginal> getListByParam(Map<String, Object> params) {
        return hxWfServerSearchMapper.getOriginalListByParame(params);
    }

    @Override
    public void saveUserWFUseRecord(Map<String, Object> params) {
        hxWfServerSearchMapper.saveUserWFUseRecord(params);
    }
}
