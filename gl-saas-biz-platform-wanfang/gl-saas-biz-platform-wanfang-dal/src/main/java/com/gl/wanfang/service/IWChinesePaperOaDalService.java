package com.gl.wanfang.service;


import com.gl.wanfang.model.WChinesePaperOa;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWChinesePaperOaDalService {

    List<WChinesePaperOa> getListByPage(Map<String, Object> params);


    /**
     * 中文oA论文简略信息
     *
     * @param params
     * @return
     */
    List<EasyWChinesePaperOaOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 中文论文详情信息
     *
     * @param params
     * @return
     */
    WChinesePaperOaOutDto getOneByParams(Map<String, Object> params);
}
