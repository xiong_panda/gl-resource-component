package com.gl.wanfang.mapper;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.WAuthorLibrary;
import com.gl.wanfang.outdto.EasyWAuthorLibraryOutDto;
import com.gl.wanfang.outdto.WAuthorLibraryOutDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WAuthorLibraryMapper extends MyMapper<WAuthorLibrary> {

    /**
     * 列表分页查询
     *
     * @param params
     * @return
     */
    List<WAuthorLibrary> getList(Map<String, Object> params);

    /**
     * 列表分页模糊查询
     *
     * @param params
     * @return
     */
    List<WAuthorLibrary> getListByPage(Map<String, Object> params);

    /**
     * 获取作者列表 简略信息
     *
     * @param params
     * @return
     */
    List<EasyWAuthorLibraryOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 获取详细信息
     *
     * @param params
     * @return
     */
    WAuthorLibraryOutDto getOneByParams(Map<String, Object> params);
}
