package com.gl.wanfang.service;

import com.gl.wanfang.model.WAuthorLibrary;
import com.gl.wanfang.outdto.EasyWAuthorLibraryOutDto;
import com.gl.wanfang.outdto.WAuthorLibraryOutDto;

import java.util.List;
import java.util.Map;

public interface IWAuthorLibraryDalService {

    /**
     * 列表分页模糊查询
     *
     * @param params
     * @return
     */
    List<WAuthorLibrary> getListByPage(Map<String, Object> params);

    /**
     * 获取作者简略信息
     *
     * @param params
     * @return
     */
    List<EasyWAuthorLibraryOutDto> getListByPage2(Map<String, Object> params);

    WAuthorLibraryOutDto getOne(Map<String, Object> params);
}
