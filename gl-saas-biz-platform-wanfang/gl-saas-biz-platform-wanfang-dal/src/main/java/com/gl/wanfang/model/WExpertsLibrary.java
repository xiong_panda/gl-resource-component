package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 专家库
 * @author code_generator
 */
@Table(name = "W_EXPERTS_LIBRARY")
@Data
public class WExpertsLibrary implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 姓名 **/
	@Column(name="Name")
    @ApiModelProperty(value = "姓名")
	private String name ; 

	/** 性别 **/
	@Column(name="Sex")
    @ApiModelProperty(value = "性别")
	private String sex ; 

	/**  **/
	@Column(name="Age")
    @ApiModelProperty(value = "")
	private String age ; 

	/** 出生日期 **/
	@Column(name="Birth_Date")
    @ApiModelProperty(value = "出生日期")
	private String birthDate ; 

	/** 出生地点 **/
	@Column(name="Birth_Address")
    @ApiModelProperty(value = "出生地点")
	private String birthAddress ; 

	/** 民族 **/
	@Column(name="National")
    @ApiModelProperty(value = "民族")
	private String national ; 

	/** 工作单位:其它工作单位 **/
	@Column(name="Other_Unit")
    @ApiModelProperty(value = "工作单位:其它工作单位")
	private String otherUnit ; 

	/** 其它工作单位 **/
	@Column(name="Other_Unit2")
    @ApiModelProperty(value = "其它工作单位")
	private String otherUnit2 ; 

	/** 工作单位 **/
	@Column(name="Unit")
    @ApiModelProperty(value = "工作单位")
	private String unit ; 

	/** 其它工作单位 **/
	@Column(name="Other_Unit3")
    @ApiModelProperty(value = "其它工作单位")
	private String otherUnit3 ; 

	/** 教育背景 **/
	@Column(name="Education_Background")
    @ApiModelProperty(value = "教育背景")
	private String educationBackground ; 

	/** 外语语种 **/
	@Column(name="Foreign_Languages")
    @ApiModelProperty(value = "外语语种")
	private String foreignLanguages ; 

	/** 工作简历 **/
	@Column(name="Job_Resume")
    @ApiModelProperty(value = "工作简历")
	private String jobResume ; 

	/** 国内外学术或专业团体任职情况 **/
	@Column(name="Co_Employment")
    @ApiModelProperty(value = "国内外学术或专业团体任职情况")
	private String coEmployment ; 

	/** 工作职务 **/
	@Column(name="Job_Position")
    @ApiModelProperty(value = "工作职务")
	private String jobPosition ; 

	/** 专家荣誉 **/
	@Column(name="Experts_Honor")
    @ApiModelProperty(value = "专家荣誉")
	private String expertsHonor ; 

	/** 技术职称 **/
	@Column(name="Technical_Titles")
    @ApiModelProperty(value = "技术职称")
	private String technicalTitles ; 

	/** 通讯地址 **/
	@Column(name="Address")
    @ApiModelProperty(value = "通讯地址")
	private String address ; 

	/** 邮码 **/
	@Column(name="Postal_Code")
    @ApiModelProperty(value = "邮码")
	private String postalCode ; 

	/** 区位 **/
	@Column(name="Zone_Bit")
    @ApiModelProperty(value = "区位")
	private String zoneBit ; 

	/** 省 **/
	@Column(name="Province")
    @ApiModelProperty(value = "省")
	private String province ; 

	/** 市 **/
	@Column(name="City")
    @ApiModelProperty(value = "市")
	private String city ; 

	/** 县 **/
	@Column(name="County")
    @ApiModelProperty(value = "县")
	private String county ; 

	/** 行政码 **/
	@Column(name="Administrative_Code")
    @ApiModelProperty(value = "行政码")
	private String administrativeCode ; 

	/** 电话 **/
	@Column(name="Telphone")
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 传真 **/
	@Column(name="Fax")
    @ApiModelProperty(value = "传真")
	private String fax ; 

	/** 电子信箱 **/
	@Column(name="Email")
    @ApiModelProperty(value = "电子信箱")
	private String email ; 

	/** 专业领域与研究方向 **/
	@Column(name="Professional_Research")
    @ApiModelProperty(value = "专业领域与研究方向")
	private String professionalResearch ; 

	/** 专业领域与研究方向 **/
	@Column(name="Professional_Research2")
    @ApiModelProperty(value = "专业领域与研究方向")
	private String professionalResearch2 ; 

	/** 院士 **/
	@Column(name="Academician")
    @ApiModelProperty(value = "院士")
	private String academician ; 

	/** 院士 **/
	@Column(name="Academician2")
    @ApiModelProperty(value = "院士")
	private String academician2 ; 

	/**  **/
	@Column(name="Awards")
    @ApiModelProperty(value = "")
	private String awards ; 

	/**  **/
	@Column(name="Awardt")
    @ApiModelProperty(value = "")
	private String awardt ; 

	/**  **/
	@Column(name="Foreign")
    @ApiModelProperty(value = "")
	private String foreign ; 

	/**  **/
	@Column(name="Exptype")
    @ApiModelProperty(value = "")
	private String exptype ; 

	/**  **/
	@Column(name="Byind")
    @ApiModelProperty(value = "")
	private String byind ; 

	/** 学科分类名 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类名")
	private String disciplineClass ; 

	/** 学科分类名 **/
	@Column(name="Discipline_Class2")
    @ApiModelProperty(value = "学科分类名")
	private String disciplineClass2 ; 

	/** 学科分类码 **/
	@Column(name="Discipline_Class_Code")
    @ApiModelProperty(value = "学科分类码")
	private String disciplineClassCode ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/**  **/
	@Column(name="Cls2")
    @ApiModelProperty(value = "")
	private String cls2 ; 


}
