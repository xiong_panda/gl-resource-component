package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 法律法规
 * @author code_generator
 */
@Table(name = "W_LAWS_REGULATIONS")
@Data
public class WLawsRegulations implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号  **/
	@Id
    @ApiModelProperty(value = "唯一编号 ")
	private String id ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 标题:英文标题 **/
	@Column(name="Title2")
    @ApiModelProperty(value = "标题:英文标题")
	private String title2 ; 

	/** 法规正文 **/
	@Column(name="Rules_Text")
    @ApiModelProperty(value = "法规正文")
	private String rulesText ; 

	/** 关键词 **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment ; 

	/** 颁布部门(全文检索) **/
	@Column(name="Proclaim_Department_FullText")
    @ApiModelProperty(value = "颁布部门(全文检索)")
	private String proclaimDepartmentFulltext ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department2")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment2 ; 

	/**  **/
	@Column(name="AUTHOR")
    @ApiModelProperty(value = "")
	private String author ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 颁布日期 **/
	@Column(name="Proclaim_Date")
    @ApiModelProperty(value = "颁布日期")
	private String proclaimDate ; 

	/** 终审日期 **/
	@Column(name="Final_Date")
    @ApiModelProperty(value = "终审日期")
	private String finalDate ; 

	/**  **/
	@Column(name="RID")
    @ApiModelProperty(value = "")
	private String rid ; 

	/** 标题 **/
	@Column(name="Title")
    @ApiModelProperty(value = "标题")
	private String title ; 

	/** 英文标题 **/
	@Column(name="English_Title")
    @ApiModelProperty(value = "英文标题")
	private String englishTitle ; 

	/** 发文文号 **/
	@Column(name="Post_Document_Code")
    @ApiModelProperty(value = "发文文号")
	private String postDocumentCode ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department3")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment3 ; 

	/** 行业地区码_部门代码 **/
	@Column(name="IndustryCode_Department_Code")
    @ApiModelProperty(value = "行业地区码_部门代码")
	private String industrycodeDepartmentCode ; 

	/** 效力级别 **/
	@Column(name="Value_Level")
    @ApiModelProperty(value = "效力级别")
	private String valueLevel ; 

	/** 效力代码 **/
	@Column(name="Value_Code")
    @ApiModelProperty(value = "效力代码")
	private String valueCode ; 

	/** 时效性 **/
	@Column(name="Timeliness")
    @ApiModelProperty(value = "时效性")
	private String timeliness ; 

	/** 批准日期 **/
	@Column(name="Approval_Date")
    @ApiModelProperty(value = "批准日期")
	private String approvalDate ; 

	/** 签字日期 **/
	@Column(name="Signature_Date")
    @ApiModelProperty(value = "签字日期")
	private String signatureDate ; 

	/**  **/
	@Column(name="YAPPRDATE")
    @ApiModelProperty(value = "")
	private String yapprdate ; 

	/**  **/
	@Column(name="YSIGNDATE")
    @ApiModelProperty(value = "")
	private String ysigndate ; 

	/** 实施日期 **/
	@Column(name="IMPLEMENTATION_Date")
    @ApiModelProperty(value = "实施日期")
	private String implementationDate ; 

	/** 失效日期 **/
	@Column(name="Invalid_Date")
    @ApiModelProperty(value = "失效日期")
	private String invalidDate ; 

	/** 终审法院 **/
	@Column(name="Final_Court")
    @ApiModelProperty(value = "终审法院")
	private String finalCourt ; 

	/** 终审日期 **/
	@Column(name="Final_Date2")
    @ApiModelProperty(value = "终审日期")
	private String finalDate2 ; 

	/** 调解日期 **/
	@Column(name="mediate_Date")
    @ApiModelProperty(value = "调解日期")
	private String mediateDate ; 

	/** 内容分类 **/
	@Column(name="Cintent_Class")
    @ApiModelProperty(value = "内容分类")
	private String cintentClass ; 

	/** 内容分类码 **/
	@Column(name="Cintent_Class_Code")
    @ApiModelProperty(value = "内容分类码")
	private String cintentClassCode ; 

	/** URL **/
	@Column(name="URL")
    @ApiModelProperty(value = "URL")
	private String url ; 

	/** PDF全文 **/
	@Column(name="PDF_FullText")
    @ApiModelProperty(value = "PDF全文")
	private String pdfFulltext ; 

	/** 相关链接 **/
	@Column(name="Relevant_link")
    @ApiModelProperty(value = "相关链接")
	private String relevantLink ; 

	/** 历史链接 **/
	@Column(name="History_link")
    @ApiModelProperty(value = "历史链接")
	private String historyLink ; 

	/** 库别代码 **/
	@Column(name="Library_Code")
    @ApiModelProperty(value = "库别代码")
	private String libraryCode ; 

	/** 制作日期 **/
	@Column(name="Make_Date")
    @ApiModelProperty(value = "制作日期")
	private String makeDate ; 

	/** 行业分类 **/
	@Column(name="Industry_Class")
    @ApiModelProperty(value = "行业分类")
	private String industryClass ; 

	/** 行业分类码 **/
	@Column(name="Industry_Class_Code")
    @ApiModelProperty(value = "行业分类码")
	private String industryClassCode ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 


}
