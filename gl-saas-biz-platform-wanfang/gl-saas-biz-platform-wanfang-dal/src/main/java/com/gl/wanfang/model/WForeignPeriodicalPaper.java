package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 外文期刊论文
 * @author code_generator
 */
@Table(name = "W_FOREIGN_PERIODICAL_PAPER")
@Data
public class WForeignPeriodicalPaper implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 唯一编号 **/
	@Id
	@GeneratedValue(generator = "JDBC")
    @ApiModelProperty(value = "唯一编号")
	private String id ;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Full_Name")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String fullName ; 

	/** Title_Title **/
	@Column(name="Title")
    @ApiModelProperty(value = "Title_Title")
	private String title ; 

	/** 摘要 **/
	@Column(name="Abstract")
    @ApiModelProperty(value = "摘要")
	private String Abstract ;

	/** 作者 **/
	@Column(name="Author")
    @ApiModelProperty(value = "作者")
	private String author ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** Discipline_Keywords **/
	@Column(name="Keyword")
    @ApiModelProperty(value = "Discipline_Keywords")
	private String keyword ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** Creator_ORG **/
	@Column(name="Creator_ORG")
    @ApiModelProperty(value = "Creator_ORG")
	private String creatorOrg ; 

	/** Date_Issued **/
	@Column(name="Date")
    @ApiModelProperty(value = "Date_Issued")
	private Date date ; 

	/** Date_Issued **/
	@Column(name="Year")
    @ApiModelProperty(value = "Date_Issued")
	private Date year ; 

	/**  **/
	@Column(name="IID")
    @ApiModelProperty(value = "")
	private String iid ; 

	/** IDentifier_DOI **/
	@Column(name="Doi")
    @ApiModelProperty(value = "IDentifier_DOI")
	private String doi ; 

	/** IDentifier_ISSNp **/
	@Column(name="Issn")
    @ApiModelProperty(value = "IDentifier_ISSNp")
	private String issn ; 

	/** IDentifier_ISSNe **/
	@Column(name="Eissn")
    @ApiModelProperty(value = "IDentifier_ISSNe")
	private String eissn ; 

	/** Creator_Creator **/
	@Column(name="Author2")
    @ApiModelProperty(value = "Creator_Creator")
	private String author2 ; 

	/** Source_Source **/
	@Column(name="Joucn")
    @ApiModelProperty(value = "Source_Source")
	private String joucn ; 

	/** Source_Source **/
	@Column(name="Fjoucn")
    @ApiModelProperty(value = "Source_Source")
	private String fjoucn ; 

	/** Source_Vol **/
	@Column(name="Vol")
    @ApiModelProperty(value = "Source_Vol")
	private String vol ; 

	/** Source_Issue **/
	@Column(name="Per")
    @ApiModelProperty(value = "Source_Issue")
	private String per ; 

	/** Source_Page **/
	@Column(name="Pg")
    @ApiModelProperty(value = "Source_Page")
	private String pg ; 

	/** Creator_ORG **/
	@Column(name="Creator_ORG2")
    @ApiModelProperty(value = "Creator_ORG")
	private String creatorOrg2 ; 

	/** Date_Download **/
	@Column(name="Download_Date")
    @ApiModelProperty(value = "Date_Download")
	private Date downloadDate ; 

	/** Publisher_Publisher **/
	@Column(name="Pubtype")
    @ApiModelProperty(value = "Publisher_Publisher")
	private String pubtype ; 

	/** Yn_Free **/
	@Column(name="Ynfree")
    @ApiModelProperty(value = "Yn_Free")
	private String ynfree ; 

	/** Discipline_SelfFL **/
	@Column(name="CID")
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String cid ; 

	/** Discipline_SelfFL **/
	@Column(name="ZcID")
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String zcid ; 

	/** Discipline_SelfFL **/
	@Column(name="TzcID")
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String tzcid ; 

	/** Discipline_SelfFL **/
	@Column(name="SzcID")
    @ApiModelProperty(value = "Discipline_SelfFL")
	private String szcid ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/**  **/
	@Column(name="Score")
    @ApiModelProperty(value = "")
	private String score ; 

	/**  **/
	@Column(name="Rn")
    @ApiModelProperty(value = "")
	private String rn ; 

	/**  **/
	@Column(name="Bn")
    @ApiModelProperty(value = "")
	private String bn ; 

	/**  **/
	@Column(name="Dn")
    @ApiModelProperty(value = "")
	private String dn ; 

	/**  **/
	@Column(name="Ots")
    @ApiModelProperty(value = "")
	private String ots ; 

	/** Core_SCI **/
	@Column(name="Sci")
    @ApiModelProperty(value = "Core_SCI")
	private String sci ; 

	/** Core_EI **/
	@Column(name="Ei")
    @ApiModelProperty(value = "Core_EI")
	private String ei ; 

	/** 基金 **/
	@Column(name="Fund")
    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/
	@Column(name="Fund_FullText_Search")
    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/** Fund_info **/
	@Column(name="Fund_Anyname2")
    @ApiModelProperty(value = "Fund_info")
	private String fundAnyname2 ; 

	/** Fund_info **/
	@Column(name="F_Fund")
    @ApiModelProperty(value = "Fund_info")
	private String fFund ; 

	/** Core_EI:Core_Sci **/
	@Column(name="Core")
    @ApiModelProperty(value = "Core_EI:Core_Sci")
	private String core ; 

	/**  **/
	@Column(name="Data_Link")
    @ApiModelProperty(value = "")
	private String dataLink ; 

	/** Fund_info **/
	@Column(name="Fund_Support")
    @ApiModelProperty(value = "Fund_info")
	private String fundSupport ; 


}
