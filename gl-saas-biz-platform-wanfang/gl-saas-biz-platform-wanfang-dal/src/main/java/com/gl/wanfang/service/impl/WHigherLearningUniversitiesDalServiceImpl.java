package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WHigherLearningUniversitiesMapper;
import com.gl.wanfang.model.WHigherLearningUniversities;
import com.gl.wanfang.outdto.EasyWHigherLearningUniversitiesOutDto;
import com.gl.wanfang.outdto.WHigherLearningUniversitiesOutDto;
import com.gl.wanfang.service.IWHigherLearningUniversitiesDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WHigherLearningUniversitiesDalServiceImpl implements IWHigherLearningUniversitiesDalService {

    @Autowired
    WHigherLearningUniversitiesMapper wHigherLearningUniversitiesMapper;

    /**
     * 高等院校模糊匹配
     *
     * @return
     */
    @Override
    public List<WHigherLearningUniversities> getWHigherLearningUniversitiesByDB(Map<String, Object> params) {
        List<WHigherLearningUniversities> listByPage = wHigherLearningUniversitiesMapper.getListByPage(params);
        return listByPage;
    }

    @Override
    public WHigherLearningUniversities selectOneByName(Map<String, Object> params) {
        return wHigherLearningUniversitiesMapper.getOneByParams(params);
    }

    @Override
    public List<EasyWHigherLearningUniversitiesOutDto> getWHigherLearningUniversitiesByDB2(Map<String, Object> params) {
        return wHigherLearningUniversitiesMapper.getWHigherLearningUniversitiesByDB2(params);
    }

    public WHigherLearningUniversitiesOutDto getDetailByParams(Map<String, Object> params) {
        return wHigherLearningUniversitiesMapper.getDetailByParams(params);
    }
}
