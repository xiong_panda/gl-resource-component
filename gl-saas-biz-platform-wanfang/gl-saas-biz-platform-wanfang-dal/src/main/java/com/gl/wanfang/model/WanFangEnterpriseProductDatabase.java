package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 企业产品数据库 
 * @author code_generator
 */
@Table(name = "W_ENTERPRISE_PRODUCT_DATABASE")
@Data
public class WanFangEnterpriseProductDatabase implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 企业名称:铭牌:简称:曾用名 **/
	@Column(name="Corp_FullName")
    @ApiModelProperty(value = "企业名称:铭牌:简称:曾用名")
	private String corpFullname ; 

	/**  **/
	@Column(name="TITLE_ANYNAME")
    @ApiModelProperty(value = "")
	private String titleAnyname ; 

	/**  **/
	@Column(name="ANYNAME")
    @ApiModelProperty(value = "")
	private String anyname ; 

	/**  **/
	@Column(name="ACNAME")
    @ApiModelProperty(value = "")
	private String acname ; 

	/** 企业名称 **/
	@Column(name="Corp_Name")
    @ApiModelProperty(value = "企业名称")
	private String corpName ; 

	/** 铭牌 **/
	@Column(name="Corp_Nameplate")
    @ApiModelProperty(value = "铭牌")
	private String corpNameplate ; 

	/** 简称 **/
	@Column(name="Corp_SHORTNAME")
    @ApiModelProperty(value = "简称")
	private String corpShortname ; 

	/** 曾用名 **/
	@Column(name="Corp_OLDNAME")
    @ApiModelProperty(value = "曾用名")
	private String corpOldname ; 

	/** 负责人姓名 **/
	@Column(name="Corp_PERSON")
    @ApiModelProperty(value = "负责人姓名")
	private String corpPerson ; 

	/** 负责人 **/
	@Column(name="Corp_MULTIPERSON")
    @ApiModelProperty(value = "负责人")
	private String corpMultiperson ; 

	/** 省名 **/
	@Column(name="Corp_PROV")
    @ApiModelProperty(value = "省名")
	private String corpProv ; 

	/** 市名 **/
	@Column(name="Corp_CITY")
    @ApiModelProperty(value = "市名")
	private String corpCity ; 

	/** 县名 **/
	@Column(name="Corp_COUN")
    @ApiModelProperty(value = "县名")
	private String corpCoun ; 

	/** 行政区代码 **/
	@Column(name="Corp_REGION")
    @ApiModelProperty(value = "行政区代码")
	private String corpRegion ; 

	/** 地址 **/
	@Column(name="Corp_ADDR")
    @ApiModelProperty(value = "地址")
	private String corpAddr ; 

	/** 电话 **/
	@Column(name="Telphone")
    @ApiModelProperty(value = "电话")
	private String telphone ; 

	/** 区位号 **/
	@Column(name="Corp_RECODE")
    @ApiModelProperty(value = "区位号")
	private String corpRecode ; 

	/** 传真 **/
	@Column(name="Corp_FAX")
    @ApiModelProperty(value = "传真")
	private String corpFax ; 

	/** 邮码 **/
	@Column(name="Postal_Code")
    @ApiModelProperty(value = "邮码")
	private String postalCode ; 

	/** 电子邮件 **/
	@Column(name="Corp_EMAIL")
    @ApiModelProperty(value = "电子邮件")
	private String corpEmail ; 

	/** 网址 **/
	@Column(name="Corp_URL")
    @ApiModelProperty(value = "网址")
	private String corpUrl ; 

	/** 成立年代 **/
	@Column(name="Corp_FOUND")
    @ApiModelProperty(value = "成立年代")
	private String corpFound ; 

	/** 注册资金 **/
	@Column(name="Corp_Money")
    @ApiModelProperty(value = "注册资金")
	private String corpMoney ; 

	/** 固定资产 **/
	@Column(name="Corp_ASSET")
    @ApiModelProperty(value = "固定资产")
	private String corpAsset ; 

	/** 职工人数 **/
	@Column(name="Corp_EMPS")
    @ApiModelProperty(value = "职工人数")
	private String corpEmps ; 

	/** 技术人员数 **/
	@Column(name="Corp_TECHS")
    @ApiModelProperty(value = "技术人员数")
	private String corpTechs ; 

	/** 营业额 **/
	@Column(name="Corp_TURNOVER")
    @ApiModelProperty(value = "营业额")
	private String corpTurnover ; 

	/** 利税 **/
	@Column(name="Corp_TAX")
    @ApiModelProperty(value = "利税")
	private String corpTax ; 

	/** 创汇额 **/
	@Column(name="Corp_EXCHANGE")
    @ApiModelProperty(value = "创汇额")
	private String corpExchange ; 

	/** 进出口权 **/
	@Column(name="Corp_IMPEXP")
    @ApiModelProperty(value = "进出口权")
	private String corpImpexp ; 

	/** 性质与级别 **/
	@Column(name="Corp_LEVEL")
    @ApiModelProperty(value = "性质与级别")
	private String corpLevel ; 

	/** 股票代码 **/
	@Column(name="Corp_STOCK")
    @ApiModelProperty(value = "股票代码")
	private String corpStock ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/** 企业简介 **/
	@Column(name="Corp_INTRO")
    @ApiModelProperty(value = "企业简介")
	private String corpIntro ; 

	/** 企业占地面积 **/
	@Column(name="Corp_SPACE")
    @ApiModelProperty(value = "企业占地面积")
	private String corpSpace ; 

	/** 厂房办公面积 **/
	@Column(name="Corp_AREA")
    @ApiModelProperty(value = "厂房办公面积")
	private String corpArea ; 

	/** 主管单位 **/
	@Column(name="Corp_COMPUNIT")
    @ApiModelProperty(value = "主管单位")
	private String corpCompunit ; 

	/** 派出机构 **/
	@Column(name="Corp")
    @ApiModelProperty(value = "派出机构")
	private String corp ; 

	/** 行业GBM **/
	@Column(name="IGBM")
    @ApiModelProperty(value = "行业GBM")
	private String igbm ; 

	/**  **/
	@Column(name="IGBM_AnyName")
    @ApiModelProperty(value = "")
	private String igbmAnyname ; 

	/**  **/
	@Column(name="IGBM_AnyName2")
    @ApiModelProperty(value = "")
	private String igbmAnyname2 ; 

	/** 行业GBM **/
	@Column(name="FIGBM")
    @ApiModelProperty(value = "行业GBM")
	private String figbm ; 

	/**  **/
	@Column(name="FIGBM_AnyName")
    @ApiModelProperty(value = "")
	private String figbmAnyname ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 机构全文检索 **/
	@Column(name="ORG_FullText_Search")
    @ApiModelProperty(value = "机构全文检索")
	private String orgFulltextSearch ; 

	/** 行业SIC **/
	@Column(name="ISIC")
    @ApiModelProperty(value = "行业SIC")
	private String isic ; 

	/** 商标 **/
	@Column(name="Brand")
    @ApiModelProperty(value = "商标")
	private String brand ; 

	/** 产品信息 **/
	@Column(name="Product_Info")
    @ApiModelProperty(value = "产品信息")
	private String productInfo ; 

	/** 经营项目 **/
	@Column(name="Business_Project")
    @ApiModelProperty(value = "经营项目")
	private String businessProject ; 

	/** 经营项目英 **/
	@Column(name="Business_Project_en")
    @ApiModelProperty(value = "经营项目英")
	private String businessProjectEn ; 

	/** 产品关键词:英文产品关键词 **/
	@Column(name="FullKeyWord")
    @ApiModelProperty(value = "产品关键词:英文产品关键词")
	private String fullkeyword ; 

	/** 产品关键词 **/
	@Column(name="Product_KeyWord")
    @ApiModelProperty(value = "产品关键词")
	private String productKeyword ; 

	/** 英文产品关键词 **/
	@Column(name="English_Product_KeyWord")
    @ApiModelProperty(value = "英文产品关键词")
	private String englishProductKeyword ; 

	/** 产品SIC **/
	@Column(name="PSIC")
    @ApiModelProperty(value = "产品SIC")
	private String psic ; 

	/** 产品GBM **/
	@Column(name="PGBM")
    @ApiModelProperty(value = "产品GBM")
	private String pgbm ; 

	/** 企业排名 **/
	@Column(name="Corp_Ranking")
    @ApiModelProperty(value = "企业排名")
	private String corpRanking ; 

	/** 重点行业 **/
	@Column(name="Corp_KEYENT")
    @ApiModelProperty(value = "重点行业")
	private String corpKeyent ; 

	/** 成品数据 **/
	@Column(name="Corp_CPDATA")
    @ApiModelProperty(value = "成品数据")
	private String corpCpdata ; 

	/** 电力数据 **/
	@Column(name="Corp_DLDATA")
    @ApiModelProperty(value = "电力数据")
	private String corpDldata ; 

	/** 冶金数据 **/
	@Column(name="Corp_YJDATA")
    @ApiModelProperty(value = "冶金数据")
	private String corpYjdata ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** --企业排名 **/
	@Column(name="Crankname")
    @ApiModelProperty(value = "--企业排名")
	private String crankname ; 

	/**  **/
	@Column(name="Crankname1")
    @ApiModelProperty(value = "")
	private String crankname1 ; 

	/**  **/
	@Column(name="Crankname2")
    @ApiModelProperty(value = "")
	private String crankname2 ; 

	/** --企业排名 **/
	@Column(name="Cranksource")
    @ApiModelProperty(value = "--企业排名")
	private String cranksource ; 

	/**  **/
	@Column(name="Cranksource1")
    @ApiModelProperty(value = "")
	private String cranksource1 ; 

	/**  **/
	@Column(name="Cranksource2")
    @ApiModelProperty(value = "")
	private String cranksource2 ; 

	/** --企业排名 **/
	@Column(name="Cranktype")
    @ApiModelProperty(value = "--企业排名")
	private String cranktype ; 

	/**  **/
	@Column(name="Cranktype1")
    @ApiModelProperty(value = "")
	private String cranktype1 ; 

	/**  **/
	@Column(name="Cranktype2")
    @ApiModelProperty(value = "")
	private String cranktype2 ; 

	/** 是否成品数据 **/
	@Column(name="Is_Finished_Product_Data")
    @ApiModelProperty(value = "是否成品数据")
	private String isFinishedProductData ; 


}
