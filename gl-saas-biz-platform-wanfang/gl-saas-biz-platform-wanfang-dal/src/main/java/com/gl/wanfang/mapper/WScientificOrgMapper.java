package com.gl.wanfang.mapper;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WScientificOrg;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.EasyWScientificOrgOutDto;
import com.gl.wanfang.outdto.WScientificOrgOutDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WScientificOrgMapper extends MyMapper<WScientificOrg> {

    List<WScientificOrg> getList(Map<String, Object> params);

    /*
     * 模糊查询
     * */

    List<WScientificOrg> getBypage(Map<String, Object> params);


    /**
     * 检索
     *
     * @param params
     * @return
     */
    List<EasyWScientificOrgOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 机构详情
     *
     * @param params
     * @return
     */
    WScientificOrgOutDto getOneParams(Map<String, Object> params);
}
