package com.gl.wanfang.mapper;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WChineseConferenceCitations;
import com.gl.wanfang.model.WChineseConferencePaper;
import com.gl.wanfang.outdto.EasyWChineseConferencePaperOutDto;
import com.gl.wanfang.outdto.WChineseConferencePaperOutDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WChineseConferenceCitationsMapper extends MyMapper<WChineseConferencePaper> {

    /**
     * 分页匹配查询
     *
     * @param params
     * @return
     */
    List<WChineseConferencePaper> getList(Map<String, Object> params);


    /**
     * 分页匹配模糊查询
     *
     * @param params
     * @return
     */
    List<WChineseConferencePaper> getListByPage(Map<String, Object> params);

    /**
     * 获取列表详情
     *
     * @param params
     * @return
     */
    List<EasyWChineseConferencePaperOutDto> getListByPage2(Map<String, Object> params);


    WChineseConferencePaperOutDto getOneByParams(Map<String, Object> params);

}
