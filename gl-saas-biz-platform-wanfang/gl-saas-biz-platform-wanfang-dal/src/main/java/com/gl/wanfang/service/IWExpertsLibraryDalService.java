package com.gl.wanfang.service;
import com.gl.wanfang.model.WExpertsLibrary;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface IWExpertsLibraryDalService {

    //分页模糊匹配
    List<WExpertsLibrary> getListByPage(Map<String, Object> params);

}
