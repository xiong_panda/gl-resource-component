package com.gl.wanfang.model;

import lombok.Data;

/**
 * 时间聚合outDto
 */
@Data
public class DateGroupByEntity {
    /**
     * 聚合的数据
     */
    String groupData;

    /**
     * 聚合数量
     */
//    Long countNum;

    /**
     * 备用字段
     */
    String sourcesInfo;
}
