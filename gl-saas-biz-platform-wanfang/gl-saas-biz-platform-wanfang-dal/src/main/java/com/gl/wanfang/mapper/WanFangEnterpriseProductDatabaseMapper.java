package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.WanFangSupplierDatabase;
import com.gl.wanfang.outdto.EasyWanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;


import com.gl.wanfang.model.WanFangEnterpriseProductDatabase;
//import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WanFangEnterpriseProductDatabaseMapper extends MyMapper<WanFangEnterpriseProductDatabase> {


    /**
     * 精准匹配
     *
     * @param params
     * @return
     */
    List<WanFangEnterpriseProductDatabase> getList(Map<String, Object> params);

    /**
     * 查询供应商
     *
     * @param params
     * @return
     */
    List<WanFangEnterpriseProductDatabase> getListEnterpriseProductDatabase(Map<String, Object> params);


    /**
     * 根据用户名字查询供应商
     *
     * @return
     */
    List<WanFangEnterpriseProductDatabase> getEnterpriseProductDatabase(@Param("params") List params);


    /**
     * 供应商&&专利关联筛选
     *
     * @param params
     * @return
     */
    List<WanFangSupplierDatabase> getSupplierByPatent(Map<String, Object> params);


    /**
     * 获取企业简略信息
     *
     * @param params
     * @return
     */
    List<EasyWanFangEnterpriseProductDatabaseOutDto> getListEnterpriseProductDatabase2(Map<String, Object> params);

    /**
     * 获取企业详情信息
     *
     * @param params
     * @return
     */
    WanFangEnterpriseProductDatabaseOutDto getOneByParams(Map<String, Object> params);


    //   List<WanFangSupplierDatabasesssss> getSupplierByPatentsssssss(Map<String, Object> params);


}
