package com.gl.wanfang.service;

import com.gl.wanfang.model.WLawsRegulations;
import com.gl.wanfang.outdto.EasyWLawsRegulationsOutDTO;
import com.gl.wanfang.outdto.WLawsRegulationsOutDTO;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWLawsRegulationsDalService {

    //分页模糊匹配
    List<WLawsRegulations> getListByPage(Map<String, Object> params);

    /**
     * 获取简略信息列表
     *
     * @param params
     * @return
     */
    List<EasyWLawsRegulationsOutDTO> getListByPage2(Map<String, Object> params);

    /**
     * 获取详情信息接口
     *
     * @param params
     * @return
     */
    WLawsRegulationsOutDTO getOneByParams(Map<String, Object> params);
}
