package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.IUserSearchMapper;
import com.gl.wanfang.service.IUserSearchDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserSearchImplDalService implements IUserSearchDalService {

    @Autowired
    IUserSearchMapper userSearchMapper;

    /**
     * 保存用户搜索记录
     * @param list
     */
    public void saveUserSearch(List<Map> list){
        userSearchMapper.saveUserSearch(list);
    }
}
