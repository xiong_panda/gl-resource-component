package com.gl.wanfang.mapper;


import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.WChinesePatent;
import com.gl.wanfang.outdto.EasyWChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WChinesePatentMapper extends MyMapper<WChinesePatent> {

    List<WChinesePatent> getList(Map params);

    //模糊匹配数据
    List<WChinesePatent> getPatentInfoByDB(Map params);

    /**
     * 中文专利简略信息
     *
     * @param params
     * @return
     */
    List<EasyWChinesePatentOUTDTO> getPatentInfoByDB2(Map<String, Object> params);

    /**
     * 中文专利详情信息
     *
     * @param params
     * @return
     */
    WChinesePatentOUTDTO getOneByParams(Map<String, Object> params);
}
