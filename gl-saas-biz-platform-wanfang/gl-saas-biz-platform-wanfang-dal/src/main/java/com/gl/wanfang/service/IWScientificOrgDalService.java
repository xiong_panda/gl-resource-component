package com.gl.wanfang.service;

import com.gl.wanfang.model.WScientificOrg;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.EasyWScientificOrgOutDto;
import com.gl.wanfang.outdto.WScientificOrgOutDto;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWScientificOrgDalService {

    List<WScientificOrg> getListByPage(Map<String, Object> params);


    /**
     * 机构减速
     *
     * @param params
     * @return
     */
    List<EasyWScientificOrgOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 机构详情
     *
     * @param params
     * @return
     */
    WScientificOrgOutDto getOneParams(Map<String, Object> params);
}
