package com.gl.wanfang.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 科研机构
 * @author code_generator
 */
@Table(name = "W_SCIENTIFIC_ORG")
@Data
public class WScientificOrg implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 记录顺序号 **/
	@Column(name="OrderID")
	@ApiModelProperty(value = "记录顺序号")
	private String orderid ;

	/** 唯一编号 **/
	@Id
	@ApiModelProperty(value = "唯一编号")
	private String id ;

	/** 全文索引 **/
	@Column(name="FullText")
	@ApiModelProperty(value = "全文索引")
	private String fulltext ;

	/** 省 **/
	@Column(name="Province")
	@ApiModelProperty(value = "省")
	private String province ;

	/** 市 **/
	@Column(name="City")
	@ApiModelProperty(value = "市")
	private String city ;

	/** 县 **/
	@Column(name="County")
	@ApiModelProperty(value = "县")
	private String county ;

	/** 机构名称:规范名称:机构曾用名 **/
	@Column(name="ORG_Name_Norm")
	@ApiModelProperty(value = "机构名称:规范名称:机构曾用名")
	private String orgNameNorm ;

	/**  **/
	@Column(name="Name_Anyname1")
	@ApiModelProperty(value = "")
	private String nameAnyname1 ;

	/**  **/
	@Column(name="Name_Anyname2")
	@ApiModelProperty(value = "")
	private String nameAnyname2 ;

	/**  **/
	@Column(name="Acname")
	@ApiModelProperty(value = "")
	private String acname ;

	/** 机构名称 **/
	@Column(name="ORG_Name")
	@ApiModelProperty(value = "机构名称")
	private String orgName ;

	/** 规范名称 **/
	@Column(name="Norm_Name")
	@ApiModelProperty(value = "规范名称")
	private String normName ;

	/** 机构曾用名 **/
	@Column(name="ORG_Name_Other")
	@ApiModelProperty(value = "机构曾用名")
	private String orgNameOther ;

	/** 依托单位 **/
	@Column(name="Rely")
	@ApiModelProperty(value = "依托单位")
	private String rely ;

	/**  **/
	@Column(name="Rely_Anyname1")
	@ApiModelProperty(value = "")
	private String relyAnyname1 ;

	/**  **/
	@Column(name="Rely_Anyname2")
	@ApiModelProperty(value = "")
	private String relyAnyname2 ;

	/** 上级主管单位 **/
	@Column(name="Compunit")
	@ApiModelProperty(value = "上级主管单位")
	private String compunit ;

	/** 内部机构名称 **/
	@Column(name="ORG_Name_Internal")
	@ApiModelProperty(value = "内部机构名称")
	private String orgNameInternal ;

	/** 下属机构名称 **/
	@Column(name="ORG_Name_Sub")
	@ApiModelProperty(value = "下属机构名称")
	private String orgNameSub ;

	/** 重点实验室名称 **/
	@Column(name="Keyname")
	@ApiModelProperty(value = "重点实验室名称")
	private String keyname ;

	/** Email **/
	@Column(name="Email")
	@ApiModelProperty(value = "Email")
	private String email ;

	/** Internet **/
	@Column(name="Url")
	@ApiModelProperty(value = "Internet")
	private String url ;

	/** 负责人 **/
	@Column(name="Person")
	@ApiModelProperty(value = "负责人")
	private String person ;

	/** 负责人 **/
	@Column(name="Multiperson")
	@ApiModelProperty(value = "负责人")
	private String multiperson ;

	/** 通信地址 **/
	@Column(name="Address")
	@ApiModelProperty(value = "通信地址")
	private String address ;

	/** 电话 **/
	@Column(name="Telphone")
	@ApiModelProperty(value = "电话")
	private String telphone ;

	/** 传真 **/
	@Column(name="Fax")
	@ApiModelProperty(value = "传真")
	private String fax ;

	/** 成立年代 **/
	@Column(name="Year")
	@ApiModelProperty(value = "成立年代")
	private Date year ;

	/** 机构简介 **/
	@Column(name="ORG_Intro")
	@ApiModelProperty(value = "机构简介")
	private String orgIntro ;

	/** 职工人数 **/
	@Column(name="Emps")
	@ApiModelProperty(value = "职工人数")
	private String emps ;

	/** 科研人数 **/
	@Column(name="Techs")
	@ApiModelProperty(value = "科研人数")
	private String techs ;

	/** 主要研究人员 **/
	@Column(name="Leadresh")
	@ApiModelProperty(value = "主要研究人员")
	private String leadresh ;

	/** 获奖情况 **/
	@Column(name="Awards")
	@ApiModelProperty(value = "获奖情况")
	private String awards ;

	/** 拥有专利数 **/
	@Column(name="Patents")
	@ApiModelProperty(value = "拥有专利数")
	private String patents ;

	/** 科研成果 **/
	@Column(name="Patent")
	@ApiModelProperty(value = "科研成果")
	private String patent ;

	/** 拥有专利 **/
	@Column(name="Profit")
	@ApiModelProperty(value = "拥有专利")
	private String profit ;

	/** 科研设备 **/
	@Column(name="Sciasset")
	@ApiModelProperty(value = "科研设备")
	private String sciasset ;

	/** 学科研究范围 **/
	@Column(name="Reshdis")
	@ApiModelProperty(value = "学科研究范围")
	private String reshdis ;

	/** 进展中课题 **/
	@Column(name="Matter")
	@ApiModelProperty(value = "进展中课题")
	private String matter ;

	/** 推广技术与项目 **/
	@Column(name="Project")
	@ApiModelProperty(value = "推广技术与项目")
	private String project ;

	/** 产品信息 **/
	@Column(name="w_Scientific_ORG")
	@ApiModelProperty(value = "产品信息")
	private String wScientificOrg ;

	/** 出版刊物 **/
	@Column(name="Jour")
	@ApiModelProperty(value = "出版刊物")
	private String jour ;

	/** 机构类别 **/
	@Column(name="ORG_Type")
	@ApiModelProperty(value = "机构类别")
	private String orgType ;

	/** 所在地代码 **/
	@Column(name="Place_Code")
	@ApiModelProperty(value = "所在地代码")
	private String placeCode ;

	/** 学科分类 **/
	@Column(name="Discipline_Class")
	@ApiModelProperty(value = "学科分类")
	private String disciplineClass ;

	/** 学科代码 **/
	@Column(name="Discipline_Code")
	@ApiModelProperty(value = "学科代码")
	private String disciplineCode ;

	/** 学科代码 **/
	@Column(name="Discipline_Code2")
	@ApiModelProperty(value = "学科代码")
	private String disciplineCode2 ;

	/** 关键词 **/
	@Column(name="KeyWord")
	@ApiModelProperty(value = "关键词")
	private String keyword ;

	/** 机构ID **/
	@Column(name="ORG_ID")
	@ApiModelProperty(value = "机构ID")
	private String orgId ;

	/** 科研机构推介信息 **/
	@Column(name="ORG_Recommendations")
	@ApiModelProperty(value = "科研机构推介信息")
	private String orgRecommendations ;

	/** 是否成品数据 **/
	@Column(name="Finished_Product")
	@ApiModelProperty(value = "是否成品数据")
	private String finishedProduct ;


}
