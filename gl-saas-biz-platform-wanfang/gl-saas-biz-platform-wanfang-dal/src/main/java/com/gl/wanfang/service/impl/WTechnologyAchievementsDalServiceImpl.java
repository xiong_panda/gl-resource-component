package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WTechnologyAchievementsMapper;
import com.gl.wanfang.model.WTechnologyAchievements;
import com.gl.wanfang.service.IWTechnologyAchievementsDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WTechnologyAchievementsDalServiceImpl implements IWTechnologyAchievementsDalService {
    @Autowired
    WTechnologyAchievementsMapper wTechnologyAchievementsMapper;
    /**
     * 分页匹配模糊查询
     * @param params
     * @return
     */
    public List<WTechnologyAchievements> getListByPage(Map<String, Object> params){
        List<WTechnologyAchievements> listByPage = wTechnologyAchievementsMapper.getListByPage(params);
        return listByPage;
    };

}
