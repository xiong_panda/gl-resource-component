package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.SysCodeMapper;
import com.gl.wanfang.model.SysCode;
import com.gl.wanfang.service.ISysCodeDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional(readOnly = true)
@Service
public class SysCodeDalServiceImpl implements ISysCodeDalService {

	@Autowired
	private SysCodeMapper sysCodeMapper;
	
	@Override
	public SysCode translateCode(Map<String, Object> params) {
		return sysCodeMapper.getOne(params);
	}


}
