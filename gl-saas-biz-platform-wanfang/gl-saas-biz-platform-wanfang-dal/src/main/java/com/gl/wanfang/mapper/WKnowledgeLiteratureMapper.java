package com.gl.wanfang.mapper;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WKnowledgeLiterature;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WKnowledgeLiteratureMapper extends MyMapper<WKnowledgeLiterature> {


	/**
	 * 知识文件匹配检索
	 * @param params
	 * @return
	 */
	List<WKnowledgeLiterature> getList(Map<String, Object> params);


	/**
	 * 知识文件模糊检索
	 * @param params
	 * @return
	 */
	List<WKnowledgeLiterature> getListByPage(Map<String, Object> params);

}
