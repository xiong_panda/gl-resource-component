package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WOverseasPatent;
import com.gl.wanfang.outdto.EasyWOverseasPatentOutDto;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WOverseasPatentMapper extends MyMapper<WOverseasPatent> {

    /**
     * 海外专利 匹配分頁查詢
     *
     * @param params
     * @return
     */
    List<WOverseasPatent> getList(Map<String, Object> params);

    /**
     * 海外专利模糊匹配查询
     *
     * @param params
     * @return
     */
    List<WOverseasPatent> getListByPage(Map<String, Object> params);

    /**
     * 获取专利简略信息列表
     *
     * @param params
     * @return
     */
    List<EasyWOverseasPatentOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 获取专利信息详细
     *
     * @param params
     * @return
     */
    WOverseasPatentOutDto getOneByParams(Map<String, Object> params);
}
