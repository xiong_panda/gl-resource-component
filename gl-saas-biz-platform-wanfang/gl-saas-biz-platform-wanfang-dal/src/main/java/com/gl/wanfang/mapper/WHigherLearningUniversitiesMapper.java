package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WHigherLearningUniversities;
import com.gl.wanfang.outdto.EasyWHigherLearningUniversitiesOutDto;
import com.gl.wanfang.outdto.WHigherLearningUniversitiesOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WHigherLearningUniversitiesMapper extends MyMapper<WHigherLearningUniversities> {

    /**
     * 高等院校列表匹配
     *
     * @return
     */
    List<WHigherLearningUniversities> getList(Map<String, Object> params);

    /**
     * 高等院校模糊匹配
     *
     * @return
     */
    List<WHigherLearningUniversities> getListByPage(Map<String, Object> params);


    /**
     * 根据名字进行查询
     *
     * @param params
     * @return
     */
    WHigherLearningUniversities getOneByParams(Map<String, Object> params);

    List<EasyWHigherLearningUniversitiesOutDto> getWHigherLearningUniversitiesByDB2(Map<String, Object> params);

    WHigherLearningUniversitiesOutDto getDetailByParams(Map<String, Object> params);


}
