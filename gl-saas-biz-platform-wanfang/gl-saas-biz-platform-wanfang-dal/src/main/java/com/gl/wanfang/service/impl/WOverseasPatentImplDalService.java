package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WOverseasPatentMapper;
import com.gl.wanfang.model.WOverseasPatent;
import com.gl.wanfang.outdto.EasyWOverseasPatentOutDto;
import com.gl.wanfang.outdto.WOverseasPatentOutDto;
import com.gl.wanfang.service.IWOverseasPatentDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
@Service
public class WOverseasPatentImplDalService implements IWOverseasPatentDalService {

    @Autowired
    WOverseasPatentMapper wOverseasPatentMapper;

    /**
     * 海外专利 模糊匹配
     *
     * @return
     */
    @Override
    public List<WOverseasPatent> getListByPage(Map<String, Object> params) {
        List<WOverseasPatent> listByPage = wOverseasPatentMapper.getListByPage(params);
        return listByPage;
    }

    @Override
    public List<EasyWOverseasPatentOutDto> getListByPage2(Map<String, Object> params) {
        return wOverseasPatentMapper.getListByPage2(params);
    }

    @Override
    public WOverseasPatentOutDto getOneByParams(Map<String, Object> params) {
        return wOverseasPatentMapper.getOneByParams(params);
    }


}
