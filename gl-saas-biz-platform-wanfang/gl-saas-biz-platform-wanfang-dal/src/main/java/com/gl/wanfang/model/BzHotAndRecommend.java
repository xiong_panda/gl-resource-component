package com.gl.wanfang.model;

/**
 * @auther Qinye
 * @date 2020/5/11 14:02
 * @description
 */
public class BzHotAndRecommend {
    /**主键id**/
    private String id;
    /**标题id**/
    private String resourceId;
    /**标题**/
    private String title;
    /**模块标识**/
    private String sourcetype;
    /**搜索用户id**/
    private String userId;
    /**用户行业**/
    private String industry;
    /**用户类型**/
    private String userType;
    /**访问时间**/
    private String requestTime;
    /**地区**/
    private String area;
    /**访问量**/
    private Integer amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSourcetype() {
        return sourcetype;
    }

    public void setSourcetype(String sourcetype) {
        this.sourcetype = sourcetype;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
