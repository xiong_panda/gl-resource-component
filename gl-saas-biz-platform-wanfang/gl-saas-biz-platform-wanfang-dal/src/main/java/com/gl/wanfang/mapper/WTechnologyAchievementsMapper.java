package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.common.mapper.MyMapper;
import com.gl.wanfang.model.WTechnologyAchievements;
import com.gl.wanfang.outdto.EasyWTechnologyAchievementsOutDto;
import com.gl.wanfang.outdto.WTechnologyAchievementsOutDto;
import org.springframework.stereotype.Component;

/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 *
 * @author code_generator
 */
@MapperPrimary
@Component
public interface WTechnologyAchievementsMapper extends MyMapper<WTechnologyAchievements> {

    /**
     * 匹配
     *
     * @param params
     * @return
     */
    List<WTechnologyAchievements> getList(Map<String, Object> params);

    /**
     * 分页模糊查询
     *
     * @param params
     * @return
     */
    List<WTechnologyAchievements> getListByPage(Map<String, Object> params);

    /**
     * 科技成果简略减速
     *
     * @param params
     * @return
     */
    List<EasyWTechnologyAchievementsOutDto> getListByPage2(Map<String, Object> params);

    /**
     * 科技成果详情检索
     *
     * @param params
     * @return
     */
    WTechnologyAchievementsOutDto getOneByParams(Map<String, Object> params);
}
