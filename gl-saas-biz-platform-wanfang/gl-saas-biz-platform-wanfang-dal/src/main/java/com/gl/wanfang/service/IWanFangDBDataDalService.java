package com.gl.wanfang.service;


import com.gl.wanfang.model.WChinesePatent;
import com.gl.wanfang.outdto.EasyWChinesePatentOUTDTO;
import com.gl.wanfang.outdto.WChinesePatentOUTDTO;

import java.util.List;
import java.util.Map;

public interface IWanFangDBDataDalService {

    /**
     * 专利本地检索
     *
     * @param page
     * @return
     */
    List<WChinesePatent> getPatentInfoByDB(Map page);

    /**
     * 中文专利简略信息
     *
     * @param params
     * @return
     */
    List<EasyWChinesePatentOUTDTO> getPatentInfoByDB2(Map<String, Object> params);


    /**
     * 中文专利详情信息
     *
     * @param params
     * @return
     */
    WChinesePatentOUTDTO getOneByParams(Map<String, Object> params);
}
