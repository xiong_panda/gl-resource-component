package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.UserSearchGatherMapper;
import com.gl.wanfang.model.BzHotAndRecommend;
import com.gl.wanfang.outdto.GatherOutDTO;
import com.gl.wanfang.service.UserSearchGatherDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @auther Qinye
 * @date 2020/5/11 14:12
 * @description
 */
@Transactional(readOnly = true)
@Service
public class UserSearchGatherDalServiceImpl implements UserSearchGatherDalService {

    @Autowired
    private UserSearchGatherMapper userSearchGatherMapper;

    @Override
    public List<GatherOutDTO> getHotAndRecommend(BzHotAndRecommend bzHotAndRecommend) {
        return userSearchGatherMapper.getHotAndRecommend(bzHotAndRecommend);
    }
}
