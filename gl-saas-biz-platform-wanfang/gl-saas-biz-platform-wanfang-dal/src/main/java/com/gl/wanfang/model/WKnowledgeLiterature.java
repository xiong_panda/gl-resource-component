package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 知识文献
 * @author code_generator
 */
@Table(name = "W_KNOWLEDGE_LITERATURE")
@Data
public class WKnowledgeLiterature implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 全文索引 **/
	@Column(name="FullText")
    @ApiModelProperty(value = "全文索引")
	private String fulltext ; 

	/** 主题全文检索(标题、关键词、摘要) **/
	@Column(name="Topic")
    @ApiModelProperty(value = "主题全文检索(标题、关键词、摘要)")
	private String topic ; 

	/** 唯一编号 **/
	@Id
    @ApiModelProperty(value = "唯一编号")
	private String id ; 

	/** 记录顺序号 **/
	@Column(name="OrderID")
    @ApiModelProperty(value = "记录顺序号")
	private String orderid ; 

	/** DOI **/
	@Column(name="Doi")
    @ApiModelProperty(value = "DOI")
	private String doi ; 

	/** w_ID **/
	@Column(name="w_ID")
    @ApiModelProperty(value = "w_ID")
	private String wId ; 

	/** V_ID **/
	@Column(name="V_ID")
    @ApiModelProperty(value = "V_ID")
	private String vId ; 

	/** C_ID **/
	@Column(name="C_ID")
    @ApiModelProperty(value = "C_ID")
	private String cId ; 

	/** L_ID **/
	@Column(name="L_ID")
    @ApiModelProperty(value = "L_ID")
	private String lId ; 

	/** 中文标题 **/
	@Column(name="Chinese_Title")
    @ApiModelProperty(value = "中文标题")
	private String chineseTitle ; 

	/** 英文标题 **/
	@Column(name="English_Title")
    @ApiModelProperty(value = "英文标题")
	private String englishTitle ; 

	/** 中文标题:英文标题 **/
	@Column(name="C_E_Title")
    @ApiModelProperty(value = "中文标题:英文标题")
	private String cETitle ; 

	/** 作者ID **/
	@Column(name="Author_ID")
    @ApiModelProperty(value = "作者ID")
	private String authorId ; 

	/** 中文作者 **/
	@Column(name="Chinese_Author")
    @ApiModelProperty(value = "中文作者")
	private String chineseAuthor ; 

	/** 英文作者 **/
	@Column(name="English_Author")
    @ApiModelProperty(value = "英文作者")
	private String englishAuthor ; 

	/** 作者个数 **/
	@Column(name="Number_Authors")
    @ApiModelProperty(value = "作者个数")
	private String numberAuthors ; 

	/** 作者FID **/
	@Column(name="Author_FID")
    @ApiModelProperty(value = "作者FID")
	private String authorFid ; 

	/** 中文第一作者 **/
	@Column(name="Chinese_First_Author")
    @ApiModelProperty(value = "中文第一作者")
	private String chineseFirstAuthor ; 

	/** 英文第一作者 **/
	@Column(name="English_First_Author")
    @ApiModelProperty(value = "英文第一作者")
	private String englishFirstAuthor ; 

	/** 作者个数 **/
	@Column(name="Author_Number")
    @ApiModelProperty(value = "作者个数")
	private String authorNumber ; 

	/** 中文作者:英文作者 **/
	@Column(name="Chinese_English_Author")
    @ApiModelProperty(value = "中文作者:英文作者")
	private String chineseEnglishAuthor ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName ; 

	/** 作者名称(全文检索) **/
	@Column(name="Author_Name2")
    @ApiModelProperty(value = "作者名称(全文检索)")
	private String authorName2 ; 

	/** 规范单位名称 **/
	@Column(name="Authority_Unit_Name")
    @ApiModelProperty(value = "规范单位名称")
	private String authorityUnitName ; 

	/** 中文作者单位 **/
	@Column(name="Chinese_Author_Unit")
    @ApiModelProperty(value = "中文作者单位")
	private String chineseAuthorUnit ; 

	/** 英文作者单位 **/
	@Column(name="English_Author_Unit")
    @ApiModelProperty(value = "英文作者单位")
	private String englishAuthorUnit ; 

	/** 中文第一作者单位 **/
	@Column(name="Chinese_First_Author_Unit")
    @ApiModelProperty(value = "中文第一作者单位")
	private String chineseFirstAuthorUnit ; 

	/** 中文第一作者单位一级名称 **/
	@Column(name="Chinese_First_Author_Unit_Name")
    @ApiModelProperty(value = "中文第一作者单位一级名称")
	private String chineseFirstAuthorUnitName ; 

	/** 英文第一作者单位 **/
	@Column(name="First_English_Author")
    @ApiModelProperty(value = "英文第一作者单位")
	private String firstEnglishAuthor ; 

	/** 机构名称 **/
	@Column(name="ORG_Name")
    @ApiModelProperty(value = "机构名称")
	private String orgName ; 

	/** 机构名称(全文检索) **/
	@Column(name="ORG_Name_Search")
    @ApiModelProperty(value = "机构名称(全文检索)")
	private String orgNameSearch ; 

	/** 中文作者单位:英文作者单位:规范单位名称 **/
	@Column(name="C_E_S_Author_Unit")
    @ApiModelProperty(value = "中文作者单位:英文作者单位:规范单位名称")
	private String cESAuthorUnit ; 

	/** QKID **/
	@Column(name="QkID")
    @ApiModelProperty(value = "QKID")
	private String qkid ; 

	/**  **/
	@Column(name="Issn")
    @ApiModelProperty(value = "")
	private String issn ; 

	/** 中文刊名 **/
	@Column(name="Chinese_Journal_Name")
    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName ; 

	/** 英文刊名 **/
	@Column(name="English_Journal_Name")
    @ApiModelProperty(value = "英文刊名")
	private String englishJournalName ; 

	/**  **/
	@Column(name="Journal")
    @ApiModelProperty(value = "")
	private String journal ; 

	/**  **/
	@Column(name="Journal_Anyname")
    @ApiModelProperty(value = "")
	private String journalAnyname ; 

	/**  **/
	@Column(name="Journal_Anyname2")
    @ApiModelProperty(value = "")
	private String journalAnyname2 ; 

	/** 中文刊名 **/
	@Column(name="Chinese_Journal_Name2")
    @ApiModelProperty(value = "中文刊名")
	private String chineseJournalName2 ; 

	/**  **/
	@Column(name="Fjoucn_Anyname")
    @ApiModelProperty(value = "")
	private String fjoucnAnyname ; 

	/**  **/
	@Column(name="Fjoucn_Anyname2")
    @ApiModelProperty(value = "")
	private String fjoucnAnyname2 ; 

	/** 日期 **/
	@Column(name="Date")
    @ApiModelProperty(value = "日期")
	private String date ; 

	/** 年 **/
	@Column(name="Year")
    @ApiModelProperty(value = "年")
	private String year ; 

	/** 卷 **/
	@Column(name="Volume")
    @ApiModelProperty(value = "卷")
	private String volume ; 

	/** 期 **/
	@Column(name="Period")
    @ApiModelProperty(value = "期")
	private String period ; 

	/** 页码 **/
	@Column(name="Page_Number")
    @ApiModelProperty(value = "页码")
	private String pageNumber ; 

	/** 页数 **/
	@Column(name="Pages")
    @ApiModelProperty(value = "页数")
	private String pages ; 

	/** 中文栏目名称 **/
	@Column(name="Chinese_Column_Name")
    @ApiModelProperty(value = "中文栏目名称")
	private String chineseColumnName ; 

	/** 英文栏目名称 **/
	@Column(name="English_Column_Name")
    @ApiModelProperty(value = "英文栏目名称")
	private String englishColumnName ; 

	/** 语种 **/
	@Column(name="Language")
    @ApiModelProperty(value = "语种")
	private String language ; 

	/** 中图分类号:机标分类号 **/
	@Column(name="Cls_Machine_Standard_Code")
    @ApiModelProperty(value = "中图分类号:机标分类号")
	private String clsMachineStandardCode ; 

	/** 文献标识码 **/
	@Column(name="Literature_IDentification_Code")
    @ApiModelProperty(value = "文献标识码")
	private String literatureIdentificationCode ; 

	/** 机标分类号 **/
	@Column(name="Machine_Label_Class_Code")
    @ApiModelProperty(value = "机标分类号")
	private String machineLabelClassCode ; 

	/** 中图分类号 **/
	@Column(name="Cls_Code")
    @ApiModelProperty(value = "中图分类号")
	private String clsCode ; 

	/** 中图分类二级 **/
	@Column(name="Cls_level2")
    @ApiModelProperty(value = "中图分类二级")
	private String clsLevel2 ; 

	/** 中图分类顶级 **/
	@Column(name="Cls_Top")
    @ApiModelProperty(value = "中图分类顶级")
	private String clsTop ; 

	/** 中图分类(三级) **/
	@Column(name="Cls_level3")
    @ApiModelProperty(value = "中图分类(三级)")
	private String clsLevel3 ; 

	/**  **/
	@Column(name="IID")
    @ApiModelProperty(value = "")
	private String iid ; 

	/** 中文关键词 **/
	@Column(name="Chinese_KeyWord")
    @ApiModelProperty(value = "中文关键词")
	private String chineseKeyword ; 

	/** 中文关键词:英文关键词:机标关键词 **/
	@Column(name="Ce_KeyWord")
    @ApiModelProperty(value = "中文关键词:英文关键词:机标关键词")
	private String ceKeyword ; 

	/** 英文关键词 **/
	@Column(name="English_KeyWord")
    @ApiModelProperty(value = "英文关键词")
	private String englishKeyword ; 

	/** 机标关键词 **/
	@Column(name="Machine_KeyWord")
    @ApiModelProperty(value = "机标关键词")
	private String machineKeyword ; 

	/** 中文摘要 **/
	@Column(name="Chinese_Abstract")
    @ApiModelProperty(value = "中文摘要")
	private String chineseAbstract ; 

	/** 中文摘要:英文摘要:正文首段 **/
	@Column(name="C_Paragraph")
    @ApiModelProperty(value = "中文摘要:英文摘要:正文首段")
	private String cParagraph ; 

	/** 英文摘要 **/
	@Column(name="English_Abstract")
    @ApiModelProperty(value = "英文摘要")
	private String englishAbstract ; 

	/** 正文首段 **/
	@Column(name="First_Paragraph")
    @ApiModelProperty(value = "正文首段")
	private String firstParagraph ; 

	/** 基金 **/
	@Column(name="Fund")
    @ApiModelProperty(value = "基金")
	private String fund ; 

	/** 基金全文检索 **/
	@Column(name="Fund_FullText_Search")
    @ApiModelProperty(value = "基金全文检索")
	private String fundFulltextSearch ; 

	/** 基金名称 **/
	@Column(name="Fund_Name")
    @ApiModelProperty(value = "基金名称")
	private String fundName ; 

	/** 基金名称 **/
	@Column(name="Fund_Name2")
    @ApiModelProperty(value = "基金名称")
	private String fundName2 ; 

	/** 基金项目 **/
	@Column(name="Fund_Project")
    @ApiModelProperty(value = "基金项目")
	private String fundProject ; 

	/** 基金名称 **/
	@Column(name="Fund_Name3")
    @ApiModelProperty(value = "基金名称")
	private String fundName3 ; 

	/** BYCS **/
	@Column(name="Bycs")
    @ApiModelProperty(value = "BYCS")
	private String bycs ; 

	/** 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊 **/
	@Column(name="Core")
    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
	private String core ; 

	/** 北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊 **/
	@Column(name="Score")
    @ApiModelProperty(value = "北大核心期刊:中信所核心期刊:南大核心期刊:中科院核心期刊:社科院核心期刊")
	private String score ; 

	/**  **/
	@Column(name="Ecore")
    @ApiModelProperty(value = "")
	private String ecore ; 

	/** 数据来源 **/
	@Column(name="Data_Source")
    @ApiModelProperty(value = "数据来源")
	private String dataSource ; 

	/** 机构层级ID **/
	@Column(name="ORG_Hierarchy_ID")
    @ApiModelProperty(value = "机构层级ID")
	private String orgHierarchyId ; 

	/** 机构ID **/
	@Column(name="ORG_ID")
    @ApiModelProperty(value = "机构ID")
	private String orgId ; 

	/** 机构所在省 **/
	@Column(name="ORG_Province")
    @ApiModelProperty(value = "机构所在省")
	private String orgProvince ; 

	/** 机构所在市 **/
	@Column(name="ORG_City")
    @ApiModelProperty(value = "机构所在市")
	private String orgCity ; 

	/** 机构类型 **/
	@Column(name="ORG_Type")
    @ApiModelProperty(value = "机构类型")
	private String orgType ; 

	/**  **/
	@Column(name="FORGID")
    @ApiModelProperty(value = "")
	private String forgid ; 

	/**  **/
	@Column(name="EndORGtype")
    @ApiModelProperty(value = "")
	private String endorgtype ; 

	/** 第一机构终级机构ID **/
	@Column(name="ORG_Frist_Final_ID")
    @ApiModelProperty(value = "第一机构终级机构ID")
	private String orgFristFinalId ; 

	/** 第一机构层级机构ID **/
	@Column(name="ORG_Frist_Hierarchy_ID")
    @ApiModelProperty(value = "第一机构层级机构ID")
	private String orgFristHierarchyId ; 

	/** 第一机构所在市 **/
	@Column(name="ORG_Frist_City")
    @ApiModelProperty(value = "第一机构所在市")
	private String orgFristCity ; 

	/** 第一机构终级机构所在市 **/
	@Column(name="ORG_Frist_Final_City")
    @ApiModelProperty(value = "第一机构终级机构所在市")
	private String orgFristFinalCity ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince ; 

	/** 第一机构所在省 **/
	@Column(name="ORG_Frist_Province2")
    @ApiModelProperty(value = "第一机构所在省")
	private String orgFristProvince2 ; 

	/** 第一机构终级机构类型 **/
	@Column(name="ORG_Frist_Final_Type")
    @ApiModelProperty(value = "第一机构终级机构类型")
	private String orgFristFinalType ; 

	/** 第一机构类型 **/
	@Column(name="ORG_Frist_Type")
    @ApiModelProperty(value = "第一机构类型")
	private String orgFristType ; 

	/** 全部作者ID **/
	@Column(name="All_Author_ID")
    @ApiModelProperty(value = "全部作者ID")
	private String allAuthorId ; 

	/**  **/
	@Column(name="ORG_Number")
    @ApiModelProperty(value = "")
	private String orgNumber ; 

	/** 文献权重 **/
	@Column(name="Literature_Weight")
    @ApiModelProperty(value = "文献权重")
	private String literatureWeight ; 

	/**  **/
	@Column(name="Quarter")
    @ApiModelProperty(value = "")
	private String quarter ; 

	/**  **/
	@Column(name="Halfyear")
    @ApiModelProperty(value = "")
	private String halfyear ; 

	/** 索引管理字段(无用) **/
	@Column(name="Error_Code")
    @ApiModelProperty(value = "索引管理字段(无用)")
	private String errorCode ; 

	/**  **/
	@Column(name="Author_Info")
    @ApiModelProperty(value = "")
	private String authorInfo ; 

	/** 作者信息.姓名 **/
	@Column(name="Author_Info_Name")
    @ApiModelProperty(value = "作者信息.姓名")
	private String authorInfoName ; 

	/** 作者信息.作者次序 **/
	@Column(name="Author_Info_Order")
    @ApiModelProperty(value = "作者信息.作者次序")
	private String authorInfoOrder ; 

	/** 作者信息.工作单位 **/
	@Column(name="Author_Info_Unit")
    @ApiModelProperty(value = "作者信息.工作单位")
	private String authorInfoUnit ; 

	/** 作者信息.工作单位一级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level1")
    @ApiModelProperty(value = "作者信息.工作单位一级机构")
	private String authorInfoUnitOrgLevel1 ; 

	/** 作者信息.工作单位二级机构 **/
	@Column(name="Author_Info_Unit_ORG_Level2")
    @ApiModelProperty(value = "作者信息.工作单位二级机构")
	private String authorInfoUnitOrgLevel2 ; 

	/** 作者信息.工作单位类型 **/
	@Column(name="Author_Info_Unit_Type")
    @ApiModelProperty(value = "作者信息.工作单位类型")
	private String authorInfoUnitType ; 

	/** 作者信息.工作单位所在省 **/
	@Column(name="Author_Info_Unit_Province")
    @ApiModelProperty(value = "作者信息.工作单位所在省")
	private String authorInfoUnitProvince ; 

	/** 作者信息.工作单位所在市 **/
	@Column(name="Author_Info_Unit_City")
    @ApiModelProperty(value = "作者信息.工作单位所在市")
	private String authorInfoUnitCity ; 

	/** 作者信息.工作单位所在县 **/
	@Column(name="Author_Info_Unit_County")
    @ApiModelProperty(value = "作者信息.工作单位所在县")
	private String authorInfoUnitCounty ; 

	/** 作者信息.唯一ID **/
	@Column(name="Author_Info_ID")
    @ApiModelProperty(value = "作者信息.唯一ID")
	private String authorInfoId ; 

	/** 作者信息.工作单位唯一ID **/
	@Column(name="Author_Info_Unit_ID")
    @ApiModelProperty(value = "作者信息.工作单位唯一ID")
	private String authorInfoUnitId ; 

	/**  **/
	@Column(name="ORG_Info")
    @ApiModelProperty(value = "")
	private String orgInfo ; 

	/** 机构信息.机构名称 **/
	@Column(name="ORG_Info_Name")
    @ApiModelProperty(value = "机构信息.机构名称")
	private String orgInfoName ; 

	/** 机构信息.机构次序 **/
	@Column(name="ORG_Info_Order")
    @ApiModelProperty(value = "机构信息.机构次序")
	private String orgInfoOrder ; 

	/** 机构信息.机构类型 **/
	@Column(name="ORG_Info_Type")
    @ApiModelProperty(value = "机构信息.机构类型")
	private String orgInfoType ; 

	/** 机构信息.省 **/
	@Column(name="ORG_Info_Province")
    @ApiModelProperty(value = "机构信息.省")
	private String orgInfoProvince ; 

	/** 机构信息.市 **/
	@Column(name="ORG_Info_City")
    @ApiModelProperty(value = "机构信息.市")
	private String orgInfoCity ; 

	/** 机构信息.县 **/
	@Column(name="ORG_Info_County")
    @ApiModelProperty(value = "机构信息.县")
	private String orgInfoCounty ; 

	/** 机构信息.五级机构层级码 **/
	@Column(name="ORG_Info_Hierarchy")
    @ApiModelProperty(value = "机构信息.五级机构层级码")
	private String orgInfoHierarchy ; 

	/** 机构信息.1级机构名称 **/
	@Column(name="ORG_Info_Level1")
    @ApiModelProperty(value = "机构信息.1级机构名称")
	private String orgInfoLevel1 ; 

	/** 机构信息.2级机构名称 **/
	@Column(name="ORG_Info_Level2")
    @ApiModelProperty(value = "机构信息.2级机构名称")
	private String orgInfoLevel2 ; 

	/** 机构信息.3级机构名称 **/
	@Column(name="ORG_Info_Level3")
    @ApiModelProperty(value = "机构信息.3级机构名称")
	private String orgInfoLevel3 ; 

	/** 机构信息.4级机构名称 **/
	@Column(name="ORG_Info_Level4")
    @ApiModelProperty(value = "机构信息.4级机构名称")
	private String orgInfoLevel4 ; 

	/** 机构信息.5级机构名称 **/
	@Column(name="ORG_Info_Level5")
    @ApiModelProperty(value = "机构信息.5级机构名称")
	private String orgInfoLevel5 ; 

	/** 标题:英文标题 **/
	@Column(name="Title2")
    @ApiModelProperty(value = "标题:英文标题")
	private String title2 ; 

	/** 关键词 **/
	@Column(name="KeyWord")
    @ApiModelProperty(value = "关键词")
	private String keyword ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment ; 

	/** 颁布部门(全文检索) **/
	@Column(name="Proclaim_Department_FullText")
    @ApiModelProperty(value = "颁布部门(全文检索)")
	private String proclaimDepartmentFulltext ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department2")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment2 ; 

	/**  **/
	@Column(name="AUTHOR")
    @ApiModelProperty(value = "")
	private String author ; 

	/** 中图分类 **/
	@Column(name="Cls")
    @ApiModelProperty(value = "中图分类")
	private String cls ; 

	/** 颁布日期 **/
	@Column(name="Proclaim_Date")
    @ApiModelProperty(value = "颁布日期")
	private String proclaimDate ; 

	/** 终审日期 **/
	@Column(name="Final_Date")
    @ApiModelProperty(value = "终审日期")
	private String finalDate ; 

	/**  **/
	@Column(name="RID")
    @ApiModelProperty(value = "")
	private String rid ; 

	/** 标题 **/
	@Column(name="Title")
    @ApiModelProperty(value = "标题")
	private String title ; 

	/** 发文文号 **/
	@Column(name="Post_Document_Code")
    @ApiModelProperty(value = "发文文号")
	private String postDocumentCode ; 

	/** 颁布部门 **/
	@Column(name="Proclaim_Department3")
    @ApiModelProperty(value = "颁布部门")
	private String proclaimDepartment3 ; 

	/** 行业地区码_部门代码 **/
	@Column(name="IndustryCode_Department_Code")
    @ApiModelProperty(value = "行业地区码_部门代码")
	private String industrycodeDepartmentCode ; 

	/** 效力级别 **/
	@Column(name="Value_Level")
    @ApiModelProperty(value = "效力级别")
	private String valueLevel ; 

	/** 效力代码 **/
	@Column(name="Value_Code")
    @ApiModelProperty(value = "效力代码")
	private String valueCode ; 

	/** 时效性 **/
	@Column(name="Timeliness")
    @ApiModelProperty(value = "时效性")
	private String timeliness ; 

	/** 批准日期 **/
	@Column(name="Approval_Date")
    @ApiModelProperty(value = "批准日期")
	private String approvalDate ; 

	/** 签字日期 **/
	@Column(name="Signature_Date")
    @ApiModelProperty(value = "签字日期")
	private String signatureDate ; 

	/**  **/
	@Column(name="YAPPRDATE")
    @ApiModelProperty(value = "")
	private String yapprdate ; 

	/**  **/
	@Column(name="YSIGNDATE")
    @ApiModelProperty(value = "")
	private String ysigndate ; 

	/** 实施日期 **/
	@Column(name="IMPLEMENTATION_Date")
    @ApiModelProperty(value = "实施日期")
	private String implementationDate ; 

	/** 失效日期 **/
	@Column(name="Invalid_Date")
    @ApiModelProperty(value = "失效日期")
	private String invalidDate ; 

	/** 终审法院 **/
	@Column(name="Final_Court")
    @ApiModelProperty(value = "终审法院")
	private String finalCourt ; 

	/** 终审日期 **/
	@Column(name="Final_Date2")
    @ApiModelProperty(value = "终审日期")
	private String finalDate2 ; 

	/** 调解日期 **/
	@Column(name="mediate_Date")
    @ApiModelProperty(value = "调解日期")
	private String mediateDate ; 

	/** 内容分类 **/
	@Column(name="Cintent_Class")
    @ApiModelProperty(value = "内容分类")
	private String cintentClass ; 

	/** 内容分类码 **/
	@Column(name="Cintent_Class_Code")
    @ApiModelProperty(value = "内容分类码")
	private String cintentClassCode ; 

	/** URL **/
	@Column(name="URL")
    @ApiModelProperty(value = "URL")
	private String url ; 

	/** PDF全文 **/
	@Column(name="PDF_FullText")
    @ApiModelProperty(value = "PDF全文")
	private String pdfFulltext ; 

	/** 相关链接 **/
	@Column(name="Relevant_link")
    @ApiModelProperty(value = "相关链接")
	private String relevantLink ; 

	/** 历史链接 **/
	@Column(name="History_link")
    @ApiModelProperty(value = "历史链接")
	private String historyLink ; 

	/** 库别代码 **/
	@Column(name="Library_Code")
    @ApiModelProperty(value = "库别代码")
	private String libraryCode ; 

	/** 制作日期 **/
	@Column(name="Make_Date")
    @ApiModelProperty(value = "制作日期")
	private String makeDate ; 

	/** 行业分类 **/
	@Column(name="Industry_Class")
    @ApiModelProperty(value = "行业分类")
	private String industryClass ; 

	/** 行业分类码 **/
	@Column(name="Industry_Class_Code")
    @ApiModelProperty(value = "行业分类码")
	private String industryClassCode ; 

	/** 学科分类 **/
	@Column(name="Discipline_Class")
    @ApiModelProperty(value = "学科分类")
	private String disciplineClass ; 

	/** 法规正文 **/
	@Column(name="Rules_Text")
    @ApiModelProperty(value = "法规正文")
	private String rulesText ; 


}
