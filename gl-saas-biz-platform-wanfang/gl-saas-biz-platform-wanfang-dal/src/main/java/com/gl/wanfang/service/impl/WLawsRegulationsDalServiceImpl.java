package com.gl.wanfang.service.impl;
import com.gl.wanfang.mapper.WLawsRegulationsMapper;
import com.gl.wanfang.model.WLawsRegulations;
import com.gl.wanfang.outdto.EasyWLawsRegulationsOutDTO;
import com.gl.wanfang.outdto.WLawsRegulationsOutDTO;
import com.gl.wanfang.service.IWLawsRegulationsDalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
@Service
public class WLawsRegulationsDalServiceImpl implements IWLawsRegulationsDalService {

    @Autowired
    WLawsRegulationsMapper lawsRegulationsMapper;

    /**
     * 法律法规
     * 分页模糊匹配
     * @param params
     * @return
     */
    public List<WLawsRegulations> getListByPage(Map<String, Object> params){
        List<WLawsRegulations> listByPage = lawsRegulationsMapper.getListByPage(params);
        return  listByPage;
    }

    @Override
    public List<EasyWLawsRegulationsOutDTO> getListByPage2(Map<String, Object> params) {
        return lawsRegulationsMapper.getListByPage2(params);
    }

    @Override
    public WLawsRegulationsOutDTO getOneByParams(Map<String, Object> params) {
        return lawsRegulationsMapper.getOneByParams(params);
    }

}
