package com.gl.wanfang.service.impl;

import com.gl.wanfang.mapper.WChinesePaperOaMapper;
import com.gl.wanfang.model.WChinesePaperOa;
import com.gl.wanfang.outdto.EasyWChinesePaperOaOutDto;
import com.gl.wanfang.outdto.WChinesePaperOaOutDto;
import com.gl.wanfang.service.IWChinesePaperOaDalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * service业务处理层
 * @author code_generator
 */
@Service
@Transactional(readOnly=true)
@Slf4j
public class WChinesePaperOaDalServiceImpl  implements IWChinesePaperOaDalService {
	@Autowired
	private WChinesePaperOaMapper WChinesePaperOaMapper;


	@Override
	public List<WChinesePaperOa> getListByPage(Map<String, Object> params) {
		List<WChinesePaperOa> bypage = WChinesePaperOaMapper.getBypage(params);
		return bypage;
	}

	@Override
	public List<EasyWChinesePaperOaOutDto> getListByPage2(Map<String, Object> params) {
		return WChinesePaperOaMapper.getListByPage2(params);
	}

	@Override
	public WChinesePaperOaOutDto getOneByParams(Map<String, Object> params) {
		return WChinesePaperOaMapper.getOneByParams(params);
	}
}
