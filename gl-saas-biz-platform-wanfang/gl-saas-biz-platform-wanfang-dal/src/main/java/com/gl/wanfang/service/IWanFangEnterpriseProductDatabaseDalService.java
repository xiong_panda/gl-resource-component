package com.gl.wanfang.service;


import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.model.WanFangEnterpriseProductDatabase;
import com.gl.wanfang.model.WanFangSupplierDatabase;
import com.gl.wanfang.outdto.EasyWanFangEnterpriseProductDatabaseOutDto;
import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;

import java.util.List;
import java.util.ListResourceBundle;
import java.util.Map;
//import com.gl.wanfang.outdto.WanFangEnterpriseProductDatabaseOutDto;


/**
 * service业务处理层,自定义方法请写在此接口中
 *
 * @author code_generator
 */
public interface IWanFangEnterpriseProductDatabaseDalService {


    /**
     * 精准分页匹配
     *
     * @param params
     * @return
     */
    public List<WanFangEnterpriseProductDatabase> getList(Map<String, Object> params);

    /**
     * 查询供应商
     *
     * @param params
     * @return
     */
    List<WanFangEnterpriseProductDatabase> getListEnterpriseProductDatabase(Map<String, Object> params);


    /**
     * 供应商筛选
     *
     * @return
     */
    List<WanFangSupplierDatabase> getEnterpriseProductDatabase(Map<String, Object> params);

    /**
     * 获取企业列表信息 简略
     *
     * @param params
     * @return
     */
    List<EasyWanFangEnterpriseProductDatabaseOutDto> getListEnterpriseProductDatabase2(Map<String, Object> params);

    /**
     * 企业详情信息
     *
     * @param params
     * @return
     */
    WanFangEnterpriseProductDatabaseOutDto getOneByParams(Map<String, Object> params);
}
