package com.gl.wanfang.mapper;

import java.util.List;
import java.util.Map;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.wanfang.model.MixZswx;
import com.gl.wanfang.outdto.EasyMixZswxOutDto;
import com.gl.wanfang.outdto.MixZswxOutDto;
import org.springframework.stereotype.Component;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface MixZswxMapper extends MyMapper<MixZswx> {

	List<MixZswx> getList(Map<String, Object> params);

	/**
	 * 知识文献模糊匹配
	 * @param params
	 * @return
	 */
	List<MixZswx> getListByPage(Map<String, Object> params);

	/**
	 * @author QinYe
	 * @date 2020/6/22 9:58
	 * @description 知识文献简略搜索
	*/
	List<EasyMixZswxOutDto> getListByPage2(Map<String, Object> params);

	/**
	 * @author QinYe
	 * @date 2020/6/22 11:19
	 * @description 知识文献详情搜索
	*/
	MixZswxOutDto getOneByParams(Map<String, Object> params);

}
