package com.gl.wanfang.service;

import com.gl.wanfang.model.DateGroupByEntity;
import com.gl.wanfang.outdto.EasyWChineseConferencePaperOutDto;

import java.util.List;
import java.util.Map;

public interface IDateGroupDalService {


    /**
     * @param groupData:聚合字段
     * @param tableName；表名
     * @return
     */
    List<DateGroupByEntity> getDataGroup(String groupData, String tableName);



}
