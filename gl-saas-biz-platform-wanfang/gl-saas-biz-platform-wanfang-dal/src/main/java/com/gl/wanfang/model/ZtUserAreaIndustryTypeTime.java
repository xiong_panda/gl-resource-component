package com.gl.wanfang.model;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 
 * @author code_generator
 */
@Table(name = "zt_user_area_industry_type_time")
@Data
public class ZtUserAreaIndustryTypeTime implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键id **/
	@Id
    @ApiModelProperty(value = "主键id")
	private String id ; 

	/** 用户id **/
	@Column(name="user_id")
    @ApiModelProperty(value = "用户id")
	private String userId ; 

	/** 用户地区 **/
	@Column(name="user_area")
    @ApiModelProperty(value = "用户地区")
	private String userArea ; 

	/** 用户类型（个人、公司） **/
	@Column(name="user_type")
    @ApiModelProperty(value = "用户类型（个人、公司）")
	private String userType ; 

	/** 用户行业 **/
	@Column(name="user_industry")
    @ApiModelProperty(value = "用户行业")
	private String userIndustry ; 

	/** 使用时间（年月日） **/
	@Column(name="use_time")
    @ApiModelProperty(value = "使用时间（年月日）")
	private Date useTime ; 

	/** 资源名称 **/
	@Column(name="sources_name")
    @ApiModelProperty(value = "资源名称")
	private String sourcesName ; 

	/** 资源id **/
	@Column(name="sources_id")
    @ApiModelProperty(value = "资源id")
	private String sourcesId ; 

	/** 资源、服务区别标识 0服务 1资源 **/
	@Column(name="flag")
    @ApiModelProperty(value = "资源、服务区别标识 0服务 1资源")
	private String flag ; 

	/** 使用时间（年月） **/
	@Column(name="service_name")
    @ApiModelProperty(value = "使用时间（年月）")
	private String serviceName ; 

	/** 资源来源（模块标识） **/
	@Column(name="service_origin")
    @ApiModelProperty(value = "资源来源（模块标识）")
	private String serviceOrigin ; 

	/** 用户访问总量 **/
	@Column(name="amount")
    @ApiModelProperty(value = "用户访问总量")
	private String amount ; 


}
