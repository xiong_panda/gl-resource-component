package ${packageFacade};

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.cloud.frame.common.pojo.Page;
import ${packageModel}.${model};


/**
 * ${modelVal}暴露的服务
 * @author code_generator
 */
public interface ${facade}{

	/** 请求前缀 **/
	public static final String REQUEST_PREFIX = "/${modelVal}";

	/**
	 * 分页查询
	 * @param page
	 */
	@RequestMapping(value=REQUEST_PREFIX+"/getList",method=RequestMethod.POST)
	@ResponseBody
	public Page<${model}> getList(@RequestBody Page<${model}> page);

	/**
	 * 列表查询
	 * @param page
	 */
	@RequestMapping(value=REQUEST_PREFIX+"/findList" , method=RequestMethod.POST)
	@ResponseBody
	public Page<${model}> findList(@RequestBody Page<${model}> page);

	/**
	 * 根据条件获取一条记录
	 * @param page
	 */
	@RequestMapping(value=REQUEST_PREFIX+"/getOne" , method=RequestMethod.POST)
	@ResponseBody
	public Page<${model}> getOne(@RequestBody Page<${model}> page);

<#-- 不生成有联合主键的且根据主键操作的原子服务 -->
<#if pkAttr??>
	/**
	 * 根据主键获取
	 * @param page
	 */
	@RequestMapping(value=REQUEST_PREFIX+"/getById" , method=RequestMethod.POST)
	@ResponseBody
	public Page<${model}> getById(@RequestBody Page<${model}> page);

	/**
	 * 通过主键删除
	 * @param page
	 */
	@RequestMapping(value=REQUEST_PREFIX+"/deleteById" , method=RequestMethod.POST)
	@ResponseBody
	public Page<${model}> deleteById(@RequestBody Page<${model}> page);
</#if>

	/**
	 * 保存或更新
	 * @param page
	 */
	@RequestMapping(value=REQUEST_PREFIX+"/save" , method=RequestMethod.POST)
	@ResponseBody
	public Page<${model}> save(@RequestBody Page<${model}> page);

	//######################### 自定义服务区域 #########################
	
	
	
	
	
	
}