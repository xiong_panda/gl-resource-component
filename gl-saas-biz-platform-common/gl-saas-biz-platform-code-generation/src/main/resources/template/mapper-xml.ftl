<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${packageMapper}.${mapper}">
	<sql id="column">
	<#list mappingList as column>
		t.${column.columnName} ${column.fieldName}${column_has_next?string(",","")} 
	</#list>
	</sql>

	<!-- 根据map参数条件来查询 author:code_generator -->
	<select id="getList" parameterType="java.util.Map" resultType="${packageModel}.${model}">
		SELECT
			<include refid="column"/>
		FROM ${table} t
		<trim prefix="WHERE" prefixOverrides="AND |OR ">
		<#list mappingList as column>
			<if test="${column.fieldName} != null and ${column.fieldName} != '' ">
  				AND t.${column.columnName} = ${"#"}{${column.fieldName}} 
  			</if>
    	</#list>
		</trim>
	</select>

<!--#############################################################################-->
<!--############################### 自定义方法实现区 ################################-->
<!--#############################################################################-->





</mapper>