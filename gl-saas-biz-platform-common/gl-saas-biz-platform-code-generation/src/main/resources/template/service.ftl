package ${packageService};

import java.util.List;
import java.util.Map;
import com.framework.mybatis.service.IBaseService;
import ${packageModel}.${model};

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ${service} extends IBaseService<${model}> {

List<${model}> getListByPage(Map<String,Object> params);

}