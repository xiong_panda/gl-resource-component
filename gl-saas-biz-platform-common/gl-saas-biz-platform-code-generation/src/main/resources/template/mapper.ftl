package ${packageMapper};

import java.util.List;
import java.util.Map;
import base.mapper.MyMapper;
import com.framework.mybatis.annotation.MapperPrimary;
import ${packageModel}.${model};
import org.springframework.stereotype.Component;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface ${mapper} extends MyMapper<${model}>{

	List<${model}> getList(Map<String,Object> params);

}