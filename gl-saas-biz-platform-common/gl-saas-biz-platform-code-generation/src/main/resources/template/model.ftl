package ${packageModel};

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * ${tableRemark}
 * @author code_generator
 */
@Table(name = "${table}")
@Data
public class ${model} implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

<#-- 生成属性 -->
<#list attInfoList as attInfo>
	/** ${attInfo.comment} **/
	<#list attInfo.annotations as anno>
	${anno}
	</#list>
    @ApiModelProperty(value = "${attInfo.comment}")
	private ${attInfo.javaType} ${attInfo.fieldName} ; 

</#list>

}