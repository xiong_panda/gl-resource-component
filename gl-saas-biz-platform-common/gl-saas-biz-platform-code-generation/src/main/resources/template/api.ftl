package ${packageApi};

import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ${packageModel}.${model};
/**
 * 服务调用客户端;
 * 注意:此接口中不可写入任何代码,所暴露的自定义服务请写在相应的facade接口中
 * @author code_generator
 */
@FeignClient(value="${applicationName}")
public interface ${api} {
/** 请求前缀 **/
	public static final String REQUEST_PREFIX = "/${modelVal}";


}