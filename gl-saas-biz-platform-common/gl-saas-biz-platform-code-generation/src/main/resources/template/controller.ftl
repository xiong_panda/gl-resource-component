package ${packageController};

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import ${packageModel}.${model};
import ${packageService}.${service};
<#if api??>import ${packageApi}.${api};</#if>

/**
 * ${model}服务暴露接口的实现层
 * @author code_generator
 */
@RestController
<#if api??>
@RequestMapping(${api}.REQUEST_PREFIX)
<#else>
@RequestMapping("/${modelVal}")
</#if>
public class ${controller} <#if api??> implements ${api}</#if> {
	private static final Logger logger = LoggerFactory.getLogger(${controller}.class);

	@Autowired
	private ${service} ${serviceVal};

	
	
	
	
	
	

}