package ${packageServiceImpl};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.framework.mybatis.service.BaseService;
import ${packageService}.${service};
import ${packageMapper}.${mapper};
import ${packageModel}.${model};
import lombok.extern.slf4j.Slf4j;

/**
 * service业务处理层
 * @author code_generator
 */
@Service
@Transactional(readOnly=true)
@Slf4j
public class ${serviceImpl} extends BaseService<${model}> implements ${service}{
	@Autowired
	private ${mapper} ${mapperVal};

	@Override
	public ${model} getOne(Map<String,Object> params){
		return ${mapperVal}.getOne(params);
	}

	@Override
	public List<${model}> getListByPage(Map<String,Object> params){
		return ${mapperVal}.getList(params);
	}
	@Override
	public List<${model}> getAll(Map<String,Object> params){
		return ${mapperVal}.getAll(params);
	}

	@Override
	@Transactional
	public void save(${model} ${modelVal}){
		int inserted = ${mapperVal}.insert(${modelVal});
	}



}