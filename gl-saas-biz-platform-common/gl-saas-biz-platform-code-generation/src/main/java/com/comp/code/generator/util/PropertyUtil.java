package com.comp.code.generator.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil
{
  private static Properties prop;
  
  static
  {
    InputStream ins = null;
    try
    {
      ins = PropertyUtil.class.getResourceAsStream("/jdbc.properties");
      prop = new Properties();
      prop.load(ins);
    }
    catch (IOException e)
    {
      System.err.println("加载security.properties错误:"+e.getMessage());
      e.printStackTrace();
      try
      {
        if (ins != null) {
          ins.close();
        }
      }
      catch (IOException ex)
      {
    	  System.err.println("加载文件传输配置文件失败:"+ ex.getMessage());
        ex.printStackTrace();
      }
    }
    finally
    {
      try
      {
        if (ins != null) {
          ins.close();
        }
      }
      catch (IOException ec)
      {
    	  System.err.println("加载文件传输配置文件失败:"+ ec.getMessage());
        ec.printStackTrace();
      }
    }
  }
  
  public static String getProperty(String name)
  {
    if (prop != null) {
      return prop.getProperty(name);
    }
    return null;
  }
}

