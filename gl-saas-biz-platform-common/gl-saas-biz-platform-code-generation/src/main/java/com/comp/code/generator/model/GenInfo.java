package com.comp.code.generator.model;

import java.io.Serializable;

public class GenInfo implements Serializable{
	private static final long serialVersionUID = -4241379681770258055L;
	
	/** 代码生成路径 **/
	private String path;
	/** 代码包前缀 **/
	private String packagePre;
	/** 代码子模块包 **/
	private String subPackage;
	/** 服务的应用名称,生成api接口层是必须的参数 **/
	private String applicationName;
	/** 要生成的包,如果不想生成其中个别包可将其注释 **/
	private String[] packageNames;
	/** 需要生成的表名 **/
	private String[] tables;
	
	public GenInfo() {
	}
	
	public GenInfo(String path, String packagePre, String subPackage,String applicationName, String[] packageNames, String[] tables) {
		this.path = path;
		this.packagePre = packagePre;
		this.applicationName = applicationName;
		this.packageNames = packageNames;
		this.tables = tables;
		this.subPackage = subPackage;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getPackagePre() {
		return packagePre;
	}
	public void setPackagePre(String packagePre) {
		this.packagePre = packagePre;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String[] getPackageNames() {
		return packageNames;
	}
	public void setPackageNames(String[] packageNames) {
		this.packageNames = packageNames;
	}
	public String[] getTables() {
		return tables;
	}
	public void setTables(String[] tables) {
		this.tables = tables;
	}
	public String getSubPackage() {
		return subPackage;
	}
	public void setSubPackage(String subPackage) {
		this.subPackage = subPackage;
	}
	
}
