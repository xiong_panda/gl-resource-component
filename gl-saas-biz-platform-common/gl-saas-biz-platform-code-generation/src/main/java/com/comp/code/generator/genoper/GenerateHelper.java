package com.comp.code.generator.genoper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.comp.code.generator.util.StringUtil;

/**
 * 代码生成帮助类
 * @author HeJian
 *
 */
public class GenerateHelper {

	
	/**
	 * 将表名转换成实体类名
	 */
	public static String getEntityName(String tableName) {
		if(StringUtil.isEmpty(tableName)) throw new RuntimeException("表名称为空!");
		String[] names = tableName.split("_");
		List<String> nameList = new ArrayList<String>(Arrays.asList(names)) ;
		//去掉空字符串
		Iterator<String> iterator = nameList.iterator();
		while(iterator.hasNext()) {
			String name = iterator.next();
			if(StringUtil.isEmpty(name)) iterator.remove(); 
		}
		if(nameList==null || nameList.size()<=0 ) throw new RuntimeException("表名称有误,请检查!");
		if(nameList.size()==1) {
			//首字母大写
			return StringUtil.toFirstUpper(nameList.get(0)).trim();
		}
		String entityName=""; 
		for (String name : nameList) {
			entityName = entityName + StringUtil.initcap(name);
		}
		return entityName;
	}
	
	/**
	 * 将数据库表的列名转换成java类的属性名
	 */
	public static String getAttrName(String colName) {
		if(StringUtil.isEmpty(colName)) throw new RuntimeException("表列名为空!");
		String[] names = colName.split("_");
		List<String> nameList = new ArrayList<String>(Arrays.asList(names)) ;
		//去掉空字符串
		Iterator<String> iterator = nameList.iterator();
		while(iterator.hasNext()) {
			String name = iterator.next();
			if(StringUtil.isEmpty(name)) iterator.remove(); 
		}
		if(nameList==null || nameList.size()<=0)  throw new RuntimeException("列名称:"+colName+"有误,请检查!");
		if(nameList.size()==1) { 
			//如果只有一个单词则全部小写
			return nameList.get(0).toLowerCase().trim();
		}
		
		String attrName = "";
		for (int i = 0; i < nameList.size(); i++) {
			String attStr = nameList.get(i).trim();
			if (i == 0) {
				attrName = attrName + attStr.toLowerCase();
			} else {
				attrName = attrName + StringUtil.initcap(attStr);
			}
		}
		return attrName;
	}
	
	/**
	 * 功能：获得列的数据类型  数据库类型转java类型
	 */
	public static String sqlType2JavaType(String sqlType) {
		if (sqlType.equalsIgnoreCase("binary_double")||sqlType.equalsIgnoreCase("double")) {
			return "Double";
		} else if (sqlType.equalsIgnoreCase("binary_float")||sqlType.equalsIgnoreCase("float")) {
			return "float";
		} else if (sqlType.equalsIgnoreCase("blob")) {
			return "byte[]";
		} else if (sqlType.equalsIgnoreCase("int") || sqlType.equalsIgnoreCase("integer")||sqlType.equalsIgnoreCase("smallint")) {
			return "Integer";
		} else if (sqlType.equalsIgnoreCase("char") || sqlType.equalsIgnoreCase("nvarchar2") || sqlType.equalsIgnoreCase("varchar2")||sqlType.equalsIgnoreCase("varchar")||sqlType.equalsIgnoreCase("text")||sqlType.equalsIgnoreCase("longtext")||sqlType.equalsIgnoreCase("mediumtext")) {
			return "String";
		} else if (sqlType.equalsIgnoreCase("date") || sqlType.equalsIgnoreCase("datetime") || sqlType.equalsIgnoreCase("timestamp") || sqlType.equalsIgnoreCase("timestamp with local time zone") || sqlType.equalsIgnoreCase("timestamp with time zone")) {
			return "Date";
		} else if (sqlType.equalsIgnoreCase("number")) {
			return "Double";
		}else if (sqlType.equalsIgnoreCase("bigint")) {
			return "Long";
		}else if (sqlType.equalsIgnoreCase("decimal")) {
			return "BigDecimal";
		}else if(sqlType.equalsIgnoreCase("tinyint")){
			return "byte";
		}else if(sqlType.equalsIgnoreCase("bit")){
			return "Boolean";
		}
		return "未添加映射";
	}
	
	/**
	 * 解析表注释
	 * @param all
	 * @return
	 */
	public static String getTableCommet(String infoStr) {
		if(infoStr==null || infoStr.trim().length()==0) return "";
		
        String comment = null;
        int index = infoStr.indexOf("COMMENT='");
        if (index < 0) {
            return "";
        }
        comment = infoStr.substring(index + 9);
        comment = comment.substring(0, comment.length() - 1);
        try {
            comment = new String(comment.getBytes("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comment;
    }
	
	/**
	 * java包名称转路径
	 * @param packageName
	 * @return
	 */
	public static String packageToPath(String packageName) {
		if(StringUtil.isEmpty(packageName)) throw new RuntimeException("类的包名称为空!");
		String rs = packageName.replaceAll("\\.", "/");
		return rs;
	}
	
	public static void main(String[] args) {
		String str = getAttrName("_a_ode_");
		System.out.println(str);
	}
}
