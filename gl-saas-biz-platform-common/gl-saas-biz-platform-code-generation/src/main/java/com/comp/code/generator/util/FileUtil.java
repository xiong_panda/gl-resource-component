package com.comp.code.generator.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileUtil {

	/**
	 * 将字符串写文件出去
	 * @param content 文件内容
	 * @param fileName 文件名称
	 * @param path 文件生成的路径
	 */
	public static void writeFile(String content, String fileName ,String path) {
		PrintWriter pw=null;
		try {
			File directory = new File(path);
			//判断文件夹是否存在,不存在则创建
			if(!directory.exists()){
				directory.mkdirs();
			}
			String outputPath = path + "/" + fileName;
			pw = new PrintWriter(new FileWriter(outputPath));
			pw.println(content);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(pw!=null){
				pw.flush();
				pw.close();
			}
		}
	}
	
	
}
