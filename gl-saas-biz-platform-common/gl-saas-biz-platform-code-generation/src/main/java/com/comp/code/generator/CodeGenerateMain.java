package com.comp.code.generator;

import com.comp.code.generator.genoper.GenerateHandler;
import com.comp.code.generator.model.GenInfo;

/**
 * 基础代码生成器
 * @author HeJian
 *
 */
public class CodeGenerateMain {

	/**
	 * 运行入口
	 * @param args
	 */
	public static void main(String[] args) {
		//代码生成路径
		final String path = "d:/code";
		//包前缀
		final String packagePre = "com.gl";
		//子模块包 用法:一级:prod, 多级:prod.channo
		final String subPackage = "base";
		//服务的应用名称,生成api接口层是必须的参数
		final String applicationName = "gl-saas-biz-parts";
		//要生成的包,如果不想生成其中个别包可将其注释
		final String packageNames[] = {
				"api",			//客户端调用的api接口层,包含api和facade的生成
				"controller",	//api暴露接口的实现层
				"service",		//service接口以及service实现impl
				"mapper",		//mapper接口
				"model",		//表模型
		};
		//需要生成的表名
		final String[] tables = {
				"BASEIC_COSTOMER",
		};

		//组装信息
		GenInfo genInfo = new GenInfo(path, packagePre,subPackage, applicationName, packageNames, tables);
		GenerateHandler handler = new GenerateHandler();
		//生成代码
		handler.generate(genInfo);
	}

}
