package com.comp.code.generator.util;

/**
 * 字符串工具类
 * @author HeJian
 *
 */
public class StringUtil {

	/**
	 * 将字符串的首字母改成小写
	 * @param str
	 * @return
	 */
	public static String toFirstLower(String str){
        char[] cs=str.toCharArray();
        cs[0]= new String( new char[]{cs[0]}).toLowerCase().charAt(0);
        return String.valueOf(cs);
	}
	
	 /**
	  * 首字母大写
	  * @param name
	  * @return
	  */
    public static String toFirstUpper(String str) {
        char[] cs=str.toCharArray();
        cs[0]= new String( new char[]{cs[0]}).toUpperCase().charAt(0);
        return String.valueOf(cs);
    }
	
	/**
	 * 功能：将输入字符串的首字母改成大写(其余为小写)
	 */
	public static String initcap(String str) {
		str = str.toLowerCase();
		char[] ch = str.toCharArray();
		if (ch[0] >= 'a' && ch[0] <= 'z') {
			ch[0] = (char) (ch[0] - 32);
		}
		return new String(ch);
	}
	
	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str==null || str.trim().length()==0;
	}
	
	public static void main(String[] args) {
		String str = toFirstLower("JelloWorld");
		System.out.println(str);
	}
	
}
