package com.comp.code.generator.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * freemark工具类
 * @author hejian
 *
 */
public class FreeMarkUtil {
	/**
     * 初始化配置
     */
    private static final Configuration CONFIGURATION = new Configuration() {
        {
            setClassForTemplateLoading(FreeMarkUtil.class, "/");
        }
    };

    /**
     * 执行
     * @param config                  配置信息
     * @param classPathModel    类路径的模板
     * @param encoding              编码
     * @param root                      map
     * @return                              填充后的文本
     * @throws IOException          文件操作异常
     * @throws TemplateException    填充异常
     */
    public static String execute(String classPathModel, String encoding, Object root) throws IOException, TemplateException {
        BufferedWriter writer = null;
        try {
            Template tp = CONFIGURATION.getTemplate(classPathModel, encoding);
            StringWriter stringWriter = new StringWriter();
            writer = new BufferedWriter(stringWriter);
            tp.setEncoding(encoding);
            tp.process(root, writer);
            return stringWriter.toString();
        } finally {
            writer.flush();
            writer.close();
        }
    }

    /**
     * 执行
     * @param config            配置信息
     * @param modelName         类路径的模板
     * @param encoding          编码
     * @param root              map
     * @param outputStream      输出流
     * @throws TemplateException    填充异常
     * @throws IOException      文件操作异常
     */
    public static void execute(String classPathModel,String encoding, Object root, FileOutputStream outputStream)throws TemplateException, IOException {
        Writer out = null;
        try {
            // 合并数据模型与模板
            out = new OutputStreamWriter(outputStream, encoding);
            // 获取模板,并设置编码方式，这个编码必须要与页面中的编码格式一致
            CONFIGURATION.getTemplate(classPathModel, encoding).process(root, out);
        } finally {
            try {
                out.flush();
                out.close();
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                System.out.println("关闭输出流失败！");
                e.printStackTrace();
            }
        }

    }

    /**
     * 执行
     * @param config                    配置信息
     * @param name                      类路径的模板
     * @param encoding                  编码
     * @param root                      map
     * @param file                      保存至文件
     * @throws TemplateException        填充异常
     * @throws IOException              文件操作异常
     */
    public static void execute(String classPathModel,String encoding, Object root, File file)throws TemplateException, IOException {
        execute(classPathModel, encoding, root, new FileOutputStream(file));
    }

    /**
     * 执行
     * @param config                    配置信息
     * @param modelName              类路径的模板
     * @param encoding                  编码
     * @param root                      map
     * @param filePathName              文件保存路径
     * @throws TemplateException        填充异常
     * @throws IOException              文件操作异常
     */
    public static void execute(String classPathModel, String encoding, Object root, String filePathName)throws TemplateException, IOException {
        execute(classPathModel, encoding, root, new FileOutputStream(filePathName));
    }

    /**
     * 执行 以utf8编码获取；  
     * @param modelName             类路径的模板
     * @param encoding              编码
     * @param root                  map
     * @return                      填充后的文本
     * @throws IOException          文件操作异常
     * @throws TemplateException    填充异常
     */
    public static String executeUTF8(String modelName,Object root) {
    	String encoding = "UTF-8";
    	String dest = null;
        try {
        	dest =  execute(modelName, encoding, root);
		} catch (Exception e) {
			System.err.println("读取模板："+modelName+"失败");
			throw new RuntimeException(e);
		}
        return dest;
    }
    
    /**
     * 执行 以utf8编码获取；  
     * @param modelName             类路径的模板
     * @param encoding              编码
     * @param root                  map
     * @return                      填充后的文本
     * @throws IOException          文件操作异常
     * @throws TemplateException    填充异常
     */
    public static String executeGBK(String modelName,Object root)  {
    	String encoding = "GBK";
    	String dest = null;
        try {
			dest =  execute(modelName, encoding, root);
		} catch (Exception e) {
			System.err.println("读取模板："+modelName+"失败");
			throw new RuntimeException(e);
		}
        return dest;
    }
    
    
}
