package com.gl.common.autoparts.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class ResourceOutTreeDto extends TreeNode {

    /**
     * 名称
     **/
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 应用类型(dictionary:apple_type)
     **/
    @ApiModelProperty(value = "应用类型(dictionary:apple_type)")
    private String appleType;

    /**
     * 类型（dictionary：resource_type）
     **/
    @ApiModelProperty(value = "类型（dictionary：resource_type）")
    private String resourceCode;

    /**
     * URL
     **/
    @ApiModelProperty(value = "path")
    private String path;

    /**
     * 图标
     **/
    @ApiModelProperty(value = "图标")
    private String iconCls;

    @ApiModelProperty(value = "资源级别")
    private int  level;

    /**
     * 用户类别
     */
    @ApiModelProperty(notes = "用户类别")
    private String userType;

    /**
     * 协作关系（用户类型为盟员用，外键关联BASIC_COLLABORATION）
     */
    @ApiModelProperty(value = "协作关系（用户类型为盟员用，外键关联BASIC_COLLABORATION）")
    private String collaborationId;

    @ApiModelProperty(value = "协作关系名称（用户类型为盟员用，外键关联BASIC_COLLABORATION）")
    private String collaborationName;

    /**
     * 状态（数据字典：use）
     **/
    @ApiModelProperty(value = "状态（数据字典：use）")
    private String status;

    /**
     * 序号
     **/
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /**
     * 简介
     **/
    @ApiModelProperty(value = "简介")
    private String synopsis;

    /**
     * 备注
     **/
    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "权限标识（多个权限标识以'',''分隔）")
    private String permissionFlag;

    /**
     * 前台菜单渲染用
     **/
    @ApiModelProperty(value = "前台菜单渲染用")
    private String component;

    /**
     * 前台菜单渲染用
     **/
    @ApiModelProperty(value = "前台菜单渲染用")
    private String leaf;

    /**
     * 前台菜单渲染用
     **/
    @ApiModelProperty(value = "前台菜单渲染用")
    private String hidden;

    @ApiModelProperty(notes = "父级名称")
    private String parentName;

    @ApiModelProperty(notes = "前段展示名字")
    private String label;

    @ApiModelProperty(notes = "是否选择中")
    private Boolean checked=false;

}
