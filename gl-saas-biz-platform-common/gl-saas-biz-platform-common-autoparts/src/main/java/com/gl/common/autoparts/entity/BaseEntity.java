package com.gl.common.autoparts.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 基类实体：实体类都继承自此类
 * @author liaoqisheng
 * @date 2019年10月12日
 */
@Data
public abstract class BaseEntity implements Serializable {
    /** 主键ID **/
    @Id
    private String id ;

    @Column(name="deleted")
    private String deleted ;
    /** 是否启用（1启用，0禁用） **/
    @Column(name="enable")
    private String enable ;

    /** 是否删除（1是，0否） **/
    /** 创建人id **/
    @Column(name="create_id")
    private String createId ;

    /** 创建人 **/
    @Column(name="create_name")
    private String createName ;

    /** 创建时间 **/
    @Column(name="create_time")
    private Date createTime ;

    /** 最后修改人id **/
    @Column(name="last_modify_id")
    private String lastModifyId ;

    /** 最后修改人 **/
    @Column(name="last_modify_name")
    private String lastModifyName ;

    /** 最后修改时间 **/
    @Column(name="last_modify_time")
    private Date lastModifyTime ;

    /** 版本号 **/
    @Column(name="version")
    private String version ;

    /** 预留字段 **/
    @Column(name="extend_field1")
    private String extendField1 ;

    /** 传输字段 **/
    @Column(name="transmission")
    private String transmission ;

}
