package com.gl.common.autoparts.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class CodeUtils {
    private static final  String[] numbers={"0","1","2","3","4","5","6","7","8","9"};


    public static String radomCode(){
        StringBuffer buffer=new StringBuffer();
        for (int i = 0; i <6 ; i++) {
            Random random=new Random();
            int nextInt = random.nextInt(numbers.length);
            buffer.append(numbers[nextInt]);
        }
        return buffer.toString();
    }

    /**
     * 生成订单
     * @param type 出库:CK,入库:RK,销售：XS
     * @return
     */
    public static String orderCode(String type){
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        df.setLenient(false);
        StringBuffer buffer=new StringBuffer(type);
        buffer.append(df.format(date));
            Random random=new Random();
            int nextInt = random.nextInt(numbers.length);
            buffer.append(numbers[nextInt]);
        return buffer.toString();
    }

}
