package com.gl.common.autoparts.enums;

import lombok.Getter;

/**
 * 是否枚举
 * @author lqs
 * @date 2019/11/29 15:01
 */
@Getter
public enum TrueOrFalseEnum {

    TRUE("1", "是"),
    FALSE("9", "否");

    /**
     * 值
     */
    private String value;
    /**
     * 描述
     */
    private String description;


    TrueOrFalseEnum(String value, String description) {
        this.value = value;
        this.description = description;
    }
}
