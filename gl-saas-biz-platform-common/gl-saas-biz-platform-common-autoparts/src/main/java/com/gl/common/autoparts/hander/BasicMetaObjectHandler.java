package com.gl.common.autoparts.hander;


import com.gl.basis.common.util.StringUtils;
import com.gl.common.autoparts.entity.BaseEntity;


import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author zww
 * @date 2019年11月27日10:09:53
 */
@Service
public class BasicMetaObjectHandler {


    public <T extends BaseEntity> void insertFill(Object root) {
        if (root == null) {
            return;
        }
        T t = (T) root;
        t.setCreateId(t.getLastModifyId());
        if (null == t.getEnable() || "".equals(t.getEnable().trim())) {
            t.setEnable("1");
        }
        t.setDeleted("0");
        t.setCreateName(t.getLastModifyName());
        t.setCreateTime(new Date());
        if(StringUtils.isEmpty(t.getId())){
            synchronized (root) {
                t.setId(UUID.randomUUID().toString().replace("-", ""));
            }
        }
        t.setLastModifyTime(new Date());
        t.setTransmission("0");
        t.setVersion(String.valueOf(System.currentTimeMillis()));
    }

    public void insertFills(List<? extends BaseEntity> srcList) {
        if (srcList == null) {
            return;
        }
        srcList.forEach(data -> {
            data.setCreateId(data.getLastModifyId());
            if (null == data.getEnable() || "".equals(data.getEnable().trim())) {
                data.setEnable("1");
            }
            data.setEnable("1");
            data.setDeleted("0");
            data.setCreateName(data.getLastModifyName());
            data.setCreateTime(new Date());
            if(StringUtils.isEmpty(data.getId())){
                data.setId(UUID.randomUUID().toString().replace("-", ""));

            }
            data.setLastModifyTime(new Date());
            data.setTransmission("0");
            data.setVersion(String.valueOf(System.currentTimeMillis()));
        });


    }

    public <T extends BaseEntity> void updateFill(Object root) {
        if (root == null) {
            return;
        }
        T t = (T) root;
        t.setLastModifyTime(new Date());
        t.setTransmission("0");
        t.setVersion(String.valueOf(System.currentTimeMillis()));
    }
}
