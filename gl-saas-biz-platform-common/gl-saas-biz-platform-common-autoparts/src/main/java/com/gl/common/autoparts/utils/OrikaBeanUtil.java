package com.gl.common.autoparts.utils;

import com.gl.basis.common.pojo.Page;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

public class OrikaBeanUtil {
    public static <T> List<T> batchTransform(final Class<T> clazz, List<? extends Object> srcList) {
        if (CollectionUtils.isEmpty(srcList)) {
            return Collections.emptyList();
        }
        MapperFactory defaultMapperFacory = new DefaultMapperFactory.Builder().build();
        List<T> list = defaultMapperFacory.getMapperFacade().mapAsList(srcList, clazz);
        return list;
    }

    public static <T> List<T>  batchTransformPage(final Class<T> clazz, List<? extends Object> srcList, Page<T> page) {
        if (CollectionUtils.isEmpty(srcList)) {
            return Collections.emptyList();
        }
        MapperFactory defaultMapperFacory = new DefaultMapperFactory.Builder().build();
        List<T> list = defaultMapperFacory.getMapperFacade().mapAsList(srcList, clazz);
        if (page.getIsPage()) {
            com.github.pagehelper.Page<?> pager = (com.github.pagehelper.Page<?>) srcList;
            page.setTotalSize(pager.getTotal());
        }
        page.setResult(list);
        return list;
    }
}
