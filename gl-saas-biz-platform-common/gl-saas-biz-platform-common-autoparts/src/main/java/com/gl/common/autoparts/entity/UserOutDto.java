package com.gl.common.autoparts.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 用户
 * @author Liao Qisheng
 * @date 2019/10/23 11:08
 */
@Data
public class UserOutDto {

    @ApiModelProperty(notes = "主键ID")
    private String id;

    @ApiModelProperty(notes = "账号")
    private String accountNumber;

    @ApiModelProperty(notes = "密码")
    private String password;

    @ApiModelProperty(notes = "绑定手机号")
    private String bindingPhoneNumber;

    @ApiModelProperty(notes = "姓名")
    private String name;

    @ApiModelProperty(notes = "联系电话")
    private String contactTel;

    @ApiModelProperty(notes = "联系地址")
    private String contactAddress;

    @ApiModelProperty(notes = "备注")
    private String remark;

    @ApiModelProperty(notes = "所属企业")
    private String enterpriseId;

    @ApiModelProperty(notes = "是否企业超级管理员（1是，0否）")
    private String enterpriseAdministrator;

    @ApiModelProperty(notes = "删除标记")
    private String enable;

    @ApiModelProperty(notes = "删除标记")
    private String deleted;

    @ApiModelProperty(notes = "创建人Id")
    private String createId;

    @ApiModelProperty(notes = "创建人")
    private String createName;

    @ApiModelProperty(notes = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(notes = "最后修改人id")
    private String lastModifyId;

    @ApiModelProperty(notes = "最后修改人")
    private String lastModifyName;

    @ApiModelProperty(notes = "最后修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifyTime;

    @ApiModelProperty(notes = "版本号")
    private String version;

    @ApiModelProperty(notes = "部门名字")
    private String departmentName;

    @ApiModelProperty(notes = "角色名字")
    private String roleName;

    @ApiModelProperty(notes = "联盟")
    private List<LinkedHashMap<String, Object>> alliances;

    @ApiModelProperty(notes = "部门")
    private List<String> departments;

    @ApiModelProperty(notes = "角色")
    private List<String> roles;

    @ApiModelProperty(notes = "资源")
    private List<ResourceOutTreeDto> resources;

    @ApiModelProperty(notes = "当前联盟id")
    private String allianceId;

    @ApiModelProperty(notes = "登录用户类型")
    private String userType;
    @ApiModelProperty(notes = "企业名称")
    private String enterpriseName;
}
