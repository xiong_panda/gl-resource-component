package com.gl.common.autoparts.utils;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.constant.CacheConstants;
import com.gl.basis.util.RedisUtil;
import com.gl.common.autoparts.entity.UserOutDto;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class UserUtils {
    @Resource
    RedisUtil redisUtil;

    /**
     * 获取当前用户登录信息
     *
     * @return
     */
    public UserOutDto getUserInfo() {
        // 当前认证通过的用户身份
        // Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 用户身份
        //Object principal = authentication.getPrincipal();
        //if(null == principal) {
        throw new BusinessException("用户信息不存在");
    }

    String accountNumber;
       /*if(principal instanceof org.springframework.security.core.userdetails.UserDetails){
            UserDetails userDetails = (UserDetails) principal;
            accountNumber = userDetails.getUsername();
        }else{
           accountNumber = principal.toString();
        }
       String json= (String) redisUtil.get(CacheConstants.USER_NAME+accountNumber);
        UserOutDto oauthUser;
        if (json==null){
           oauthUser=new UserOutDto();
           oauthUser.setName("UNKOWN");
            oauthUser.setAllianceId("UNKOWN");
            oauthUser.setId("UNKOWN");
            return  oauthUser;
        }
        oauthUser = JSON.parseObject(json,UserOutDto.class);
        return  oauthUser;*/



    /**
     * 获取当前用户登录信息
     * @return
     */
   public String getUserInfoAliianceId(){
        if (getUserInfo().getAllianceId().equals("UNKOWN")){
            throw new BusinessException("请选择联盟");
        }
      return  getUserInfo().getAllianceId();
    }

}
