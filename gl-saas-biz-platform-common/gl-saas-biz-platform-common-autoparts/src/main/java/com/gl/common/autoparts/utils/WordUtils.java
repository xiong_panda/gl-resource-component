package com.gl.common.autoparts.utils;

import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.xwpf.usermodel.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

public class WordUtils {

    private static final Integer WORD_TO_PDF_OPERAND=17;

    /**
     * 根据模板生成新的word
     * 判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为填充
     *
     * @param inputUrl  模板存放地址
     * @param outputUrl 新文档存放地址
     * @param textMap   需要替换的信息
     * @param tableList 需要插入的信息
     * @return
     */
    public  boolean changeWord(String inputUrl, String outputUrl, Map<String, String> textMap, List<String[]> tableList) {
        //模板转换默认成功
        boolean changeFlag = true;
        try {
            //获取docx解析对象
            XWPFDocument document = new XWPFDocument(POIXMLDocument.openPackage(inputUrl));
            //解析替换文本段落对象
            WordUtils.changeText(document, textMap);
            //解析替换表格对象
            WordUtils.changeTable(document, textMap, tableList);
            //生成新的word
            File file = new File(outputUrl);
            FileOutputStream stream = new FileOutputStream(file);
            document.write(stream);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
            changeFlag = false;
        }
        return changeFlag;

    }

    /**
     * 替换段落文本
     *
     * @param document docx解析对象
     * @param textMap  需要替换的信息集合
     */
    private static void changeText(XWPFDocument document, Map<String, String> textMap) {
        //获取段落集合
        List<XWPFParagraph> paragraphs = document.getParagraphs();

        for (XWPFParagraph paragraph : paragraphs) {
            //判断此段落是否需要替换
            String text = paragraph.getText();
            if (checkText(text)) {
                List<XWPFRun> runs = paragraph.getRuns();
                for (XWPFRun run : runs) {
                    //替换模板的原位置
                    run.setText(changeValue(run.toString(), textMap), 0);
                }

            }
        }

    }

    /**
     * 替换表格对象方法
     *
     * @param document  doc解析对象
     * @param textMap   需要替换的信息集合
     * @param tableList 需要插入的表格信息集合
     */
    private static void changeTable(XWPFDocument document, Map<String, String> textMap, List<String[]> tableList) {
        //获取表格对象对象
        List<XWPFTable> tables = document.getTables();
        for (int i = 0; i < tables.size(); i++) {
            //只处理行数大于等于2的表格，切不循环表头
            XWPFTable table = tables.get(i);
            if (table.getRows().size() > 1) {
                //判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
                if (checkText(table.getText())) {
                    List<XWPFTableRow> rows = table.getRows();
                    //遍历表格，并替换模板
                    eachTable(rows,textMap);
                } else {
                    isnertTable(table,tableList);
                }
            }

        }


    }

    private static boolean checkText(String text) {
        boolean check = false;
        if (text.indexOf("$") != -1) {
            check = true;
        }
        return check;
    }

    private static String changeValue(String value, Map<String, String> textMap) {
        Set<Entry<String, String>> textSets = textMap.entrySet();

        for (Entry<String, String> textSet : textSets) {
            //匹配模板替换 格式 ${key}
            String key = "${" + textSet.getKey() + "}";
            if (value.indexOf(key) != -1) {
                value = textSet.getValue();
            }
        }
        //模板未匹配到区域替换为空
        if (checkText(value)) {
            value = "";
        }
        return value;
    }

    private static void eachTable(List<XWPFTableRow> rows, Map<String, String> textMap) {
        for (XWPFTableRow row : rows){
            List<XWPFTableCell> cells=row.getTableCells();
            for (XWPFTableCell cell : cells){
                //判断单元格是否需要替换
                if (checkText(cell.getText())){
                    List<XWPFParagraph> paragraphs=cell.getParagraphs();
                    for (XWPFParagraph paragraph : paragraphs){
                        List<XWPFRun> runs=paragraph.getRuns();
                        for (XWPFRun run : runs){
                            run.setText(changeValue(run.toString(),textMap),0);
                        }
                    }
                }
            }

        }
    }

    /**
     * 为表格插入数据，行数不够就添加新行
     * @param table 需要插入数据的表格
     * @param tableList 插入数据的集合
     */
    private static void isnertTable(XWPFTable table,List<String[]> tableList) {
        //创建行，根据需要插入的数据添加新行 不处理表头
        for (int i = 1; i <tableList.size() ; i++) {
            XWPFTableRow row=table.createRow();
        }
        //遍历表格插入数据
        List<XWPFTableRow> rows=table.getRows();
        for (int i = 1; i <rows.size() ; i++) {
            XWPFTableRow newRow=table.getRow(i);
            List<XWPFTableCell> cells=newRow.getTableCells();
            for (int j = 0; j <cells.size() ; j++) {
                XWPFTableCell cell =cells.get(j);
                if (tableList.size()>(i-1)){
                    cell.setText(tableList.get(i-1)[j]);
                }
            }
        }

    }
}
