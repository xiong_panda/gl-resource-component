package com.gl.biz.city;

/**
 * 公共错误码
 */
public enum CityCommonErroCode {
    SUCCESS("0", "成功"),
    JSON_PARSE_ERRO("1", "JSON解析异常"),
    PARAMS_NULL_ERRO("2", "参数为空"),
    PARAMS_PARSE_ERRO("3", "参数格式不正确，请以键值对形式");

    private String code;

    private String msg;

    CityCommonErroCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
