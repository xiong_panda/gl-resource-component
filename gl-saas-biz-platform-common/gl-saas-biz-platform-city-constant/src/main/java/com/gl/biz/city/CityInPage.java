package com.gl.biz.city;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.gl.basis.common.util.BeanUtil;
import lombok.Data;

import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 入参
 */
@Data
public class CityInPage implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 第几页，默认1
     */
    private Integer pageNo = 1;
    private Integer pageSize = 10;//每页显示数
    private String order;
    private String orderDesc = "asc";
    private Long totalSize;//总记录数
    private static final String __PAGENO__ = "__pageNo__";
    private static final String __PAGESIZE__ = "__pageSize__";
    private static final String __ORDERBY__ = "__orderBy__";

    /**
     * 查询条件放于此
     */
    private final Map<String, Object> params = new HashMap<>(0);

    public Map<String, Object> getParams() {
        return this.params;
    }

    public void setParams(Map<String, Object> params) {
        if (params == null || params.isEmpty()) {
            return;
        }
        if (!params.containsKey(__PAGESIZE__)) {
            params.put(__PAGESIZE__, this.getPageSize());
        }
        if (!params.containsKey(__PAGENO__)) {
            params.put(__PAGENO__, this.getPageNo());
        }
        this.params.putAll(params);
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
        if (order != null && order.trim().length() != 0) {
            this.params.put(__ORDERBY__, order + " " + this.getOrderDesc());
        }
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
        this.setOrder(order);
    }

    /**
     * 后台强制分页
     *
     * @param pageNo 当前页
     * @param order  排序字段 数据库同名
     */
    public void startPage(Integer pageNo, String order) {
        this.setPageNo(pageNo);
        this.setOrder(order);
    }

    /**
     * 后台强制分页
     *
     * @param pageNo    当前页
     * @param order     排序字段 数据库同名
     * @param orderDesc 排序顺序
     */
    public void startPage(Integer pageNo, String order, String orderDesc) {
        this.setPageNo(pageNo);
        this.orderBy(order, orderDesc);
    }

    /**
     * 后台强制分页
     *
     * @param pageNo   当前页
     * @param pageSize 分页条数
     */
    public void startPage(Integer pageNo, Integer pageSize) {
        this.setPageNo(pageNo);
        this.setPageSize(pageSize);
    }

    /**
     * 后台强制分页
     *
     * @param pageNo   当前页
     * @param pageSize 分页条数
     * @param order    排序字段 数据库同名
     */
    public void startPage(Integer pageNo, Integer pageSize, String order) {
        this.setPageNo(pageNo);
        this.setPageSize(pageSize);
        this.orderBy(order);
    }

    /**
     * 后台强制分页
     *
     * @param pageNo    当前页
     * @param pageSize  分页条数
     * @param order     排序字段 数据库同名
     * @param orderDesc 排序顺序
     */
    public void startPage(Integer pageNo, Integer pageSize, String order, String orderDesc) {
        this.setPageNo(pageNo);
        this.setPageSize(pageSize);
        this.orderBy(order, orderDesc);
    }

    /**
     * 排序
     *
     * @param order 排序字段 数据库同名
     */
    public void orderBy(String order) {
        this.setOrder(order);
    }

    /**
     * 排序
     *
     * @param order     排序字段 数据库同名
     * @param orderDesc 排序顺序
     */
    public void orderBy(String order, String orderDesc) {
        this.setOrder(order);
        this.setOrderDesc(orderDesc);
    }


    public static CityInPage convertParam(Map<String, Object> params) {
        CityInPage pageParam = new CityInPage();
        pageParam.setParams(params);
        return pageParam;
    }

    public static CityInPage convertMapParam(Object params) {
        CityInPage pageParam = new CityInPage();
        if (!(params instanceof Map)) {
            pageParam.setParams(BeanUtil.transBean2Map(params));
        } else {
            pageParam.setParams((Map) params);
        }

        return pageParam;
    }

    private String unescapeJava(String str) {
        Writer out = new StringWriter();
        try {
            if (str != null) {
                int sz = str.length();
                StringBuilder unicode = new StringBuilder(4);
                boolean hadSlash = false;
                boolean inUnicode = false;

                for (int i = 0; i < sz; ++i) {
                    char ch = str.charAt(i);
                    if (inUnicode) {
                        unicode.append(ch);
                        if (unicode.length() == 4) {
                            try {
                                int nfe = Integer.parseInt(unicode.toString(), 16);
                                out.write((char) nfe);
                                unicode.setLength(0);
                                inUnicode = false;
                                hadSlash = false;
                            } catch (NumberFormatException var9) {
                            }
                        }
                    } else if (hadSlash) {
                        hadSlash = false;
                        switch (ch) {
                            case '\"':
                                out.write(34);
                                break;
                            case '\'':
                                out.write(39);
                                break;
                            case '\\':
                                out.write(92);
                                break;
                            case 'b':
                                out.write(8);
                                break;
                            case 'f':
                                out.write(12);
                                break;
                            case 'n':
                                out.write(10);
                                break;
                            case 'r':
                                out.write(13);
                                break;
                            case 't':
                                out.write(9);
                                break;
                            case 'u':
                                inUnicode = true;
                                break;
                            default:
                                out.write(ch);
                        }
                    } else if (ch == 92) {
                        hadSlash = true;
                    } else {
                        out.write(ch);
                    }
                }

                if (hadSlash) {
                    out.write(92);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return out.toString();
    }

}