package com.gl.biz.city;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.List;

@Data
public class CityOutResult<T> {



    /**
     * 数据总条数
     */
    private long totalSize;


    /**
     * 返回数据放于此
     */
    private Object result;



    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }


    public Object gtResult() {
        return result;
    }


    //    public void setResult(Object obj) {
//        this.result = obj;
//        if (obj != null && obj instanceof List && obj instanceof com.github.pagehelper.Page) {
//            com.github.pagehelper.Page<T> pager = (com.github.pagehelper.Page<T>) obj;
//            this.setTotalSize(pager.getTotal());
//        }
//    }
    public void setResult(Object obj) {
        this.result = obj;
        if (obj != null && obj instanceof List && obj instanceof com.github.pagehelper.Page) {
            com.github.pagehelper.Page<T> pager = (com.github.pagehelper.Page<T>) obj;
            this.setTotalSize(pager.getTotal());
        }
    }

    public String gtResultString() {
        if (result != null) {
            if (result instanceof String) {
                return (String) result;
            } else {
                return JSON.toJSONString(result);
            }
        } else {
            return null;
        }
    }
}
