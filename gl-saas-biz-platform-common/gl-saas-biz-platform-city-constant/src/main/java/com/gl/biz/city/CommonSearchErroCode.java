package com.gl.biz.city;

/**
 * 城市群公共检索异常
 */
public enum CommonSearchErroCode {
    AUTHOR_SEARCH_ERRO("20", "作者检索异常"),
    AUTHOR_DETAIL_ERRO("21", "作者详情异常"),
    LAW_SEARCH_ERRO("20", "法律法规检索异常"),
    LAW_DETAIL_ERRO("21", "法律法规详情异常"),
    EXPERT_SEARCH_ERRO("20", "专家检索异常"),
    EXPERT_DETAIL_ERRO("21", "专家详详情异常"),
    ORGAN_SEARCH_ERRO("20", "机构检索异常"),
    ORGAN_DETAIL_ERRO("21", "机构详情异常"),
    CPATENT_SEARCH_ERRO("20", "中文专利检索异常"),
    CPATENT_DETAIL_ERRO("21", "中文专利详详情异常"),
    COMPANY_SEARCH_ERRO("30", "企业检索异常"),
    COMPANY__DETAIL_ERRO("21", "企业法规详情异常"),
    ACHIEVEMENT_SEARCH_ERRO("100", "科技成果检索异常"),
    ACHIEVEMENT_DEATAIL_ERRO("101", "科技成果详情异常"),

    CHINESE_JOURNAL_PAPERS_SEARCH_ERRO("90", "中文期刊检索异常"),
    OVERSEAS_PATENT_SEARCH_ERRO("100","海外专利检索异常"),
    CHINESE_PAPER_OA_SEARCH_ERRO("110","中文OA检索异常"),
    CHINESE_CONFERENCE_SEARCH_ERRO("120","中文会议检索异常"),
    EN_OA_SEARCH_ERRO("130","外文OA检索异常"),
    HIGHER_LEARNING_UNIVERSITY_SEARCH_ERRO("140","高等院校检索异常"),
    CHLITERATURE_SEARCH_ERRO("150","知识文献检索异常");

    private String code;

    private String msg;

    CommonSearchErroCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
