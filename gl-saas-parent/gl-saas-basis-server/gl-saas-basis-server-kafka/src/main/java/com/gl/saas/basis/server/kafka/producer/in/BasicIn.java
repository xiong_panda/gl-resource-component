package com.gl.saas.basis.server.kafka.producer.in;

import lombok.Data;

/**
 * @author zww
 * @date 2019年10月25日14:46:19
 */
@Data
public class BasicIn {
    private String key;
    private String topic;
    private Object data;

}
