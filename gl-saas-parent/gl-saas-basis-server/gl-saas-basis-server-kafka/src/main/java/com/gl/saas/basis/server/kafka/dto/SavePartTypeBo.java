package com.gl.saas.basis.server.kafka.dto;

import lombok.Data;

/**
 * @author ZWW
 */
@Data
public class SavePartTypeBo {
    private String id;
    private String name;
    private String brandId;
    private String code;
    private String remark;
    private String parentId;
    private String allianceId;

}
