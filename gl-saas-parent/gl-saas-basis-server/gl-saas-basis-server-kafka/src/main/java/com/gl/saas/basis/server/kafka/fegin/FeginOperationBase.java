/*package com.gl.saas.basis.server.kafka.fegin;

import com.gl.basis.common.config.FeignMultipartSupportConfig;
import com.gl.basis.common.pojo.Page;
import com.gl.saas.basis.server.kafka.dto.BasicPartsImportDto;
import com.gl.saas.basis.server.kafka.dto.SavePartTypeBo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "OPERATION-BASE-SERVICE", configuration = FeignMultipartSupportConfig.class)
public interface FeginOperationBase {
    @PostMapping("/basicParts/saveList" )
    Page<BasicPartsImportDto> saveList(@RequestBody Page<BasicPartsImportDto> page);

      @PostMapping("/basicPartsType/saveList" )
    Page<SavePartTypeBo> saveTypeList(@RequestBody Page<SavePartTypeBo> page);

    @PostMapping("/basicParts/downloadPartList" )
    Page<BasicPartsImportDto> downloadPartList(@RequestBody Page<BasicPartsImportDto> page);
}
*/