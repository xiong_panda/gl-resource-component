/*package com.gl.saas.basis.server.kafka.consumer.done.impl;

import com.alibaba.fastjson.JSON;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.common.util.BeanUtil;
import com.gl.saas.basis.server.kafka.consumer.done.MsgDone;
import com.gl.saas.basis.server.kafka.dto.BasicPartsImportDto;
import com.gl.saas.basis.server.kafka.dto.SaveBrandAllianceDto;
import com.gl.saas.basis.server.kafka.dto.SaveBrandDto;
import com.gl.saas.basis.server.kafka.dto.SavePartTypeBo;
import com.gl.saas.basis.server.kafka.fegin.FeginOperationBase;
import com.gl.saas.basis.server.kafka.fegin.FeginPaltformBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

*//**
 * @author zww
 * @date 2019年12月5日9:38:38
 *//*
@Service(value = "ImportPartsDone")
public class ImportPartsDone implements MsgDone {


    @Override
    public void doSomething(String key, String value, Map<String, Object> beans) {
        //导入配件
        if (key.equals("importPart")) {
            importParts(value, beans);
        } else if (key.equals("downloadPart")) {
            downloadPart(value, beans);

        }
    }

    private void downloadPart(String value, Map<String, Object> beans) {
        SaveBrandAllianceDto saveBrandAllianceDto = JSON.parseObject(value, SaveBrandAllianceDto.class);
        FeginOperationBase feginOperationBase = (FeginOperationBase) beans.get("feginOperationBase");
        FeginPaltformBase feginPaltformBase = (FeginPaltformBase) beans.get("feginPaltformBase");
        Map<String, Object> params = new HashMap<>();
        params.put("brandCode", saveBrandAllianceDto.getBrandId());
        params.put("brandId", saveBrandAllianceDto.getBrandId());
        //查询配件分类
        Page<SavePartTypeBo> partTypeBoPage = new Page<>();
        partTypeBoPage.setParams(params);
        Page<SavePartTypeBo> partTypePage = feginPaltformBase.getPartTypeListBrand(partTypeBoPage);
        //查询配件
        Page<BasicPartsImportDto> basicPartsImportDtoPage = new Page<>();
        basicPartsImportDtoPage.setParams(params);
        Page<BasicPartsImportDto> listParts = feginPaltformBase.getListParts(basicPartsImportDtoPage);
        //写入分类
        List<SavePartTypeBo> partTypeBoList = partTypePage.getList();
        if (partTypeBoList != null && partTypeBoList.size() != 0) {
            for (SavePartTypeBo savePartTypeBo : partTypeBoList
            ) {
                savePartTypeBo.setAllianceId(saveBrandAllianceDto.getAllianceId());
            }
            partTypeBoPage.setList(partTypeBoList);
            feginOperationBase.saveTypeList(partTypeBoPage);
        }
        //写入配件
        List<BasicPartsImportDto> partList = listParts.getList();
        if (partList != null && partList.size() != 0) {
            for (BasicPartsImportDto basicPartsImportDto : partList
            ) {
                basicPartsImportDto.setAllianceCode(saveBrandAllianceDto.getAllianceId());
            }
            basicPartsImportDtoPage.setList(partList);
            feginOperationBase.downloadPartList(basicPartsImportDtoPage);
        }

    }

    private void importParts(String value, Map<String, Object> beans) {
        //转换
        Page inPage = JSON.parseObject(value, Page.class);
        //获取配件分类
        List<SavePartTypeBo> savePartTypeBos = new ArrayList<>();
        if (inPage.getList() != null && inPage.getList().size() > 0) {
            savePartTypeBos = inPage.getList();
        }
        //获取配件
        List<BasicPartsImportDto> basicPartsImportDtos = new ArrayList<>();
        if (inPage.getResult() != null && !inPage.getResult().equals("0000")) {
            basicPartsImportDtos = (List<BasicPartsImportDto>) inPage.getResult();
        }
        //写入联盟库
        Page<BasicPartsImportDto> page = new Page<>();
        page.setList(basicPartsImportDtos);
        page.setResult(savePartTypeBos);
        FeginOperationBase feginOperationBase = (FeginOperationBase) beans.get("feginOperationBase");
        feginOperationBase.saveList(page);
        //写入平台库
        FeginPaltformBase feginPaltformBase = (FeginPaltformBase) beans.get("feginPaltformBase");
        feginPaltformBase.saveList(page);
        //变更品牌是否导入状态
        Page<SaveBrandDto> page1 = new Page<>();
        SaveBrandDto saveBrandDto = new SaveBrandDto();
       Map<String,Object> map= (Map<String, Object>) basicPartsImportDtos.get(0);
        saveBrandDto.setId(map.get("brandCode").toString());
        saveBrandDto.setImportBrand(1);
        page1.setT(saveBrandDto);
        feginPaltformBase.updateById(page1);
    }
}
*/