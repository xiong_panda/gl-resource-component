package com.gl.saas.basis.server.kafka.producer.out;

import lombok.Data;

/**
 * @author zww
 * @date 2019年10月25日14:46:19
 */
@Data
public class BasicOut {

    private Integer code;
    private String message;

    public BasicOut(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static BasicOut ok(){
        return new BasicOut(1,"操作成功");
    }
    public static BasicOut fail(String message){
        return new BasicOut(0,message);
    }
}
