package com.gl.saas.basis.server.kafka.consumer.done.impl;


import com.gl.saas.basis.server.kafka.consumer.done.MsgDone;

import java.util.Map;

public class LogMsgDone implements MsgDone {
    @Override
    public void doSomething(String key, String value, Map<String,Object> beans) {
        System.out.println(key+"这是LogMsgDone");
    }
}
