package com.gl.saas.basis.server.kafka.consumer.done;

import javax.servlet.ServletContextEvent;
import java.util.Map;

public interface MsgDone {

    void doSomething(String key, String value, Map<String,Object> beans);
}
