package com.gl.saas.basis.server.kafka.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zww
 * @date 2019年10月30日16:08:04
 */
@Data
@ApiModel(value="创建品牌", description="品牌")
public class SaveBrandDto {
    @ApiModelProperty(notes = "品牌id")
    private String id;
    @ApiModelProperty(notes = "品牌名称")
    private String name;
    @ApiModelProperty(notes = "品牌编码")
    private String code;
    @ApiModelProperty(notes = "启用禁用。0:禁用，1:启用")
    private String enable;
    @ApiModelProperty(notes = "图片URL")
    private String brandImgUrl;
    @ApiModelProperty(notes = "备注")
    private String remark;
    @ApiModelProperty(value = "是否导入配件(0，未导入）")
    private Integer importBrand;
}
