package com.gl.saas.basis.server.kafka.consumer.done.impl;

import com.gl.saas.basis.server.kafka.consumer.done.MsgDone;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service(value="OrderMsgDone")
public class OrderMsgDone implements MsgDone {
    @Override
    public void doSomething(String key, String value, Map<String,Object> beans) {
        System.out.println(key+"这是OrderMsgDone");
    }
}
