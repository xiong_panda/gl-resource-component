/*package com.gl.saas.basis.server.kafka.consumer;

import com.gl.saas.basis.server.kafka.consumer.done.MsgDone;
import com.gl.saas.basis.server.kafka.fegin.FeginOperationBase;
import com.gl.saas.basis.server.kafka.fegin.FeginPaltformBase;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletContextEvent;
import java.util.HashMap;
import java.util.Map;

*//**
 * @author zww
 *//*
@Component
public class ConsumerListen {
    private final Logger logger = LoggerFactory.getLogger(ConsumerListen.class);
    @Resource
    private FeginOperationBase feginOperationBase;
    @Resource
    FeginPaltformBase feginPaltformBase;

    @KafkaListener(topics = "#{'${spring.kafka.consumer.topic}'.split(',')}")
    public void listen(ConsumerRecord<?,?> record) {
        String topic = record.topic();
        try {
            keyMethodMap keyMethodMap=new keyMethodMap();
            if (keyMethodMap.getKeyMethods().get(topic)==null){
                logger.error("未配置topic：{}",topic);
                return;
            }
            Map<String,Object> beans=new HashMap<>(16);
            beans.put("feginOperationBase",feginOperationBase);
            beans.put("feginPaltformBase",feginPaltformBase);
            Class clz =  Class.forName(keyMethodMap.getKeyMethods().get(topic));
            MsgDone msgDone = (MsgDone)clz.newInstance();
            String key= (String) record.key();
            String value= (String) record.value();
            call(msgDone,key,value,beans);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


  private  static   void call(MsgDone msgDone,String key,String value,Map<String,Object> beans) {
        msgDone.doSomething(key,value,beans);
    }
}
*/