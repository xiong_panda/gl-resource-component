package com.gl.saas.basis.server.kafka.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ZWW
 * @date 2019年12月5日17:50:47
 */
@Data
public class SaveBrandAllianceDto {
    @ApiModelProperty(value = "id")
    private String id ;
    @ApiModelProperty(value = "品牌编码（外键关联brand：id）")
    private String brandId ;
    @ApiModelProperty(value = "远程调用使用，联盟id")
    private String allianceId ;
}
