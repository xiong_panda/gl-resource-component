package com.gl.saas.basis.server.kafka.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("导入配件返回")
public class BasicPartsImportDto {
    @ApiModelProperty(value = "行")
    private Integer line;
    @ApiModelProperty(value = "顺序")
    private Integer soft;
    @ApiModelProperty(value = "助记号")
    private String helpCode;
    @ApiModelProperty(value = "配件编码")
    private String code;
    @ApiModelProperty(value = "品牌id")
    private String brandCode;
    @ApiModelProperty(value = "品牌名称")
    private String brandName;
    @ApiModelProperty(value = "配件名称")
    private String name;
    @ApiModelProperty(value = "车型")
    private String carModel;
    @ApiModelProperty(value = "规格型号")
    private String specModel;
    @ApiModelProperty(value = "计量单位")
    private String unit;
    @ApiModelProperty(value = "配件分类（外键关联parts_type：id）")
    private String typeCode;
    @ApiModelProperty(value = "配件分类名称")
    private String typeName;
    @ApiModelProperty(value = "通俗名称")
    private String otherName;
    @ApiModelProperty(value = "可替代件")
    private String alternativeCode;
    @ApiModelProperty(value = "生产厂家（供应商")
    private String factory;
    @ApiModelProperty(value = "生产地址")
    private String factoryAddress;
    @ApiModelProperty(value = "是否大件（1是，0否）")
    private String larg;
    @ApiModelProperty(value = "颜色")
    private String color;
    @ApiModelProperty(value = "重量")
    private BigDecimal weight;
    @ApiModelProperty(value = "配件分类（外键关联parts_type：id）")
    private String corrCode;
    @ApiModelProperty(value = "上级配件")
    private String parentCode;
    @ApiModelProperty(value = "保修期")
    private String warranty;
    @ApiModelProperty(value = "最低包装数")
    private String minPacks;
    @ApiModelProperty(value = "联盟id")
    private String allianceCode;
    @ApiModelProperty(value = "库存上限")
    private Integer stockWarnMax;
    @ApiModelProperty(value = "库存下限")
    private Integer stockWarnMin;
    @ApiModelProperty(value = "订货警戒库存")
    private Integer stockWarnOrder;
    @ApiModelProperty(value = "供应商编码")
    private String vendorCode;
    @ApiModelProperty(value = "供应商")
    private String vendor;
    @ApiModelProperty(value = "条码")
    private String barCode;
    @ApiModelProperty(value = "是否进口")
    private String imported;
    @ApiModelProperty(value = "仓库名称")
    private String wareHouseName;
    @ApiModelProperty(value = "仓库名称")
    private String wareHouseId;
    @ApiModelProperty(value = "仓库名称")
    private String localtionName;
    @ApiModelProperty(value = "货位ID")
    private String localtionId;
}
