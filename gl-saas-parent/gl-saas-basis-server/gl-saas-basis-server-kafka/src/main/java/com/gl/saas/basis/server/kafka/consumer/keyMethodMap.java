package com.gl.saas.basis.server.kafka.consumer;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class keyMethodMap {

    private Map<String,String> keyMethods;



    public keyMethodMap() {
        Map<String, String> keyMethods=new HashMap<>();
        //配置topic 对应实现类 key是topic，value 对应实现类
      //  keyMethods.put("mytopic","com.gl.saas.basis.server.kafka.consumer.done.impl.LogMsgDone");
       keyMethods.put("mytopic","com.gl.saas.basis.server.kafka.consumer.done.impl.ImportPartsDone");
    //    keyMethods.put("mytopic","com.gl.saas.basis.server.kafka.consumer.done.impl.OrderMsgDone");
        this.keyMethods = keyMethods;
    }
}
