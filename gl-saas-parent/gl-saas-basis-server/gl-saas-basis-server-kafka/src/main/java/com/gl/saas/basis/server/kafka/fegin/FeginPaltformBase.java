/*package com.gl.saas.basis.server.kafka.fegin;

import com.gl.basis.common.config.FeignMultipartSupportConfig;
import com.gl.basis.common.pojo.Page;
import com.gl.saas.basis.server.kafka.dto.BasicPartsImportDto;
import com.gl.saas.basis.server.kafka.dto.SaveBrandDto;
import com.gl.saas.basis.server.kafka.dto.SavePartTypeBo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "BASE-SERVICE", configuration = FeignMultipartSupportConfig.class)
public interface FeginPaltformBase {
    @PostMapping("/basicParts/saveListPlatform" )
    Page<BasicPartsImportDto> saveList(@RequestBody Page<BasicPartsImportDto> page);
    *//**
     * 通过id修改品牌
     * @param page 品牌
     * @return Page
     *//*
    @PostMapping("/basicBrand/updateBrandImportById")
    Page<SaveBrandDto> updateById(@RequestBody Page<SaveBrandDto> page);

    *//**
     * 通过品牌查询配件列表
     * @param page
     * @return Page
     *//*
    @PostMapping("/basicParts/getList")
    Page<BasicPartsImportDto> getListParts(@RequestBody Page<BasicPartsImportDto> page);
    *//**
     * 配件分类列表
     * @param page
     * @return Page
     *//*
    @PostMapping("/basicPartsType/getListBrand")
     Page<SavePartTypeBo> getPartTypeListBrand(@RequestBody  Page<SavePartTypeBo> page);
}
*/