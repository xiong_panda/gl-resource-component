//package com.gl.saas.basis.server.sms.config;
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author YangGuoKuan
// * url白名单处理 application.yml中配置需要放权的url白名单
// */
//@ConfigurationProperties(prefix = "security.oauth2")
//public class ResourceUrlProperties {
//
//	/**
//	 * 监控中心和swagger需要访问的url
//	 */
//	private static final String[] ENDPOINTS = {
//			"/**/actuator/**" , "/**/actuator/**/**" ,
//			"/**/v2/api-docs/**", "/**/swagger-ui.html", "/**/swagger-resources/**", "/**/webjars/**",
//			"/**/turbine.stream","/**/turbine.stream**/**", "/**/hystrix", "/**/hystrix.stream", "/**/hystrix/**", "/**/hystrix/**/**",	"/**/proxy.stream/**" ,
//			"/**/druid/**", "/**/favicon.ico", "/**/prometheus",
//			"/oauth/**","/sms/token", "/code/img","**/send/**",
//	};
//
//	private String[] ignored;
//
//	/**
//	 * 自定义的url和监控中心需要访问的url集合
//	 * @return
//	 */
//	public String[] getIgnored() {
//		if (ignored == null || ignored.length == 0) {
//			return ENDPOINTS;
//		}
//
//		List<String> list = new ArrayList<>();
//		for (String url : ENDPOINTS) {
//			list.add(url);
//		}
//		for (String url : ignored) {
//			list.add(url);
//		}
//
//		return list.toArray(new String[list.size()]);
//	}
//
//	public void setIgnored(String[] ignored) {
//		this.ignored = ignored;
//	}
//
//}
