package com.gl.saas.basis.server.sms.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResult {

    private Integer code;

    private String message;

    private ResultCode data;

    public BaseResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResult(Integer code, String message, ResultCode data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ResultCode {
        private String key;
        private String code;
    }
}
