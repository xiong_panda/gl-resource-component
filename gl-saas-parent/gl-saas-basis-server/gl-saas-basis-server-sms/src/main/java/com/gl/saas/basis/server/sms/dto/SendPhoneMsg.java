package com.gl.saas.basis.server.sms.dto;

import lombok.Data;

@Data
public class SendPhoneMsg {

    private Integer type;
    private String phone;
    private String sendMsg;

}
