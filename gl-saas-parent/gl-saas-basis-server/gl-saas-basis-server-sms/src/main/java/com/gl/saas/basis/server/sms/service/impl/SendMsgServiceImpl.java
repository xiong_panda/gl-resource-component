package com.gl.saas.basis.server.sms.service.impl;

import com.gl.basis.common.util.StringUtils;
import com.gl.basis.constant.CacheConstants;

import com.gl.basis.util.RedisUtil;
import com.gl.saas.basis.server.sms.dto.BaseResult;
import com.gl.saas.basis.server.sms.service.SendMsgService;
import com.gl.saas.basis.server.sms.utils.CodeUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class SendMsgServiceImpl implements SendMsgService {
//   @Value("${aliyun.accessKey}")
//    private String accessKey;
//    @Value("${aliyun.accessSecret}")
//    private String accessSecret;
//    @Value("${aliyun.signName}")
//    private String signName;
//    @Value("${aliyun.templateCode.code}")
//    private String templateCode;
//    @Value("${aliyun.templateCode.other}")
//    private String templateOtherCode;
    @Resource
RedisUtil redisUtil;

    @Override
    public BaseResult sendMsg(String phone, Integer type, String sendJSON) {

        if(phone!=null){
            if(phone.length()!=10){
                return  new BaseResult(0,"请输入正确的电话号码");
            }
        }
        if (type == null) {
            return new BaseResult(0, "请输入正确的短信类型");
        }
        BaseResult.ResultCode resultCode=null;
        if (type == 1) {
            //判断用户发送短信频率
//           if( StringUtils.isEmpty(redisUtil.get(CacheConstants.SMS+phone).toString())){
//               String  mes= (String)redisUtil.get(CacheConstants.SMS+phone);
//               return new  BaseResult(0,"你的验证码"+mes);
//           };
            String code = CodeUtils.radomCode();
            Map<String, Object> map = new HashMap<>(1);
            map.put("code", code);
           // String templateParam = JSON.toJSONString(map);
           // SendMsgUtils.sendMsg(accessKey, accessSecret, phone, signName, templateCode, templateParam);
            redisUtil.set(CacheConstants.SMS+phone, code,300L);
          String rediscode = (String) redisUtil.get(CacheConstants.SMS+phone);
            System.err.println(rediscode);
            resultCode=new  BaseResult.ResultCode();
            resultCode.setCode(code);
            resultCode.setKey(phone);
            System.out.println(phone+"____"+code);
        } else if (type == 2) {
            //SendMsgUtils.sendMsg(accessKey, accessSecret, phone, signName, templateCode, sendJSON);
        }
        return new BaseResult(1, "成功");
    }

    @Override
    public BaseResult validataMsg(String phone, String code) {

        if (phone==null){
            return new BaseResult(0, "请输入手机号");
        }
        if (code==null){
            return new BaseResult(0, "请输入验证码");
        }
        String redisCode = redisUtil.get(CacheConstants.SMS+phone).toString();
        if (redisCode==null){
            return new BaseResult(0, "验证码不存在");
        }
        if (!redisCode.equals(code)){
            return new BaseResult(0, "验证码不错误");
        }
        return  new BaseResult(1, "操作成功");
    }
}
