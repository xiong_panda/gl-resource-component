package com.gl.saas.basis.server.sms.controller;

import com.gl.saas.basis.server.sms.dto.BaseResult;
import com.gl.saas.basis.server.sms.dto.SendPhoneMsg;
import com.gl.saas.basis.server.sms.service.SendMsgService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

@RestController
@RequestMapping("/send")
public class SendMsgController {

    @Resource
    SendMsgService sendMsgService;


    @PostMapping("/phone")
    public BaseResult sendMsgPhone(@RequestBody SendPhoneMsg sendPhoneMsg){
        return sendMsgService.sendMsg(sendPhoneMsg.getPhone(),sendPhoneMsg.getType(),sendPhoneMsg.getSendMsg());
    }

    @GetMapping("/validataMsg")
    public BaseResult validataMsg(@RequestParam("phone") String phone, @RequestParam("code")String code){
        return sendMsgService.validataMsg(phone,code);
    }

}
