package com.gl.saas.basis.server.sms.service;


import com.gl.saas.basis.server.sms.dto.BaseResult;

public interface SendMsgService {
    BaseResult sendMsg(String phone, Integer type, String sendJSON);

    BaseResult validataMsg(String phone, String code);
}
