//package com.gl.saas.basis.server.sms.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
//
//
//@Configuration
//@EnableResourceServer
//@EnableConfigurationProperties(ResourceUrlProperties.class)
//public class ResourceServerAutoConfig extends ResourceServerConfigurerAdapter {
//
//    @Autowired
//    private ResourceUrlProperties resourceUrlProperties;
//    @Autowired
//    private RedisConnectionFactory redisConnectionFactory;
//
//    @Bean
//    public TokenStore tokenStore() {
//        return new RedisTokenStore(redisConnectionFactory);
//    }
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//        resources
//                .tokenStore(tokenStore());
//    }
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.
//                headers().frameOptions().disable()
//                .and()
//                .authorizeRequests()
////                .antMatchers("/**")
////                .access("#oauth2.hasScope('all')")
//                .antMatchers(resourceUrlProperties.getIgnored())
//                .permitAll()
//                .anyRequest().authenticated();
//    }
//}
