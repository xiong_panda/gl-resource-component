package com.gl.basis.zuul.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 应用访问日志
 * @author code_generator
 */
@Table(name = "SYS_APP_LOG")
public class SysAppLog implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 接口名称 **/
	@Id
	private String fPkid ; 

	/** 接口名称 **/
	@Column(name="interface_name")
	private String interfaceName ; 

	/** 访问ip **/
	@Column(name="access_ip")
	private String accessIp ; 

	/** 客户端id **/
	@Column(name="app_key")
	private String appKey ; 

	/** 访问时长 **/
	@Column(name="visit_duration")
	private Long visitDuration ; 

	/** 访问时间 **/
	@Column(name="access_time")
	private Date accessTime ; 

	/** 访问服务ip **/
	@Column(name="service_ip")
	private String serviceIp ; 

	/** 请求参数 **/
	@Column(name="params")
	private String params ; 

	/** 描述信息 **/
	@Column(name="description")
	private String description ; 

	/** 启用状态 **/
	@Column(name="f_enable")
	private String fEnable ; 

	/** 删除标志 **/
	@Column(name="f_isdelete")
	private String fIsdelete ; 

	/** 插入人员id **/
	@Column(name="f_inputid")
	private String fInputid ; 

	/**  **/
	@Column(name="f_inputtime")
	private Date fInputtime ; 

	/**  **/
	@Column(name="f_endid")
	private String fEndid ; 

	/** 更新时间 **/
	@Column(name="f_endtime")
	private Date fEndtime ;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getAccessIp() {
		return accessIp;
	}

	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public Long getVisitDuration() {
		return visitDuration;
	}

	public void setVisitDuration(Long visitDuration) {
		this.visitDuration = visitDuration;
	}

	public Date getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(Date accessTime) {
		this.accessTime = accessTime;
	}

	public String getServiceIp() {
		return serviceIp;
	}

	public void setServiceIp(String serviceIp) {
		this.serviceIp = serviceIp;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	} 


}
