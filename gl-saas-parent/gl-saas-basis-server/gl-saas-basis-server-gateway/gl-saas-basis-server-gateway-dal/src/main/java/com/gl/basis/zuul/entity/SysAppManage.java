package com.gl.basis.zuul.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效! 应用管理
 * 
 * @author code_generator
 */
@Table(name = "SYS_APP_MANAGE")
public class SysAppManage implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	/**  **/
	@Id
	private String fPkid;

	/** 接入的客户端ID **/
	@Column(name = "app_key")
	private String appKey;

	/** 解密公钥（rsa_public_key） **/
	@Column(name = "public_key")
	private String publicKey;

	/** 加密私钥（rsa_private_key_pkcs8） **/
	@Column(name = "private_key")
	private String privateKey;
	
	/** 签名key(支持MD5,hash) **/
	@Column(name = "sign_key")
	private String signKey;

	/** 回调地址 **/
	@Column(name = "redirect_uri")
	private String redirectUri;

	/** 客户端ip **/
	@Column(name = "req_ip")
	private String reqIp;

	/** 日调用次数限制 **/
	@Column(name = "daily_visits")
	private Integer dailyVisits;

	/** 状态 **/
	@Column(name = "state")
	private String state;

	/** 描述信息 **/
	@Column(name = "description")
	private String description;

	/** 启用状态 **/
	@Column(name = "f_enable")
	private String fEnable;

	/** 删除标志 **/
	@Column(name = "f_isdelete")
	private String fIsdelete;

	/** 插入人员id **/
	@Column(name = "f_inputid")
	private String fInputid;

	/**  **/
	@Column(name = "f_inputtime")
	private Date fInputtime;

	/**  **/
	@Column(name = "f_endid")
	private String fEndid;

	/** 更新时间 **/
	@Column(name = "f_endtime")
	private Date fEndtime;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}

	public String getReqIp() {
		return reqIp;
	}

	public void setReqIp(String reqIp) {
		this.reqIp = reqIp;
	}

	public Integer getDailyVisits() {
		return dailyVisits;
	}

	public void setDailyVisits(Integer dailyVisits) {
		this.dailyVisits = dailyVisits;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	}

	public String getSignKey() {
		return signKey;
	}

	public void setSignKey(String signKey) {
		this.signKey = signKey;
	}

}
