package com.gl.basis.zuul.mapper;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.basis.zuul.entity.SysAppManage;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface SysAppManageMapper extends MyMapper<SysAppManage>{

	List<SysAppManage> getList(Map<String,Object> params);

	void subtractionFreuency(Map<String,Object> params);

}
