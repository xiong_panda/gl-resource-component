package com.gl.basis.zuul.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gl.basis.zuul.entity.SysAppLog;
import com.gl.basis.zuul.mapper.SysAppLogMapper;
import com.gl.basis.zuul.service.ISysAppLogDalService;

@Service
public class SysAppLogDalServiceImpl implements ISysAppLogDalService{

	@Autowired
	private SysAppLogMapper sysAppLogMapper;
	
	@Override
	public List<SysAppLog> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Transactional
	@Override
	public void addlog(SysAppLog log) {
		sysAppLogMapper.insertListIncludeId(Arrays.asList(log));
	}

}
