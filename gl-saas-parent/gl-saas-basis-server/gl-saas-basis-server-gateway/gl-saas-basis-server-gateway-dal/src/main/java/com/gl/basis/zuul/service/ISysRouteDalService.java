package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.zuul.entity.SysRoute;
import com.gl.basis.zuul.outdto.Route;

/**
 * service业务处理层,自定义方法请写在此接口中
 * 
 */
public interface ISysRouteDalService {

	List<SysRoute> getListByPage(Map<String, Object> params);

	/**
	 * 查询所有路由
	 * 
	 * @return
	 */
	List<Route> get();

}
