package com.gl.basis.zuul.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效! 公共参数
 * 
 * @author code_generator
 */
@Table(name = "SYS_APP_TOKEN")
public class SysAppToken implements java.io.Serializable {
	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	private static final long serialVersionUID = 1L;

	/**  **/
	@Id
	private String fPkid;

	/** 应用id **/
	@Column(name = "appid")
	private String appid;

	/** 用户id **/
	@Column(name = "userid")
	private String userid;

	/** token **/
	@Column(name = "access_token")
	private String accessToken;

	/** 过期时间 **/
	@Column(name = "expire_time")
	private Date expireTime;

	/** token生成时间 **/
	@Column(name = "start_time")
	private Date startTime;

	/** 启用状态 **/
	@Column(name = "f_enable")
	private String fEnable;

	/** 删除标志 **/
	@Column(name = "f_isdelete")
	private String fIsdelete;

	/** 插入人员id **/
	@Column(name = "f_inputid")
	private String fInputid;

	/**  **/
	@Column(name = "f_inputtime")
	private Date fInputtime;

	/**  **/
	@Column(name = "f_endid")
	private String fEndid;

	/** 更新时间 **/
	@Column(name = "f_endtime")
	private Date fEndtime;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	}

}
