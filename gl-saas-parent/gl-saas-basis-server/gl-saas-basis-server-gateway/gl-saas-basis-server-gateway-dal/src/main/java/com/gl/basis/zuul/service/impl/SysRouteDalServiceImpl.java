package com.gl.basis.zuul.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysRoute;
import com.gl.basis.zuul.mapper.SysRouteMapper;
import com.gl.basis.zuul.outdto.Route;
import com.gl.basis.zuul.service.ISysRouteDalService;

/**
 * 
 * 路由获取
 * @author xiehong
 *
 */
@Service
public class SysRouteDalServiceImpl implements ISysRouteDalService {

	@Autowired
	private SysRouteMapper sysRouteMapper;
	
	@Override
	public List<SysRoute> getListByPage(Map<String, Object> params) {
		return sysRouteMapper.getList(params);
	}

	@Override
	public List<Route> get() {
		return sysRouteMapper.get();
	}

}
