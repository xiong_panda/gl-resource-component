package com.gl.basis.zuul.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 第三方用户接入信息表
 * @author code_generator
 */
@Table(name = "SYS_APP_CUSTOMER")
public class SysAppCustomer implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@Id
	private String fPkid ; 

	/** 用户名（包含单位或者个人） **/
	@Column(name="user_name")
	private String userName ; 

	/** 密码 **/
	@Column(name="password")
	private String password ; 

	/** 接入人手机号 **/
	@Column(name="phone")
	private String phone ; 

	/** 昵称 **/
	@Column(name="nickname")
	private String nickname ; 

	/** 客户端id **/
	@Column(name="app_key")
	private String appKey ; 

	/** 地址 **/
	@Column(name="address")
	private String address ; 

	/** 余额 **/
	@Column(name="money")
	private Long money ; 

	/** 金额截止使用时间 **/
	@Column(name="deadline_time")
	private Date deadlineTime ; 

	/** 启用状态 **/
	@Column(name="f_enable")
	private String fEnable ; 

	/** 删除标志 **/
	@Column(name="f_isdelete")
	private String fIsdelete ; 

	/** 插入人员id **/
	@Column(name="f_inputid")
	private String fInputid ; 

	/**  **/
	@Column(name="f_inputtime")
	private Date fInputtime ; 

	/**  **/
	@Column(name="f_endid")
	private String fEndid ; 

	/** 更新时间 **/
	@Column(name="f_endtime")
	private Date fEndtime ;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getMoney() {
		return money;
	}

	public void setMoney(Long money) {
		this.money = money;
	}

	public Date getDeadlineTime() {
		return deadlineTime;
	}

	public void setDeadlineTime(Date deadlineTime) {
		this.deadlineTime = deadlineTime;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	} 


}
