package com.gl.basis.zuul.mapper;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

import com.gl.basis.zuul.entity.SysRoute;
import com.gl.basis.zuul.outdto.Route;
import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 */
@MapperPrimary
@Component
public interface SysRouteMapper extends MyMapper<SysRoute>{

	List<SysRoute> getList(Map<String,Object> params);

	List<Route> get();

}
