package com.gl.basis.zuul.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;

/**
 * 注意:注解只能加在属性字段上才会生效! 第三方路由信息表
 * 
 * @author code_generator
 */
@Table(name = "SYS_APP_MAPPING")
public class SysAppMapping implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	/**  **/
	@Id
	private String fPkid;

	/** 对外接口名称 **/
	@Column(name = "gateway_api_name")
	private String gatewayApiName;

	/** 对内的接口url **/
	@Column(name = "inside_api_url")
	private String insideApiUrl;

	/** api状态（1可用，0不可用） **/
	@Column(name = "state")
	private String state;

	/** 接口版本（V0.01） **/
	@Column(name = "version")
	private String version;

	/** 描述 **/
	@Column(name = "describe")
	private String describe;
	/** 接口签名方式（MD5,RSA） **/
	@Column(name = "sign_method")
	private String signMethod;

	/** 服务实例名称id **/
	@Column(name = "service_id")
	private String serviceId;

	/** 是否是冥等操作 **/
	@Column(name = "idempotents")
	private String idempotents;
	/** 收费金额 **/
	@Column(name = "coolect_feesmoney")
	private Integer coolectFeesmoney;

	/** 是否收费 **/
	@Column(name = "needfee")
	private String needfee;

	/** 启用状态 **/
	@Column(name = "f_enable")
	private String fEnable;

	/** 删除标志 **/
	@Column(name = "f_isdelete")
	private String fIsdelete;

	/** 插入人员id **/
	@Column(name = "f_inputid")
	private String fInputid;

	/**  **/
	@Column(name = "f_inputtime")
	private Date fInputtime;

	/**  **/
	@Column(name = "f_endid")
	private String fEndid;

	/** 更新时间 **/
	@Column(name = "f_endtime")
	private Date fEndtime;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getGatewayApiName() {
		return gatewayApiName;
	}

	public void setGatewayApiName(String gatewayApiName) {
		this.gatewayApiName = gatewayApiName;
	}

	public String getInsideApiUrl() {
		return insideApiUrl;
	}

	public void setInsideApiUrl(String insideApiUrl) {
		this.insideApiUrl = insideApiUrl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getIdempotents() {
		return idempotents;
	}

	public void setIdempotents(String idempotents) {
		this.idempotents = idempotents;
	}

	public String getNeedfee() {
		return needfee;
	}

	public void setNeedfee(String needfee) {
		this.needfee = needfee;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	}

	public String getSignMethod() {
		return signMethod;
	}

	public void setSignMethod(String signMethod) {
		this.signMethod = signMethod;
	}

	public Integer getCoolectFeesmoney() {
		return coolectFeesmoney;
	}

	public void setCoolectFeesmoney(Integer coolectFeesmoney) {
		this.coolectFeesmoney = coolectFeesmoney;
	}

}
