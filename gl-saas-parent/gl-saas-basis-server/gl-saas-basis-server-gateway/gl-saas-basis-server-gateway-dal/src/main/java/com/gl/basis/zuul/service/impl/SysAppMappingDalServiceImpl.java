package com.gl.basis.zuul.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppMapping;
import com.gl.basis.zuul.mapper.SysAppMappingMapper;
import com.gl.basis.zuul.service.ISysAppMappingDalService;


@Service
public class SysAppMappingDalServiceImpl implements ISysAppMappingDalService{

	@Autowired
	private SysAppMappingMapper sysAppMappingMapper;
	
	@Override
	public List<SysAppMapping> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public SysAppMapping findAppMappingUrl(String method) {
		return sysAppMappingMapper.findAppMappingUrl(Collections.singletonMap("gatewayApiName", method));
	}

}
