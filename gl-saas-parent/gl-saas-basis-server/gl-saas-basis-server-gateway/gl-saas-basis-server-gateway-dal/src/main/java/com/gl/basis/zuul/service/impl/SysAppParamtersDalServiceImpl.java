package com.gl.basis.zuul.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppParamters;
import com.gl.basis.zuul.mapper.SysAppParamtersMapper;
import com.gl.basis.zuul.service.ISysAppParamtersDalService;

@Service
public class SysAppParamtersDalServiceImpl  implements ISysAppParamtersDalService{

	@Autowired
	private SysAppParamtersMapper sysAppParamtersMapper;
	
	@Override
	public List<SysAppParamters> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public List<String> findCommParams(Map<String, Object> params) {
		return sysAppParamtersMapper.findCommParams(params);
	}

}
