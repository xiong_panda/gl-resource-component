package com.gl.basis.zuul.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.gl.common.mapper.MyMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;
import com.gl.basis.zuul.entity.SysAppMapping;
/**
 * mapper接口,自定义方法写入此接口,并在xml中实现
 * @author code_generator
 */
@MapperPrimary
@Component
public interface SysAppMappingMapper extends MyMapper<SysAppMapping>{

	List<SysAppMapping> getList(Map<String,Object> params);

	SysAppMapping findAppMappingUrl(Map<String,Object> params);

}
