package com.gl.basis.zuul.entity;

import java.util.Date;
import java.math.BigDecimal;
import java.sql.*;
import javax.persistence.*;
import lombok.Data;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 公共参数
 * @author code_generator
 */
@Table(name = "SYS_APP_PARAMTERS")
@Data
public class SysAppParamters implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@Id
	private String fPkid ; 

	/** 名称 **/
	@Column(name="name")
	private String name ; 

	/** 参数 **/
	@Column(name="params")
	private String params ; 

	/** 状态 **/
	@Column(name="state")
	private String state ; 

	/** 描述 **/
	@Column(name="describe")
	private String describe ; 

	/** 启用状态 **/
	@Column(name="f_enable")
	private String fEnable ; 

	/** 删除标志 **/
	@Column(name="f_isdelete")
	private String fIsdelete ; 

	/** 插入人员id **/
	@Column(name="f_inputid")
	private String fInputid ; 

	/**  **/
	@Column(name="f_inputtime")
	private Date fInputtime ; 

	/**  **/
	@Column(name="f_endid")
	private String fEndid ; 

	/** 更新时间 **/
	@Column(name="f_endtime")
	private Date fEndtime ;

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getfEnable() {
		return fEnable;
	}

	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}

	public String getfIsdelete() {
		return fIsdelete;
	}

	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}

	public String getfInputid() {
		return fInputid;
	}

	public void setfInputid(String fInputid) {
		this.fInputid = fInputid;
	}

	public Date getfInputtime() {
		return fInputtime;
	}

	public void setfInputtime(Date fInputtime) {
		this.fInputtime = fInputtime;
	}

	public String getfEndid() {
		return fEndid;
	}

	public void setfEndid(String fEndid) {
		this.fEndid = fEndid;
	}

	public Date getfEndtime() {
		return fEndtime;
	}

	public void setfEndtime(Date fEndtime) {
		this.fEndtime = fEndtime;
	} 


}
