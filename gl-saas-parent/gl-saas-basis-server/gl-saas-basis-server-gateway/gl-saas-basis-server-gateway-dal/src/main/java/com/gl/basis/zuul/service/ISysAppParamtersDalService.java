package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.zuul.entity.SysAppParamters;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ISysAppParamtersDalService {

List<SysAppParamters> getListByPage(Map<String,Object> params);

List<String> findCommParams(Map<String, Object> params);

}
