package com.gl.basis.zuul.entity;

import java.util.Date;
import javax.persistence.*;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 路由表
 * @author code_generator
 */
@Table(name = "SYS_ROUTE")
public class SysRoute implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/**  **/
	@Id
	private String fPkId ; 

	/** 路由id,黑名单时不需要 **/
	@Column(name="route_id")
	private String routeId ; 

	/** 路由路径 **/
	@Column(name="path")
	private String path ; 

	/** 路由转发服务 **/
	@Column(name="service_id")
	private String serviceId ; 

	/** 项目web、app **/
	@Column(name="project")
	private String project ; 

	/** 路由类型（toute,white,black） **/
	@Column(name="route_type")
	private String routeType ; 

	/**  **/
	@Column(name="f_input_time")
	private Date fInputTime ;

	public String getfPkId() {
		return fPkId;
	}

	public void setfPkId(String fPkId) {
		this.fPkId = fPkId;
	}

	public String getRouteId() {
		return routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public Date getfInputTime() {
		return fInputTime;
	}

	public void setfInputTime(Date fInputTime) {
		this.fInputTime = fInputTime;
	} 


}
