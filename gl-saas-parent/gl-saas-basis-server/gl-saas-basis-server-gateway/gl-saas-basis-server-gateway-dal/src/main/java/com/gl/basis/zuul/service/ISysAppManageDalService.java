package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.zuul.entity.SysAppManage;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ISysAppManageDalService  {

List<SysAppManage> getListByPage(Map<String,Object> params);

SysAppManage findSerct(Map<String, Object> params);

void subtractionFreuency(Map<String,Object> params);

}
