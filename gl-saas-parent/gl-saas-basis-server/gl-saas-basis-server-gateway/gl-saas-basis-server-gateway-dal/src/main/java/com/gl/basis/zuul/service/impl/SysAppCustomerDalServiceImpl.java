package com.gl.basis.zuul.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppCustomer;
import com.gl.basis.zuul.mapper.SysAppCustomerMapper;
import com.gl.basis.zuul.service.ISysAppCustomerDalService;

@Service
public class SysAppCustomerDalServiceImpl implements ISysAppCustomerDalService{

	@Autowired
	private SysAppCustomerMapper sysAppCustomerMapper;
	
	@Override
	public List<SysAppCustomer> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public Integer findCustomerMoney(String appkey) {
		return sysAppCustomerMapper.findCustomerMoney(Collections.singletonMap("appKey", appkey));
	}

}
