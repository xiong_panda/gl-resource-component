package com.gl.basis.zuul.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gl.basis.zuul.entity.SysAppManage;
import com.gl.basis.zuul.mapper.SysAppManageMapper;
import com.gl.basis.zuul.service.ISysAppManageDalService;

@Service
public class SysAppManageDalServiceImpl implements ISysAppManageDalService {

	@Autowired
	private SysAppManageMapper sysAppManageMapper;
	
	@Override
	public List<SysAppManage> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public SysAppManage findSerct(Map<String, Object> params) {
		return sysAppManageMapper.getOne(params);
	}

	@Transactional
	@Override
	public void subtractionFreuency(Map<String,Object> params) {
		sysAppManageMapper.subtractionFreuency(params);
	}

}
