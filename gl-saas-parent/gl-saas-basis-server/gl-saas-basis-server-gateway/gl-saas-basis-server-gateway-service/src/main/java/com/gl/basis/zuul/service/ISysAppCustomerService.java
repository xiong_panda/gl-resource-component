package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.zuul.entity.SysAppCustomer;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ISysAppCustomerService  {

List<SysAppCustomer> getListByPage(Map<String,Object> params);

/**
 * 获取金额
 * @param appkey
 * @return
 */
Integer findCustomerMoney(String appkey);

}
