package com.gl.basis.zuul.filter;

import com.gl.basis.zuul.config.ZuulConfig;
import com.gl.basis.zuul.util.UrlPattern;
import com.gl.basis.zuul.util.ZuulUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 * 黑名单管理
 *
 */
@Slf4j
@Component
public class BlackListFilter extends ZuulFilter {

    private ZuulConfig zuulConfig;

    public BlackListFilter(ZuulConfig zuulConfig) {
        this.zuulConfig = zuulConfig;
    }

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 5;
    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().sendZuulResponse();
    }

    @Override
    public Object run() {
        long start = System.currentTimeMillis();
        RequestContext context = RequestContext.getCurrentContext();
        String path = context.getRequest().getServletPath();
        if (!matchBlackList(path)) {
            ZuulUtil.failFast(context, 403, 403, "请求的地址禁用");
        }
        long end = System.currentTimeMillis();
        log.info("msg1=计算黑名单耗时: {}", end - start);
        return null;
    }

    private boolean matchBlackList(String path) {
        // 没配置代表不启用用白名单功能
        if (CollectionUtils.isEmpty(zuulConfig.getBlackListAll())) {
            return true;
        }
        String pattern =
            zuulConfig.getBlackListAll().stream().filter(o ->UrlPattern.isMatch(path,o)).findFirst().orElse(null);
        if (StringUtils.isEmpty(pattern)) {
            return true;
        }
        log.info("msg1=为请求[{}]找到可匹配的黑名单规则[{}], 请求被禁用", path, pattern);
        return false;
    }

}
