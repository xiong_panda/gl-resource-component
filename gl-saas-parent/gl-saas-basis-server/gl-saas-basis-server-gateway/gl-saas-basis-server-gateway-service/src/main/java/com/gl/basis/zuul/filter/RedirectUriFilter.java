package com.gl.basis.zuul.filter;


import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.basis.auth.constants.AuthorityConstants;
import com.gl.basis.common.constant.ResultCode;
import com.gl.basis.common.pojo.CommBean;
import com.gl.basis.common.pojo.CommonResult;
import com.gl.basis.common.util.RSAUtils;
import com.gl.basis.zuul.entity.SysAppManage;
import com.gl.basis.zuul.entity.SysAppMapping;
import com.gl.basis.zuul.enums.RouteType;
import com.gl.basis.zuul.service.ISysAppManageService;
import com.gl.basis.zuul.service.ISysAppMappingService;
import com.gl.basis.zuul.util.CommonUtil;
import com.gl.basis.zuul.util.RequestUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

/**
 * 当用户传地址，页面跳转
 * 
 * @author admin
 *
 */
@Component
public class RedirectUriFilter extends ZuulFilter {

	private static final Logger log = LoggerFactory.getLogger(RedirectUriFilter.class);
	@Resource
	private ISysAppManageService sysAppManageService;
	@Resource
	private ISysAppMappingService sysAppMappingService;
	@Resource
	private ObjectMapper objectMapper;

	@Override
	public boolean shouldFilter() {
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
        String path = request.getServletPath();
        //包含api就不转发
        if (!currentContext.sendZuulResponse() ||( StringUtils.isNotBlank(path)&& path.contains(RouteType.API.getCode()))) {
			return false;
		}
		return true;
	}

	@Override
	public Object run() throws ZuulException {

		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
		String requestParams = RequestUtils.getRequestParams(currentContext, request);
		CommBean reqParams = JSONObject.parseObject(requestParams, CommBean.class);
		String reqsign = reqParams.getSign();
		

		return null;
	}

	@Override
	public String filterType() {
		return FilterConstants.POST_TYPE;
	}

	@Override
	public int filterOrder() {
		return 0;
	}

}
