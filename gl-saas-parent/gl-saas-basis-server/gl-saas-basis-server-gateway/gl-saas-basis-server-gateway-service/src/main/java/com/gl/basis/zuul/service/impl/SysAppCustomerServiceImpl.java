package com.gl.basis.zuul.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppCustomer;
import com.gl.basis.zuul.service.ISysAppCustomerDalService;
import com.gl.basis.zuul.service.ISysAppCustomerService;

/**
 * 	用户账户
 * @author admin
 *
 */
@Service
public class SysAppCustomerServiceImpl implements ISysAppCustomerService{

	@Autowired
	private ISysAppCustomerDalService sysAppCustomerDalService;
	
	@Override
	public List<SysAppCustomer> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public Integer findCustomerMoney(String appkey) {
		return sysAppCustomerDalService.findCustomerMoney(appkey);
	}

}
