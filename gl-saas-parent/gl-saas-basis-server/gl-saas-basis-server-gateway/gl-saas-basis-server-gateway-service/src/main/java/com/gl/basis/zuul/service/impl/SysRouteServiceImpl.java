package com.gl.basis.zuul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.outdto.Route;
import com.gl.basis.zuul.service.ISysRouteDalService;
import com.gl.basis.zuul.service.ISysRouteService;


@Service
public class SysRouteServiceImpl implements ISysRouteService {

	@Autowired
	private ISysRouteDalService  sysRouteDalService;
	
	@Override
	public List<Route> get() {
		return sysRouteDalService.get();
	}

}
