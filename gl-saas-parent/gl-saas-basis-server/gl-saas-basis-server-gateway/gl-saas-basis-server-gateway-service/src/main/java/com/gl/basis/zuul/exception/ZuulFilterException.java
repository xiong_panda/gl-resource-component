package com.gl.basis.zuul.exception;

/**
 * TODO
 *
 * @author
 * @date 2019/7/5 4:55 PM
 */
public class ZuulFilterException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ZuulFilterException(String message) {
        super(message);
    }

    public ZuulFilterException(String message, Throwable cause) {
        super(message, cause);
    }

	public ZuulFilterException() {
		super();
	}

}
