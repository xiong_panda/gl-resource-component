package com.gl.basis.zuul.wrapper;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.netflix.zuul.http.HttpServletRequestWrapper;

/**
 *   防止xss攻击
 * @author Administrator
 *
 */

public class XssAndSqlHttpServletRequestWrapper extends HttpServletRequestWrapper{

	
	public XssAndSqlHttpServletRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);
		if (!StringUtils.isEmpty(value)) {
			value = StringEscapeUtils.escapeJava(value);
		}
		return value;
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] values = super.getParameterValues(name);
		if (null == values) {
			return null;
		}
		for(int i = 0; i < values.length; i ++) {
			String value = 	values[i];
			values[i]= StringEscapeUtils.escapeJava(value);
		}
		return values;
		
	}
	
	
}
