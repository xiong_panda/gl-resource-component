package com.gl.basis.zuul.service;

import java.util.List;

import com.gl.basis.zuul.outdto.Route;

public interface ISysRouteService {

	List<Route> get();

}
