/*package com.gl.basis.zuul.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.gl.basis.common.pojo.CommonResult;
import com.gl.basis.common.pojo.Page;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import lombok.extern.slf4j.Slf4j;

*//**
 * 自定义error错误页面
 * @author 
 *//*
@Slf4j
@RestController
public class ErrorHandlerController  implements ErrorController {
 
    *//**
     * 	出异常后进入该方法，交由下面的方法处理
     *//*
    @Override
    public String getErrorPath() {
        return "/error";
    }
 
    @RequestMapping("/error")
    @ResponseBody
    public Object error(HttpServletRequest request, HttpServletResponse response){
       
        RequestContext ctx = RequestContext.getCurrentContext();
        ZuulException exception = (ZuulException)ctx.getThrowable();
        Integer status = (Integer)request.getAttribute("javax.servlet.error.status_code");
        log.error("网关全局异常输出：{}，异常详情：{}",exception.getMessage() ,exception);
        return  CommonResult.failed(status+"",exception.getMessage().contains("Filter")?"服务不可用！":exception.getMessage());
    }
 

}*/