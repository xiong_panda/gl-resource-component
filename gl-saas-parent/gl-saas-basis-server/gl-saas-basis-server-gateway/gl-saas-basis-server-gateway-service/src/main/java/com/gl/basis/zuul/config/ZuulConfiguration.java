package com.gl.basis.zuul.config;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulConfiguration {

    @Bean
    public RouteLocator routeLocator(ServerProperties server, ZuulProperties zuulProperties, ZuulConfig zuulConfig) {
        return new RouteLocator(server.getServlet().getServletPrefix(), zuulProperties, zuulConfig);
    }
}
