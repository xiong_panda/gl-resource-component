package com.gl.basis.zuul.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppManage;
import com.gl.basis.zuul.service.ISysAppManageDalService;
import com.gl.basis.zuul.service.ISysAppManageService;

@Service
public class SysAppManageServiceImpl implements ISysAppManageService{

	@Autowired
	private ISysAppManageDalService sysAppManageDalService;
	
	@Override
	public List<SysAppManage> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public SysAppManage findSerct(String appKey) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("appKey", appKey);
		return sysAppManageDalService.findSerct(params);
	}

	@Override
	public void subtractionFreuency(String appKey) {
		sysAppManageDalService.subtractionFreuency(Collections.singletonMap("appKey", appKey));
	}

}
