package com.gl.basis.zuul.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigChangeListener;
import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfig;

import lombok.extern.slf4j.Slf4j;

/**
 * @Title: MyCommandLineRunner
 * @Description:启动开启分布式配置中心阿波罗监听
 * @author 937910825@qq.com
 * @date 2019年10月13日下午11:12:26
 */
@Component
@Slf4j
public class ApolloCommandLineRunner implements CommandLineRunner {
	@ApolloConfig
	private Config config;

	@Override
	public void run(String... args) throws Exception {

		log.info("####分布式配置中心监听启动#####");
		config.addChangeListener(new ConfigChangeListener() {

			@Override
			public void onChange(ConfigChangeEvent changeEvent) {
				log.debug("####分布式配置中心监听#####" + changeEvent.changedKeys().toString());
			}
		});
	}

}