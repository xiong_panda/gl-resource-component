package com.gl.basis.zuul.enums;

/**
 *
 */
public enum RouteType {

    ROUTE("route"),
    WHITE("white"),
    API("api"),
    BLACK("black"),;

    private String code;

    RouteType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
