package com.gl.basis.zuul.wrapper;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.http.HttpServletResponseWrapper;

public class ErrorHttpServletResponseWrapper extends  HttpServletResponseWrapper {

	
	
	public ErrorHttpServletResponseWrapper(HttpServletResponse response) {
		super(response);
		System.out.println("");
		RequestContext currentContext = RequestContext.getCurrentContext();
		InputStream inputStream = currentContext.getResponseDataStream();
		byte[] buf = new byte[4096];
		int length = 0;
		StringBuilder sb = new StringBuilder();
		try {
			while ((length = inputStream.read(buf)) != -1) {
				sb.append(new String(buf,0,length));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(sb.toString());
		
	}

}
