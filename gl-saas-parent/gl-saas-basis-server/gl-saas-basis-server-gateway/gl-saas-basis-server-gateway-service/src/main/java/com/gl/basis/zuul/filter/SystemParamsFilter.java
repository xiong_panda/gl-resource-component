package com.gl.basis.zuul.filter;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.basis.auth.constants.AuthorityConstants;
import com.gl.basis.common.constant.ResultCode;
import com.gl.basis.common.pojo.CommonResult;
import com.gl.basis.zuul.enums.RouteType;
import com.gl.basis.zuul.service.ISysAppParamtersService;
import com.gl.basis.zuul.util.RequestUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import lombok.extern.slf4j.Slf4j;

/**
 * 对公共参数校验
 * @author xiehong
 *
 */
@Slf4j
@Component
public class SystemParamsFilter  extends ZuulFilter {
	
	@Resource
	private ISysAppParamtersService sysAppParamtersService;
	@Resource
	private ObjectMapper objectMapper;
	@Override
	public String filterType() {
		return FilterConstants.PRE_TYPE;
	}

	@Override
	public int filterOrder() {
		return 10;
	}

	@Override
	public boolean shouldFilter() {
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
        String path = request.getServletPath();
        //包含api就不转发
		if (!currentContext.sendZuulResponse() ||( StringUtils.isNotBlank(path)&& path.contains(RouteType.API.getCode()))) {
			return false;
		}
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		log.info("参数校验");
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
		List<String> list = sysAppParamtersService.findCommParams();
		String requestParams = RequestUtils.getRequestParams(currentContext, request);
	    Map<String, String> reqParams = JSONObject.parseObject(requestParams, Map.class);
		if (!list.isEmpty()) {
			for (String params : list) {
				String parameter = reqParams.get(params);
				if (StringUtils.isBlank(parameter)) {
					currentContext.setSendZuulResponse(false);
					HttpServletResponse response = currentContext.getResponse();
					response.setContentType(AuthorityConstants.APPLICATION_JSON);
					try {
						currentContext.setResponseBody(objectMapper.writeValueAsString(CommonResult.failed(ResultCode.VALIDATE_FAILED)));
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					return null;
				}
			}
		}
		return null;
	}

	
}
