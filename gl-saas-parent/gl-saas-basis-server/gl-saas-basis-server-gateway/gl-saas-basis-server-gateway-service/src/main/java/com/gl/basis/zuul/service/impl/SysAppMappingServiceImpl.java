package com.gl.basis.zuul.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppMapping;
import com.gl.basis.zuul.service.ISysAppMappingDalService;
import com.gl.basis.zuul.service.ISysAppMappingService;

@Service
public class SysAppMappingServiceImpl implements ISysAppMappingService{

	@Autowired
	private ISysAppMappingDalService sysAppMappingDalService;
	
	@Override
	public List<SysAppMapping> getListByPage(Map<String, Object> params) {
		return null;
	}

	//@Cacheable("findAppMappingUrl")
	@Override
	public SysAppMapping findAppMappingUrl(String method) {
		return sysAppMappingDalService.findAppMappingUrl(method);
	}

}
