package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.zuul.entity.SysAppMapping;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ISysAppMappingService {

List<SysAppMapping> getListByPage(Map<String,Object> params);

/**
 * 查询映射路径
 * @param method
 */
SysAppMapping findAppMappingUrl(String method);

}
