package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.gl.basis.common.pojo.CommBean;
import com.gl.basis.zuul.entity.SysAppLog;

/**
 * service业务处理层,自定义方法请写在此接口中
 * @author code_generator
 */
public interface ISysAppLogService {

List<SysAppLog> getListByPage(Map<String,Object> params);

void addLog(CommBean reqParams,HttpServletRequest request);

}
