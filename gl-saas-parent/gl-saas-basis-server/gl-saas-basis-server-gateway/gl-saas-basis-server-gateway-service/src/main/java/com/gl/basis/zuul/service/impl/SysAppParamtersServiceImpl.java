package com.gl.basis.zuul.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.basis.zuul.entity.SysAppParamters;
import com.gl.basis.zuul.service.ISysAppParamtersDalService;
import com.gl.basis.zuul.service.ISysAppParamtersService;


@Service
public class SysAppParamtersServiceImpl implements ISysAppParamtersService{

	@Autowired
	private ISysAppParamtersDalService sysAppParamtersDalService;
	
	@Override
	public List<SysAppParamters> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Override
	public List<String> findCommParams() {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("state", "0");
		params.put("fEnable", "0");
		return sysAppParamtersDalService.findCommParams(params);
	}

}
