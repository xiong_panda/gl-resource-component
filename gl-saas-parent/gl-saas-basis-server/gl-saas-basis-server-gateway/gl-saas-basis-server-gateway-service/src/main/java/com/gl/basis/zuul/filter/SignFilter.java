package com.gl.basis.zuul.filter;

import java.io.IOException;
import java.util.Base64;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.basis.auth.constants.AuthorityConstants;
import com.gl.basis.common.constant.ResultCode;
import com.gl.basis.common.pojo.CommonResult;
import com.gl.basis.common.util.IpUtil;
import com.gl.basis.common.util.Md5Utils;
import com.gl.basis.common.util.NetworkUtil;
import com.gl.basis.common.util.RSAUtils;
import com.gl.basis.zuul.entity.SysAppManage;
import com.gl.basis.zuul.enums.RouteType;
import com.gl.basis.zuul.service.ISysAppManageService;
import com.gl.basis.zuul.util.CommonUtil;
import com.gl.basis.zuul.util.RequestUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import lombok.extern.slf4j.Slf4j;

/**
 * 签名验证
 * 
 * @author xiehong
 *
 */
@Slf4j
@Component
public class SignFilter extends ZuulFilter {
	private static final Logger logger = LoggerFactory.getLogger(SignFilter.class);

	@Resource
	private ISysAppManageService sysAppManageService;
	@Resource
	private ObjectMapper objectMapper;

	@Override
	public String filterType() {
		return FilterConstants.PRE_TYPE;
	}

	@Override
	public int filterOrder() {
		return 102;
	}

	@Override
	public boolean shouldFilter() {
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
        String path = request.getServletPath();
        //包含api就不转发
        if (!currentContext.sendZuulResponse() ||( StringUtils.isNotBlank(path)&& path.contains(RouteType.API.getCode()))) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object run() throws ZuulException {
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
		
		String requestParams = RequestUtils.getRequestParams(currentContext, request);
		Map<String, String> reqParams = JSONObject.parseObject(requestParams, Map.class);
		String reqsign = reqParams.get(AuthorityConstants.SIGN);
		String signMethod = reqParams.get(AuthorityConstants.SIGN_METHOD);
		String bzData = reqParams.get(AuthorityConstants.BZ_DATA);
		SysAppManage manage = sysAppManageService.findSerct(reqParams.get(AuthorityConstants.APP_KEY));
		
		try {
			String ipAddress = NetworkUtil.getIpAddress(request);
			log.info("访问IP：{}",ipAddress);
			if (manage == null) {
				currentContext.setSendZuulResponse(false);
				HttpServletResponse response = currentContext.getResponse();
				response.setContentType(AuthorityConstants.APPLICATION_JSON);
				currentContext.setResponseBody(objectMapper.writeValueAsString(CommonResult.failed(ResultCode.APPKEY_ERROR)));
				return null;
			} else if (manage != null && StringUtils.isBlank(manage.getReqIp())) {
				currentContext.setSendZuulResponse(false);
				HttpServletResponse response = currentContext.getResponse();
				response.setContentType(AuthorityConstants.APPLICATION_JSON);
				currentContext.setResponseBody(objectMapper.writeValueAsString(CommonResult.failed(ResultCode.IP_DISABLE_ERROR)));
				return null;
			}else if (!(manage.getReqIp().contains(ipAddress))) {
				currentContext.setSendZuulResponse(false);
				HttpServletResponse response = currentContext.getResponse();
				response.setContentType(AuthorityConstants.APPLICATION_JSON);
				currentContext.setResponseBody(objectMapper.writeValueAsString(CommonResult.failed(ResultCode.IP_DISABLE_ERROR)));
				return null;
			}		
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if (null != manage && StringUtils.isNotBlank(manage.getSignKey())
				&& AuthorityConstants.SIGN_MD5.equalsIgnoreCase(signMethod)) {
			String shaHex = Md5Utils.signShaHex(reqParams, manage.getSignKey());
			log.info("签名：{}", shaHex);
			if (reqsign.equalsIgnoreCase(shaHex)) {
				return null;
			}
		} else if (StringUtils.isNotBlank(bzData) && null != manage && StringUtils.isNotBlank(manage.getPublicKey())
				&& AuthorityConstants.SIGN_RSA.equalsIgnoreCase(signMethod)) {
			try {
				// 解密数据
				//TODO
				// 验证签名
				boolean flag = RSAUtils.verify(CommonUtil.buildWaitingForSign(reqParams).getBytes(), manage.getPublicKey(), reqsign);
				
				if (flag) {
					return null;
				}
			} catch (Exception e) {
				log.error("rsa解密失敗:{}",e.getMessage() ,e);
			}

		}

		currentContext.setSendZuulResponse(false);
		HttpServletResponse response = currentContext.getResponse();
		response.setContentType(AuthorityConstants.APPLICATION_JSON);
		try {
			currentContext.setResponseBody(objectMapper.writeValueAsString(CommonResult.failed(ResultCode.SIGN_ERROR)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
