package com.gl.basis.zuul.config;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 网关配置
 *
 */
@Component
@ConfigurationProperties(prefix = "zuul", ignoreUnknownFields = true)
public class ZuulConfig {

    /**
     * 	忽略校验逻辑的请求
     */
    private List<String> ignoredAuthPaths;

    /**
     *	 白名单请求地址
     */
    private List<String> whiteList;

    /**
     * 	黑名单请求地址
     */
    private List<String> blackList;

    /**
     * 	白名单请求地址
     */
    private List<String> whiteListExtenal;

    /**
     * 	黑名单请求地址
     */
    private List<String> blackListExtenal;

    public List<String> getIgnoredAuthPaths() {
        return ignoredAuthPaths;
    }

    public void setIgnoredAuthPaths(List<String> ignoredAuthPaths) {
        this.ignoredAuthPaths = ignoredAuthPaths;
    }

    public List<String> getWhiteList() {
        return whiteList;
    }

    public void setWhiteList(List<String> whiteList) {
        this.whiteList = whiteList;
    }

    public List<String> getBlackList() {
        return blackList;
    }

    public void setBlackList(List<String> blackList) {
        this.blackList = blackList;
    }

    public List<String> getWhiteListAll() {
        if (CollectionUtils.isNotEmpty(whiteListExtenal)) {
            return whiteListExtenal;
        }
        return whiteList;
    }

    public List<String> getBlackListAll() {
        if (CollectionUtils.isNotEmpty(blackListExtenal)) {
            return blackListExtenal;
        }
        return blackList;
    }

    public List<String> getWhiteListExtenal() {
        return whiteListExtenal;
    }

    public void setWhiteListExtenal(List<String> whiteListExtenal) {
        this.whiteListExtenal = whiteListExtenal;
    }

    public List<String> getBlackListExtenal() {
        return blackListExtenal;
    }

    public void setBlackListExtenal(List<String> blackListExtenal) {
        this.blackListExtenal = blackListExtenal;
    }
}
