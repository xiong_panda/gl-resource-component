package com.gl.basis.zuul.service;

import java.util.List;
import java.util.Map;

import com.gl.basis.zuul.entity.SysAppParamters;

/**
 * 请求公共参数
 * @author code_generator
 */
public interface ISysAppParamtersService {

List<SysAppParamters> getListByPage(Map<String,Object> params);

/**
 * 公共参数
 * @return
 */
List<String> findCommParams();

}
