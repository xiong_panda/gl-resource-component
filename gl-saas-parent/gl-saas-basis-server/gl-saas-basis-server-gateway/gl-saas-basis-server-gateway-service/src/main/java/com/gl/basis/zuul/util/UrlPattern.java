package com.gl.basis.zuul.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * url匹配
 * @author admin
 *
 */
public class UrlPattern {
    Logger logger= LoggerFactory.getLogger(getClass());

	public static boolean isMatch(String path, String o) {
		if (path.contains(o.replace("/**", ""))) {
			return true;
		}
		return false;
	}

}
