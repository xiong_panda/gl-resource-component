package com.gl.basis.zuul.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.auth.constants.AuthorityConstants;
import com.gl.basis.common.constant.ResultCode;
import com.gl.basis.common.pojo.CommBean;
import com.gl.basis.common.pojo.CommonResult;
import com.gl.basis.zuul.entity.SysAppMapping;
import com.gl.basis.zuul.enums.RouteType;
import com.gl.basis.zuul.service.ISysAppCustomerService;
import com.gl.basis.zuul.service.ISysAppMappingService;
import com.gl.basis.zuul.util.RequestUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *	 是否收费
 * @author admin
 *
 */
@Component
public class CollectFessFilter extends ZuulFilter {

	@Resource
	public ISysAppMappingService sysAppMappingService;
	@Resource
	public ISysAppCustomerService sysAppCustomerService;

	@Override
	public boolean shouldFilter() {
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
        String path = request.getServletPath();
        //包含api就不转发
        if (!currentContext.sendZuulResponse() ||( StringUtils.isNotBlank(path)&& path.contains(RouteType.API.getCode()))) {
			return false;
		}
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext currentContext = RequestContext.getCurrentContext();
		HttpServletRequest request = currentContext.getRequest();
		String requestParams = RequestUtils.getRequestParams(currentContext, request);
		CommBean reqParams = JSONObject.parseObject(requestParams, CommBean.class);

		SysAppMapping mappingUrl = sysAppMappingService.findAppMappingUrl(reqParams.getMethod());

		if (mappingUrl != null && StringUtils.isNotBlank(mappingUrl.getNeedfee())
				&& AuthorityConstants.COLLECTFESS_CODE.equals(mappingUrl.getNeedfee())
				&& mappingUrl.getCoolectFeesmoney() > 0) {
			// 验证用户金额是否有
			Integer money = sysAppCustomerService.findCustomerMoney(reqParams.getAppkey());
			if (money <= 0 || money < mappingUrl.getCoolectFeesmoney()) {
				currentContext.setSendZuulResponse(false);
				HttpServletResponse response = currentContext.getResponse();
				response.setContentType(AuthorityConstants.APPLICATION_JSON);
				currentContext.setResponseBody(JSON.toJSONString(CommonResult.failed(ResultCode.ARREARAGE_ERROR)));
			}
		}

		return null;
	}

	@Override
	public String filterType() {
		return FilterConstants.POST_TYPE;
	}

	@Override
	public int filterOrder() {
		return 300;
	}

}
