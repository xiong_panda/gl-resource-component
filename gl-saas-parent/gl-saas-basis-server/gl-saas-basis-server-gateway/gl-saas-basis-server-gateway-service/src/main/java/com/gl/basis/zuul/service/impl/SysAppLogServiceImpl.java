package com.gl.basis.zuul.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.gl.basis.common.pojo.CommBean;
import com.gl.basis.common.util.IdWorker;
import com.gl.basis.common.util.NetworkUtil;
import com.gl.basis.zuul.entity.SysAppLog;
import com.gl.basis.zuul.service.ISysAppLogDalService;
import com.gl.basis.zuul.service.ISysAppLogService;


/**
 * 日志更新
 * @author admin
 *
 */
@Service
public class SysAppLogServiceImpl implements ISysAppLogService{

	@Autowired
	private ISysAppLogDalService sysAppLogDalServicel;
	@Resource
	private IdWorker idWorker;
	@Override
	public List<SysAppLog> getListByPage(Map<String, Object> params) {
		return null;
	}

	@Async
	@Override
	public void addLog(CommBean reqParams,HttpServletRequest request) {
		SysAppLog log = new SysAppLog();
		log.setfPkid(idWorker.nextId()+"");
		log.setInterfaceName(reqParams.getMethod());
		try {
			log.setAccessIp(NetworkUtil.getIpAddress(request));
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.setAppKey(reqParams.getAppkey());
		log.setAccessTime(new Date());
		log.setParams(reqParams.getBz_data());
		log.setfInputtime(new Date());
		sysAppLogDalServicel.addlog(log);
	}

}
