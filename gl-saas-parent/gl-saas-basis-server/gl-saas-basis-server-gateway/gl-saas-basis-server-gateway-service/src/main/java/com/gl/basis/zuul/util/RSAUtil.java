package com.gl.basis.zuul.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import java.security.Signature;

import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 * RSA非对称加密解密
 *
 */
public class RSAUtil {
	/*** 私钥文件名 */
	private static final String PRIVKEY_NAME = "rsa_pri_key.pem";
	/*** 公钥文件名 */
	private static final String PUBKEY_NAME = "rsa_pub_key.pem";
	
	
	/**
	 * 生成经BASE64编码后的RSA公钥和私钥到文件系统
     * @param path 文件路径
     * 
     */
    public static void createKeyPairs(String path) {
    	try {
    		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
    		generator.initialize(1024, new SecureRandom());
    		KeyPair pair = generator.generateKeyPair();
    		PublicKey pubKey = pair.getPublic();
    		PrivateKey privKey = pair.getPrivate();
    		byte[] pubk = pubKey.getEncoded();
    		byte[] privk = privKey.getEncoded();
    		// base64编码，屏蔽特殊字符
    		String strpk = new String(Base64.encode(pubk));
    		String strprivk = new String(Base64.encode(privk));
    		
    		// 输出私钥文件
    		File priKeyfile = new File(path + "/" + PRIVKEY_NAME);
    		FileOutputStream out = new FileOutputStream(priKeyfile);
    		out.write(strprivk.getBytes());
    		// 输出公钥文件
    		File pubKeyfile = new File(path +"/" +PUBKEY_NAME);
    		FileOutputStream outPub = new FileOutputStream(pubKeyfile);
    		outPub.write(strpk.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }
	
    
    /**
     * RSA公钥加密
     * @param content	待加密的明文
     * @param pubKey	RSA公钥
     * @return	经BASE64编码后的密文
     */
    public static String pubKeyEnc(String content,String pubKey){
    	try {
    		KeyFactory keyf = KeyFactory.getInstance("RSA");
            //获取公钥
            InputStream is = new ByteArrayInputStream(pubKey.getBytes("utf-8"));
            byte[] pubbytes = new byte[new Long(pubKey.length()).intValue()];
            is.read(pubbytes);
            X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64.decode(new String(pubbytes)));
            PublicKey pkey = keyf.generatePublic(pubX509);
            
            //公钥加密
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pkey);
            byte[] cipherText = cipher.doFinal(content.getBytes());
           
            // 将加密结果转换为Base64编码结果；便于internet传送
            return Base64.encode(cipherText);
    	} catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
    	}
    }
    
    
    /**
     * RSA公钥解密
     * @param ciphertext 经BASE64编码过的待解密的密文
     * @param pubKey RSA公钥
     * @return utf-8编码的明文
     */
    public static String pubKeyDec(String ciphertext ,String pubKey){
    	try {
    		KeyFactory keyf = KeyFactory.getInstance("RSA");
			 
			//获取公钥
			InputStream is = new ByteArrayInputStream(pubKey.getBytes("utf-8"));
			byte[] pubbytes = new byte[new Long(pubKey.length()).intValue()];
			is.read(pubbytes);
			X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64.decode(new String(pubbytes)));
			PublicKey pkey = keyf.generatePublic(pubX509);
			
			//公钥解密
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, pkey);
			byte[] text = cipher.doFinal(Base64.decode(ciphertext));
			
			return new String(text,"UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }
    
    
    /**
     * RSA私钥加密
     * @param content 待加密的明文
     * @param privKey RSA私钥
     * @return	经BASE64编码后的密文
     */
    public static String privKeyEnc(String content,String privKey){
    	try {
    		KeyFactory keyf = KeyFactory.getInstance("RSA");
    		
            //获取私钥
            InputStream key = new ByteArrayInputStream(privKey.getBytes("utf-8"));
            byte[] pribytes = new byte[new Long(privKey.length()).intValue()];
            key.read(pribytes);
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(new String(pribytes)));
            PrivateKey prikey = keyf.generatePrivate(priPKCS8);
            
            //私钥加密
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, prikey);
            byte[] cipherText = cipher.doFinal(content.getBytes());
            
            //将加密结果转换为Base64编码结果；便于internet传送
            return Base64.encode(cipherText);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		}
    }
    
    
    /**
     * RSA私钥解密
     * @param ciphertext	经BASE84编码过的待解密密文
     * @param privKey	RSA私钥
     * @return	utf-8编码的明文
     */
    public static String privKeyDec(String ciphertext ,String privKey){
    	try {
    		KeyFactory keyf = KeyFactory.getInstance("RSA");
    		
            //获取私钥
            InputStream key = new ByteArrayInputStream(privKey.getBytes("utf-8"));
            byte[] pribytes = new byte[new Long(privKey.length()).intValue()];
            key.read(pribytes);
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(new String(pribytes)));
            PrivateKey prikey = keyf.generatePrivate(priPKCS8);
            
            //私钥解密
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, prikey);
            byte[] content = cipher.doFinal(Base64.decode(ciphertext));
            
            return new String(content,"UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		}
    }
    
    /**
     * RSA私钥数字签名
     * @param content 待签内容
     * @param privKey RSA私钥
     * @return 经BASE64编码后的签名串
     */
    public static String sign(String content,String privKey){
    	try {
			KeyFactory keyf=KeyFactory.getInstance("RSA");
			
			 //获取私钥
			InputStream key = new ByteArrayInputStream(privKey.getBytes("utf-8"));
			byte[] pribytes = new byte[new Long(privKey.length()).intValue()];
			key.read(pribytes);
			PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(new String(pribytes)));
			PrivateKey priKey=keyf.generatePrivate(priPKCS8);
			
			//实例化Signature；签名算法：MD5withRSA
			Signature signature = Signature.getInstance("MD5withRSA");
			//初始化Signature
			signature.initSign(priKey);
			//更新
			signature.update(content.getBytes());
			return Base64.encode(signature.sign());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }
    
    /**
     * RSA公钥校验数字签名
     * @param content 待校验的内容
     * @param pubKey RSA公钥
     * @param signedStr 签名字符串
     * @return	true:校验成功；false:校验失败
     */
    public static boolean verify(String content,String pubKey,String signedStr){
    	try {
    		//实例化密钥工厂  
			KeyFactory keyf=KeyFactory.getInstance("RSA");
			
			//获取公钥
			InputStream is = new ByteArrayInputStream(pubKey.getBytes("utf-8"));
			byte[] pubbytes = new byte[new Long(pubKey.length()).intValue()];
			is.read(pubbytes);
			X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64.decode(new String(pubbytes)));
			PublicKey pkey = keyf.generatePublic(pubX509);
   
			//实例化Signature；签名算法：MD5withRSA
			Signature signature = Signature.getInstance("MD5withRSA");  
			signature.initVerify(pkey);
			signature.update(content.getBytes());
			//验证
			return signature.verify(Base64.decode(signedStr));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }
    
    
    public static void main(String[] args) {
		//createKeyPairs("d:");
	
    	String privKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJNauHWqagYmf6UFQlAld0Bh41hTvCmlbs1rSqjjqKtVvq1LMKWQJ/2w/0mcxKTuTQAjoaBfRMJ6UI3VsmfRuSARVbLncfYnSFIqCsDYzQDA3i6YurkSw9lMzY9hlS1LOPiAoEeYjpZ+ogVpfwrH2AJilWPTN+rJ2Jw3FCXXt9KvAgMBAAECgYAxyfWJzIsGiPuYRq6hZgdlOa4XqjNp3vwRbK9NYZ8cRVyoMT++sXbRwXyA4veOwvZBlciG56nUDIArbIlbiwGB0QpV0zqdWVpPO1cQyxM3dnqZy3W6BQr0+XdK61qTDXr7Dj9NTaV5E/QNU7Uqi39PCUYLWvfMmR6EnwPKJVWMAQJBAMgL069s1iQfLyniEqRw7DYBWHexqU/e5Z1K587JEPTOss5M764GEJFNCPeomXyGxGoWLZJgus4ar/iBfnxIY2sCQQC8ke5qJGDkLpYsG/ZRiex6AaR8mgP7ztMUXN6qCkeHFdOl0BC80DbV37MbPgvsINfEdhhGuSJ4SK1h/XYMpCLNAkEAxuOrVTB6P3OZqqSQMFntH0x7LpW+ZiCQXbBJhDg32Y1gDOhFK9nvwua6UbCY2UwMIAVoza3KTHwQFJx6qwo1fQJATkhq6KeXjcMFVuR42prekDv0VEIAG15eZJq1WXQRA7R8+94nxKrPyPQoP8v/WRS9XnMEWv/qnnLVf9OgWVkelQJAEMwAijFy7DVeVXZ1OyH/KuBN3ZbJR7InsH1iWmI4hcUPnxqRujGxbpJkuB+gLy39EonV6zhxIAmZYSxsraf60A==";
    	String pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCTWrh1qmoGJn+lBUJQJXdAYeNYU7wppW7Na0qo46irVb6tSzClkCf9sP9JnMSk7k0AI6GgX0TCelCN1bJn0bkgEVWy53H2J0hSKgrA2M0AwN4umLq5EsPZTM2PYZUtSzj4gKBHmI6WfqIFaX8Kx9gCYpVj0zfqydicNxQl17fSrwIDAQAB";
       
		Map<String,String> params = new HashMap();
		params.put("method", "/sys/login");
		params.put("timestamp", "2020-6-17 16:30:59");
		params.put("appkey", "11");
		params.put("redirectUri", "http://192.168.2.247:89/login/#/login");
		params.put("version", "v0.01");
		params.put("sign_method", "rsa");
		params.put("format", "json");
		params.put("bz_data", "{\"password\":\"123456\",\"mobile\":\"13699466643\"}");
		
    	//String name = "this is a example哈哈";
    	System.out.println("------------私钥加密公钥解密------------------");
    	//私钥加密
    	//String smiwen = privKeyEnc(name, privKey);
    //	System.out.println("私钥加密结果："+smiwen);
    	//公钥解密
    	//String content = pubKeyDec(smiwen, pubKey);
    	//System.out.println("公钥还原内容："+content);
    	
    /*	System.out.println("-----------公钥加密私钥解密---------------");
    	//公钥加密
    	String pubcstr = pubKeyEnc(name, pubKey);
    	System.out.println("公钥加密结果："+pubcstr);
    	//私钥解密
    	String c2 = privKeyDec(pubcstr, privKey);
    	System.out.println("私钥还原内容："+c2);*/
    	
    	System.out.println("-----------私钥签名公钥验签--------------------");
    	String singedStr = sign(CommonUtil.buildWaitingForSign(params), privKey);
    	System.out.println("数据："+CommonUtil.buildWaitingForSign(params));
    	System.out.println("私钥签名结果："+singedStr);
    	System.out.println("验签结果："+verify(CommonUtil.buildWaitingForSign(params), pubKey, singedStr));
    	
    	//createKeyPairs("d:/");
	}
    
	
}
