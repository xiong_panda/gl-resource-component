package com.gl.basis.zuul;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.scheduling.annotation.EnableAsync;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;

/**
 * 
 * @Title: AppGateWay
 * @Description: 微服务网关
 * @author 937910825@qq.com
 * @date 2019年10月13日上午11:55:07
 */
@EnableAsync
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
@EnableApolloConfig
public class AppGateWay {

	public static void main(String[] args) {
		
		SpringApplication.run(AppGateWay.class, args);
	}

/*	@ApolloConfig
	private Config config;
	
	

	// 添加文档来源
	@Component
	@Primary
	class DocumentationConfig implements SwaggerResourcesProvider {
		@Override
		public List<SwaggerResource> get() {
			return resources();
		}

		@SuppressWarnings("unchecked")
		private List<SwaggerResource> resources() {
			// 从阿波罗平台获取配置文件
			String swaDocJson = config.getProperty("information.resources.zuul.swagger.document", null);
			JSONArray docJsonArray = JSONArray.parseArray(swaDocJson);
			@SuppressWarnings("rawtypes")
			List resources = new ArrayList<>();
			// 遍历集合数组
			for (Object object : docJsonArray) {
				JSONObject jsonObject = (JSONObject) object;
				String name = jsonObject.getString("name");
				String location = jsonObject.getString("location");
				String version = jsonObject.getString("version");
				resources.add(swaggerResource(name, location, version));
			}
		
			return resources;
		}

		private SwaggerResource swaggerResource(String name, String location, String version) {
			SwaggerResource swaggerResource = new SwaggerResource();
			swaggerResource.setName(name);
			swaggerResource.setLocation(location);
			swaggerResource.setSwaggerVersion(version);
			return swaggerResource;
		}

	}*/

}
