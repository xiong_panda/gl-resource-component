package com.gl.basis.zuul.outdto;

import lombok.Data;

/**
 * 路由
 * 
 * @author xiehong
 *
 */
public class Route {

	/**
	 * 路由id,黑白名单时不需要
	 */
	private String routeId;

	/**
	 * 路由路径
	 */
	private String path;

	/**
	 * 路由转发服务
	 */
	private String serviceId;

	/**
	 * 项目 web、app
	 */
	private String project;

	/**
	 * 路由类型 route、white、black
	 */
	private String routeType;

	public String getRouteId() {
		return routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

}
