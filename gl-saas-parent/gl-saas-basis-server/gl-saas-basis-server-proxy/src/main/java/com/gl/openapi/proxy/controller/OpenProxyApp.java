package com.gl.openapi.proxy.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 此服务只做代理业务中转
 * @author admin
 *
 */
@EnableEurekaClient
@SpringBootApplication
public class OpenProxyApp {

	public static void main(String[] args) {
		SpringApplication.run(OpenProxyApp.class, args);
	}
}
