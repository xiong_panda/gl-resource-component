package com.gl.basis.fastdfs;

import com.github.tobato.fastdfs.FdfsClientConfig;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

/**
 * @author YangGuoKUan
 */
@Import(FdfsClientConfig.class)
@EnableFeignClients
@SpringBootApplication
@EnableSwagger2Doc
public class FastdfsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastdfsApplication.class, args);
    }

}
