package com.gl.basis.fastdfs.service.impl;

import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadByteArray;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.fastdfs.service.FdfsUpAndDowService;
import com.gl.saas.basis.fastdfs.model.dto.FileDTO;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Service
public class FdfsUpAndDowServiceImpl implements FdfsUpAndDowService {

    @Autowired
    private FastFileStorageClient fastFileStorageClient;
    @Autowired
    private FdfsWebServer fdfsWebServer;
    /**
     *  文件上传
     *  最后返回fastDFS中的文件名称;group1/M00/01/04/CgMKrVvS0geAQ0pzAACAAJxmBeM793.doc
     * @param file 页面提交时文件
     * @return
     */
    @Override
    public FileDTO uploadFile(MultipartFile file) {
        if(null == file) {
            throw new BusinessException("上传文件为空");
        }
        FileDTO fileDTO = new FileDTO();
        //获取文件名称
        String originalFileName = file.getOriginalFilename();
        //获取文件类型
        String extension = FilenameUtils.getExtension(originalFileName);
        //获取文件大小
        long fileSize = file.getSize();
        StorePath  storePath = null;
        try {
            storePath = fastFileStorageClient.uploadFile(file.getInputStream(), fileSize, extension, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String filePath = this.getResourceAccessUrl(storePath);
        fileDTO.setFileName(originalFileName);
        fileDTO.setFilePath(filePath);

        return fileDTO;
    }


    @Override
    public byte[] downloadFile(String filePath) {
        String group = filePath.substring(0, filePath.indexOf("/"));
        String path = filePath.substring(filePath.indexOf("/") + 1);
        byte[] bytes = fastFileStorageClient.downloadFile(group, path, new DownloadByteArray());
        return bytes;
    }

    @Override
    public void delete(String filePath) {
        if(StringUtils.isBlank(filePath)) {
            throw new BusinessException("删除文件路径为空");
        }
        StorePath storePath = StorePath.parseFromUrl(filePath);
        fastFileStorageClient.deleteFile(storePath.getGroup(), storePath.getPath());
    }

    /**
     * 获取文件资源访问路径
     * @param storePath
     * @return
     */
    private String getResourceAccessUrl(StorePath storePath) {
        StringBuilder stringBuilder = new StringBuilder();
        if(StringUtils.isNotBlank(fdfsWebServer.getWebServerUrl())) {
            stringBuilder.append(fdfsWebServer.getWebServerUrl());
        }
        if(StringUtils.isNotBlank(storePath.getFullPath())) {
            stringBuilder.append(storePath.getFullPath());
        }
        String filePath = stringBuilder.toString();
        return filePath;
    }
}
