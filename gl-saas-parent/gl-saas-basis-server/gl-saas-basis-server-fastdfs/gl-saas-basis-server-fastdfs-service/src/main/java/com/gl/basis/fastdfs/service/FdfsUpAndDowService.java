package com.gl.basis.fastdfs.service;

import com.gl.saas.basis.fastdfs.model.dto.FileDTO;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author YangGuoKuan
 */
public interface FdfsUpAndDowService {

    /**
     * 上传文件
     * @param file
     * @return
     */

    FileDTO uploadFile(MultipartFile file);

    /**
     * 文件下载
     * @param filePath
     * @return
     */
    byte[] downloadFile(String filePath);

    /**
     * 文件删除
     * @param filePath
     */
    void delete(String filePath);

}
