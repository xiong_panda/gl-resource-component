package com.gl.basis.fastdfs.controller;


import com.gl.basis.common.exception.BusinessException;
import com.gl.basis.common.pojo.Page;
import com.gl.basis.fastdfs.service.FdfsUpAndDowService;
import com.gl.saas.basis.fastdfs.model.dto.FileDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * @Author: YangGuoKuan
 * @Date: 2019-10-15 11:42
 */

@Api(tags = {"文件服务接口"})
@RestController
@RequestMapping(value = "/file")
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FdfsUpAndDowService fdfsUpAndDowService;
    @Autowired
    private HttpServletRequest request;

    @PostMapping("/upload")
    @ApiOperation(notes = "文件上传",value ="upload")
    public Page upload(@RequestPart("file")MultipartFile file){
        if(null == file) {
            throw new BusinessException("上传文件为空");
        }
        logger.info("文件[{}]上传开始", file.getOriginalFilename());
        FileDTO fileDTO = fdfsUpAndDowService.uploadFile(file);
        logger.info("文件[{}]上传结束", file.getOriginalFilename());
        Page page = new Page();
        page.setResult(fileDTO);
        page.setSuccess(true);
        return page;
    }

    @GetMapping("/download")
    @ApiOperation(notes = "文件下载",value ="download" )
    public void download(HttpServletResponse response){
        String fileName = request.getParameter("fileName");
        String filePath = request.getParameter("filePath");
        logger.info("文件[{}]下载开始", fileName);
        byte[] data = fdfsUpAndDowService.downloadFile(filePath);
        logger.info("文件[{}]下载结束", fileName);
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            ServletOutputStream servletOutputStream = response.getOutputStream();
            IOUtils.write(data, servletOutputStream);
        } catch (IOException e) {
            throw new BusinessException(e.getMessage());
        }
    }

    @GetMapping("/downloadForByte")
    @ApiOperation(notes = "文件下载数据流",value ="download" )
    public byte[] downloadForByte(HttpServletRequest request) {
        String filePath = request.getParameter("filePath");
        logger.info("文件[{}]下载开始", filePath);
        byte[] data = fdfsUpAndDowService.downloadFile(filePath);
        logger.info("文件[{}]下载结束", filePath);
        return data;
    }

    @PostMapping("/delete")
    @ApiOperation(notes = "文件删除",value ="delete" )
    public Page delete(HttpServletRequest request){
        String filePath = request.getParameter("filePath");
        logger.info("文件[{}]删除开始", filePath);
        fdfsUpAndDowService.delete(filePath);
        logger.info("文件[{}]删除结束", filePath);
        Page page = new Page();
        page.setSuccess(true);
        return page;
    }

}
