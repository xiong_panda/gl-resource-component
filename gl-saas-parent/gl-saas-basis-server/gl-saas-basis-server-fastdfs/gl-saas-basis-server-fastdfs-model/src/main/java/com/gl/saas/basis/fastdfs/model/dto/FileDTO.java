package com.gl.saas.basis.fastdfs.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author YangGuoKuan
 * @date 2019/10/25 15:18
 */
@Data
public class FileDTO implements Serializable {
    private static final long serialVersionUID = -2102578413633323658L;
    private String fileName;
    private String filePath;
}
