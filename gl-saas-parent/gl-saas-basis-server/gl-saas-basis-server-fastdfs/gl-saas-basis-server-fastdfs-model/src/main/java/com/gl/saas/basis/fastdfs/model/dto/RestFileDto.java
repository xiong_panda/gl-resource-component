package com.gl.saas.basis.fastdfs.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
public class RestFileDto implements Serializable {
  private MultipartFile file;
}
