package com.gl.basis.mongodb.constant;

/**
 * TODO
 *
 * @author
 * @date 2019/6/10 9:56 AM
 */
public interface MongoConstants {

    String COLLECTION_CAR_TASK_RECORD = "MongoCarTaskRecord";

    String COLLECTION_CAR_CLEAN_RECORD = "MongoCarCleanRecord";

    String COLLECTION_COMMENT_COUNT_RECORD = "MongoCommentCount";

    String COLLECTION_CAR_SERVICE_TASK_RECORD = "MongoCarTaskRecord";

    String COLLECTION_CAR_WORK_RECORD = "MongoCarWorkRecord";

    String COLLECTION_CAR_TASK_LOG_RECORD = "MongoCarTaskLogRecord";

    String COLLECTION_CAR_WORK_LOG_RECORD = "MongoCarWorkLogRecord";

    String COLLECTION_TRAIN_EXAM_RECORD = "MongoTrainExamRecord";

    String COLLECTION_TRAIN_EXAM_RENEW_EVENT_RECORD = "MongoTrainExamRenewEventRecord";

    String COLLECTION_CITY_RULE_CONFIG_LOG_RECORD = "MongoCityRuleConfigLogRecord";

    String COLLECTION_DRIVER_AUTH_LOG = "MongoDriverAuthLog";

    String COLLECTION_TEST = "Test";

    String COLLECTION_GOFUN_USER_FRONT = "MongoGfUserFront";
    String COLLECTION_SCHEDULE_JOB = "MongoScheduleJob";
    /**
     * 默认还车网点缓存
      * @param null
     * @author guojingsui
     * @date 2019/9/25 16:24
     */
    String COLLECTION_OPTIMIZE_RETURN_PARKING = "MongoOptimizeReturnParking";
}
