package com.gl.basis.mongodb.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "datasource.data.mongodb.option")
public class MongoOptionProperties {
	
	private List<String> address;
	//# 为集群设置所需的副本集名称
	private String replicaSet;
	private String database;
	private String username;
	private String password;
	
	//# Configure spring.data.mongodbDB Pool
	//#客户端最小连接数
    private Integer minConnectionPerHost = 0;
    //#客户端最大连接数，超过了将会被阻塞，默认100
    private Integer maxConnectionPerHost = 100;
    //#可被阻塞的线程数因子，默认值为5，如果connectionsPerHost配置为10，那么最多能阻塞50个线程，超过50个之后就会收到一个异常
    private Integer threadsAllowedToBlockForConnectionMultiplier = 5;
    //#服务器查询超时时间，它定义驱动在抛出异常之前等待服务器查询成功，默认30s,单位milliseconds
    private Integer serverSelectionTimeout = 30000;
    //#阻塞线程获取连接的最长等待时间，默认120000 ms
    private Integer maxWaitTime = 120000;
    //#连接池连接最大空闲时间
    private Integer maxConnectionIdleTime = 0;
    //#连接池连接的最大存活时间
    private Integer maxConnectionLifeTime = 0;
    //#连接超时时间，默认值是0，就是不超时
    private Integer connectTimeout = 10000;
    //socket超时时间，默认值是0，就是不超时
    private Integer socketTimeout = 0;
   //keep alive标志，默认false
    private Boolean socketKeepAlive = false;
    //#驱动是否使用ssl进行连接，默认是false
    private Boolean sslEnabled = false;
    //#定义是否允许使用无效的主机名
    private Boolean sslInvalidHostNameAllowed = false;
    //#设置由驱动程序注册的JMX bean是否应该始终是mbean，而不管VM是Java 6还是更大
    private Boolean alwaysUseMBeans = false;
    //#驱动用来确保集群中服务器状态的心跳频率
    private Integer heartbeatFrequency = 10000;
    //#驱动重新检查服务器状态最少等待时间
    private Integer minHeartbeatFrequency = 500;
    //集群心跳连接的超时时间
    private Integer heartbeatConnectTimeout = 20000;
    //#集群心跳连接的socket超时时间
    private Integer heartbeatSocketTimeout = 20000;
    //#设置本地阈值
    private Integer localThreshold = 15;
    
    
    /**
     * 
		#read-preference：MongoDB有5种ReadPreference模式：
		#    primary    主节点，默认模式，读操作只在主节点，如果主节点不可用，报错或者抛出异常。
		#    primaryPreferred   首选主节点，大多情况下读操作在主节点，如果主节点不可用，如故障转移，读操作在从节点。
		#    secondary    从节点，读操作只在从节点， 如果从节点不可用，报错或者抛出异常。
		#    secondaryPreferred    首选从节点，大多情况下读操作在从节点，特殊情况（如单主节点架构）读操作在主节点。
		#    nearest    最邻近节点，读操作在最邻近的成员，可能是主节点或者从节点。
		#write-concern：WriteConcern的7种写入安全机制抛出异常的级别：
		#    NONE:没有异常抛出
		#    NORMAL:仅抛出网络错误异常，没有服务器错误异常，写入到网络就返回
		#    SAFE:抛出网络错误异常、服务器错误异常；并等待服务器完成写操作。
		#    MAJORITY: 抛出网络错误异常、服务器错误异常；并多数主服务器完成写操作。
		#    FSYNC_SAFE: 抛出网络错误异常、服务器错误异常；写操作等待服务器将数据刷新到磁盘。
		#    JOURNAL_SAFE:抛出网络错误异常、服务器错误异常；写操作等待服务器提交到磁盘的日志文件。
		#    REPLICAS_SAFE:抛出网络错误异常、服务器错误异常；等待至少2台服务器完成写操作。

     */

}