package com.gl.common.mybatis;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.gl.common.mapper.IBaseMapper;
import com.gl.common.mybatis.annotation.MapperPrimary;

import tk.mybatis.spring.mapper.MapperScannerConfigurer;

/**
 * MyBatis扫描接口,通用mapper的集成，使用的tk.mybatis.spring.mapper.MapperScannerConfigurer<br/>
 * 如果你不使用通用Mapper，可以改为org.xxx...
 * @author 
 */
@Configuration
@AutoConfigureAfter(MybatisPrimaryConfig.class)
public class MyBatisPrimaryMapperScannerConfig {
	private static final Logger logger = LoggerFactory.getLogger(MyBatisPrimaryMapperScannerConfig.class);
	
	
	/**
	 * 配置通用mapper 和MapperScannerConfigurer  mapper包扫描 ,需要在sqlSessionFactory初始化后再初始化
	 */
	@Primary
	@Bean(name="mapperScannerConfigurer")
    public MapperScannerConfigurer mapperScannerConfigurer() {
		logger.info("===>>>初始化主数据源mybatis的MapperScanner");
		
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("com.gl.**.mapper");
        mapperScannerConfigurer.setAnnotationClass(MapperPrimary.class);
        //mapperScannerConfigurer.setMarkerInterface(Mapper.class);//直接继承了Mapper接口的才会被扫描
        
        //通用mapper配置
        Properties properties = new Properties();
        // 这里要特别注意，不要把IBaseMapper放到 basePackage 中，也就是不能同其他Mapper一样被扫描到。 
        properties.setProperty("mappers", IBaseMapper.class.getName()); //自定义mapper配置
        //properties.setProperty("IDENTITY", "MYSQL");
        properties.setProperty("notEmpty", "true");
        //这里特别注意大小写
        properties.setProperty("ORDER", "BEFORE");
        mapperScannerConfigurer.setProperties(properties);
        return mapperScannerConfigurer;
    }
	
	
}
