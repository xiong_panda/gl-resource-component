package com.gl.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "druid")
public class DruidStatConfig {

	private String userName="admin";
	private String password="123456";
	
	private boolean resetEnable=false;
	
	public String getUserName() {
		return userName;
	}
	public String getPassword() {
		return password;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isResetEnable() {
		return resetEnable;
	}
	public void setResetEnable(boolean resetEnable) {
		this.resetEnable = resetEnable;
	}
	
}
