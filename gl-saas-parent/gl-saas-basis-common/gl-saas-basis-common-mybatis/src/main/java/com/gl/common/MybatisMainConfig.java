package com.gl.common;

import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.JdkRegexpMethodPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;
import com.gl.common.config.DruidStatConfig;
import com.gl.common.listener.AbandonedConnectionCleanupTheardListener;
import com.gl.common.mybatis.MyBatisPrimaryMapperScannerConfig;
import com.gl.common.mybatis.MybatisPrimaryConfig;


/**
 * mybatis配置
 * 在需要使用的项目中引入该配置,并存在配置:classpath:config/app/jdbc.properties
 * mapper的xml文件需要写在:classpath:config/mapper/目录下
 * @author
 *
 */
@Configuration
@Import(value={
		MybatisPrimaryConfig.class, //mybatis配置
		MyBatisPrimaryMapperScannerConfig.class, //mybatis mapper 扫描配置
})
@EnableConfigurationProperties({ DruidStatConfig.class })
@ServletComponentScan(basePackageClasses = {AbandonedConnectionCleanupTheardListener.class})
public class MybatisMainConfig {

	@Autowired
	private DruidStatConfig druidStatConfig;

	@Bean
	public FilterRegistrationBean druidWebStatFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		//设置过滤器
		filterRegistrationBean.setFilter(new WebStatFilter());
		//添加过滤规则.
	    filterRegistrationBean.addUrlPatterns("/*");
	    //添加不需要忽略的格式信息.
	    filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,*.swf,/druid/*");
	    filterRegistrationBean.addInitParameter("principalSessionName", "sessionInfo");
	    filterRegistrationBean.addInitParameter("profileEnable", "true");
	    //filter在AnonAuthFilter前面
	    filterRegistrationBean.setOrder(0);
	    return filterRegistrationBean;
	}

	@Bean
    public ServletRegistrationBean DruidStatViewServlet(){
       ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(),"/druid/*");
       //添加初始化参数：initParams
       //白名单：
       //servletRegistrationBean.addInitParameter("allow","127.0.0.1");

       //IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not permitted to view this page.
       //servletRegistrationBean.addInitParameter("deny","192.168.1.73");

       //登录查看信息的账号密码.
       servletRegistrationBean.addInitParameter("loginUsername",druidStatConfig.getUserName());
       servletRegistrationBean.addInitParameter("loginPassword",druidStatConfig.getPassword());
       //是否能够重置数据.
       servletRegistrationBean.addInitParameter("resetEnable",String.valueOf(druidStatConfig.isResetEnable()));
       return servletRegistrationBean;
    }

	@Bean
    public DruidStatInterceptor druidStatInterceptor() {
        DruidStatInterceptor druidStatInterceptor = new DruidStatInterceptor();
        return druidStatInterceptor;
    }

	@Bean
	@Scope("prototype")
	public JdkRegexpMethodPointcut druidStatPointcut() {
		JdkRegexpMethodPointcut pointcut = new JdkRegexpMethodPointcut();
		pointcut.setPattern("com.gl.*.service.*");
		return pointcut;
	}

	@Bean
	public DefaultPointcutAdvisor druidStatAdvisor(DruidStatInterceptor druidStatInterceptor,
			JdkRegexpMethodPointcut druidStatPointcut) {
		DefaultPointcutAdvisor defaultPointAdvisor = new DefaultPointcutAdvisor();
		defaultPointAdvisor.setPointcut(druidStatPointcut);
		defaultPointAdvisor.setAdvice(druidStatInterceptor);
		return defaultPointAdvisor;
	}

}
