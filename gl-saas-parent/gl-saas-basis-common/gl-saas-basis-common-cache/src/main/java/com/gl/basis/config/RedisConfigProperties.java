package com.gl.basis.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
@Configuration
@Component
@ConfigurationProperties(prefix = "spring.redis")
@PropertySource(value={"classpath:META-INF/app.properties"},encoding="UTF-8")
public class RedisConfigProperties {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private int port;
    @Value("${spring.redis.password}")
    private String password;
    @Value("${spring.redis.database}")
    private int database;
    @Value("${spring.redis.pool.max-active}")
    private String poolMaxActive;
    @Value("${spring.redis.pool.max-wait}")
   private String poolMaxIdle;
    @Value("${spring.redis.pool.min-idle}")
   private  String poolMinIdle;
    @Value("${spring.redis.timeout}")
    private long timeOut;
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getDatabase() {
		return database;
	}
	public void setDatabase(int database) {
		this.database = database;
	}
	public String getPoolMaxActive() {
		return poolMaxActive;
	}
	public void setPoolMaxActive(String poolMaxActive) {
		this.poolMaxActive = poolMaxActive;
	}
	public String getPoolMaxIdle() {
		return poolMaxIdle;
	}
	public void setPoolMaxIdle(String poolMaxIdle) {
		this.poolMaxIdle = poolMaxIdle;
	}
	public String getPoolMinIdle() {
		return poolMinIdle;
	}
	public void setPoolMinIdle(String poolMinIdle) {
		this.poolMinIdle = poolMinIdle;
	}
	public long getTimeOut() {
		return timeOut;
	}
	public void setTimeOut(long timeOut) {
		this.timeOut = timeOut;
	}
}
