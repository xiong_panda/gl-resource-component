package com.gl.basis.constant;

public interface CacheConstants {


	/** cache 缓存前缀 **/
	public String CACHE_PREFIX="CACHE:";


	/** cache 缓存前缀 **/
	public String USER_NAME="USER_NAME:";

	/** cache 缓存前缀 **/
	public String SMS="SMS:";

	public String BRAND_LIST = "BRAND_LIST:";

	public String PART_LIST = "PART_LIST:";

	public String LOCALTION_LIST = "LOCALTION_LIST:";

	public String COLLABORATION = "COLLABORATION:";

	public String ENTERPRISE_TYPE_DICTIONARY = "ENTERPRISE_TYPE_DICTIONARY:";

	public String WEARHOUSE = "WEARHOUSE:";
}
