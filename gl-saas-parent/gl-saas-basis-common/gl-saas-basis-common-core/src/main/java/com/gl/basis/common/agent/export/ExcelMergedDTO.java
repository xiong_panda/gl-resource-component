package com.gl.basis.common.agent.export;

import lombok.Data;

/**
 * 合并单元格内容
 */
@Data
public class ExcelMergedDTO {
    /**
     * 行
     */
    Integer mergedBeginRow;
    Integer mergedEndRow;
    Integer mergedBeginCol;
    Integer mergedEndCol;
}
