package com.gl.basis.common.agent.export;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;


@Data
public abstract class ExcelDTO implements Serializable, ExceleImpl {
    /**
     * 列字段
     */
    String filed;
    /**
     * 列名称
     */
    @NonNull
    String name;
    /**
     * 批注
     */
    String comment;
    /**
     * 背景色
     */
    Short backgroundColor;
    /**
     * 字体大小
     */
    @NonNull
    Short fontHeightInPoints;
    /**
     * 字体颜色
     */
    Short fontColor;
    /**
     * 行高
     */
    Short rowHeight;
    /**
     * 列宽
     */
    Short colWidth;

    public ExcelDTO() {
    }


}
