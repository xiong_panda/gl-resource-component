package com.gl.basis.common.pojo;

/**
 * 接收实体类
 * @author xiehong
 *
 */
public class CommBean {

	private String method;
	private String sign;
	private String timestamp;
	private String appkey;
	private String redirectUrl;
	private String sign_method ="";
	private String format;
	//参数放于此
	private String bz_data;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getSign_method() {
		return sign_method;
	}

	public void setSign_method(String sign_method) {
		this.sign_method = sign_method;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getBz_data() {
		return bz_data;
	}

	public void setBz_data(String bz_data) {
		this.bz_data = bz_data;
	}

	

}
