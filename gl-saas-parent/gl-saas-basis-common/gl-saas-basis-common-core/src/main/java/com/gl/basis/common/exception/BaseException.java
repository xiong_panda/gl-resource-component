package com.gl.basis.common.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import com.gl.basis.common.constant.ResultCode;
import com.google.protobuf.ByteString;

public class BaseException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private Throwable exception;
	private String message =ResultCode.INTERNAL_SERVER_ERROR.getMsg();
	private String code = ResultCode.INTERNAL_SERVER_ERROR.getCode();
	private ByteString data = null;
	private String causeType; //异常类型
	
	private Map<String,Object> map = new HashMap<>();

	public BaseException() {
		initCause(null);
	}

	public BaseException(String msg) {
		super(msg);
		initCause(null);
		this.message = msg;
	}

	public BaseException(String msg, String code) {
		super(msg);
		initCause(null);
		this.message = msg;
		this.code = code;
	}
	
	public BaseException(String msg, String code,ByteString data) {
		super(msg);
		initCause(null);
		this.message = msg;
		this.code = code;
		this.data = data;
	}
	
	public BaseException(Throwable thrown) {
		initCause(null);
		exception = thrown;
	}

	public BaseException(String msg, String code, Throwable thrown) {
		super(msg);
		initCause(null);
		this.message = msg;
		this.code = code;
		this.exception = thrown;
	}
	
	public BaseException(String message, String code, Map<String, Object> params) {
		super();
		this.message = message;
		this.code = code;
		this.map = params;
	}

	public BaseException(Throwable exception, String message, String code, Map<String, Object> params) {
		super();
		this.exception = exception;
		this.message = message;
		this.code = code;
		this.map = params;
	}

	public void printStackTrace() {
		printStackTrace(System.err);
	}

	public void printStackTrace(PrintStream outStream) {
		printStackTrace(new PrintWriter(outStream));
	}

	public void printStackTrace(PrintWriter writer) {
		super.printStackTrace(writer);
		if (getException() != null) {
			getException().printStackTrace(writer);
		}
		writer.flush();
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public ByteString getData() {
		return data;
	}

	public void setData(ByteString data) {
		this.data = data;
	}

	public String getCauseType() {
		return causeType;
	}

	public void setCauseType(String causeType) {
		this.causeType = causeType;
	}
	
}