package com.gl.basis.common.exception;

import java.util.Map;

import com.gl.basis.common.constant.IBaseConstants;
import com.google.protobuf.ByteString;

/**
 * 框架异常
 */
public class FrameworkException extends BaseException{
	private static final long serialVersionUID = 1L;
	private String causeType = IBaseConstants.FRAMEWORK_EXCEPTION;

	public FrameworkException(String message) {
		super(message);
		super.setCauseType(causeType);
	}

	public FrameworkException(String message, String code) {
		super(message, code);
		super.setCauseType(causeType);
	}
	
	public FrameworkException(String message, String code,ByteString data) {
		super(message, code,data);
		super.setCauseType(causeType);
	}
	
	public FrameworkException(String message, String code, Map<String,Object> map) {
		super(message, code, map);
		super.setCauseType(causeType);
	}

	public FrameworkException(Throwable exception) {
		super(exception);
		super.setCauseType(causeType);
	}

	public FrameworkException(String message, String code, Throwable exception) {
		super(message, code, exception);
		super.setCauseType(causeType);
	}
}
