package com.gl.basis.common.agent;

public enum PriceChannel {
    CHANNEL_NOE("one", "一级价格"),
    CHANNEL_TWO("two", "二级价格"),
    CHANNEL_THREE("three", "三级价格");
    private String channelCode;
    private String channelName;

    public String getChannelCode() {
        return channelCode;
    }
    public String getChannelName() {
        return channelName;
    }
    PriceChannel(String channelCode, String channelName) {
        this.channelCode = channelCode;
        this.channelName = channelName;
    }

    public static PriceChannel build(String str) {
        for (PriceChannel value : PriceChannel.values()) {
            if (str.equals(value.channelCode)) {
                return value;
            }
        }
        return CHANNEL_NOE;
    }

}
