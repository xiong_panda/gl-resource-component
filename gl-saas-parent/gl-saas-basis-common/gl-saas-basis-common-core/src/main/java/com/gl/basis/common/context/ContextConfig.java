package com.gl.basis.common.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 公共上下文组件配置
 *
 */
@Configuration
public class ContextConfig {

	/**
	 * spring 上下文工具类
	 * @return
	 */
	@Bean
	ContextUtils contextUtils(){
		return new ContextUtils();
	}
	
}
