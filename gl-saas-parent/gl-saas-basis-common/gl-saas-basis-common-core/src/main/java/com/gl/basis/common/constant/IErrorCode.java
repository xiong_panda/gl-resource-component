package com.gl.basis.common.constant;

/**
 * 封装API的错误码
 */
public interface IErrorCode {
    String getCode();

    String getMsg();
}
