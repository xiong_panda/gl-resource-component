
package com.gl.basis.common.agent;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 日期工具
 *
 * @author 小江
 */
public class DateUtils {
    public static String ZH_PATTERN_MONTH = "yyyy-MM";
    public static String ZH_PATTERN_DAY = "yyyy-MM-dd";
    public static String ZH_PATTERN_HOUR = "yyyy-MM-dd HH";
    public static String ZH_PATTERN_MINUTE = "yyyy-MM-dd HH:mm";
    public static String ZH_PATTERN_SECOND = "yyyy-MM-dd HH:mm:ss";
    public static String ZH_PATTERN_MONTH_DD = "MM-dd";


    public static long currentTimeSecs() {
        return Clock.systemUTC().millis() / 1000;
    }

    public static long currentTimeMillis() {
        return Clock.systemUTC().millis();
    }

    public static String dateTimeFormatter(String pattern) {
        return dateTimeFormatter(pattern, LocalDateTime.now());
    }

    public static String dateTimeFormatter(String pattern, LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDate dateToLocalDate(Date date) {
        return dateToLocalDateTime(date).toLocalDate();
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    public static Date localDateToDate(LocalDate localDate) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDate.atStartOfDay().atZone(zone).toInstant();
        return Date.from(instant);
    }

    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * 年-月-日(2016-01-01)转localDate
     */
    public static LocalDate stringToLocalDate(String date) {
        DateTimeFormatter germanFormatter = DateTimeFormatter.ofPattern(ZH_PATTERN_DAY).withLocale(Locale.CHINESE);
        return LocalDate.parse(date, germanFormatter);
    }


    public static LocalDate stringToLocalDate(String date, String pattern) {
        DateTimeFormatter germanFormatter = DateTimeFormatter.ofPattern(pattern).withLocale(Locale.CHINESE);
        return LocalDate.parse(date, germanFormatter);
    }

    /**
     * 年月日(20160101)转localDate
     */
    public static LocalDate stringToLocalDate(Long date) {
        DateTimeFormatter germanFormatter = DateTimeFormatter.ofPattern("yyyyMMdd").withLocale(Locale.CHINESE);
        return LocalDate.parse(date.toString(), germanFormatter);
    }

    /**
     * @param date yyyy-MM-dd HH:mm:ss
     * @return LocalDateTime
     */
    public static LocalDateTime stringToLocalDatetime(String date) {
        DateTimeFormatter germanFormatter = DateTimeFormatter.ofPattern(ZH_PATTERN_SECOND);
        return LocalDateTime.parse(date, germanFormatter);
    }


    /**
     * unix 时间转 localDate
     */
    public static long unixTime(LocalDate localDate) {
        ZoneId zoneId = ZoneId.systemDefault();
        return localDate.atStartOfDay(zoneId).toEpochSecond();
    }

    public static long unixTime(LocalDateTime localDateTime) {
        ZoneId zoneId = ZoneId.systemDefault();
        return localDateTime.atZone(zoneId).toEpochSecond();
    }


    /**
     * unix 转localDateTime
     */
    public static LocalDateTime unixTimeToLocalDateTime(long unixTime) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(unixTime), ZoneId.systemDefault());
    }

    /**
     * 毫秒时间戳转字符日期
     *
     * @param unixTimeStamp 毫秒时间戳
     * @param pattern       需要的格式
     * @return 字符日期
     */
    public static String longToDateString(long unixTimeStamp, String pattern) {
        return unixMillsToLocalDateTime(unixTimeStamp).format(DateTimeFormatter.ofPattern(pattern));
    }


    public static LocalDateTime unixMillsToLocalDateTime(long timeMills) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timeMills), ZoneId.systemDefault());
    }

    /**
     * unix 转LocalDate
     */
    public static LocalDate unixTimeToLocalDate(long unixTime) {
        return unixTimeToLocalDateTime(unixTime).toLocalDate();
    }

    public static LocalDate unixMillsToLocalDate(long timeMills) {
        return unixMillsToLocalDateTime(timeMills).toLocalDate();
    }


    /**
     * 字符串转秒时间戳
     *
     * @param dateString 字符日期
     * @param pattern    字符日期的格式
     * @return
     */
    public static long stringDateToTimeStampSecs(String dateString, String pattern) {
        return unixTime(stringToLocalDate(dateString, pattern));
    }

    /**
     * 字符转毫秒时间戳
     *
     * @param dateString 字符日期
     * @param pattern    字符日期的格式
     * @return
     */
    public static long stringDateToTimeStampMillis(String dateString, String pattern) {
        return unixTime(stringToLocalDate(dateString, pattern)) * 1000;
    }

    /**
     * 根据系统时间获取本周星期一凌晨时间
     *
     * @param
     * @return
     * @author xiaojiang
     **/
    public static long getTimeInMillisOfMondayMorning() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(DateUtils.currentTimeMillis());
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1) {
            dayOfWeek += 7;
        }
        cal.add(Calendar.DATE, 2 - dayOfWeek);
        return getDayStartTimeInMillis(cal.getTime());
    }

    /**
     * 根据系统时间获取本周星期一凌晨时间
     **/
    public static long getTimeInSecondOfMondayMorning() {
        return getTimeInMillisOfMondayMorning() / 1000;
    }

    /**
     * 根据系统时间获取本周星期日傍晚时间
     **/
    public static long getTimeInSecondOfSundayEvening() {
        return getTimeInMillisOfSundayEvening() / 1000;
    }

    /**
     * 根据系统时间获取本周星期日傍晚时间
     **/
    public static long getTimeInMillisOfSundayEvening() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(getTimeInMillisOfMondayMorning());
        cal.add(Calendar.DAY_OF_WEEK, 6);
        Date weekEndSta = cal.getTime();
        return getDayEndTimeInMillis(weekEndSta);
    }

    /**
     * 获取某个日期的开始时间
     **/
    public static long getDayStartTimeInSecond(Date date) {
        return getDayStartTimeInMillis(date) / 1000;
    }

    /**
     * 获取今天的开始时间
     **/
    public static long getDayStartTimeInMillis() {
        return getDayStartTimeInMillis(new Date());
    }

    /**
     * 获取今天的开始时间
     **/
    public static long getDayStartTimeInSecond() {
        return getDayStartTimeInMillis() / 1000;
    }

    /**
     * 获取某个日期的开始时间
     **/
    public static long getDayStartTimeInMillis(Date date) {
        Calendar calendar = Calendar.getInstance();
        if (null != date) {
            calendar.setTime(date);
        }
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * 获取今天的结束时间
     **/
    public static long getDayEndTimeInMillis() {
        return getDayEndTimeInMillis(new Date());
    }

    /**
     * 获取今天的结束时间
     **/
    public static long getDayEndTimeInSecond() {
        return getDayEndTimeInMillis() / 1000;
    }

    /**
     * 获取某个日期的结束时间
     **/
    public static long getDayEndTimeInSecond(Date date) {
        return getDayEndTimeInMillis(date);
    }

    /**
     * 获取某个日期的结束时间
     *
     * @param date
     * @return
     * @author xiaojiang
     **/
    public static long getDayEndTimeInMillis(Date date) {
        Calendar calendar = Calendar.getInstance();
        if (null != date) {
            calendar.setTime(date);
        }
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    /**
     * 取某月开始时间
     *
     * @param date
     * @return
     * @author xiaojiang
     **/
    public static long getMonthStartTimeInMills(Date date) {
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
        }
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        return getDayStartTimeInMillis(c.getTime());
    }

    /**
     * 取某月开始时间
     *
     * @param date
     * @return
     * @author xiaojiang
     **/
    public static long getMonthStartTimeInSecond(Date date) {
        return getMonthStartTimeInMills(date) / 1000;
    }

    /**
     * 取本月开始时间
     *
     * @return
     * @author xiaojiang
     **/
    public static long getCurrentMonthStartTimeInMillis() {
        return getMonthStartTimeInMills(new Date());
    }

    /**
     * 取本月开始时间
     *
     * @return
     * @author xiaojiang
     **/
    public static long getCurrentMonthStartTimeInSecond() {
        return getMonthStartTimeInSecond(new Date());
    }

    /**
     * 取某月结束时间
     *
     * @param date
     * @return
     * @author xiaojiang
     **/
    public static long getMonthEndTimeInMills(Date date) {
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
        }
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return getDayEndTimeInMillis(c.getTime());
    }

    /**
     * 取某月结束时间
     *
     * @param date
     * @return
     * @author xiaojiang
     **/
    public static long getMonthEndTimeInSecond(Date date) {
        return getMonthEndTimeInMills(date) / 1000;
    }

    /**
     * 取本月结束时间
     *
     * @return
     * @author xiaojiang
     **/
    public static long getCurrentMonthEndTimeInMillis() {
        return getMonthEndTimeInMills(new Date());
    }

    /**
     * 取本月结束时间
     *
     * @return
     * @author xiaojiang
     **/
    public static long getCurrentMonthEndTimeInSecond() {
        return getMonthEndTimeInSecond(new Date());
    }

    /**
     * 获取天
     *
     * @param second
     * @return
     * @author xiaojiang
     **/
    public static int getYearFromSecond(long second) {
        return getYearFromMillis(second * 1000);
    }

    /**
     * 获取年
     *
     * @param millis
     * @return
     * @author xiaojiang
     **/
    public static int getYearFromMillis(long millis) {
        return getLocalDateTimeByMillis(millis).getYear();
    }

    public static LocalDateTime getLocalDateTimeByMillis(long millis) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = Instant.ofEpochMilli(millis);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime;
    }

    /**
     * 获取月
     *
     * @param millis
     * @return
     * @author xiaojiang
     **/
    public static int getMonthFromMillis(long millis) {
        return getLocalDateTimeByMillis(millis).getMonthValue();
    }

    /**
     * 获取月
     *
     * @param second
     * @return
     * @author xiaojiang
     **/
    public static int getMonthFromSecond(long second) {
        return getMonthFromMillis(second * 1000);
    }

    /**
     * 获取日
     *
     * @param millis
     * @return
     * @author xiaojiang
     **/
    public static int getDayFromMillis(long millis) {
        return getLocalDateTimeByMillis(millis).getDayOfMonth();
    }

    /**
     * 获取日
     *
     * @param second
     * @return
     * @author xiaojiang
     **/
    public static int getDayFromSecond(long second) {
        return getDayFromMillis(second * 1000);
    }

    /**
     * 获取年月日
     *
     * @param millis
     * @return
     * @author xiaojiang
     **/
    public static long getYearMonthDayFromMillis(long millis) {
        LocalDateTime localDateTime = getLocalDateTimeByMillis(millis);
        return (((localDateTime.getYear() * 100) + localDateTime.getMonthValue()) * 100) + localDateTime.getDayOfMonth();
    }

    /**
     * 获取年月日
     *
     * @param second
     * @return
     * @author xiaojiang
     **/
    public static long getYearMonthDayFromSecond(long second) {
        return getYearMonthDayFromMillis(second * 1000);
    }

    /**
     * 获取年月
     *
     * @param millis
     * @return
     * @author xiaojiang
     **/
    public static long getYearMonthFromMillis(long millis) {
        LocalDateTime localDateTime = getLocalDateTimeByMillis(millis);
        return (localDateTime.getYear() * 100) + localDateTime.getMonthValue();
    }

    /**
     * 获取年月
     *
     * @param second
     * @return
     * @author xiaojiang
     **/
    public static long getYearMonthFromSecond(long second) {
        return getYearMonthFromMillis(second * 1000);
    }

    public static void main(String[] args) {
        System.out.println(DateUtils.getYearFromMillis(DateUtils.currentTimeMillis()));
    }

    public enum Format {
        YYYY {
            @Override
            public long LongValue() {
                return YYYY.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return localDateTime.getYear();
            }


            @Override
            public String StringValue(String... pattern) {
                return YYYY.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                return String.valueOf(localDateTime.getYear());
            }
        },
        YYYYMM {
            @Override
            public long LongValue() {
                return YYYYMM.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return localDateTime.getYear() * 1_00 + localDateTime.getMonthValue();
            }

            @Override
            public String StringValue(String... pattern) {
                return YYYYMM.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                if (pattern.length == 0) {
                    return dateTimeFormatter(ZH_PATTERN_MONTH, localDateTime);
                } else {
                    return dateTimeFormatter(pattern[0], localDateTime);
                }
            }
        },
        YYYYMMDD {
            @Override
            public long LongValue() {
                return YYYYMMDD.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return Long.valueOf(localDateTime.format(DateTimeFormatter.ofPattern("YYYYMMdd")));
            }

            @Override
            public String StringValue(String... pattern) {
                return YYYYMMDD.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                if (pattern.length == 0) {
                    return dateTimeFormatter(ZH_PATTERN_DAY, localDateTime);
                } else {
                    return dateTimeFormatter(pattern[0], localDateTime);
                }
            }
        },

        YYYYMMDDHH {
            @Override
            public long LongValue() {
                return YYYYMMDDHH.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return Long.valueOf(localDateTime.format(DateTimeFormatter.ofPattern("YYYYMMddHH")));
            }

            @Override
            public String StringValue(String... pattern) {
                return YYYYMMDDHH.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                if (pattern.length == 0) {
                    return dateTimeFormatter(ZH_PATTERN_HOUR, localDateTime);
                } else {
                    return dateTimeFormatter(pattern[0], localDateTime);
                }
            }
        }, YYYYMMDDHHMM {
            @Override
            public long LongValue() {
                return YYYYMMDDHHMM.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return Long.valueOf(localDateTime.format(DateTimeFormatter.ofPattern("YYYYMMddHHmm")));
            }

            @Override
            public String StringValue(String... pattern) {
                return YYYYMMDDHHMM.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                if (pattern.length == 0) {
                    return dateTimeFormatter(ZH_PATTERN_MINUTE, localDateTime);
                } else {
                    return dateTimeFormatter(pattern[0], localDateTime);
                }
            }
        }, YYYYMMDDHHMMSS {
            @Override
            public long LongValue() {
                return YYYYMMDDHHMMSS.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return Long.valueOf(localDateTime.format(DateTimeFormatter.ofPattern("YYYYMMddHHmmss")));
            }


            @Override
            public String StringValue(String... pattern) {
                return YYYYMMDDHHMMSS.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                if (pattern.length == 0) {
                    return dateTimeFormatter(ZH_PATTERN_SECOND, localDateTime);
                } else {
                    return dateTimeFormatter(pattern[0], localDateTime);
                }

            }
        }, MMDD {
            @Override
            public long LongValue() {
                return MMDD.LongValue(LocalDateTime.now());
            }

            @Override
            public long LongValue(LocalDateTime localDateTime) {
                return Long.valueOf(localDateTime.format(DateTimeFormatter.ofPattern("MMdd")));
            }


            @Override
            public String StringValue(String... pattern) {
                return MMDD.StringValue(LocalDateTime.now(), pattern);
            }

            @Override
            public String StringValue(LocalDateTime localDateTime, String... pattern) {
                if (pattern.length == 0) {
                    return dateTimeFormatter(ZH_PATTERN_MONTH_DD, localDateTime);
                } else {
                    return dateTimeFormatter(pattern[0], localDateTime);
                }

            }
        };

        public abstract long LongValue();

        public abstract long LongValue(LocalDateTime localDateTime);

        public abstract String StringValue(String... pattern);

        public abstract String StringValue(LocalDateTime localDateTime, String... pattern);
    }


}
