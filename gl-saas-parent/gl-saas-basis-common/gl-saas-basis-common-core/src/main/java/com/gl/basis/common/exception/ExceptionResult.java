package com.gl.basis.common.exception;

import java.io.Serializable;

import lombok.Data;


/**
 * 异常结果封装
 *
 */
@Data
public class ExceptionResult implements Serializable{
	private static final long serialVersionUID = 5888542463486118802L;

	/** 异常消息 **/
	private String msg;
	/** 异常状态  false **/
	private Boolean success = false;
	/** 异常码 **/
	private String code;
	/** 异常 **/
	private String exception;
	/** 异常 **/
	private String causeType;
	/** http状态码 **/
	private Integer status;

}
