package com.gl.basis.common.exception;

import java.util.Map;

import com.gl.basis.common.constant.IBaseConstants;



/**
 * 访问异常
 */
public class AccessException extends BaseException {
	private static final long serialVersionUID = 1L;
	private String causeType = IBaseConstants.ACCESS_EXCEPTION;
	
	public AccessException(String message) {
		super(message);
		super.setCauseType(causeType);
	}

	public AccessException(String message, String code) {
		super(message, code);
		super.setCauseType(causeType);
	}
	public AccessException(String message, String code,Map<String,Object> map) {
		super(message, code,map);
		super.setCauseType(causeType);
	}

	public AccessException(Throwable exception) {
		super(exception);
		super.setCauseType(causeType);
	}

	public AccessException(String message, String code, Throwable exception) {
		super(message, code, exception);
		super.setCauseType(causeType);
	}
}
