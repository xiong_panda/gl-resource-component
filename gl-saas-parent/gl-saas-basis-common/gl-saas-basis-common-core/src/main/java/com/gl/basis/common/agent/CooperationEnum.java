package com.gl.basis.common.agent;

import java.util.HashMap;
import java.util.Map;

public enum CooperationEnum {
    FACILITATOR("facilitator", "服务商","00000040"),
    SUPPLIER("supplier", "经销商","00000041"),
    ;
    private String name;
    private String value;
    private String message;

    CooperationEnum() {
    }

    CooperationEnum(String name, String message, String value) {
        this.name = name;
        this.value = value;
        this.message = message;
    }

    CooperationEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public static Map<String, Object> allValues() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("CooperationEnum", CooperationEnum.values());
        return maps;
    }

    public static CooperationEnum build(String str) {
        for (CooperationEnum value : CooperationEnum.values()) {
            if (str.equals(value.value)) {
                return value;
            }
        }
        return FACILITATOR;
    }
}
