package com.gl.basis.common.agent;
/**
 *配件来源
 */
public enum PartsSource {
    LOCALITY("locality", "1"),
    YE_MA("yema", "0"),
    ;
    private String souce;
    private String modification;

    public String getSouce() {
        return souce;
    }

    public String getModification() {
        return modification;
    }

    PartsSource(String souce, String modification) {
        this.souce = souce;
        this.modification = modification;
    }

    PartsSource() {
    }

    public static PartsSource build(String str) {
        for (PartsSource value : PartsSource.values()) {
            if (str.equals(value.souce)) {
                return value;
            }
        }
        return LOCALITY;
    }
}
