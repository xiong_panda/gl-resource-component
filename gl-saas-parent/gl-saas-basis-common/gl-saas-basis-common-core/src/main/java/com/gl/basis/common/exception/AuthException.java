package com.gl.basis.common.exception;

import java.util.Map;

import com.gl.basis.common.constant.IBaseConstants;


/**
 * 认证异常
 *
 */
public class AuthException extends BaseException{
	private static final long serialVersionUID = 3375924472484080218L;
	private String causeType = IBaseConstants.AUTH_EXCEPTION;
	
	public AuthException(String message) {
		super(message);
		super.setCauseType(causeType);
	}

	public AuthException(String message, String code) {
		super(message, code);
		super.setCauseType(causeType);
	}
	
	public AuthException(String message, String code,Map<String,Object> map) {
		super(message, code,map);
		super.setCauseType(causeType);
	}

	public AuthException(Throwable exception) {
		super(exception);
		super.setCauseType(causeType);
	}

	public AuthException(String message, String code, Throwable exception) {
		super(message, code, exception);
		super.setCauseType(causeType);
	}
	
	
}
