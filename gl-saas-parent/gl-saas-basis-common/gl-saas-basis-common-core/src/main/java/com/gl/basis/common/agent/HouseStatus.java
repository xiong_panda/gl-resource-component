package com.gl.basis.common.agent;

/**
 * 待出库订单状态
 *
 * @author 小江
 */
public enum HouseStatus {


    HOUSE_STAY("stay","代出库"),
    HOUSE_PART("part","部分出库"),
    HOUSE_ALREADY("already","已出库"),


    ;
    private String status;
    private String houseName;

    HouseStatus(String status, String houseName) {
        this.status = status;
        this.houseName = houseName;
    }


    public String getStatus() {
        return status;
    }

    public String getHouseName() {
        return houseName;
    }
}
