package com.gl.basis.common.agent.export;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface ExceleImpl {


    /**
     * 表格抬头
     */
    LinkedHashMap<Integer, LinkedHashMap<Integer, ? extends ExcelDTO>> buildExcelTitleMap();

    /**
     * 表格列数量
     */
    int getNumCol();

    /**
     * 表格名称
     */
    String getExceleName();

    /**
     * 表格sheet名
     */
    String getSheetName();

    /**
     * 合并单元格样式
     */
    default List<ExcelMergedDTO> getExcelMerged() {
        return null;
    }

    /**
     * 行高
     */
    default Map<Integer, Short> getExcelRowHeight() {
        return new LinkedHashMap<Integer, Short>() {{
            put(0, (short) 1000);
        }};
    }


}
