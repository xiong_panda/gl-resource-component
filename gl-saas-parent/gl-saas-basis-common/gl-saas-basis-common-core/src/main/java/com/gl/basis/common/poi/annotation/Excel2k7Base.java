package com.gl.basis.common.poi.annotation;


import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Excel 2007
 * 
 */
public class Excel2k7Base extends ExcelBase {
    /** 获取sheet列表 */
    public ArrayList<String> getSheetList(String filePath) {
        ArrayList<String> sheetList = new ArrayList<String>(0);
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(filePath));
            Iterator<Sheet> iterator = wb.iterator();
            while (iterator.hasNext()) {
                sheetList.add(iterator.next().getSheetName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sheetList;
    }

    /** 读取Excel文件内容 */
    public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, String columns) {
        ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(filePath));
            XSSFSheet sheet = wb.getSheetAt(sheetIndex);

            dataList = readExcel(sheet, rows, getColumnNumber(sheet, columns));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public Sheet readSheetFromExcel(String filePath, int sheetIndex) {
        Sheet sheet = null;
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(filePath));
            sheet = wb.getSheetAt(sheetIndex);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sheet;
    }

    public Workbook readWorkbookFromExcel(InputStream fileIs) {
        XSSFWorkbook wb = null;
        try {
            wb = new XSSFWorkbook(fileIs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wb;
    }

    /** 读取Excel文件内容 */
    public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, int[] cols) {
        ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
        try {
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(filePath));
            XSSFSheet sheet = wb.getSheetAt(sheetIndex);

            dataList = readExcel(sheet, rows, cols);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }
}