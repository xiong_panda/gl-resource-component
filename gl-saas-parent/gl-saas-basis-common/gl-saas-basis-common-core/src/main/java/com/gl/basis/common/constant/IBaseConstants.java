package com.gl.basis.common.constant;

/**
 * 公众常量
 * @author 
 *
 */
public interface IBaseConstants {
	/** 访问异常 **/
	public String ACCESS_EXCEPTION="AccessException";
	/** 业务异常 **/
	public String BUSINESS_EXCEPTION="BusinessException";
	
	/** 框架异常 **/
	public String FRAMEWORK_EXCEPTION="FrameworkException";
	/** 登录异常 **/
	public String LOGIN_EXCEPTION="LoginException";
	/** 认证异常 **/
	public String AUTH_EXCEPTION="AuthException";
	
	/** 访问异常 **/
	public String ERROR_HEADER = "c-exception-info";
	/** 系统异常 **/
	public String SYSTEM_EXCEPTION="SystemException";
	
}
