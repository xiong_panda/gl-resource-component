/*
 * © 2019 Copyright. Powered by Hongsheng Wu.
 */

package com.gl.basis.common.util;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 	复制两个对象
 * @author xiehong
 *
 */
public class Copier {

	public static Map<String, BeanCopier> beanCopierMap = new ConcurrentHashMap<>();

	/**
	 * 属性复制
	 *
	 * @param source            源对象
	 * @param target              目标对象
	 */
	public static void copy(Object source, Object target) {
		Assert.notNull(source, "源对象不能为空!");
		Assert.notNull(target, "目标对象不能为空!");
		String beanKey = generateKey(source.getClass(), target.getClass());
		BeanCopier copier = null;
		if (!beanCopierMap.containsKey(beanKey)) {
			copier = BeanCopier.create(source.getClass(), target.getClass(), false);
			beanCopierMap.put(beanKey, copier);
		} else {
			copier = beanCopierMap.get(beanKey);
		}
		copier.copy(source, target, null);
	}


	private static String generateKey(Class<?> class1, Class<?> class2) {
		return class1.toString() + class2.toString();
	}


}
