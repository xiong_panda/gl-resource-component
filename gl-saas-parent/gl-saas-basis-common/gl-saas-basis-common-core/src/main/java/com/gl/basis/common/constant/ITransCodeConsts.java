package com.gl.basis.common.constant;

/**
 * 接口基础交易码  ,一些自定义交易结果码请继承此常量接口
 * 编码范围:0000 ----> 9999
 * 编码规范:code msg
 * 
 *
 */
public interface ITransCodeConsts {
	/************************************** 通用码区 START **************************/
	/** 成功 **/
	public String SUCC_CODE="0000";
	/** 成功 **/
	public String SUCC_MSG="成功";
	
	/** 其它失败 **/
	public String ERROR_CODE="9999";
	/** 其它失败 **/
	public String ERROR_MSG="服务器繁忙，请稍后重试";
	/************************************** 通用码区 END **************************/
	
	/************************************** 具体码区 START **************************/
	
	/************************************** 具体码区 END **************************/
	/** API认证失败 **/
	public String AUTH_FAIL_CODE="9998";
	/** API认证失败 **/
	public String AUTH_FAIL_MSG="匿名访问,请登录!";
	
	/** 登录过期,重新登录 **/
	public String AUTH_EXP_CODE="9997";
	/** 登录过期,重新登录 **/
	public String AUTH_EXP_MSG="登录过期,重新登录";

    /** 用户权限异常 **/
    public String USER_PERMISSION_ERROR_CODE="9996";
    /** 用户权限异常**/
    public String USER_PERMISSION_ERROR_MSG="权限不足，请联系管理员";


}
