package com.gl.basis.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;

import com.gl.basis.common.controller.ErrorController;
import com.gl.basis.common.feign.FeignConfiguration;
import com.gl.basis.common.handler.GlobalExceptionHandler;

@Configuration
@Import(value= {ErrorController.class,
		FeignConfiguration.class,
		FeignMultipartSupportConfig.class})
public class CoreMainConfig {

	/**
	 * 全局异常处理
	 * 
	 * @return
	 */
	@Bean
	@Order(-1)
	GlobalExceptionHandler globalExceptionHandler() {
		GlobalExceptionHandler exceptionHandler = new GlobalExceptionHandler();
		exceptionHandler.setOrder(-1);
		return exceptionHandler;
	}

	
}
