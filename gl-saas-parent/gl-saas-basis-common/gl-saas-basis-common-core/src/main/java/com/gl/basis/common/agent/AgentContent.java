package com.gl.basis.common.agent;

import com.gl.basis.common.util.StringUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代理商上下文
 *
 * @author 小江
 */
@Getter
@NoArgsConstructor
public class AgentContent<T> {
    /**
     * 联盟id
     */
    private String allianceId;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 用户id
     */
    private String userId;

    private String lastModifyId ;


    private String lastModifyName ;
    private Map<String, Object> cooperation;
    private CooperationEnum cooperationEnum;
    private T t;

    public AgentContent(String allianceId, String companyId, String userId) {
        this.allianceId = allianceId;
        this.companyId = companyId;
        this.userId = userId;
    }

    public AgentContent buildAlliancdId(String allianceId, String companyId, String userId, Map<String, Object> cooperation) {
        this.allianceId = allianceId;
        this.companyId = companyId;
        this.userId = userId;
        this.cooperation = cooperation;
        return this;
    }
    public AgentContent buildAlliancdId(String allianceId, String companyId, String userId) {
        this.allianceId = allianceId;
        this.companyId = companyId;
        this.userId = userId;
        return this;
    }
    public AgentContent build(String lastModifyId,String lastModifyName) {
        this.lastModifyId = lastModifyId;
        this.lastModifyName = lastModifyName;
        return this;
    }


    public AgentContent build(T t) {
        this.t = t;
        return this;
    }

    public Map<String, Object> getMap(String... str) {
        Map<String, Object> map = new HashMap<>();
        if (this.t instanceof Map) {
            map = (Map<String, Object>) this.t;
        }
        if (str != null && str.length != 0 ) {
            for (String s : str) {
                switch (s) {
                    case "allianceId":
                        map.put("allianceId", this.allianceId);
                        break;
                    case "companyId":
                        map.put("companyId", this.companyId);
                        break;
                    case "userId":
                        map.put("userId", this.userId);
                        break;
                    default:
                        break;
                }
            }
        }
        return map;
    }
    /**
     *根据某个字段获取值 如果没有返回null
     */
    public String getString(String str){
        Object o = this.getMap().get(str);
        return o == null || StringUtils.isEmpty(o.toString()) ? null : o.toString();
    }
    public List<String> getList(String str){
        Object o = this.getMap().get(str);
        if (o == null || StringUtils.isEmpty(o.toString()) ){
            return null;
        }
        if (o instanceof  List){
            return (List<String>)o;
        }
        return null;
    }
}
