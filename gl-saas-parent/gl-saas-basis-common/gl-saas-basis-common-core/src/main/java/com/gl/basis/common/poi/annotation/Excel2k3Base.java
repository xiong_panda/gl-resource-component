package com.gl.basis.common.poi.annotation;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;


/**
 * Excel 97-2003格式
 * 
 */
public class Excel2k3Base extends ExcelBase {
    /** 获取sheet列表 */
    public ArrayList<String> getSheetList(String filePath) {
        ArrayList<String> sheetList = new ArrayList<String>(0);
        try {
            HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(filePath));
            int i = 0;
            while (true) {
                try {
                    String name = wb.getSheetName(i);
                    sheetList.add(name);
                    i++;
                } catch (Exception e) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sheetList;
    }

    /** 读取Excel文件内容 */
    public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, String columns) {
        ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
        try {
            HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(filePath));
            HSSFSheet sheet = wb.getSheetAt(sheetIndex);

            dataList = readExcel(sheet, rows, getColumnNumber(sheet, columns));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public Sheet readSheetFromExcel(String filePath, int sheetIndex) {
        Sheet sheet = null;
        try {
            HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(filePath));
            sheet = wb.getSheetAt(sheetIndex);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sheet;
    }

    public Workbook readWorkbookFromExcel(InputStream fileIs) {
        Workbook wb = null;
        try {
            wb = new HSSFWorkbook(fileIs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wb;
    }

    /** 读取Excel文件内容 */
    public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, int[] cols) {
        ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>>();
        try {
            HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(filePath));
            HSSFSheet sheet = wb.getSheetAt(sheetIndex);

            dataList = readExcel(sheet, rows, cols);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }
}