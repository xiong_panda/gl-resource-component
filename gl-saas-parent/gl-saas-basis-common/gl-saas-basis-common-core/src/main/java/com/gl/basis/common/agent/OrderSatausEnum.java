package com.gl.basis.common.agent;

import jdk.internal.dynalink.beans.StaticClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * TODO
 *
 * @author cj
 * @projectproject
 * @date 2020/5/28 9:33
 */
public enum OrderSatausEnum {
    DO_SAVE("doSave", "草稿箱", "草稿箱"),
    STAY_PRICE("stayPrice","待报价","待报价"),
    ALREADY_PRICE("alreadyPrice", "待付款","已报价"),

    HAS_PAY("hasPay", "已确认","待审核"),

    HAS_AUITH("hasAuith", "待收货", "审核通过"),

    NOT_AUITH("notAuith", "待确认", "审核不通过");

    private String key;
    /**
     *服务商
     */
    private String facilitator;
    /**
     *供应商
     */
    private String supplier;


    OrderSatausEnum() {
    }

    OrderSatausEnum(String key, String facilitator, String supplier) {
        this.key = key;
        this.facilitator = facilitator;
        this.supplier = supplier;
    }

    public String getKey() {
        return key;
    }

    public String getFacilitator() {
        return facilitator;
    }
    public String getSupplier() {
        return supplier;
    }
    public static Map<String, Object> getAllSupplier() {
        Map<String, Object> maps = new HashMap<>();
        for (OrderSatausEnum value : OrderSatausEnum.values()) {
            maps.put(value.getKey(), value.getSupplier());
        }
        return maps;
    }

    public static Map<String, Object> getAllFacilitator() {
        Map<String, Object> maps = new HashMap<>();
        for (OrderSatausEnum value : OrderSatausEnum.values()) {
            maps.put(value.getKey(), value.getFacilitator());
        }
        return maps;
    }

//    public static  List<Object> getall(){
//        return OrderSatausEnum.values();
//    }
    public static List<Object> getSupplierkeys() {
        List<Object> list = new ArrayList<>();
        for (OrderSatausEnum value : OrderSatausEnum.values()) {
            OrderStautsOut out = new OrderStautsOut();
            out.setName(value.key);
            out.setValue(value.supplier);
            list.add(out);
        }
        return list ;
    }


    public static List<Object> getFacilitatorkeys() {
        List<Object> list = new ArrayList<>();
        for (OrderSatausEnum value : OrderSatausEnum.values()) {
            OrderStautsOut out = new OrderStautsOut();
            out.setName(value.key);
            out.setValue(value.facilitator);
            list.add(out);
        }
        return list ;
    }
    public static OrderSatausEnum build(String str) {
        for (OrderSatausEnum value : OrderSatausEnum.values()) {
            if (str.equals(value.key)) {
                return value;
            }
        }
        return DO_SAVE;
    }

    public static void main(String[] args) {
        System.out.println(OrderSatausEnum.values().toString());
        Stream.of(OrderSatausEnum.values()).forEach(System.out::println);
        Stream.of(OrderSatausEnum.values()).forEach(k->System.out.println(k.getKey()));
        Stream.of(OrderSatausEnum.values()).forEach(k -> System.out.println(k.getSupplier()));
    }
}
