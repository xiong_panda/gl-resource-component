package com.gl.basis.common.agent;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author cj
 * @projectproject
 * @date 2020/5/29 9:50
 */
public enum PartsStatusEnum {
    PARTS_NOMAL("partsNomal", "常规件"),
    PARTS_UNMARKETABLE("partsUnmakertable", "滞销件"),
    PARTS_SELL_WELL("partsSellWell", "畅销件");
    private String name;
    private String value;

    PartsStatusEnum() {
    }

    PartsStatusEnum(String value, String name) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public static Map<String, Object> allvalues() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("PartsStatusEunm", PartsStatusEnum.values());
        return maps;
    }

    public static PartsStatusEnum build(String str) {
        for (PartsStatusEnum value : PartsStatusEnum.values()) {
            if (str.equals(value.name)) {
                return value;
            }
        }
        return PARTS_NOMAL;
    }
}
