package com.gl.basis.common.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.basis.common.constant.ResultCode;
import com.gl.basis.common.pojo.Page;
@CrossOrigin
@RestController
public class ErrorController {

    //公共错误跳转
    @SuppressWarnings("all")
	@RequestMapping(value="/autherror")
    public Page autherror(int code) {
    	Page page = new Page();
    	page.setSuccess(false);
    	if (1==1) {
    		page.setCode(ResultCode.UNAUTHENTICATED.getCode());
    		page.setMsg(ResultCode.UNAUTHENTICATED.getMsg());
		}else {
			page.setCode(ResultCode.UNAUTHORIZED.getCode());
    		page.setMsg(ResultCode.UNAUTHORIZED.getMsg());
		}
        return page;
    }

}
