package com.gl.basis.common.constant;

/**
 * 返回值枚举
 * 
 * @author xiehong
 *
 */
public enum ResultCode implements IErrorCode{
	SUCCESS("0", "成功"), // 成功
	FAIL("400", "失败！"), // 失败
	UNAUTHORIZED("401", "无权访问，当前帐号权限不足!"), // 未认证（签名错误）
	ACCESS_DENIED("403", "拒绝访问"), // 没有访问权限
	NOT_FOUND("404", "页面不存在!"), // 接口不存在
	UNAUTHENTICATED("10001", "您还未登录"),
	NO_USER("10002", "手机号不存在"), 
	NO_COMPANY_USER("10003", "非企业用户"),
	
	INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
	VALIDATE_FAILED("100", "参数检验失败"), 
	GATEWAY_ERROR("404", "请求不存在，请重试！"),
	TIMESTAMP_ERROR("101", "时间戳过期！"),
	TIMEFORM_ERROR("103", "时间戳格式错误！"),
	SIGN_ERROR("104", "签名错误！"),
	TIMESTAMP_ISNULL_ERROR("105", "时间戳不存在！"),
	IP_DISABLE_ERROR("106", "IP地址与申请地址不匹配！"),
	APPKEY_ERROR("107", "客户端appkey不存在，请联系管理员！"),
	IDEMPOTENCY_ERROR("108", "请勿重复请求！"),
	ARREARAGE_ERROR("109", "账户余额不足，请充值！"),
	FREUECY_ERROR("110", "调用次数已经用完！"),

    INFO("204", "请求受理成功，响应数据为空！"),
    UNAUTHENTIC("401", "无权访问，请先登录！"),
    NOTFOUND("404", "服务器未找到资源"),
    ERROR("500", "服务器发生错误！"),
	PARAMS_ERROR("1001", "参数为空！"),
	FUNCTION_ERROR("1002", "菜单为空！"),
	ROLE_LEAGE_ERROR("1003", "角色或者联盟必须选一个！"),
	COOL_LEAGE_ERROR("1004", "协作关系为空！"),
	APPLICATION_ID_ERROR("1005", "应用id为空！"),
	UNION_ERROR("1006", "联盟为空或者联盟未绑定企业信息！"),
	ROLE_ERROR("1007", "角色信息为空！"),
	PERSSIONS_ERROR("1008", "权限信息为空！"),
	APP_ERROR("1009", "企业基础应用为空！"),
	FUNCTION_FIND_ERROR("1010", "查询菜单平台不能为空！"),
	IMAGE_ERROR("1011", "密码输入错误"),
	FREQUENT_OPERATIONS_ERROR("1012", "登录过于频繁，请等待"),

	;

	/** 枚举值 */
	private final String code;

	/** 枚举描述 */
	private final String msg;

	/**
	 *
	 * @param code    枚举值
	 * @param message 枚举描述
	 */
	private ResultCode(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	/**
	 * @return Returns the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return Returns the message.
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @return Returns the code.
	 */
	public String code() {
		return code;
	}

	
	/**
	 * 通过枚举<code>code</code>获得枚举
	 *
	 * @param code
	 * @return AttachmentEnum
	 */
	public static ResultCode getByCode(String code) {
		for (ResultCode _enum : values()) {
			if (_enum.getCode().equals(code)) {
				return _enum;
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 *
	 * @return List<AttachmentEnum>
	 */
	public static java.util.List<ResultCode> getAllEnum() {
		java.util.List<ResultCode> list = new java.util.ArrayList<>(values().length);
		for (ResultCode _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 *
	 * @return List<String>
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<>(values().length);
		for (ResultCode _enum : values()) {
			list.add(_enum.code());
		}
		return list;
	}

	/**
	 * 通过code获取msg
	 * 
	 * @param code 枚举值
	 * @return
	 */
	public static String getMsgByCode(String code) {
		if (code == null) {
			return null;
		}
		ResultCode _enum = getByCode(code);
		if (_enum == null) {
			return null;
		}
		return _enum.getMsg();
	}

	/**
	 * 获取枚举code
	 * 
	 * @param _enum
	 * @return
	 */
	public static String getCode(ResultCode _enum) {
		if (_enum == null) {
			return null;
		}
		return _enum.getCode();
	}

	
}
