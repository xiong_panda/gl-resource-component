package com.gl.basis.common.exception;

import com.gl.basis.common.constant.IBaseConstants;

public class SystemException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5928303185182327043L;
	private String causeType = IBaseConstants.SYSTEM_EXCEPTION;
			
	public SystemException(Throwable exception) {
		super(exception);
	}
	
	public SystemException(String message) {
		super(message);
	}

}
