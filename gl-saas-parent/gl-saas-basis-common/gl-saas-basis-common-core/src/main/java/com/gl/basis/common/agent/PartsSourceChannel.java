package com.gl.basis.common.agent;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author cj
 * @projectproject
 * @date 2020/5/29 9:42
 */
public enum PartsSourceChannel {
    LOCAL_STORE("localStore", "本地库存"),
    TERRACE_STORE("terraceStore", "平台库存"),
    CHAIN_STORE("chainStore", "产业链库存");
    private String name;
    private String value;

    PartsSourceChannel() {
    }

    PartsSourceChannel(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public static Map<String, Object> allvalues() {
        Map<String, Object> maps = new HashMap<>();
        for (PartsSourceChannel value : PartsSourceChannel.values()) {
            maps.put(value.name, value.value);
        }
        return maps;
    }

    public static PartsSourceChannel build(String str) {
        for (PartsSourceChannel value : PartsSourceChannel.values()) {
            if (str.equals(value.name)) {
                return value;
            }
        }
        return LOCAL_STORE;
    }
}
