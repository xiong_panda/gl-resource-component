package com.gl.basis.common.exception;

import java.util.Map;

import com.gl.basis.common.constant.IBaseConstants;
import com.google.protobuf.ByteString;

/**
 * 业务异常
 */
public class BusinessException extends BaseException {
	private static final long serialVersionUID = 1L;
	private String causeType = IBaseConstants.BUSINESS_EXCEPTION;

	public BusinessException(String message) {
		super(message);
		super.setCauseType(causeType);
	}

	public BusinessException(String message, String code) {
		super(message, code);
		super.setCauseType(causeType);
	}
	
	public BusinessException(String message, String code,ByteString data) {
		super(message, code,data);
		super.setCauseType(causeType);
	}
	
	public BusinessException(String message, String code,Map<String,Object> map) {
		super(message, code,map);
		super.setCauseType(causeType);
	}
	
	public BusinessException(String message, String code,Map<String,Object> map, ByteString data) {
		super(message, code,map);
		super.setData(data);
		super.setCauseType(causeType);
	}

	public BusinessException(Throwable exception) {
		super(exception);
		super.setCauseType(causeType);
	}

	public BusinessException(String message, String code, Throwable exception) {
		super(message, code, exception);
		super.setCauseType(causeType);
	}
	
	public BusinessException(Throwable exception,String message, String code, Map<String,Object> map) {
		super(exception, message, code, map);
		super.setCauseType(causeType);
	}
}
