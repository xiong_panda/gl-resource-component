package com.gl.basis.common.agent.export;

public interface Export {

    /**
     * 单元格列
     */
    String getFiled();

    /**
     * 单元格说明
     */
    String getMessage();

    /**
     * 单元格行
     */
    int getShow();

    /**
     * 单元格宽
     */
    Short getColWidth();


    ;
}
