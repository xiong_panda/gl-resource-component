package com.gl.basis.common.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.gl.basis.common.exception.BusinessException;


public class DateUtils {

    /**
     * 得到当前时间
     *
     * @return Date
     */
    public static Date getCurrentTimestamp() {
        return new Date();
    }

    public static String format(Date date, String pattern) {
        return new SimpleDateFormat(pattern).format(date);
    }

    /**
     * @return String
     * @methodName: getCurrentXsdDateTime
     * @Description:获取当前时间的字符串，格式为yyyy-MM-ddTHH:mm:ss
     * @author zhangmin
     * @date 2015-1-5 下午1:48:39
     */
    public static String getCurrentXsdDateTime() {
        return getCurrentPrettyDate() + "T" + getCurrentTime();
    }

    /**
     * @param date
     * @return String
     * @methodName: getHourMinuteSecond
     * @Description:获取时间固定格式HHmmss
     * @author zhangmin
     * @date 2015-1-5 下午1:55:16
     */
    public static String getHourMinuteSecond(Date date) {
        return new SimpleDateFormat("HHmmss").format(date);
    }

    /**
     * @param date
     * @return String
     * @methodName: getYearMonthDay
     * @Description:获取时间固定格式HHmmss
     * @author zhangmin
     * @date 2015-1-5 下午1:54:52
     */
    public static String getYearMonthDay(Date date) {
        return new SimpleDateFormat("yyyyMMdd").format(date);
    }

    /**
     * @return String
     * @methodName: getUpDayByCurrentDay
     * @Description:获取当前日期的上一天日期
     * @author zhangmin
     * @date 2015-1-5 下午1:48:45
     */
    public static String getUpDayByCurrentDay() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_MONTH, -1);
        String upDay = format(c.getTime(), "yyyyMMdd");
        return upDay;
    }

    /**
     * @param beforeDay
     * @return String
     * @methodName: getBeforDay
     * @Description:获取当前日期的前几天
     * @author zhangmin
     * @date 2015-1-5 下午1:48:57
     */
    public static String getBeforDay(int beforeDay) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_MONTH, -beforeDay);
        String upDay = format(c.getTime(), "yyyyMMdd");
        return upDay;
    }

    /**
     * @return String
     * @methodName: g
     * @Description:获取当前时间的字符串，格式为yyyy-MM-ddTHH:mm:ss
     * @author zhangmin
     * @date 2015-1-5 下午1:49:06
     */
    public static String g() {
        return getCurrentPrettyDate() + "T" + getCurrentTime();
    }

    /**
     * 获取当前时间字符串，格式为yyyy-MM-dd
     *
     * @return
     */
    public static String getCurrentPrettyDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /**
     * 获取当前时间字符串，格式为HH:mm:ss
     *
     * @return
     */
    public static String getCurrentTime() {
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    /**
     * 获取当前时间字符串，格式为yyyyMMddHHmmss
     *
     * @return
     */
    public static String getCurDateTime() {
        return getCurDate("yyyyMMddHHmmss");
    }

    /**
     * 获取当前时间
     *
     * @param pattern 格式yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getCurDate(String pattern) {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat(pattern);
        df.setLenient(false);
        return df.format(date);
    }

    /**
     * 字符串转换为日期
     *
     * @param date   日期字符串
     * @param patten 格式yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date parse(String date, String patten) {
        SimpleDateFormat sdf = new SimpleDateFormat(patten);
        try {
        	if(date.indexOf("-")>-1) {
        		//对yyyy-MM-dd HH:mm:ss 格式字符串进行转换
        		return sdf.parse(date);
        	}else {
        		//针对yyyyMMdd HH:mm:ss 格式字符串进行转换
        		return sdf.parse(strToDateFormat(date));
        	}            
        } catch (ParseException e) {
        	throw new BusinessException(e.getMessage());
        }
    }
    /**
     *将字符串格式yyyyMMdd的字符串转为日期，格式"yyyy-MM-dd"
     *
     * @param date 日期字符串
     * @return 返回格式化的日期
     * @throws ParseException 分析时意外地出现了错误异常
     */
    public static String strToDateFormat(String date) {        
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	        formatter.setLenient(false);
	        Date newDate;
			newDate = formatter.parse(date);
			formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        return formatter.format(newDate);
		} catch (ParseException e) {
			throw new BusinessException(e.getMessage());
		}
        
    }
    
    
    /**
     * 校验字符串格式的日期是否合法
     *
     * @param date
     * @param pattern
     * @return 合法返回true，否则返回false
     */
    public static boolean validDate(String date, String patten) {
        SimpleDateFormat sdf = new SimpleDateFormat(patten);
        try {
            sdf.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * 格林威治时间转换为自定义格式的时间
     *
     * @param gmt
     * @param patten
     * @return String
     * @throws
     */
    public static Date gmtToDate(String gmt) {
        return new Date(Long.parseLong(gmt));
    }

    /**
     * 格林威治时间转换为自定义格式的时间
     *
     * @param gmt
     * @param patten
     * @return String
     * @throws
     */
    public static String gmtToStr(String gmt, String patten) {
        String res = null;
        if (StringUtils.isEmpty(gmt)) {
            return res;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(patten);
        res = sdf.format(gmtToDate(gmt));
        return res;
    }

    /**
     * 转换格林威治时间
     *
     * @param gmt
     * @param patten
     * @return String
     * @throws
     */
    public static String strToGmt(String date, String patten) {
        String res = null;
        if (StringUtils.isEmpty(date)) {
            return res;
        }
        if (!validDate(date, patten)) {
            return res;
        }
        Date parse = parse(date, patten);
        return String.valueOf(parse.getTime());
    }

    /**
     * 当前秒
     *
     * @return
     */
    public static String currentTimeSecond() {
        return new BigDecimal(System.currentTimeMillis()).divide(new BigDecimal(1000)).toString();
    }

    /**
     * @param @param  date
     * @param @return 参数
     * @return String  返回类型
     * @throws
     * @Title: getCurDate
     * @Description: 把long 转换成 Date
     */
    public static Date parse(long date) {
        return new Date(date);
    }

    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int differentDaysByMillisecond(Date date1, Date date2) {
        return (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
    }

    /**
     * 获取当前时间到第二天凌晨距离的秒数
     *
     * @return
     */
    public static Long getSecondsNextEarlyMorning() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Long seconds = (cal.getTimeInMillis() - System.currentTimeMillis()) / 1000;
        return seconds.longValue();
    }

    /**
     * 日期加运算方法
     *
     * @param field
     * @param amount
     * @return
     */
    public static Date dateAdd(Date dt, int field, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.add(field, amount);
        return calendar.getTime();
    }

    /**
     * 获取给定日期当天的开始时间
     *
     * @param dt
     * @return
     */
    public static Date getStartOfDay(Date dt) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取给定日期当天的结束时间
     *
     * @param dt
     * @return
     */
    public static Date getEndOfDay(Date dt) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * 获取给定日期当前小时的开始时间
     *
     * @param dt
     * @return
     */
    public static Date getStartOfHour(Date dt) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取给定日期当前小时的结束时间
     *
     * @param dt
     * @return
     */
    public static Date getEndOfHour(Date dt) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }
    
    /**
     * 获取当前时间到下个月1号0点的秒数
     *
     * @return
     */
    public static Long getSecondsToNextMonth() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Long seconds = (cal.getTimeInMillis() - System.currentTimeMillis()) / 1000;
        return seconds.longValue();
    }
    /**
     * 获取当月第几天
     * @Title: getDay
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param de
     * @return 参数
     * @return int    返回类型
     * @throws
     */
    public static int getDay(Date de) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(de);
    	return c.get(Calendar.DAY_OF_MONTH);
    }
    /**
     * 获取月份 0为1月
     * @Title: getMonth
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param de
     * @return 参数
     * @return int    返回类型
     * @throws
     */
    public static int getMonth(Date de) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(de);
    	return c.get(Calendar.MONTH);
    }
    
    /**
     * 获取年
     * @Title: getYear
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param de
     * @return 参数
     * @return int    返回类型
     * @throws
     */
    public static int getYear(Date de) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(de);
    	return c.get(Calendar.YEAR);
    }
    
    /**
     * 
     * @Title: setCalendar
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param year
     * @param month month-1
     * @param day
     * @return 参数
     * @return Calendar    返回类型
     * @throws
     */
    public static Calendar setCalendar(int year , int month , int day) {
    	Calendar c = Calendar.getInstance();
    	c.set(year, month, day);
    	return  c;
    }
    
    /**
     * 月份+n
     * @Title: addMonth
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param de
     * @return 参数
     * @return String    返回类型
     * @throws
     */
    public static String addMonth(Date de,int n) {
    	DateFormat df = new SimpleDateFormat("yyyyMMdd");
    	Calendar c = Calendar.getInstance();
    	c.setTime(de);
    	c.add(Calendar.MONTH, n);
    	return df.format(c.getTime());
    }
    
    
    /**
     * 月份+n
     * @Title: addMonth
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param de
     * @return 参数
     * @return String    返回类型
     * @throws
     */
    public static String addYear(Date de,int n) {
    	DateFormat df = new SimpleDateFormat("yyyy年MM月");
    	Calendar c = Calendar.getInstance();
    	c.setTime(de);
    	c.add(Calendar.YEAR, n);
    	return df.format(c.getTime());
    }
    
    public static Date strToDate(String date) {
    	try {
    		DateFormat df = new SimpleDateFormat("yyyyMMdd");
        	Date dt = df.parse(date);
        	return dt;
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    	
    }
    
}
