package com.gl.basis.common.dataenum;

import org.springframework.dao.DataAccessException;

/**
 * 数据库异常enum
 */
public enum DatabaseExceptionEnum {
    // 一项操作成功地执行，但在释放数据库资源时发生异常（例如，关闭一个Connection）
    CleanupFailureDataAccessException(org.springframework.dao.CleanupFailureDataAccessException.class, "数据库资源释放异常"),
    // 数据访问资源彻底失败，例如不能连接数据库
    DataAccessResourceFailureException(org.springframework.dao.DataAccessResourceFailureException.class, "数据访问资源异常"),
    // Insert或Update数据时违反了完整性，例如违反了惟一性限制
    DataIntegrityViolationException(org.springframework.dao.DataIntegrityViolationException.class, "违反完整性限制"),
    // 某些数据不能被检测到，例如不能通过关键字找到一条记录
    DataRetrievalFailureException(org.springframework.dao.DataRetrievalFailureException.class, "某些数据不能被检测"),
    // 当前的操作因为死锁而失败
    DeadlockLoserDataAccessException(org.springframework.dao.DeadlockLoserDataAccessException.class, "数据死锁"),
    // Update时发生某些没有预料到的情况，例如更改超过预期的记录数。当这个异常被抛出时，执行着的事务不会被回滚
    IncorrectUpdateSemanticsDataAccessException(org.springframework.dao.IncorrectUpdateSemanticsDataAccessException.class, "不可预料的Update操作"),
    // 一个数据访问的JAVA API没有正确使用，例如必须在执行前编译好的查询编译失败了
    InvalidDataAccessApiUsageException(org.springframework.dao.InvalidDataAccessApiUsageException.class, "JAVA API使用错误"),
    // 错误使用数据访问资源，例如用错误的SQL语法访问关系型数据库
    InvalidDataAccessResourceUsageException(org.springframework.dao.InvalidDataAccessResourceUsageException.class, "数据访问资源使用错误"),
    // 乐观锁的失败。这将由ORM工具或用户的DAO实现抛出
    OptimisticLockingFailureException(org.springframework.dao.OptimisticLockingFailureException.class, "乐观锁失败"),
    // Java类型和数据类型不匹配，例如试图把String类型插入到数据库的数值型字段中
    TypeMismatchDataAccessException(org.springframework.dao.TypeMismatchDataAccessException.class, "Java类型和数据类型不匹配"),
    // 有错误发生，但无法归类到某一更为具体的异常中
    UncategorizedDataAccessException(org.springframework.dao.UncategorizedDataAccessException.class, "未知错误"),

    BadSqlGrammarException(org.springframework.jdbc.BadSqlGrammarException.class, "SQL语句语法错误"),
    CannotGetJdbcConnectionException(org.springframework.jdbc.CannotGetJdbcConnectionException.class, "无法获取JDBC连接"),
    IncorrectResultSetColumnCountException(org.springframework.jdbc.IncorrectResultSetColumnCountException.class, "错误的结果集列数"),
    InvalidResultSetAccessException(org.springframework.jdbc.InvalidResultSetAccessException.class, "无效结果访问"),
    JdbcUpdateAffectedIncorrectNumberOfRowsException(org.springframework.jdbc.JdbcUpdateAffectedIncorrectNumberOfRowsException.class, "JdbcUpdateAffectedIncorrectNumberOfRowsException"),
    LobRetrievalFailureException(org.springframework.jdbc.LobRetrievalFailureException.class, "LobRetrievalFailureException"),
    SQLWarningException(org.springframework.jdbc.SQLWarningException.class, "SQL警告"),
    UncategorizedSQLException(org.springframework.jdbc.UncategorizedSQLException.class, "未分类的SQL"),
    ;

    private Class clazz;
    private String msg;

    public Class getClazz() {
        return clazz;
    }

    public String getMsg() {
        return msg;
    }

    DatabaseExceptionEnum(Class<? extends DataAccessException> clazz, String msg) {
        this.clazz = clazz;
        this.msg = msg;
    }

    public static DatabaseExceptionEnum valueOf(Class clazz) {
        if (clazz == null) {
            return null;
        }

        DatabaseExceptionEnum[] enums = DatabaseExceptionEnum.values();
        for (DatabaseExceptionEnum enu : enums) {
            if (clazz.equals(enu.clazz)) {
                return enu;
            }
        }

        return null;
    }

    public static <T> DatabaseExceptionEnum valueOf(T e) {
        if (e == null) {
            return null;
        }

        DatabaseExceptionEnum[] enums = DatabaseExceptionEnum.values();
        for (DatabaseExceptionEnum enu : enums) {
            if (e.getClass().equals(enu.clazz)) {
                return enu;
            }
        }

        return null;
    }
}
