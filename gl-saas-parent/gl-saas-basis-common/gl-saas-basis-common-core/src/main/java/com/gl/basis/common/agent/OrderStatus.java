package com.gl.basis.common.agent;
/**
 *
 *订单状态
 *@author 小江
 */
public enum OrderStatus{


    ORDER_DRAFT("draft","草稿"),
    ORDER_STAY("stay","代发货"),
    ORDER_PART("part","部分发货"),
    ORDER_ALREADY("already","已发货"),


    ;
    private String status;
    private String orderName;

    OrderStatus(String status, String orderName) {
        this.status = status;
        this.orderName = orderName;
    }


    public String getStatus() {
        return status;
    }

    public String getOrderName() {
        return orderName;
    }
}
