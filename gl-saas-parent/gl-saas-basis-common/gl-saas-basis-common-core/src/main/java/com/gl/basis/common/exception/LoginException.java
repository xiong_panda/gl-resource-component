package com.gl.basis.common.exception;

import java.util.Map;

import com.gl.basis.common.constant.IBaseConstants;


/**
 * 登录异常
 */
public class LoginException extends BaseException{
	private static final long serialVersionUID = 1L;
	private String causeType = IBaseConstants.LOGIN_EXCEPTION;

	public LoginException(String message) {
		super(message);
		super.setCauseType(causeType);
	}

	public LoginException(String message, String code) {
		super(message, code);
		super.setCauseType(causeType);
	}
	
	public LoginException(String message, String code,Map<String,Object> map) {
		super(message, code,map);
		super.setCauseType(causeType);
	}

	public LoginException(Throwable exception) {
		super(exception);
		super.setCauseType(causeType);
	}

	public LoginException(String message, String code, Throwable exception) {
		super(message, code, exception);
		super.setCauseType(causeType);
	}
}
