package com.gl.basis.auth.shiro.filter;


import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gl.basis.auth.constants.AuthorityConstants;
import com.gl.basis.auth.shiro.token.JWTToken;
import com.gl.basis.auth.util.JWTUtil;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *   	请求拦截器
 * @author Administrator
 *
 */
public class JWTFilter extends BasicHttpAuthenticationFilter {

    @Value("${jwt.anonymous.urls}")
    private String anonymousStr;
    @Resource
   	private JWTUtil jWTUtil;


    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        String contextPath = WebUtils.getPathWithinApplication(WebUtils.toHttp(servletRequest));
        if (!StringUtils.isEmpty(anonymousStr)) {
            String[] anonUrls = anonymousStr.split(",");
            //匿名可访问的url
            for (int i = 0; i < anonUrls.length; i++) {
                if (contextPath.contains(anonUrls[i])) {
                    return true;
                }
            }
        }
        //获取请求头token
        AuthenticationToken token = this.createToken(servletRequest, servletResponse);
        if (null == token ||token.getPrincipal() == null ) {
            handler401(servletResponse, "401", "请登录，谢谢！");
            return false;
        } else {
            try {
                this.getSubject(servletRequest, servletResponse).login(token);
                return true;
            } catch (Exception e) {
                String msg = e.getMessage();
                //token错误
                if (msg.contains("incorrect")) {
                    handler401(servletResponse, "401", msg);
                    return false;
                    //token过期
                } else if (msg.contains("expired")) {
                    //尝试刷新token
                    if (this.refreshToken(servletRequest, servletResponse)) {
                        return true;
                    } else {
                        handler401(servletResponse, "401", "token已过期,请重新登录");
                        return false;
                    }
                }
                handler401(servletResponse, "401", msg);
                return false;
            }
        }
    }

    /**
     * 此处为AccessToken刷新，进行判断RefreshToken是否过期，未过期就返回新的AccessToken且继续正常访问
     *
     * @param servletRequest
     * @param servletResponse
     * @return
     */
    private boolean refreshToken(ServletRequest servletRequest, ServletResponse servletResponse) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //获取header，tokenStr
        String oldToken = request.getHeader(AuthorityConstants.AUTHORIZATION_HEADER);
       // String userName = jWTUtil.getName(oldToken);
      //  String key = CommonConstant.JWT_TOKEN + userName;
        //获取redis tokenStr
      /*  String redisUserInfo = (String) redisUtil.get(key);
       if (redisUserInfo != null) {
            if (oldToken.equals(redisUserInfo)) {
                User vo =null; //this.userService.getUserByUserName(userName);
                //重写生成token(刷新)
                String newTokenStr = JWTUtil.sign(vo.getMobile(), vo.getPassWord());
                JWTToken jwtToken = new JWTToken(newTokenStr);
               // userService.addTokenToRedis(userName, newTokenStr);
                SecurityUtils.getSubject().login(jwtToken);
                response.setHeader("Authorization", newTokenStr);
                return true;
            }*/
       // }
        return false;
    }

    /**
     * token有问题
     *
     * @param response
     * @param code
     * @param msg
     */
    private void handler401(ServletResponse response, String code, String msg) {
        try {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setStatus(HttpStatus.OK.value());
            httpResponse.setContentType("application/json;charset=utf8");
            httpResponse.getWriter().write("{\"code\":" + code + ", \"msg\":\"" + msg + "\"}");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 	支持跨域
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "Authorization,Origin,X-Requested-With,Content-Type,Accept");
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(httpServletRequest, httpServletResponse);
    }

    @Override
    protected AuthenticationToken createToken(ServletRequest servletRequest, ServletResponse servletResponse) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String Authorization = request.getHeader(AuthorityConstants.AUTHORIZATION_HEADER);
        String andimRealm = request.getHeader(AuthorityConstants.PLATFORMTYPE_HEADER);
        if (StringUtils.isNotBlank(Authorization) && Authorization.startsWith(AuthorityConstants.BEARER_HEADER)) {
			String token = Authorization.replace(AuthorityConstants.BEARER_HEADER, "");
			if (StringUtils.isBlank(andimRealm)) {
				andimRealm=AuthorityConstants.FRONT_PLATFORM;
			}
			return new JWTToken(token,andimRealm);
		}
        return null;
    }
}
