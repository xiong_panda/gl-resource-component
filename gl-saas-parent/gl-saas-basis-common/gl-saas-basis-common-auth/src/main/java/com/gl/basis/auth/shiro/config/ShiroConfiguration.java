package com.gl.basis.auth.shiro.config;

import org.apache.commons.collections.map.HashedMap;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import com.gl.basis.auth.shiro.StatelessDefaultSubjectFactory;
import com.gl.basis.auth.shiro.filter.JWTFilter;
import com.gl.basis.auth.shiro.realm.SaasRealm;
import com.gl.basis.auth.util.JWTUtil;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Filter;

/**
 *  shiro 配置中心
 * @author Administrator
 *
 */
@Configuration
public class ShiroConfiguration {
	@Bean(name = "saasRealm")
	public SaasRealm authRealm() {
		SaasRealm authRealm = new SaasRealm();
		authRealm.setCachingEnabled(true);
		return authRealm;
	}

	@Bean(name = "jwtFilter")
	public JWTFilter jwtFilter() {
		return new JWTFilter();
	}
	@Bean(name = "jWTUtil")
	public JWTUtil jWTUtil() {
		return new JWTUtil();
	}
	
	@Bean(name = "subjectFactory")
	public StatelessDefaultSubjectFactory subjectFactory() {
		StatelessDefaultSubjectFactory statelessDefaultSubjectFactory = new StatelessDefaultSubjectFactory();
		return statelessDefaultSubjectFactory;
	}

	@Bean(name = "sessionManager")
	public DefaultSessionManager sessionManager() {
		DefaultSessionManager sessionManager = new DefaultSessionManager();
		sessionManager.setSessionValidationSchedulerEnabled(false);
		return sessionManager;
	}

	@Bean(name = "defaultSessionStorageEvaluator")
	public DefaultSessionStorageEvaluator defaultSessionStorageEvaluator() {
		DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
		defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
		return defaultSessionStorageEvaluator;
	}

	@Bean(name = "subjectDAO")
	public DefaultSubjectDAO subjectDAO(
			@Qualifier("defaultSessionStorageEvaluator") DefaultSessionStorageEvaluator defaultSessionStorageEvaluator) {
		DefaultSubjectDAO defaultSubjectDAO = new DefaultSubjectDAO();
		defaultSubjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
		return defaultSubjectDAO;
	}

	@Bean(name = "securityManager")
	public SecurityManager securityManager(@Qualifier("saasRealm") SaasRealm authRealm,
			@Qualifier("subjectDAO") DefaultSubjectDAO subjectDAO,
			@Qualifier("sessionManager") DefaultSessionManager sessionManager,
			@Qualifier("subjectFactory") StatelessDefaultSubjectFactory subjectFactory) {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		securityManager.setRealm(authRealm);
		securityManager.setSubjectDAO(subjectDAO);
		securityManager.setSubjectFactory(subjectFactory);
		securityManager.setSessionManager(sessionManager);
		return securityManager;
	}

	

	@SuppressWarnings("all")
	@Bean
	public FilterRegistrationBean delegatingFilterProxy() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		DelegatingFilterProxy proxy = new DelegatingFilterProxy();
		proxy.setTargetFilterLifecycle(true);
		proxy.setTargetBeanName("shiroFilter");
		filterRegistrationBean.setFilter(proxy);
		return filterRegistrationBean;
	}

	@Bean(name = "shiroFilter")
	public ShiroFilterFactoryBean shiroFilter(@Qualifier("securityManager") SecurityManager securityManager,
			@Qualifier("jwtFilter") JWTFilter jwtFilter) {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(securityManager);
		
		@SuppressWarnings("unchecked")
		Map<String, Filter> filters = new HashedMap(2);
		filters.put("jwtFilter", jwtFilter);
		shiroFilterFactoryBean.setFilters(filters);
		// 拦截链
		Map<String, String> filterMap = new LinkedHashMap<>();

		filterMap.put("/**", "jwtFilter");

		// perms -- 具有某中权限 (使用注解配置授权)
		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
		return shiroFilterFactoryBean;
	}

	/**
	 * 使用shiro注解
	 * 
	 * @return
	 */
	@Bean(name = "advisorAutoProxyCreator")
	public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		advisorAutoProxyCreator.setProxyTargetClass(true);
		return advisorAutoProxyCreator;
	}

	@Bean(name = "authorizationAttributeSourceAdvisor")
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
		return authorizationAttributeSourceAdvisor;
	}
	
	/**
	 * 解决no session
	 */
	/*@Bean
	public OpenEntityManagerInViewFilter openEntityManagerInViewFilter() {
		return new OpenEntityManagerInViewFilter();
	}*/
}
