package com.gl.basis.auth.controller;

import com.gl.basis.auth.response.ProfileResult;
import io.jsonwebtoken.Claims;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseController {

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected String mobile;
    protected String userId;
    protected String username;
    protected String fMidCompanyid;
    protected String companyUserType;
    protected String companyName;
    protected String userType;
    /**
     * 联盟id
     */
    protected String fMidLmid;

    protected Claims claims;

    protected Map<String, String> cooperation;
    protected Map<String, List<String>>  companyIds = new HashMap<>();
    //使用shiro获取
    @ModelAttribute
    public void setResAnReq(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;

        //获取session中的安全数据
        Subject subject = SecurityUtils.getSubject();
        //1.subject获取所有的安全数据集合
        PrincipalCollection principals = subject.getPrincipals();


        if (principals != null && !principals.isEmpty()) {
            //2.获取安全数据
            ProfileResult result = (ProfileResult) principals.getPrimaryPrincipal();
            this.fMidCompanyid = result.getfMidCompanyid();
            this.userId = result.getUserId();
            this.username = result.getUsername();
            this.mobile = result.getMobile();
            this.companyUserType = result.getCompanyUserType();
            this.userType = result.getUserType();
            this.fMidLmid = result.getfMidLmid();
            this.companyName = result.getCompanyName();
            this.cooperation = result.getCooperation();
            this.companyIds = result.getCompanyIds();

        }
    }

}
