package com.gl.basis.auth.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lombok.Data;
/**
 * 角色
 * @author code_generator
 */
@Data
public class Role implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键ID **/
	private String roleId ; 

	/** 角色名称 **/
	private String name ; 

	/** 说明 **/
	private String description ; 

	/** 企业id **/
	private String companyId ; 

	 private List<Permission> permissions = new ArrayList<Permission>(0);//角色与模块  多对多

}
