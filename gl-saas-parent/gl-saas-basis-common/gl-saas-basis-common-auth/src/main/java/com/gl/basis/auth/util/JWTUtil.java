package com.gl.basis.auth.util;



import org.springframework.beans.factory.annotation.Value;

import com.gl.basis.auth.response.ProfileResult;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.Map;

/**
 *  jwt验证工具
 * @author Administrator
 *
 */
public class JWTUtil {
	
    
	
    //签名私钥
    @Value("${jwt.tokenKey}")
    private String key;
    //签名的失效时间
    @Value("${jwt.tokenExpireTime}")
    private Long ttl;

    /**
     * 		设置认证token
     *      id:登录用户id
     *      subject：登录用户名
     *
     */
    public String createJwt(String id, String name, Map<String,Object> map) {
        //1.设置失效时间
        long now = System.currentTimeMillis();//当前毫秒
        long exp = now + ttl;
        //2.创建jwtBuilder
        JwtBuilder jwtBuilder = Jwts.builder().setId(id).setSubject(name)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, key);
        //3.根据map设置claims
        for(Map.Entry<String,Object> entry : map.entrySet()) {
            jwtBuilder.claim(entry.getKey(),entry.getValue());
        }
        jwtBuilder.setExpiration(new Date(exp));
        //4.创建token
        String token = jwtBuilder.compact();
        return token;
    }
    /**
     * 		设置认证token
     *      id:登录用户id
     *      subject：登录用户名
     *
     */
    public String createJwt(String id, String name, ProfileResult ce) {
    	//1.设置失效时间
    	long now = System.currentTimeMillis();//当前毫秒
    	long exp = now + ttl;
    	//2.创建jwtBuilder
    	JwtBuilder jwtBuilder = Jwts.builder().setId(id).setSubject(name)
    			.setIssuedAt(new Date())
    			.signWith(SignatureAlgorithm.HS256, key);
    	//3.根据map设置claims
    	jwtBuilder.claim("data",ce);
    	jwtBuilder.setExpiration(new Date(exp));
    	//4.创建token
    	String token = jwtBuilder.compact();
    	return token;
    }


    /**
     * 	解析token字符串获取clamis
     */
    public Claims parseJwt(String token) {
        Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
        return claims;
    }
 
    /**
     * 	解析token字符串获取clamis
     */
    public String getName(String token) {
       String sub = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
        return sub;
    }
 
    
}
