package com.gl.basis.auth.shiro.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.gl.basis.auth.shiro.handler.RefurbishTokenHandler;

@Configuration
public class TokenInterceptorConfig extends WebMvcConfigurationSupport {

	@Resource
	private RefurbishTokenHandler refurbishTokenHandler;

	@Override
	protected void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(refurbishTokenHandler);
		// 指定拦截器的url地址
		// .addPathPatterns("/**")
		// 指定不拦截的url地址
		// .excludePathPatterns("","");
	}

}
