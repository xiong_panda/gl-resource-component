package com.gl.basis.auth.constants;

import java.util.Date;

/**
 * · 权限常量
 * 
 * @author xiehong
 *
 */
public interface AuthorityConstants {

	/**
	 * 注册时用户类型选择
	 */

	public static Integer USER = 0;
	public static Integer COMPANY = 1;

	/**
	 * token认证头
	 */
	public static  String BEARER_HEADER ="Bearer ";
	public static  String AUTHORIZATION_HEADER ="Authorization";
	public static  String PLATFORMTYPE_HEADER ="platformType";
	public static  String APPLICATION_JSON ="application/json;charset=UTF-8";
	
	

	/**
	 * 用户类型 ： comUser 普通用户；comAdmin：企业用户：saasAdmin ;服务商用户：serviceUser ；匿名用户：anonUser
	 */
	public static String SAASA_DMIN = "saasAdmin";
	//public static String COMPANY_ADMIN = "comAdmin";
	//public static String COMPANY_USER = "comUser";
	public static String SERVICE_USER = "serviceUser";
	public static String ANON_USER = "anonUser";

	

	/**
	 * 存储当前用户等级
	 */
	public static String USER_LEVEL = "USER_LEVEL";

	/**
	 * 匿名可访问角色
	 */
	public static final int EN_VISIBLE = 2;
	/**
	 * 当前用户
	 */
	public static String CURRENT_USER = "CURRENT_USER";

	/** token过期时间 **/
	public static final long EXPRIE_TIME = 5 * 60 * 1000;



	
	/** 可见性en_visible 0：公开，1:登录可见，2：联盟 **/
	public static final String VISIBLE_USER = "0";
	public static final String VISIBLE_ALL = "1";
	public static final String VISIBLE_LEAGUE = "2";

	/** 获取当前时间 **/
	public static final Date TIME_NOW = new Date(System.currentTimeMillis());

	/** 登陆token **/
	public static final String JWT_DEFAULT_TOKEN_NAME = "token";

	/** JWT用户名 **/
	public static final String JWT_USERNAME = "username";

	/** JWT刷新新token响应状态码 **/
	public static final int JWT_REFRESH_TOKEN_CODE = 460;

	/**
	 * JWT刷新新token响应状态码， Redis中不存在，但jwt未过期，不生成新的token，返回361状态码
	 */
	public static final int JWT_INVALID_TOKEN_CODE = 461;

	/** JWT Token默认密钥 **/
	public static final String JWT_DEFAULT_SECRET = "666666";

	/** JWT 默认过期时间，3600L，单位秒 **/
	public static final Long JWT_DEFAULT_EXPIRE_SECOND = 3600L;

	

	
	/**角色最高等级标识*/
	public static final int ROLE_HIGHE_LEVEL = 0;
	/**角色层级递进标识*/
	public static final int ROLE_NEXT_LEVEL = 1;

	/**pid为0的为应用系统**/
	public static final String APPLICATION_FLAG="0";
	/**################################ 平台可见码值  start ####################################################**/

	/**	 * 企业可见性0:不可见；1：可见	 */
	public static final String COMPANY_HIDDEN = "0";
	public static final String COMPANY_VISIBLE = "1";
	
	/**################################ 平台可见码值  end ####################################################**/
	/**################################ 启用码值  start ####################################################**/
	/** 状态status 0：启用，1：禁用,新增数据状态默认为启用 **/
	public static final String STATUS_OFF = "1";
	public static final String STATUS_ON = "0";
	
	
	/**################################ 启用码值  end ####################################################**/
	/**################################ 删除码值  start ####################################################**/
	
	/** 删除标志del_flag 0：正常，1：删除,新增数据删除标志默认为正常 **/
	public static final String DEL_YES = "1";
	public static final String DEL_NO = "0";

	/**################################ 删除码值  end ####################################################**/
	/**################################ 菜单码值  start ####################################################**/
	/** 菜单类型 type 1：菜单，2：功能，3：Api **/
	public static final String PERMISSION_MENU = "1";
	public static final String PERMISSION_POINT = "2";
	public static final String PERMISSION_API = "3";
	/**################################ 菜单码值  end ####################################################**/
	
	
	/**################################ 客户类型码值  start ####################################################**/
	
	/** 后台管理员 */
	public static final String SUPER_ADMIN = "00000001";
	
	/** 后台操作员 */
	public static final String PLATFORM_OPERATOR = "00000002";
	
	
	/**	 * 企业用户类型 值码：企业超级用户	 */
	public static final String COMPANY_ADMIN = "00000003";
	/**	 * 企业用户类型 值码：企业超操作用户	 */
	public static final String COMPANY_USER = "00000004";
	
	/** 普通客户 */
	public static final String AVERAGE_USER = "00000005";
	

	/**个人用户类型：潜客，车主**/
	public static final String USER_POTENTIAL= "00000006";
	/**车主**/
	public static final String USER_CAR_OWNER= "00000007";
	
	
	/**################################ 客户类型码值   end ####################################################**/
	
	
	/**################################ 协作关系码值  start ####################################################**/
	
	/**	 *	 协作关系(企业间）	 */
	public static final String ENTERPRISE_STATUS ="00000046";
	
	
	
	/**################################ 协作关系码值  end ####################################################**/
	
	
	
	
	/**################################ 平台标识码值  start ####################################################**/
	
	/**	 * 用户来源 码值：platform_type 国龙平台	 */
	public static final String GL_PLATFORM = "00000010";
	/**	 * 用户来源 码值：platform_type 前端 (默认为代理商平台)	 */
	public static final String FRONT_PLATFORM = "00000020";
	/**	 资源平台 用户来源 码值：platform_type )	 */
	public static final String RESOURCE_TYPE = "00000015";
	
	/**	 *城市群 用户来源 码值：platform_type )	 */
	public static final String PLATFORM_TYPE = "PLATFORM_CITY_TYPE";

	
	/**################################ 平台标识码值  end ####################################################**/
	/**################################ 企业应用码值  start ####################################################**/
	/**
	 * 企业基础应用码值
	 */
	public static final String ENTERPRISE_BASE="00000016";
	
	/**
	 * 企业联盟应用码值
	 */
	public static final String UNION_APPLY="00000012";
	
	
	
	
	
	
	
	
	/**################################ 企业应用码值  end ####################################################**/

	
	/**################################ 联盟初始化常量  start ####################################################**/
	
	/**	 * 盟员最大数量	 */
	public static final Integer MAX_UNION_MEMBER = 10;
	
	/**	 * 关键业务最大数	 */
	public static final Integer MAX_BUSINESS = 10;
	
	/**	 * 下级单位最大数	 */
	public static final Integer MAX_UNIT = 10;
	
	/**	 * 企业用户数上限	 */
	public static final Integer MAX_EMPLOYEE = 100;
	
	/**	 * 企业管理员人数上限	 */
	public static final Integer MAX_ADMINISTRATORS = 1;
	
	
	
	
	
	
	
	
	/**################################ 联盟初始化常量  end ####################################################**/
	/**################################ 系统等级类型常量  start ####################################################**/
	/***
	 * 系统等级类型
	 */
	public static final String SYS_LEVEL="SYS_LEVEL";
	/**################################ 系统等级类型常量  end ####################################################**/
	/**################################ 联盟前端标志常量  start ####################################################**/
	
	// 联盟标志（1:盟主；0:盟员）
	public static final String UNION_LEADER="1";
	public static final String UNION_MEMBER="0";

	/**################################ 联盟前端标志常量  end ####################################################**/
	
	/**################################ 平台登录标志常量  start ####################################################**/
	
	public static final String SERVICE_TYPE="SERVICE_TYPE";
	
	/**################################ 平台登录标志常量  end ####################################################**/
	
	/**################################ 第三方平台标志常量  start ####################################################**/
	
	public static final String SIGN="sign";
	public static final String APP_KEY="appkey";
	public static final String DATE_FORM="yyyy-MM-dd HH:mm:ss";
	public static final String TIMESTAMP="timestamp";
	public static final String CODE="code";
	public static final String MSG="msg";
	public static final String SIGN_METHOD="sign_method";
	public static final String SIGN_MD5="MD5";
	public static final String SIGN_RSA="RSA";
	public static final String REQ_METHOD="method";
	public static final String BZ_DATA="bz_data";
	/**是否幂等key*/
	public static final String GATEWAY_IDEMPOTENCY="GATEWAY:IDEMPOTENCY:";
	/**是否幂等*/
	public static final String DEMPOTENCY_CODE="1";
	/**是否收费*/
	public static final String COLLECTFESS_CODE="1";
	
	/**################################ 第三方平台标志常量  end ####################################################**/
	

}
