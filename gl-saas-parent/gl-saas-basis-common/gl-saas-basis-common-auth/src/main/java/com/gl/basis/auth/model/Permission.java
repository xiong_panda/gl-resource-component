package com.gl.basis.auth.model;

import java.util.ArrayList;
import java.util.List;
/**
 * 注意:注解只能加在属性字段上才会生效!
 * 角色权限
 * @author code_generator
 */
public class Permission implements java.io.Serializable{
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getFunctionCode() {
		return functionCode;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getApiMethod() {
		return apiMethod;
	}

	public void setApiMethod(String apiMethod) {
		this.apiMethod = apiMethod;
	}

	public String getApLevel() {
		return apLevel;
	}

	public void setApLevel(String apLevel) {
		this.apLevel = apLevel;
	}

	public String getPointIcon() {
		return pointIcon;
	}

	public void setPointIcon(String pointIcon) {
		this.pointIcon = pointIcon;
	}

	private static final long serialVersionUID = 1L;

	/** 主键ID **/
	private String fPkid ; 

	/** 权限描述 **/
	private String description ; 

	/** 权限名称 **/
	private String functionName ; 

	/** 权限类型 1 为菜单2为功能 3为API **/
	private String functionType ; 

	/** 父id **/
	private String fParentid ; 

	/** 权限标识 **/
	private String functionCode ;

	/** 企业可见性 00000009:前后台,00000010：后台, 00000011:前台**/
	private String enVisible ;

	private String apiUrl ;
	
	private String apiMethod ; 
	
	private String apLevel ; 
	
	private String mIcon ; 
	private String mFunctionUrl ; 
	private String mSequence ; 
	
	private String pointIcon ;
	private String component ;
	private String icon ;
	private String name ;
	private String fAppId ;
	/** 按钮 **/
	private String button ;

	public String getButton() {
		return button;
	}

	public void setButton(String button) {
		this.button = button;
	}

	public String getfAppId() {
		return fAppId;
	}

	public void setfAppId(String fAppId) {
		this.fAppId = fAppId;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getfPkid() {
		return fPkid;
	}

	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}

	public String getfParentid() {
		return fParentid;
	}

	public void setfParentid(String fParentid) {
		this.fParentid = fParentid;
	}

	public String getmIcon() {
		return mIcon;
	}

	public void setmIcon(String mIcon) {
		this.mIcon = mIcon;
	}

	public String getmFunctionUrl() {
		return mFunctionUrl;
	}

	public void setmFunctionUrl(String mFunctionUrl) {
		this.mFunctionUrl = mFunctionUrl;
	}

	public String getmSequence() {
		return mSequence;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setmSequence(String mSequence) {
		this.mSequence = mSequence;
	} 



	private List<Permission> children = new ArrayList<Permission>();

	public List<Permission> getChildren() {
		return children;
	}

	public void setChildren(List<Permission> children) {
		this.children = children;
	}

	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getEnVisible() {
		return enVisible;
	}

	public void setEnVisible(String enVisible) {
		this.enVisible = enVisible;
	}

}
