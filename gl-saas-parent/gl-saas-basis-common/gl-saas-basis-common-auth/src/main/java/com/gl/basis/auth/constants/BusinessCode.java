package com.gl.basis.auth.constants;

/**
 * 	返回值枚举
 * @author xiehong
 *
 */
public enum BusinessCode {
	
	SUPPLIER_STATUS("00000060", "SUPPLIER_STATUS","临时供应商"),//供应商（协作企业状态）
	DPAM_STATUS("00000051", "DPAM_STATUS","新建站"),//经销商、配件代理商、制造厂（协作企业状态）
	PRAV_STATUS("00000051", "PRAV_STATUS","新建站"),//服务商、配件商（协作企业状态）

	;

	/** 枚举值 */
	private final String code;

	private final String type;
	/** 枚举描述 */
	private final String message;

	/**
	 *
	 * @param code 枚举值
	 * @param message 枚举描述
	 */
	private BusinessCode(String code, String type,String message ) {
		this.code = code;
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}

	/**
	 * @return Returns the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return Returns the code.
	 */
	public String code() {
		return code;
	}

	/**
	 * @return Returns the message.
	 */
	public String message() {
		return message;
	}

	/**
	 * 通过枚举<code>code</code>获得枚举
	 *
	 * @param code
	 * @return AttachmentEnum
	 */
	public static BusinessCode getByCode(String code) {
		for (BusinessCode _enum : values()) {
			if (_enum.getCode().equals(code)) {
				return _enum;
			}
		}
		return null;
	}
	public static String getType(String Type) {
		for (BusinessCode _enum : values()) {
			if (_enum.getType().equals(Type)) {
				return _enum.getCode();
			}
		}
		return null;
	}

	/**
	 * 获取全部枚举
	 *
	 * @return List<AttachmentEnum>
	 */
	public static java.util.List<BusinessCode> getAllEnum() {
		java.util.List<BusinessCode> list = new java.util.ArrayList<>(values().length);
		for (BusinessCode _enum : values()) {
			list.add(_enum);
		}
		return list;
	}

	/**
	 * 获取全部枚举值
	 *
	 * @return List<String>
	 */
	public static java.util.List<String> getAllEnumCode() {
		java.util.List<String> list = new java.util.ArrayList<>(values().length);
		for (BusinessCode _enum : values()) {
			list.add(_enum.code());
		}
		return list;
	}

	/**
	 * 通过code获取msg
	 * @param code 枚举值
	 * @return
	 */
	public static String getMsgByCode(String code) {
		if (code == null) {
			return null;
		}
		BusinessCode _enum = getByCode(code);
		if (_enum == null) {
			return null;
		}
		return _enum.getMessage();
	}
	/**
	 * 通过code获取msg
	 * @param code 枚举值
	 * @return
	 */
	public static String getCode(String msg) {
		if (msg == null) {
			return null;
		}
		for (BusinessCode _enum : values()) {
			if (_enum.getMessage().equals(msg)) {
				return _enum.getCode();
			}
		}

		return null;

	}

	/**
	 * 获取枚举code
	 * @param _enum
	 * @return
	 */
	public static String getCode(BusinessCode _enum) {
		if (_enum == null) {
			return null;
		}
		return _enum.getCode();
	}

}
