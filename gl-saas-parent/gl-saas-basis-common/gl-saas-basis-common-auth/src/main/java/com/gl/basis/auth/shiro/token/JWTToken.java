package com.gl.basis.auth.shiro.token;

import java.io.Serializable;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * 	token 实体
 * @author Administrator
 *
 */
public class JWTToken implements AuthenticationToken  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 密钥
    private String token;
    private String platformType;

   

    public JWTToken(String token, String platformType) {
		this.token = token;
		this.platformType = platformType;
	}

	public String getPlatformType() {
		return platformType;
	}

	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}

	@Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
