package com.gl.basis.auth.shiro.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gl.basis.auth.response.ProfileResult;
import com.gl.basis.auth.shiro.token.JWTToken;
import com.gl.basis.auth.util.JWTUtil;

import io.jsonwebtoken.Claims;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

/***
 * 	公共的realm：
 * 	认证
 *
 * @author Administrator
 *
 */
public class SaasRealm extends AuthorizingRealm {

	@Resource
	private JWTUtil jWTUtil;

	 @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }



	 /**
     * 	授权
     *
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
    	// 1.获取安全数据
		ProfileResult result = (ProfileResult) principalCollection.getPrimaryPrincipal();
		// 2.获取权限信息
		@SuppressWarnings("unchecked")
		Set<String> apisPerms = (Set<String>) result.getRoles().get("apis");
		@SuppressWarnings("unchecked")
		Set<String> pointsPerms = (Set<String>) result.getRoles().get("points");
		@SuppressWarnings("unchecked")
		Set<String> menusPerms = (Set<String>) result.getRoles().get("menus");
		if (apisPerms== null ||apisPerms.isEmpty()) {
			apisPerms = new HashSet<>();
		}
		if(null != menusPerms && !menusPerms.isEmpty() ) {
			apisPerms.addAll(menusPerms);
		}
		if(null != pointsPerms&& !pointsPerms.isEmpty()) {
			apisPerms.addAll(pointsPerms);
		}
		// 3.构造权限数据，返回值
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		List<String> list = JSONObject.parseArray(apisPerms.toString(),String.class);
		info.setStringPermissions(new HashSet<>(list));
		return info;
    }

    /**
     *	 认证
     */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
	String  token = (String) authenticationToken.getCredentials();
		Claims parseJwt = jWTUtil.parseJwt(token);
		//获取token 中的数据
		 Object obj = parseJwt.get("data");
		SimpleAuthenticationInfo info = null;
		if (obj instanceof Map) {
			ProfileResult profileResult = JSONObject.parseObject(JSON.toJSONString(obj), ProfileResult.class);
			 info = new SimpleAuthenticationInfo(profileResult, token, this.getName());
		}

		return info;
	}



}
