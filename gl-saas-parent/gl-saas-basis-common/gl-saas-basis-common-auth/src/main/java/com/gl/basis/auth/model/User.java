package com.gl.basis.auth.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 用户信息表
 * @author code_generator
 */
public class User implements java.io.Serializable{
	private static final long serialVersionUID = 1L;


	

	/** 用户ID **/
    @ApiModelProperty(value = "用户ID")
	private String fPkid ; 

	/** 手机号码 **/
    @ApiModelProperty(value = "手机号码")
	private String mobile ; 

    /** 用户昵称 **/
    @ApiModelProperty(value = "用户昵称")
    private String userNickname ; 
	/** 用户名 **/
    @ApiModelProperty(value = "用户名")
	private String userName ; 
    /** 性别 **/
    @ApiModelProperty(value = "性别")
    private String sex ; 

	/** 用户密码 **/
    @ApiModelProperty(value = "用户密码")
	private String passWord ; 

	/** 所属企业ID **/
	private String fMidCompanyid ; 

	/** 个人用户类型 码值：车主、潜客 **/
    @ApiModelProperty(value = "个人用户类型 码值：车主、潜客")
	private String userType ; 

	/** 企业用户类型 值码：企业超级用户、企业普通用户 **/
    @ApiModelProperty(value = "企业用户类型 值码：企业超级用户、企业普通用户")
	private String companyUserType ; 

	/** 用户来源 码值：platform_type **/
    @ApiModelProperty(value = "用户来源 码值：platform_type")
	private String platformType ; 

	/** 用户身份证号 **/
    @ApiModelProperty(value = "用户身份证号")
	private String idNumber ; 
    /** 用户生日 **/
    @ApiModelProperty(value = "用户生日")
    private String birthday ; 

	/** 签发机关 **/
    @ApiModelProperty(value = "签发机关")
	private String issuingAuthority ; 

	/** 证件签发日期 **/
    @ApiModelProperty(value = "证件签发日期")
	private Date issuingStartTime ; 

	/** 证件有效截至日期 **/
    @ApiModelProperty(value = "证件有效截至日期")
	private Date issuingEndTime ; 

	/** 身份证住址 **/
    @ApiModelProperty(value = "身份证住址")
	private String idCardAddr ; 

	/** 民族 **/
    @ApiModelProperty(value = "民族")
	private String nationality ; 

	/** qq号 **/
    @ApiModelProperty(value = "qq号")
	private String qq ; 

	/** 支付宝账号 **/
    @ApiModelProperty(value = "支付宝账号")
	private String alipayAccount ; 

	/** 微信号 **/
    @ApiModelProperty(value = "微信号")
	private String microsignal ; 

	/** 邮箱 **/
    @ApiModelProperty(value = "邮箱")
	private String email ; 

	/** 邮编 **/
    @ApiModelProperty(value = "邮编")
	private String postcode ; 

	/** 现居住地址 **/
    @ApiModelProperty(value = "现居住地址")
	private String contactAddress ; 

	/** 积分 **/
    @ApiModelProperty(value = "积分")
	private Integer integral ; 

	/** 岗位id **/
    @ApiModelProperty(value = "岗位id")
	private String fMidPostid ; 

	/** 角色ID **/
    @ApiModelProperty(value = "角色ID")
	private String fMidRoleid ; 

	/** 部门ID 可能有多个部门 **/
    @ApiModelProperty(value = "部门ID 可能有多个部门")
	private String fMidDeptid ; 

	/** 备注 **/
    @ApiModelProperty(value = "备注")
	private String remark ; 

	/** 删除标记（1：已删除，0：正常） **/
    @ApiModelProperty(value = "删除标记（1：已删除，0：正常）")
	private String fIsdelete ; 

	/** 启用状态 **/
    @ApiModelProperty(value = "启用状态")
	private String fEnable ; 

    /**企业名称*/
    private String companyName ; 

	/**
	 * 联盟id
	 */
	private String fMidLmid;
	/** 用户账号 **/
    @ApiModelProperty(value = "用户账号")
	private String userAccount ; 

	private List<Permission> permissions = new ArrayList<Permission>(0);//角色与模块  多对多


	private List<Role> roles = new ArrayList<Role>();//用户与角色   多对多


	public String getfPkid() {
		return fPkid;
	}


	public void setfPkid(String fPkid) {
		this.fPkid = fPkid;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getUserNickname() {
		return userNickname;
	}


	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}


	public String getPassWord() {
		return passWord;
	}


	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}


	public String getfMidCompanyid() {
		return fMidCompanyid;
	}


	public void setfMidCompanyid(String fMidCompanyid) {
		this.fMidCompanyid = fMidCompanyid;
	}


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getCompanyUserType() {
		return companyUserType;
	}


	public void setCompanyUserType(String companyUserType) {
		this.companyUserType = companyUserType;
	}


	public String getPlatformType() {
		return platformType;
	}


	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}


	public String getIdNumber() {
		return idNumber;
	}


	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}


	public String getBirthday() {
		return birthday;
	}


	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}


	public String getIssuingAuthority() {
		return issuingAuthority;
	}


	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}


	public Date getIssuingStartTime() {
		return issuingStartTime;
	}


	public void setIssuingStartTime(Date issuingStartTime) {
		this.issuingStartTime = issuingStartTime;
	}


	public Date getIssuingEndTime() {
		return issuingEndTime;
	}


	public void setIssuingEndTime(Date issuingEndTime) {
		this.issuingEndTime = issuingEndTime;
	}


	public String getIdCardAddr() {
		return idCardAddr;
	}


	public void setIdCardAddr(String idCardAddr) {
		this.idCardAddr = idCardAddr;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public String getQq() {
		return qq;
	}


	public void setQq(String qq) {
		this.qq = qq;
	}


	public String getAlipayAccount() {
		return alipayAccount;
	}


	public void setAlipayAccount(String alipayAccount) {
		this.alipayAccount = alipayAccount;
	}


	public String getMicrosignal() {
		return microsignal;
	}


	public void setMicrosignal(String microsignal) {
		this.microsignal = microsignal;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPostcode() {
		return postcode;
	}


	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}


	public String getContactAddress() {
		return contactAddress;
	}


	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}


	public Integer getIntegral() {
		return integral;
	}


	public void setIntegral(Integer integral) {
		this.integral = integral;
	}


	public String getfMidPostid() {
		return fMidPostid;
	}


	public void setfMidPostid(String fMidPostid) {
		this.fMidPostid = fMidPostid;
	}


	public String getfMidRoleid() {
		return fMidRoleid;
	}


	public void setfMidRoleid(String fMidRoleid) {
		this.fMidRoleid = fMidRoleid;
	}


	public String getfMidDeptid() {
		return fMidDeptid;
	}


	public void setfMidDeptid(String fMidDeptid) {
		this.fMidDeptid = fMidDeptid;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getfIsdelete() {
		return fIsdelete;
	}


	public void setfIsdelete(String fIsdelete) {
		this.fIsdelete = fIsdelete;
	}


	public String getfEnable() {
		return fEnable;
	}


	public void setfEnable(String fEnable) {
		this.fEnable = fEnable;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getfMidLmid() {
		return fMidLmid;
	}


	public void setfMidLmid(String fMidLmid) {
		this.fMidLmid = fMidLmid;
	}


	public List<Permission> getPermissions() {
		return permissions;
	}


	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}


	public List<Role> getRoles() {
		return roles;
	}


	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


	public String getUserAccount() {
		return userAccount;
	}


	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}



}
