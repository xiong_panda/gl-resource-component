package com.gl.basis.auth.response;

import org.apache.commons.lang3.StringUtils;
import org.crazycake.shiro.AuthCachePrincipal;

import com.gl.basis.auth.constants.AuthorityConstants;
import com.gl.basis.auth.model.Permission;
import com.gl.basis.auth.model.User;

import java.io.Serializable;
import java.util.*;

public class ProfileResult implements Serializable,AuthCachePrincipal {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//用户手机号
	private String mobile;
	//用户id
    private String userId;
    //用户名称
    private String username;
    //企业id
    private String fMidCompanyid;
    //企业类型
    private String companyUserType;
    //企业名称
    private String companyName;
    //用户类型
    private String userType;
    //联盟id
    private String fMidLmid;
    //token生成时间
    private Date expDate;
    
    private Map<String,Object> roles = new HashMap<>();

    private Map<String,String> cooperation = new HashMap<>();
	/**
	 *联盟所有企业id
	 */
    private Map<String,List<String>>  companyIds = new HashMap<>();



	/**
     *
     * @param user
     */
    public ProfileResult(
    		User user, List<Permission> list) {
        this.mobile = user.getMobile();
        this.userId = user.getfPkid();
        this.username = user.getUserName();
        this.companyName = user.getCompanyName();
        this.fMidCompanyid = user.getfMidCompanyid();
        this.companyUserType= user.getCompanyUserType();
        this.userType=user.getUserType();
        this.fMidLmid=user.getfMidLmid();
        this.expDate = new Date();
        Set<String> menus = new HashSet<>();
        Set<String> points = new HashSet<>();
        Set<String> apis = new HashSet<>();
        if (null == list || list.isEmpty()) {
			return;
		}
        for (Permission perm : list) {
            String code = perm.getFunctionCode();
            if(AuthorityConstants.PERMISSION_MENU.equals(perm.getFunctionType())&& StringUtils.isNotBlank(code)) {
                menus.add(code);
            }else if(AuthorityConstants.PERMISSION_POINT.equals(perm.getFunctionType())&& StringUtils.isNotBlank(code)) {
                points.add(code);
            }else if( StringUtils.isNotBlank(code)) {
                apis.add(code);
            }
        }
        this.roles.put("menus",menus);
        this.roles.put("points",points);
        this.roles.put("apis",apis);
    }


    @Override
    public String getAuthCacheKey() {
        return null;
    }


	public ProfileResult(String mobile, String userId, String username, String fMidCompanyid, String companyUserType,
			String companyName, String userType, String fMidLmid, Date expDate, Map<String, Object> roles) {
		super();
		this.mobile = mobile;
		this.userId = userId;
		this.username = username;
		this.fMidCompanyid = fMidCompanyid;
		this.companyUserType = companyUserType;
		this.companyName = companyName;
		this.userType = userType;
		this.fMidLmid = fMidLmid;
		this.expDate = expDate;
		this.roles = roles;
	}
	public ProfileResult buildCooperation(Map<String,String> map){
		cooperation.putAll(map);
		return this;
	}
	public ProfileResult buildCompanyIds(Map<String,List<String>> map){
		companyIds.putAll(map);
		return this;
	}

	public Date getExpDate() {
		return expDate;
	}

	public Map<String, String> getCooperation() {
		return cooperation;
	}
	public Map<String, List<String>> getCompanyIds() {
		return companyIds;
	}


	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}


	public ProfileResult() {
	}


	public String getfMidCompanyid() {
		return fMidCompanyid;
	}


	public void setfMidCompanyid(String fMidCompanyid) {
		this.fMidCompanyid = fMidCompanyid;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getCompanyUserType() {
		return companyUserType;
	}


	public void setCompanyUserType(String companyUserType) {
		this.companyUserType = companyUserType;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public Map<String, Object> getRoles() {
		return roles;
	}


	public void setRoles(Map<String, Object> roles) {
		this.roles = roles;
	}


	public String getfMidLmid() {
		return fMidLmid;
	}


	public void setfMidLmid(String fMidLmid) {
		this.fMidLmid = fMidLmid;
	}
    
    
}
