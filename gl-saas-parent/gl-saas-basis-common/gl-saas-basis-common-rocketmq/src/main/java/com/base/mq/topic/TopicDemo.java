package com.base.mq.topic;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

public class TopicDemo {
	
	public static void main(String[] args) throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
		
		DefaultMQProducer producer = new DefaultMQProducer("test_mq");
		
		//设置nameserver的地址
		producer.setNamesrvAddr("192.168.2.247:9877");
		//启动生产者
		producer.start();
		
		/**
		 * 创建topic
		 * key :broker的名称，topic 名称
		 */
		
	//	producer.createTopic("broker01", "test-topic", 4);
	
		//同步發送消息
		String s = "測試 异步消息發送455416。。。";
		Message msg = new Message("test-topic", "add", s.getBytes());
		//SendResult send = producer.send(msg);
		producer.send(msg, new SendCallback() {
			
			@Override
			public void onSuccess(SendResult sendResult) {
					System.out.println(sendResult.getMsgId());
					System.out.println(sendResult);
				System.out.println("发送成功");
			}
			
			@Override
			public void onException(Throwable e) {
				System.out.println(e.getMessage());
				System.out.println("发送失败");
				
			}
		});
	/*	System.out.println(send.getMsgId());
		System.out.println(send.getMessageQueue());
		System.out.println(send.getSendStatus());*/
		System.out.println("topic 創建成功");
	//	producer.shutdown();
	}

}
