package com.base.mq.produce;

import java.util.List;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class OrderProducer {

		//有序创建订单
	public static void main(String[] args) throws Exception {
		DefaultMQProducer producer = new DefaultMQProducer("HAOKE_ORDER_PRODUCER");
		producer.setNamesrvAddr("192.168.189.152:9876");
		producer.start();

		
		
		for (int i = 0; i < 100; i++) {
			String msgStr = "order --> " + i;
			int orderId = i % 10; // 模拟生成订单id    

			Message message = new Message("test-topic", "ORDER_MSG",
					msgStr.getBytes(RemotingHelper.DEFAULT_CHARSET));
			SendResult sendResult = 	producer.send(message, new MessageQueueSelector() {
				@Override
				public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
					return mqs.get((Integer)arg);
				}
			}, i);
			
			
			/*SendResult sendResult = producer.send(message, (mqs, msg, arg) -> {
				Integer id = (Integer) arg;
				int index = id % mqs.size();
				return mqs.get(index);
			}, orderId);
*/
			System.out.println(sendResult);
		}

		producer.shutdown();
	}
}
