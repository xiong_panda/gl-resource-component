package com.gl.sass.mq.push;



/**
 * 	消息推送
 * @author xiehong
 *
 */
public interface RpcMessageService {

    /**
     * 实名认证成功
     * 手机+推送
     *
     * @param pushToken token
     * @param sim       手机号
     */
    void realNameAuthSuccess(String pushToken, String sim);

    /**
     * 	测试消息
     * @param body
     */
	void transfer(String body);

  

}

