package com.gl.sass.mq.push.message;

/**
 * @Author
 * @Date 2018-12-08 14:58
 */
public enum TemplateCode {


    TEMPLATE_SMS_VERIFY_CODE("1000030", "登陆验证码模版", 0),
    ;

    private String code;

    /**
     * 默认值
     */
    private String value;

    /**
     * 推送语音类型
     */
    private Integer pushType;

    TemplateCode(String code, String value, Integer pushType) {
        this.code = code;
        this.value = value;
        this.pushType = pushType;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public Integer getPushType() {
        return pushType;
    }
}
