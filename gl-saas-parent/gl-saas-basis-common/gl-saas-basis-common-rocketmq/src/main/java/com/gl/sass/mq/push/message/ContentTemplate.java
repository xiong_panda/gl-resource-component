package com.gl.sass.mq.push.message;

import java.util.*;

/**
 * 模版内容
 *
 * @author
 * @date 2019/6/18 10:20 AM
 */
public enum ContentTemplate {

    /**
     * 实名认证
     */
    CROWD_REALNAME_AUTH_SUCCESS("CROWD_REALNAME_AUTH_SUCCESS", "您的实名认证已通过，请完成后续操作，获得抢单资格～"),
    CROWD_REALNAME_AUTH_FAIL("CROWD_REALNAME_AUTH_FAIL", "您的实名认证未通过${reason}"),

  
    ;

    private String code;

    /**
     * 默认值
     */
    private String value;

    private static final Map<String, ContentTemplate> LOOKUP = new HashMap<>();

    ContentTemplate(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static String getValue(String code) {
        ContentTemplate template = LOOKUP.get(code);
        return Optional.ofNullable(template).map(ContentTemplate::getValue).orElse(null);
    }

    public static ContentTemplate find(String code) {
        return LOOKUP.get(code);
    }

    static {
        Iterator var0 = EnumSet.allOf(ContentTemplate.class).iterator();

        while (var0.hasNext()) {
            ContentTemplate e = (ContentTemplate)var0.next();
            LOOKUP.put(e.code, e);
        }

    }

    public static void main(String[] args) {
        String field = "一";
        String code = "2⃣";
        System.out.println(String.format("%1$s:%2$s", field, code, "3"));
    }}
