package com.gl.sass.mq.properties;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author xiehong
 *
 */
@Getter
@Setter
public class ConsumerProperties extends BaseProperties {

    private String topic;

    private String tag;

    /**
     * 	每1分钟调整一次线程池，这也是针对消费者来说的
     * 	具体为如果消息堆积超过10W条，则调大线程池，最多64个线程
     * 	如果消息堆积少于8W条，则调小线程池，最少20的线程
     */
    private int threadMin = 2;

    private int threadMax = 4;

    /**
     * 	消息拉取一次的数量 (ThreadMax - ThreadMin) / 2
     */
    private int pullBatchSize = 3;

    /**
     * 	并发消费时，一次消费消息的数量，默认为1
     */
    private int consumerMessageBatchMaxSize = 1;

    @Override
    public String toString() {
        return "ConsumerProperties{" + "topic='" + topic + '\'' + ", tag='" + tag + '\'' + ", threadMin=" + threadMin +
               ", threadMax=" + threadMax + ", pullBatchSize=" + pullBatchSize + ", consumerMessageBatchMaxSize=" +
               consumerMessageBatchMaxSize + "} " + super.toString();
    }
}
