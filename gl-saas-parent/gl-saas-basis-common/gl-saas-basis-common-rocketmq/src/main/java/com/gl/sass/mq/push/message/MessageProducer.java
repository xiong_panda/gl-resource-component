package com.gl.sass.mq.push.message;

import com.gl.sass.mq.producer.RocketMQProducer;

import org.apache.rocketmq.client.producer.SendResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 	消息发送
 * @author xiehong
 *
 */
@Component
public class MessageProducer {

    private static final Logger log = LoggerFactory.getLogger(MessageProducer.class);

    @Autowired
    private RocketMQProducer rocketMQProducer;




    //region 公共方法

    /**
     * 	发送短信验证码
     *
     * @param phoneNumber 手机号
     * @param code        验证码
     */
    public void sendVerifySms(String phoneNumber, String code) {
        Map<String, Object> param = new HashMap<>();
        param.put("smsCode", code);
        sendDXMSms(TemplateCode.TEMPLATE_SMS_VERIFY_CODE.getCode(), phoneNumber, param);
    }

   
    public void sendDXMSms(String templateCode, String phoneNumber, Map<String, Object> param) {
        log.info("msg1=发送短信请求,,phoneNumber={},, content={},, templateCode={}", phoneNumber, param, templateCode);
    }




    //region 私有方法

    public void sendTest(String body) {
    	  log.info("msg1=发送测试请求,,msg={}", body);
    	 SendResult send = rocketMQProducer.send(PlatformTransformer.TOPIC_PLATFORM,
    	            PlatformTransformer.TAG_SMS, body);
    	 log.info("发送测试msgId,,msgId={}",send.getMsgId());
    }
 
  
}
