package com.gl.sass.mq.properties;

import lombok.Getter;

import java.util.Optional;

/**
 * 
 * @author xiehong
 *
 */
@Getter
public class BaseProperties {

    private String groupName;

    private String namesrvAddr;

    public BaseProperties setNamesrvAddr(String namesrvAddr) {
        this.namesrvAddr = namesrvAddr;
        return this;
    }

    public BaseProperties setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public boolean isValid() {
        return Optional.ofNullable(groupName).isPresent() && Optional.ofNullable(namesrvAddr).isPresent();
    }

    @Override
    public String toString() {
        return "BaseProperties{" + "groupName='" + groupName + '\'' + ", namesrvAddr='" + namesrvAddr + '\'' + '}';
    }
}
