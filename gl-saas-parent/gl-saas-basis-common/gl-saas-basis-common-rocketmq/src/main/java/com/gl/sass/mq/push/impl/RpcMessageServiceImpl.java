package com.gl.sass.mq.push.impl;

import com.gl.sass.mq.push.RpcMessageService;
import com.gl.sass.mq.push.message.MessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 	对消息体封装描述
 * @author xiehong
 *
 */
@Slf4j
@Service
public class RpcMessageServiceImpl implements RpcMessageService {

    @Autowired
    private MessageProducer messageProducer;

    @Override
    public void realNameAuthSuccess(String pushToken, String sim) {
      //  this.sendAppPush(MessageTemplate.CROWD_REALNAME_AUTH_APP_SUCCESS, pushToken, sim, new HashMap<>());
      //  this.sendSms(MessageTemplate.CROWD_REALNAME_AUTH_SMS_SUCCESS, sim, new HashMap<>());
    }


    private void formatContent(String template, Map<String, Object> param) {
        String content = template;
        for (Map.Entry<String, Object> entry : param.entrySet()) {
            content = content.replace("${" + entry.getKey() + "}", entry.getValue().toString());
        }
        param.put("content", content);
    }


	@Override
	public void transfer(String body) {
		messageProducer.sendTest(body);		
	}



}
