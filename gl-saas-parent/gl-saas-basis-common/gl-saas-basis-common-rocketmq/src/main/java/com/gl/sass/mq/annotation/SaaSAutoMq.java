package com.gl.sass.mq.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;


/**
 * 
 * @author xiehong
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface SaaSAutoMq {

    String topic();

    String groupName();

    int threadMin() default 2;

    int threadMax() default 4;

    int pullBatchSize() default 3;

    int consumerMessageBatchMaxSize() default 1;

}
