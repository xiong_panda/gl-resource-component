package com.gl.sass.mq.annotation;

import org.springframework.context.annotation.Import;

import com.gl.sass.mq.configure.MQRetryAutoConfigure;

import java.lang.annotation.*;


/**
 *	 是否开启MQ重试策略
 * @author xiehong
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({MQRetryAutoConfigure.class})
public @interface EnableMQRetry {

}
