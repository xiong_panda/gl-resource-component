package com.gl.sass.mq.configure;

import org.apache.logging.log4j.LogManager;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gl.sass.mq.factory.ProduceFactory;
import com.gl.sass.mq.producer.RocketMQProducer;

import javax.annotation.Resource;

/**
 * 
 * @author xiehong
 *
 */
@Configuration
public class ProduceAutoConfigure {
    private static org.apache.logging.log4j.Logger log = LogManager.getLogger(ProduceAutoConfigure.class);

    @Resource
    private ProduceFactory produceFactory;

    @Bean
    public RocketMQProducer rocketMQProducer() throws MQClientException {
        log.info("RocketMQProducer生产者启动！");
        RocketMQProducer producer = produceFactory.buildRocketMQProducer();
        return producer;
    }

}
