package com.gl.sass.mq.push.message;

import java.util.*;


/**
 * 消息模版
 *
 * @author
 * @date 2019/6/18 10:20 AM
 */
public enum MessageTemplate {


    TEMPLATE_SMS_VERIFY_CODE("1000030", "1000030", "登陆验证码模版", null),

    /**
     * 实名认证成功
     */
    CROWD_REALNAME_AUTH_APP_SUCCESS("CROWD_REALNAME_AUTH_APP_SUCCESS", "CROWD_APP_TEMPLATE", "实名认证成功", ContentTemplate.CROWD_REALNAME_AUTH_SUCCESS),
    CROWD_REALNAME_AUTH_SMS_SUCCESS("CROWD_REALNAME_AUTH_SMS_SUCCESS", "CROWD_SMS_TEMPLATE", "实名认证成功", ContentTemplate.CROWD_REALNAME_AUTH_SUCCESS),


  ;

    /**
     * 模版标示
     */
    private String identifier;

    /**
     * 消息系统中的模版编码
     */
    private String code;

    /**
     * 模版描述
     */
    private String value;

    /**
     * 内容模版
     */
    private String template;
    /** 标题 */
    private String title;


    private static final Map<String, MessageTemplate> LOOKUP = new HashMap<>();

    MessageTemplate(String identifier, String code, String value, ContentTemplate template) {
        this.identifier = identifier;
        this.code = code;
        this.value = value;
        this.template = Optional.ofNullable(template).map(ContentTemplate::getValue).orElse(null);
    }

    MessageTemplate(String identifier, String code, String value, ContentTemplate template, String title) {
        this(identifier, code, value, template);
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public String getTemplate() {
        return template;
    }

    public String getTitle() {
        return title;
    }

    public static MessageTemplate find(String code) {
        return LOOKUP.get(code);
    }

    static {
        Iterator var0 = EnumSet.allOf(MessageTemplate.class).iterator();
        while (var0.hasNext()) {
            MessageTemplate e = (MessageTemplate)var0.next();
            LOOKUP.put(e.code, e);
        }
    }

}
